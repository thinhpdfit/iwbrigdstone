var userChatModule = angular.module('UserChat', []);
userChatModule.directive('ngScrollBottom', ['$timeout', function ($timeout) {
	  return {
	    scope: {
	      ngScrollBottom: "="
	    },
	    link: function ($scope, $element) {
	      $scope.$watchCollection('ngScrollBottom', function (newValue) {
	        if (newValue) {
	          $timeout(function(){
	            $element.scrollTop($element[0].scrollHeight);
	          }, 0);
	        }
	      });
	    }
	  }
	}]);
userChatModule.controller('chatCtrl', function($scope, $http) {
	
	$scope.message = "";
	$scope.chatHistory={};
	$scope.currentGroup={};
	$scope.chatGroup=[];
	$scope.stompClient=null;
	$scope.connected=false;
	$scope.init = function() {
		if($scope.userId!=null&&$scope.userId!=''){
			$scope.userId=localStorage['userId'];
		}
		
	 	if($scope.stompClient!=null){
	 		$scope.stompClient.disconnect();
	 	}
	 	if($scope.userId!=null&&$scope.userId!=''){
	 		$scope.createClient();
	 	}else{
	 		$scope.createUser();
	 	}
	};
	$scope.createUser = function() {
		$http({
			method : "GET",
			url : "/newUser",
			
		}).then(function succes(response) {
			console.log(response);
			$scope.userId=response.data;
			localStorage['userId']=response.data;
			$scope.createClient();
		}, function error(response) {
			$scope.error = response.statusText;
		});
	};
	$scope.createClient = function() {
		$http({
			method : "GET",
			url : "/newClient"
		}).then(function succes(response) {
			$scope.client=response.data;
			$scope.connect();
		}, function error(response) {
			$scope.error = response.statusText;
		});
	};
	$scope.connect = function() {
		var socket = new SockJS('/gs-guide-websocket');
		$scope.stompClient = Stomp.over(socket);
		$scope.stompClient.connect({}, function (frame) {
	 	   $scope.connected=true;
	        console.log('Connected: ' + frame);
	       $scope.stompClient.subscribe('/topic/client/'+$scope.client.id,$scope.processResponse);
			$scope.stompClient.send("/app/user/group/"+$scope.userId+'/'+$scope.client.id, {}, JSON.stringify({}));
	    });
	};
	$scope.processResponse=function (response) {
		  var socketResponse=JSON.parse(response.body);
 		  if(socketResponse.groupList!=null){
 			  socketResponse.groupList.forEach(function(item,index){
 					var groupHistory=$scope.chatHistory[item.groupId];
	 				if(groupHistory==null){
	 					groupHistory=[];
	 					var chatGroup=$scope.chatGroup;
	 					chatGroup.push(item);
	 		 			  $scope.$apply(function(){
	 		 				 $scope.chatGroup=chatGroup;
	 		 				$scope.chatHistory[item.groupId]=groupHistory;
	 		 			  });
 		 				if($scope.currentGroup==null||$scope.currentGroup.id==null){
 		 					$scope.currentGroup=item;
 		 				}
 			 			$scope.stompClient.send("/app/user/chatHistory/"+$scope.userId+'/'+$scope.client.id+"/"+item.id, {}, JSON.stringify({}));
		 			$scope.stompClient.subscribe(item.topic+item.id,$scope.processResponse);
 				}
 			  });
 		  }
 		  if(socketResponse.chatMessageList!=null){
 			  socketResponse.chatMessageList.forEach(function(item,index){
	 				$scope.$apply(function(){
	 					var tmp=$scope.chatHistory[item.group.id];
		 				if(tmp==null){
		 					tmp=[];
		 				}
		 				tmp.push(item);
	 	 				 $scope.chatHistory[item.group.id]=tmp;
	 				});
 	 			});
		  }
    };
	$scope.send = function() {
	 	if($scope.stompClient!=null){
	 		if($scope.currentGroup!=null&&$scope.currentGroup.id!=null&& $scope.message!=null&& $scope.message!=''){
	 			var hash=CryptoJS.SHA512( $scope.message).toString(CryptoJS.enc.Hex);
	 			$scope.stompClient.send("/app/user/chat/"+$scope.userId+'/'+$scope.client.id+"/"+$scope.currentGroup.id, {}, JSON.stringify({'message': $scope.message,"hash": hash}));
	        	 $scope.message=null;
	 		}
	 	}
	};
	$scope.changeGroup = function(group) {
		if(group!=null){
			$scope.currentGroup=group;
		}
	};
});
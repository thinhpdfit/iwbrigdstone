var adminChatModule = angular.module('AdminChat', []);
adminChatModule.directive('ngScrollBottom', ['$timeout', function ($timeout) {
	  return {
	    scope: {
	      ngScrollBottom: "="
	    },
	    link: function ($scope, $element) {
	      $scope.$watchCollection('ngScrollBottom', function (newValue) {
	        if (newValue) {
	          $timeout(function(){
	            $element.scrollTop($element[0].scrollHeight);
	          }, 0);
	        }
	      });
	    }
	  }
	}]);
adminChatModule.controller('chatCtrl',['$scope','$http', '$interval',
	function($scope, $http,$interval) {
		$scope.message = "";
		$scope.peddingGroupCount=0;
		$scope.chatHistory={};
		$scope.currentGroup={};
		$scope.adminList=[];
		$scope.chatGroup=[];
		$scope.stompClient=null;
		$scope.connected=false;
		$scope.init = function() {
		 	if($scope.stompClient!=null){
		 		$scope.stompClient.disconnect();
		 	}
		 	if($scope.adminId!=null && $scope.adminId != ""){
				$http({
					method : "GET",
					url : "/newClient"
				}).then(function succes(response) {
					$scope.client=response.data;
					localStorage['admin_id']=$scope.client.id;
					localStorage['admin_secret']=$scope.client.secret;
					$scope.connect();
				}, function error(response) {
					$scope.error = response.statusText;
				});
		 	}
		};
		$scope.connect = function() {
			var socket = new SockJS('/gs-guide-websocket');
			$scope.stompClient = Stomp.over(socket);
			$scope.stompClient.connect({}, function (frame) {
		 	   $scope.connected=true;
		        console.log('Connected: ' + frame);
			       $scope.stompClient.subscribe('/topic/admin/'+$scope.adminId,$scope.processResponse);
			       $scope.stompClient.subscribe('/topic/client/'+$scope.client.id,$scope.processResponse);
				$scope.stompClient.send("/app/admin/group/"+$scope.adminId+'/'+$scope.client.id, {}, JSON.stringify({}));

				$scope.pingInterval = 
					$interval(function() {
						$scope.stompClient.send("/app/admin/ping/"+$scope.adminId+'/'+$scope.client.id, {}, JSON.stringify({}));
			          }, 60*1000);
		    });
		};
		$scope.processResponse=function (response) {
			  var socketResponse=JSON.parse(response.body);
			  if(socketResponse.peddingGroupCount!=null){
				  $scope.$apply(function(){
 	 				 $scope.peddingGroupCount=socketResponse.peddingGroupCount;
				  });
			  }
	 		  if(socketResponse.adminList!=null){
	 			 $scope.$apply(function(){
	 				 $scope.adminList=socketResponse.adminList;
	 			 });
			  }
	 		  if(socketResponse.groupList!=null){
	 			  socketResponse.groupList.forEach(function(item,index){
	 					var groupHistory=$scope.chatHistory[item.groupId];
		 				if(groupHistory==null){
		 					groupHistory=[];
		 					var chatGroup=$scope.chatGroup;
		 					chatGroup.push(item);
		 		 			  $scope.$apply(function(){
		 		 				 $scope.chatGroup=chatGroup;
		 		 				$scope.chatHistory[item.groupId]=groupHistory;
		 		 			  });
	 		 				if($scope.currentGroup==null||$scope.currentGroup.id==null){
	 		 					$scope.currentGroup=item;
	 		 				}
			 			$scope.stompClient.send("/app/admin/chatHistory/"+$scope.adminId+'/'+$scope.client.id+"/"+item.id, {}, JSON.stringify({}));
			 			$scope.stompClient.subscribe(item.topic+item.id,$scope.processResponse);
	 				}
	 			  });
	 		  }
	 		  if(socketResponse.chatMessageList!=null){
	 			  socketResponse.chatMessageList.forEach(function(item,index){
	 					var groupHistory=$scope.chatHistory[item.group.id];
		 				if(groupHistory==null){
		 					groupHistory=[];
		 				}
		 				groupHistory.push(item);
		 				$scope.$apply(function(){
		 	 				 $scope.chatHistory[item.group.id]=groupHistory;
		 				});
	 	 			});
			  }
	    };
		$scope.send = function() {
		 	if($scope.stompClient!=null){
		 		if($scope.currentGroup!=null&&$scope.currentGroup.id!=null&& $scope.message!=null&& $scope.message!=''){
		 			var hash=CryptoJS.SHA512( $scope.message).toString(CryptoJS.enc.Hex);
		 			$scope.stompClient.send("/app/admin/chat/"+$scope.adminId+'/'+$scope.client.id+"/"+$scope.currentGroup.id, {}, JSON.stringify({'message': $scope.message,"hash": hash}));
		        	 $scope.message=null;
		 		}
		 	}
		};
		$scope.getNextGroup = function() {
		 	if($scope.stompClient!=null){
	 			$scope.stompClient.send("/app/admin/nextGroup/"+$scope.adminId+'/'+$scope.client.id, {}, JSON.stringify({}));
		 	}
		};
		$scope.getAdminList = function() {
		 	if($scope.stompClient!=null){
	 			$scope.stompClient.send("/app/admin/adminList/"+$scope.adminId+'/'+$scope.client.id, {}, JSON.stringify({}));
		 	}
		};
		$scope.assign = function(admin) {
			if(admin!=null&&$scope.currentGroup!=null&&$scope.currentGroup.id!=null){
	 			$scope.stompClient.send("/app/admin/assign/"+$scope.adminId+'/'+$scope.client.id+"/"+admin.id+"/"+$scope.currentGroup.id, {}, JSON.stringify({}));
			}
		};
		$scope.changeGroup = function(group) {
			if(group!=null){
				$scope.currentGroup=group;
			}
		};
	}]);
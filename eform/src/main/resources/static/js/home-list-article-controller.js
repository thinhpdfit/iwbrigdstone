/**
 * 
 */
/*var homelistarticleapp = angular.module('homeListArticleApp',['ngResource']);*/
 
 
app.controller("homeListArticleController",function($scope,$sce,$window,$http,PagerService){
	
	$scope.listallarticlepaging = function listallarticlepaging(start,numberofitem){
		$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
		var data ="startIndex=" + start + "&numberofitem=" + numberofitem;
		$http.post('/blog/listallPublishArticlepaging',data).success(function(data,status,header,config){
			$scope.lstArticle = data;
			for(var item in $scope.lstArticle){
				$scope.lstArticle[item].subcontent = $sce.trustAsHtml($scope.lstArticle[item].subcontent)
				//console.log($scope.lstArticle[item].subcontent);
			}
		}).error(function(data,status,header,config){
			
		});
	};
	
	$scope.listhotarticle = function listhotarticle(start,numberofitem){
		$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
		var data ="startIndex=" + start + "&numberofitem=" + numberofitem;
		$http.post('/blog/listallPublishArticleOrderByView',data).success(function(data,status,header,config){
			$scope.lstHotArticle = data;
			/*for(var item in $scope.lstArticle){
				$scope.lstHotArticle[item].subcontent = $sce.trustAsHtml($scope.lstArticle[item].subcontent)
				//console.log($scope.lstArticle[item].subcontent);
			}*/
		}).error(function(data,status,header,config){
			
		});
	};
	
	$scope.pagesize = 10;
	$scope.setPage = setPage;
	$scope.pager = {};
    initController();

    function initController() {
    	
        // initialize to page 1
    	$http.get('/blog/countPublishArticle').success(function(data,status,header,config){
    		$scope.total = data;
    		//alert($scope.total);
    		$scope.setPage(1);
    	}).error(function(data,status,header,config){
    		
    	});
    	
        //$scope.listallarticlepaging(0,$scope.pagesize);
    }

    function setPage(page) {
        if (page < 1 || page >  $scope.pager.totalPages) {
            return;
        }
      
        // get pager object from service
        $scope.pager = PagerService.GetPager($scope.total, page,$scope.pagesize);

        // get current page of items
        /*vm.items = vm.dummyItems.slice(vm.pager.startIndex, vm.pager.endIndex + 1);*/
        $scope.listallarticlepaging($scope.pager.startIndex,$scope.pagesize);
        $('html, body').animate({scrollTop: '0px'}, 300);
	
    }
});

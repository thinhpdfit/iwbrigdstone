/**
 * 
 */

//var artcleDetailApp = angular.module('artcleDetailApp',['ngRoute','ngResource','ngSanitize','ngMaterial']);

/*artcleDetailApp.config(function($routeProvider,$locationProvider) {
	
	// use the HTML5 History API
	//$locationProvider.html5Mode(true);
	
});*/

app.controller("articleDetaiController",function($scope,$sce,$route,$compile,$http,$window,$mdDialog,$document, $location, $anchorScroll){
	$scope.showSearch =true;
	var path = window.location.href;
	var id = path.substring(path.lastIndexOf("/")+1, path.length);
	//$scope.lstcomment=null;
	if(id.indexOf("#") >0){
		id = id.substring(0,id.indexOf("#"));
	}
	
	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
	var data = "id=" + id;
	$http.post("/blog/articlebyid",data).success(function(data,status,header,config){
		$scope.article = data;
		//console.log($scope.article.content);
		var ncontent = $sce.trustAsHtml($scope.article.content);
		$scope.article.content = ncontent;	
		var data2 = "id=" + id;
		$http.post("/blog/commentoarticle",data2).success(function(data,status,header,config){
			
			$scope.lstcomment = data;
			console.log($scope.lstcomment);
			
			
		});
		//$http.defaults.headers.post["Content-Type"] = "application/JSON";
		$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
		data2 = "id=" + $scope.article.id;
		$http.post("/blog/increaseArticleView",data2);
		var data3 = "publishdate=" + $scope.article.publishDate;
		$http.post("/blog/getrelatearticle",data3).success(function(data,status,header,config){
			$scope.lstrelatearticle = data;
			//console.log($scope.lstrelatearticle);		
		}).error(function(data,status,header,config){
			
		});
		
	}).error(function(data,status,header,config){
		
	});
	
	$http.get("/blog/getuserinfo",data).success(function(data,status,header,config){
		$scope.currentuser = data;
		$scope.name = $scope.currentuser.displayName;
		$scope.email = $scope.currentuser.email;
		//console.log($scope.currentuser);
	});
	
	$scope.postcomment = function(article){
		
		$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
		var data ="name=" + $scope.name + "&email=" + $scope.email +"&body=" + $scope.commentcontent + "&articleid=" + article.id + "&createdby=" + $scope.currentuser.id;
		$http.post("/blog/postcomment",data).success(function(data,status,header,config){
			$scope.showAlert('Thành công','Bạn đã gửi bình luận thành công');
			var data2 = "id=" + id;
			$http.post("/blog/commentoarticle",data2).success(function(data,status,header,config){
				$scope.lstcomment = data;
				//$scope.name="";
				//$scope.email="";
				$scope.commentcontent="";
				$scope.article.commentcount+=1;
				$scope.article.commentCountActive+=1;
			});			
		}).error(function(data,status,header,config){
			$scope.showAlert('Lỗi','Có lỗi ' + status + ' xảy ra khi gửi bình luận');
		});
	};
	
	$scope.showAlert = function(title,message) {
	    // Appending dialog to document.body to cover sidenav in docs app
	    // Modal dialogs should fully cover application
	    // to prevent interaction outside of dialog
	    $mdDialog.show(
	      $mdDialog.alert()
	        .parent(angular.element(document.querySelector('#popupContainer')))
	        .clickOutsideToClose(true)
	        .title(title)
	        .textContent(message)
	        .ariaLabel('Alert Dialog Demo')
	        .ok('Đồng ý')	        
	    );
	  };
   $scope.gotoComments = function() {
      // set the location.hash to the id of
      // the element you wish to scroll to.
      $location.hash('comments');
      // call $anchorScroll()
      $anchorScroll();
    };
    
   $scope.replyClick = function(commentID){
	   $scope.replycommentcontent="";
	   var replyform = angular.element(document.querySelector("#replycommentform"));
	   if(!isEmpty(replyform)){
		   replyform.remove();
	   }
	   var string="<form class='clearfix' ng-submit='postreplycomment(" + commentID + ")' id='replycommentform'>" +
			   		"<div class='col_full'>" +
			   			"<textarea name='replycomment' cols='58' rows='3'  required='true' ng-model='replycommentcontent' class='sm-form-control'></textarea>" +
			   		"</div>" +
			   		"<div class='col_full nobottommargin'>" +
			   			"<button name='replycomment' type='submit' id='replycomment-submit-button'  value='Submit' class='button button-3d nomargin'>Gửi</button>" +
					"</div>" +
				  "</form>";
	  
	   replyform = angular.element(document.querySelector("#reply" + commentID));
	   //alert(replyform);
	   replyform.append($compile(string)($scope));
   };
   
   $scope.postreplycomment = function(commentID){
	   $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
		var data ="name=" + $scope.name + "&email=" + $scope.email +"&body=" + $scope.replycommentcontent + "&articleid=" + $scope.article.id + "&createdby=" + $scope.currentuser.id + "&commentId=" + commentID;
		$http.post("/blog/postreplycomment",data).success(function(data,status,header,config){
			$scope.showAlert('Thành công','Bạn đã gửi bình luận thành công');
			var replyform = angular.element(document.querySelector("#replycommentform"));
			   if(!isEmpty(replyform)){
				   replyform.remove();
			   }
			var data2 = "id=" + id;
			$http.post("/blog/commentoarticle",data2).success(function(data,status,header,config){
				$scope.lstcomment = data;
				//$scope.name="";
				//$scope.email="";
				$scope.commentcontent="";
				$scope.article.commentcount+=1;
				$scope.article.commentCountActive+=1;
			});			
		}).error(function(data,status,header,config){
			$scope.showAlert('Lỗi','Có lỗi ' + status + ' xảy ra khi trả lời bình luận');
		});
	   
	   
   };
   
});
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

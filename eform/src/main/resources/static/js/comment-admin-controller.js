/**
 * 
 */

app.controller("commentAdminControler",function($scope,$sce,listArticleService,PagerService,$window,$http,$mdDialog){
	

	
	
	$scope.listallcomment = function(){
		var path = window.location.href;
		var id = path.substring(path.lastIndexOf("/")+1, path.length);
		$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
		var data = "id=" + id;
		$http.post("/blog/articlebyid",data).success(function(data,status,header,config){
			$scope.article = data;
			$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
			var data2 ="id=" + $scope.article.id 
			$http.post('/blog/getallcommentofarticle',data2).success(function(data,status,header,config){
				$scope.lstcomment= data;
				
			}).error(function(data,status,header,config){
				
			});
		}).error(function(data,status,header,config){
			$window.location.replace('/blog/admin/admin_list_article');
		});
		
	};
	
	$scope.listallcomment();

	
	$scope.delItem = function delItem(comment){
		$http.defaults.headers.post["Content-Type"] = "application/json";
		$http.post("/blog/admin/deletecomment",comment).success(function(data, status, headers, config){
			$scope.showAlert('Thành công','Đã xóa thành công Bình luận');
			$scope.listallcomment();

		}).error(function(data, status, headers, config){
			$scope.showAlert('Lỗi','Có lỗi xảy ra khi xóa Bình luận ' + status);
		});
		
	}
	$scope.publishArticle = function publishArticle(article){
		$http.defaults.headers.post["Content-Type"] = "application/json";
		$http.post("/blog/admin/publishcomment",article).success(function(data, status, headers, config){
			$scope.showAlert('Thành công','Xuất bản thành công Bình luận');
			$scope.listallcomment();

		}).error(function(data, status, headers, config){
			$scope.showAlert('Lỗi','Có lỗi xảy ra khi Xuất bản Bình luận ' + status);
		});
	}
	
	$scope.unPublishArticle = function unPublishArticle(article){
		$http.defaults.headers.post["Content-Type"] = "application/json";
		$http.post("/blog/admin/unpublishcomment",article).success(function(data, status, headers, config){
			 $scope.showAlert('Thành công','Dừng xuất bản thành công Bình luận');
			 $scope.listallcomment();

		}).error(function(data, status, headers, config){
			$scope.showAlert('Lỗi','Có lỗi xảy ra khi Dừng xuất bản Bình luận ' + status);
		});
	}
	
	
	$scope.showAlert = function(title,message) {
	    // Appending dialog to document.body to cover sidenav in docs app
	    // Modal dialogs should fully cover application
	    // to prevent interaction outside of dialog
	    $mdDialog.show(
	      $mdDialog.alert()
	        .parent(angular.element(document.querySelector('#popupContainer')))
	        .clickOutsideToClose(false)
	        .title(title)
	        .textContent(message)
	        .ariaLabel(title)
	        .ok('Đồng ý')
	        
	    );
	  };
	
	$scope.showDeleteConfirm = function(ev,article) {
		  // Appending dialog to document.body to cover sidenav in docs app
		  var confirm = $mdDialog.confirm()
		    .title('Bạn thực sự muốn Bình luận viết này?')
		    .textContent('Nếu bạn chọn đồng ý Bình luận này sẽ bị xóa vĩnh viễn khỏi hệ thống')
		    .ariaLabel('Xóa bình luận')
		    .targetEvent(ev)
		    .ok('Đồng ý')
		    .cancel('Bỏ qua');

		  $mdDialog.show(confirm).then(function() {
			  $scope.delItem(article);
		  }, function() {
			  
		  });
		};
	
	$scope.showUnpublishConfirm = function(ev,article) {
		  // Appending dialog to document.body to cover sidenav in docs app
		  var confirm = $mdDialog.confirm()
		    .title('Bạn thực sự muốn dừng xuất bản Bình luận này?')
		    .textContent('Nếu bạn chọn đồng ý Bình luận này sẽ chuyển sang trạng thái Không xuất bản')
		    .ariaLabel('Dừng xuất bản')
		    .targetEvent(ev)
		    .ok('Đồng ý')
		    .cancel('Bỏ qua');

		  $mdDialog.show(confirm).then(function() {
			  $scope.unPublishArticle(article);
			 
		  }, function() {
			  
		  });
		};
	 
	 $scope.showPublishConfirm = function(ev,article) {
		  // Appending dialog to document.body to cover sidenav in docs app
		  var confirm = $mdDialog.confirm()
		    .title('Bạn thực sự muốn Xuất bản Bình luận này?')
		    .textContent('Nếu bạn chọn đồng ý Bình luận này sẽ chuyển sang trạng thái Xuất bản')
		    .ariaLabel('Xuất bản')
		    .targetEvent(ev)
		    .ok('Đồng ý')
		    .cancel('Bỏ qua');

		  $mdDialog.show(confirm).then(function() {
			  $scope.publishArticle(article);
			 
		  }, function() {
			  
		  });
		}; 
		
		 $scope.comeback = function comeback(){
		    	$window.history.back();
		    }
});
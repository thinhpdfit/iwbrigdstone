/**
 * 
 */
//var  app = angular.module('newArticleApp',['ngRoute','ngResource','ngMaterial']);

app.controller("articleData",function($scope,$sce,$window,$http,$timeout, $q, $log, $mdDialog,$rootScope ){
	$scope.headingTitle ="Blog";
	$scope.showSearch =false;
	$scope.content="";
	$scope.title="";
	$http.defaults.headers.post["Content-Type"] = "application/json";
	
	$scope.showAlert = function(title,message) {
	    // Appending dialog to document.body to cover sidenav in docs app
	    // Modal dialogs should fully cover application
	    // to prevent interaction outside of dialog
	    $mdDialog.show(
	      $mdDialog.alert()
	        .parent(angular.element(document.querySelector('#popupContainer')))
	        .clickOutsideToClose(false)
	        .title(title)
	        .textContent(message)
	        .ariaLabel(title)
	        .ok('Đồng ý')
	        
	    );
	  };
	
	$scope.addNewAndPublishArticle = function addNewAndPublishArticle(){
		var currentDate = new Date();
		var data ={
			id:"",
			title:$scope.title,
			content:$scope.content,
			createdDate: currentDate,
			modifiedDate:null,
			createdBy:null,
			status:"true",
			publishDate:currentDate
		}
		$http.defaults.headers.post["Content-Type"] = "application/json";
		$http.post("/blog/admin/createArticle",data).success(function(data, status, headers, config){
			 //alert("Thêm mới thành công bài viết ");
			 $scope.showAlert("Thành công","Thêm mới thành công bài viết");
			 $window.location.replace('/blog/admin/admin_list_article.html');

		}).error(function(data, status, headers, config){
			//alert("Lỗi " + status );
			$scope.showAlert("Lỗi","Có lỗi " + status + " khi thêm mới bài viết");
		});
	}
	
	$scope.addNewAndSaveArticle = function addNewAndSaveArticle(){
		var currentDate = new Date()
		var data ={
			id:"",
			title:$scope.title,
			content:$scope.content,
			createdDate:currentDate,
			modifiedDate:null,
			createdBy:null,
			status:"false",
			publishDate:null
		}
		$http.defaults.headers.post["Content-Type"] = "application/json";
		$http.post("/blog/admin/createArticle",data).success(function(data, status, headers, config){
			 $scope.showAlert("Thành công","Thêm mới thành công bài viết");
			 $window.location.replace('/blog/admin/admin_list_article.html');

		}).error(function(data, status, headers, config){
			$scope.showAlert("Lỗi","Có lỗi " + status + " khi thêm mới bài viết");
		});
	}
	
	
	var self = this;

	$scope.simulateQuery = false;
	$scope.isDisabled    = false;

    // list of `state` value/display objects
	
	$scope.querySearch   = querySearch;
	$scope.results        = $scope.querySearch("");
	$scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;
    
    $scope.newState = newState;

    function newState(state) {
      alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
    	var data ="name=" + query;
    	
    	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;";
    	$http.post("/blog/admin/listprocess",data).success(function(data, status, headers, config){
			if(!isEmpty(data)){			
				$scope.lstprocess = data;
				//console.log(lstprocess);
				$scope.results =$scope.lstprocess.map( function (item) {
			        return {
			          value: item,
			          display: item.name
			        };
			      });
				//alert(valuereturn);
				console.log($scope.results);
			}
			else
				$scope.results="";
		}).error(function(data, status, headers, config){
			alert("Lỗi " + status );
		});
    	if(!isEmpty($scope.results)){
	    	
		     return $scope.results;
    	}
    	
    	return "";
    }

    function searchTextChange(text) {
      $log.info('Text changed to ' + text);
    }

    
    function selectedItemChange(item) {
      $log.info('Item changed to ' + JSON.stringify(item));
      //console.log(item.value)
      if(!isEmpty(item)){
    	  var strcontent = "<p>Mô tả quy trình: " + item.value.description + "</p>" + item.value.donviapdung + "<p>Biểu đồ:</p>" + item.value.iframe;
    	  $scope.content += $sce.trustAsHtml(strcontent);
    	 /* $scope.content = strcontent;*/
          $scope.title = item.display;
          
      }
      else{
    	  $scope.content = "";
          $scope.title = "";
      }
      
    }

   
    
    
   
   
      $scope.showPreviewDialog = function(ev) {
    	   
    	   $scope.mdcontent = $sce.trustAsHtml($scope.content);
    	    $mdDialog.show({
    	      controller: DialogController,
    	      contentElement: '#previewDialog',
    	      parent: angular.element(document.body),
    	      targetEvent: ev,
    	      clickOutsideToClose: true
    	    });
    	  };
      
      function DialogController($scope, $mdDialog) {
    	
    	    $scope.hide = function() {
    	      $mdDialog.hide();
    	    };

    	    $scope.cancel = function() {
    	      $mdDialog.cancel();
    	    };

    	    $scope.answer = function(answer) {
    	      $mdDialog.hide(answer);
    	    };
    	}
    function isEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    };
    
    $scope.comeback = function comeback(){
    	$window.history.back();
    }
	
});

app.directive('ckEditor', function () {
	  return {
	    require: '?ngModel',
	    link: function (scope, elm, attr, ngModel) {
	      var ck = CKEDITOR.replace(elm[0]);
	      if (!ngModel) return;
	      ck.on('instanceReady', function () {
	        ck.setData(ngModel.$viewValue);
	      });
	      function updateModel() {
	    	  ngModel.$setViewValue(ck.getData());
	        /*scope.$apply(function () {
	          ngModel.$setViewValue(ck.getData());
	            
	        });*/
	      }
	      ck.on('change', updateModel);
	      ck.on('key', updateModel);
	      ck.on('dataReady', updateModel);

	      ngModel.$render = function (value) {
	        ck.setData(ngModel.$viewValue);
	      };
	    }
	  };
});




	    



	/**
	Copyright 2016 Google Inc. All Rights Reserved. 
	Use of this source code is governed by an MIT-style license that can be foundin the LICENSE file at http://material.angularjs.org/HEAD/license.
	**/
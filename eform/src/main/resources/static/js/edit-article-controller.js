/**
 * 
 */

app.controller("editController",function($scope,$sce,listArticleService,$window,$http,$mdDialog,$timeout, $q, $log){
		var path = window.location.href;
		var id = path.substring(path.lastIndexOf("/")+1, path.length);
		$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
		var data = "id=" + id;
		$http.post("/blog/articlebyid",data).success(function(data,status,header,config){
			$scope.article = data;
			$scope.title = $scope.article.title;
			$scope.content = $scope.article.content;
			
			}).error(function(data,status,header,config){
				$window.location.replace('/blog/admin/admin_list_article');
			});
			

		
		
		$scope.saveArticle = function saveArticle(){				
			$scope.article.title = $scope.title;
			$scope.article.content = $scope.content;
			$http.defaults.headers.post["Content-Type"] = "application/json";
			$http.post("/blog/admin/updatearticle",$scope.article).success(function(data, status, headers, config){
				 $scope.showAlert("Thành công","Bạn đã lưu thành công bài viết");
				 $window.location.replace('/blog/admin/admin_list_article.html');
				 listArticleService.set(null);

			}).error(function(data, status, headers, config){
				$scope.showAlert("Lỗi","Có lỗi " + status +" xảy ra khi lưu bài viết")
			});
		}
		
		$scope.simulateQuery = false;
		$scope.isDisabled    = false;

	    // list of `state` value/display objects
		//$scope.results        = loadAll('');
		$scope.querySearch   = querySearch;
		$scope.selectedItemChange = selectedItemChange;
	    $scope.searchTextChange   = searchTextChange;
	    
	    $scope.newState = newState;

	    function newState(state) {
	      alert("Sorry! You'll need to create a Constitution for " + state + " first!");
	    }

	    // ******************************
	    // Internal methods
	    // ******************************

	    /**
	     * Search for states... use $timeout to simulate
	     * remote dataservice call.
	     */
	    function querySearch (query) {
	    	var data ="name=" + query;
	    	
	    	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;";
	    	$http.post("/blog/admin/listprocess",data).success(function(data, status, headers, config){
				if(!isEmpty(data)){			
					$scope.lstprocess = data;
					//console.log(lstprocess);
					$scope.results =$scope.lstprocess.map( function (item) {
				        return {
				          value: item,
				          display: item.name
				        };
				      });
					//alert(valuereturn);
					console.log($scope.results);
				}
				else
					$scope.results="";
			}).error(function(data, status, headers, config){
				alert("Lỗi " + status );
			});
	    	if(!isEmpty($scope.results)){
		    	
			     return $scope.results;
	    	}
	    	
	    	return "";
	    }

	    function searchTextChange(text) {
	      $log.info('Text changed to ' + text);
	    }

	    
	    function selectedItemChange(item) {
	      $log.info('Item changed to ' + JSON.stringify(item));
	      //console.log(item.value)
	      if(!isEmpty(item)){
	    	  var strcontent = "<p>Mô tả quy trình: " + item.value.description + "</p>" + item.value.donviapdung + "<p>Biểu đồ:</p>" + item.value.iframe; 
	    	  $scope.content += strcontent;
	          $scope.title = item.display;
	          //$scope.$apply();  
	      }
	      else{
	    	  $scope.content = "";
	          $scope.title = "";
	      }
	      
	    }
		
	    $scope.showPreviewDialog = function(ev,content,title) {
			   $scope.mdtitle = title;
	    	   $scope.mdcontent = $sce.trustAsHtml(content);
	    	    $mdDialog.show({
	    	      controller: DialogController,
	    	      contentElement: '#previewDialog',
	    	      parent: angular.element(document.body),
	    	      targetEvent: ev,
	    	      clickOutsideToClose: true
	    	    });
	    	  };
	      
	      function DialogController($scope, $mdDialog) {
	    	
	    	    $scope.hide = function() {
	    	      $mdDialog.hide();
	    	    };

	    	    $scope.cancel = function() {
	    	      $mdDialog.cancel();
	    	    };

	    	    $scope.answer = function(answer) {
	    	      $mdDialog.hide(answer);
	    	    };
	    	}
	    $scope.comeback = function comeback(){
	    	$window.location.replace('/blog/admin/admin_list_article.html');
	    }
	    
	    $scope.showAlert = function(title,message) {
		    // Appending dialog to document.body to cover sidenav in docs app
		    // Modal dialogs should fully cover application
		    // to prevent interaction outside of dialog
		    $mdDialog.show(
		      $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#popupContainer')))
		        .clickOutsideToClose(false)
		        .title(title)
		        .textContent(message)
		        .ariaLabel(title)
		        .ok('Đồng ý')
		        
		    );
		  };
	});
/**
 * 
 */
var  app = angular.module('listArticleApp',['ngRoute','ngResource','ngMaterial','ui.router']);

/*app.config(function($routeProvider,$locationProvider) {
	$routeProvider
	.when("/commentadmin",{
		templateUrl: '/blog/admin/admin_list-comment.html',
		controller: 'commentAdminControler'
	})
	.when("/editarticle",{
		templateUrl: '/blog/admin/edit_article.html',
		controller: 'editController'
	})
	.otherwise({
		templateUrl: '/blog/admin/table_article.html',
		controller: 'listArticleController'    	
     });
	
});*/

/*app.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('/', {
            url:'/',
            templateUrl: '/blog/admin/table_article',
            controller: 'listArticleController'
        }).state('editarticle', {
            url:'edit_article/:hotelId',
            templateUrl: '/blog/admin/edit_article',
            controller: 'editController'
        });
});*/

app.run(function ($rootScope,$http) {
	 var map = {};
	 $http.get("/blog/checkpermistion").success(function(data, status, headers, config){
		 map = data;		
		 $rootScope.mappermission = map;
		 //console.log(map);
		 
	 });
   
});

app.factory('listArticleService', function() {
	 var savedData = {}
	 function set(data) {
	   savedData = data;
	 }
	 function get() {
	  return savedData;
	 }

	 return {
	  set: set,
	  get: get
	 }

	});

app.factory('PagerService', function() {
     // service definition
     var service = {};

     service.GetPager = GetPager;

     return service;

     // service implementation
     function GetPager(totalItems, currentPage, pageSize) {
         // default to first page
         currentPage = currentPage || 1;
         //alert(totalItems);
         // default page size is 10
         pageSize = pageSize || 10;

         // calculate total pages
         var totalPages = Math.ceil(totalItems / pageSize);

         var startPage, endPage;
         if (totalPages <= 10) {
             // less than 10 total pages so show all
             startPage = 1;
             endPage = totalPages;
         } else {
             // more than 10 total pages so calculate start and end pages
             if (currentPage <= 6) {
                 startPage = 1;
                 endPage = 10;
             } else if (currentPage + 4 >= totalPages) {
                 startPage = totalPages - 9;
                 endPage = totalPages;
             } else {
                 startPage = currentPage - 5;
                 endPage = currentPage + 4;
             }
         }

         // calculate start and end item indexes
         var startIndex = (currentPage - 1) * pageSize;
         var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

         // create an array of pages to ng-repeat in the pager control
         var pages = _.range(startPage, endPage + 1);

         // return object with all pager properties required by the view
         return {
             totalItems: totalItems,
             currentPage: currentPage,
             pageSize: pageSize,
             totalPages: totalPages,
             startPage: startPage,
             endPage: endPage,
             startIndex: startIndex,
             endIndex: endIndex,
             pages: pages
         };
     }
 });



app.directive('ckEditor', function () {
	  return {
	    require: '?ngModel',
	    link: function (scope, elm, attr, ngModel) {
	      var ck = CKEDITOR.replace(elm[0]);
	      if (!ngModel) return;
	      ck.on('instanceReady', function () {
	        ck.setData(ngModel.$viewValue);
	      });
	      function updateModel() {
	    	  ngModel.$setViewValue(ck.getData());
	       /* scope.$apply(function () {
	          ngModel.$setViewValue(ck.getData());
	        });*/
	      }
	      ck.on('change', updateModel);
	      ck.on('key', updateModel);
	      ck.on('dataReady', updateModel);

	      ngModel.$render = function (value) {
	        ck.setData(ngModel.$viewValue);
	      };
	    }
	  };
});

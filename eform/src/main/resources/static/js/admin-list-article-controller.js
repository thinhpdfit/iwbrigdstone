/**
 * 
 */

app.controller("listArticleController",function($scope,$sce,listArticleService, $rootScope,PagerService,$window,$http,$mdDialog){
		
		$scope.headingTitle ="Blog";
		$scope.showSearch =false;
		
		
		
		/*function findAll() {
	        //get all tasks and display initially
	        $http.get('/blog/listallarticle').
	            success(function (data) {
	                if (data != undefined) {
	                    $scope.lstArticle = data;
	                } else {
	                    $scope.lstArticle = [];
	                }
	            });
	    }
		
		findAll();*/
		
		$scope.listallarticlepaging = function listallarticlepaging(start,numberofitem){
			$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
			var data ="startIndex=" + start + "&numberofitem=" + numberofitem;
			$http.post('/blog/listallarticlepaging',data).success(function(data,status,header,config){
				$scope.lstArticle = data;
				/*for(var item in $scope.lstArticle){
					$scope.lstArticle[item].subcontent = $sce.trustAsHtml($scope.lstArticle[item].subcontent)
					//console.log($scope.lstArticle[item].subcontent);
				}*/
			}).error(function(data,status,header,config){
				
			});
		};
		
		
		
		
		/**/		
		$scope.pagesize = 10;
		$scope.setPage = setPage;
		$scope.pager = {};
	    initController();

	    function initController() {
	        // initialize to page 1
	    	 $http.get("/blog/checkpermistion").success(function(data, status, headers, config){
				 $scope.mappermission = data;					 			
			 });
	    	$http.get('/blog/countAllArticle').success(function(data,status,header,config){
				$scope.total = data;
				//alert($scope.total)
				$scope.setPage(1);
			}).error(function(data,status,header,config){
				
			});
	    	$scope.mappermission = $rootScope.mappermission;
			//console.log($scope.mappermistion);
	        //$scope.listallarticlepaging(0,$scope.pagesize);
	    }

	    function setPage(page) {
	        if (page < 1 || page >  $scope.pager.totalPages) {
	            return;
	        }

	        // get pager object from service
	        $scope.pager = PagerService.GetPager($scope.total, page,$scope.pagesize);

	        // get current page of items
	        /*vm.items = vm.dummyItems.slice(vm.pager.startIndex, vm.pager.endIndex + 1);*/
	        $scope.listallarticlepaging($scope.pager.startIndex,$scope.pagesize);
	        $('html, body').animate({scrollTop: '0px'}, 300);
		
	    }
		
		$scope.delItem = function delItem(article){
			$http.defaults.headers.post["Content-Type"] = "application/json";
			$http.post("/blog/admin/deletearticle",article).success(function(data, status, headers, config){
				$scope.showAlert('Thành công','Đã xóa thành công bài viết');
				 $scope.listallarticlepaging($scope.pager.startIndex,$scope.pagesize);
	
			}).error(function(data, status, headers, config){
				$scope.showAlert('Lỗi','Có lỗi xảy ra khi xóa bài viết ' + status);
			});
			
		}
		$scope.publishArticle = function publishArticle(article){
			$http.defaults.headers.post["Content-Type"] = "application/json";
			$http.post("/blog/admin/publisharticle",article).success(function(data, status, headers, config){
				$scope.showAlert('Thành công','Xuất bản thành công bài viết');
				 $scope.listallarticlepaging($scope.pager.startIndex,$scope.pagesize);
	
			}).error(function(data, status, headers, config){
				$scope.showAlert('Lỗi','Có lỗi xảy ra khi Xuất bản bài viết ' + status);
			});
		}
		
		$scope.unPublishArticle = function unPublishArticle(article){
			$http.defaults.headers.post["Content-Type"] = "application/json";
			$http.post("/blog/admin/unpublisharticle",article).success(function(data, status, headers, config){
				 $scope.showAlert('Thành công','Dừng xuất bản thành công bài viết');
				 $scope.listallarticlepaging($scope.pager.startIndex,$scope.pagesize);
	
			}).error(function(data, status, headers, config){
				$scope.showAlert('Lỗi','Có lỗi xảy ra khi Dừng xuất bản bài viết ' + status);
			});
		}
		
		$scope.showAlert = function(title,message) {
		    // Appending dialog to document.body to cover sidenav in docs app
		    // Modal dialogs should fully cover application
		    // to prevent interaction outside of dialog
		    $mdDialog.show(
		      $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#popupContainer')))
		        .clickOutsideToClose(false)
		        .title(title)
		        .textContent(message)
		        .ariaLabel(title)
		        .ok('Đồng ý')
		        
		    );
		  };
		
		$scope.showDeleteConfirm = function(ev,article) {
			  // Appending dialog to document.body to cover sidenav in docs app
			  var confirm = $mdDialog.confirm()
			    .title('Bạn thực sự muốn xóa bài viết này?')
			    .textContent('Nếu bạn chọn đồng ý bài viết này sẽ bị xóa vĩnh viễn khỏi hệ thống')
			    .ariaLabel('Lucky day')
			    .targetEvent(ev)
			    .ok('Đồng ý')
			    .cancel('Bỏ qua');
	
			  $mdDialog.show(confirm).then(function() {
				  $scope.delItem(article);
			  }, function() {
				  
			  });
			};
			
		 $scope.showUnpublishConfirm = function(ev,article) {
			  // Appending dialog to document.body to cover sidenav in docs app
			  var confirm = $mdDialog.confirm()
			    .title('Bạn thực sự muốn dừng xuất bản bài viết này?')
			    .textContent('Nếu bạn chọn đồng ý bài viết này sẽ bị chuyển sang trạng thái Không xuất bản')
			    .ariaLabel('Lucky day')
			    .targetEvent(ev)
			    .ok('Đồng ý')
			    .cancel('Bỏ qua');
	
			  $mdDialog.show(confirm).then(function() {
				  $scope.unPublishArticle(article);
				 
			  }, function() {
				  
			  });
			};
		 
		 $scope.showPublishConfirm = function(ev,article) {
			  // Appending dialog to document.body to cover sidenav in docs app
			  var confirm = $mdDialog.confirm()
			    .title('Bạn thực sự muốn Xuất bản bài viết này?')
			    .textContent('Nếu bạn chọn đồng ý bài viết này sẽ bị chuyển sang trạng thái Xuất bản')
			    .ariaLabel('Lucky day')
			    .targetEvent(ev)
			    .ok('Đồng ý')
			    .cancel('Bỏ qua');
	
			  $mdDialog.show(confirm).then(function() {
				  $scope.publishArticle(article);
				 
			  }, function() {
				  
			  });
			}; 
			
		$scope.editItemAction = function(article){
			listArticleService.set(article);
			$window.location.replace('/blog/admin/edit_article/' + article.id);
		};
		
		
		$scope.commentAdmin = function(article){
			listArticleService.set(article);
			$window.location.replace('/blog/admin/admin_list-comment');
		}
		
		$scope.showPreviewDialog = function(ev,content,title) {
			   $scope.mdtitle = title;
	    	   $scope.mdcontent = $sce.trustAsHtml(content);
	    	    $mdDialog.show({
	    	      controller: DialogController,
	    	      contentElement: '#previewDialog',
	    	      parent: angular.element(document.body),
	    	      targetEvent: ev,
	    	      clickOutsideToClose: true
	    	    });
	    	  };
	      
	      function DialogController($scope, $mdDialog) {
	    	
	    	    $scope.hide = function() {
	    	      $mdDialog.hide();
	    	    };

	    	    $scope.cancel = function() {
	    	      $mdDialog.cancel();
	    	    };

	    	    $scope.answer = function(answer) {
	    	      $mdDialog.hide(answer);
	    	    };
	    	}
		
		
	});
	
	function isEmpty(obj) {
	    for(var key in obj) {
	        if(obj.hasOwnProperty(key))
	            return false;
	    }
	    return true;
	}
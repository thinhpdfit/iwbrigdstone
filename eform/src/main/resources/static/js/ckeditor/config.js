/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.filebrowserBrowseUrl = '/js/ckeditor/TVS-Manager/index.html';
    config.filebrowserImageBrowseUrl = '/js/ckeditor/TVS-Manager/index.html';
    config.filebrowserFlashBrowseUrl = '/js/ckeditor/TVS-Manager/index.html';
    config.allowedContent = true;
    config.extraPlugins = 'audio,video,youtube,lineheight';
   
    config.height= 600;
//    config.extraAllowedContent='video[*]{*};source[*]{*}';
};

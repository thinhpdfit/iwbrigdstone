/**
 * 
 */
function onSignIn(googleUser) {
	var profile = googleUser.getBasicProfile();
	var email=profile.getEmail();
	  var id_token = googleUser.getAuthResponse().id_token;
	  if(document.getElementById("loginButton")!=null){
		  if(document.getElementById("username")!=null){
			  document.getElementById("username").value= email;
		  }
		  if(document.getElementById("password")!=null){
		  document.getElementById("password").value= id_token;
		  }
		  if(document.getElementById("authProvider")!=null){
		  document.getElementById("authProvider").value= "google";
		  }
		  document.getElementById("loginButton").click();
	  }
}
function signOutGoogle() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function() {
		signOutLocal();
	});
}
function signOutLocal() {
	document.getElementById("logoutButton").click();
}
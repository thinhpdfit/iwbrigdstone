
var rollingToTop = function() {
	alert("d");
	$('html, body').animate({scrollTop:0}, 200);
};

function onLoad() {
    gapi.load('auth2', function() {
      gapi.auth2.init();
    });
}

function setTimeoutNotification(id) {
	playSoundNotif();
	var idStr = '#'+id;
	setTimeout(function() {
		$(idStr).fadeOut();
	}, 5000);
}

function playSound(filename){
    document.getElementById("sound").innerHTML='<audio autoplay="autoplay"><source src="' + filename + '.mp3" type="audio/mpeg" /><embed hidden="true" autostart="true" loop="false" src="' + filename +'.mp3" /></audio>';
}

function playSoundNotif(){
	playSound("/comment-task");
}

function highligh(id){
	var idTmp = "#"+id;
	$(idTmp).addClass('highlighted');
	setTimeout(function(){
		$(idTmp).removeClass('highlighted');
	}, 4000);
		
}

function maskAsClicked(id){
	var idTmp = "#"+id;
	$(idTmp).addClass("notif-list-item-clicked");
}

$(document).ready(function() {

	
	$.fn.extend({
	    animateCss: function (animationName) {
	        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	        this.addClass('animated ' + animationName).one(animationEnd, function() {
	            $(this).removeClass('animated ' + animationName);
	        });
	    }
	});
	
	$("head").append("<meta name='viewport' content='width=device-width, initial-scale=1.0'/>");
	$('head').append('<meta name="google-signin-client_id" content="551102288950-82shbdiem8atb3siumnfahu1cs7iv5d8.apps.googleusercontent.com">');

	$(document).on("click", ".user-wrap", function(){
		$(".u-option-wrap").toggle();
	});
	
	$(document).on("click", ".notif-btn", function(){
		$(".notif-container-wrap").toggle();
	});
	
	$(document).mouseup(function (e){
		console.log(e.target);
		var userWrap = $(".user-wrap");
	    if (!userWrap.is(e.target) && userWrap.has(e.target).length === 0){
	    	$(".u-option-wrap").hide();
	    }
	    
	    var notifwrap = $(".notif-wrap");
	    if (!notifwrap.is(e.target) && notifwrap.has(e.target).length === 0){
	    	$(".notif-container-wrap").hide();
	    }
	});
	
	$(document).on("click", ".btn-toggle-menu", function(){
		$(".container").toggleClass("container-menu-active");
		$(".main-menu-bar").toggleClass("main-menu-bar-small");
		$(".main-menu-bar-content").toggleClass("main-menu-bar-content-small");
		$(".logo-wrap").toggleClass("logo-wrap-hiden");
		$(".header-left").toggleClass("header-left-small");

		$(".dashboard-container .tk-item").animateCss('bounce');
	});
	
	$(document).on("click", ".n-list-item-btn", function(){
		$(this).parent().addClass("notif-list-item-clicked");
	});
	
	
	
	
	$(document).on("click", ".btn-show-search", function(){
		$(".v-slot-search-form, .search-form").stop().slideToggle(100);
		$(".btn-show-search").toggleClass("btn-show-search-active");
	});
	
	$(document).on("click", ".btn-show-setting-form", function(){
		$(".form-ttt-setting").stop().slideToggle(100);
		$(".btn-show-setting-form").toggleClass("btn-show-ttt-form-active");
		$(".btn-show-setting-form").parent(".form-row").toggleClass("form-row-closed");
	});
	
	$(document).on("click", ".btn-show-general-form", function(){
		$(".form-ttt-general").stop().slideToggle(100);
		$(".btn-show-general-form").toggleClass("btn-show-ttt-form-active");
		$(".btn-show-general-form").parent(".form-row").toggleClass("form-row-closed");
	});
	
	$(document).on("click", ".btn-show-detail", function(){
		$(this).parent(".form-row").find(".detail-row-wrap").stop().slideToggle(100);;
		$(this).toggleClass("btn-show-detail-active");
		$(this).parent(".form-row").toggleClass("form-row-closed");
	});
	
	$(document).on("click", ".pro-def-wrap", function(){
		$(this).parent().find(".pro-task-wrap").stop().slideToggle(200);
		$(this).toggleClass("pro-def-wrap-active");
	});
	
	$(document).on("click", ".task-tl-i-detail-btn-loaded", function(){
		$(this).parent().find(".task-tl-i-detail-content").toggleClass("task-tl-i-detail-content-hidden");
		$(this).find(".v-icon.FontAwesome").html("&#xf067;");
		$(this).find(".v-icon.FontAwesome.active").html("&#xf068;");
		$(this).find(".v-icon.FontAwesome").toggleClass("active");
	});
	
	$(document).on("click", ".td-nav-cmt", function(){
		var $container = $(".v-scrollable");
		var $scrollTo = $('.td-comment-wrap');
		var $userWrap = $(".td-user-wrap");
		var $form = $(".td-form-wrap");
		$container.animate({scrollTop: $userWrap.height()+$form.height()+272, scrollLeft: 0},300);
		$(".td-nav-btn-active").removeClass("td-nav-btn-active");
		$(this).addClass("td-nav-btn-active");
	});
	$(document).on("click", ".td-nav-att", function(){
		var $container = $(".v-scrollable");
		var $userWrap = $(".td-user-wrap");
		var $form = $(".td-form-wrap");
		var $comment = $(".td-comment-wrap");
		var $scrollTo = $('.td-att-wrap');
		$container.animate({scrollTop: $userWrap.height()+$form.height()+$comment.height()+272, scrollLeft: 0},300);
		$(".td-nav-btn-active").removeClass("td-nav-btn-active");
		$(this).addClass("td-nav-btn-active");	
	});
	$(document).on("click", ".td-nav-open-btn", function(){
		$(".td-nav-wrap").toggleClass("td-nav-wrap-active");
		$(this).toggleClass("td-nav-open-btn-active");
	});
	
	$(document).on("click", ".td-user-rl-see-more", function(){
		$(".td-user-rl-wrap").slideToggle(100);
		$(this).toggleClass("td-user-rl-see-more-open");
	});
	
	$(document).on("focusin", ".header-search-box-txt", function(){
		$(".search-bg-blur").addClass("search-bg-blur-active");
	});
	$(document).on("focusout", ".header-search-box-txt", function(){
		$(".search-bg-blur").removeClass("search-bg-blur-active");
	});
	
	$(document).on("click", ".collapse-panel-item-head", function(){
		$(this).parent().find(".collapse-panel-item-content").slideToggle(200);
		$(this).find(".collapse-panel-item-btn").toggleClass("collapse-panel-item-btn-rotate");
	});
	$(document).on("click", ".chat-box-collapse", function(){
		$(this).parent().find(".chat-box").slideToggle();
		$(this).parent().toggleClass("chat-box-wrap-hide");
	});
	
	$(document).on("click", "#btnLogoutGoogle", function(){
		var auth2 = gapi.auth2.getAuthInstance();
		auth2.signOut().then(function() {
			window.location = "/logout";
		});
	});

});

package ckeditor.com.upload;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import ckeditor.com.nartex.FileManager;
import ckeditor.org.json.JSONObject;

public class FileBrower extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

    //Initialize global variables
    public void init() throws ServletException {
    }

    //Process the HTTP Get request
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        doPost(request, response);
    }

    /**
     *	Check if user is authorized
     *
     *	@return boolean true is access granted, false if no access
     */

    public boolean auth() {
        // You can insert your own code over here to check if the user is authorized.
        return true;
    }

    //Process the HTTP Post request
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
    	System.out.println("responseData: 0");
        FileManager fm = new FileManager(getServletContext(), request);

        JSONObject responseData = null;

        String mode = "";
        boolean putTextarea = false;
        if (!auth()) {
            fm.error(fm.lang("AUTHORIZATION_REQUIRED"));
            System.out.println("fm: 0");
        } else {
            if (request.getMethod().equals("GET")) {
                if (request.getParameter("mode") != null &&
                    request.getParameter("mode") != "") {
                    mode = request.getParameter("mode");
                    System.out.println("mode:"+mode);
                    if (mode.equals("getinfo")) {
                        if (fm.setGetVar("path", request.getParameter("path"))) {
                            responseData = fm.getInfo();
                        }
                    } else if (mode.equals("getfolder")) {
                        if (fm.setGetVar("path", request.getParameter("path"))) {
                            responseData = fm.getFolder();
                        }
                    } else if (mode.equals("rename")) {
                        if (fm.setGetVar("old", request.getParameter("old")) &&
                            fm.setGetVar("new", request.getParameter("new"))) {
                            responseData = fm.rename();
                        }
                    } else if (mode.equals("delete")) {
                        if (fm.setGetVar("path", request.getParameter("path"))) {
                            responseData = fm.delete();
                        }
                    } else if (mode.equals("addfolder")) {
                        if (fm.setGetVar("path", request.getParameter("path")) &&
                            fm.setGetVar("name", request.getParameter("name"))) {
                            responseData = fm.addFolder();
                        }
                    } else if (mode.equals("download")) {
                        if (fm.setGetVar("path", request.getParameter("path"))) {
                            fm.download(response);
                        }
                    } else if (mode.equals("preview")) {
                        if (fm.setGetVar("path", request.getParameter("path"))) {
                            fm.preview(response);
                        }
                    } else {
                        fm.error(fm.lang("MODE_ERROR"));
                    }
                }
            } else if (request.getMethod().equals("POST")) {
                mode = "upload";
                System.out.println("MODE:upload");
                responseData = fm.add();
                putTextarea = true;
            }
        }
        if (responseData == null) {
            responseData = fm.getError();
            System.out.println("responseData1: ");
        }
        if (responseData != null) {
        	System.out.println("responseData2: "+ responseData.toString());
            PrintWriter pw = response.getWriter();
            String responseStr = responseData.toString();
            if (putTextarea)
                responseStr = "<textarea>" + responseStr + "</textarea>";
            //fm.log("c:\\logfilej.txt", "mode:" + mode + ",response:" + responseStr);
            pw.print(responseStr);
            pw.flush();
            pw.close();

        }

    }

    //Clean up resources
    public void destroy() {
    }
}

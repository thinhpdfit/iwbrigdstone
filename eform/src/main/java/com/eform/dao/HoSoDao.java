package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.HoSo;

@Repository
public interface HoSoDao extends CrudRepository<HoSo, Long>, HoSoDaoCustom{

}

package com.eform.dao;

import java.util.List;

import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;
import com.eform.model.entities.Signature;

public interface SignatureDaoCustom {
	public boolean checkSigned(FileVersion file, String userId);
	public List<Signature> getSignatureFind(FileVersion file);
	
}

package com.eform.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;

@Repository
public class BieuMauDaoImpl implements BieuMauDaoCustom{

	@PersistenceContext 
	private EntityManager em;
	
	@Autowired
	BmODao bmODao;
	@Autowired
	BmOTruongThongTinDao bmOTruongThongTinDao;
	@Autowired
	BmHangDao bmHangDao;
	@Autowired
	FormulaDao formulaDao;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BieuMau> findByCode(String ma) {
		if(ma == null || ma.isEmpty()){
			return null;
		}
		String sql = "select ldm from BieuMau ldm where UPPER(ma) = :ma";
		return  em.createQuery(sql).setParameter("ma", ma.toUpperCase()).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BieuMau> findByCode(List<String> maList){
		if(maList == null || maList.isEmpty()){
			return new ArrayList<BieuMau>();
		}
		String sql = "select ldm from BieuMau ldm where ldm.ma in :maList";
		return  em.createQuery(sql).setParameter("maList", maList).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
    public List<BieuMau> getBieuMauFind(String ma, String ten, Long loaiBieuMau, List<Long> trangThai, int firstResult, int maxResults) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (loaiBieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiBieuMau = :loaiBieuMau ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        Query query = em.createQuery((("SELECT o FROM BieuMau o "+ sql)+" ORDER BY o.id DESC"));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (loaiBieuMau!= null) {
            query.setParameter("loaiBieuMau", loaiBieuMau);
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }

	@Override
    public Long getBieuMauFind(String ma, String ten, Long loaiBieuMau, List<Long> trangThai) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (loaiBieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiBieuMau = :loaiBieuMau ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        Query query = em.createQuery((("SELECT count(o) FROM BieuMau o "+ sql)+""));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (loaiBieuMau!= null) {
            query.setParameter("loaiBieuMau", loaiBieuMau);
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        return ((Long) query.getSingleResult());
    }
	
	@Override
	@Transactional(transactionManager="eformTransactionManager")
	public void updateBieuMau(BieuMau obj, List<BmO> bmOList, List<BmHang> bmHangList,
			List<BmOTruongThongTin> oTruongTTList) {
		if (obj != null) {
			if (obj.getId() != null) {
				em.merge(obj);
				
//				bmOTruongThongTinDao.deleteByBieuMau(obj);
				em.createQuery("DELETE FROM BmOTruongThongTin o WHERE o.bieuMau = :bieuMau").setParameter("bieuMau", obj).executeUpdate();

//				bmODao.deleteByBieuMau(obj);
				em.createQuery("DELETE FROM BmO o WHERE o.bieuMau = :bieuMau").setParameter("bieuMau", obj).executeUpdate();
				
//				bmHangDao.deleteByBieuMau(obj);
				em.createQuery("DELETE FROM BmHang o WHERE o.bieuMau = :bieuMau").setParameter("bieuMau", obj).executeUpdate();
				
				em.createQuery("DELETE FROM Formula o WHERE o.bieuMau = :bieuMau").setParameter("bieuMau", obj).executeUpdate();
				
			} else {
				em.persist(obj);
			}
			
			if(bmHangList != null){
				for(BmHang bmHang : bmHangList){
					bmHangDao.save(bmHang);
				}
			}
			
			if(bmOList != null){
				for(BmO bmO : bmOList){
					bmODao.save(bmO);
				}
			}
			
			if(oTruongTTList != null){
				for(BmOTruongThongTin oTruongTT : oTruongTTList){
					bmOTruongThongTinDao.save(oTruongTT);
				}
			}
		}
	}

	@Override
	public void deleteBieuMau(BieuMau obj) {
		if (obj != null) {
			if (obj.getId() != null) {
				bmOTruongThongTinDao.deleteByBieuMau(obj);
				bmODao.deleteByBieuMau(obj);
				bmHangDao.deleteByBieuMau(obj);
				em.createQuery("DELETE FROM Formula o WHERE o.bieuMau = :bieuMau").setParameter("bieuMau", obj).executeUpdate();
				em.remove(obj);
			} 
		}
	}

	@SuppressWarnings("unchecked")
	@Override
    public List<BieuMau> getBieuMauFind(String ma, List<String> maList, String ten, Long loaiBieuMau, List<Long> trangThai, int firstResult, int maxResults) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((maList!= null)&&(maList.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) in :maList ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (loaiBieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiBieuMau = :loaiBieuMau ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        Query query = em.createQuery((("SELECT o FROM BieuMau o "+ sql)+" ORDER BY o.id DESC "));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((maList!= null)&&(maList.isEmpty() == false)) {
            query.setParameter("maList", maList);
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (loaiBieuMau!= null) {
            query.setParameter("loaiBieuMau", loaiBieuMau);
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }

	@Override
    public Long getBieuMauFind(String ma, List<String> maList, String ten, Long loaiBieuMau, List<Long> trangThai) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((maList!= null)&&(maList.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ma in :maList ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (loaiBieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiBieuMau = :loaiBieuMau ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        Query query = em.createQuery((("SELECT count(o) FROM BieuMau o "+ sql)+""));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((maList!= null)&&(maList.isEmpty() == false)) {
            query.setParameter("maList", maList);
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (loaiBieuMau!= null) {
            query.setParameter("loaiBieuMau", loaiBieuMau);
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        return ((Long) query.getSingleResult());
    }

	@Override
	public void insertWithId(List<BieuMau> list) {
		for (BieuMau bieuMau : list) {
			em.merge(bieuMau);
		}
	}


}
package com.eform.dao;

import java.util.List;
import java.util.Map;

import com.eform.model.entities.HoSo;

public interface HoSoDaoCustom {
	public List<HoSo> getHoSoFind(
			String ma, 
			List<Long> trangThai, 
			String taskId,
			String processInstanceId,
			int firstResult, 
			int maxResults);
	public List<HoSo> getHoSoFind(List<String> processInstanceIdList);
	
	public List<Map<String, Object>> getReportResult(String sql);
	
	public List<Map<String, Object>> getReportResult(String sql, Object[] objParams, int[] types);

	public List<Map<String, Object>> getHoSoJsonByKey(String sql, Object[] objParams, int[] types);
	
	public List<Map<String, Object>> getReportResult(String sql, Map<String, Object> paramMap);

}

package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.ChatGroup;
import com.eform.model.entities.ChatMessage;

@Repository
public class ChatMessageDaoImpl implements ChatMessageDaoCustom{

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<ChatMessage> find(String userId, ChatGroup group, int from, int max) {
		String sql = "";
        if ((userId!= null)&&(userId.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.userId = :userId ");
        }
        if (group!= null && group.getId() != null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.group.id = :groupId ");
        }
        
        Query query = em.createQuery((("SELECT o FROM ChatMessage o "+ sql)+""));
        if ((userId!= null)&&(userId.isEmpty() == false)) {
        	 query.setParameter("userId", userId);
        }
        if (group!= null && group.getId() != null) {
        	query.setParameter("groupId", group.getId());
        }
        
        if (from > 0) {
            query = query.setFirstResult(from);
        }
        if (max > 0) {
            query = query.setMaxResults(max);
        }
        return query.getResultList();
	}

	

}


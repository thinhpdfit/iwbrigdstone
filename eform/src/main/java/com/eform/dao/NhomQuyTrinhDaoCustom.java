package com.eform.dao;

import java.util.List;

import com.eform.model.entities.NhomQuyTrinh;

public interface NhomQuyTrinhDaoCustom{
	public List<NhomQuyTrinh> findByCode(String ma);
	public List<NhomQuyTrinh> getNhomQuyTrinhFind(String ma,
    		String ten, 
    		List<Long> trangThai, 
    		NhomQuyTrinh nhomQuyTrinh, 
    		int firstResult, 
    		int maxResults);
	public Long getNhomQuyTrinhFind(String ma,
    		String ten, 
    		List<Long> trangThai, 
    		NhomQuyTrinh nhomQuyTrinh);
}
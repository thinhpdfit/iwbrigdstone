package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.LoaiDanhMuc;

public interface LoaiDanhMucDao extends CrudRepository<LoaiDanhMuc, Long>, LoaiDanhMucDaoCustom{
}

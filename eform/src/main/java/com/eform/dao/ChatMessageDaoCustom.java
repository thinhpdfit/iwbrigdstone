package com.eform.dao;

import java.util.List;

import com.eform.model.entities.ChatGroup;
import com.eform.model.entities.ChatMessage;

public interface ChatMessageDaoCustom {
	public List<ChatMessage> find(String userId, ChatGroup group, int from, int max);
}

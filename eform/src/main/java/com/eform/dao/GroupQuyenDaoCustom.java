package com.eform.dao;

import java.util.List;

import org.activiti.engine.identity.Group;

import com.eform.model.entities.GroupQuyen;
import com.eform.model.entities.HtQuyen;


public interface GroupQuyenDaoCustom {
	
	public List<GroupQuyen> getGroupQuyenFind(
			String groupId, 
			int firstResult, 
			int maxResults);
	
	public List<GroupQuyen> getGroupQuyenFind(
			List<String> groupId, 
			HtQuyen htQuyen,
			int firstResult, 
			int maxResults);
	
	public Long getGroupQuyenFind(
    		String groupId);
	
	
	public void deleteByGroup(Group group);
}

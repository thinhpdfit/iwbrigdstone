package com.eform.dao;

import java.util.List;

import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HtDonVi;


public interface DeploymentHtDonViDaoCustom {

	public List<DeploymentHtDonVi> getDeploymentHtDonViFind(String modelerId);
	public List<DeploymentHtDonVi> getDeploymentHtDonViFind(String modelerId, HtDonVi donVi);
}

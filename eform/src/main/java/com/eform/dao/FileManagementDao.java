package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;

public interface FileManagementDao extends CrudRepository<FileManagement, Long>, FileManagementDaoCustom{

}

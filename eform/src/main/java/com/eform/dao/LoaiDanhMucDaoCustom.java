package com.eform.dao;

import java.util.List;

import com.eform.model.entities.LoaiDanhMuc;

public interface LoaiDanhMucDaoCustom{
	public List<LoaiDanhMuc> findByCode(String ma);
	public List<LoaiDanhMuc> getLoaiDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc,
			int firstResult, 
			int maxResults);
	 public Long getLoaiDanhMucFind(
			 String ma, 
			 String ten, 
			 List<Long> trangThai, 
			 LoaiDanhMuc loaiDanhMuc);
	 
	 public void insertWithId(List<LoaiDanhMuc> loaiDanhMuc);

}

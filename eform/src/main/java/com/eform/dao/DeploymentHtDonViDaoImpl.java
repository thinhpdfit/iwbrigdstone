package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HtDonVi;

@Repository
public class DeploymentHtDonViDaoImpl  implements DeploymentHtDonViDaoCustom{

	@PersistenceContext private EntityManager em;

	@Override
	public List<DeploymentHtDonVi> getDeploymentHtDonViFind(String deploymentId) {
		String sql = "";
		if(deploymentId != null){
			sql += " where o.deploymentId = :deploymentId ";
		}
		sql = "select o from DeploymentHtDonVi o " + sql;
		Query q = em.createQuery(sql);
		if(deploymentId != null){
			q.setParameter("deploymentId", deploymentId);
		}
		return q.getResultList();
	}

	@Override
	public List<DeploymentHtDonVi> getDeploymentHtDonViFind(String deploymentId, HtDonVi donVi) {
		String sql = "";
		if(deploymentId != null){
			sql += " where o.deploymentId = :deploymentId ";
		}
		if(donVi != null && donVi.getId() != null){
			if(!sql.isEmpty()){
				sql += " and ";
			}else{
				sql += " where ";
			}
			sql += " o.htDonVi.id = :htDonViId ";
		}
		sql = "select o from DeploymentHtDonVi o " + sql;
		Query q = em.createQuery(sql);
		if(deploymentId != null){
			q.setParameter("deploymentId", deploymentId);
		}
		if(donVi != null && donVi.getId() != null){
			q.setParameter("htDonViId", donVi.getId());
		}
		return q.getResultList();
	}
	
	
	
}

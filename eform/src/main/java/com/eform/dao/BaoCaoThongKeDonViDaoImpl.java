package com.eform.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.BaoCaoThongKe;

@Repository
public class BaoCaoThongKeDonViDaoImpl implements BaoCaoThongKeDonViDaoCustom{

	@PersistenceContext 
	private EntityManager em;

	@Override
	public void deleteByBaoCaoThongKe(BaoCaoThongKe bctk) {
		if(bctk != null && bctk.getId() != null){
			String sql = "delete from BaoCaoThongKeDonVi o where o.baoCaoThongKe = :baoCaoThongKe";
			em.createQuery(sql).setParameter("baoCaoThongKe", bctk).executeUpdate();
		}
	}
	
}
package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.HtDonVi;

@Repository
public class BaoCaoThongKeDaoImpl implements BaoCaoThongKeDaoCustom{

	@PersistenceContext 
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BaoCaoThongKe> findByCode(String ma) {
		if(ma == null || ma.isEmpty()){
			return null;
		}
		String sql = "select ldm from BaoCaoThongKe ldm where UPPER(ma) = :ma";
		return  em.createQuery(sql).setParameter("ma", ma.toUpperCase()).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
    public List<BaoCaoThongKe> getBaoCaoThongKeFind(String ma, String ten, List<HtDonVi> htDonViList,List<Long> trangThai,  
			boolean laDonViTao, int firstResult, int maxResults) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        

        if (htDonViList!= null && !htDonViList.isEmpty()) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.htDonVi in :htDonViList ");
        }
        
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        if(htDonViList!= null && !htDonViList.isEmpty() && !laDonViTao){
          	 if (sql.isEmpty()) {
                   sql = "WHERE ";
               } else {
                   sql = (sql +"OR ");
               }
               sql = (sql +"o.id in (select o1.baoCaoThongKe.id from BaoCaoThongKeDonVi o1 where o1.htDonVi in :htDonViList1 and o1.baoCaoThongKe.trangThai = 1)");
          }
        Query query = em.createQuery((("SELECT o FROM BaoCaoThongKe o "+ sql)+" ORDER BY o.ngayTao DESC "));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (htDonViList!= null && !htDonViList.isEmpty()) {
        	query.setParameter("htDonViList", htDonViList);
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }

        if(htDonViList!= null && !htDonViList.isEmpty() && !laDonViTao){
        	query.setParameter("htDonViList1", htDonViList);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }

	@Override
	public Long getBaoCaoThongKeFind(String ma, String ten,List<HtDonVi> htDonViList, List<Long> trangThai,  
			boolean laDonViTao) {
		String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (htDonViList!= null && !htDonViList.isEmpty()) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.htDonVi in :htDonViList ");
            
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        if(htDonViList!= null && !htDonViList.isEmpty() && !laDonViTao){
       	 if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"OR ");
            }
            sql = (sql +"o.id in (select o1.baoCaoThongKe.id from BaoCaoThongKeDonVi o1 where o1.htDonVi in :htDonViList1 and o1.baoCaoThongKe.trangThai = 1)");
       }
        Query query = em.createQuery("SELECT count(o) FROM BaoCaoThongKe o "+ sql);
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (htDonViList!= null && !htDonViList.isEmpty()) {
        	query.setParameter("htDonViList", htDonViList);
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if(htDonViList!= null && !htDonViList.isEmpty() && !laDonViTao){
        	query.setParameter("htDonViList1", htDonViList);
        }
        return (Long) query.getSingleResult();
	}


}
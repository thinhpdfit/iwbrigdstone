package com.eform.dao;

import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;

public interface FileManagementDaoCustom {
	public FileVersion getMaxVersion(FileManagement file);
	public void deleteWithChild(FileManagement file);
	
}

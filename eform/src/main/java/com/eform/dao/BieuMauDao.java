package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.BieuMau;

public interface BieuMauDao extends CrudRepository<BieuMau, Long>, BieuMauDaoCustom{

}

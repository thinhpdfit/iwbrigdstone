package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;

@Repository
public class TruongThongTinDaoImpl implements TruongThongTinDaoCustom{

	@PersistenceContext private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TruongThongTin> findByCode(String ma) {
		if(ma == null || ma.isEmpty()){
			return null;
		}
		String sql = "select ldm from TruongThongTin ldm where UPPER(ma) = :ma";
		return  em.createQuery(sql).setParameter("ma", ma.toUpperCase()).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
    public List<TruongThongTin> getTruongThongTinFind(String ma, String ten, List<Long> trangThai, LoaiDanhMuc loaiDanhMuc,
			List<Long> kieuDuLieu,
			List<String> viTriHienThi, int firstResult, int maxResults) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        if (loaiDanhMuc!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiDanhMuc = :loaiDanhMuc ");
        }
        
        if (kieuDuLieu!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.kieuDuLieu in :kieuDuLieu ");
        }
        if (viTriHienThi!= null && !viTriHienThi.isEmpty()) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +" ( ");
            int i = 0;
            for(String str : viTriHienThi){
            	i++;
            	sql += " o.viTriHienThi like :viTriHienThi"+str;
            	if(i < viTriHienThi.size()){
            		sql += " or ";
            	}
            }
            sql = (sql +" ) ");
        }
        Query query = em.createQuery((("SELECT o FROM TruongThongTin o "+ sql)+" ORDER BY o.id DESC "));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if (loaiDanhMuc!= null) {
            query.setParameter("loaiDanhMuc", loaiDanhMuc);
        }
        if (kieuDuLieu!= null) {
            query.setParameter("kieuDuLieu", kieuDuLieu);
        }
        if (viTriHienThi!= null && !viTriHienThi.isEmpty()) {
            for(String str : viTriHienThi){
            	query.setParameter("viTriHienThi"+str, "%"+str+"%");
            }
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }

	@Override
    public Long getTruongThongTinFind(String ma, String ten, List<Long> trangThai, LoaiDanhMuc loaiDanhMuc,
			List<Long> kieuDuLieu,
			List<String> viTriHienThi) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        if (loaiDanhMuc!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiDanhMuc = :loaiDanhMuc ");
        }
        if (kieuDuLieu!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.kieuDuLieu in :kieuDuLieu ");
        }
        if (viTriHienThi!= null && !viTriHienThi.isEmpty()) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +" ( ");
            int i = 0;
            for(String str : viTriHienThi){
            	i++;
            	sql += " o.viTriHienThi like :viTriHienThi"+str;
            	if(i < viTriHienThi.size()){
            		sql += " or ";
            	}
            }
            sql = (sql +" ) ");
        }
        Query query = em.createQuery((("SELECT count(o) FROM TruongThongTin o "+ sql)+""));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if (loaiDanhMuc!= null) {
            query.setParameter("loaiDanhMuc", loaiDanhMuc);
        }
        if (kieuDuLieu!= null) {
            query.setParameter("kieuDuLieu", kieuDuLieu);
        }
        if (viTriHienThi!= null && !viTriHienThi.isEmpty()) {
            for(String str : viTriHienThi){
            	query.setParameter("viTriHienThi"+str, "%"+str+"%");
            }
        }
        return ((Long) query.getSingleResult());
    }

	@Override
	public void insertWithId(List<TruongThongTin> list) {
		for (TruongThongTin truongThongTin : list) {
			em.merge(truongThongTin);
		}

		em.flush();
	}

	
}

package com.eform.dao;

import com.eform.model.entities.EmailPheDuyet;

public interface EmailPheDuyetDaoCustom {
	
	public EmailPheDuyet findByRefCode(String refCode);
	public EmailPheDuyet findByRefCode(Long id, String refCode, Long trangThai);
}

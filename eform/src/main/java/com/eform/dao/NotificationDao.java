package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.Notification;

@Transactional(transactionManager="eformTransactionManager")
public interface NotificationDao extends CrudRepository<Notification, Long>, NotificationDaoCustom{

}

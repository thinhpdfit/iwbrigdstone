package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.ProcinstInfo;

public interface ProcinstInfoDao extends CrudRepository<ProcinstInfo, Long>, ProcinstInfoDaoCustom{
	
}

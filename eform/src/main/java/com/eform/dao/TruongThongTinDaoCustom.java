package com.eform.dao;

import java.util.List;

import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;


public interface TruongThongTinDaoCustom {
	
	public List<TruongThongTin> findByCode(String ma);
	
	public List<TruongThongTin> getTruongThongTinFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc, 
			List<Long> kieuDuLieu,
			List<String> viTriHienThi, 
			int firstResult, 
			int maxResults);
	
	public Long getTruongThongTinFind(
    		String ma, 
    		String ten, 
    		List<Long> trangThai, 
    		LoaiDanhMuc loaiDanhMuc,
			List<Long> kieuDuLieu,
			List<String> viTriHienThi);
	public void insertWithId(List<TruongThongTin> list);
}

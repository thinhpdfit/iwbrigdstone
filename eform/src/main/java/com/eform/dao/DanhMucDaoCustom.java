package com.eform.dao;

import java.util.List;

import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;

public interface DanhMucDaoCustom {
	public List<DanhMuc> findByCode(String ma);
	public List<DanhMuc> getDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc,
			DanhMuc danhMuc, 
			int firstResult, 
			int maxResults);

	public Long getDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc, 
			DanhMuc danhMuc);

	 public void insertWithId(List<DanhMuc> ldanhMuc);
}

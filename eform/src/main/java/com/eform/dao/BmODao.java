package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.BmO;

public interface BmODao extends CrudRepository<BmO, Long>, BmODaoCustom{

}

package com.eform.dao;

import java.util.List;

import com.eform.model.entities.HtThamSo;

public interface HtThamSoDaoCustom {
	public List<HtThamSo> getHtThamSoFind(
			String ma, 
			List<Long> trangThai, 
			List<Long> loai, 
			int firstResult, 
			int maxResults);

	public HtThamSo getHtThamSoFind(String ma);
	
	public void updateList(List<HtThamSo> htThamSoList);
}

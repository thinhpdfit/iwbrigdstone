package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.Signature;

public interface SignatureDao extends CrudRepository<Signature, Long>, SignatureDaoCustom{

}

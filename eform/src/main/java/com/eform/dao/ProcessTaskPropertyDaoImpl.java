package com.eform.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(transactionManager="eformTransactionManager")
public class ProcessTaskPropertyDaoImpl implements ProcessTaskPropertyDaoCustom{

	@PersistenceContext private EntityManager em;
    
}

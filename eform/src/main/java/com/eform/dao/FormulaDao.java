package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.Formula;

public interface FormulaDao extends CrudRepository<Formula, Long>, FormulaDaoCustom{

}

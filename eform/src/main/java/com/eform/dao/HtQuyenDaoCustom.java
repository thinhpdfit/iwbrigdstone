package com.eform.dao;

import java.util.List;

import com.eform.model.entities.HtQuyen;


public interface HtQuyenDaoCustom {
	
	public List<HtQuyen> findByCode(String ma);
	
	public List<HtQuyen> getHtQuyenFind(
			String ma, 
			String viewName,
			String ten, 
			List<Long> trangThai,
			HtQuyen htQuyen,
			int firstResult, 
			int maxResults);
	
	public Long getHtQuyenFind(
    		String ma, 
			String viewName,
    		String ten, 
    		List<Long> trangThai,
			HtQuyen htQuyen);
}

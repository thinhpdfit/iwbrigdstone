package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.TaskButton;

@Repository
public interface TaskButtonDao extends CrudRepository<TaskButton, Long>, TaskButtonDaoCustom{

}

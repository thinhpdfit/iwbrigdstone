package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.ProcinstInfo;

@Repository
public class ProcinstInfoDaoImpl implements ProcinstInfoDaoCustom{

	@PersistenceContext
	private EntityManager em;

	@Override
	public ProcinstInfo findByProcinstIdAndInfoKey(String procinstId, String infoKey) {
		if(procinstId != null && infoKey != null){
			String sql = "SELECT o from ProcinstInfo o where o.procinstId = :procinstId and o.infoKey = :infoKey";
			List<ProcinstInfo> results = em.createQuery(sql).setParameter("procinstId", procinstId).setParameter("infoKey", infoKey).setFirstResult(0).setMaxResults(1).getResultList();
			if(results != null && !results.isEmpty()){
				return results.get(0);
			}
		}
		return null;
	}

	
}


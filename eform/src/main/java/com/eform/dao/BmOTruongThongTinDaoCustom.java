package com.eform.dao;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.TruongThongTin;


public interface BmOTruongThongTinDaoCustom {
	public int deleteByBieuMau(BieuMau bieuMau);
    List<BmOTruongThongTin> getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO , TruongThongTin truongTT, int firstResult, int maxResults);
    Long getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO, TruongThongTin truongTT);
    List<TruongThongTin> findTruongThongtinByBieuMau(List<BieuMau> bieuMauList, Boolean tableDisplay, Boolean searchFormDisPlay);
    public void insertWithId(List<BmOTruongThongTin> list);
}

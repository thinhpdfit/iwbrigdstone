package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.EmailPheDuyet;

@Repository
public class EmailPheDuyetDaoImpl  implements EmailPheDuyetDaoCustom{

	@PersistenceContext private EntityManager em;

	@Override
	public EmailPheDuyet findByRefCode(String refCode) {
		List<EmailPheDuyet> results = em.createQuery("Select o from EmailPheDuyet o where o.refCode = :refCode").setParameter("refCode", refCode).setMaxResults(1).getResultList();
		if(results != null && !results.isEmpty()){
			return results.get(0);
		}
		return null;
	}
	
	@Override
	public EmailPheDuyet findByRefCode(Long id, String refCode, Long trangThai) {
		List<EmailPheDuyet> results = em.createQuery("Select o from EmailPheDuyet o where o.id = :id and o.refCode = :refCode and o.trangThai = :trangThai").setParameter("id", id).setParameter("refCode", refCode).setParameter("trangThai", trangThai).setMaxResults(1).getResultList();
		if(results != null && !results.isEmpty()){
			return results.get(0);
		}
		return null;
	}
	
}

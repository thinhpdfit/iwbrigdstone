package com.eform.dao;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;


public interface BmHangDaoCustom {
	public int deleteByBieuMau(BieuMau bieuMau);
    List<BmHang> getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau, int firstResult, int maxResults);
    Long getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau);
    public void insertWithId(List<BmHang> list);
}

package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.NhomQuyTrinh;

@Repository
@Transactional(transactionManager="eformTransactionManager")
public class NhomQuyTrinhDaoImpl implements NhomQuyTrinhDaoCustom{

	@PersistenceContext private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<NhomQuyTrinh> findByCode(String ma) {
		if(ma == null || ma.isEmpty()){
			return null;
		}
		String sql = "select ldm from NhomQuyTrinh ldm where UPPER(ma) = :ma";
		return  em.createQuery(sql).setParameter("ma", ma.toUpperCase()).getResultList();
	}
	
	
    @SuppressWarnings("unchecked")
	public List<NhomQuyTrinh> getNhomQuyTrinhFind(String ma, String ten, List<Long> trangThai, NhomQuyTrinh nhomQuyTrinh, int firstResult, int maxResults) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        if (nhomQuyTrinh!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.nhomQuyTrinh = :nhomQuyTrinh ");
        }
        Query query = em.createQuery((("SELECT o FROM NhomQuyTrinh o "+ sql + " ORDER BY o.id DESC ")));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if (nhomQuyTrinh!= null) {
            query.setParameter("nhomQuyTrinh", nhomQuyTrinh);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }


	@Override
	public Long getNhomQuyTrinhFind(String ma, String ten, List<Long> trangThai, NhomQuyTrinh nhomQuyTrinh) {
		 String sql = "";
	        if ((ma!= null)&&(ma.isEmpty() == false)) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"UPPER(o.ma) like :ma ");
	        }
	        if ((ten!= null)&&(ten.isEmpty() == false)) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"UPPER(o.ten) like :ten ");
	        }
	        if (trangThai!= null) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"o.trangThai in :trangThai ");
	        }
	        if (nhomQuyTrinh!= null) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"o.nhomQuyTrinh = :nhomQuyTrinh ");
	        }
	        Query query = em.createQuery((("SELECT count(o) FROM NhomQuyTrinh o "+ sql)));
	        if ((ma!= null)&&(ma.isEmpty() == false)) {
	            query.setParameter("ma", ma.toUpperCase());
	        }
	        if ((ten!= null)&&(ten.isEmpty() == false)) {
	            query.setParameter("ten", ten.toUpperCase());
	        }
	        if (trangThai!= null) {
	            query.setParameter("trangThai", trangThai);
	        }
	        if (nhomQuyTrinh!= null) {
	            query.setParameter("nhomQuyTrinh", nhomQuyTrinh);
	        }
	        return (Long) query.getSingleResult();
	}
    
    
}

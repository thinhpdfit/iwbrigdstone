package com.eform.dao;

import java.util.List;

import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.HtDonVi;

public interface BaoCaoThongKeDaoCustom {
	public List<BaoCaoThongKe> findByCode(String ma);
	public List<BaoCaoThongKe> getBaoCaoThongKeFind(
			String ma, 
			String ten, 
			List<HtDonVi> htDonViList,
			List<Long> trangThai,  
			boolean laDonViTao, 
			int firstResult, 
			int maxResults);
	public Long getBaoCaoThongKeFind(
			String ma, 
			String ten, 
			List<HtDonVi> htDonViList,
			List<Long> trangThai,  
			boolean laDonViTao);
}

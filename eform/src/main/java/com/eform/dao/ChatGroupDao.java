package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.ChatGroup;

public interface ChatGroupDao extends CrudRepository<ChatGroup, Long>, ChatGroupDaoCustom{

}

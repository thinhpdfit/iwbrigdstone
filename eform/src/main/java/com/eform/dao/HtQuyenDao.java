package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.HtQuyen;

@Repository
public interface HtQuyenDao extends CrudRepository<HtQuyen, Long>, HtQuyenDaoCustom{

}

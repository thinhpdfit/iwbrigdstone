package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.TruongThongTin;

@Transactional(transactionManager="eformTransactionManager")
public interface TruongThongTinDao extends CrudRepository<TruongThongTin, Long>, TruongThongTinDaoCustom{

}

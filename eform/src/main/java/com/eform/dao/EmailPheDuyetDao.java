package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.EmailPheDuyet;

@Repository
public interface EmailPheDuyetDao extends CrudRepository<EmailPheDuyet, Long>, EmailPheDuyetDaoCustom{

}

package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.HtThamSo;

public interface HtThamSoDao extends CrudRepository<HtThamSo, Long>, HtThamSoDaoCustom{

}

package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.DanhMuc;

@Repository
public interface DanhMucDao extends CrudRepository<DanhMuc, Long>, DanhMucDaoCustom{

}

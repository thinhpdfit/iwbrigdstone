package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.LoaiDanhMuc;

@Repository
public class LoaiDanhMucDaoImpl implements LoaiDanhMucDaoCustom{
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LoaiDanhMuc> findByCode(String ma) {
		if(ma == null || ma.isEmpty()){
			return null;
		}
		String sql = "select ldm from LoaiDanhMuc ldm where UPPER(ma) = :ma";
		return em.createQuery(sql).setParameter("ma", ma.toUpperCase()).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LoaiDanhMuc> getLoaiDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc,
			int firstResult, 
			int maxResults) {
		
		String sql = "";
		if ((ma != null) && (ma.isEmpty() == false)) {
			if (sql.isEmpty()) {
				sql = "WHERE ";
			} else {
				sql = (sql + "AND ");
			}
			sql = (sql + "UPPER(o.ma) like :ma ");
		}
		if ((ten != null) && (ten.isEmpty() == false)) {
			if (sql.isEmpty()) {
				sql = "WHERE ";
			} else {
				sql = (sql + "AND ");
			}
			sql = (sql + "UPPER(o.ten) like :ten ");
		}
		if (trangThai != null) {
			if (sql.isEmpty()) {
				sql = "WHERE ";
			} else {
				sql = (sql + "AND ");
			}
			sql = (sql + "o.trangThai in :trangThai ");
		}
		if (loaiDanhMuc != null) {
			if (sql.isEmpty()) {
				sql = "WHERE ";
			} else {
				sql = (sql + "AND ");
			}
			sql = (sql + "o.loaiDanhMuc = :loaiDanhMuc ");
		}
		Query query = em.createQuery((("SELECT o FROM LoaiDanhMuc o " + sql) +" ORDER BY o.id DESC "));
		if ((ma != null) && (ma.isEmpty() == false)) {
			query.setParameter("ma", ma.toUpperCase());
		}
		if ((ten != null) && (ten.isEmpty() == false)) {
			query.setParameter("ten", ten.toUpperCase());
		}
		if (trangThai != null) {
			query.setParameter("trangThai", trangThai);
		}
		if (loaiDanhMuc != null) {
			query.setParameter("loaiDanhMuc", loaiDanhMuc);
		}
		if (firstResult > 0) {
			query = query.setFirstResult(firstResult);
		}
		if (maxResults > 0) {
			query = query.setMaxResults(maxResults);
		}
		return query.getResultList();
	}
	
	@Override
    public Long getLoaiDanhMucFind(
    		String ma, 
    		String ten, 
    		List<Long> trangThai, 
    		LoaiDanhMuc loaiDanhMuc) {
    	
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        if (loaiDanhMuc!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiDanhMuc = :loaiDanhMuc ");
        }
        Query query = em.createQuery((("SELECT count(o) FROM LoaiDanhMuc o "+ sql)+""));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if (loaiDanhMuc!= null) {
            query.setParameter("loaiDanhMuc", loaiDanhMuc);
        }
        return ((Long) query.getSingleResult());
    }

	@Override
	public void insertWithId(List<LoaiDanhMuc> loaiDanhMucList) {
		for (LoaiDanhMuc loaiDanhMuc : loaiDanhMucList) {
			em.persist(loaiDanhMuc);
		}
	}

}

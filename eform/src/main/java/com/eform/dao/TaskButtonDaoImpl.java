package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.GroupQuyen;
import com.eform.model.entities.TaskButton;

@Repository
public class TaskButtonDaoImpl  implements TaskButtonDaoCustom{

	@PersistenceContext private EntityManager em;

	@Override
	public List<TaskButton> getTaskButton(String activitiId, String modelId) {
		String sql = "select o from TaskButton o where 1 = 1 ";
		if(activitiId != null){
			sql += " and o.activitiId = :activitiId";
		}
		if(modelId != null){
			sql += " and o.modelId = :modelId";
			
		}
		Query q = em.createQuery(sql);
		if(activitiId != null){
			q.setParameter("activitiId", activitiId);
		}
		if(modelId != null){
			q.setParameter("modelId", modelId);
			
		}
		return q.getResultList();
	}
	
}

package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;

@Repository
public class FileVersionDaoImpl implements FileVersionDaoCustom{

	@PersistenceContext private EntityManager em;

	@Override
	public List<FileVersion> getFileVersionFind(FileManagement manager) {
		if(manager != null){
			String sql = "select o from FileVersion o where o.fileManagement.id = :fileManagementId order by o.version desc";
			List<FileVersion> list = em.createQuery(sql).setParameter("fileManagementId", manager.getId()).getResultList();
			return list;
		}
		
		return null;
	}

	@Override
	public void deleteWithChild(FileVersion file) {
		if(file!= null){
			String sql1 = "delete from Signature o where o.fileVersion.id = :fileVersionId";
			em.createQuery(sql1).setParameter("fileVersionId", file.getId()).executeUpdate();
			String sql2 = "delete from FileVersion o where o.id = :fileVersionId";
			em.createQuery(sql2).setParameter("fileVersionId", file.getId()).executeUpdate();
		}
	}
}


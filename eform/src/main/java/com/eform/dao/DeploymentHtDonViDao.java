package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.DeploymentHtDonVi;

@Repository
public interface DeploymentHtDonViDao extends CrudRepository<DeploymentHtDonVi, Long>, DeploymentHtDonViDaoCustom{

}

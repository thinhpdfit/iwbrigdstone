package com.eform.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.HoSo;

@Repository
public class HoSoDaoImpl implements HoSoDaoCustom{

	@PersistenceContext private EntityManager em;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public List<HoSo> getHoSoFind(String ma, List<Long> trangThai, String taskId, String processInstanceId,
			int firstResult, int maxResults) {
		 String sql = "";
	        if ((ma!= null)&&(ma.isEmpty() == false)) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"o.ma like :ma ");
	        }
	        
	        if (trangThai!= null) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"o.trangThai in :trangThai ");
	        }
	        if ((taskId!= null)&&(taskId.isEmpty() == false)) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"o.taskId = :taskId ");
	        }
	        if ((processInstanceId!= null)&&(processInstanceId.isEmpty() == false)) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"o.processInstanceId = :processInstanceId ");
	        }
	        Query query = em.createQuery(("SELECT o FROM HoSo o "+ sql));
	        if ((ma!= null)&&(ma.isEmpty() == false)) {
	            query.setParameter("ma", ma);
	        }
	        
	        if (trangThai!= null) {
	            query.setParameter("trangThai", trangThai);
	        }
	        if ((taskId!= null)&&(taskId.isEmpty() == false)) {
	            query.setParameter("taskId", taskId);
	        }
	        if ((processInstanceId!= null)&&(processInstanceId.isEmpty() == false)) {
	            query.setParameter("processInstanceId", processInstanceId);
	        }
	        if (firstResult > 0) {
	            query = query.setFirstResult(firstResult);
	        }
	        if (maxResults > 0) {
	            query = query.setMaxResults(maxResults);
	        }
	        return query.getResultList();
	}

	@Override
	public List<HoSo> getHoSoFind(List<String> processInstanceIdList) {
		if(processInstanceIdList != null && !processInstanceIdList.isEmpty()){
//			Object str =jdbcTemplate.queryForList("select (ho_so_json::json#>>'{hoso,name}') as group ,sum((ho_so_json::json#>>'{hoso,xe}')::integer) as value from ho_so group by (ho_so_json::json#>>'{hoso,name}')");
//			
			String sql = "select o from HoSo o where o.processInstanceId in :processInstanceIdList";
			return em.createQuery(sql).setParameter("processInstanceIdList", processInstanceIdList).getResultList();
		}
		return new ArrayList();
	}
	
	@Override
	public List<Map<String, Object>> getReportResult(String sql){
		return jdbcTemplate.queryForList(sql);
	}
	
	@Override
	public List<Map<String, Object>> getReportResult(String sql, Object[] objParams, int[] types){
		return jdbcTemplate.queryForList(sql, objParams, types);
	}

	@Override
	public List<Map<String, Object>> getHoSoJsonByKey(String sql, Object[] objParams, int[] types) {
		if(sql != null){
			return jdbcTemplate.queryForList(sql, objParams, types);
		}
		return new ArrayList();
	}

	@Override
	public List<Map<String, Object>> getReportResult(String sql, Map<String, Object> paramMap) {
		SqlParameterSource namedParameters = new MapSqlParameterSource(paramMap);
		return namedParameterJdbcTemplate.queryForList(sql, namedParameters);
	}
	
}


package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.ChatMessage;

public interface ChatMessageDao extends CrudRepository<ChatMessage, Long>, ChatMessageDaoCustom{

}

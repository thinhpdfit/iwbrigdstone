package com.eform.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.HtQuyen;
import com.google.gwt.thirdparty.guava.common.collect.Lists;

@Transactional(transactionManager="eformTransactionManager")
@Service
public class HtQuyenServices {
	@Autowired
	HtQuyenDao htQuyenDao;

	public void update(List<HtQuyen> update,List<HtQuyen>delete){
		htQuyenDao.save(update);
		htQuyenDao.delete(delete);
	}
	
	public List<HtQuyen> getHtQuyenFindAll(){
		return Lists.newArrayList(htQuyenDao.findAll());
	}
	
	public List<HtQuyen> getHtQuyenFind(
			String ma, 
			String viewName,
			String ten, 
			List<Long> trangThai, HtQuyen htQuyen,
			int firstResult, 
			int maxResults){
		return htQuyenDao.getHtQuyenFind(ma, viewName, ten, trangThai, htQuyen, firstResult,maxResults);
	}
	
	
	public Long getHtQuyenFind(
    		String ma, 
    		String viewName, 
    		String ten, 
    		List<Long> trangThai, HtQuyen htQuyen){
		return htQuyenDao.getHtQuyenFind(ma, viewName, ten, trangThai, htQuyen);
	}
	
}

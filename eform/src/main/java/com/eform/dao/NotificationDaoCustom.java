package com.eform.dao;

import java.util.List;

import com.eform.model.entities.Notification;

public interface NotificationDaoCustom {
	public List<Notification> getNotificationFind(
			List<Long> types, 
			String fromUserId,
			String userId,
			String refKey,
			List<Long> trangThai, 
			int firstResult, 
			int maxResults);

	public Long getNotificationFind(
			List<Long> types, 
			String fromUserId,
			String userId,
			String refKey,
			List<Long> trangThai);


	public void markAsRead();
}

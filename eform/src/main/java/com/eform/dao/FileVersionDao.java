package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.FileVersion;

public interface FileVersionDao extends CrudRepository<FileVersion, Long>, FileVersionDaoCustom{

}

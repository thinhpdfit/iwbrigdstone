package com.eform.dao;

import java.util.List;

import com.eform.model.entities.HtChucVu;


public interface HtChucVuDaoCustom {
	
	public List<HtChucVu> findByCode(String ma);
	
	public List<HtChucVu> getHtChucVuFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			int firstResult, 
			int maxResults);
	
	public Long getHtChucVuFind(
    		String ma, 
    		String ten, 
    		List<Long> trangThai);
}

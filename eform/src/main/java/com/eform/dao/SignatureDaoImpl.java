package com.eform.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.FileVersion;
import com.eform.model.entities.Signature;

@Repository
public class SignatureDaoImpl implements SignatureDaoCustom{

	@PersistenceContext private EntityManager em;

	@Override
	public boolean checkSigned(FileVersion file, String userId) {
		if(file != null && userId != null){
			String sql = "select count(o) from Signature o where o.fileVersion.id = :fileVersionId and o.nguoiKy = :userId";
			Long count = (Long) em.createQuery(sql).setParameter("fileVersionId", file.getId()).setParameter("userId", userId).getSingleResult();
			if(count > 0){
				return true;
			}
		}
		return false;
	}

	@Override
	public List<Signature> getSignatureFind(FileVersion file) {
		List<Signature> list = new ArrayList();
		if(file != null){
			String sql = "select o from Signature o where o.fileVersion.id = :fileVersionId order by o.thuTu desc";
			list = em.createQuery(sql).setParameter("fileVersionId", file.getId()).getResultList();
		}
		return list;
	}
}


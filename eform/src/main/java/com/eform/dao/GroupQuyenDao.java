package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.GroupQuyen;

@Repository
public interface GroupQuyenDao extends CrudRepository<GroupQuyen, Long>, GroupQuyenDaoCustom{

}

package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.Formula;

@Repository

public class FormulaDaoImpl implements FormulaDaoCustom{

	@PersistenceContext private EntityManager em;

	@Override
	public List<Formula> getFormulaFind(BieuMau bieuMau) {
		if(bieuMau != null && bieuMau.getId() != null){
			String sql = "select o from Formula o where o.bieuMau.id = :bmId";
			return  em.createQuery(sql).setParameter("bmId", bieuMau.getId()).getResultList();
		}
		return null;
	}
	

}


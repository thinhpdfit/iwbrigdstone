package com.eform.dao;

import java.util.List;

import com.eform.model.entities.ChatGroup;

public interface ChatGroupDaoCustom {
	public List<ChatGroup> find(String userId, String groupCode, int from, int max);
}

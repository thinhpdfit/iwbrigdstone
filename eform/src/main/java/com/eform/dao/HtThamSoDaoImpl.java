package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaUpdate;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.HtThamSo;

@Repository
public class HtThamSoDaoImpl implements HtThamSoDaoCustom{

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<HtThamSo> getHtThamSoFind(String ma, List<Long> trangThai, List<Long> loai, int firstResult, int maxResults) {
		String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ma like :ma ");
        }
        
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        
        if (loai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loai in :loai ");
        }
        
        Query query = em.createQuery(("SELECT o FROM HtThamSo o "+ sql +" order by o.thuTu"));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma);
        }
        
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        
        if (loai!= null) {
            query.setParameter("loai", loai);
        }
        
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}

	@Override
	public void updateList(List<HtThamSo> htThamSoList) {
		if(htThamSoList != null  && !htThamSoList.isEmpty()){
			for(HtThamSo p : htThamSoList){
				em.merge(p);
			}
		}
	}

	@Override
	public HtThamSo getHtThamSoFind(String ma) {
		List<HtThamSo> htThamSoList = getHtThamSoFind(ma, null, null, 0, 1);
		if(htThamSoList != null && !htThamSoList.isEmpty()){
			return htThamSoList.get(0);
		}
		return null;
	}


}


package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.HtDonVi;

@Repository
public interface HtDonViDao extends CrudRepository<HtDonVi, Long>, HtDonViDaoCustom{

}

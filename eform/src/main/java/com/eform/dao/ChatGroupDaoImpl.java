package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.ChatGroup;

@Repository
public class ChatGroupDaoImpl implements ChatGroupDaoCustom{

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<ChatGroup> find(String userId, String groupCode, int from, int max) {
		String sql = "";
        if ((userId!= null)&&(userId.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.userId = :userId ");
        }
        if (groupCode!= null && !groupCode.isEmpty()) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.code = :groupCode ");
        }
        
        Query query = em.createQuery((("SELECT o FROM ChatGroup o "+ sql)+""));
        if ((userId!= null)&&(userId.isEmpty() == false)) {
        	 query.setParameter("userId", userId);
        }
        if (groupCode!= null && !groupCode.isEmpty()) {
        	query.setParameter("groupCode", groupCode);
        }
        
        if (from > 0) {
            query = query.setFirstResult(from);
        }
        if (max > 0) {
            query = query.setMaxResults(max);
        }
        return query.getResultList();
	}

	

}


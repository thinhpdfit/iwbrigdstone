package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.activiti.engine.identity.Group;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.GroupQuyen;
import com.eform.model.entities.HtQuyen;

@Repository
public class GroupQuyenDaoImpl  implements GroupQuyenDaoCustom{

	@PersistenceContext private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
    public List<GroupQuyen> getGroupQuyenFind(String groupId, int firstResult, int maxResults) {
        String sql = "";
        if ((groupId!= null)&&(groupId.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.groupId like :groupId ");
        }
        Query query = em.createQuery((("SELECT o FROM GroupQuyen o "+ sql)+"ORDER BY o.groupId "));
        if ((groupId!= null)&&(groupId.isEmpty() == false)) {
            query.setParameter("groupId", groupId);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }
	
	@SuppressWarnings("unchecked")
	@Override
    public List<GroupQuyen> getGroupQuyenFind(List<String> groups,HtQuyen htQuyen, int firstResult, int maxResults) {
        String sql = "";
        if ((groups!= null)&&(groups.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.groupId in :groups ");
        }
        if (htQuyen!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.htQuyen.id = :htQuyenId ");
        }
        Query query = em.createQuery((("SELECT o FROM GroupQuyen o "+ sql)+"ORDER BY o.groupId "));
        if ((groups!= null)&&(groups.isEmpty() == false)) {
            query.setParameter("groups", groups);
        }
        if (htQuyen!= null) {
        	query.setParameter("htQuyenId", htQuyen.getId());
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }

	@Override
    public Long getGroupQuyenFind(String groupId) {
        String sql = "";
        if ((groupId!= null)&&(groupId.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.groupId like :groupId ");
        }
        Query query = em.createQuery((("SELECT count(o) FROM GroupQuyen o "+ sql)+""));
        if ((groupId!= null)&&(groupId.isEmpty() == false)) {
            query.setParameter("groupId", groupId);
        }
        return ((Long) query.getSingleResult());
    }

	@Override
	public void deleteByGroup(Group group) {
		if(group != null && group.getId() != null){
			String sql = "DELETE from GroupQuyen o where o.groupId = :groupId";
			em.createQuery(sql).setParameter("groupId", group.getId()).executeUpdate();
		}
	}

}

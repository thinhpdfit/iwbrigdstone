package com.eform.dao;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.LoaiDanhMuc;


public interface BmODaoCustom {
	public int deleteByBieuMau(BieuMau bieuMau);
	List<BmO> getBmOFind(String ma, String ten, Long loaiO, BieuMau bieuMau, BmHang bmHang,BieuMau bieuMauCon, int firstResult, int maxResults);
    Long getBmOFind(String ma, String ten, Long loaiO, BieuMau bieuMau, BmHang bmHang);

	 public void insertWithId(List<BmO> list);
}

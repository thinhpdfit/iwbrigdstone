package com.eform.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.BaoCaoThongKe;

public interface BaoCaoThongKeDao extends CrudRepository<BaoCaoThongKe, Long>, BaoCaoThongKeDaoCustom{

}

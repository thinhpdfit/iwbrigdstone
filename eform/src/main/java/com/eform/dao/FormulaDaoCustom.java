package com.eform.dao;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.Formula;


public interface FormulaDaoCustom {
	public List<Formula> getFormulaFind(BieuMau bieuMau);
}

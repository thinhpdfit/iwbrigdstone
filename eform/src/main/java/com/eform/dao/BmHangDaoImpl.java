package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;

@Repository
public class BmHangDaoImpl implements BmHangDaoCustom{

	@PersistenceContext private EntityManager em;
	
	@Override
	public int deleteByBieuMau(BieuMau bieuMau) {
		if (bieuMau!= null) {
			Query query = em.createQuery("DELETE FROM BmHang o WHERE o.bieuMau = :bieuMau");
            query.setParameter("bieuMau", bieuMau);
            int i = query.executeUpdate();
            em.flush();
            return i;
        }
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BmHang> getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau, int firstResult,
			int maxResults) {
		String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ma like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ten like :ten ");
        }
        if (loaiHang!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiHang = :loaiHang ");
        }
        if (bieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bieuMau = :bieuMau ");
        }
        Query query = em.createQuery("SELECT o FROM BmHang o "+ sql +" order by o.thuTu ");
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma);
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten);
        }
        if (loaiHang!= null) {
            query.setParameter("loaiHang", loaiHang);
        }
        if (bieuMau!= null) {
            query.setParameter("bieuMau", bieuMau);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}

	@Override
	public Long getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau) {
		String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ma like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ten like :ten ");
        }
        if (loaiHang!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiHang = :loaiHang ");
        }
        if (bieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bieuMau = :bieuMau ");
        }
        Query query = em.createQuery("SELECT count(o) FROM BmHang o "+ sql);
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma);
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten);
        }
        if (loaiHang!= null) {
            query.setParameter("loaiHang", loaiHang);
        }
        if (bieuMau!= null) {
            query.setParameter("bieuMau", bieuMau);
        }

        return ((Long) query.getSingleResult());
	}

	@Override
	public void insertWithId(List<BmHang> list) {
		for (BmHang bmHang : list) {
			em.merge(bmHang);
		}
	}
	

}


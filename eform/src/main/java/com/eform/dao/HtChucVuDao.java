package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.HtChucVu;

@Transactional(transactionManager="eformTransactionManager")
public interface HtChucVuDao extends CrudRepository<HtChucVu, Long>, HtChucVuDaoCustom{

}

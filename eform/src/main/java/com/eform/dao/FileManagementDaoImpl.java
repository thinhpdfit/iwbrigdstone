package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;

@Repository
public class FileManagementDaoImpl implements FileManagementDaoCustom{

	@PersistenceContext private EntityManager em;

	@Override
	public FileVersion getMaxVersion(FileManagement file) {
		if(file == null){
			return null;
		}
		String sql = "select o from FileVersion o where o.fileManagement.id = :managementId and o.version is not null order by o.version desc";
		List<FileVersion> version = (List<FileVersion>) em.createQuery(sql).setParameter("managementId", file.getId()).setFirstResult(0).setMaxResults(1).getResultList();
		if(version != null && !version.isEmpty()){
			return version.get(0);
		}
		return null;
	}

	@Override
	public void deleteWithChild(FileManagement file) {
		if(file != null){
			String sql1 = "delete from Signature o where o.fileVersion.id in (select o1.id from FileVersion o1 where o1.fileManagement.id = :fileManagementId)";
			em.createQuery(sql1).setParameter("fileManagementId", file.getId()).executeUpdate();
			String sql2 = "delete from FileVersion o where o.fileManagement.id = :fileManagementId";
			em.createQuery(sql2).setParameter("fileManagementId", file.getId()).executeUpdate();
			String sql3 = "delete from FileManagement o where o.id = :fileManagementId";
			em.createQuery(sql3).setParameter("fileManagementId", file.getId()).executeUpdate();
		}
	}
}


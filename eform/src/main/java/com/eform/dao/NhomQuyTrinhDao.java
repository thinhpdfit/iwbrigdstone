package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.NhomQuyTrinh;

@Transactional(transactionManager="eformTransactionManager")
public interface NhomQuyTrinhDao extends CrudRepository<NhomQuyTrinh, Long>, NhomQuyTrinhDaoCustom{

}

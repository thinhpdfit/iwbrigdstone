package com.eform.dao;

import java.util.List;

import com.eform.model.entities.TaskButton;

public interface TaskButtonDaoCustom {
	public List<TaskButton> getTaskButton(String activitiId, String modelId);
}

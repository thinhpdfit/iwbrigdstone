package com.eform.dao;

import java.util.List;

import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.ProcinstInfo;

public interface ProcinstInfoDaoCustom {
	ProcinstInfo findByProcinstIdAndInfoKey(String procinstId, String infoKey);
}

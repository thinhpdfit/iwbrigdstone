package com.eform.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.BaoCaoThongKeDonVi;
import com.eform.model.entities.HtDonVi;

public interface BaoCaoThongKeDonViDao extends CrudRepository<BaoCaoThongKeDonVi, Long>, BaoCaoThongKeDonViDaoCustom{
	public List<BaoCaoThongKeDonVi> findByBaoCaoThongKe(BaoCaoThongKe baoCaoThongKe);
	public List<BaoCaoThongKeDonVi> findByHtDonVi(HtDonVi htDonVi);
}

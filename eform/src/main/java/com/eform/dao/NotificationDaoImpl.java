package com.eform.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.Notification;

@Repository
public class NotificationDaoImpl implements NotificationDaoCustom{

	@PersistenceContext private EntityManager em;

	@Override
	public List<Notification> getNotificationFind(List<Long> types, String fromUserId, String userId, String refKey,
			List<Long> trangThai, int firstResult, int maxResults) {
		String sql = "";
        if ((types!= null)&&(types.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.type in :types ");
        }
        if ((fromUserId!= null)&&(fromUserId.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.fromUserId = :fromUserId ");
        }
        if ((userId!= null)&&(userId.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.userId = :userId ");
        }
        if (trangThai!= null && !trangThai.isEmpty()) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        if ((refKey!= null)&&(refKey.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.refKey = :refKey ");
        }
        Query query = em.createQuery((("SELECT o FROM Notification o "+ sql)+" ORDER BY o.notificationTime Desc "));
        if ((types!= null)&&(types.isEmpty() == false)) {
        	 query.setParameter("types", types);
        }
        if ((fromUserId!= null)&&(fromUserId.isEmpty() == false)) {

       	 query.setParameter("fromUserId", fromUserId);
        }
        if ((userId!= null)&&(userId.isEmpty() == false)) {

       	 query.setParameter("userId", userId);
        }
        if (trangThai!= null && !trangThai.isEmpty()) {
       	 query.setParameter("trangThai", trangThai);
        }
        if ((refKey!= null)&&(refKey.isEmpty() == false)) {
       	 query.setParameter("refKey", refKey);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}

	@Override
	public Long getNotificationFind(List<Long> types, String fromUserId, String userId, String refKey,
			List<Long> trangThai) {
		String sql = "";
        if ((types!= null)&&(types.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.type in :types ");
        }
        if ((fromUserId!= null)&&(fromUserId.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.fromUserId = :fromUserId ");
        }
        if ((userId!= null)&&(userId.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.userId = :userId ");
        }
        if (trangThai!= null && !trangThai.isEmpty()) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        if ((refKey!= null)&&(refKey.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.refKey = :refKey ");
        }
        Query query = em.createQuery("SELECT count(o.id) FROM Notification o "+ sql);
        if ((types!= null)&&(types.isEmpty() == false)) {
        	 query.setParameter("types", types);
        }
        if ((fromUserId!= null)&&(fromUserId.isEmpty() == false)) {

       	 query.setParameter("fromUserId", fromUserId);
        }
        if ((userId!= null)&&(userId.isEmpty() == false)) {

       	 query.setParameter("userId", userId);
        }
        if (trangThai!= null && !trangThai.isEmpty()) {
       	 query.setParameter("trangThai", trangThai);
        }
        if ((refKey!= null)&&(refKey.isEmpty() == false)) {
       	 query.setParameter("refKey", refKey);
        }
        
        return ((Long) query.getSingleResult());
	}

	@Override
	public void markAsRead() {
		String sql = "update Notification o set o.trangThai = 1 where o.trangThai = 0";
		em.createQuery(sql).executeUpdate();
	}
	
	
}

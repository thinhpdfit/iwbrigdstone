package com.eform.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.TruongThongTin;

@Repository

public class BmOTruongThongTinDaoImpl implements BmOTruongThongTinDaoCustom{

	@PersistenceContext private EntityManager em;
	
	@Override
	
	public int deleteByBieuMau(BieuMau bieuMau) {
		if (bieuMau!= null) {
			Query query = em.createQuery("DELETE FROM BmOTruongThongTin o WHERE o.bieuMau = :bieuMau");
            query.setParameter("bieuMau", bieuMau);
            int i = query.executeUpdate();
            return i;
        }
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BmOTruongThongTin> getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO, TruongThongTin truongTT,
			int firstResult, int maxResults) {
		String sql = "";
        
        if (bieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bieuMau = :bieuMau ");
        }
        if (bmHang!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bmHang = :bmHang ");
        }
        
        if (bmO!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bmO = :bmO ");
        }
        
        if (truongTT != null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.truongThongTin = :truongTT ");
        }
        Query query = em.createQuery("SELECT o FROM BmOTruongThongTin o "+ sql);

        if (bieuMau!= null) {
            query.setParameter("bieuMau", bieuMau);
        }
        if (bmHang!= null) {
            query.setParameter("bmHang", bmHang);
        }

        if (bmO!= null) {
            query.setParameter("bmO", bmO);
        }
        if (truongTT!= null) {
            query.setParameter("truongTT", truongTT);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}

	@Override
	public Long getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO, TruongThongTin truongTT) {
		String sql = "";
		if (bieuMau!= null) {
			
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bieuMau = :bieuMau ");
        }
        if (bmHang!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bmHang = :bmHang ");
        }
        
        if (bmO!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bmO = :bmO ");
        }
        
        if (truongTT != null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.truongThongTin = :truongTT ");
        }
        Query query = em.createQuery("SELECT count(o) FROM BmOTruongThongTin o "+ sql);

        if (bieuMau!= null) {
            query.setParameter("bieuMau", bieuMau);
        }
        if (bmHang!= null) {
            query.setParameter("bmHang", bmHang);
        }

        if (bmO!= null) {
            query.setParameter("bmO", bmO);
        }
        if (truongTT!= null) {
            query.setParameter("truongTT", truongTT);
        }
        return (Long) query.getSingleResult();
	}

	@Override
	public List<TruongThongTin> findTruongThongtinByBieuMau(List<BieuMau> bieuMauList, Boolean tableDisplay, Boolean searchFormDisplay) {
		 String sql = "";
		 if(bieuMauList != null && !bieuMauList.isEmpty()){
			 if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
		 sql = (sql +"o.bieuMau in :bieuMauList ");
		 }
		 if (tableDisplay!= null) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"o.truongThongTin.tableDisplay = :tableDisplay ");
			 }	
		 if (searchFormDisplay!= null) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"o.truongThongTin.searchFormDisplay = :searchFormDisplay ");
			 }	
        Query query = em.createQuery((("SELECT o.truongThongTin FROM BmOTruongThongTin o "+ sql)+" ORDER BY o.truongThongTin.tableDisplayOrder, o.truongThongTin.searchFormDisplayOrder "));
        if(bieuMauList != null && !bieuMauList.isEmpty()){
        	query.setParameter("bieuMauList", bieuMauList);
        }
        if (tableDisplay!= null) {
            if(tableDisplay){
            	query.setParameter("tableDisplay", 1L);
            }else{
            	query.setParameter("tableDisplay", 0L);
            }
		 }	
        if (searchFormDisplay!= null) {
        	if(searchFormDisplay){
            	query.setParameter("searchFormDisplay", 1L);
            }else{
            	query.setParameter("searchFormDisplay", 0L);
            }
		 }	
		return query.getResultList();
	}

	@Override
	public void insertWithId(List<BmOTruongThongTin> list) {
		for (BmOTruongThongTin bmOTruongThongTin : list) {
			em.merge(bmOTruongThongTin);
		}
	}
	
	
	
}


package com.eform.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.ProcessTaskProperty;

@Transactional(transactionManager="eformTransactionManager")
public interface ProcessTaskPropertyDao extends CrudRepository<ProcessTaskProperty, Long>, ProcessTaskPropertyDaoCustom{

	public List<ProcessTaskProperty> findByDeploymentIdAndTaskDefinitionId(String dploymentId, String taskDefinitionId);

}

package com.eform.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.model.entities.DocumentOrder;

@Repository
public interface DocumentOrderDao extends CrudRepository<DocumentOrder, Long>, DocumentOrderDaoCustom{
	public List<DocumentOrder> findByDocumentBook(String documentBook);
}

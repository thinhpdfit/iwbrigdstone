package com.eform.dao;

import java.util.List;

import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;

public interface FileVersionDaoCustom {
	public List<FileVersion> getFileVersionFind(FileManagement manager);
	public void deleteWithChild(FileVersion file);
}

package com.eform.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.BmHang;


@Transactional(transactionManager="eformTransactionManager")
public interface BmHangDao extends CrudRepository<BmHang, Long>, BmHangDaoCustom{

}

package com.eform.dao;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;

public interface BieuMauDaoCustom {
	public List<BieuMau> findByCode(String ma);
	
	public List<BieuMau> findByCode(List<String> maList);
	
	public List<BieuMau> getBieuMauFind(
			String ma, 
			String ten, 
			Long loaiBieuMau, 
			List<Long> trangThai, 
			int firstResult, 
			int maxResults);

    public Long getBieuMauFind(
    		String ma, 
    		String ten, 
    		Long loaiBieuMau, 
    		List<Long> trangThai);
    
    public void updateBieuMau(
    		BieuMau obj, 
    		List<BmO> bmOList, 
    		List<BmHang> bmHangList, 
    		List<BmOTruongThongTin> oTruongTTList);
    
    public void deleteBieuMau(BieuMau obj);
    
	public List<BieuMau> getBieuMauFind(
			String ma, 
			List<String> maList, 
			String ten, 
			Long loaiBieuMau, 
			List<Long> trangThai, 
			int firstResult, 
			int maxResults);

    public Long getBieuMauFind(
    		String ma, 
    		List<String> maList, 
    		String ten, 
    		Long loaiBieuMau, 
    		List<Long> trangThai);
    public void insertWithId(List<BieuMau> list);
}

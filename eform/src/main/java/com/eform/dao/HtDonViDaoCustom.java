package com.eform.dao;

import java.util.List;

import com.eform.model.entities.HtDonVi;


public interface HtDonViDaoCustom {
	
	public List<HtDonVi> findByCode(String ma);
	
	public List<HtDonVi> getHtDonViFind(
			String ma, 
			String ten,
			HtDonVi donViCha,
			List<Long> trangThai,
			int firstResult, 
			int maxResults);
	
	public Long getHtDonViFind(
    		String ma, 
    		String ten, 
			HtDonVi donViCha,
    		List<Long> trangThai);
	
}

package com.eform.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.CauHinhHienThi;

public interface CauHinhHienThiDao extends CrudRepository<CauHinhHienThi, Long>, CauHinhHienThiDaoCustom{
	public void deleteByProcessDefId(String processDefId);
	public List<CauHinhHienThi> findByProcessDefId(String processDefId);

}

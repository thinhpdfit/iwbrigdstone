package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.ProcinstInfoDao;
import com.eform.model.entities.ProcinstInfo;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class ProcinstInfoServiceImpl implements ProcinstInfoService {

	@Autowired
	private ProcinstInfoDao ProcinstInfoDao;

	@Override
	
	public ProcinstInfo save(ProcinstInfo entity) {
		return ProcinstInfoDao.save(entity);
	}

	@Override
	
	public Iterable<ProcinstInfo> save(Iterable<ProcinstInfo> entities) {
		return ProcinstInfoDao.save(entities);
	}

	@Override
	public ProcinstInfo findOne(Long id) {
		return ProcinstInfoDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return ProcinstInfoDao.exists(id);
	}

	@Override
	public Iterable<ProcinstInfo> findAll() {
		return ProcinstInfoDao.findAll();
	}

	@Override
	public Iterable<ProcinstInfo> findAll(Iterable<Long> ids) {
		return ProcinstInfoDao.findAll(ids);
	}

	@Override
	public long count() {
		return ProcinstInfoDao.count();
	}

	@Override
	public void delete(Long id) {
		ProcinstInfoDao.delete(id);
	}

	@Override
	public void delete(ProcinstInfo entity) {
		ProcinstInfoDao.delete(entity);
	}

	@Override
	public void delete(Iterable<ProcinstInfo> entities) {
		ProcinstInfoDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		ProcinstInfoDao.deleteAll();
	}

	@Override
	public ProcinstInfo findByProcinstIdAndInfoKey(String procinstId, String infoKey) {
		
		return ProcinstInfoDao.findByProcinstIdAndInfoKey(procinstId, infoKey);
	}

}

package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.DeploymentHtDonViDao;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.ProcessTaskProperty;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class DeploymentHtDonViServiceImpl implements DeploymentHtDonViService {

	@Autowired
	private DeploymentHtDonViDao modelerHtDonViDao;
	@Autowired
	private ProcessTaskPropertyService processTaskPropertyService;

	@Override
	public DeploymentHtDonVi save(DeploymentHtDonVi entity) {
		return modelerHtDonViDao.save(entity);
	}

	@Override
	public Iterable<DeploymentHtDonVi> save(Iterable<DeploymentHtDonVi> entities) {
		return modelerHtDonViDao.save(entities);
	}

	@Override
	public DeploymentHtDonVi findOne(Long id) {
		return modelerHtDonViDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return modelerHtDonViDao.exists(id);
	}

	@Override
	public Iterable<DeploymentHtDonVi> findAll() {
		return modelerHtDonViDao.findAll();
	}

	@Override
	public Iterable<DeploymentHtDonVi> findAll(Iterable<Long> ids) {
		return modelerHtDonViDao.findAll(ids);
	}

	@Override
	public long count() {
		return modelerHtDonViDao.count();
	}

	@Override
	public void delete(Long id) {
		modelerHtDonViDao.delete(id);
	}

	@Override
	public void delete(DeploymentHtDonVi entity) {
		modelerHtDonViDao.delete(entity);
	}

	@Override
	public void delete(Iterable<DeploymentHtDonVi> entities) {
		modelerHtDonViDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		modelerHtDonViDao.deleteAll();
	}

	@Override
	public List<DeploymentHtDonVi> getDeploymentHtDonViFind(String modelerId) {
		return modelerHtDonViDao.getDeploymentHtDonViFind(modelerId);
	}

	@Override
	public void update(List<DeploymentHtDonVi> update, List<DeploymentHtDonVi> delete) {
		modelerHtDonViDao.delete(delete);
		modelerHtDonViDao.save(update);
	}

	@Override
	public List<DeploymentHtDonVi> getDeploymentHtDonViFind(String modelerId, HtDonVi donVi) {
		// TODO Auto-generated method stub
		return modelerHtDonViDao.getDeploymentHtDonViFind(modelerId, donVi);
	}

	@Override
	public void update(List<DeploymentHtDonVi> update, List<DeploymentHtDonVi> delete,
			List<ProcessTaskProperty> taskProperties) {
		modelerHtDonViDao.delete(delete);
		modelerHtDonViDao.save(update);
		processTaskPropertyService.save(taskProperties);
	}



}

package com.eform.service;

import java.util.List;

import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;

public interface FileVersionService {
	public FileVersion save(FileVersion entity);
	public Iterable<FileVersion> save(Iterable<FileVersion> entities);
	public FileVersion findOne(Long id);
	public boolean exists(Long id);
	public Iterable<FileVersion> findAll();
	public Iterable<FileVersion> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(FileVersion entity);
	public void delete(Iterable<FileVersion> entities);
	public void deleteAll();
	public void save(FileVersion version, FileManagement manager);
	
	public List<FileVersion> getFileVersionFind(FileManagement manager);
	public void deleteWithChild(FileVersion entity);
}

package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.FormulaDao;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.Formula;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class FormulaServiceImpl implements FormulaService {

	@Autowired
	private FormulaDao formulaDao;

	@Override
	public Formula save(Formula entity) {
		return formulaDao.save(entity);
	}

	@Override
	public Iterable<Formula> save(Iterable<Formula> entities) {
		return formulaDao.save(entities);
	}

	@Override
	public Formula findOne(Long id) {
		return formulaDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return formulaDao.exists(id);
	}

	@Override
	public Iterable<Formula> findAll() {
		return formulaDao.findAll();
	}

	@Override
	public Iterable<Formula> findAll(Iterable<Long> ids) {
		return formulaDao.findAll(ids);
	}

	@Override
	public long count() {
		return formulaDao.count();
	}

	@Override
	public void delete(Long id) {
		formulaDao.delete(id);
	}

	@Override
	public void delete(Formula entity) {
		formulaDao.delete(entity);
	}

	@Override
	public void delete(Iterable<Formula> entities) {
		formulaDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		formulaDao.deleteAll();
	}

	@Override
	public List<Formula> getFormulaFind(BieuMau bieuMau) {
		// TODO Auto-generated method stub
		return formulaDao.getFormulaFind(bieuMau);
	}

}

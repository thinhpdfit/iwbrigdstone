package com.eform.service;

import java.util.List;

import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.BaoCaoThongKeDonVi;
import com.eform.model.entities.HtDonVi;

public interface BaoCaoThongKeService {
	public BaoCaoThongKe save(BaoCaoThongKe entity);
	public Iterable<BaoCaoThongKe> save(Iterable<BaoCaoThongKe> entities);
	public BaoCaoThongKe findOne(Long id);
	public boolean exists(Long id);
	public Iterable<BaoCaoThongKe> findAll();
	public Iterable<BaoCaoThongKe> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(BaoCaoThongKe entity);
	public void delete(Iterable<BaoCaoThongKe> entities);
	public void deleteAll();
	public List<BaoCaoThongKe> findByCode(String ma);
	

	public List<BaoCaoThongKe> getBaoCaoThongKeFind(
			String ma, 
			String ten, 
			List<HtDonVi> htDonViList,
			List<Long> trangThai, 
			boolean laDonViTao,
			int firstResult, 
			int maxResults);
	
	public Long getBaoCaoThongKeFind(
			String ma, 
			String ten,
			List<HtDonVi> htDonViList,
			List<Long> trangThai,  
			boolean laDonViTao);
	
	public BaoCaoThongKe save(BaoCaoThongKe entity, List<BaoCaoThongKeDonVi> baoCaoThongKeDonViList);
	
	public List<BaoCaoThongKeDonVi> findByBaoCaoThongKe(BaoCaoThongKe baoCaoThongKe);
	public List<BaoCaoThongKeDonVi> findByHtDonVi(HtDonVi htDonVi);

}

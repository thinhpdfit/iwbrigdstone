package com.eform.service;

import java.util.List;

import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;

public interface TruongThongTinService {
	public TruongThongTin save(TruongThongTin entity);
	public Iterable<TruongThongTin> save(Iterable<TruongThongTin> entities);
	public TruongThongTin findOne(Long id);
	public boolean exists(Long id);
	public Iterable<TruongThongTin> findAll();
	public Iterable<TruongThongTin> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(TruongThongTin entity);
	public void delete(Iterable<TruongThongTin> entities);
	public void deleteAll();
	public List<TruongThongTin> findByCode(String ma);
	
	public List<TruongThongTin> getTruongThongTinFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc, 
			List<Long> kieuDuLieu, 
			List<String> viTriHienThi,
			int firstResult, 
			int maxResults);
	
	public Long getTruongThongTinFind(
    		String ma, 
    		String ten, 
    		List<Long> trangThai, 
    		LoaiDanhMuc loaiDanhMuc,
			List<Long> kieuDuLieu,
			List<String> viTriHienThi);
}

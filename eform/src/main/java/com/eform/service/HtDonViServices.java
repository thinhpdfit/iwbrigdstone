package com.eform.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.HtDonViDao;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.HtDonVi;
import com.google.gwt.thirdparty.guava.common.collect.Lists;

@Transactional(transactionManager="eformTransactionManager")
@Service
public class HtDonViServices {
	@Autowired
	HtDonViDao htDonViDao;

	public void update(List<HtDonVi> update,List<HtDonVi>delete){
		htDonViDao.save(update);
		htDonViDao.delete(delete);
	}
	public HtDonVi findOne(Long pk){
		return htDonViDao.findOne(pk);
	}
	public List<HtDonVi> findByCode(String ma){
		return htDonViDao.findByCode(ma);
	}
	public ArrayList<HtDonVi> getHtDonViFindAll(){
		return Lists.newArrayList(htDonViDao.findAll());
	}
	
	public List<HtDonVi> getHtDonViFind(
			String ma, 
			String ten, 
			HtDonVi donViCha,
			List<Long> trangThai,
			int firstResult, 
			int maxResults){
		return htDonViDao.getHtDonViFind(ma, ten,
				donViCha, trangThai, firstResult, maxResults);
	}
	
}

package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.HtThamSoDao;
import com.eform.model.entities.HtThamSo;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class HtThamSoServiceImpl implements HtThamSoService {

	@Autowired
	private HtThamSoDao htThamSoDao;

	@Override
	
	public HtThamSo save(HtThamSo entity) {
		return htThamSoDao.save(entity);
	}

	@Override
	
	public Iterable<HtThamSo> save(Iterable<HtThamSo> entities) {
		return htThamSoDao.save(entities);
	}

	@Override
	public HtThamSo findOne(Long id) {
		return htThamSoDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return htThamSoDao.exists(id);
	}

	@Override
	public Iterable<HtThamSo> findAll() {
		return htThamSoDao.findAll();
	}

	@Override
	public Iterable<HtThamSo> findAll(Iterable<Long> ids) {
		return htThamSoDao.findAll(ids);
	}

	@Override
	public long count() {
		return htThamSoDao.count();
	}

	@Override
	public void delete(Long id) {
		htThamSoDao.delete(id);
	}

	@Override
	public void delete(HtThamSo entity) {
		htThamSoDao.delete(entity);
	}

	@Override
	public void delete(Iterable<HtThamSo> entities) {
		htThamSoDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		htThamSoDao.deleteAll();
	}

	@Override
	public List<HtThamSo> getHtThamSoFind(String ma, List<Long> trangThai, List<Long> loai,  int firstResult, int maxResults) {
		return htThamSoDao.getHtThamSoFind(ma, trangThai, loai, firstResult, maxResults);
	}

	@Override
	public void updateList(List<HtThamSo> htThamSoList) {
		htThamSoDao.updateList(htThamSoList);
	}

	@Override
	public HtThamSo getHtThamSoFind(String ma) {
		return htThamSoDao.getHtThamSoFind(ma);
	}

	@Override
	public String getHtThamSoValue(String ma) {
		HtThamSo htThamSo = htThamSoDao.getHtThamSoFind(ma);
		if(htThamSo != null){
			return htThamSo.getGiaTri();
		}
		return "";
	}

}

package com.eform.service;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.Formula;

public interface FormulaService {
	public Formula save(Formula entity);
	public Iterable<Formula> save(Iterable<Formula> entities);
	public Formula findOne(Long id);
	public boolean exists(Long id);
	public Iterable<Formula> findAll();
	public Iterable<Formula> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(Formula entity);
	public void delete(Iterable<Formula> entities);
	public void deleteAll();
	
	public List<Formula> getFormulaFind(BieuMau bieuMau);
	
}

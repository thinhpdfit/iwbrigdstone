package com.eform.service;

import java.util.List;

import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;

public interface DanhMucService{
	public DanhMuc save(DanhMuc entity);
	public Iterable<DanhMuc> save(Iterable<DanhMuc> entities);
	public DanhMuc findOne(Long id);
	public boolean exists(Long id);
	public Iterable<DanhMuc> findAll();
	public Iterable<DanhMuc> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(DanhMuc entity);
	public void delete(Iterable<DanhMuc> entities);
	public void deleteAll();
	public List<DanhMuc> findByCode(String ma);
	public List<DanhMuc> getDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc,
			DanhMuc danhMuc, 
			int firstResult, 
			int maxResults);

	public Long getDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc, 
			DanhMuc danhMuc);
}

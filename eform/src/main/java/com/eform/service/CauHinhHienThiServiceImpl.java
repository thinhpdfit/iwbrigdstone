package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.CauHinhHienThiDao;
import com.eform.model.entities.CauHinhHienThi;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class CauHinhHienThiServiceImpl implements CauHinhHienThiService {

	@Autowired
	private CauHinhHienThiDao cauHinhHienThiDao;

	@Override
	
	public CauHinhHienThi save(CauHinhHienThi entity) {
		return cauHinhHienThiDao.save(entity);
	}

	@Override
	
	public Iterable<CauHinhHienThi> save(Iterable<CauHinhHienThi> entities) {
		return cauHinhHienThiDao.save(entities);
	}

	@Override
	public CauHinhHienThi findOne(Long id) {
		return cauHinhHienThiDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return cauHinhHienThiDao.exists(id);
	}

	@Override
	public Iterable<CauHinhHienThi> findAll() {
		return cauHinhHienThiDao.findAll();
	}

	@Override
	public Iterable<CauHinhHienThi> findAll(Iterable<Long> ids) {
		return cauHinhHienThiDao.findAll(ids);
	}

	@Override
	public long count() {
		return cauHinhHienThiDao.count();
	}

	@Override
	public void delete(Long id) {
		cauHinhHienThiDao.delete(id);
	}

	@Override
	public void delete(CauHinhHienThi entity) {
		cauHinhHienThiDao.delete(entity);
	}

	@Override
	public void delete(Iterable<CauHinhHienThi> entities) {
		cauHinhHienThiDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		cauHinhHienThiDao.deleteAll();
	}

	@Override
	public void save(List<CauHinhHienThi> list, String processDefId) {
		cauHinhHienThiDao.deleteByProcessDefId(processDefId);
		cauHinhHienThiDao.save(list);
	}

	@Override
	public List<CauHinhHienThi> findByProcessDefId(String processDefId) {
		return cauHinhHienThiDao.findByProcessDefId(processDefId);
	}


}

package com.eform.service;

import java.util.List;

import com.eform.model.entities.NhomQuyTrinh;

public interface NhomQuyTrinhService {
	public NhomQuyTrinh save(NhomQuyTrinh entity);
	public Iterable<NhomQuyTrinh> save(Iterable<NhomQuyTrinh> entities);
	public NhomQuyTrinh findOne(Long id);
	public boolean exists(Long id);
	public Iterable<NhomQuyTrinh> findAll();
	public Iterable<NhomQuyTrinh> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(NhomQuyTrinh entity);
	public void delete(Iterable<NhomQuyTrinh> entities);
	public void deleteAll();
	public List<NhomQuyTrinh> findByCode(String ma);
    public List<NhomQuyTrinh> getNhomQuyTrinhFind(String ma,
    		String ten, 
    		List<Long> trangThai, 
    		NhomQuyTrinh nhomQuyTrinh, 
    		int firstResult, 
    		int maxResults);

    public Long getNhomQuyTrinhFind(String ma,
    		String ten, 
    		List<Long> trangThai, 
    		NhomQuyTrinh nhomQuyTrinh);
}

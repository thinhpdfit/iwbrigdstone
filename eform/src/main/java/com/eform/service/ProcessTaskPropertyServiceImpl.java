package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.ProcessTaskPropertyDao;
import com.eform.model.entities.ProcessTaskProperty;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class ProcessTaskPropertyServiceImpl implements ProcessTaskPropertyService {

	@Autowired
	private ProcessTaskPropertyDao processTaskPropertyDao;

	@Override
	public ProcessTaskProperty save(ProcessTaskProperty entity) {
		return processTaskPropertyDao.save(entity);
	}

	@Override
	public Iterable<ProcessTaskProperty> save(Iterable<ProcessTaskProperty> entities) {
		return processTaskPropertyDao.save(entities);
	}

	@Override
	public ProcessTaskProperty findOne(Long id) {
		return processTaskPropertyDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return processTaskPropertyDao.exists(id);
	}

	@Override
	public Iterable<ProcessTaskProperty> findAll() {
		return processTaskPropertyDao.findAll();
	}

	@Override
	public Iterable<ProcessTaskProperty> findAll(Iterable<Long> ids) {
		return processTaskPropertyDao.findAll(ids);
	}

	@Override
	public long count() {
		return processTaskPropertyDao.count();
	}

	@Override
	public void delete(Long id) {
		processTaskPropertyDao.delete(id);
	}

	@Override
	public void delete(ProcessTaskProperty entity) {
		processTaskPropertyDao.delete(entity);
	}

	@Override
	public void delete(Iterable<ProcessTaskProperty> entities) {
		processTaskPropertyDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		processTaskPropertyDao.deleteAll();
	}

	@Override
	public List<ProcessTaskProperty> findByDeploymentIdAndTaskDefinitionId(String dploymentId, String taskDefinitionId) {
		// TODO Auto-generated method stub
		return processTaskPropertyDao.findByDeploymentIdAndTaskDefinitionId(dploymentId, taskDefinitionId);
	}

}

package com.eform.service;

import java.util.List;

import com.eform.model.entities.ProcessTaskProperty;

public interface ProcessTaskPropertyService {
	public ProcessTaskProperty save(ProcessTaskProperty entity);
	public Iterable<ProcessTaskProperty> save(Iterable<ProcessTaskProperty> entities);
	public ProcessTaskProperty findOne(Long id);
	public boolean exists(Long id);
	public Iterable<ProcessTaskProperty> findAll();
	public Iterable<ProcessTaskProperty> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(ProcessTaskProperty entity);
	public void delete(Iterable<ProcessTaskProperty> entities);
	public void deleteAll();
	
	public List<ProcessTaskProperty> findByDeploymentIdAndTaskDefinitionId(String dploymentId, String taskDefinitionId);

}

package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.ChatMessageDao;
import com.eform.model.entities.ChatGroup;
import com.eform.model.entities.ChatMessage;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class ChatMessageServiceImpl implements ChatMessageService {

	@Autowired
	private ChatMessageDao chatMessageDao;

	@Override
	public ChatMessage save(ChatMessage entity) {
		return chatMessageDao.save(entity);
	}

	@Override
	public Iterable<ChatMessage> save(Iterable<ChatMessage> entities) {
		return chatMessageDao.save(entities);
	}

	@Override
	public ChatMessage findOne(Long id) {
		return chatMessageDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return chatMessageDao.exists(id);
	}

	@Override
	public Iterable<ChatMessage> findAll() {
		return chatMessageDao.findAll();
	}

	@Override
	public Iterable<ChatMessage> findAll(Iterable<Long> ids) {
		return chatMessageDao.findAll(ids);
	}

	@Override
	public long count() {
		return chatMessageDao.count();
	}

	@Override
	public void delete(Long id) {
		chatMessageDao.delete(id);
	}

	@Override
	public void delete(ChatMessage entity) {
		chatMessageDao.delete(entity);
	}

	@Override
	public void delete(Iterable<ChatMessage> entities) {
		chatMessageDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		chatMessageDao.deleteAll();
	}
	
	@Override
	public List<ChatMessage> find(String userId, ChatGroup group, int from, int max) {
		// TODO Auto-generated method stub
		return chatMessageDao.find(userId, group, from, max);
	}


}

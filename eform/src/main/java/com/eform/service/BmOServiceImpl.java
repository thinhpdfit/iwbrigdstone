package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.BmODao;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class BmOServiceImpl implements BmOService {

	@Autowired
	private BmODao bmODao;

	@Override
	public BmO save(BmO entity) {
		return bmODao.save(entity);
	}

	@Override
	public Iterable<BmO> save(Iterable<BmO> entities) {
		return bmODao.save(entities);
	}

	@Override
	public BmO findOne(Long id) {
		return bmODao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return bmODao.exists(id);
	}

	@Override
	public Iterable<BmO> findAll() {
		return bmODao.findAll();
	}

	@Override
	public Iterable<BmO> findAll(Iterable<Long> ids) {
		return bmODao.findAll(ids);
	}

	@Override
	public long count() {
		return bmODao.count();
	}

	@Override
	public void delete(Long id) {
		bmODao.delete(id);
	}

	@Override
	public void delete(BmO entity) {
		bmODao.delete(entity);
	}

	@Override
	public void delete(Iterable<BmO> entities) {
		bmODao.delete(entities);

	}

	@Override
	public void deleteAll() {
		bmODao.deleteAll();
	}

	@Override
	public int deleteByBieuMau(BieuMau bieuMau) {
		// TODO Auto-generated method stub
		return bmODao.deleteByBieuMau(bieuMau);
	}

	@Override
	public List<BmO> getBmOFind(String ma, String ten, Long loaiO, BieuMau bieuMau, BmHang bmHang, BieuMau bieuMauCon,  int firstResult,
			int maxResults) {
		// TODO Auto-generated method stub
		return bmODao.getBmOFind(ma, ten, loaiO, bieuMau, bmHang,bieuMauCon, firstResult, maxResults);
	}

	@Override
	public Long getBmOFind(String ma, String ten, Long loaiO, BieuMau bieuMau, BmHang bmHang) {
		// TODO Auto-generated method stub
		return bmODao.getBmOFind(ma, ten, loaiO, bieuMau, bmHang);
	}

}

package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.TaskButtonDao;
import com.eform.model.entities.TaskButton;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class TaskButtonServiceImpl implements TaskButtonService {

	@Autowired
	private TaskButtonDao taskButtonDao;


	@Override
	public TaskButton save(TaskButton entity) {
		return taskButtonDao.save(entity);
	}

	@Override
	public Iterable<TaskButton> save(Iterable<TaskButton> entities) {
		return taskButtonDao.save(entities);
	}

	@Override
	public TaskButton findOne(Long id) {
		return taskButtonDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return taskButtonDao.exists(id);
	}

	@Override
	public Iterable<TaskButton> findAll() {
		return taskButtonDao.findAll();
	}

	@Override
	public Iterable<TaskButton> findAll(Iterable<Long> ids) {
		return taskButtonDao.findAll(ids);
	}

	@Override
	public long count() {
		return taskButtonDao.count();
	}

	@Override
	public void delete(Long id) {
		taskButtonDao.delete(id);
	}

	@Override
	public void delete(TaskButton entity) {
		taskButtonDao.delete(entity);
	}

	@Override
	public void delete(Iterable<TaskButton> entities) {
		taskButtonDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		taskButtonDao.deleteAll();
	}

	@Override
	public List<TaskButton> getTaskButton(String activitiId, String modelId) {
		// TODO Auto-generated method stub
		return taskButtonDao.getTaskButton(activitiId, modelId);
	}

	}

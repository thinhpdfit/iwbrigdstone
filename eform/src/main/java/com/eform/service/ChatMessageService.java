package com.eform.service;

import java.util.List;

import com.eform.model.entities.ChatGroup;
import com.eform.model.entities.ChatMessage;

public interface ChatMessageService {
	public ChatMessage save(ChatMessage entity);
	public Iterable<ChatMessage> save(Iterable<ChatMessage> entities);
	public ChatMessage findOne(Long id);
	public boolean exists(Long id);
	public Iterable<ChatMessage> findAll();
	public Iterable<ChatMessage> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(ChatMessage entity);
	public void delete(Iterable<ChatMessage> entities);
	public void deleteAll();
	
	public List<com.eform.model.entities.ChatMessage> find(String userId, ChatGroup group, int from, int max);
}

package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.NotificationDao;
import com.eform.model.entities.Notification;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	private NotificationDao notificationDao;

	@Override
	
	public Notification save(Notification entity) {
		return notificationDao.save(entity);
	}

	@Override
	public Iterable<Notification> save(Iterable<Notification> entities) {
		return notificationDao.save(entities);
	}

	@Override
	public Notification findOne(Long id) {
		return notificationDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return notificationDao.exists(id);
	}

	@Override
	public Iterable<Notification> findAll() {
		return notificationDao.findAll();
	}

	@Override
	public Iterable<Notification> findAll(Iterable<Long> ids) {
		return notificationDao.findAll(ids);
	}

	@Override
	public long count() {
		return notificationDao.count();
	}

	@Override
	public void delete(Long id) {
		notificationDao.delete(id);
	}

	@Override
	public void delete(Notification entity) {
		notificationDao.delete(entity);
	}

	@Override
	public void delete(Iterable<Notification> entities) {
		notificationDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		notificationDao.deleteAll();
	}

	@Override
	public List<Notification> getNotificationFind(List<Long> types, String fromUserId, String userId, String refKey,
			List<Long> trangThai, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return notificationDao.getNotificationFind(types, fromUserId, userId, refKey, trangThai, firstResult, maxResults);
	}

	@Override
	public Long getNotificationFind(List<Long> types, String fromUserId, String userId, String refKey,
			List<Long> trangThai) {
		// TODO Auto-generated method stub
		return notificationDao.getNotificationFind(types, fromUserId, userId, refKey, trangThai);
	}

	@Override
	public void markAsRead() {
		notificationDao.markAsRead();
	}

}

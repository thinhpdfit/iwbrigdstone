package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.BmHangDao;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class BmHangServiceImpl implements BmHangService {

	@Autowired
	private BmHangDao bmHangDao;

	@Override
	public BmHang save(BmHang entity) {
		return bmHangDao.save(entity);
	}

	@Override
	public Iterable<BmHang> save(Iterable<BmHang> entities) {
		return bmHangDao.save(entities);
	}

	@Override
	public BmHang findOne(Long id) {
		return bmHangDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return bmHangDao.exists(id);
	}

	@Override
	public Iterable<BmHang> findAll() {
		return bmHangDao.findAll();
	}

	@Override
	public Iterable<BmHang> findAll(Iterable<Long> ids) {
		return bmHangDao.findAll(ids);
	}

	@Override
	public long count() {
		return bmHangDao.count();
	}

	@Override
	public void delete(Long id) {
		bmHangDao.delete(id);
	}

	@Override
	public void delete(BmHang entity) {
		bmHangDao.delete(entity);
	}

	@Override
	public void delete(Iterable<BmHang> entities) {
		bmHangDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		bmHangDao.deleteAll();
	}

	@Override
	public int deleteByBieuMau(BieuMau bieuMau) {
		// TODO Auto-generated method stub
		return bmHangDao.deleteByBieuMau(bieuMau);
	}

	@Override
	public List<BmHang> getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau, int firstResult,
			int maxResults) {
		// TODO Auto-generated method stub
		return bmHangDao.getBmHangFind(ma, ten, loaiHang, bieuMau, firstResult, maxResults);
	}

	@Override
	public Long getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau) {
		// TODO Auto-generated method stub
		return bmHangDao.getBmHangFind(ma, ten, loaiHang, bieuMau);
	}

}

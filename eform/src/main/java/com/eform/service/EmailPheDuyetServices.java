package com.eform.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.EmailPheDuyetDao;
import com.eform.model.entities.EmailPheDuyet;

@Transactional(transactionManager="eformTransactionManager")
@Service
public class EmailPheDuyetServices {
	@Autowired
	EmailPheDuyetDao emailPheDuyetDao;

	public EmailPheDuyet update(EmailPheDuyet update){
		return emailPheDuyetDao.save(update);
	}
	
	public EmailPheDuyet getEmailPheDuyetFind(Long id){
		return emailPheDuyetDao.findOne(id);
	}
	
	public EmailPheDuyet getEmailPheDuyetFind(String refCode){
		return emailPheDuyetDao.findByRefCode(refCode);
	}
	
	public EmailPheDuyet getEmailPheDuyetFind(Long id, String refCode, Long trangThai){
		return emailPheDuyetDao.findByRefCode(id, refCode, trangThai);
	}
	
}

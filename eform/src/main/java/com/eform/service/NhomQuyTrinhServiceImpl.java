package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.NhomQuyTrinhDao;
import com.eform.model.entities.NhomQuyTrinh;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class NhomQuyTrinhServiceImpl implements NhomQuyTrinhService {

	@Autowired
	private NhomQuyTrinhDao nhomQuyTrinhDao;

	@Override
	public NhomQuyTrinh save(NhomQuyTrinh entity) {
		return nhomQuyTrinhDao.save(entity);
	}

	@Override
	public Iterable<NhomQuyTrinh> save(Iterable<NhomQuyTrinh> entities) {
		return nhomQuyTrinhDao.save(entities);
	}

	@Override
	public NhomQuyTrinh findOne(Long id) {
		return nhomQuyTrinhDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return nhomQuyTrinhDao.exists(id);
	}

	@Override
	public Iterable<NhomQuyTrinh> findAll() {
		return nhomQuyTrinhDao.findAll();
	}

	@Override
	public Iterable<NhomQuyTrinh> findAll(Iterable<Long> ids) {
		return nhomQuyTrinhDao.findAll(ids);
	}

	@Override
	public long count() {
		return nhomQuyTrinhDao.count();
	}

	@Override
	public void delete(Long id) {
		nhomQuyTrinhDao.delete(id);
	}

	@Override
	public void delete(NhomQuyTrinh entity) {
		nhomQuyTrinhDao.delete(entity);
	}

	@Override
	public void delete(Iterable<NhomQuyTrinh> entities) {
		nhomQuyTrinhDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		nhomQuyTrinhDao.deleteAll();
	}

	@Override
	public List<NhomQuyTrinh> findByCode(String ma) {
		// TODO Auto-generated method stub
		return nhomQuyTrinhDao.findByCode(ma);
	}

	@Override
	public List<NhomQuyTrinh> getNhomQuyTrinhFind(String ma, String ten, List<Long> trangThai,
			NhomQuyTrinh nhomQuyTrinh, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return nhomQuyTrinhDao.getNhomQuyTrinhFind(ma, ten, trangThai, nhomQuyTrinh, firstResult, maxResults);
	}

	@Override
	public Long getNhomQuyTrinhFind(String ma, String ten, List<Long> trangThai, NhomQuyTrinh nhomQuyTrinh) {
		return nhomQuyTrinhDao.getNhomQuyTrinhFind(ma, ten, trangThai, nhomQuyTrinh);
	}

}

package com.eform.service;

import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;

public interface FileManagementService {
	public FileManagement save(FileManagement entity);
	public Iterable<FileManagement> save(Iterable<FileManagement> entities);
	public FileManagement findOne(Long id);
	public boolean exists(Long id);
	public Iterable<FileManagement> findAll();
	public Iterable<FileManagement> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(FileManagement entity);
	public void delete(Iterable<FileManagement> entities);
	public void deleteAll();
	public FileVersion getMaxVersion(FileManagement file);
	public void deleteWithChild(FileManagement entity);
	
}

package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.BaoCaoThongKeDao;
import com.eform.dao.BaoCaoThongKeDonViDao;
import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.BaoCaoThongKeDonVi;
import com.eform.model.entities.HtDonVi;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class BaoCaoThongKeServiceImpl implements BaoCaoThongKeService {

	@Autowired
	private BaoCaoThongKeDao baoCaoThongKeDao;
	@Autowired
	private BaoCaoThongKeDonViDao baoCaoThongKeDonViDao;

	@Override
	public BaoCaoThongKe save(BaoCaoThongKe entity) {
		return baoCaoThongKeDao.save(entity);
	}

	@Override
	public Iterable<BaoCaoThongKe> save(Iterable<BaoCaoThongKe> entities) {
		return baoCaoThongKeDao.save(entities);
	}

	@Override
	public BaoCaoThongKe findOne(Long id) {
		return baoCaoThongKeDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return baoCaoThongKeDao.exists(id);
	}

	@Override
	public Iterable<BaoCaoThongKe> findAll() {
		return baoCaoThongKeDao.findAll();
	}

	@Override
	public Iterable<BaoCaoThongKe> findAll(Iterable<Long> ids) {
		return baoCaoThongKeDao.findAll(ids);
	}

	@Override
	public long count() {
		return baoCaoThongKeDao.count();
	}

	@Override
	public void delete(Long id) {
		baoCaoThongKeDao.delete(id);
	}

	@Override
	public void delete(BaoCaoThongKe entity) {
		baoCaoThongKeDao.delete(entity);
	}

	@Override
	public void delete(Iterable<BaoCaoThongKe> entities) {
		baoCaoThongKeDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		baoCaoThongKeDao.deleteAll();
	}

	@Override
	public List<BaoCaoThongKe> findByCode(String ma) {
		return baoCaoThongKeDao.findByCode(ma);
	}

	@Override
	public List<BaoCaoThongKe> getBaoCaoThongKeFind(String ma, String ten,
			List<HtDonVi> htDonViList, List<Long> trangThai,
			boolean laDonViTao, int firstResult,
			int maxResults) {
		return baoCaoThongKeDao.getBaoCaoThongKeFind(ma, ten,htDonViList, trangThai, laDonViTao, firstResult, maxResults);
	}

	@Override
	public Long getBaoCaoThongKeFind(String ma, String ten,
			List<HtDonVi> htDonViList, List<Long> trangThai,
			boolean laDonViTao) {
		// TODO Auto-generated method stub
		return baoCaoThongKeDao.getBaoCaoThongKeFind(ma, ten,htDonViList, trangThai, laDonViTao);
	}

	@Override
	public BaoCaoThongKe save(BaoCaoThongKe entity, List<BaoCaoThongKeDonVi> baoCaoThongKeDonViList) {
		baoCaoThongKeDao.save(entity);
		baoCaoThongKeDonViDao.deleteByBaoCaoThongKe(entity);
		baoCaoThongKeDonViDao.save(baoCaoThongKeDonViList);
		return null;
	}

	@Override
	public List<BaoCaoThongKeDonVi> findByBaoCaoThongKe(BaoCaoThongKe baoCaoThongKe) {
		// TODO Auto-generated method stub
		return baoCaoThongKeDonViDao.findByBaoCaoThongKe(baoCaoThongKe);
	}

	@Override
	public List<BaoCaoThongKeDonVi> findByHtDonVi(HtDonVi htDonVi) {
		// TODO Auto-generated method stub
		return baoCaoThongKeDonViDao.findByHtDonVi(htDonVi);
	}


}

package com.eform.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.FileManagementDao;
import com.eform.dao.FileVersionDao;
import com.eform.dao.SignatureDao;
import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class FileManagementServiceImpl implements FileManagementService {

	@Autowired
	private FileManagementDao fileManagementDao;

	@Autowired
	private FileVersionDao FileVersionDao;
	@Autowired
	private SignatureDao signatureDao;

	@Override
	public FileManagement save(FileManagement entity) {
		return fileManagementDao.save(entity);
	}

	@Override
	public Iterable<FileManagement> save(Iterable<FileManagement> entities) {
		return fileManagementDao.save(entities);
	}

	@Override
	public FileManagement findOne(Long id) {
		return fileManagementDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return fileManagementDao.exists(id);
	}

	@Override
	public Iterable<FileManagement> findAll() {
		return fileManagementDao.findAll();
	}

	@Override
	public Iterable<FileManagement> findAll(Iterable<Long> ids) {
		return fileManagementDao.findAll(ids);
	}

	@Override
	public long count() {
		return fileManagementDao.count();
	}

	@Override
	public void delete(Long id) {
		fileManagementDao.delete(id);
	}

	@Override
	public void delete(FileManagement entity) {
		fileManagementDao.delete(entity);
	}

	@Override
	public void delete(Iterable<FileManagement> entities) {
		fileManagementDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		fileManagementDao.deleteAll();
	}

	@Override
	public FileVersion getMaxVersion(FileManagement file) {
		return fileManagementDao.getMaxVersion(file);
	}

	@Override
	public void deleteWithChild(FileManagement entity) {
		fileManagementDao.deleteWithChild(entity);
	}

	}

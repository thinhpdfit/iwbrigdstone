package com.eform.service;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.TruongThongTin;

public interface BmOTruongThongTinService {
	public BmOTruongThongTin save(BmOTruongThongTin entity);
	public Iterable<BmOTruongThongTin> save(Iterable<BmOTruongThongTin> entities);
	public BmOTruongThongTin findOne(Long id);
	public boolean exists(Long id);
	public Iterable<BmOTruongThongTin> findAll();
	public Iterable<BmOTruongThongTin> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(BmOTruongThongTin entity);
	public void delete(Iterable<BmOTruongThongTin> entities);
	public void deleteAll();
	public int deleteByBieuMau(BieuMau bieuMau);
    List<BmOTruongThongTin> getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO , TruongThongTin truongTT, int firstResult, int maxResults);
    Long getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO, TruongThongTin truongTT);
    List<TruongThongTin> findTruongThongtinByBieuMau(List<BieuMau> bieuMauList, Boolean tableDisplay, Boolean searchFormDisPlay);
}

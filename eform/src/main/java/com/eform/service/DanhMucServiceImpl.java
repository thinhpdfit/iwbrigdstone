package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.DanhMucDao;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class DanhMucServiceImpl implements DanhMucService{

	@Autowired
	private DanhMucDao danhMucDao;

	@Override
	public DanhMuc save(DanhMuc entity) {
		return danhMucDao.save(entity);
	}

	@Override
	public Iterable<DanhMuc> save(Iterable<DanhMuc> entities) {
		return danhMucDao.save(entities);
	}

	@Override
	public DanhMuc findOne(Long id) {
		return danhMucDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return danhMucDao.exists(id);
	}

	@Override
	public Iterable<DanhMuc> findAll() {
		return danhMucDao.findAll();
	}

	@Override
	public Iterable<DanhMuc> findAll(Iterable<Long> ids) {
		return danhMucDao.findAll(ids);
	}

	@Override
	public long count() {
		return danhMucDao.count();
	}

	@Override
	public void delete(Long id) {
		danhMucDao.delete(id);
	}

	@Override
	public void delete(DanhMuc entity) {
		danhMucDao.delete(entity);
	}

	@Override
	public void delete(Iterable<DanhMuc> entities) {
		danhMucDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		danhMucDao.deleteAll();
	}

	@Override
	public List<DanhMuc> findByCode(String ma) {
		// TODO Auto-generated method stub
		return danhMucDao.findByCode(ma);
	}

	@Override
	public List<DanhMuc> getDanhMucFind(String ma, String ten, List<Long> trangThai, LoaiDanhMuc loaiDanhMuc,
			DanhMuc danhMuc, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return danhMucDao.getDanhMucFind(ma, ten, trangThai, loaiDanhMuc, danhMuc, firstResult, maxResults);
	}

	@Override
	public Long getDanhMucFind(String ma, String ten, List<Long> trangThai, LoaiDanhMuc loaiDanhMuc, DanhMuc danhMuc) {
		// TODO Auto-generated method stub
		return danhMucDao.getDanhMucFind(ma, ten, trangThai, loaiDanhMuc, danhMuc);
	}
}

package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.DanhMucDao;
import com.eform.dao.LoaiDanhMucDao;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;

@Transactional(transactionManager="eformTransactionManager")
@Service
public class LoaiDanhMucServiceImpl implements LoaiDanhMucService {

	@Autowired
	private LoaiDanhMucDao loaiDanhMucDao;
	@Autowired
	private DanhMucDao danhMucDao;
	
	@Override
	public LoaiDanhMuc save(LoaiDanhMuc entity) {
		return loaiDanhMucDao.save(entity);
	}

	@Override
	public Iterable<LoaiDanhMuc> save(Iterable<LoaiDanhMuc> entities) {
		
		return loaiDanhMucDao.save(entities);
	}

	@Override
	public LoaiDanhMuc findOne(Long id) {
		
		return loaiDanhMucDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		
		return loaiDanhMucDao.exists(id);
	}

	@Override
	public Iterable<LoaiDanhMuc> findAll() {
		
		return loaiDanhMucDao.findAll();
	}

	@Override
	public Iterable<LoaiDanhMuc> findAll(Iterable<Long> ids) {
		
		return loaiDanhMucDao.findAll(ids);
	}

	@Override
	public long count() {
		
		return loaiDanhMucDao.count();
	}

	@Override
	public void delete(Long id) {
		loaiDanhMucDao.delete(id);

	}

	@Override
	public void delete(LoaiDanhMuc entity) {
		
		loaiDanhMucDao.delete(entity);
	}

	@Override
	public void delete(Iterable<LoaiDanhMuc> entities) {
		loaiDanhMucDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		loaiDanhMucDao.deleteAll();

	}

	@Override
	public List<LoaiDanhMuc> findByCode(String ma) {
		
		return loaiDanhMucDao.findByCode(ma);
	}

	@Override
	public List<LoaiDanhMuc> getLoaiDanhMucFind(String ma, String ten, List<Long> trangThai, LoaiDanhMuc loaiDanhMuc,
			int firstResult, int maxResults) {
		
		return loaiDanhMucDao.getLoaiDanhMucFind(ma, ten, trangThai, loaiDanhMuc, firstResult, maxResults);
	}

	@Override
	public Long getLoaiDanhMucFind(String ma, String ten, List<Long> trangThai, LoaiDanhMuc loaiDanhMuc) {
		
		return loaiDanhMucDao.getLoaiDanhMucFind(ma, ten, trangThai, loaiDanhMuc);
	}

	@Override
	public LoaiDanhMuc save(LoaiDanhMuc entity, List<DanhMuc> danhMucList) {
		
		LoaiDanhMuc loaiDm = loaiDanhMucDao.save(entity);
		if(danhMucList != null && !danhMucList.isEmpty()){
			danhMucDao.save(danhMucList);
		}
		return loaiDm;
	}

	@Override
	public void insertWithId(List<LoaiDanhMuc> loaiDanhMuc) {
		loaiDanhMucDao.insertWithId(loaiDanhMuc);
	}
	

}

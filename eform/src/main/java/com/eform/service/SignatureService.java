package com.eform.service;

import java.util.List;

import com.eform.model.entities.FileVersion;
import com.eform.model.entities.Signature;

public interface SignatureService {
	public Signature save(Signature entity);
	public Iterable<Signature> save(Iterable<Signature> entities);
	public Signature findOne(Long id);
	public boolean exists(Long id);
	public Iterable<Signature> findAll();
	public Iterable<Signature> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(Signature entity);
	public void delete(Iterable<Signature> entities);
	public void deleteAll();
	
	public boolean checkSigned(FileVersion file, String userId);
	public List<Signature> getSignatureFind(FileVersion file);
	public void save(FileVersion fileVersion, Signature sign);
	
}

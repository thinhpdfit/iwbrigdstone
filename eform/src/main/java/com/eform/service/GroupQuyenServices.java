package com.eform.service;
 
import java.util.List;

import org.activiti.engine.identity.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.GroupQuyenDao;
import com.eform.model.entities.GroupQuyen;
import com.eform.model.entities.HtQuyen;
import com.google.gwt.thirdparty.guava.common.collect.Lists;

@Transactional(transactionManager="eformTransactionManager")
@Service
public class GroupQuyenServices {
	@Autowired
	GroupQuyenDao groupQuyenDao;

	public void update(List<GroupQuyen> update,List<GroupQuyen>delete){
		groupQuyenDao.save(update);
		groupQuyenDao.delete(delete);
	}
	
	public void update(Group group, List<GroupQuyen> update){
		groupQuyenDao.deleteByGroup(group);
		groupQuyenDao.save(update);
	}
	
	public List<GroupQuyen> getGroupQuyenFindAll(){
		return Lists.newArrayList(groupQuyenDao.findAll());
	}
	
	public List<GroupQuyen> getGroupQuyenFind(
			String groupId,
			int firstResult, 
			int maxResults){
		return groupQuyenDao.getGroupQuyenFind(groupId,firstResult,maxResults);
	}
	
	public List<GroupQuyen> getGroupQuyenFind(
			List<String> groupId,
			HtQuyen htQuyen,
			int firstResult, 
			int maxResults){
		return groupQuyenDao.getGroupQuyenFind(groupId,htQuyen,firstResult,maxResults);
	}
	
	
	public Long getGroupQuyenFind(
    		String groupId){
		return groupQuyenDao.getGroupQuyenFind(groupId);
	}
	
}

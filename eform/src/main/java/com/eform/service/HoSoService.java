package com.eform.service;

import java.util.List;
import java.util.Map;

import com.eform.model.entities.EmailPheDuyet;
import com.eform.model.entities.HoSo;

public interface HoSoService {
	public HoSo save(HoSo entity);
	public HoSo save(HoSo entity, EmailPheDuyet emailPheDuyet);
	public Iterable<HoSo> save(Iterable<HoSo> entities);
	public HoSo findOne(Long id);
	public boolean exists(Long id);
	public Iterable<HoSo> findAll();
	public Iterable<HoSo> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(HoSo entity);
	public void delete(Iterable<HoSo> entities);
	public void deleteAll();
	public List<HoSo> getHoSoFind(
			String ma, 
			List<Long> trangThai, 
			String taskId,
			String processInstanceId,
			int firstResult, 
			int maxResults);
	
	public List<HoSo> getHoSoFind(List<String> processInstanceIdList);
	
	public List<Map<String, Object>> getReportResult(String sql);
	
	public List<Map<String, Object>> getReportResult(String sql, Object[] objParams, int[] types);

	public List<Map<String, Object>> getHoSoJsonByKey(String sql, Object[] objParams, int[] types);

	public List<Map<String, Object>> getReportResult(String sql, Map<String, Object> paramMap);
	
}

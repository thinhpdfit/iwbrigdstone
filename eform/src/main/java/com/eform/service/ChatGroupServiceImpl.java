package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.ChatGroupDao;
import com.eform.model.entities.ChatGroup;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class ChatGroupServiceImpl implements ChatGroupService {

	@Autowired
	private ChatGroupDao chatGroupDao;

	@Override
	public ChatGroup save(ChatGroup entity) {
		return chatGroupDao.save(entity);
	}

	@Override
	public Iterable<ChatGroup> save(Iterable<ChatGroup> entities) {
		return chatGroupDao.save(entities);
	}

	@Override
	public ChatGroup findOne(Long id) {
		return chatGroupDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return chatGroupDao.exists(id);
	}

	@Override
	public Iterable<ChatGroup> findAll() {
		return chatGroupDao.findAll();
	}

	@Override
	public Iterable<ChatGroup> findAll(Iterable<Long> ids) {
		return chatGroupDao.findAll(ids);
	}

	@Override
	public long count() {
		return chatGroupDao.count();
	}

	@Override
	public void delete(Long id) {
		chatGroupDao.delete(id);
	}

	@Override
	public void delete(ChatGroup entity) {
		chatGroupDao.delete(entity);
	}

	@Override
	public void delete(Iterable<ChatGroup> entities) {
		chatGroupDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		chatGroupDao.deleteAll();
	}

	@Override
	public List<ChatGroup> find(String userId, String groupCode, int from, int max) {
		// TODO Auto-generated method stub
		return chatGroupDao.find(userId, groupCode, from, max);
	}


}

package com.eform.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.EmailPheDuyetDao;
import com.eform.dao.HoSoDao;
import com.eform.model.entities.EmailPheDuyet;
import com.eform.model.entities.HoSo;

@Transactional(transactionManager="eformTransactionManager")
@Service
public class HoSoServiceImpl implements HoSoService {

	@Autowired
	private HoSoDao hoSoDao;

	@Autowired
	private EmailPheDuyetDao emailPheDuyetDao;

	@Override
	public HoSo save(HoSo entity) {
		return hoSoDao.save(entity);
	}
	
	@Override
	public HoSo save(HoSo entity, EmailPheDuyet emailPheDuyet){
		emailPheDuyetDao.save(emailPheDuyet);
		return hoSoDao.save(entity);
	}

	@Override
	public Iterable<HoSo> save(Iterable<HoSo> entities) {
		return hoSoDao.save(entities);
	}

	@Override
	public HoSo findOne(Long id) {
		return hoSoDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return hoSoDao.exists(id);
	}

	@Override
	public Iterable<HoSo> findAll() {
		return hoSoDao.findAll();
	}

	@Override
	public Iterable<HoSo> findAll(Iterable<Long> ids) {
		return hoSoDao.findAll(ids);
	}

	@Override
	public long count() {
		return hoSoDao.count();
	}

	@Override
	public void delete(Long id) {
		hoSoDao.delete(id);
	}

	@Override
	public void delete(HoSo entity) {
		hoSoDao.delete(entity);
	}

	@Override
	public void delete(Iterable<HoSo> entities) {
		hoSoDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		hoSoDao.deleteAll();
	}

	@Override
	public List<HoSo> getHoSoFind(String ma, List<Long> trangThai, String taskId, String processInstanceId,
			int firstResult, int maxResults) {
		return hoSoDao.getHoSoFind(ma, trangThai, taskId, processInstanceId, firstResult, maxResults);
	}

	@Override
	public List<HoSo> getHoSoFind(List<String> processInstanceIdList) {
		return hoSoDao.getHoSoFind(processInstanceIdList);
	}
	
	@Override
	public List<Map<String, Object>> getReportResult(String sql){
		return hoSoDao.getReportResult(sql);
	}
	
	@Override
	public List<Map<String, Object>> getReportResult(String sql, Object[] objParams, int[] types){
		return hoSoDao.getReportResult(sql, objParams, types);
	}

	@Override
	public List<Map<String, Object>> getHoSoJsonByKey(String sql, Object[] objParams, int[] types) {
		return hoSoDao.getHoSoJsonByKey(sql, objParams, types);
	}

	@Override
	public List<Map<String, Object>> getReportResult(String sql, Map<String, Object> paramMap) {
		return hoSoDao.getReportResult(sql, paramMap);
	}


}

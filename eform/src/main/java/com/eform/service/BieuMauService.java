package com.eform.service;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.Formula;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;

public interface BieuMauService {
	public BieuMau save(BieuMau entity);
	public Iterable<BieuMau> save(Iterable<BieuMau> entities);
	public BieuMau findOne(Long id);
	public boolean exists(Long id);
	public Iterable<BieuMau> findAll();
	public Iterable<BieuMau> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(BieuMau entity);
	public void delete(Iterable<BieuMau> entities);
	public void deleteAll();
	public List<BieuMau> findByCode(String ma);
	public List<BieuMau> findByCode(List<String> maList);
	
	public List<BieuMau> getBieuMauFind(
			String ma, 
			String ten, 
			Long loaiBieuMau, 
			List<Long> trangThai, 
			int firstResult, 
			int maxResults);

    public Long getBieuMauFind(
    		String ma, 
    		String ten, 
    		Long loaiBieuMau, 
    		List<Long> trangThai);
    
    public List<BieuMau> getBieuMauFind(
    		String ma, 
			List<String> maList, 
			String ten, 
			Long loaiBieuMau, 
			List<Long> trangThai, 
			int firstResult, 
			int maxResults);

    public Long getBieuMauFind(
    		String ma, 
    		List<String> maList, 
    		String ten, 
    		Long loaiBieuMau, 
    		List<Long> trangThai);
    
    public void updateBieuMau(
    		BieuMau obj, 
    		List<BmO> bmOList, 
    		List<BmHang> bmHangList, 
    		List<BmOTruongThongTin> oTruongTTList);
    
    public void updateBieuMau(
    		BieuMau obj, 
    		List<BmO> bmOList, 
    		List<BmHang> bmHangList, 
    		List<BmOTruongThongTin> oTruongTTList, List<Formula> formula);
    
    public void deleteBieuMau(BieuMau obj);
    
    public int deleteBmOByBieuMau(BieuMau bieuMau);
	List<BmO> getBmOFind(String ma, String ten, Long loaiO, BieuMau bieuMau, BmHang bmHang, BieuMau bieuMauCon, int firstResult, int maxResults);
    Long getBmOFind(String ma, String ten, Long loaiO, BieuMau bieuMau, BmHang bmHang);
    
    public int deleteBmOTruongThongtinByBieuMau(BieuMau bieuMau);
    List<BmOTruongThongTin> getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO , TruongThongTin truongTT, int firstResult, int maxResults);
    Long getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO, TruongThongTin truongTT);
    
    public int deleteBmHangByBieuMau(BieuMau bieuMau);
    List<BmHang> getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau, int firstResult, int maxResults);
    Long getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau);
    

	public List<DanhMuc> getDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc,
			DanhMuc danhMuc, 
			int firstResult, 
			int maxResults);

	public Long getDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc, 
			DanhMuc danhMuc);
	
	public List<TruongThongTin> getTruongThongTinFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc, 
			List<Long> kieuDuLieu, 
			int firstResult, 
			int maxResults);
	
	public void importBieuMau(List<LoaiDanhMuc> loaiDanhMuc,
				List<DanhMuc> danhMucList, 
				List<TruongThongTin> truongThongTinList, 
				List<BieuMau> bieuMauList,
				List<BmHang> hangList,
				List<BmO> oList,
				List<BmOTruongThongTin> oTruongTTList);
}

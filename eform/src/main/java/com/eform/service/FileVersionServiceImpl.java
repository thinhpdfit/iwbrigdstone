package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.FileManagementDao;
import com.eform.dao.FileVersionDao;
import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class FileVersionServiceImpl implements FileVersionService {

	@Autowired
	private FileVersionDao fileVersionDao;

	@Autowired
	private FileManagementDao fileManagementDao;

	@Override
	public FileVersion save(FileVersion entity) {
		return fileVersionDao.save(entity);
	}

	@Override
	public Iterable<FileVersion> save(Iterable<FileVersion> entities) {
		return fileVersionDao.save(entities);
	}

	@Override
	public FileVersion findOne(Long id) {
		return fileVersionDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return fileVersionDao.exists(id);
	}

	@Override
	public Iterable<FileVersion> findAll() {
		return fileVersionDao.findAll();
	}

	@Override
	public Iterable<FileVersion> findAll(Iterable<Long> ids) {
		return fileVersionDao.findAll(ids);
	}

	@Override
	public long count() {
		return fileVersionDao.count();
	}

	@Override
	public void delete(Long id) {
		fileVersionDao.delete(id);
	}

	@Override
	public void delete(FileVersion entity) {
		fileVersionDao.delete(entity);
	}

	@Override
	public void delete(Iterable<FileVersion> entities) {
		fileVersionDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		fileVersionDao.deleteAll();
	}

	@Override
	public void save(FileVersion version, FileManagement manager) {
		fileManagementDao.save(manager);
		fileVersionDao.save(version);
	}

	@Override
	public List<FileVersion> getFileVersionFind(FileManagement manager) {
		// TODO Auto-generated method stub
		return fileVersionDao.getFileVersionFind(manager);
	}

	@Override
	public void deleteWithChild(FileVersion entity) {
		fileVersionDao.deleteWithChild(entity);
	}

	}

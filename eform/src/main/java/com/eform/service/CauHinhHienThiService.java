package com.eform.service;

import java.util.List;

import com.eform.model.entities.CauHinhHienThi;

public interface CauHinhHienThiService {
	public CauHinhHienThi save(CauHinhHienThi entity);
	public Iterable<CauHinhHienThi> save(Iterable<CauHinhHienThi> entities);
	public CauHinhHienThi findOne(Long id);
	public boolean exists(Long id);
	public Iterable<CauHinhHienThi> findAll();
	public Iterable<CauHinhHienThi> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(CauHinhHienThi entity);
	public void delete(Iterable<CauHinhHienThi> entities);
	public void deleteAll();
	public void save(List<CauHinhHienThi> list, String processDefId);
	public List<CauHinhHienThi> findByProcessDefId(String processDefId);
}

package com.eform.service;

import java.util.List;

import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.ProcessTaskProperty;

public interface DeploymentHtDonViService {
	public DeploymentHtDonVi save(DeploymentHtDonVi entity);
	public Iterable<DeploymentHtDonVi> save(Iterable<DeploymentHtDonVi> entities);
	public DeploymentHtDonVi findOne(Long id);
	public boolean exists(Long id);
	public Iterable<DeploymentHtDonVi> findAll();
	public Iterable<DeploymentHtDonVi> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(DeploymentHtDonVi entity);
	public void delete(Iterable<DeploymentHtDonVi> entities);
	public void deleteAll();
	public List<DeploymentHtDonVi> getDeploymentHtDonViFind(String modelerId);

	public void update(List<DeploymentHtDonVi> update,List<DeploymentHtDonVi>delete);
	public void update(List<DeploymentHtDonVi> update,List<DeploymentHtDonVi>delete, List<ProcessTaskProperty> taskProperties);
	public List<DeploymentHtDonVi> getDeploymentHtDonViFind(String modelerId, HtDonVi donVi);
}

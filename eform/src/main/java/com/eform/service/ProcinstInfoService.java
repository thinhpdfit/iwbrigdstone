package com.eform.service;

import com.eform.model.entities.ProcinstInfo;

public interface ProcinstInfoService {
	public ProcinstInfo save(ProcinstInfo entity);
	public Iterable<ProcinstInfo> save(Iterable<ProcinstInfo> entities);
	public ProcinstInfo findOne(Long id);
	public boolean exists(Long id);
	public Iterable<ProcinstInfo> findAll();
	public Iterable<ProcinstInfo> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(ProcinstInfo entity);
	public void delete(Iterable<ProcinstInfo> entities);
	public void deleteAll();
	public ProcinstInfo findByProcinstIdAndInfoKey(String procinstId, String infoKey);
}

package com.eform.service;

import java.util.List;

import com.eform.model.entities.DocumentOrder;

public interface DocumentOrderService {
	public DocumentOrder save(DocumentOrder entity);
	public Iterable<DocumentOrder> save(Iterable<DocumentOrder> entities);
	public DocumentOrder findOne(Long id);
	public boolean exists(Long id);
	public Iterable<DocumentOrder> findAll();
	public Iterable<DocumentOrder> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(DocumentOrder entity);
	public void delete(Iterable<DocumentOrder> entities);
	public void deleteAll();
	
	public List<DocumentOrder> findByDocumentBook(String documentBook);
}

package com.eform.service;

import java.util.List;

import com.eform.model.entities.TaskButton;

public interface TaskButtonService {
	public TaskButton save(TaskButton entity);
	public Iterable<TaskButton> save(Iterable<TaskButton> entities);
	public TaskButton findOne(Long id);
	public boolean exists(Long id);
	public Iterable<TaskButton> findAll();
	public Iterable<TaskButton> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(TaskButton entity);
	public void delete(Iterable<TaskButton> entities);
	public void deleteAll();
	public List<TaskButton>  getTaskButton(String activitiId, String modelId);
	
	
}

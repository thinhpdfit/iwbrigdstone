package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.FileVersionDao;
import com.eform.dao.SignatureDao;
import com.eform.model.entities.FileVersion;
import com.eform.model.entities.Signature;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class SignatureServiceImpl implements SignatureService {

	@Autowired
	private SignatureDao signatureDao;

	@Autowired
	private FileVersionDao fileVersionDao;

	@Override
	public Signature save(Signature entity) {
		return signatureDao.save(entity);
	}

	@Override
	public Iterable<Signature> save(Iterable<Signature> entities) {
		return signatureDao.save(entities);
	}

	@Override
	public Signature findOne(Long id) {
		return signatureDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return signatureDao.exists(id);
	}

	@Override
	public Iterable<Signature> findAll() {
		return signatureDao.findAll();
	}

	@Override
	public Iterable<Signature> findAll(Iterable<Long> ids) {
		return signatureDao.findAll(ids);
	}

	@Override
	public long count() {
		return signatureDao.count();
	}

	@Override
	public void delete(Long id) {
		signatureDao.delete(id);
	}

	@Override
	public void delete(Signature entity) {
		signatureDao.delete(entity);
	}

	@Override
	public void delete(Iterable<Signature> entities) {
		signatureDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		signatureDao.deleteAll();
	}

	@Override
	public boolean checkSigned(FileVersion file, String userId) {
		return signatureDao.checkSigned(file, userId);
	}

	@Override
	public List<Signature> getSignatureFind(FileVersion file) {
		return signatureDao.getSignatureFind(file);
	}

	@Override
	public void save(FileVersion fileVersion, Signature sign) {
		if(fileVersion != null){
			fileVersionDao.save(fileVersion);
		}
		if(sign != null){
			signatureDao.save(sign);
		}
	}

	}

package com.eform.service;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;

public interface BmOService {
	public BmO save(BmO entity);
	public Iterable<BmO> save(Iterable<BmO> entities);
	public BmO findOne(Long id);
	public boolean exists(Long id);
	public Iterable<BmO> findAll();
	public Iterable<BmO> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(BmO entity);
	public void delete(Iterable<BmO> entities);
	public void deleteAll();
	public int deleteByBieuMau(BieuMau bieuMau);
	List<BmO> getBmOFind(String ma, String ten, Long loaiO, BieuMau bieuMau, BmHang bmHang, BieuMau bieuMauCon, int firstResult, int maxResults);
    Long getBmOFind(String ma, String ten, Long loaiO, BieuMau bieuMau, BmHang bmHang);
}

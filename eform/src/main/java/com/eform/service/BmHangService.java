package com.eform.service;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;

public interface BmHangService {
	public BmHang save(BmHang entity);
	public Iterable<BmHang> save(Iterable<BmHang> entities);
	public BmHang findOne(Long id);
	public boolean exists(Long id);
	public Iterable<BmHang> findAll();
	public Iterable<BmHang> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(BmHang entity);
	public void delete(Iterable<BmHang> entities);
	public void deleteAll();
	public int deleteByBieuMau(BieuMau bieuMau);
    List<BmHang> getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau, int firstResult, int maxResults);
    Long getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau);
}

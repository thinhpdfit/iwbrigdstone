package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.DocumentOrderDao;
import com.eform.model.entities.DocumentOrder;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class DocumentOrderServiceImpl implements DocumentOrderService {

	@Autowired
	private DocumentOrderDao documentOrderDao;


	@Override
	public DocumentOrder save(DocumentOrder entity) {
		return documentOrderDao.save(entity);
	}

	@Override
	public Iterable<DocumentOrder> save(Iterable<DocumentOrder> entities) {
		return documentOrderDao.save(entities);
	}

	@Override
	public DocumentOrder findOne(Long id) {
		return documentOrderDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return documentOrderDao.exists(id);
	}

	@Override
	public Iterable<DocumentOrder> findAll() {
		return documentOrderDao.findAll();
	}

	@Override
	public Iterable<DocumentOrder> findAll(Iterable<Long> ids) {
		return documentOrderDao.findAll(ids);
	}

	@Override
	public long count() {
		return documentOrderDao.count();
	}

	@Override
	public void delete(Long id) {
		documentOrderDao.delete(id);
	}

	@Override
	public void delete(DocumentOrder entity) {
		documentOrderDao.delete(entity);
	}

	@Override
	public void delete(Iterable<DocumentOrder> entities) {
		documentOrderDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		documentOrderDao.deleteAll();
	}

	@Override
	public List<DocumentOrder> findByDocumentBook(String documentBook) {
		// TODO Auto-generated method stub
		return documentOrderDao.findByDocumentBook(documentBook);
	}

	}

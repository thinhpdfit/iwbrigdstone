package com.eform.service;

import java.util.List;

import com.eform.model.entities.Notification;

public interface NotificationService {
	public Notification save(Notification entity);
	public Iterable<Notification> save(Iterable<Notification> entities);
	public Notification findOne(Long id);
	public boolean exists(Long id);
	public Iterable<Notification> findAll();
	public Iterable<Notification> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(Notification entity);
	public void delete(Iterable<Notification> entities);
	public void deleteAll();
	
	public List<Notification> getNotificationFind(
			List<Long> types, 
			String fromUserId,
			String userId,
			String refKey,
			List<Long> trangThai, 
			int firstResult, 
			int maxResults);

	public Long getNotificationFind(
			List<Long> types, 
			String fromUserId,
			String userId,
			String refKey,
			List<Long> trangThai);
	
	public void markAsRead();
}

package com.eform.service;

import java.util.List;

import com.eform.model.entities.HtThamSo;

public interface HtThamSoService {
	public HtThamSo save(HtThamSo entity);
	public Iterable<HtThamSo> save(Iterable<HtThamSo> entities);
	public HtThamSo findOne(Long id);
	public boolean exists(Long id);
	public Iterable<HtThamSo> findAll();
	public Iterable<HtThamSo> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(HtThamSo entity);
	public void delete(Iterable<HtThamSo> entities);
	public void deleteAll();
	public List<HtThamSo> getHtThamSoFind(String ma, List<Long> trangThai, List<Long> loai, int firstResult, int maxResults);
	public void updateList(List<HtThamSo> htThamSoList);
	public HtThamSo getHtThamSoFind(String ma);
	public String getHtThamSoValue(String ma);
}

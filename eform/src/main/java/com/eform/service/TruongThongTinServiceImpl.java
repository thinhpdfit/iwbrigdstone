package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.TruongThongTinDao;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class TruongThongTinServiceImpl implements TruongThongTinService {

	@Autowired
	private TruongThongTinDao truongThongTinDao;

	@Override
	public TruongThongTin save(TruongThongTin entity) {
		return truongThongTinDao.save(entity);
	}

	@Override
	public Iterable<TruongThongTin> save(Iterable<TruongThongTin> entities) {
		return truongThongTinDao.save(entities);
	}

	@Override
	public TruongThongTin findOne(Long id) {
		return truongThongTinDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return truongThongTinDao.exists(id);
	}

	@Override
	public Iterable<TruongThongTin> findAll() {
		return truongThongTinDao.findAll();
	}

	@Override
	public Iterable<TruongThongTin> findAll(Iterable<Long> ids) {
		return truongThongTinDao.findAll(ids);
	}

	@Override
	public long count() {
		return truongThongTinDao.count();
	}

	@Override
	public void delete(Long id) {
		truongThongTinDao.delete(id);
	}

	@Override
	public void delete(TruongThongTin entity) {
		truongThongTinDao.delete(entity);
	}

	@Override
	public void delete(Iterable<TruongThongTin> entities) {
		truongThongTinDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		truongThongTinDao.deleteAll();
	}

	@Override
	public List<TruongThongTin> findByCode(String ma) {
		// TODO Auto-generated method stub
		return truongThongTinDao.findByCode(ma);
	}

	@Override
	public List<TruongThongTin> getTruongThongTinFind(String ma, String ten, List<Long> trangThai,
			LoaiDanhMuc loaiDanhMuc, List<Long> kieuDuLieu,
			List<String> viTriHienThi, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return truongThongTinDao.getTruongThongTinFind(ma, ten, trangThai, loaiDanhMuc, kieuDuLieu, viTriHienThi, firstResult, maxResults);
	}

	@Override
	public Long getTruongThongTinFind(String ma, String ten, List<Long> trangThai, LoaiDanhMuc loaiDanhMuc,
			List<Long> kieuDuLieu,
			List<String> viTriHienThi) {
		// TODO Auto-generated method stub
		return truongThongTinDao.getTruongThongTinFind(ma, ten, trangThai, loaiDanhMuc, kieuDuLieu, viTriHienThi);
	}

}

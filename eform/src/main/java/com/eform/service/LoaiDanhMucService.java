package com.eform.service;

import java.util.List;

import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;

public interface LoaiDanhMucService {
	public LoaiDanhMuc save(LoaiDanhMuc entity);
	public Iterable<LoaiDanhMuc> save(Iterable<LoaiDanhMuc> entities);
	public LoaiDanhMuc findOne(Long id);
	public boolean exists(Long id);
	public Iterable<LoaiDanhMuc> findAll();
	public Iterable<LoaiDanhMuc> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(LoaiDanhMuc entity);
	public void delete(Iterable<LoaiDanhMuc> entities);
	public void deleteAll();
	

	public LoaiDanhMuc save(LoaiDanhMuc entity, List<DanhMuc> danhMucList);
	
	public List<LoaiDanhMuc> findByCode(String ma);
	public List<LoaiDanhMuc> getLoaiDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc,
			int firstResult, 
			int maxResults);
	 public Long getLoaiDanhMucFind(
			 String ma, 
			 String ten, 
			 List<Long> trangThai, 
			 LoaiDanhMuc loaiDanhMuc);

	 public void insertWithId(List<LoaiDanhMuc> loaiDanhMuc);

}

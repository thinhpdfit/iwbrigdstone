package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.BmOTruongThongTinDao;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.TruongThongTin;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class BmOTruongThongTinServiceImpl implements BmOTruongThongTinService {

	@Autowired
	private BmOTruongThongTinDao mmOTruongThongTinDao;

	@Override
	public BmOTruongThongTin save(BmOTruongThongTin entity) {
		return mmOTruongThongTinDao.save(entity);
	}

	@Override
	public Iterable<BmOTruongThongTin> save(Iterable<BmOTruongThongTin> entities) {
		return mmOTruongThongTinDao.save(entities);
	}

	@Override
	public BmOTruongThongTin findOne(Long id) {
		return mmOTruongThongTinDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return mmOTruongThongTinDao.exists(id);
	}

	@Override
	public Iterable<BmOTruongThongTin> findAll() {
		return mmOTruongThongTinDao.findAll();
	}

	@Override
	public Iterable<BmOTruongThongTin> findAll(Iterable<Long> ids) {
		return mmOTruongThongTinDao.findAll(ids);
	}

	@Override
	public long count() {
		return mmOTruongThongTinDao.count();
	}

	@Override
	public void delete(Long id) {
		mmOTruongThongTinDao.delete(id);
	}

	@Override
	public void delete(BmOTruongThongTin entity) {
		mmOTruongThongTinDao.delete(entity);
	}

	@Override
	public void delete(Iterable<BmOTruongThongTin> entities) {
		mmOTruongThongTinDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		mmOTruongThongTinDao.deleteAll();
	}

	@Override
	public int deleteByBieuMau(BieuMau bieuMau) {
		// TODO Auto-generated method stub
		return mmOTruongThongTinDao.deleteByBieuMau(bieuMau);
	}

	@Override
	public List<BmOTruongThongTin> getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO,
			TruongThongTin truongTT, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return mmOTruongThongTinDao.getBmOTruongThongTinFind(bieuMau, bmHang, bmO, truongTT, firstResult, maxResults);
	}

	@Override
	public Long getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO, TruongThongTin truongTT) {
		// TODO Auto-generated method stub
		return mmOTruongThongTinDao.getBmOTruongThongTinFind(bieuMau, bmHang, bmO, truongTT);
	}

	@Override
	public List<TruongThongTin> findTruongThongtinByBieuMau(List<BieuMau> bieuMauList, Boolean tableDisplay, Boolean searchFormDisPlay) {
		// TODO Auto-generated method stub
		return mmOTruongThongTinDao.findTruongThongtinByBieuMau(bieuMauList, tableDisplay, searchFormDisPlay);
	}

}

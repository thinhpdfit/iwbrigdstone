package com.eform.service;

import java.util.List;

import com.eform.model.entities.ChatGroup;

public interface ChatGroupService {
	public ChatGroup save(ChatGroup entity);
	public Iterable<ChatGroup> save(Iterable<ChatGroup> entities);
	public ChatGroup findOne(Long id);
	public boolean exists(Long id);
	public Iterable<ChatGroup> findAll();
	public Iterable<ChatGroup> findAll(Iterable<Long> ids);
	public long count();
	public void delete(Long id);
	public void delete(ChatGroup entity);
	public void delete(Iterable<ChatGroup> entities);
	public void deleteAll();
	
	
	public List<ChatGroup> find(String userId, String groupCode, int from, int max);
}

package com.eform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.dao.BieuMauDao;
import com.eform.dao.BmHangDao;
import com.eform.dao.BmODao;
import com.eform.dao.BmOTruongThongTinDao;
import com.eform.dao.DanhMucDao;
import com.eform.dao.FormulaDao;
import com.eform.dao.LoaiDanhMucDao;
import com.eform.dao.TruongThongTinDao;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.Formula;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;

@Transactional(transactionManager = "eformTransactionManager")
@Service
public class BieuMauServiceImpl implements BieuMauService {

	@Autowired
	private BieuMauDao bieuMauDao;
	@Autowired
	private FormulaDao formulaDao;
	@Autowired
	private BmODao bmODao;
	@Autowired
	private BmHangDao bmHangDao;
	@Autowired
	private BmOTruongThongTinDao bmOTruongThongTinDao;
	@Autowired
	private TruongThongTinDao truongThongTinDao;
	@Autowired
	private DanhMucDao danhMucDao;
	@Autowired
	private LoaiDanhMucDao loaiDanhMucDao;

	@Override
	public BieuMau save(BieuMau entity) {
		return bieuMauDao.save(entity);
	}

	@Override
	public Iterable<BieuMau> save(Iterable<BieuMau> entities) {
		return bieuMauDao.save(entities);
	}

	@Override
	public BieuMau findOne(Long id) {
		return bieuMauDao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		return bieuMauDao.exists(id);
	}

	@Override
	public Iterable<BieuMau> findAll() {
		return bieuMauDao.findAll();
	}

	@Override
	public Iterable<BieuMau> findAll(Iterable<Long> ids) {
		return bieuMauDao.findAll(ids);
	}

	@Override
	public long count() {
		return bieuMauDao.count();
	}

	@Override
	public void delete(Long id) {
		bieuMauDao.delete(id);
	}

	@Override
	public void delete(BieuMau entity) {
		bieuMauDao.delete(entity);
	}

	@Override
	public void delete(Iterable<BieuMau> entities) {
		bieuMauDao.delete(entities);

	}

	@Override
	public void deleteAll() {
		bieuMauDao.deleteAll();
	}

	@Override
	public List<BieuMau> findByCode(String ma) {
		return bieuMauDao.findByCode(ma);
	}
	
	@Override
	public List<BieuMau> findByCode(List<String> maList) {
		return bieuMauDao.findByCode(maList);
	}

	@Override
	public List<BieuMau> getBieuMauFind(String ma, String ten, Long loaiBieuMau, List<Long> trangThai, int firstResult,
			int maxResults) {
		// TODO Auto-generated method stub
		return bieuMauDao.getBieuMauFind(ma, ten, loaiBieuMau, trangThai, firstResult, maxResults);
	}

	@Override
	public Long getBieuMauFind(String ma, String ten, Long loaiBieuMau, List<Long> trangThai) {
		// TODO Auto-generated method stub
		return bieuMauDao.getBieuMauFind(ma, ten, loaiBieuMau, trangThai);
	}

	@Override
	public void updateBieuMau(BieuMau obj, List<BmO> bmOList, List<BmHang> bmHangList,
			List<BmOTruongThongTin> oTruongTTList) {
		bieuMauDao.updateBieuMau(obj, bmOList, bmHangList, oTruongTTList);
	}

	@Override
	public void deleteBieuMau(BieuMau obj) {
		//bieuMauDao.deleteBieuMau(obj);
		if (obj != null) {
			if (obj.getId() != null) {
				bmOTruongThongTinDao.deleteByBieuMau(obj);
				bmODao.deleteByBieuMau(obj);
				bmHangDao.deleteByBieuMau(obj);
				bieuMauDao.delete(obj);
			} 
		}
	}

	@Override
	public int deleteBmOByBieuMau(BieuMau bieuMau) {
		// TODO Auto-generated method stub
		return bmODao.deleteByBieuMau(bieuMau);
	}

	@Override
	public List<BmO> getBmOFind(String ma, String ten, Long loaiO, BieuMau bieuMau, BmHang bmHang, BieuMau bieuMauCon, int firstResult,
			int maxResults) {
		// TODO Auto-generated method stub
		return bmODao.getBmOFind(ma, ten, loaiO, bieuMau, bmHang, bieuMauCon, firstResult, maxResults);
	}

	@Override
	public Long getBmOFind(String ma, String ten, Long loaiO, BieuMau bieuMau, BmHang bmHang) {
		// TODO Auto-generated method stub
		return bmODao.getBmOFind(ma, ten, loaiO, bieuMau, bmHang);
	}

	@Override
	public int deleteBmOTruongThongtinByBieuMau(BieuMau bieuMau) {
		// TODO Auto-generated method stub
		return bmOTruongThongTinDao.deleteByBieuMau(bieuMau);
	}

	@Override
	public List<BmOTruongThongTin> getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO,
			TruongThongTin truongTT, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return bmOTruongThongTinDao.getBmOTruongThongTinFind(bieuMau, bmHang, bmO, truongTT, firstResult, maxResults);
	}

	@Override
	public Long getBmOTruongThongTinFind(BieuMau bieuMau, BmHang bmHang, BmO bmO, TruongThongTin truongTT) {
		// TODO Auto-generated method stub
		return bmOTruongThongTinDao.getBmOTruongThongTinFind(bieuMau, bmHang, bmO, truongTT);
	}

	@Override
	public int deleteBmHangByBieuMau(BieuMau bieuMau) {
		// TODO Auto-generated method stub
		return bmHangDao.deleteByBieuMau(bieuMau);
	}

	@Override
	public List<BmHang> getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau, int firstResult,
			int maxResults) {
		// TODO Auto-generated method stub
		return bmHangDao.getBmHangFind(ma, ten, loaiHang, bieuMau, firstResult, maxResults);
	}

	@Override
	public Long getBmHangFind(String ma, String ten, Long loaiHang, BieuMau bieuMau) {
		// TODO Auto-generated method stub
		return bmHangDao.getBmHangFind(ma, ten, loaiHang, bieuMau);
	}

	@Override
	public List<DanhMuc> getDanhMucFind(String ma, String ten, List<Long> trangThai, LoaiDanhMuc loaiDanhMuc,
			DanhMuc danhMuc, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return danhMucDao.getDanhMucFind(ma, ten, trangThai, loaiDanhMuc, danhMuc, firstResult, maxResults);
	}

	@Override
	public Long getDanhMucFind(String ma, String ten, List<Long> trangThai, LoaiDanhMuc loaiDanhMuc, DanhMuc danhMuc) {
		// TODO Auto-generated method stub
		return danhMucDao.getDanhMucFind(ma, ten, trangThai, loaiDanhMuc, danhMuc);
	}

	@Override
	public List<TruongThongTin> getTruongThongTinFind(String ma, String ten, List<Long> trangThai,
			LoaiDanhMuc loaiDanhMuc, List<Long> kieuDuLieu, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return truongThongTinDao.getTruongThongTinFind(ma, ten, trangThai, loaiDanhMuc, kieuDuLieu, null, firstResult, maxResults);
	}

	@Override
	public List<BieuMau> getBieuMauFind(String ma, List<String> maList, String ten, Long loaiBieuMau, List<Long> trangThai,
			int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return bieuMauDao.getBieuMauFind(ma, maList, ten, loaiBieuMau, trangThai, firstResult, maxResults);
	}

	@Override
	public Long getBieuMauFind(String ma, List<String> maList, String ten, Long loaiBieuMau, List<Long> trangThai) {
		// TODO Auto-generated method stub
		return bieuMauDao.getBieuMauFind(ma,maList, ten, loaiBieuMau, trangThai);
	}

	@Override
	public void updateBieuMau(BieuMau obj, List<BmO> bmOList, List<BmHang> bmHangList,
			List<BmOTruongThongTin> oTruongTTList, List<Formula> formula) {
		bieuMauDao.updateBieuMau(obj, bmOList, bmHangList, oTruongTTList);
		formulaDao.save(formula);
		
	}

	@Override
	public void importBieuMau(List<LoaiDanhMuc> loaiDanhMuc, List<DanhMuc> danhMucList,
			List<TruongThongTin> truongThongTinList, List<BieuMau> bieuMauList, List<BmHang> hangList, List<BmO> oList,
			List<BmOTruongThongTin> oTruongTTList) {
		loaiDanhMucDao.save(loaiDanhMuc);
//		danhMucDao.insertWithId(danhMucList);
//		truongThongTinDao.insertWithId(truongThongTinList);
//		bieuMauDao.insertWithId(bieuMauList);
//		bmHangDao.insertWithId(hangList);
//		bmODao.insertWithId(oList);
//		bmOTruongThongTinDao.insertWithId(oTruongTTList);
	}

}

package com.eform.blog.services;

import java.util.List;

import com.eform.blog.entites.Comment;



public interface CommentService {
	public Iterable<Comment> save(Iterable<Comment> entities);
	public Comment save(Comment entity);
	public Comment findOne(Long id);
	public boolean exists(Long id);
	public Iterable<Comment> findAll();
	public Iterable<Comment> findAll(Iterable<Long> ids);
	public List<Comment> findLimitedOrderByCreateDate(String condition,int firstResult, int maxResults);
	public List<Comment> findAllOderByCreateDate(String condition,String Order);
	public long count();
	public long countOnCondition(String condition);
	public void delete(Long id);
	public void delete(Comment entity);
	public void delete(Iterable<Comment> entities);
	public void deleteAll();
}

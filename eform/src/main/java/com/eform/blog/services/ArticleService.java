package com.eform.blog.services;

import java.util.Date;
import java.util.List;

import com.eform.blog.entites.Article;



public interface ArticleService {
	public Iterable<Article> save(Iterable<Article> entities);
	public Article save(Article entity);
	public Article findOne(Long id);
	public boolean exists(Long id);
	public Iterable<Article> findAll();
	public Iterable<Article> findAll(Iterable<Long> ids);
	public List<Article> findLimitedOrderByCreateDate(String condition,int firstResult, int maxResults);
	public List<Article> findLimitedOrderByView(String condition,int firstResult, int maxResults);
	public List<Article> findLimitedOrderByCommentCount(String condition,int firstResult, int maxResults);
	public long countOnCondition(String condition);
	public List<Article> findAllOderByCreateDate(String condition);
	public List<Article> findAllRelatedArticle(Date publishdate,int size);
	public long count();
	public void delete(Long id);
	public void delete(Article entity);
	public void delete(Iterable<Article> entities);
	public void deleteAll();
}

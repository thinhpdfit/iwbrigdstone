package com.eform.blog.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.blog.dao.CommentDao;
import com.eform.blog.entites.Comment;



@Transactional(transactionManager = "eformTransactionManager")
@Service
public class CommentServiceImpl implements CommentService {

	@Autowired	
	private CommentDao commentdao;
	@Override
	public Iterable<Comment> save(Iterable<Comment> entities) {
		// TODO Auto-generated method stub
		
		return commentdao.save(entities);
	}

	@Override
	public Comment save(Comment entity) {
		// TODO Auto-generated method stub
		
		return commentdao.save(entity);
	}

	@Override
	public Comment findOne(Long id) {
		// TODO Auto-generated method stub
		
		return commentdao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		// TODO Auto-generated method stub
		
		return commentdao.exists(id);
	}

	@Override
	public Iterable<Comment> findAll() {
		// TODO Auto-generated method stub
		
		return commentdao.findAll();
	}

	@Override
	public Iterable<Comment> findAll(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		
		return commentdao.findAll(ids);
	}
	@Override
	public List<Comment> findLimitedOrderByCreateDate(String condition,int firstResult, int maxResults){
		return commentdao.findLimitedOrderByCreateDate(condition,firstResult, maxResults);
	}
	@Override
	public List<Comment> findAllOderByCreateDate(String condition, String Order){
		return commentdao.findAllOderByCreateDate(condition,Order);
	}
	@Override
	public long count() {
		// TODO Auto-generated method stub
		
		return commentdao.count();
	}
	@Override
	public long countOnCondition(String condition){
		return commentdao.CountAllByCondition(condition);
	}
		
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		commentdao.delete(id);

	}

	@Override
	public void delete(Comment entity) {
		// TODO Auto-generated method stub
		commentdao.delete(entity);

	}

	@Override
	public void delete(Iterable<Comment> entities) {
		// TODO Auto-generated method stub
		commentdao.delete(entities);

	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		commentdao.deleteAll();

	}

}

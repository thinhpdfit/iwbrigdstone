package com.eform.blog.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.blog.dao.ArticleDao;
import com.eform.blog.dao.CommentDao;
import com.eform.blog.entites.Article;
import com.eform.blog.entites.Comment;


@Transactional(transactionManager = "eformTransactionManager")
@Service
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	private ArticleDao articledao;
	
	@Autowired
	private CommentDao commentdao;
	@Override
	public Iterable<Article> save(Iterable<Article> entities) {
		// TODO Auto-generated method stub
		
		return articledao.save(entities);
	}

	@Override
	public Article save(Article entity) {
		// TODO Auto-generated method stub
		
		return articledao.save(entity);
	}

	@Override
	public Article findOne(Long id) {
		// TODO Auto-generated method stub
		
		return articledao.findOne(id);
	}

	@Override
	public boolean exists(Long id) {
		// TODO Auto-generated method stub
		
		return articledao.exists(id);
	}

	@Override
	public Iterable<Article> findAll() {
		// TODO Auto-generated method stub
		
		return articledao.findAll();
	}

	@Override
	public Iterable<Article> findAll(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return articledao.findAll(ids);
	}
	@Override
	public List<Article> findLimitedOrderByCreateDate(String condition,int firstResult, int maxResults){
		return articledao.findLimitedOrderByCreateDate(condition,firstResult, maxResults);
	}
	@Override
	public List<Article> findLimitedOrderByView(String condition,int firstResult, int maxResults){
		return articledao.findLimitedOrderByView(condition,firstResult, maxResults);
	}
	@Override
	public List<Article> findLimitedOrderByCommentCount(String condition,int firstResult, int maxResults){
		return articledao.findLimitedOrderByComment(condition, firstResult, maxResults);
	}
	
	@Override
	public List<Article> findAllOderByCreateDate(String condition){
		return articledao.findAllOderByCreateDate(condition);
	}
	@Override
	public List<Article> findAllRelatedArticle(Date publishdate,int size){
		return articledao.findRelatedArticle(publishdate,size);
	}
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return articledao.count();
	}
	
	@Override
	public long countOnCondition(String condition){
		return articledao.countOnCondition(condition);
	}
	
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		articledao.delete(id);

	}

	@Override
	public void delete(Article entity) {
		// TODO Auto-generated method stub
		if(entity.getCommentcount()!=null && entity.getCommentcount()>0){
			 String condition= "where o.article.id = '" + entity.getId() + "'";
	    	 List<Comment>lstComment = commentdao.findAllOderByCreateDate(condition,"DESC");
	    	 for(Comment comment:lstComment){
	    		 commentdao.delete(comment);
	    	 }
		}
		articledao.delete(entity);

	}

	@Override
	public void delete(Iterable<Article> entities) {
		// TODO Auto-generated method stub
		articledao.delete(entities);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		articledao.deleteAll();

	}

}

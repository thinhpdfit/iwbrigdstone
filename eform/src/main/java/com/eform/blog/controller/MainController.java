package com.eform.blog.controller;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.security.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eform.blog.entites.Article;
import com.eform.blog.entites.Comment;
import com.eform.blog.services.ArticleService;
import com.eform.blog.services.CommentService;
import com.eform.common.utils.AuthenticationUtils;
import com.eform.common.utils.CommonUtils;
import com.fasterxml.jackson.databind.ObjectMapper;



@Controller
public class MainController {
	
	/**
	 * 
	 */
	@Autowired
	private ArticleService articleservice;
	
	@Autowired
	private CommentService commentservice;
	
	@Autowired
	 protected IdentityService identityService;
	/**
	 * @return
	 */
	@RequestMapping(value="/blog/index",method = RequestMethod.GET)
    public String homepage(){
		//System.out.println("-------------------OK");
        return "blog/index";
    }
	
	@RequestMapping(value="/blog/front/article_detail/{id}",method = RequestMethod.GET)
    public String articleDetailPage(){
		//System.out.println("-------------------article_detail");
        return "blog/front/article_detail";
    }
	
	
	
	/*@RequestMapping(value="/blog/admin/table_article",method = RequestMethod.GET)
    public String table_articlePage(){
		//System.out.println("-------------------article_detail");
        return "/blog/admin/table_article";
    }*/
	
	
	
	/*@RequestMapping(value="/js/ckeditor/TVS-Manager/connectors/jsp/*",method = RequestMethod.GET)
    public String ckfinder(){
		System.out.print("OK");
        return "/js/ckeditor/TVS-Manager/connectors/jsp/filemanager.jsp";
    }*/
	
	
	/*@RequestMapping(value="/blog/articledetail/*",method = RequestMethod.GET)
    public String articleDetail(){
        return "/view/article_detail.html";
    }*/
	
	
	/**
	 * @return
	 */
	@RequestMapping(value="/blog/listallarticle",produces = MediaType.APPLICATION_JSON_VALUE,method = RequestMethod.GET)
    public @ResponseBody String ListAllArtical(){
		 String value ="";
		 List<Article>  lstArticle = new ArrayList<Article>();
		 /*Iterable<Article> iterArticle = articleservice.findAll();
		 Iterator<Article> ratorArticle = iterArticle.iterator();
		 while(ratorArticle.hasNext())
		 {
			 Article item = (Article)ratorArticle.next();
			 lstArticle.add(item);
		 }*/
		 lstArticle = articleservice.findAllOderByCreateDate(null);
	 	 final OutputStream out = new ByteArrayOutputStream();
	     final ObjectMapper mapper = new ObjectMapper();
	
	     try {
	    	value =mapper.writeValueAsString(lstArticle);
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
	     return value;
    }
	
	/**
	 * @return
	 */
	@RequestMapping(value="/blog/countPublishArticle",method = RequestMethod.GET)
    public ResponseEntity<?>  countPublishArticle(){
		
		 long numberofPublishArticle =0;
		 try {
			 	numberofPublishArticle = articleservice.countOnCondition("where o.status=true");
			 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			 }
		 return new ResponseEntity<Long>(numberofPublishArticle, HttpStatus.OK);
    }
	
	
	
	/**
	 * @param startIndex
	 * @param numberofitem
	 * @return
	 */
	@RequestMapping(value="/blog/listallPublishArticlepaging",method = RequestMethod.POST)
    public ResponseEntity<?>  ListAllArticalPaging(@RequestParam("startIndex") int startIndex,@RequestParam("numberofitem") int numberofitem){
		 String value ="";
		 List<Article>  lstArticle = new ArrayList<Article>();
		 lstArticle = articleservice.findLimitedOrderByCreateDate("where o.status=true",startIndex, numberofitem);
	 	 final OutputStream out = new ByteArrayOutputStream();
	     final ObjectMapper mapper = new ObjectMapper();
	     try {
	    	value =mapper.writeValueAsString(lstArticle);
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
	     return new ResponseEntity<List<Article>>(lstArticle, HttpStatus.OK);
    }
	
	
	
	@RequestMapping(value="/blog/listallPublishArticleOrderByView",method = RequestMethod.POST)
    public ResponseEntity<?>  listallPublishArticleOrderByView(@RequestParam("startIndex") int startIndex,@RequestParam("numberofitem") int numberofitem){
		 String value ="";
		 List<Article>  lstArticle = new ArrayList<Article>();
		 lstArticle = articleservice.findLimitedOrderByView("where o.status=true",startIndex, numberofitem);
	 	 final OutputStream out = new ByteArrayOutputStream();
	     final ObjectMapper mapper = new ObjectMapper();
	     try {
	    	value =mapper.writeValueAsString(lstArticle);
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
	     return new ResponseEntity<List<Article>>(lstArticle, HttpStatus.OK);
    }
	
	@RequestMapping(value="/blog/listallPublishArticleOrderByCommentCount",method = RequestMethod.POST)
    public ResponseEntity<?>  listallPublishArticleOrderByCommentCount(@RequestParam("startIndex") int startIndex,@RequestParam("numberofitem") int numberofitem){
		 String value ="";
		 List<Article>  lstArticle = new ArrayList<Article>();
		 lstArticle = articleservice.findLimitedOrderByCommentCount("where o.status=true",startIndex, numberofitem);
	 	 final OutputStream out = new ByteArrayOutputStream();
	     final ObjectMapper mapper = new ObjectMapper();
	     try {
	    	value =mapper.writeValueAsString(lstArticle);
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
	     return new ResponseEntity<List<Article>>(lstArticle, HttpStatus.OK);
    }
	
	/**
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/blog/articlebyid",method = RequestMethod.POST)
	public ResponseEntity<?>  ListAllArticalPaging(@RequestParam("id") Long id){
		 Article aritcle = null;
	     try {
	    	 aritcle = articleservice.findOne(id);
	    	 
	    	 //aritcle.getComments();
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<Article>(aritcle, HttpStatus.BAD_REQUEST);
		 }
	     return new ResponseEntity<Article>(aritcle, HttpStatus.OK);
    }
	
	@RequestMapping(value="/blog/increaseArticleView",method = RequestMethod.POST)
	public @ResponseBody boolean increaseArticleView(@RequestParam("id") Long id){
		try {			
			Article article = articleservice.findOne(id);
			int viewcount = 0;
			if(article.getView()!=null)
				viewcount =article.getView();
			article.setView(viewcount+1);
			articleservice.save(article);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	/**
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/blog/commentoarticle",method = RequestMethod.POST)
	public ResponseEntity<?>  ListAllCommentOfArticle(@RequestParam("id") Long id){
		List<Comment> lstComment = null;
		 String jsonData ="";
	     try {
	    	 String condition= "where o.article.id = '" + id + "' and o.commentId is Null and o.status = true";
	    	 lstComment = commentservice.findAllOderByCreateDate(condition,"ASC");
	    	 if(lstComment!=null && lstComment.size()>0){
	    		 jsonData +="[";
		    	 for(Comment comment:lstComment){
		    		  jsonData +="{\"id\":\"" + comment.getId() + "\",\"name\":\"" + comment.getName() +"\",\"email\":\"" + comment.getEmail() +"\",\"body\":\"" + comment.getBody() +"\",\"createdDate\":\"" + comment.getCreatedDate() + "\"";
		    		  condition= "where o.commentId = '" + comment.getId() + "' and o.status = true";
		    		  List<Comment> lstReply = commentservice.findAllOderByCreateDate(condition,"ASC");
		    		  if(lstReply!=null && lstReply.size() >0){
		    			  jsonData+=",\"replys\":[";
		    			  for(Comment reply:lstReply)
		    			  {
		    				  jsonData+="{\"name\":\"" + reply.getName() +"\",\"email\":\"" + reply.getEmail() +"\",\"body\":\"" + reply.getBody() +"\",\"createdDate\":\"" + reply.getCreatedDate() + "\"},";
		    			  }
		    			  jsonData= jsonData.substring(0, jsonData.length()-1);
		    			  jsonData+="]";
		    		  }
		    		  jsonData+="},";
		    	 }
		    	 jsonData= jsonData.substring(0, jsonData.length()-1);
		    	 jsonData +="]";
	    	 }
	    	 //aritcle.getComments();
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<String>(jsonData, HttpStatus.BAD_REQUEST);
		 }
	     return new ResponseEntity<String>(jsonData, HttpStatus.OK);
    }
	
	
	
	
	/**
	 * @param comment
	 * @return
	 */
	@RequestMapping(value="/blog/postcomment",method = RequestMethod.POST)
	public @ResponseBody boolean postComment(@RequestParam("name") String name, @RequestParam("email") String email,@RequestParam("body") String commentcontent,@RequestParam("articleid") Long articleid,@RequestParam("createdby") String createdby){
		try {			
			Comment comment = new Comment();
			Date currentdate = new Date();
			comment.setName(name);
			comment.setEmail(email);
			comment.setBody(commentcontent);
			comment.setCreatedDate(new java.sql.Timestamp(currentdate.getTime()));
			comment.setCreatedBy(createdby);
			Article article = articleservice.findOne(articleid);
			comment.setArticle(article);
			comment.setStatus(true);
			commentservice.save(comment);
			int commentcount = 0;
			int commentcountactive = 0;
			
			if(article.getCommentcount()!=null)
				commentcount = article.getCommentcount();
			article.setCommentcount(commentcount +1);
			if(article.getCommentCountActive()!=null)
				commentcountactive = article.getCommentCountActive();
			article.setCommentCountActive(commentcountactive +1);
			articleservice.save(article);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@RequestMapping(value="/blog/postreplycomment",method = RequestMethod.POST)
	public @ResponseBody boolean postReplyComment(@RequestParam("name") String name, @RequestParam("email") String email,@RequestParam("body") String commentcontent,@RequestParam("articleid") Long articleid,@RequestParam("createdby") String createdby,@RequestParam("commentId") Integer commentId){
		try {			
			Comment comment = new Comment();
			Date currentdate = new Date();
			comment.setName(name);
			comment.setEmail(email);
			comment.setBody(commentcontent);
			comment.setCreatedDate(new java.sql.Timestamp(currentdate.getTime()));
			comment.setCreatedBy(createdby);
			Article article = articleservice.findOne(articleid);
			comment.setArticle(article);
			comment.setStatus(true);
			comment.setCommentId(commentId);
			commentservice.save(comment);
			int commentcount = 0;
			int commentcountactive = 0;
			
			if(article.getCommentcount()!=null)
				commentcount = article.getCommentcount();
			article.setCommentcount(commentcount +1);
			if(article.getCommentCountActive()!=null)
				commentcountactive = article.getCommentCountActive();
			article.setCommentCountActive(commentcountactive +1);
			articleservice.save(article);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@RequestMapping(value="/blog/getrelatearticle",method = RequestMethod.POST)
	public ResponseEntity<?>   getrelatearticle(@RequestParam("publishdate") Long publishdate){
		List<Article> lstrelatearticle= new ArrayList<Article>();
		try {
			Date d = new Date(publishdate);
			lstrelatearticle = articleservice.findAllRelatedArticle(d, 5);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  new ResponseEntity<List<Article>>(lstrelatearticle, HttpStatus.BAD_REQUEST);
		}
		 return new ResponseEntity<List<Article>>(lstrelatearticle, HttpStatus.OK);
	}
	
	@RequestMapping(value="/blog/getuserinfo",method = RequestMethod.GET)
	public ResponseEntity<?>   getuserinfo(){
		User user = null;
		CurrentUser currentuser = new CurrentUser();
		try {			
			user = identityService.createUserQuery().userId(CommonUtils.getUserLogedIn()).singleResult();
			currentuser.setDisplayName(user.getFirstName() + " " + user.getLastName());
			currentuser.setEmail(user.getEmail());
			currentuser.setId(user.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  new ResponseEntity<CurrentUser>(currentuser, HttpStatus.BAD_REQUEST);
		}
		 return new ResponseEntity<CurrentUser>(currentuser, HttpStatus.OK);
	}
	
	@RequestMapping(value="/blog/checkpermistion",method = RequestMethod.GET)
	public ResponseEntity<?>   checkpermistion(){
		Map<Long,Boolean> mapPermistion = AuthenticationUtils.getQuyen("07");
		try {			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  new ResponseEntity<Map<Long,Boolean>>(mapPermistion, HttpStatus.BAD_REQUEST);
		}
		 return new ResponseEntity<Map<Long,Boolean>>(mapPermistion, HttpStatus.OK);
	}
	
	
}
class CurrentUser{
	
	String displayname;
	String id;
	String email;
	
	public String getDisplayName() {
		return displayname;
	}
	public void setDisplayName(String displayName) {
		displayname = displayName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}

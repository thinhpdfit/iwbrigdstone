package com.eform.blog.controller;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eform.blog.ProcessDefinitionExtra;
import com.eform.blog.entites.Article;
import com.eform.blog.entites.Comment;
import com.eform.blog.services.ArticleService;
import com.eform.blog.services.CommentService;
import com.eform.common.utils.AuthenticationUtils;
import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HtDonVi;
import com.eform.service.DeploymentHtDonViService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class AdminControler {

	@Autowired
	private ArticleService articleservice;

	@Autowired
	private CommentService commentservice;

	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private DeploymentHtDonViService deploymenthtdonviservice;
	
	@RequestMapping(value="/blog/admin/article_addnew",method = RequestMethod.GET)
    public String articleAddNewPage(){
		//System.out.println("-------------------article_detail");
		try{
			Map<Long,Boolean> mapPermistion = AuthenticationUtils.getQuyen("07");
			if(mapPermistion.get(1L))
				return "blog/admin/article_addnew";
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		return "blog/index";
    }
	
	@RequestMapping(value="/blog/admin/admin_list_article",method = RequestMethod.GET)
    public String adminListArticlePage(){
		//System.out.println("-------------------article_detail");
		try{
			Map<Long,Boolean> mapPermistion = AuthenticationUtils.getQuyen("07");
			if(mapPermistion.get(1L) || mapPermistion.get(2L) || mapPermistion.get(4L) || mapPermistion.get(8L))
				return "blog/admin/admin_list_article";
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		return "blog/index";
    }
	
	@RequestMapping(value="/blog/admin/edit_article/{id}",method = RequestMethod.GET)
    public String edit_articlePage(){
		//System.out.println("-------------------article_detail");
		try{
			Map<Long,Boolean> mapPermistion = AuthenticationUtils.getQuyen("07");
			if(mapPermistion.get(2L) || mapPermistion.get(8L))
				return "blog/admin/edit_article";
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		return "blog/index";
    }
	@RequestMapping(value="/blog/admin/admin_list_comment/{id}",method = RequestMethod.GET)
    public String admin_listcommentPage(){
		//System.out.println("-------------------article_detail");
		try{
			Map<Long,Boolean> mapPermistion = AuthenticationUtils.getQuyen("07");
			if(mapPermistion.get(2L) || mapPermistion.get(8L))
				return "blog/admin/admin_list_comment";
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		return "blog/index";
    }
	
	/**
	 * @param startIndex
	 * @param numberofitem
	 * @return
	 */
	@RequestMapping(value="/blog/listallarticlepaging",method = RequestMethod.POST)
    public ResponseEntity<?>  ListAllArticalPaging(@RequestParam("startIndex") int startIndex,@RequestParam("numberofitem") int numberofitem){
		 String value ="";
		 List<Article>  lstArticle = new ArrayList<Article>();
		 lstArticle = articleservice.findLimitedOrderByCreateDate("",startIndex, numberofitem);
	 	 final OutputStream out = new ByteArrayOutputStream();
	     final ObjectMapper mapper = new ObjectMapper();
	     try {
	    	value =mapper.writeValueAsString(lstArticle);
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
	     return new ResponseEntity<List<Article>>(lstArticle, HttpStatus.OK);
    }
	
	@RequestMapping(value="/blog/getallcommentofarticle",method = RequestMethod.POST)
	public ResponseEntity<?>  getallcommentofarticle(@RequestParam("id") Long id){
		 List<Comment> lstComment = null;
		 String jsonData ="";
	     try {
	    	 //String condition= "where o.article.id = '" + id + "' and o.commentId is Null";
	    	 String condition= "where o.article.id = '" + id + "'";
	    	 lstComment = commentservice.findAllOderByCreateDate(condition,"DESC");
	    	 /*if(lstComment!=null && lstComment.size()>0){
	    		 jsonData +="[";
		    	 for(Comment comment:lstComment){
		    		  jsonData +="{\"id\":\"" + comment.getId() + "\",\"name\":\"" + comment.getName() +"\",\"email\":\"" + comment.getEmail() +"\",\"body\":\"" + comment.getBody() +"\",\"createdDate\":\"" + comment.getCreatedDate() + "\"";
		    		  condition= "where o.commentId.id = '" + comment.getId() + "'";
		    		  List<Comment> lstReply = commentservice.findAllOderByCreateDate(condition,"DESC");
		    		  if(lstReply!=null && lstReply.size() >0){
		    			  jsonData+=",\"replys\":[";
		    			  for(Comment reply:lstReply)
		    			  {
		    				  jsonData+="{\"name\":\"" + reply.getName() +"\",\"email\":\"" + reply.getEmail() +"\",\"body\":\"" + reply.getBody() +"\",\"createdDate\":\"" + reply.getCreatedDate() + "\"},";
		    			  }
		    			  jsonData= jsonData.substring(0, jsonData.length()-1);
		    			  jsonData+="]";
		    		  }
		    		  jsonData+="},";
		    	 }
		    	 jsonData= jsonData.substring(0, jsonData.length()-1);
		    	 jsonData +="]";
	    	 }*/
	    	 //aritcle.getComments();
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<List<Comment>>(lstComment, HttpStatus.BAD_REQUEST);
		 }
	     return new ResponseEntity<List<Comment>>(lstComment, HttpStatus.OK);
    }
	
	/**
	 * @return
	 */
	@RequestMapping(value="/blog/countAllArticle",method = RequestMethod.GET)
    public ResponseEntity<?>  countPublishArticle(){
		
		 long numberofPublishArticle =0;
		 try {
			 	numberofPublishArticle = articleservice.countOnCondition("");
			 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			 }
		 return new ResponseEntity<Long>(numberofPublishArticle, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/blog/admin/createArticle",produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<?> CreateArticle(@RequestBody Article article) {
		System.out.println("OK");
		try {
			String temp = article.getContent();
			//temp = URLDecoder.decode(replacer(temp),"UTF-8");
			temp = StringEscapeUtils.unescapeHtml4(temp);
			article.setContent(temp);
			if (temp.length() > 1100) {
				String subcontent = temp.substring(0, 1000);
				if (!subcontent.endsWith("</p>")) {
					int i = temp.indexOf("</p>", 1000);
					subcontent += temp.substring(1000, i + 4);
					article.setSubcontent(subcontent);
				}
			} else {
				article.setSubcontent(temp);
			}
			articleservice.save(article);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);

	}

	/**
	 * @param article
	 * @return
	 */
	@RequestMapping(value = "/blog/admin/deletearticle", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<?> deleteArticle(@RequestBody Article article) {
		try {
			articleservice.delete(article);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

	/**
	 * @param article
	 * @return
	 */
	@RequestMapping(value = "/blog/admin/updatearticle", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<?> updateArticle(@RequestBody Article article) {
		try {
			String temp = article.getContent();
			temp = StringEscapeUtils.unescapeHtml4(temp);
			article.setContent(temp);
			if (temp.length() > 1100) {
				String subcontent = temp.substring(0, 1000);
				if (!subcontent.endsWith("</p>")) {
					int i = temp.indexOf("</p>", 1000);
					subcontent += temp.substring(1000, i + 4);
					article.setSubcontent(subcontent);
				}
			} else {
				article.setSubcontent(temp);
			}

			Date currentdate = new Date();
			article.setModifiedDate(new java.sql.Timestamp(currentdate.getTime()));
			articleservice.save(article);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

	/**
	 * @param article
	 * @return
	 */
	@RequestMapping(value = "/blog/admin/publisharticle", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<?> publishArticle(@RequestBody Article article) {
		try {
			article.setStatus(true);
			Date currentdate = new Date();
			article.setPublishDate(new java.sql.Timestamp(currentdate.getTime()));
			articleservice.save(article);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

	/**
	 * @param article
	 * @return
	 */
	@RequestMapping(value = "/blog/admin/unpublisharticle", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<?> unPublishArticle(@RequestBody Article article) {
		try {
			article.setStatus(false);
			articleservice.save(article);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/blog/admin/publishcomment", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<?> publishcomment(@RequestBody Comment comment) {
		try {
			comment.setStatus(true);			
			commentservice.save(comment);
			Article article = comment.getArticle();
			int commentcountactive = 0;
			if(article.getCommentCountActive()!=null && article.getCommentCountActive()>0)
			{
				commentcountactive = article.getCommentCountActive();
				article.setCommentCountActive(commentcountactive +1);
				articleservice.save(article);
			}		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

	/**
	 * @param article
	 * @return
	 */
	@RequestMapping(value = "/blog/admin/unpublishcomment", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<?> unpublishcomment(@RequestBody Comment comment) {
		try {
			comment.setStatus(false);
			commentservice.save(comment);
			Article article = comment.getArticle();
			int commentcountactive = 0;
			if(article.getCommentCountActive()!=null && article.getCommentCountActive()>0)
			{
				commentcountactive = article.getCommentCountActive();
				article.setCommentCountActive(commentcountactive -1);
				articleservice.save(article);
			}		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

	@RequestMapping(value = "/blog/admin/deletecomment", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<?> deletecomment(@RequestBody Comment comment) {
		try {
			Article article = comment.getArticle();
			int commentcount = 0;
			int commentcountactive = 0;
			if(article.getCommentcount()!=null && article.getCommentcount()>0)
			{
				commentcount = article.getCommentcount();
				
				article.setCommentcount(commentcount -1);
				
			}			
			if(article.getCommentCountActive()!=null && article.getCommentCountActive()>0)
			{
				
				commentcountactive = article.getCommentCountActive();
				
				article.setCommentCountActive(commentcountactive -1);
			}			
			commentservice.delete(comment);
			articleservice.save(article);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/blog/admin/listprocess",produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<?>  listProcess(@RequestParam("name") String name) {
		String value = "";
		
		List<ProcessDefinition> lstproDef = new ArrayList<ProcessDefinition>();
		if (!"".equals(name)) 
			name= "%" + name +"%";
		else
			name= "%%";
			
		try {
			
			lstproDef = repositoryService.createProcessDefinitionQuery().processDefinitionNameLike(name).list();
			List<ProcessDefinitionExtra> lstpro = new ArrayList<ProcessDefinitionExtra>();
			for(ProcessDefinition item:lstproDef){
				ProcessDefinitionExtra proextra =new ProcessDefinitionExtra();
				proextra.setId(item.getId());
				proextra.setName(item.getName());
				proextra.setKey(item.getKey());
				proextra.setResourceName(item.getResourceName());
				proextra.setCategory(item.getCategory());
				proextra.setDescription(item.getDescription());
				String iframe = "<iframe class='workflow-model-iframe' width='100%' frameborder='0' allowtransparency='true' src='/diagram-viewer/index.html?processDefinitionId=" + proextra.getId() + "'></iframe>";
				proextra.setIframe(iframe);
				List<DeploymentHtDonVi> lstdeploymenthtdonvi = deploymenthtdonviservice.getDeploymentHtDonViFind(item.getDeploymentId()) ;
				String strDonviapdung="<p>Đơn vị áp dụng: ";
				for(DeploymentHtDonVi dv:lstdeploymenthtdonvi){
					strDonviapdung+= dv.getHtDonVi().getTen() + ",";
				}
				
				strDonviapdung+= "</p>";
				proextra.setDonviapdung(strDonviapdung);
				lstpro.add(proextra);
			}
			final OutputStream out = new ByteArrayOutputStream();
		    final ObjectMapper mapper = new ObjectMapper();
		    value =mapper.writeValueAsString(lstpro);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<String>(value, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>(value, HttpStatus.OK);
		
	}


}

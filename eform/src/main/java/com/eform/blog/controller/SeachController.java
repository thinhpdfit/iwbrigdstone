package com.eform.blog.controller;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.eform.blog.entites.Article;
import com.eform.blog.services.ArticleService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class SeachController {

	@Autowired
	private ArticleService articleservice;
	
	@RequestMapping(value="/blog/searchAction",method = RequestMethod.POST)
    public ResponseEntity<?>  searchAction(@RequestParam("q") String q,@RequestParam("startIndex") int startIndex,@RequestParam("numberofitem") int numberofitem){
		 String value ="";
		 List<Article>  lstArticle = new ArrayList<Article>();
		 String conditon = "where o.status=true ";
		 conditon += "AND f_unaccent(o.content) like ('%' || f_unaccent('" + q + "') || '%') ";	 	
	     try {
	    	 lstArticle = articleservice.findLimitedOrderByCreateDate(conditon,startIndex, numberofitem);
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			 return new ResponseEntity<List<Article>>(lstArticle, HttpStatus.BAD_REQUEST);
		 }
	     return new ResponseEntity<List<Article>>(lstArticle, HttpStatus.OK);
    }
}

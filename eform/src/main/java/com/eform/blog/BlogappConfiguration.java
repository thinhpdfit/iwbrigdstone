package com.eform.blog;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import ckeditor.com.upload.FileBrower;
import ckeditor.medias.file;

@Configuration
/*@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.blog", entityManagerFactoryRef = "blogEntityManagerMf", transactionManagerRef = "blogTransactionManager")*/
public class BlogappConfiguration extends WebMvcConfigurerAdapter  {
	
	/*@Autowired
	protected Environment environment;
	@Autowired
	@Qualifier("blogdb")
	DataSource blogdb;
	
	@PersistenceContext(unitName = "blogdb")
    @Bean(name = "blogEntityManagerMf")
    public LocalContainerEntityManagerFactoryBean blogMySqlEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		LocalContainerEntityManagerFactoryBean em =  builder.dataSource(blogdb).persistenceUnit("blogdb").packages("com.blog").build();
		System.out.println("LocalContainerEntityManagerFactoryBean: "+em);
		return em;
     }

	@Bean(name = "blogTransactionManager")
	public JpaTransactionManager blogTransactionManager(@Qualifier("blogEntityManagerMf") final EntityManagerFactory emf) {
	    JpaTransactionManager transactionManager = new JpaTransactionManager();
	    transactionManager.setEntityManagerFactory(emf);
	    return transactionManager;
	}*/
	@Bean
	public ServletRegistrationBean fileBrowerServlet() {
		  FileBrower a1 = new FileBrower();
		     ServletRegistrationBean a = new ServletRegistrationBean(a1, "/file/brower");
		     return a;
	}
	
	@Bean
	public ServletRegistrationBean imageServlet() {
		  file a2 = new file();
		  ServletRegistrationBean b = new ServletRegistrationBean(a2, "/media/*");
		     return b;
	}
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/blog/admin/table_article").setViewName("blog/admin/table_article::content");
		registry.addViewController("/blog/admin/edit_article").setViewName("blog/admin/edit_article::content");
		
		/*registry.addViewController("/hotels/partialsCreate").setViewName("hotels/partialsCreate::content");
		registry.addViewController("/hotels/partialsEdit").setViewName("hotels/partialsEdit::content");
		registry.addViewController("/mappings/partialsMappings").setViewName("mappings/partialsMappings::content");*/
	}
	
}

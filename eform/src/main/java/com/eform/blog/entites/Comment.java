package com.eform.blog.entites;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;



/**
 * The persistent class for the comment database table.
 * 
 */
@Entity
@Table(name="BLOG_COMMENT")
@Cacheable(false)
public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID", nullable = false)
    @SequenceGenerator(name = "BLOG_COMMENT_SEQ_GEN", sequenceName = "BLOG_COMMENT_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BLOG_COMMENT_SEQ_GEN")
	private Integer id;

	
	
	private String body;

	
	@Column(name="created_date")
	private Timestamp createdDate;

	//bi-directional many-to-one association to Article
	@ManyToOne
	@JoinColumn(name = "article_id")
	private Article article;
	@Column(name="comment_id")
	private Integer commentId;
	@Column(name="reply")
	private Integer reply;
	
	@Column(name="name")
	private String name;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="status")
	private boolean status;
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="email")
	private String email;
	
	public Comment() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Article getArticle() {
		return this.article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	public Integer getReply() {
		return this.reply;
	}

	public void setReply(Integer reply) {
		this.reply = reply;
	}
	public Integer getCommentId() {
		return this.commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

}
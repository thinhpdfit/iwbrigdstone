package com.eform.blog.entites;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Array;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the article database table.
 * 
 */
@Entity
@Table(name="BLOG_ARTICLE")
@Cacheable(false)
public class Article implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID", nullable = false)
    @SequenceGenerator(name = "BLOG_ARTICLE_SEQ_GEN", sequenceName = "BLOG_ARTICLE_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BLOG_ARTICLE_SEQ_GEN")
	private Long id;

	private String content;
	
	
	private String subcontent;
	
	public String getSubcontent() {
		return subcontent;
	}

	public void setSubcontent(String subcontent) {
		this.subcontent = subcontent;
	}

	@Column(name = "created_by", length = 100)
	private String createdBy;

	
	@Column(name="created_date")
	private Timestamp createdDate;

	
	@Column(name="modified_date")
	private Timestamp modifiedDate;

	
	@Column(name="publish_date")
	private Timestamp publishDate;

	private Boolean status;
	@Column(name="title", length = 500)
	private String title;

	@Column(name = "view")
	private Integer view;
	
	@Column(name = "commentcount")
	private Integer commentcount;
	
	@Column(name = "comment_count_active")
	private Integer commentCountActive;
	
	
	
	public Integer getCommentCountActive() {
		return commentCountActive;
	}

	public void setCommentCountActive(Integer commentCountActive) {
		this.commentCountActive = commentCountActive;
	}

	public Integer getCommentcount() {
		return commentcount;
	}

	public void setCommentcount(Integer commentcount) {
		this.commentcount = commentcount;
	}

	public Integer getView() {
		return view;
	}

	public void setView(Integer view) {
		this.view = view;
	}
	
	

	public Article() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Timestamp getPublishDate() {
		return this.publishDate;
	}

	public void setPublishDate(Timestamp publishDate) {
		this.publishDate = publishDate;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

}
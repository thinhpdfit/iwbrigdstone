package com.eform.blog.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.eform.blog.entites.Article;



public interface ArticleDao extends CrudRepository<Article, Long>, ArticleDaoCustom{
	
}

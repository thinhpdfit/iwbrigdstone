package com.eform.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.blog.entites.Comment;


@Repository
public class CommentDaoImpl implements CommentDaoCustom {
	@PersistenceContext 
	private EntityManager em;
	@SuppressWarnings("unchecked")

	@Override
	public List<Comment> findLimitedOrderByCreateDate(String condition, int firstResult, int maxResults) {
		String queryString="Select o from Comment o ";
		if(condition!=null)
			queryString+=condition;
		queryString+=" order by o.createdDate DESC";
		Query query = em.createQuery(queryString);
        
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}

	@Override
	public List<Comment> findAllOderByCreateDate(String condition, String Order) {
		String queryString="Select o from Comment o ";
		if(condition!=null)
			queryString+=condition;
		if(!Order.isEmpty())
			queryString+=" order by o.createdDate " + Order;
		else
			queryString+=" order by o.createdDate DESC";
		Query query = em.createQuery(queryString);
        return query.getResultList();
	}
	@Override
	public Long CountAllByCondition(String condition){
		String queryString="Select count(o) from Comment o ";
		if(condition!=null)
			queryString+=condition;
		//queryString+=" order by o.createdDate DESC";
		Query query = em.createQuery(queryString);
        return (long)(query.getSingleResult());
	}

}

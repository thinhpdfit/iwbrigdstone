package com.eform.blog.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.eform.blog.entites.Article;



public interface  ArticleDaoCustom {
	public List<Article> findLimitedOrderByCreateDate(String condition,int firstResult, int maxResults);
	public List<Article> findAllOderByCreateDate(String condition);
	public List<Article> findRelatedArticle(Date publishdate,int maxResults);
	public List<Article> findLimitedOrderByView(String condition,int firstResult, int maxResults);
	public List<Article> findLimitedOrderByComment(String condition,int firstResult, int maxResults);
	public long countOnCondition(String condition);

	

}

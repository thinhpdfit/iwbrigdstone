package com.eform.blog.dao;

import java.util.List;

import com.eform.blog.entites.Comment;





public interface CommentDaoCustom {
	public List<Comment> findLimitedOrderByCreateDate(String condition,int firstResult, int maxResults);
	public List<Comment> findAllOderByCreateDate(String condition, String Order);
	public Long CountAllByCondition(String condition);
}

package com.eform.blog.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.blog.entites.Comment;



public interface CommentDao extends CrudRepository<Comment, Long>,CommentDaoCustom {

}

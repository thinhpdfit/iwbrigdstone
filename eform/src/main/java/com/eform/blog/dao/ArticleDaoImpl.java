package com.eform.blog.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.blog.entites.Article;


@Repository
public class ArticleDaoImpl implements ArticleDaoCustom {
	@PersistenceContext 
	private EntityManager em;
	@SuppressWarnings("unchecked")
	@Override
	public List<Article> findLimitedOrderByCreateDate(String condition,int firstResult, int maxResults) {
		String queryString="Select o from Article o ";
		if(condition!=null)
			queryString+=condition;
		queryString+=" order by o.createdDate DESC";
		Query query = em.createQuery(queryString);
        
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}
	@Override
	public List<Article> findAllOderByCreateDate(String condition) {
		String queryString="Select o from Article o ";
		if(condition!=null)
			queryString+=condition;
		queryString+=" order by o.createdDate DESC";
		Query query = em.createQuery(queryString);
        return query.getResultList();
	}
	
	@Override
	public List<Article> findRelatedArticle(Date publishdate,int maxResults) {
		String queryString="Select o from Article o ";
		String condition ="where o.status=true and o.publishDate < :p1";	
		queryString +=condition;
		queryString+=" order by o.createdDate DESC";
		Query query = em.createQuery(queryString);
		 if (maxResults > 0) {
	            query = query.setMaxResults(maxResults);
	        }
		query.setParameter("p1", publishdate);
        return query.getResultList();
	}
	
	@Override
	public long countOnCondition(String condition){
		String queryString="Select count(o) from Article o ";
		if(condition!=null)
			queryString+=condition;
		//queryString+=" order by o.createdDate DESC";
		Query query = em.createQuery(queryString);
        return (long)(query.getSingleResult());
	}

	@Override
	public List<Article> findLimitedOrderByView(String condition,int firstResult, int maxResults){
		String queryString="Select o from Article o ";
		if(condition!=null)
			queryString+=condition;
		queryString+=" order by o.view DESC";
		Query query = em.createQuery(queryString);
        
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}
	@Override
	public List<Article> findLimitedOrderByComment(String condition,int firstResult, int maxResults){
		String queryString="Select o from Article o ";
		if(condition!=null)
			queryString+=condition;
		queryString+=" order by o.commentCountActive DESC";
		Query query = em.createQuery(queryString);
        
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}

}

package com.eform.sharepoint.type;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ListItems extends ArrayList<Item>{

	public ListItems(){
		super();
	}
	public ListItems(Element listitemsE){
		super();
		parse(listitemsE);
	}
	
	public void parse(Element listitemsE){
		if(listitemsE!=null){
			
			NodeList rowNl=listitemsE.getElementsByTagName("z:row");
			if(rowNl!=null){
				for(int rowNlI=0,rowNlN=rowNl.getLength();rowNlI<rowNlN;rowNlI++){
					Node rowN=rowNl.item(rowNlI);
					if(rowN!=null&&rowN instanceof Element){
						Element rowE=(Element)rowN;
						add(new Item(rowE));
					}
				}
			}
			
		}
	}
}

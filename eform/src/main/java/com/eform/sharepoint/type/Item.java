package com.eform.sharepoint.type;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Element;

public class Item{
	private boolean folder;
	private String name;

	public Item(){
		super();
	}
	public Item(Element row){
		super();
	parse(row);
}

public void parse(Element row){
	Pattern folderP = Pattern.compile("[0-9]+;#1");
	Pattern nameP = Pattern.compile("[0-9]+;#1");
	if(row!=null){
		String owsFSObjType=row.getAttribute("ows_FSObjType");
		 Matcher folderM = folderP.matcher(owsFSObjType);
		folder=folderM.matches();
		name=row.getAttribute("ows_LinkFilename");
	}
}
public boolean isFolder() {
	return folder;
}
public void setFolder(boolean folder) {
	this.folder = folder;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

}
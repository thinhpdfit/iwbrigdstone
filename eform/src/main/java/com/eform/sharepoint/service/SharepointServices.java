package com.eform.sharepoint.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.eform.sharepoint.type.ListItems;

public class SharepointServices {
	HttpClient httpClient = HttpClientBuilder.create().build();
	private String scheme;
	private String host;
	private int port;
	private String path;
	private String username;
	private String password;
	public SharepointServices(String scheme,String host,int port,String path,String username,String password){
		this.scheme=scheme;
		this.host=host;
		this.port=port;
		this.path=path;
		this.username=username;
		this.password=password;
	}
	
	private HttpUriRequest createFolderRequest(String url) throws Exception{
		URI uri=new URI(scheme,null,host,port,path+"/Dws.asmx",null,null);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.newDocument();
		Element envelope=doc.createElement("soap:Envelope");
		envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		envelope.setAttribute("xmlns:soap","http://schemas.xmlsoap.org/soap/envelope/");
		doc.appendChild(envelope);

		Element bodyE=doc.createElement("soap:Body");
		envelope.appendChild(bodyE);
		Element createFolderE=doc.createElement("CreateFolder");
		createFolderE.setAttribute("xmlns", "http://schemas.microsoft.com/sharepoint/soap/dws/");
		bodyE.appendChild(createFolderE);
		Element urlE=doc.createElement("url");
		urlE.setTextContent(url);
		createFolderE.appendChild(urlE);
		StringEntity entity =new StringEntity(getDocumentString(doc,false),"UTF-8");
		RequestBuilder builder=RequestBuilder.post()
				.addHeader("Content-Type", "text/xml; charset=utf-8")
				.addHeader("Authorization", "Basic "+Base64.encodeBase64String((username+":"+password).getBytes()))
				.addHeader("SOAPAction", "\"http://schemas.microsoft.com/sharepoint/soap/dws/CreateFolder\"")
				.setEntity(entity)
				.setUri(uri);
		HttpUriRequest request=builder.build();
		return request;
	}
	
	public String createFolder(String url) throws Exception{
		HttpUriRequest request=createFolderRequest(url);
		HttpResponse response = httpClient.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();
        if(responseCode==200){
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document doc = db.parse(response.getEntity().getContent());
		NodeList nodeList=doc.getElementsByTagName("CreateFolderResult");
		if(nodeList!=null){
			for(int index=0,num=nodeList.getLength();index<num;index++){
				Node node=nodeList.item(index);
				if(node!=null&&node instanceof Element){
					Element createFolderResultE=(Element)node;
					String xmlStr= createFolderResultE.getTextContent();
					Document resDoc=db.parse(new ByteArrayInputStream(xmlStr.getBytes()));
					NodeList errorNl=resDoc.getElementsByTagName("Error");
					if(errorNl!=null&&errorNl.getLength()>0){
						for(int errorNlI=0,errorNlN=errorNl.getLength();errorNlI<errorNlN;errorNlI++){
							Node errorN=errorNl.item(errorNlI);
							if(errorN!=null&&errorN instanceof Element){
								Element errorE=(Element)errorN;
								return errorE.getTextContent();
							}
						}
					}
				}
			}
		}
        }else{
        	System.out.println(response.getStatusLine().getStatusCode());            
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
        }
        return null;
	}
	
	private HttpUriRequest deleteFolderRequest(String url) throws Exception{
		URI uri=new URI(scheme,null,host,port,path+"/Dws.asmx",null,null);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.newDocument();
		Element envelope=doc.createElement("soap:Envelope");
		envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		envelope.setAttribute("xmlns:soap","http://schemas.xmlsoap.org/soap/envelope/");
		doc.appendChild(envelope);

		Element bodyE=doc.createElement("soap:Body");
		envelope.appendChild(bodyE);
		Element deleteFolderE=doc.createElement("DeleteFolder");
		deleteFolderE.setAttribute("xmlns", "http://schemas.microsoft.com/sharepoint/soap/dws/");
		bodyE.appendChild(deleteFolderE);
		Element urlE=doc.createElement("url");
		urlE.setTextContent(url);
		deleteFolderE.appendChild(urlE);
		StringEntity entity =new StringEntity(getDocumentString(doc,false),"UTF-8");
		RequestBuilder builder=RequestBuilder.post()
				.addHeader("Content-Type", "text/xml; charset=utf-8")
				.addHeader("Authorization", "Basic "+Base64.encodeBase64String((username+":"+password).getBytes()))
				.addHeader("SOAPAction", "\"http://schemas.microsoft.com/sharepoint/soap/dws/DeleteFolder\"")
				.setEntity(entity)
				.setUri(uri);
		HttpUriRequest request=builder.build();
		return request;
	}
	
	public String deleteFolder(String url) throws Exception{
		HttpUriRequest request=deleteFolderRequest(url);
		HttpResponse response = httpClient.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();
        if(responseCode==200){
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document doc = db.parse(response.getEntity().getContent());
		NodeList nodeList=doc.getElementsByTagName("DeleteFolderResult");
		if(nodeList!=null){
			for(int index=0,num=nodeList.getLength();index<num;index++){
				Node node=nodeList.item(index);
				if(node!=null&&node instanceof Element){
					Element deleteFolderResultE=(Element)node;
					return deleteFolderResultE.getTextContent();
				}
			}
		}
        }else{
        	System.out.println(response.getStatusLine().getStatusCode());            
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
        }
        return null;
	}
	//Documents
	private HttpUriRequest getListRequest(String listName) throws Exception{
		URI uri=new URI(scheme,null,host,port,path+"/lists.asmx",null,null);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.newDocument();
		Element envelope=doc.createElement("soap:Envelope");
		envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		envelope.setAttribute("xmlns:soap","http://schemas.xmlsoap.org/soap/envelope/");
		doc.appendChild(envelope);

		Element bodyE=doc.createElement("soap:Body");
		envelope.appendChild(bodyE);
		Element getListE=doc.createElement("GetList");
		getListE.setAttribute("xmlns", "http://schemas.microsoft.com/sharepoint/soap/");
		bodyE.appendChild(getListE);
		Element listNameE=doc.createElement("listName");
		listNameE.setTextContent(listName);
		getListE.appendChild(listNameE);
		StringEntity entity =new StringEntity(getDocumentString(doc,false),"UTF-8");
		RequestBuilder builder=RequestBuilder.post()
				.addHeader("Content-Type", "text/xml; charset=utf-8")
				.addHeader("Authorization", "Basic "+Base64.encodeBase64String((username+":"+password).getBytes()))
				.addHeader("SOAPAction", "\"http://schemas.microsoft.com/sharepoint/soap/GetList\"")
				.setEntity(entity)
				.setUri(uri);
		HttpUriRequest request=builder.build();
		return request;
	}
	
	public String getList(String listName) throws Exception{
		HttpUriRequest request=getListRequest(listName);
		HttpResponse response = httpClient.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();
        if(responseCode==200){
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document doc = db.parse(response.getEntity().getContent());
		System.out.println(getDocumentString(doc,false));
		NodeList nodeList=doc.getElementsByTagName("GetListResult");
		if(nodeList!=null){
 			for(int index=0,num=nodeList.getLength();index<num;index++){
				Node node=nodeList.item(index);
				if(node!=null&&node instanceof Element){
					Element getListResultE=(Element)node;
					return getListResultE.getTextContent();
				}
			}
		}
        }else{
        	System.out.println(response.getStatusLine().getStatusCode());            
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
        }
        return null;
	}
	
	public Element createQueryOptions(String folder) throws Exception{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	DocumentBuilder db = dbf.newDocumentBuilder(); 
	Document doc = db.newDocument();
		Element queryOptionsE=doc.createElement("QueryOptions");
			Element folderE=doc.createElement("Folder");
			folderE.setTextContent(folder);
			queryOptionsE.appendChild(folderE);
			return queryOptionsE;
	}
	
	private HttpUriRequest getListItemsRequest(String listName,String viewName,String query,String viewFields,String rowLimit,Element queryOptionsE,String webID) throws Exception{
		URI uri=new URI(scheme,null,host,port,path+"/lists.asmx",null,null);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.newDocument();
		Element envelope=doc.createElement("soap:Envelope");
		envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		envelope.setAttribute("xmlns:soap","http://schemas.xmlsoap.org/soap/envelope/");
		doc.appendChild(envelope);

		Element bodyE=doc.createElement("soap:Body");
		envelope.appendChild(bodyE);
		Element getListItemsE=doc.createElement("GetListItems");
		getListItemsE.setAttribute("xmlns", "http://schemas.microsoft.com/sharepoint/soap/");
		bodyE.appendChild(getListItemsE);
		if(listName!=null&&listName.isEmpty()==false){
		Element listNameE=doc.createElement("listName");
		listNameE.setTextContent(listName);
		getListItemsE.appendChild(listNameE);
		}
		if(viewName!=null&&viewName.isEmpty()==false){
		Element viewNameE=doc.createElement("viewName");
		viewNameE.setTextContent(viewName);
		getListItemsE.appendChild(viewNameE);
		}
		if(query!=null&&query.isEmpty()==false){
		Element queryE=doc.createElement("query");
		queryE.setTextContent(query);
		getListItemsE.appendChild(queryE);
		}
		if(viewFields!=null&&viewFields.isEmpty()==false){
		Element viewFieldsE=doc.createElement("viewFields");
		viewFieldsE.setTextContent(viewFields);
		getListItemsE.appendChild(viewFieldsE);
		}
		if(rowLimit!=null&&rowLimit.isEmpty()==false){
		Element rowLimitE=doc.createElement("rowLimit");
		rowLimitE.setTextContent(rowLimit);
		getListItemsE.appendChild(rowLimitE);
		}
		if(queryOptionsE!=null){
			Element queryOptionsWrapperE=doc.createElement("queryOptions");
			getListItemsE.appendChild(queryOptionsWrapperE);
			queryOptionsWrapperE.appendChild(doc.importNode(queryOptionsE, true));
		}
		if(webID!=null&&webID.isEmpty()==false){
		Element webIDE=doc.createElement("webID");
		webIDE.setTextContent(webID);
		getListItemsE.appendChild(webIDE);
		}
		StringEntity entity =new StringEntity(getDocumentString(doc,false),"UTF-8");
		RequestBuilder builder=RequestBuilder.post()
				.addHeader("Content-Type", "text/xml; charset=utf-8")
				.addHeader("Authorization", "Basic "+Base64.encodeBase64String((username+":"+password).getBytes()))
				.addHeader("SOAPAction", "\"http://schemas.microsoft.com/sharepoint/soap/GetListItems\"")
				.setEntity(entity)
				.setUri(uri);
		HttpUriRequest request=builder.build();
		return request;
	}
	
	public ListItems getListItems(String listName,String viewName,String query,String viewFields,String rowLimit,Element queryOptionsE,String webID) throws Exception{
		HttpUriRequest request=getListItemsRequest(listName,viewName,query,viewFields,rowLimit,queryOptionsE,webID);
		HttpResponse response = httpClient.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();
        if(responseCode==200){
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document doc = db.parse(response.getEntity().getContent());
		getDocumentString(doc, true);
		NodeList resultNl=doc.getElementsByTagName("GetListItemsResult");
		if(resultNl!=null){
			for(int resultNlI=0,resultNlN=resultNl.getLength();resultNlI<resultNlN;resultNlI++){
				Node resultN=resultNl.item(resultNlI);
				if(resultN!=null&&resultN instanceof Element){
					Element resultE=(Element)resultN;
					NodeList listitemsNl=resultE.getElementsByTagName("listitems");
					if(listitemsNl!=null){
						for(int listitemsNlI=0,listitemsNlN=listitemsNl.getLength();listitemsNlI<listitemsNlN;listitemsNlI++){
							Node listitemsN=listitemsNl.item(listitemsNlI);
							if(listitemsN!=null&&listitemsN instanceof Element){
								Element listitemsE=(Element)listitemsN;
								ListItems listItems=new ListItems(listitemsE);
								return listItems;
							}
						}
					}
				}
			}
		}
        }else{
        	System.out.println(response.getStatusLine().getStatusCode());            
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
        }
        return null;
	}

	private HttpUriRequest updateListItemsRequest(String listName,Element updatesE) throws Exception{
		URI uri=new URI(scheme,null,host,port,path+"/lists.asmx",null,null);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.newDocument();
		Element envelope=doc.createElement("soap:Envelope");
		envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		envelope.setAttribute("xmlns:soap","http://schemas.xmlsoap.org/soap/envelope/");
		doc.appendChild(envelope);

		Element bodyE=doc.createElement("soap:Body");
		envelope.appendChild(bodyE);
		Element getListItemsE=doc.createElement("UpdateListItems");
		getListItemsE.setAttribute("xmlns", "http://schemas.microsoft.com/sharepoint/soap/");
		bodyE.appendChild(getListItemsE);
		if(listName!=null&&listName.isEmpty()==false){
		Element listNameE=doc.createElement("listName");
		listNameE.setTextContent(listName);
		getListItemsE.appendChild(listNameE);
		}
		if(updatesE!=null){
			Element updatesWrapperE=doc.createElement("updates");
			getListItemsE.appendChild(updatesWrapperE);
			updatesWrapperE.appendChild(doc.importNode(updatesE, true));
		}
		StringEntity entity =new StringEntity(getDocumentString(doc,false),"UTF-8");
		RequestBuilder builder=RequestBuilder.post()
				.addHeader("Content-Type", "text/xml; charset=utf-8")
				.addHeader("Authorization", "Basic "+Base64.encodeBase64String((username+":"+password).getBytes()))
				.addHeader("SOAPAction", "\"http://schemas.microsoft.com/sharepoint/soap/UpdateListItems\"")
				.setEntity(entity)
				.setUri(uri);
		HttpUriRequest request=builder.build();
		return request;
	}
	
	public ListItems updateListItems(String listName,Element updatesE) throws Exception{
		HttpUriRequest request=updateListItemsRequest(listName,updatesE);
		HttpResponse response = httpClient.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();
        if(responseCode==200){
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document doc = db.parse(response.getEntity().getContent());
		getDocumentString(doc, true);
		NodeList resultNl=doc.getElementsByTagName("GetListItemsResult");
		if(resultNl!=null){
			for(int resultNlI=0,resultNlN=resultNl.getLength();resultNlI<resultNlN;resultNlI++){
				Node resultN=resultNl.item(resultNlI);
				if(resultN!=null&&resultN instanceof Element){
					Element resultE=(Element)resultN;
					NodeList listitemsNl=resultE.getElementsByTagName("listitems");
					if(listitemsNl!=null){
						for(int listitemsNlI=0,listitemsNlN=listitemsNl.getLength();listitemsNlI<listitemsNlN;listitemsNlI++){
							Node listitemsN=listitemsNl.item(listitemsNlI);
							if(listitemsN!=null&&listitemsN instanceof Element){
								Element listitemsE=(Element)listitemsN;
								ListItems listItems=new ListItems(listitemsE);
								return listItems;
							}
						}
					}
				}
			}
		}
        }else{
        	System.out.println(response.getStatusLine().getStatusCode());            
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
        }
        return null;
	}

	public Element createFieldInformation(String displayName,String type,String value) throws Exception{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.newDocument();
		Element fieldInformationE=doc.createElement("FieldInformation");
		fieldInformationE.setAttribute("DisplayName",displayName);
		fieldInformationE.setAttribute("Type",type);
		fieldInformationE.setAttribute("Value",value);
			return fieldInformationE;
	}
	
	private HttpUriRequest copyIntoItemsRequest(String sourceUrl,List<String> destinationUrls,List<Element> fields,File file) throws Exception{
		URI uri=new URI(scheme,null,host,port,path+"/copy.asmx",null,null);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.newDocument();
		Element envelope=doc.createElement("soap:Envelope");
		envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		envelope.setAttribute("xmlns:soap","http://schemas.xmlsoap.org/soap/envelope/");
		doc.appendChild(envelope);

		Element bodyE=doc.createElement("soap:Body");
		envelope.appendChild(bodyE);
		Element copyIntoItemsE=doc.createElement("CopyIntoItems");
		copyIntoItemsE.setAttribute("xmlns", "http://schemas.microsoft.com/sharepoint/soap/");
		bodyE.appendChild(copyIntoItemsE);
		if(sourceUrl!=null&&sourceUrl.isEmpty()==false){
		Element sourceUrlE=doc.createElement("SourceUrl");
		sourceUrlE.setTextContent(sourceUrl);
		copyIntoItemsE.appendChild(sourceUrlE);
		}
		if(destinationUrls!=null&&destinationUrls.isEmpty()==false){
		Element destinationUrlsE=doc.createElement("DestinationUrls");
		copyIntoItemsE.appendChild(destinationUrlsE);
		for(String destinationUrl:destinationUrls){
			Element stringE=doc.createElement("string");
			stringE.setTextContent(destinationUrl);
			destinationUrlsE.appendChild(stringE);
		}
		}
		if(fields!=null){
		Element fieldsE=doc.createElement("Fields");
		for(Element field:fields){
			fieldsE.appendChild(doc.importNode(field, true));
		}
		copyIntoItemsE.appendChild(fieldsE);
		}
		if(file!=null&&file.exists()){
			ByteArrayOutputStream os=new ByteArrayOutputStream();
			FileInputStream fis=new FileInputStream(file);
			byte[] b=new byte[1024];
			int len=0;
			while((len=fis.read(b))!=-1){
				os.write(b, 0, len);
			}
			
		Element streamE=doc.createElement("Stream");
		streamE.setTextContent(Base64.encodeBase64String(os.toByteArray()));
		copyIntoItemsE.appendChild(streamE);
		}
		StringEntity entity =new StringEntity(getDocumentString(doc,false),"UTF-8");
		RequestBuilder builder=RequestBuilder.post()
				.addHeader("Content-Type", "text/xml; charset=utf-8")
				.addHeader("Authorization", "Basic "+Base64.encodeBase64String((username+":"+password).getBytes()))
				.addHeader("SOAPAction", "\"http://schemas.microsoft.com/sharepoint/soap/CopyIntoItems\"")
				.setEntity(entity)
				.setUri(uri);
		HttpUriRequest request=builder.build();
		return request;
	}
	
	public String copyIntoItems(String sourceUrl,List<String> destinationUrls,List<Element> fields,File file) throws Exception{
		HttpUriRequest request=copyIntoItemsRequest(sourceUrl,destinationUrls,fields,file);
		HttpResponse response = httpClient.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();
        if(responseCode==200){
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document doc = db.parse(response.getEntity().getContent());
		getDocumentString(doc, false);
		NodeList resultNl=doc.getElementsByTagName("CopyIntoItemsResult");
		if(resultNl!=null){
			for(int resultNlI=0,resultNlN=resultNl.getLength();resultNlI<resultNlN;resultNlI++){
				Node resultN=resultNl.item(resultNlI);
				if(resultN!=null&&resultN instanceof Element){
					Element resultE=(Element)resultN;
					String resultStr=resultE.getTextContent();
					if(resultStr==null||resultStr.equals("0")==false){
						return "Error";
					}
				}
			}
		}
        }else{
        	System.out.println(response.getStatusLine().getStatusCode());            
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
        }
        return null;
	}

	private HttpUriRequest addAttachmentRequest(String listName,String listItemID,String fileName,File file) throws Exception{
		URI uri=new URI(scheme,null,host,port,path+"/lists.asmx",null,null);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.newDocument();
		Element envelope=doc.createElement("soap:Envelope");
		envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		envelope.setAttribute("xmlns:soap","http://schemas.xmlsoap.org/soap/envelope/");
		doc.appendChild(envelope);

		Element bodyE=doc.createElement("soap:Body");
		envelope.appendChild(bodyE);
		Element addAttachmentE=doc.createElement("AddAttachment");
		addAttachmentE.setAttribute("xmlns", "http://schemas.microsoft.com/sharepoint/soap/");
		bodyE.appendChild(addAttachmentE);
		if(listName!=null&&listName.isEmpty()==false){
		Element listNameE=doc.createElement("listName");
		listNameE.setTextContent(listName);
		addAttachmentE.appendChild(listNameE);
		}
		if(listItemID!=null&&listItemID.isEmpty()==false){
		Element listItemIDE=doc.createElement("listItemID");
		listItemIDE.setTextContent(listItemID);
		addAttachmentE.appendChild(listItemIDE);
		}
		if(fileName!=null&&fileName.isEmpty()==false){
		Element fileNameE=doc.createElement("fileName");
		fileNameE.setTextContent(fileName);
		addAttachmentE.appendChild(fileNameE);
		}
		if(file!=null&&file.exists()){
			ByteArrayOutputStream os=new ByteArrayOutputStream();
			FileInputStream fis=new FileInputStream(file);
			byte[] b=new byte[1024];
			int len=0;
			while((len=fis.read(b))!=-1){
				os.write(b, 0, len);
			}
			
		Element attachmentE=doc.createElement("attachment");
		attachmentE.setTextContent(Base64.encodeBase64String(os.toByteArray()));
		addAttachmentE.appendChild(attachmentE);
		}
		StringEntity entity =new StringEntity(getDocumentString(doc,false),"UTF-8");
		RequestBuilder builder=RequestBuilder.post()
				.addHeader("Content-Type", "text/xml; charset=utf-8")
				.addHeader("Authorization", "Basic "+Base64.encodeBase64String((username+":"+password).getBytes()))
				.addHeader("SOAPAction", "\"http://schemas.microsoft.com/sharepoint/soap/AddAttachment\"")
				.setEntity(entity)
				.setUri(uri);
		HttpUriRequest request=builder.build();
		return request;
	}
	
	public ListItems addAttachment(String listName,String listItemID,String fileName,File file)throws Exception{
		HttpUriRequest request=addAttachmentRequest(listName, listItemID, fileName, file);
		HttpResponse response = httpClient.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();
        if(responseCode==200){
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document doc = db.parse(response.getEntity().getContent());
		getDocumentString(doc, false);
		NodeList resultNl=doc.getElementsByTagName("GetListItemsResult");
		if(resultNl!=null){
			for(int resultNlI=0,resultNlN=resultNl.getLength();resultNlI<resultNlN;resultNlI++){
				Node resultN=resultNl.item(resultNlI);
				if(resultN!=null&&resultN instanceof Element){
					Element resultE=(Element)resultN;
					NodeList listitemsNl=resultE.getElementsByTagName("listitems");
					if(listitemsNl!=null){
						for(int listitemsNlI=0,listitemsNlN=listitemsNl.getLength();listitemsNlI<listitemsNlN;listitemsNlI++){
							Node listitemsN=listitemsNl.item(listitemsNlI);
							if(listitemsN!=null&&listitemsN instanceof Element){
								Element listitemsE=(Element)listitemsN;
								ListItems listItems=new ListItems(listitemsE);
								return listItems;
							}
						}
					}
				}
			}
		}
        }else{
        	System.out.println(response.getStatusLine().getStatusCode());            
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
        }
        return null;
	}
	
	public Element createUpdates(Map<String,String> fields) throws Exception{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	DocumentBuilder db = dbf.newDocumentBuilder(); 
	Document doc = db.newDocument();
		Element batchE=doc.createElement("Batch");
			Element methodE=doc.createElement("Method");
			methodE.setAttribute("ID", "1");
			methodE.setAttribute("Cmd", "New");
			batchE.appendChild(methodE);
			if(fields!=null){
			Iterator<String> keyI=fields.keySet().iterator();
			while(keyI.hasNext()){
				String key=keyI.next();
				String value=fields.get(key);
				Element fieldE=doc.createElement("Field");
				fieldE.setAttribute("Name",key);
				fieldE.setTextContent(value);
				methodE.appendChild(fieldE);
			}
			}
			return batchE;
	}
	
	

	public String getDocumentString(Document doc,boolean omitXmlDeclaration) throws Exception{
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		if(omitXmlDeclaration){
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		}
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(doc), new StreamResult(writer));
		String str= writer.getBuffer().toString();
		System.out.println(str);
		return str;
	}

}

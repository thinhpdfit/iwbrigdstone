package com.eform.demo.entites;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DEMO_TTNV")
@SequenceGenerator(sequenceName = "THONGTINNHANVIEN_SEQ", name = "THONGTINNHANVIEN_SEQ_GEN", initialValue = 1, allocationSize = 1)
public class ThongTinNhanVien implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(generator = "THONGTINNHANVIEN_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	private Long id;

	private Timestamp thoigianvao;
	private Timestamp thoigianra;
	private String hoten;
	private String lydo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "thoigianvao")
	public Timestamp getThoigianvao() {
		return thoigianvao;
	}

	public void setThoigianvao(Timestamp thoigianvao) {
		this.thoigianvao = thoigianvao;
	}

	@Column(name = "thoigianra")
	public Timestamp getThoigianra() {
		return thoigianra;
	}

	public void setThoigianra(Timestamp thoigianra) {
		this.thoigianra = thoigianra;
	}

	@Column(name = "lydo", length = 2000)
	public String getLydo() {
		return lydo;
	}

	public void setLydo(String lydo) {
		this.lydo = lydo;
	}

	public String getHoten() {
		return hoten;
	}

	public void setHoten(String hoten) {
		this.hoten = hoten;
	}
}
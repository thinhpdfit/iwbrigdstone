package com.eform.demo.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.demo.entites.ThongTinNhanVien;

@Repository
public interface ThongTinNhanVienRes extends CrudRepository<ThongTinNhanVien, Long>  {

	
}


package com.eform.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomErrorController implements ErrorController {
	private static final String PATH = "/error";

	@RequestMapping(value = PATH)
	public String error(HttpServletRequest request, HttpServletResponse response, Model model) {
		model.addAttribute("errorCode", response.getStatus());
		return "error";
	}

	@Override
	public String getErrorPath() {
		return PATH;
	}

}
package com.eform.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.User;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.eform.common.Constants;
import com.eform.common.ThamSo;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.FileUtils;
import com.eform.common.utils.FormUtils;
import com.eform.common.utils.SignatureUtils;
import com.eform.model.entities.EmailPheDuyet;
import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;
import com.eform.model.entities.HoSo;
import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.Signature;
import com.eform.service.EmailPheDuyetServices;
import com.eform.service.FileManagementService;
import com.eform.service.HoSoService;
import com.eform.service.HtThamSoService;
import com.eform.service.SignatureService;

@Controller
@RequestMapping("/mail-approve")
public class MailTaskApproveController {
	@Autowired
	private EmailPheDuyetServices emailPheDuyetServices;
	@Autowired
	private TaskService taskService;
	@Autowired
	 protected transient FormService formService;
	@Autowired
	private HoSoService hoSoService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private FileManagementService fileManagementService;
	@Autowired
	private SignatureService signatureService;
	@Autowired
	protected IdentityService identityService;
	@Autowired
	private HtThamSoService htThamSoService;
	private String uploadPath;
	
	@RequestMapping(value = "/approve/{refCode}/{idVar}/{approve}", method=RequestMethod.GET)
	public String approve(@PathVariable("refCode") String refCode, 
			@PathVariable("idVar") Long idVar, @PathVariable("approve") Long approve, 
			@RequestParam(value = "k", required = false) String key, 
			@RequestParam(value = "v", required = false) String value, 
			ModelMap modelMap){
		try {
			uploadPath = "";
			
			HtThamSo uploadPathParam = htThamSoService.getHtThamSoFind(ThamSo.FOLDER_UPLOAD_PATH);
			if(uploadPathParam != null){
				uploadPath = uploadPathParam.getGiaTri();
			}
			
			if(refCode != null && idVar != null){
				EmailPheDuyet emailPheDuyet = emailPheDuyetServices.getEmailPheDuyetFind(idVar, refCode, 1L);
				if(emailPheDuyet != null){
					Task taskCurrent = taskService.createTaskQuery().processInstanceId(emailPheDuyet.getProcessInstanceId()).taskDefinitionKey(emailPheDuyet.getActivityId()).singleResult();
					if(taskCurrent != null){
						String userAssigned = taskCurrent.getAssignee();
						
						//Nếu approve thì ký số trên file
						if(approve != null && approve.intValue() == 1){
							String attachStr = emailPheDuyet.getTruongKySo();
							if(attachStr != null){
								String attachArr[] = attachStr.split(",");
								if (attachArr != null && attachArr.length > 0) {
									for (int i = 0; i < attachArr.length; i++) {
										if (attachArr[i] != null && !attachArr[i].trim().isEmpty()) {
											Object v = runtimeService.getVariable(taskCurrent.getProcessInstanceId(), attachArr[i].trim());
											if (v != null && v instanceof String) {
												String str = (String) v;
												String fileIfArr[] = str.split(",");
												if (fileIfArr != null && fileIfArr.length > 0) {
													
													String pdfPath = "";
													for (int j = 0; j < fileIfArr.length; j++) {
														if (fileIfArr[j] != null && fileIfArr[j].trim().matches("[0-9]+")) {
															FileManagement fileManagement = fileManagementService.findOne(new Long(fileIfArr[j].trim()));
															
															
															
															Date date = new Date();
															SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmssSSS");
															String prefix = df.format(date)+"_";
															
															FileVersion versionMax = fileManagementService.getMaxVersion(fileManagement);
															List<Signature> sigList = signatureService.getSignatureFind(versionMax);
															int thuTuCurrent = 1;
															if(sigList != null && !sigList.isEmpty()){
																thuTuCurrent = sigList.size()+1;
															}
															
															if (versionMax.getTrangThai() != null
																	&& versionMax.getTrangThai().intValue() == 1
																	&& versionMax.getSignedPath() != null) {
																pdfPath = versionMax.getSignedPath();
															} else if (versionMax.getUploadPath() != null) {
																if("pdf".equals(versionMax.getDinhDang())){
																	pdfPath = uploadPath + versionMax.getUploadPath();
																}else{
																	
																	String docPath = uploadPath + versionMax.getUploadPath();
																	pdfPath = prefix + fileManagement.getTieuDe()+".pdf";
																	FileUtils.createPdf(docPath, uploadPath + pdfPath);
																}
															}
															
															String signedPath = prefix + fileManagement.getTieuDe()+"signature"+thuTuCurrent+".pdf";
															String userName = "";
															if(userAssigned != null){
																User u = identityService.createUserQuery().userId(userAssigned).singleResult();
																if(u!= null){
																	userName = u.getFirstName()+" "+u.getLastName();
																}
															}
															
															//SignatureUtils.signPdf("/"+uploadPath + pdfPath, "/"+uploadPath + signedPath, userName, thuTuCurrent);
															FileUtils.removeFile(uploadPath + pdfPath);
															FileUtils.uploadToDocumentList(uploadPath+signedPath, signedPath, fileManagement.getTieuDe(), fileManagement.getTieuDe());
															versionMax.setTrangThai(1L);
															versionMax.setSignedPath(signedPath);
															Signature signature = new Signature();
															signature.setFileVersion(versionMax);
															signature.setGhiChu(null);
															signature.setNgayKy(new Timestamp(date.getTime()));
															signature.setNguoiKy(userAssigned);
															signature.setThuTu(new Long(thuTuCurrent));
															signatureService.save(versionMax, signature);
															
														}
													}
													
												}
											}
										}
									}
								}
							}
							
						}
						if(key != null && value != null){
							//Map<String, String> properties = new HashMap<String, String>();
							//properties.put(key, value);
//								formService.submitTaskFormData(taskCurrent.getId(), properties);
							runtimeService.setVariable(taskCurrent.getProcessInstanceId(), key, value);
							taskService.complete(taskCurrent.getId());
						}else{
							taskService.complete(taskCurrent.getId());
						}
						
						List<HoSo> hoSoList = hoSoService.getHoSoFind(null, null, null, taskCurrent.getProcessInstanceId(), 0, 1);
						if (hoSoList != null && !hoSoList.isEmpty()) {
							Date date = new Date();
							HoSo hoSo = hoSoList.get(0);
							String boHS = hoSo.getHoSoGiaTri();
							EFormBoHoSo eBoHS = CommonUtils.getEFormBoHoSo(boHS);
							EFormHoSo eHoSo = eBoHS.getHoSo();
							EFormBoHoSo eFormBoHS = FormUtils.getBoHs(eBoHS, eHoSo, true, taskCurrent.getId());
							if(eFormBoHS != null){
								boHS = CommonUtils.getEFormBoHoSoString(eFormBoHS);
							}
							hoSo.setHoSoGiaTri(boHS);
							hoSo.setTaskId(taskCurrent.getId());
							hoSo.setProcessInstanceId(taskCurrent.getProcessInstanceId());
							hoSo.setNgayCapNhat(new Timestamp(date.getTime()));
							if (hoSo != null) {
								emailPheDuyet.setTrangThai(2L);
								hoSoService.save(hoSo, emailPheDuyet);
							}
						}
						modelMap.put("mss", "success");
						
						return "approved";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		modelMap.put("mss", "error");
		return "approved";
	}
}

package com.eform;

import java.sql.Driver;
import java.util.List;

import javax.sql.DataSource;

import org.activiti.engine.impl.db.EntityDependencyOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.eform.model.entities.TruongThongTin;

@SpringBootApplication
@ComponentScan(basePackages = { "com.eform" })
@EnableTransactionManagement
public class EformApplication {

	private static int serverPort = 8080; 
	private static String serverContextPath = "";
	private static String dataSource = "jdbc:postgresql://localhost:5432/iw?useUnicode=true&characterEncoding=UTF-8";
	private static String username = "postgres";
	private static String password = "123456";

	public EformApplication() {
	}

	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
		factory.setPort(serverPort);
		factory.setContextPath(serverContextPath); 
		return factory;
	}

	@Bean(name = "activitids")
	@Primary
	public DataSource getActivitids() {
		return DataSourceBuilder.create().driverClassName("org.postgresql.Driver").url(dataSource).username(username)
				.password(password).build();
	}

	@Bean(name = "eformds")
	public DataSource getEformds() {
		return DataSourceBuilder.create().driverClassName("org.postgresql.Driver").url(dataSource).username(username)
				.password(password).build();
	}

	public static void main(String[] args) {
		if(args!=null){
		for (int i=0,n=args.length;i<n;i++) {
			String s =args[i];
			int index = s.indexOf("=");
			if (index != -1) {
				String key = s.substring(0, index);
				String value = s.substring(index + 1);
				if (key.equalsIgnoreCase("serverPort")) {
					try {
						serverPort = Integer.parseInt(value);
					} catch (Exception e) {
						System.out.println("Server port config Error. Using default port 8080 ");
						e.printStackTrace();
					}
				} else if (key.equalsIgnoreCase("serverContextPath")) {
					serverContextPath = value;
				} else if (key.equalsIgnoreCase("dataSource")) {
					dataSource = value;
				} else if (key.equalsIgnoreCase("username")) {
					username = value;
				} else if (key.equalsIgnoreCase("password")) {
					password = value;
				} else {
					System.out.println(key + ":" + value);
				}
			} else {
				System.out.println(s);
			}
		}
		}
		SpringApplication.run(EformApplication.class, args);
	}
}

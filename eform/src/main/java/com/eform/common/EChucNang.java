package com.eform.common;

public enum EChucNang {
    E_FORM_LOAI_DANH_MUC("E_FORM_LOAI_DANH_MUC"),
    E_FORM_NHOM_QUY_TRINH("E_FORM_NHOM_QUY_TRINH"),
    E_FORM_DANH_MUC("E_FORM_DANH_MUC"),
    E_FORM_DINH_KY("E_FORM_DINH_KY"),
    E_FORM_TRUONG_THONG_TIN("E_FORM_TRUONG_THONG_TIN"),
    E_FORM_BIEU_MAU("E_FORM_BIEU_MAU"),
    E_FORM_HO_SO("E_FORM_HO_SO"),
    HT_QUYEN("HT_QUYEN"),
    HT_DON_VI("HT_DON_VI"),
    E_FORM_TAI_LIEU("E_FORM_TAI_LIEU"),
    E_FORM_PROC_INST("E_FORM_PROC_INST"),
    E_FORM_BAO_CAO_THONG_KE("E_FORM_BAO_CAO_THONG_KE"),
    HT_CHUC_VU("HT_CHUC_VU");
	
    private final String ma;

    private EChucNang(String ma) {
        this.ma = ma;
    }

    public String getMa() {
        return ma;
    }
}

package com.eform.common;

public interface Constants {
	public static final String SPSV_USERNAME = "vcb\\Administrator"; 
	public static final String SPSV_PASSWORD = "TvsSP20!%";
	public static final String SPSV_FOLDER = "Documents/iworkflow";
	public static final String SPSV_URL = "http://intranet.mine.vn/";
	

	public static final String CHART_LIB = "G";

	public static final String DATE_FORMAT_SHORT = "dd/MM/yyyy";
	public static final String DATE_FORMAT_DAY_MONTH_TEXT = "dd 'Tháng' MM";
	public static final String DATE_FORMAT_TIME = "'lúc' HH:mm";
	public static final String DATE_FORMAT_DAY_MONTH = "dd/MM";
	public static final String DATE_FORMAT_YEAR = "yyyy";
	public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";
	public static final String DATE_FORMAT_LONG = "dd' Tháng 'MM' 'yyyy' lúc 'HH:mm";
	public static final String BG_CSS_CLASS_NAME = "bg-rand-";
	
	public static final String ACTION_VIEW = "xem";
	public static final String ACTION_PREVIEW = "xem-truoc";
	public static final String ACTION_EDIT = "sua";
	public static final String ACTION_DEL = "xoa";
	public static final String ACTION_ADD = "them-moi";
	public static final String ACTION_APPROVE = "phe-duyet";
	public static final String ACTION_REFUSE = "tu-choi";
	public static final String ACTION_DEPLOY = "deploy";
	public static final String ACTION_START = "start";
	public static final String ACTION_PHAN_NHOM = "phan-nhom";
	public static final String ACTION_PHAN_QUYEN = "phan-quyen";
	public static final String ACTION_ACTIVE = "active";
	public static final String ACTION_SUSPEND = "suspend";
	public static final String ACTION_VIEW_HISTORY = "archive";
	public static final String ACTION_COPY = "copy";
	
	// Security roles
	public static final String SECURITY_ROLE = "security-role";
	public static final String SECURITY_ROLE_USER = "user";
	public static final String SECURITY_ROLE_ADMIN = "admin";

	//Form property types
	public static final String ENUM_FORM_TYPE = "enum";
	public static final String LONG_FORM_TYPE = "long";
	public static final String STRING_FORM_TYPE = "string";
	public static final String DATE_FORM_TYPE = "date";
	public static final String DOUBLE_FORM_TYPE = "double";
	public static final String BOOLEAN_FORM_TYPE = "boolean";

}

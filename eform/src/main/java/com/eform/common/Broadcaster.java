package com.eform.common;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Broadcaster {

    private static final List<BroadcastListener> listeners = new CopyOnWriteArrayList<BroadcastListener>();

    public static synchronized void register(BroadcastListener listener) {
        listeners.add(listener);
    }

    public static synchronized  void unregister(BroadcastListener listener) {
        listeners.remove(listener);
    }

    public static synchronized  void broadcast(final String type, final String message, final String userId, final String refKey) {
        for (BroadcastListener listener : listeners) {
            listener.receiveBroadcast(type, message, userId, refKey);
        }
    }

    public interface BroadcastListener {
        public void  receiveBroadcast(String type, String message, String userId, String refKey);
    }

}
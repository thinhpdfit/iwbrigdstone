package com.eform.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SpringApplicationContext {
	private static ApplicationContext ctx;
	@Autowired
	private void setApplicationContext(ApplicationContext applicationContext){
		ctx=applicationContext;
	}
	
	public static ApplicationContext getApplicationContext(){
		return ctx;
	}
	
}

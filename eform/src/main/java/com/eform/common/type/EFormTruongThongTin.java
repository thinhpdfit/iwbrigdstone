package com.eform.common.type;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(Include.NON_NULL)
public class EFormTruongThongTin  implements Comparable<EFormTruongThongTin> {
    @XmlAttribute(name = "ma")
	@JsonProperty("ma")
    private String maTruongThongTin;
    @XmlAttribute(name = "thutu")
	@JsonProperty("thutu")
    private Integer thuTu;
    @XmlAttribute(name = "giatrichu")
	@JsonProperty("giatrichu")
    private String giaTriChu;
    @XmlAttribute(name = "giatriso")
	@JsonProperty("giatriso")
    private Double giaTriSo;
    @XmlAttribute(name = "giatringay")
	@JsonProperty("giatringay")
    private java.util.Date giaTriNgay;
    
    public EFormTruongThongTin() {
        super();
    }

    public int compareTo(EFormTruongThongTin o) {
        if(o!=null){
            if (thuTu == null) {
                return -1;
            }
            if (o.thuTu == null) {
                return 1;
            }
            if (thuTu == o.thuTu) {
                if (maTruongThongTin == null) {
                    return -1;
                }
                if (o.maTruongThongTin == null) {
                    return 1;
                }
                return maTruongThongTin.compareTo(o.maTruongThongTin);
            } else {
                return thuTu - o.thuTu;
            }
        }
        return 0;
    }

    public void setMaTruongThongTin(String maTruongThongTin) {
        this.maTruongThongTin = maTruongThongTin;
    }

    public String getMaTruongThongTin() {
        return maTruongThongTin;
    }

    public void setGiaTriChu(String giaTriChu) {
        this.giaTriChu = giaTriChu;
    }

    public String getGiaTriChu() {
        return giaTriChu;
    }

    public void setGiaTriSo(Double giaTriSo) {
        this.giaTriSo = giaTriSo;
    }

    public Double getGiaTriSo() {
        return giaTriSo;
    }

    public void setGiaTriNgay(Date giaTriNgay) {
        this.giaTriNgay = giaTriNgay;
    }

    public Date getGiaTriNgay() {
        return giaTriNgay;
    }

    public void setThuTu(Integer thuTu) {
        this.thuTu = thuTu;
    }

    public Integer getThuTu() {
        return thuTu;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((maTruongThongTin == null) ? 0 : maTruongThongTin.hashCode());
		result = prime * result + ((thuTu == null) ? 0 : thuTu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EFormTruongThongTin other = (EFormTruongThongTin) obj;
		if (maTruongThongTin == null) {
			if (other.maTruongThongTin != null)
				return false;
		} else if (!maTruongThongTin.equals(other.maTruongThongTin))
			return false;
		if (thuTu == null) {
			if (other.thuTu != null)
				return false;
		} else if (!thuTu.equals(other.thuTu))
			return false;
		return true;
	}
    
    
    
}

package com.eform.common.type;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.eform.model.entities.TruongThongTin;

public class CellDetail implements Serializable {
	private static final long serialVersionUID = -2307840754616468084L;
	private int rowIndex;
	private int colIndex;
	private int rowSpan;
	private int colSpan;
	private Long cellType;
	private CellDetail parent;
	private Boolean deleted;
	private String cellId;
	private List<TruongThongTin> readOnly;
	private List<TruongThongTin> filter;
	private String groupCode;
	private Map<TruongThongTin, String> constrantCode;
	private Long displayButton;
	

	@Override
	public Object clone() throws CloneNotSupportedException {
		CellDetail cell = new CellDetail();
		cell.setRowIndex(rowIndex);
		cell.setColIndex(colIndex);
		cell.setRowSpan(rowSpan);
		cell.setColSpan(colSpan);
		cell.setCellType(cellType);
		cell.setDeleted(deleted);
		cell.setCellId(cellId);
		cell.setReadOnly(readOnly);
		cell.setGroupCode(groupCode);
		cell.setConstrantCode(constrantCode);
		cell.setDisplayButton(displayButton);
		return cell;
	}

	public List<TruongThongTin> getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(List<TruongThongTin> readOnly) {
		this.readOnly = readOnly;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public int getColIndex() {
		return colIndex;
	}

	public void setColIndex(int colIndex) {
		this.colIndex = colIndex;
	}

	public int getRowSpan() {
		return rowSpan;
	}

	public void setRowSpan(int rowSpan) {
		this.rowSpan = rowSpan;
	}

	public int getColSpan() {
		return colSpan;
	}

	public void setColSpan(int colSpan) {
		this.colSpan = colSpan;
	}

	public Long getCellType() {
		return cellType;
	}

	public void setCellType(Long cellType) {
		this.cellType = cellType;
	}

	public CellDetail getParent() {
		return parent;
	}

	public void setParent(CellDetail parent) {
		this.parent = parent;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public Map<TruongThongTin, String> getConstrantCode() {
		return constrantCode;
	}

	public void setConstrantCode(Map<TruongThongTin, String> constrantCode) {
		this.constrantCode = constrantCode;
	}

	public List<TruongThongTin> getFilter() {
		return filter;
	}

	public void setFilter(List<TruongThongTin> filter) {
		this.filter = filter;
	}
	public Long getDisplayButton() {
		return displayButton;
	}

	public void setDisplayButton(Long displayButton) {
		this.displayButton = displayButton;
	}


}

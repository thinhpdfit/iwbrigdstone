package com.eform.common.type;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public class EFormTreeItem<E> implements Serializable {
    private EFormTreeItem parent;
    private List<EFormTreeItem> childList;
    private E e;

    public EFormTreeItem clone()  {
        EFormTreeItem<E> objectNew = new EFormTreeItem<E>();
        objectNew.setE(getE());
        if (childList != null) {
            objectNew.setChildList(new ArrayList());
            for (EFormTreeItem child : childList) {
                EFormTreeItem childNew=child.clone();
                childNew.setParent(objectNew);
                objectNew.getChildList().add(childNew);
            }
        }
        return objectNew;
    }

    public void setParent(EFormTreeItem parent) {
        this.parent = parent;
    }

    public EFormTreeItem getParent() {
        return parent;
    }

    public void setChildList(List<EFormTreeItem> childList) {
        this.childList = childList;
    }

    public List<EFormTreeItem> getChildList() {
        return childList;
    }

    public void setE(E e) {
        this.e = e;
    }

    public E getE() {
        return e;
    }
}


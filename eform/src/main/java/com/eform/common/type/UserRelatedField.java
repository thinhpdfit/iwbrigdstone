package com.eform.common.type;

import com.eform.model.entities.TruongThongTin;
import com.vaadin.ui.Component;

public class UserRelatedField {
	private Component component;
	private TruongThongTin truongThongTin;
	private String constrantCode;//ràng buộc bởi trường thông tin nào
	public UserRelatedField() {
		super();
	}
	public Component getComponent() {
		return component;
	}
	public void setComponent(Component comboBox) {
		this.component = comboBox;
	}
	public TruongThongTin getTruongThongTin() {
		return truongThongTin;
	}
	public void setTruongThongTin(TruongThongTin truongThongTin) {
		this.truongThongTin = truongThongTin;
	}
	public String getConstrantCode() {
		return constrantCode;
	}
	public void setConstrantCode(String constrantCode) {
		this.constrantCode = constrantCode;
	}
	
	
}

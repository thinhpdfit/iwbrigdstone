package com.eform.common.type;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name = "bohoso")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("bohoso")
@JsonInclude(Include.NON_NULL)
public class EFormBoHoSo{
    
    @XmlElement(name = "hoso")
    @JsonProperty("hoso")
    private EFormHoSo hoSo;
    
    @XmlElement(name = "lichsu")
    @JsonProperty("lichsu")
    private ArrayList<EFormHoSo> lichSuList;
    
    public EFormBoHoSo() {
        super();
    }

	public EFormHoSo getHoSo() {
		return hoSo;
	}

	public void setHoSo(EFormHoSo hoSo) {
		this.hoSo = hoSo;
	}

	public ArrayList<EFormHoSo> getLichSuList() {
		return lichSuList;
	}

	public void setLichSuList(ArrayList<EFormHoSo> lichSuList) {
		this.lichSuList = lichSuList;
	}

}

package com.eform.common.type;

import com.eform.model.entities.NhomQuyTrinh;

public class ModelInfo {
	private String id;
	private String ten;
	private String moTa;
	private int icon;
	private int color;
	private NhomQuyTrinh nhom;
	private String rules;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getMoTa() {
		return moTa;
	}
	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}
	public NhomQuyTrinh getNhom() {
		return nhom;
	}
	public void setNhom(NhomQuyTrinh nhom) {
		this.nhom = nhom;
	}
	public int getIcon() {
		return icon;
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}
	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	public String getRules() {
		return rules;
	}
	public void setRules(String rules) {
		this.rules = rules;
	}
	
	
}

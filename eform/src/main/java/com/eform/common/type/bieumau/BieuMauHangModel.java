package com.eform.common.type.bieumau;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.eform.model.entities.BmHang;
import com.eform.sync.model.entities.SyncBmHang;

@XmlRootElement(name = "Hang")
@XmlAccessorType(XmlAccessType.FIELD)
public class BieuMauHangModel {
	@XmlAttribute(name = "Id")
	private Long id;
	@XmlAttribute(name = "Ma")
	private String ma;
	@XmlAttribute(name = "Ten")
    private String ten;
	@XmlAttribute(name = "LoaiHang")
    private Long loaiHang;
	@XmlAttribute(name = "ThuTu")
    private Long thuTu;

	@XmlElement(name = "O")
    private ArrayList<BieuMauOModel> oList;
	public String getMa() {
		return ma;
	}
	public void setMa(String ma) {
		this.ma = ma;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public Long getLoaiHang() {
		return loaiHang;
	}
	public void setLoaiHang(Long loaiHang) {
		this.loaiHang = loaiHang;
	}
	public Long getThuTu() {
		return thuTu;
	}
	public void setThuTu(Long thuTu) {
		this.thuTu = thuTu;
	}
	
	public ArrayList<BieuMauOModel> getoList() {
		return oList;
	}
	public void setoList(ArrayList<BieuMauOModel> oList) {
		this.oList = oList;
	}
	public BieuMauHangModel() {
		super();
	}
	public BieuMauHangModel(SyncBmHang bmHang) {
		super();
		this.id = bmHang.getId();
		this.ma = bmHang.getMa();
		this.ten = bmHang.getTen();
		this.loaiHang = bmHang.getLoaiHang();
		this.thuTu = bmHang.getThuTu();
	}
	
	public SyncBmHang getEntity(){
		SyncBmHang hang = new SyncBmHang();
		hang.setId(this.id);
		hang.setLoaiHang(this.loaiHang);
		hang.setMa(this.ma);
		hang.setTen(this.ten);
		hang.setThuTu(this.thuTu);
		return hang;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BieuMauHangModel other = (BieuMauHangModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
	
}

package com.eform.common.type.bieumau;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BieuMauExport")
@XmlAccessorType(XmlAccessType.FIELD)
public class BieuMauExportModel {
	@XmlElement(name = "LoaiDanhMuc")
    private ArrayList<LoaiDanhMucModel> loaiDanhMucList;
	@XmlElement(name = "TruongThongTin")
    private ArrayList<TruongThongTinModel> truongThongTinList;
	@XmlElement(name = "BieuMau")
    private ArrayList<BieuMauModel> bieuMauList;
	public ArrayList<TruongThongTinModel> getTruongThongTinList() {
		return truongThongTinList;
	}
	public void setTruongThongTinList(ArrayList<TruongThongTinModel> truongThongTinList) {
		this.truongThongTinList = truongThongTinList;
	}
	public ArrayList<BieuMauModel> getBieuMauList() {
		return bieuMauList;
	}
	public void setBieuMauList(ArrayList<BieuMauModel> bieuMauList) {
		this.bieuMauList = bieuMauList;
	}
	public ArrayList<LoaiDanhMucModel> getLoaiDanhMucList() {
		return loaiDanhMucList;
	}
	public void setLoaiDanhMucList(ArrayList<LoaiDanhMucModel> loaiDanhMucList) {
		this.loaiDanhMucList = loaiDanhMucList;
	}
	public BieuMauExportModel() {
		super();
	}
	
}

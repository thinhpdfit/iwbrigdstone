package com.eform.common.type.bieumau;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.eform.sync.model.entities.SyncBieuMau;

@XmlRootElement(name = "BieuMau")
@XmlAccessorType(XmlAccessType.FIELD)
public class BieuMauModel {

	@XmlAttribute(name = "Id")
	private Long id;
	
	@XmlAttribute(name = "Ma")
	private String ma;
	
	@XmlAttribute(name = "Ten")
    private String ten;
	
	@XmlAttribute(name = "LoaiBieuMau")
    private Long loaiBieuMau;
	
	@XmlAttribute(name = "TrangThai")
    private Long trangThai;
	
	@XmlAttribute(name = "SoHang")
    private Long soHang;
	
	@XmlAttribute(name = "SoCot")
    private Long soCot;
	
	@XmlElement(name = "Hang")
    private ArrayList<BieuMauHangModel> hangList;
	
	public String getMa() {
		return ma;
	}
	public void setMa(String ma) {
		this.ma = ma;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public Long getLoaiBieuMau() {
		return loaiBieuMau;
	}
	public void setLoaiBieuMau(Long loaiBieuMau) {
		this.loaiBieuMau = loaiBieuMau;
	}
	public Long getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(Long trangThai) {
		this.trangThai = trangThai;
	}
	public Long getSoHang() {
		return soHang;
	}
	public void setSoHang(Long soHang) {
		this.soHang = soHang;
	}
	public Long getSoCot() {
		return soCot;
	}
	public void setSoCot(Long soCot) {
		this.soCot = soCot;
	}

	public ArrayList<BieuMauHangModel> getHangList() {
		return hangList;
	}
	public void setHangList(ArrayList<BieuMauHangModel> hangList) {
		this.hangList = hangList;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public BieuMauModel() {
		super();
	}
	public BieuMauModel(SyncBieuMau bieuMau) {
		super();
		this.id = bieuMau.getId();
		this.ma = bieuMau.getMa();
		this.ten = bieuMau.getTen();
		this.loaiBieuMau = bieuMau.getLoaiBieuMau();
		this.trangThai = bieuMau.getTrangThai();
		this.soHang = bieuMau.getSoHang();
		this.soCot = bieuMau.getSoCot();
	} 
	
	public SyncBieuMau getEntity(){
		SyncBieuMau bieuMau = new SyncBieuMau();
		bieuMau.setId(this.id);
		bieuMau.setLoaiBieuMau(this.loaiBieuMau);
		bieuMau.setMa(this.ma);
		bieuMau.setSoCot(this.soCot);
		bieuMau.setSoHang(this.soHang);
		bieuMau.setTen(this.ten);
		bieuMau.setTrangThai(this.trangThai);
		return bieuMau;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BieuMauModel other = (BieuMauModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
	
}

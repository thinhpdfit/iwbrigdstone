package com.eform.common.type.bieumau;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.eform.sync.model.entities.SyncLoaiDanhMuc;


@XmlRootElement(name = "LoaiDanhMuc")
@XmlAccessorType(XmlAccessType.FIELD)
public class LoaiDanhMucModel {
	@XmlAttribute(name = "Id")
	private Long id;
	@XmlAttribute(name = "Ma")
	private String ma;
	@XmlAttribute(name = "Ten")
    private String ten;
	@XmlAttribute(name = "TrangThai")
    private Long trangThai;
	@XmlElement(name = "DanhMuc")
	private ArrayList<DanhMucModel> danhMucList;
	@XmlAttribute(name = "LoaiDanhMucChaId")
    private Long loaiDanhMucChaId;
	
	public String getMa() {
		return ma;
	}
	public void setMa(String ma) {
		this.ma = ma;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public Long getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(Long trangThai) {
		this.trangThai = trangThai;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public ArrayList<DanhMucModel> getDanhMucList() {
		return danhMucList;
	}
	public void setDanhMucList(ArrayList<DanhMucModel> danhMucList) {
		this.danhMucList = danhMucList;
	}
	
	public Long getLoaiDanhMucChaId() {
		return loaiDanhMucChaId;
	}
	public void setLoaiDanhMucChaId(Long loaiDanhMucChaId) {
		this.loaiDanhMucChaId = loaiDanhMucChaId;
	}
	public LoaiDanhMucModel() {
		super();
	}
	public LoaiDanhMucModel(SyncLoaiDanhMuc loaiDanhMuc) {
		super();
		this.id = loaiDanhMuc.getId();
		this.ma = loaiDanhMuc.getMa();
		this.ten = loaiDanhMuc.getTen();
		this.trangThai = loaiDanhMuc.getTrangThai();
		if(loaiDanhMuc.getLoaiDanhMuc() != null){
			this.loaiDanhMucChaId = loaiDanhMuc.getLoaiDanhMuc().getId();
		}
	}
	

	public SyncLoaiDanhMuc getEntity(){
		SyncLoaiDanhMuc loaiDanhMuc = new SyncLoaiDanhMuc();
		loaiDanhMuc.setId(this.id);
		loaiDanhMuc.setMa(this.ma);
		loaiDanhMuc.setTen(this.ten);
		loaiDanhMuc.setTrangThai(this.trangThai);
		return loaiDanhMuc;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoaiDanhMucModel other = (LoaiDanhMucModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}

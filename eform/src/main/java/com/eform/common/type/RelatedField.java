package com.eform.common.type;

import com.eform.model.entities.LoaiDanhMuc;
import com.vaadin.ui.ComboBox;

public class RelatedField {
	private ComboBox comboBox;
	private LoaiDanhMuc loaiDanhMuc;
	public ComboBox getComboBox() {
		return comboBox;
	}
	public void setComboBox(ComboBox comboBox) {
		this.comboBox = comboBox;
	}
	public LoaiDanhMuc getLoaiDanhMuc() {
		return loaiDanhMuc;
	}
	public void setLoaiDanhMuc(LoaiDanhMuc loaiDanhMuc) {
		this.loaiDanhMuc = loaiDanhMuc;
	}
	public RelatedField() {
		super();
	}
	
	
}

package com.eform.common.type;

import com.vaadin.navigator.View;

public class PageBreadcrumbInfo {
	private String viewName;
	private String title;
	public String getViewName() {
		return viewName;
	}
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}

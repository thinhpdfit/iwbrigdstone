package com.eform.common.type.bieumau;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.eform.sync.model.entities.SyncBmOTruongThongTin;
import com.eform.sync.model.entities.SyncTruongThongTin;

@XmlRootElement(name = "OTruongThongTin")
@XmlAccessorType(XmlAccessType.FIELD)
public class BieuMauOTruongThongTinModel {
	@XmlAttribute(name = "Id")
	private Long id;
	@XmlAttribute(name = "ThuTu")
    private Long thuTu;
	@XmlAttribute(name = "TruongThongTinId")
    private Long truongThongTinId;
	@XmlAttribute(name = "ReadOnly")
    private Long readOnly;
	@XmlAttribute(name = "Constrant")
    private String constrant;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getThuTu() {
		return thuTu;
	}
	public void setThuTu(Long thuTu) {
		this.thuTu = thuTu;
	}

	
	
	public Long getTruongThongTinId() {
		return truongThongTinId;
	}
	public void setTruongThongTinId(Long truongThongTinId) {
		this.truongThongTinId = truongThongTinId;
	}
	public Long getReadOnly() {
		return readOnly;
	}
	public void setReadOnly(Long readOnly) {
		this.readOnly = readOnly;
	}
	public String getConstrant() {
		return constrant;
	}
	public void setConstrant(String constrant) {
		this.constrant = constrant;
	}
	public BieuMauOTruongThongTinModel() {
		super();
	}
	public BieuMauOTruongThongTinModel(SyncBmOTruongThongTin bmOTruongThongTin) {
		super();
		this.id = bmOTruongThongTin.getId();
		this.thuTu = bmOTruongThongTin.getThuTu();
		this.readOnly = bmOTruongThongTin.getReadOnly();
		this.constrant = bmOTruongThongTin.getConstrant();
		if(bmOTruongThongTin.getTruongThongTin() != null){
			this.truongThongTinId = bmOTruongThongTin.getTruongThongTin().getId();
		}
	}
	
	public SyncBmOTruongThongTin getEntity(){
		SyncBmOTruongThongTin oTruongTT = new SyncBmOTruongThongTin();
		oTruongTT.setId(this.id);
		oTruongTT.setThuTu(this.thuTu);
		oTruongTT.setReadOnly(this.readOnly);
		oTruongTT.setConstrant(this.constrant);
		if(this.getTruongThongTinId() != null){
			SyncTruongThongTin truongThongTin = new SyncTruongThongTin();
			truongThongTin.setId(this.getTruongThongTinId());
			oTruongTT.setTruongThongTin(truongThongTin);
		}
		return oTruongTT;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BieuMauOTruongThongTinModel other = (BieuMauOTruongThongTinModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
    
}

package com.eform.common.type;

import java.io.Serializable;

public class ComboboxItem implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String text;
	private Object value;
	
	public ComboboxItem(){
		
	}
	
	public ComboboxItem(String text, Object value) {
		super();
		this.text = text;
		this.value = value;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return text;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComboboxItem other = (ComboboxItem) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	


}

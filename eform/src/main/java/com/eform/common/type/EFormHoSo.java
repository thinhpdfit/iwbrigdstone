package com.eform.common.type;

import java.util.ArrayList;

import java.util.Collections;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name = "hoso")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("hoso")
@JsonInclude(Include.NON_NULL)
public class EFormHoSo  implements Comparable<EFormHoSo> {
    @XmlTransient
    private String maTemp;
    
    @XmlAttribute(name = "ma")
    @JsonProperty("ma")
    private String maBieuMau;
    
    @XmlAttribute(name = "thutu")
    @JsonProperty("thutu")
    private Integer thuTu;
    
    @XmlElement(name = "truongthongtin")
    @JsonProperty("truongthongtin")
    private ArrayList<EFormTruongThongTin> truongThongTinList;
    
    @XmlElement(name = "nhom")
    @JsonProperty("nhom")
    private ArrayList<EFormHoSo> hoSoList;
    
    @XmlAttribute(name = "taskid")
    @JsonProperty("taskid")
    private String taskId;

    @XmlAttribute(name = "loopitempath")
    @JsonProperty("loopitempath")
    private String loopItemPath;
    

    @XmlAttribute(name = "datalooppath")
    @JsonProperty("datalooppath")
    private String dataLoopPath;
    

    @XmlAttribute(name = "loopitemindex")
    @JsonProperty("loopitemindex")
    private Integer loopItemIndex;
    
    private Integer thuTuHang;
    
    @XmlTransient
    private Long bmOId;
    
    public EFormHoSo() {
        super();
    }

    public void sort() {
        if (hoSoList != null) {
            Collections.sort(hoSoList);
            for (EFormHoSo hoSo : hoSoList) {
                hoSo.sort();
            }
        }
        if (truongThongTinList != null) {
            Collections.sort(truongThongTinList);
        }
    }

    public int compareTo(EFormHoSo o) {
        if (o != null) {
            if (thuTu == null) {
                return -1;
            }
            if (o.thuTu == null) {
                return 1;
            }
            if (thuTu == o.thuTu) {
                if (maBieuMau == null) {
                    return -1;
                }
                if (o.maBieuMau == null) {
                    return 1;
                }
                return maBieuMau.compareTo(o.maBieuMau);
            } else {
                return thuTu - o.thuTu;
            }
        }
        return 0;
    }
    
    public EFormHoSo clone(){
    	EFormHoSo clone = new EFormHoSo();
    	clone.setHoSoList(getHoSoList());
    	clone.setLoopItemIndex(getLoopItemIndex());
    	clone.setLoopItemPath(getLoopItemPath());
    	clone.setMaBieuMau(getMaBieuMau());
    	clone.setMaTemp(getMaTemp());
    	clone.setTaskId(getTaskId());
    	clone.setThuTu(getThuTu());
    	clone.setThuTuHang(getThuTuHang());
    	clone.setTruongThongTinList(getTruongThongTinList());
    	clone.setBmOId(getBmOId());
    	return clone;
    }

    public void setHoSoList(ArrayList<EFormHoSo> hoSoList) {
        this.hoSoList = hoSoList;
    }

    public ArrayList<EFormHoSo> getHoSoList() {
        return hoSoList;
    }

    public void setTruongThongTinList(ArrayList<EFormTruongThongTin> truongThongTinList) {
        this.truongThongTinList = truongThongTinList;
    }

    public ArrayList<EFormTruongThongTin> getTruongThongTinList() {
        return truongThongTinList;
    }

    public void setMaBieuMau(String maBieuMau) {
        this.maBieuMau = maBieuMau;
    }

    public String getMaBieuMau() {
        return maBieuMau;
    }

    public void setThuTu(Integer thuTu) {
        this.thuTu = thuTu;
    }

    public Integer getThuTu() {
        return thuTu;
    }

    public void setMaTemp(String maTemp) {
        this.maTemp = maTemp;
    }

    public String getMaTemp() {
        return maTemp;
    }

    public void setThuTuHang(Integer thuTuHang) {
        this.thuTuHang = thuTuHang;
    }

    public Integer getThuTuHang() {
        return thuTuHang;
    }

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getLoopItemPath() {
		return loopItemPath;
	}

	public void setLoopItemPath(String loopItemPath) {
		this.loopItemPath = loopItemPath;
	}

	public Integer getLoopItemIndex() {
		return loopItemIndex;
	}

	public void setLoopItemIndex(Integer loopItemIndex) {
		this.loopItemIndex = loopItemIndex;
	}

	public Long getBmOId() {
		return bmOId;
	}

	public void setBmOId(Long bmOId) {
		this.bmOId = bmOId;
	}

	public String getDataLoopPath() {
		return dataLoopPath;
	}

	public void setDataLoopPath(String dataLoopPath) {
		this.dataLoopPath = dataLoopPath;
	}
    
	
}


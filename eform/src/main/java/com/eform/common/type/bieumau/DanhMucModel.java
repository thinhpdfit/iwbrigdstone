package com.eform.common.type.bieumau;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.eform.sync.model.entities.SyncDanhMuc;

@XmlRootElement(name = "DanhMuc")
@XmlAccessorType(XmlAccessType.FIELD)
public class DanhMucModel {
	@XmlAttribute(name = "Id")
	private Long id;
	@XmlAttribute(name = "Ma")
	private String ma;
	@XmlAttribute(name = "Ten")
    private String ten;
	@XmlAttribute(name = "TrangThai")
    private Long trangThai;
	@XmlAttribute(name = "ThuTu")
    private Long thuTu;
	@XmlAttribute(name = "DanhMucChaId")
    private Long danhMucChaId;
	public String getMa() {
		return ma;
	}
	public void setMa(String ma) {
		this.ma = ma;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public Long getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(Long trangThai) {
		this.trangThai = trangThai;
	}
	public Long getThuTu() {
		return thuTu;
	}
	public void setThuTu(Long thuTu) {
		this.thuTu = thuTu;
	}
	public DanhMucModel() {
		super();
	}
	public DanhMucModel(SyncDanhMuc danhMuc) {
		super();
		this.id = danhMuc.getId();
		this.ma = danhMuc.getMa();
		this.ten = danhMuc.getTen();
		this.trangThai = danhMuc.getTrangThai();
		this.thuTu = danhMuc.getThuTu();
		if(danhMuc.getDanhMuc() != null){
			this.danhMucChaId = danhMuc.getDanhMuc().getId();
		}
	}
	
	public SyncDanhMuc getEntity(){
		SyncDanhMuc danhMuc = new SyncDanhMuc();
		danhMuc.setId(this.id);
		danhMuc.setMa(this.ma);
		danhMuc.setTen(this.ten);
		danhMuc.setThuTu(this.thuTu);
		danhMuc.setTrangThai(this.trangThai);
		if(this.danhMucChaId != null){
			SyncDanhMuc dmCha = new SyncDanhMuc();
			dmCha.setId(this.danhMucChaId);
			danhMuc.setDanhMuc(dmCha);
		}
		return danhMuc;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DanhMucModel other = (DanhMucModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
    
}

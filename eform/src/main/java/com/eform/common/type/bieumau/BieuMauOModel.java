package com.eform.common.type.bieumau;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmO;

@XmlRootElement(name = "O")
@XmlAccessorType(XmlAccessType.FIELD)
public class BieuMauOModel {

	@XmlAttribute(name = "Id")
	private Long id;
	@XmlAttribute(name = "Ma")
	private String ma;
	@XmlAttribute(name = "Ten")
    private String ten;
	@XmlAttribute(name = "GhepCot")
    private Long ghepCot;
	@XmlAttribute(name = "GhepHang")
    private Long ghepHang;
	@XmlAttribute(name = "LoaiO")
    private Long loaiO;
	@XmlAttribute(name = "ThuTu")
    private Long thuTu;
	@XmlAttribute(name = "ViTri")
    private Long viTri;
	@XmlElement(name = "OTruongThongTin")
    private ArrayList<BieuMauOTruongThongTinModel> oTruongThongTinList;

	@XmlAttribute(name = "BieuMauConId")
    private Long bieuMauConId;
	
	public String getMa() {
		return ma;
	}
	public void setMa(String ma) {
		this.ma = ma;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public Long getGhepCot() {
		return ghepCot;
	}
	public void setGhepCot(Long ghepCot) {
		this.ghepCot = ghepCot;
	}
	public Long getGhepHang() {
		return ghepHang;
	}
	public void setGhepHang(Long ghepHang) {
		this.ghepHang = ghepHang;
	}
	public Long getLoaiO() {
		return loaiO;
	}
	public void setLoaiO(Long loaiO) {
		this.loaiO = loaiO;
	}
	public Long getThuTu() {
		return thuTu;
	}
	public void setThuTu(Long thuTu) {
		this.thuTu = thuTu;
	}
	public Long getViTri() {
		return viTri;
	}
	public void setViTri(Long viTri) {
		this.viTri = viTri;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public ArrayList<BieuMauOTruongThongTinModel> getoTruongThongTinList() {
		return oTruongThongTinList;
	}
	public void setoTruongThongTinList(ArrayList<BieuMauOTruongThongTinModel> oTruongThongTinList) {
		this.oTruongThongTinList = oTruongThongTinList;
	}
	
	
	public Long getBieuMauConId() {
		return bieuMauConId;
	}
	public void setBieuMauConId(Long bieuMauConId) {
		this.bieuMauConId = bieuMauConId;
	}
	public BieuMauOModel() {
		super();
	}
	public BieuMauOModel(SyncBmO bmO) {
		super();
		this.id = bmO.getId();
		this.ma = bmO.getMa();
		this.ten = bmO.getTen();
		this.ghepCot = bmO.getGhepCot();
		this.ghepHang = bmO.getGhepHang();
		this.loaiO = bmO.getLoaiO();
		this.thuTu = bmO.getThuTu();
		this.viTri = bmO.getViTri();
		if(bmO.getBieuMauCon() != null){
			this.bieuMauConId = bmO.getBieuMauCon().getId();
		}
	}
	
	public SyncBmO getEntity(){
		SyncBmO bmO = new SyncBmO();
		bmO.setGhepCot(this.ghepCot);
		bmO.setGhepHang(this.ghepHang);
		bmO.setId(this.id);
		bmO.setLoaiO(this.loaiO);
		bmO.setMa(this.ma);
		bmO.setTen(this.ten);
		bmO.setThuTu(this.thuTu);
		bmO.setViTri(this.viTri);
		if(this.bieuMauConId != null){
			SyncBieuMau bmCon = new SyncBieuMau();
			bmCon.setId(this.bieuMauConId);
			bmO.setBieuMauCon(bmCon);
		}
		return bmO;
	}
	
	
}

package com.eform.common.type.bieumau;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.eform.sync.model.entities.SyncLoaiDanhMuc;
import com.eform.sync.model.entities.SyncTruongThongTin;


@XmlRootElement(name = "TruongThongTin")
@XmlAccessorType(XmlAccessType.FIELD)
public class TruongThongTinModel {
	@XmlAttribute(name = "Id")
	private Long id;
	@XmlAttribute(name = "Ma")
	private String ma;
	@XmlAttribute(name = "Ten")
    private String ten;
	@XmlAttribute(name = "TrangThai")
    private Long trangThai;
	@XmlAttribute(name = "KieuDuLieu")
    private Long kieuDuLieu;
	@XmlAttribute(name = "LoaiHienThi")
    private Long loaiHienThi;
	@XmlAttribute(name = "InlineStyle")
    private String inlineStyle;
	@XmlAttribute(name = "Required")
    private Long required;
	@XmlAttribute(name = "Expressions")
    private String expressions;
	@XmlAttribute(name = "ExpressionMessage")
    private String expressionMessage;
	@XmlAttribute(name = "Minlength")
    private Long minlength;
	@XmlAttribute(name = "Maxlength")
    private Long maxlength;
	@XmlAttribute(name = "Minvalue")
    private BigDecimal minvalue;
	@XmlAttribute(name = "Maxvalue")
    private BigDecimal maxvalue;
	@XmlAttribute(name = "FileSize")
    private Long fileSize;
	@XmlAttribute(name = "FileExtensionAlow")
    private String fileExtensionAlow;
	@XmlAttribute(name = "MinDuration")
    private String minDuration;
	@XmlAttribute(name = "MaxDuration")
    private String maxDuration;
	@XmlAttribute(name = "MinDurationType")
    private Long minDurationType;
	@XmlAttribute(name = "MaxDurationType")
    private Long maxDurationType;
	@XmlAttribute(name = "DefaultValue")
    private String defaultValue;
	@XmlAttribute(name = "DateDefaultType")
    private Long dateDefaultType;
	@XmlAttribute(name = "TimeDetail")
    private Long timeDetail;
	@XmlAttribute(name = "TableDisplay")
    private Long tableDisplay;
	@XmlAttribute(name = "SearchFormDisplay")
    private Long searchFormDisplay;
	@XmlAttribute(name = "TableDisplayOrder")
    private Long tableDisplayOrder;
	@XmlAttribute(name = "SearchFormDisplayOrder")
    private Long searchFormDisplayOrder;
	@XmlAttribute(name = "LoaiDanhMucId")
    private Long loaiDanhMucId;
	
	public String getMa() {
		return ma;
	}
	public void setMa(String ma) {
		this.ma = ma;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public Long getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(Long trangThai) {
		this.trangThai = trangThai;
	}
	public Long getKieuDuLieu() {
		return kieuDuLieu;
	}
	public void setKieuDuLieu(Long kieuDuLieu) {
		this.kieuDuLieu = kieuDuLieu;
	}
	public Long getLoaiHienThi() {
		return loaiHienThi;
	}
	public void setLoaiHienThi(Long loaiHienThi) {
		this.loaiHienThi = loaiHienThi;
	}
	public String getInlineStyle() {
		return inlineStyle;
	}
	public void setInlineStyle(String inlineStyle) {
		this.inlineStyle = inlineStyle;
	}
	public Long getRequired() {
		return required;
	}
	public void setRequired(Long required) {
		this.required = required;
	}
	public String getExpressions() {
		return expressions;
	}
	public void setExpressions(String expressions) {
		this.expressions = expressions;
	}
	public String getExpressionMessage() {
		return expressionMessage;
	}
	public void setExpressionMessage(String expressionMessage) {
		this.expressionMessage = expressionMessage;
	}
	public Long getMinlength() {
		return minlength;
	}
	public void setMinlength(Long minlength) {
		this.minlength = minlength;
	}
	public Long getMaxlength() {
		return maxlength;
	}
	public void setMaxlength(Long maxlength) {
		this.maxlength = maxlength;
	}
	public BigDecimal getMinvalue() {
		return minvalue;
	}
	public void setMinvalue(BigDecimal minvalue) {
		this.minvalue = minvalue;
	}
	public BigDecimal getMaxvalue() {
		return maxvalue;
	}
	public void setMaxvalue(BigDecimal maxvalue) {
		this.maxvalue = maxvalue;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public String getFileExtensionAlow() {
		return fileExtensionAlow;
	}
	public void setFileExtensionAlow(String fileExtensionAlow) {
		this.fileExtensionAlow = fileExtensionAlow;
	}
	public String getMinDuration() {
		return minDuration;
	}
	public void setMinDuration(String minDuration) {
		this.minDuration = minDuration;
	}
	public String getMaxDuration() {
		return maxDuration;
	}
	public void setMaxDuration(String maxDuration) {
		this.maxDuration = maxDuration;
	}
	public Long getMinDurationType() {
		return minDurationType;
	}
	public void setMinDurationType(Long minDurationType) {
		this.minDurationType = minDurationType;
	}
	public Long getMaxDurationType() {
		return maxDurationType;
	}
	public void setMaxDurationType(Long maxDurationType) {
		this.maxDurationType = maxDurationType;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public Long getDateDefaultType() {
		return dateDefaultType;
	}
	public void setDateDefaultType(Long dateDefaultType) {
		this.dateDefaultType = dateDefaultType;
	}
	public Long getTimeDetail() {
		return timeDetail;
	}
	public void setTimeDetail(Long timeDetail) {
		this.timeDetail = timeDetail;
	}
	public Long getTableDisplay() {
		return tableDisplay;
	}
	public void setTableDisplay(Long tableDisplay) {
		this.tableDisplay = tableDisplay;
	}
	public Long getSearchFormDisplay() {
		return searchFormDisplay;
	}
	public void setSearchFormDisplay(Long searchFormDisplay) {
		this.searchFormDisplay = searchFormDisplay;
	}
	public Long getTableDisplayOrder() {
		return tableDisplayOrder;
	}
	public void setTableDisplayOrder(Long tableDisplayOrder) {
		this.tableDisplayOrder = tableDisplayOrder;
	}
	public Long getSearchFormDisplayOrder() {
		return searchFormDisplayOrder;
	}
	public void setSearchFormDisplayOrder(Long searchFormDisplayOrder) {
		this.searchFormDisplayOrder = searchFormDisplayOrder;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLoaiDanhMucId() {
		return loaiDanhMucId;
	}
	public void setLoaiDanhMucId(Long loaiDanhMucId) {
		this.loaiDanhMucId = loaiDanhMucId;
	}
	public TruongThongTinModel() {
		super();
	}
	public TruongThongTinModel(SyncTruongThongTin truongThongtin) {
		super();
		this.id = truongThongtin.getId();
		this.ma = truongThongtin.getMa();
		this.ten = truongThongtin.getTen();
		this.trangThai = truongThongtin.getTrangThai();
		this.kieuDuLieu = truongThongtin.getKieuDuLieu();
		this.loaiHienThi = truongThongtin.getLoaiHienThi();
		this.inlineStyle = truongThongtin.getInlineStyle();
		this.required = truongThongtin.getRequired();
		this.expressions = truongThongtin.getExpressions();
		this.expressionMessage = truongThongtin.getExpressionMessage();
		this.minlength = truongThongtin.getMinlength();
		this.maxlength = truongThongtin.getMaxlength();
		this.minvalue = truongThongtin.getMinvalue();
		this.maxvalue = truongThongtin.getMaxvalue();
		this.fileSize = truongThongtin.getFileSize();
		this.fileExtensionAlow = truongThongtin.getFileExtensionAlow();
		this.minDuration = truongThongtin.getMinDuration();
		this.maxDuration = truongThongtin.getMaxDuration();
		this.minDurationType = truongThongtin.getMinDurationType();
		this.maxDurationType = truongThongtin.getMaxDurationType();
		this.defaultValue = truongThongtin.getDefaultValue();
		this.dateDefaultType = truongThongtin.getDateDefaultType();
		this.timeDetail = truongThongtin.getTimeDetail();
		this.tableDisplay = truongThongtin.getTableDisplay();
		this.searchFormDisplay = truongThongtin.getSearchFormDisplay();
		this.tableDisplayOrder = truongThongtin.getTableDisplayOrder();
		this.searchFormDisplayOrder = truongThongtin.getSearchFormDisplayOrder();
		if(truongThongtin.getLoaiDanhMuc() != null){
			this.loaiDanhMucId = truongThongtin.getLoaiDanhMuc().getId();
		}
	}
	
	public SyncTruongThongTin getEntity(){
		SyncTruongThongTin truongThongtin = new SyncTruongThongTin();
		truongThongtin.setId(this.id);
		truongThongtin.setMa(this.ma);
		truongThongtin.setTen(this.ten);
		truongThongtin.setTrangThai(this.trangThai);
		truongThongtin.setKieuDuLieu(this.kieuDuLieu);
		truongThongtin.setLoaiHienThi(this.loaiHienThi);
		truongThongtin.setInlineStyle(this.inlineStyle);
		truongThongtin.setRequired(this.required);
		truongThongtin.setExpressions(this.expressions);
		truongThongtin.setExpressionMessage(this.expressionMessage);
		truongThongtin.setMinlength(this.minlength);
		truongThongtin.setMaxlength(this.maxlength);
		truongThongtin.setMinvalue(this.minvalue);
		truongThongtin.setMaxvalue(this.maxvalue);
		truongThongtin.setFileSize(this.fileSize);
		truongThongtin.setFileExtensionAlow(this.fileExtensionAlow);
		truongThongtin.setMinDuration(this.minDuration);
		truongThongtin.setMaxDuration(this.maxDuration);
		truongThongtin.setMinDurationType(this.minDurationType);
		truongThongtin.setMaxDurationType(this.maxDurationType);
		truongThongtin.setDefaultValue(this.defaultValue);
		truongThongtin.setDateDefaultType(this.dateDefaultType);
		truongThongtin.setTimeDetail(this.timeDetail);
		truongThongtin.setTableDisplay(this.tableDisplay);
		truongThongtin.setSearchFormDisplay(this.searchFormDisplay);
		truongThongtin.setTableDisplayOrder(this.tableDisplayOrder);
		truongThongtin.setSearchFormDisplayOrder(this.searchFormDisplayOrder);
		if(this.loaiDanhMucId != null){
			SyncLoaiDanhMuc loaiDanhMuc = new SyncLoaiDanhMuc();
			loaiDanhMuc.setId(this.loaiDanhMucId);
			truongThongtin.setLoaiDanhMuc(loaiDanhMuc);
		}
		return truongThongtin;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TruongThongTinModel other = (TruongThongTinModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	
}

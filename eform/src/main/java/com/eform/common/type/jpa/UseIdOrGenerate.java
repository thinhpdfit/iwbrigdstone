package com.eform.common.type.jpa;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Hashtable;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentityGenerator;

public class UseIdOrGenerate extends IdentityGenerator {
	private static final Logger log = Logger.getLogger(UseIdOrGenerate.class.getName());

	@Override
	public Serializable generate(SessionImplementor session, Object obj) throws HibernateException {
	    if (obj == null) throw new HibernateException(new NullPointerException()) ;

	    Long userId = null;
	    
		try {
			Field f = obj.getClass().getDeclaredField("id");
		    f.setAccessible(true);
		    userId = (Long) f.get(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    if (userId == null) {
	        Serializable id = super.generate(session, obj) ;
	        return id;
	    } else {
	        return userId;

	    }
	}
}
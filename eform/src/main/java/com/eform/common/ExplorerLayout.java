package com.eform.common;

public interface ExplorerLayout {

	static final String MAIN_MENU_BAR_ID = "main-menu-bar-id";
	
	

	static final String THEME = "eform";

	static final String MENU_BUTTON_ITEM = "menu-btn-item";
	static final String BUTTON_TOGGLE_MENU = "btn-toggle-menu";
	static final String BUTTON_SUCCESS = "btn btn-success";
	static final String BUTTON_WARNING = "btn btn-warning";
	static final String BUTTON_DANGER = "btn btn-danger";
	static final String ACTIVE_MENU = "active-menu";
	static final String HEADER = "header";
	static final String HEADER_LEFT = "header-left";
	static final String LOGO_LABEL = "logo-lable";
	static final String LOGO_WRAP = "logo-wrap";
	static final String HEADER_RIGHT = "header-right";
	static final String HEADER_RIGHT_CONTENT = "header-right-content";
	static final String HEADER_SEARCH_BOX = "header-search-box";
	static final String HEADER_SEARCH_BOX_TXT = "header-search-box-txt";
	static final String HEADER_SEARCH_BOX_LBL = "header-search-box-lbl";
	static final String SEARCH_BG_BLUR = "search-bg-blur";
	static final String USER_WRAP = "user-wrap";
	static final String USER_NAME = "user-name";
	static final String USER_IMG = "user-img";
	static final String BUTTON_LOGOUT = "btn-logout";
	static final String CONTAINER = "container";
	static final String MAIN_COTAINER = "main-container";
	static final String MAIN_MENU_BAR = "main-menu-bar";
	static final String MAIN_MENU_BAR_CONTENT = "main-menu-bar-content";
	static final String MAIN_MENU_BAR_HIDDEN = "main-menu-bar main-menu-bar-hidden";
	static final String MAIN_WRAP = "main-wrap";
	static final String MAIN_CONTENT = "main-content";
	static final String MAIN_TITLE_WRAP = "main-title-wrap";
	static final String INFO_WRAP = "info-wrap";
	static final String NAME_WRAP = "name-wrap";
	static final String BTN_CLAIM = "btn-claim";
	static final String INFO_CONTAINER = "info-container";
	static final String INFO_ITEM = "info-item";
	static final String TABSHEET = "tabsheet";
	static final String TABSHEET_WRAP = "tabsheet-wrap";
	static final String TAB_CONTENT = "tab-content";
	static final String TAB_RELATED_CONTENT = "tab-related-content";
	static final String SWITCH_VIEW_BTN = "btn-switch-view";
	static final String DETAIL_TITLE = "detail-title";
	static final String SUB_TITLE_WRAP = "sub-title-wrap";
	static final String MAIN_TITLE = "main-title";
	static final String MAIN_TITLE_ICON_RIGHT = "main-title-icon-right";
	static final String FOOTER = "footer";
	static final String WRAPPER = "wrapper";
	static final String FOOTER_LABEL = "footer-label";
	static final String SEARCH_FORM = "search-form";
	static final String SEARCH_FORM_ROW = "sf-row";
	static final String SEARCH_FORM_LABEL_WRAP = "sf-lbl-wrap";
	static final String SEARCH_FORM_BUTTON_ROW = "sf-btn-row";
	static final String SEARCH_FORM_BUTTON_WRAP = "sf-btn-wrap";
	static final String SEARCH_FORM_LABEL = "sf-label";
	static final String SEARCH_FORM_TEXTFIELD_WRAP = "sf-tf-wrap";
	static final String BUTTON_SEARCH = "sf-btn";
	static final String SEARCH_FORM_TEXTFIELD = "sf-textfield";
	static final String DATA_WRAP = "data-wrap";
	static final String TK_WRAP = "tk-wrap";
	static final String TK_ITEM = "tk-item";
	static final String TK_ITEM_TT = "tk-item-tt";
	static final String BTN_OWNER = "btn-owner";
	static final String BTN_TK_DETAIL = "btn-tk-detail";
	static final String BTN_FINISHED = "btn-finished";
	static final String BTN_CURRENT_TASK = "btn-current-task";
	static final String TK_ITEM_SL = "tk-item-sl";
	static final String TK_ITEM_ICON = "tk-item-icon";
	static final String DATA_TABLE = "data-table";
	static final String DATA_GRID = "data-grid";
	static final String BUTTON_ACTION = "btn-action";
	static final String BUTTON_ACTION_WRAP = "btn-action-wrap";
	static final String BUTTON_ACTION_LEFT = "btn-action-left";
	static final String ACTION_POPUP = "action-popup";
	static final String HELP_TEXT_POPUP = "help-text-popup";
	static final String HELP_TEXT_CONTENT = "help-text-content";
	static final String ACTION_POPUP_SMAIL = "action-popup-smail";
	static final String SUB_TITLE = "sub-title";
	static final String BUTTON_ADDMEMBER = "btn btn-addmember";
	static final String ICON_COLOR = "icon-color";
	static final String NEW_TASK_LIST = "new-task-list";
	static final String PROCESS_LIST = "process-list";
	static final String DASHBOARD_BLOCK = "dashboard_block";
	static final String DASHBOARD_BLOCK_TITLE_WRAP = "db-title-wrap";
	static final String NEW_TASK_LIST_TITLE_WRAP = "new-task-title-wrap";
	static final String PROCESS_LIST_TITLE_WRAP = "process-title-wrap";
	static final String DASHBOARD_BLOCK_CONTAINER = "db-container";
	static final String NEW_TASK_CONTAINER = "new-task-container";
	static final String PROCESS_CONTAINER = "process-container";
	static final String DASHBOARD_BLOCK_TITLE = "db-block-title";
	static final String NEW_TASK_TITLE = "new-task-title";
	static final String PROCESS_TITLE = "process-title";
	static final String NEW_TASK_EMPTY = "new-task-empty";
	static final String PROCESS_EMPTY = "process-empty";
	static final String NEW_TASK_BTN = "new-task-btn";
	static final String NEW_TASK_LBL = "new-task-lbl";
	static final String NEW_TASK_ITEM = "new-task-item";
	static final String PROCESS_ITEM = "process-item";
	static final String NEW_TASK_ITEM_NUMBER = "new-task-item-number";
	static final String PROCESS_ITEM_NUMBER = "process-item-number";
	static final String NEW_TASK_ITEM_NUMBER_WRAP = "new-task-item-number-wrap";
	static final String PROCESS_ITEM_NUMBER_WRAP = "process-item-number-wrap";
	static final String NEW_TASK_ITEM_INFO_WRAP = "new-task-item-info-wrap";
	static final String PROCESS_ITEM_INFO_WRAP = "process-item-info-wrap";
	static final String NEW_TASK_ITEM_NAME_WRAP = "new-task-item-name-wrap";
	static final String NEW_TASK_ITEM_NAME = "new-task-item-name";
	static final String PROCESS_ITEM_NAME = "process-item-name";
	static final String NEW_TASK_ITEM_DETAIL_WRAP = "new-task-item-detail-wrap";
	static final String NEW_TASK_CREATE_TIME = "new-task-create-time";
	static final String NEW_TASK_PROCESS = "new-task-process";
	static final String FILE_ITEM = "file-item";
	static final String FILE_BTN_WRAP = "file-btn-wrap";
	static final String FILE_ITEM_LBL = "file-item-lbl";
	static final String FILE_ITEM_BTN_REMOVE = "file-item-btn-remove";
	static final String FILE_ITEM_BTN_DOWNLOAD = "file-item-btn-download";
	static final String UPLOAD_WRAP = "upload-wrap";
	static final String FILE_LIST = "file-list";
	static final String BTN_UPLOAD = "btn-upload";
	static final String MODEL_GRID_WRAP = "model-grid-wrap";
	static final String MODEL_ITEM_WRAP = "model-item-wrap";
	static final String MODEL_ITEM_LIST = "model-item-list";
	static final String MODEL_ITEM = "model-item";
	static final String MODEL_ITEM_IMG_WRAP = "model-item-img-wrap";
	static final String MODEL_ITEM_INFO_WRAP = "model-item-info-wrap";
	static final String MODEL_ITEM_INFO = "model-item-info";
	static final String MODEL_ITEM_LBL = "model-item-lbl";
	static final String MODEL_ITEM_STATUS = "model-item-status";
	static final String MODEL_ITEM_SUSPENDED = "model-item-suspended";
	static final String MODEL_ITEM_USING = "model-item-using";
	static final String MODEL_ITEM_DEPLOY_TIME = "model-item-dep-time";
	static final String MODEL_ITEM_ACTION = "model-item-action";
	
	
	static final String BUTTON_ADDNEW = "btn-addnew";
	static final String BUTTON_DOWNLOAD = "btn-download";
	static final String BUTTON_ADDNEW_SUBTASK = "btn-addnew-subtask";
	static final String BUTTON_IMPORT = "btn-import";
	static final String BUTTON_SAVE = "btn-save";
	
	static final String DETAIL_WRAP = "detail-wrap";
	static final String DETAIL_FORM = "detail-form";
	static final String DETAIL_FORM_BUTTONS = "detail-form-buttons";
	static final String BUTTONS_CENTER = "buttons-center";

	static final String PAGINATION_BAR = "pagination-bar";
	static final String SHOW_MORE_BAR = "show-more-bar";
	static final String PAGINATION_BAR_LEFT = "pagination-bar-left";
	static final String PAGINATION_BAR_RIGHT = "pagination-bar-right";
	static final String PAGINATION_BUTTON = "btn-pagination";
	static final String PAGINATION_BUTTON_NEXT = "btn-pagination-next";
	static final String PAGINATION_BUTTON_PREV = "btn-pagination-prev";
	static final String PAGINATION_BUTTON_FIRST = "btn-pagination-first";
	static final String PAGINATION_BUTTON_LAST = "btn-pagination-last";
	static final String PAGINATION_CUSTOM_PAGE_SIZE = "cbb-pagination";
	static final String PAGINATION_PAGE_INDEX = "txt-pagination";
	static final String LBL_PAGE = "lbl-page";
	static final String LBL_COUNT = "lbl-count";
	

	static final String BTN_GROUP_CENTER = "btn-group-center";
	static final String BTN_POPUP_GROUP_CENTER = "btn-popup-group-center";
	static final String BTN_INIT_GRID_BM = "btn-init-grid-bm";
	static final String BIEU_MAU_GRID_WRAP = "bm-grid-wrap";
	static final String BIEU_MAU_GRID = "bm-grid";

	static final String CELL_DETAIL_WRAP = "cell-detail-wrap";
	static final String BUTTON_MERGE_CELL = "btn-merge-cell";
	static final String BUTTON_MERGE_RIGHT_CELL = "btn-merge-right";
	static final String BUTTON_MERGE_BOTTOM_CELL = "btn-merge-bottom";
	static final String BUTTON_EDIT_CELL = "btn-edit-cell";
	static final String LBL_CELL_INDEX = "lbl-cell-index";
	static final String TXT_CELL_INDEX = "txt-cell-index";

	static final String EDIT_CELL_FORM = "edit-cell-form";
	static final String TEXT_AREA = "text-area-fc";
	static final String BUTTON_ADD_ROW = "btn-add-row";
	static final String MENU_TREE = "menu-tree";

	static final String LBL_CELL_CONTENT = "lbl-cell-content";
	static final String E_FORM_GENERATE = "eform-generate";
	static final String E_FORM_GENERATE_WRAP = "eform-generate-wrap";
	static final String E_FORM_CELL_WRAP = "eform-cell-wrap";
	static final String GRID_DETAIL = "grid-detail";
	static final String RPOC_IMAGE_WRAP = "proc-image-wrap";
	static final String RPOC_IMAGE = "proc-image";
	static final String FORM_CONTROL = "form-gen-control";
	static final String FORM_COMPLETED_USER_CONTROL = "form-gen-completed-u";
	static final String FORM_NUMBER = "form-gen-number";
	static final String FORM_EDITOR = "form-gen-editor";
	static final String FORM_DATE = "form-gen-date";
	static final String FORM_EMAIL = "form-gen-email";
	static final String FORM_INPUT_TEXT = "form-gen-input_text";
	static final String FORM_TEXT_AREA = "form-gen-text-area";
	static final String FORM_CHECKBOX = "form-gen-checkbox";
	static final String FORM_RADIO = "form-gen-radio";
	static final String FORM_COMBOBOX = "form-gen-combobox";
	static final String PEOPLE_TAB_ASSIGN = "people-assignee";
	static final String PEOPLE_TAB_INVOL = "people-invol";
	static final String PEOPLE_TAB_WRAP = "people-wrap";
	static final String PEOPLE_TAB_OWNER = "people-owner";
	static final String PEOPLE_OWNER_LBL = "lbl-people-owner";
	static final String PEOPLE_OWNER_NAME = "people-owner-name";
	static final String PEOPLE_ASSIGNEE_LBL = "lbl-people-assignee";
	static final String PEOPLE_INVOL_LBL = "lbl-people-invol";
	static final String PEOPLE_ASSIGNEE_NAME = "people-assignee-name";
	static final String BTN_TRANSFER = "btn-transfer";
	static final String BTN_ASSIGN = "btn-assign";
	static final String BTN_INVOL = "btn-invol";
	static final String LBL_EMPTY_TAB = "lbl-empty-tab";

	static final String COMMENTS_CONTAINER = "cmt-container";
	static final String CMT_ITEM = "cmt-item";
	static final String COMMENTS_INFO_WRAP = "cmt-info-wrap";
	static final String COMMENTS_INFO = "cmt-info";
	static final String USER_IMAGE_WRAP = "user-img-wrap";
	static final String USER_IMAGE = "user-img";
	static final String USER_SHORT_NAME = "user-shortname";
	static final String USER_CMT_NAME = "user-cmt-name";
	static final String CMT_TIME = "cmt-time";
	static final String USER_TASK_ROLE = "user-task-role";
	static final String CMT_CONTENT_WRAP = "cmt-content-wrap";
	
	
	static final String LBL_CONFIRM = "lbl-confirm";
	static final String BTN_TASK_LIST = "btn-task-list";
	static final String BTN_TASK_LIST_ACTIVE = "btn-task-list-active";
	static final String TASK_LIST_BUTTON_WRAP = "task-list-btn-wrap";
	static final String COMMENT_BOX_WRAP = "cmt-box-wrap";
	static final String COMMENT_BOX = "cmt-box";
	static final String USER_CMT = "user-cmt";
	static final String BTN_POST_COMMENT = "btn-post-comment";
	static final String TASK_DESC = "task-desc";
	
	
	static final String TASK_LIST_WRAP = "task-list-wrap";
	static final String TASK_ITEM = "task-item";
	static final String TASK_IMAGE_WRAP = "task-img-wrap";
	static final String TASK_IMAGE = "task-img";
	static final String TASK_INFO_WRAP = "task-info-wrap";
	static final String TASK_NAME_WRAP = "task-name-wrap";
	static final String TASK_NAME = "task-name";
	static final String TASK_CREATE_TIME = "task-create-time";
	static final String TASK_DESC_WRAP = "task-desc-wrap";
	static final String TASK_ITEM_DESC = "task-item-desc";
	static final String TASK_ITEM_NO_DESC = "task-item-no-desc";
	static final String TASK_PROC_INS_WRAP = "task-proins-wrap";
	static final String TASK_PROC_INS = "task-proins";
	static final String TASK_ACTION_WRAP = "task-action-wrap";
	static final String BTN_TASK_DETAIL = "btn-task-detail";
	static final String TASK_MORE_INFO_WRAP = "task-more-info-wrap";
	static final String TASK_PRIORITY = "task-prioriry";
	static final String TASK_PRIORITY_STAR = "task-prioriry-star";
	static final String TASK_DUEDATE = "task-duedate";
	static final String TASK_DETAIL_PRIORITY_STAR = "task-detail-prioriry-star";
	static final String TASK_DETAIL_CREATE_DATE = "task-detail-create-date";
	

	static final String ADD_RELATED_CONTENT_WRAP = "add-related-wrap";
	static final String BTN_ADD_RELATED_CONTENT = "btn-add-related-wrap";
	static final String RELATED_CONTENT_WRAP = "related-wrap";
	static final String ATTACHMENT_ITEM = "attachment-item";
	static final String ATTACHMENT_ITEM_IMG_WRAP = "attachment-item-img-wrap";
	static final String ATTACHMENT_ITEM_IMG = "attachment-item-img";
	static final String ATTACHMENT_ITEM_INFO_WRAP = "attachment-item-info-wrap";
	static final String ATTACHMENT_ITEM_NAME_WRAP = "attachment-item-name-wrap";
	static final String ATTACHMENT_ITEM_NAME = "attachment-item-name";
	static final String ATTACHMENT_ITEM_USER = "attachment-item-user";
	static final String ATTACHMENT_ITEM_TIME = "attachment-item-time";
	static final String ATTACHMENT_ITEM_DESC_WRAP = "attachment-item-desc-wrap";
	static final String ATTACHMENT_ITEM_DESC = "attachment-item-desc";
	static final String ATTACHMENT_ITEM_ACTION_WRAP = "attachment-item-action-wrap";
	static final String ATTACHMENT_ITEM_ACTION_BTN = "attachment-item-action-btn";
	

	static final String FORM_START_WRAP = "form-start-wrap";
	static final String HTML_DATA_LBL = "lbl-html-table";

	static final String REMOVE_BTN_ACT = "btn-remove-act";
	static final String GROUP_REPORT = "group-report";
	static final String BTN_ADD_RP = "btn-add-rpt";
	static final String BTN_ADD_FIELD = "btn-add-field";
	static final String BTN_REMOVE_TBL_RPT = "btn-remove-tbl-rpt";
	
	static final String PAGING_WRAP = "paging-wrap";

	static final String FILE_INFO_TITLE_WRAP = "file-title-wrap";
	static final String FILE_INFO_TITLE_LBL = "file-title-lbl";
	static final String FILE_INFO_TITLE = "file-title";

	static final String FILE_INFO_DESC_WRAP = "file-desc-wrap";
	static final String FILE_INFO_DESC_LBL = "file-desc-lbl";
	static final String FILE_INFO_DESC = "file-desc";
	static final String FILE_ITEM_TITLE = "file-item-label";
	static final String FILE_ITEM_ICON = "file-item-icon";

	static final String FORM_BTN_CONTAINER = "btn-form-container";
	static final String FORM_BTN_WRAP = "btn-form-wrap";
	static final String ADD_BTN_WRAP = "add-btn-wrap";
	static final String BTN_ADD_BTN = "btn-add-btn";
	

	static final String GROUP_BTN_WRAP = "group-btn-wrap";
	static final String BTN_ADD_BTN_WRAP = "btn-add-btn-wrap";
	static final String BTN_ADD_GROUP = "btn-add-group";

	static final String FILTER_WRAP = "filter-wrap";
	static final String PROCESS_FILTER_WRAP = "process-filter-wrap";
	static final String DATE_FILTER_WRAP = "date-filter-wrap";
	static final String FROM_DATE_FILTER_WRAP = "from_date-filter-wrap";
	static final String TO_DATE_FILTER_WRAP = "to_date-filter-wrap";
	static final String SUBMIT_FILTER_WRAP = "submit-filter-wrap";
	static final String PERIODIC_FILTER_WRAP = "periodic-filter-wrap";
	
	//Dashboard
	static final String DATA_ROW = "data-row";
	static final String TK_FINISHED = "tk-finished";
	static final String TK_QUEUE = "tk-queue";
	static final String TK_ASSIGN = "tk-assign";
	static final String TK_INBOX = "tk-inbox";
	static final String TK_OWNER = "tk-owner";
	static final String ITEM_WRAP = "item-wrap";
	static final String DASHBOARD_CONTAINER = "dashboard-container";
	
	//Menu bar 

	static final String USER_OPTION_WRAP = "u-option-wrap";
	static final String USER_OPTION_BTN = "u-option-btn";
	
	
	static final String MESSAGE_BAR = "mssg-bar";
	static final String MESSAGE_ITEM = "mssg-item";
	static final String MESSAGE_NUMBER = "mssg-number";
	static final String MESSAGE_BUTTON = "mssg-btn";
	
	//
	static final String PAGE_TITLE_WRAP = "page-title-wrap";
	static final String PAGE_TITLE = "page-title";
	static final String PAGE_SMALL_TITLE = "page-small-title";
	static final String BTN_SHOW_SEARCH_FORM = "btn-show-search";
	static final String PAGE_TITLE_ACTION_WRAP = "page-title-action-wrap";
	

	static final String PAGE_BREADCRUMB_WRAP = "page-breadcrumb-wrap";
	static final String PAGE_BREADCRUMB_ITEM = "page-breadcrumb-item";
	static final String PAGE_BREADCRUMB_SPACE = "page-breadcrumb-space";
	

	static final String FORM_ROW = "form-row";
	static final String FORM_ROW_TITLE = "form-row-ttl";

	static final String FORM_TTT_GENERAL = "form-ttt-general";
	static final String FORM_TTT_SETTING = "form-ttt-setting";
	static final String TTT_FORM_BUTTONS = "ttt-form-buttons";
	static final String TTT_FORM_WRAP = "ttt-form-wrap";
	static final String BTN_SHOW_GENERAL_FORM = "btn-show-general-form";
	static final String BTN_SHOW_DETAIL = "btn-show-detail";
	static final String DETAIL_ROW_WRAP = "detail-row-wrap";
	static final String PROCESS_DETAIL = "process-detail";
	static final String BTN_SHOW_SETTING_FORM = "btn-show-setting-form";
	

	static final String BTN_ADD_LOAI_DM = "btn-add-loai-dm";
	static final String CBB_LOAI_DM = "cbb-loai-dm";
	
	//Profile

	static final String USER_PAGE_WRAP = "u-page-wrap";
	static final String USER_GENERAL_INFO_WRAP = "u-general-info-wrap";
	static final String USER_DETAIL_INFO_WRAP = "u-detail-info-wrap";
	static final String USER_PIC_WRAP = "u-pic-wrap";
	static final String USER_PIC = "u-pic";
	static final String USER_PIC_HEADER = "u-pic-header";
	static final String USER_PIC_SHORT_NAME = "u-pic-short-name";
	static final String LBL_USER_NAME = "lbl-u-name";
	static final String LBL_USER_POSITION = "lbl-u-position";
	static final String LBL_USER_UNIT = "lbl-u-unit";
	
	static final String LBL_USER_INFO_TTL_WRAP = "lbl-u-info-ttl-wrap";
	static final String LBL_USER_INFO_TTL = "lbl-u-info-ttl";
	static final String USER_INFO_TAB = "u-info-tab";
	static final String BTN_CHANGE_PIC = "btn-change-pic";
	static final String USER_PIC_POPUP = "u-pic-popup";
	static final String USER_PIC_POPUP_WRAP = "u-pic-popup-wrap";
	static final String CHANGE_PIC_POPUP_CONTENT = "change-pic-popup-content";
	
	static final String BTN_UPLOAD_PIC_POPUP_WRAP = "btn-upload-pic-popup-wrap";
	static final String BTN_UPLOAD_PIC_POPUP = "btn-upload-pic-popup";
	

	static final String LBL_STATUS = "lbl-status";
	static final String STATUS_COMMENT_WRAP = "status-cmt-wrap";
	static final String STATUS_COMMENT = "status-cmt";
	

	static final String DON_VI_LIST_WRAP = "dv-list-wrap";
	static final String DON_VI_LBL = "dv-lbl";

	static final String PROCESS_DETAIL_CONTAINER = "process-detail-container";
	static final String PROCESS_DETAIL_TAB = "process-detail-tab";
	

	static final String OPTION_WRAP = "option-wrap";
	static final String BTN_ADD_ROW = "btn-add-row-generated";
	static final String BTN_ADD_ROW_WRAP = "btn-add-row-generated-wrap";
	static final String LAST_CELL_WRAP = "last-cell-wrap";
	static final String BTN_REMOVE_ROW = "btn-remove-row";
	static final String BTN_REMOVE_ROW_WRAP = "btn-remove-row-wrap";
	

	static final String CHECK_BOX_LAP_HANG = "chk-lap-hang";

	static final String TITLE_WRAP_PROCESS_DETAIL = "process-detail-title-wrap";
	static final String PROCESS_DETAIL_CONTENT = "process-detail-content";
	static final String DETAIL_FORM_GEN_WRAP = "detail-form-gen-wrap";
	

	static final String PROCESS_DEF_CONTAINER = "pro-def-container";
	static final String PROCESS_DEF_WRAP = "pro-def-wrap";
	static final String PROCESS_DEF_ITEM_WRAP = "pro-def-item-wrap";
	static final String PROCESS_DEF_ICON_WRAP = "pro-def-icon-wrap";
	static final String PROCESS_DEF_ICON = "pro-def-icon";
	static final String PROCESS_DEF_INFO_WRAP = "pro-def-info-wrap";
	static final String PROCESS_DEF_NAME = "pro-def-name";
	static final String PROCESS_DEF_DESC = "pro-def-desc";
	static final String PROCESS_DEF_NUMBER_WRAP = "pro-def-number-wrap";
	static final String PROCESS_DEF_NUMBER = "pro-def-number";
	
	static final String PROCESS_DEF_ITEM = "pro-def-item";
	static final String PROCESS_TASK_ITEM = "pro-task-item";
	static final String PROCESS_TASK_WRAP = "pro-task-wrap";
	static final String PROCESS_TASK_ICON = "pro-task-icon";
	static final String PROCESS_TASK_INDEX = "pro-task-ind";
	static final String PROCESS_TASK_PRIORITY_STAR = "pro-task-priority-star";
	static final String PROCESS_TASK_INFO = "pro-task-info";
	static final String PROCESS_TASK_NAME_WRAP = "pro-task-name-wrap";
	static final String PROCESS_TASK_NAME = "pro-task-name";
	static final String PROCESS_TASK_NAME_LINK = "pro-task-name-link";
	static final String PROCESS_TASK_CREATE_TIME = "pro-task-create-time";
	static final String PROCESS_TASK_CREATE_TIME_LBL = "pro-task-create-time_lbl";
	static final String PROCESS_TASK_DESC_WRAP = "pro-task-desc-wrap";
	static final String PROCESS_TASK_PRO_INS_INFO_WRAP = "pro-task-pro-ins-info-wrap";
	static final String PROCESS_TASK_DUEDATE_WRAP = "pro-task-duedate-wrap";
	static final String PROCESS_TASK_DESC = "pro-task-desc";
	

	static final String PROCESS_TASK_DUEDATE_LBL = "pro-task-duedate_lbl";
	

	static final String TASK_NAV_WRAP = "task-nav";
	static final String TASK_NAV_TITLE = "task-nav-title";
	static final String TASK_NAV_SMALL_TITLE = "task-nav-small-title";
	static final String TASK_NAV_BTN_CONTAINER = "task-nav-btn-container";
	static final String TASK_NAV_BTN_WRAP = "task-nav-btn-wrap";
	static final String TASK_NAV_BTN = "task-nav-btn";
	static final String TASK_NAV_BTN_ACTIVE = "task-nav-btn-active";
	static final String TASK_NAV_NUMBER = "task-nav-number";
	static final String TASK_NAV_NUMBER_ZERO = "task-nav-number-zero";
	

	static final String TASK_LIST_EMPTY = "task-list-empty";
	

	static final String BTN_SHOW_MORE_WRAP = "btn-show-more-wrap";
	static final String BTN_SHOW_MORE = "btn-show-more";
	

	static final String APP_ITEM = "app-item";
	static final String APP_ITEM_ICON_WRAP = "app-item-icon-wrap";
	static final String APP_ITEM_INFO_WRAP = "app-item-ifo-wrap";
	static final String APP_ITEM_ICON = "app-item-icon";
	static final String APP_ITEM_DESC_WRAP = "app-item-desc-wrap";
	static final String APP_ITEM_DESC = "app-item-desc";
	static final String APP_ITEM_NAME = "app-item-name";
	static final String APP_ITEM_ACTION_WRAP = "app-item-action-wrap";
	static final String APP_ITEM_ACTION = "app-item-action";
	static final String BTN_EDIT_MODELER = "btn-edit-modeler";
	

	static final String GRID_FORM_LAYOUT = "grid-form-layout";
	static final String GRID_ROW_FORM_LAYOUT = "grid-row-form-layout";
	static final String GRID_CELL_FORM_LAYOUT = "grid-cell-form-layout";

	static final String BTN_PROCESS_DETAIL_WRAP = "btn-process-detail-wrap";
	static final String BTN_PROCESS_DETAIL = "btn-process-detail";
	

	static final String TEXT_CELL_CONTENT = "txt-cell-content";
	

	static final String ANIMATE_BOUNCE = "bounce";
	static final String ANIMATE_BOUNCE_IN = "bounceIn";

	static final String BTN_GEN_PASS = "btn-gen-pass";
	static final String TXT_GEN_PASS = "txt-gen-pass";
	static final String USER_FORM = "user-detail-form";
	
	

	static final String TASK_DETAIL_TITLE_WRAP = "task-detail-ttl-wrap";
	static final String TASK_DETAIL_TITLE = "task-detail-ttl";
	

	static final String TASK_TIME_LINE_WRAP = "task-tl-wrap";
	static final String TASK_TIME_LINE_SUBPROCESS_WRAP = "task-tl-subprocess-wrap";
	static final String TASK_TIME_LINE_SUPER_WRAP = "task-tl-super-wrap";
	static final String TASK_TIME_LINE_START_YEAR_WRAP = "task-tl-start-year-wrap";
	static final String TASK_TIME_LINE_START_YEAR = "task-tl-start-year";
	static final String TASK_TIME_LINE_ITEM = "task-tl-item";
	static final String TASK_TIME_LINE_DATE_WRAP = "task-tl-date-wrap";
	
	static final String TASK_TIME_LINE_INFO = "task-tl-info";
	static final String TASK_TIME_LINE_DATE = "task-tl-date";
	static final String TASK_TIME_LINE_DATE_SUBPROCESS = "task-tl-date-subprocess";
	static final String TASK_TIME_LINE_DATE_SUBPROCESS_WRAP = "task-tl-date-subprocess-wrap";
	static final String TASK_TIME_LINE_DATE_SUPER_WRAP = "task-tl-date-super-wrap";
	
	static final String TASK_TIME_LINE_EVENT = "task-tl-event";
	static final String TASK_TIME_LINE_EVENT_TITLE_WRAP = "task-tl-event-ttl-wrap";
	static final String TASK_TIME_LINE_EVENT_TITLE = "task-tl-event-ttl";
	static final String TASK_TIME_LINE_MAIN = "task-tl-main-content";
	static final String TASK_TIME_LINE_EVENT_DESC_WRAP = "task-tl-event-desc-wrap";
	static final String TASK_TIME_LINE_EVENT_DESC = "task-tl-event-desc";
	static final String TASK_TIME_LINE_EVENT_OWNER_WRAP = "task-tl-event-owner-wrap";
	static final String TASK_TIME_LINE_EVENT_OWNER = "task-tl-event-owner";
	static final String TASK_TIME_LINE_SUBPROCESS_BTN = "task-tl-btn-subprocess";
	

	static final String TASK_TIME_LINE_DETAIL_WRAP = "task-tl-detail-wrap";
	

	static final String TASK_TIME_LINE_EVENT_START_WRAP = "task-tl-event-start-wrap";
	static final String TASK_TIME_LINE_EVENT_START = "task-tl-event-start";
	static final String TASK_TIME_LINE_START_WRAP = "task-tl-start-wrap";
	static final String TASK_TIME_LINE_START = "task-tl-start";
	static final String TASK_TIME_LINE_START_DATE = "task-tl-start-date";
	static final String TASK_TIME_LINE_START_TITLE_WRAP = "task-tl-start-ttl-wrap";
	static final String TASK_TIME_LINE_START_TITLE = "task-tl-start-ttl";

	static final String TASK_TIME_LINE_EVENT_FINISH_WRAP = "task-tl-event-finish-wrap";
	static final String TASK_TIME_LINE_DUEDATE_WRAP = "task-tl-duedate-wrap";
	static final String TASK_TIME_LINE_DUEDATE = "task-tl-duedate";
	static final String TASK_TIME_LINE_EVENT_FINISH = "task-tl-event-finish";
	static final String TASK_TIME_LINE_FINISH_WRAP = "task-tl-finish-wrap";
	static final String TASK_TIME_LINE_FINISH = "task-tl-finish";
	static final String TASK_TIME_LINE_FINISH_DATE = "task-tl-finish-date";
	static final String TASK_TIME_LINE_FINISH_TITLE_WRAP = "task-tl-finish-ttl-wrap";
	static final String TASK_TIME_LINE_FINISH_TITLE = "task-tl-finish-ttl";

	static final String TASK_TIME_LINE_ITEM_DETAIL_WRAP = "task-tl-i-detail-wrap";
	static final String TASK_TIME_LINE_ITEM_DETAIL_BTN = "task-tl-i-detail-btn";
	static final String TASK_TIME_LINE_ITEM_DETAIL_BTN_LOADED = "task-tl-i-detail-btn-loaded";
	static final String TASK_TIME_LINE_ITEM_DETAIL = "task-tl-i-detail";
	static final String TASK_TIME_LINE_ITEM_DETAIL_CONTENT = "task-tl-i-detail-content";
	static final String TASK_TIME_LINE_ITEM_DETAIL_CONTENT_ACTIVE = "task-tl-i-detail-content-active";
	
	static final String TASK_TIME_LINE_ITEM_DETAIL_CONTAINER = "task-tl-i-detail-container";
	

	static final String TASK_TIME_LINE_EVENT_RUNNING_WRAP = "task-tl-event-running-wrap";
	static final String TASK_TIME_LINE_EVENT_RUNNING = "task-tl-event-running";
	

	static final String TASK_DETAIL_FORM_WRAP = "td-form-wrap";
	static final String TASK_DETAIL_HEADER_WRAP = "td-title-wrap";
	static final String TASK_BTN_CLAIM = "td-btn-claim";
	static final String TASK_CLAIM_WRAP = "td-claim-wrap";
	static final String TASK_DETAIL_PRIORITY_WRAP = "td-priority-wrap";
	static final String TASK_DETAIL_PRIORITY = "td-priority";
	static final String TASK_DETAIL_NAME_WRAP = "td-name-wrap";
	static final String TASK_DETAIL_NAME = "td-name";
	static final String TASK_DETAIL_CREATE_TIME_WRAP = "td-create-time-wrap";
	static final String TASK_DETAIL_CREATE_TIME = "td-create-time";
	static final String TASK_DETAIL_DUEDATE = "td-duedate";
	static final String TASK_DETAIL_HISTORY_BTN = "td-history-btn";
	

	static final String TASK_DETAIL_COMMENT_WRAP = "td-comment-wrap";
	static final String TASK_DETAIL_SUBTASK_WRAP = "td-subtask-wrap";
	static final String TASK_DETAIL_SUBTASK_CONTENT = "td-subtask-content";
	static final String TASK_DETAIL_INTERACTIVE_WRAP = "td-interactive-wrap";

	static final String TASK_DETAIL_COMMENT_HEADER = "td-comment-header";
	static final String TASK_DETAIL_COMMENT_TOTAL_WRAP = "td-cmt-total-wrap";
	static final String TASK_DETAIL_COMMENT_TOTAL = "td-cmt-total";
	static final String TASK_DETAIL_COMMENT_LBL = "td-cmt-lbl";

	static final String TASK_DETAIL_COMMENT_CMT_COUNT = "td-cmt-cmt-count";
	static final String TASK_DETAIL_COMMENT_CMT = "td-cmt-cmt";
	static final String TASK_DETAIL_COMMENT_ATT_COUNT = "td-cmt-att-count";
	static final String TASK_DETAIL_TASK_EVENT_COUNT = "td-task-event-count";
	static final String TASK_DETAIL_COMMENT_BUTTONS = "td-cmt-buttons";
	static final String TASK_DETAIL_NAV_WRAP = "td-nav-wrap";
	static final String TASK_DETAIL_NAV_COMMENT = "td-nav-cmt";
	static final String TASK_DETAIL_NAV_OPEN_BTN = "td-nav-open-btn";
	static final String TASK_DETAIL_NAV_ATT = "td-nav-att";
	static final String TASK_DETAIL_NAV_EVENT = "td-nav-event";
	static final String TASK_DETAIL_COMMENT_BOX = "td-cmt-box";
	static final String TASK_DETAIL_COMMENT_IMG = "td-cmt-img";
	static final String TASK_DETAIL_COMMENT_IMG_WRAP = "td-cmt-img-wrap";
	static final String TASK_DETAIL_COMMENT_TXT_WRAP = "td-cmt-txt-wrap";
	static final String TASK_DETAIL_COMMENT_TXT = "td-cmt-txt";

	static final String TASK_DETAIL_COMMENT_ITEM = "td-cmt-item";
	static final String TASK_DETAIL_COMMENT_DATE_WRAP = "td-cmt-date-wrap";
	static final String TASK_DETAIL_COMMENT_DATE = "td-cmt-date";
	static final String TASK_DETAIL_COMMENT_USER = "td-cmt-user";
	static final String TASK_DETAIL_COMMENT_USER_WRAP = "td-cmt-user-wrap";
	static final String TASK_DETAIL_COMMENT_INFO = "td-cmt-info";
	static final String TASK_DETAIL_COMMENT_CONTENT_WRAP = "td-cmt-content-wrap";
	static final String TASK_DETAIL_COMMENT_CONTENT = "td-cmt-content";
	static final String TASK_DETAIL_COMMENT_CONTAINER = "td-cmt-container";
	static final String TASK_DETAIL_BTN_DELETE_COMMENT = "td-cmt-btn-del-cmt";
	

	static final String TASK_DETAIL_ATT_WRAP = "td-att-wrap";
	static final String TASK_DETAIL_ATT_HEADER = "td-att-header";
	static final String TASK_DETAIL_ATT_COUNT = "td-att-count";
	static final String TASK_DETAIL_ATT_CONTAINER = "td-att-container";
	static final String TASK_DETAIL_ATT_ITEM = "td-att-item";
	static final String TASK_DETAIL_ATT_ITEM_NAME = "td-att-name";
	static final String TASK_DETAIL_ATT_ITEM_ICON = "td-att-icon";
	static final String TASK_DETAIL_ATT_ITEM_NAME_WRAP = "td-att-name-wrap";
	static final String TASK_DETAIL_ATT_ITEM_USER = "td-att-user";
	static final String TASK_DETAIL_ATT_ITEM_TIME = "td-att-time";
	static final String TASK_DETAIL_ATT_ITEM_INFO_WRAP = "td-att-info-wrap";
	static final String TASK_DETAIL_ATT_ITEM_DONWLOAD_WRAP = "td-att-download-wrap";
	static final String TASK_DETAIL_ATT_ITEM_DONWLOAD_BTN = "td-att-download-btn";
	static final String TASK_DETAIL_ATT_ITEM_ADD_BTN = "td-att-add-btn";
	static final String TASK_DETAIL_ATT_ITEM_TYPE = "td-att-type";
	static final String TASK_DETAIL_ATT_ITEM_TYPE_EMPTY = "td-att-type-empty";
	

	static final String TASK_DETAIL_ATT_ITEM_ADD = "td-att-item-add";
	
	
	static final String TASK_DETAIL_USER_WRAP = "td-user-wrap";
	static final String TASK_DETAIL_USER_MAIN = "td-user-main";
	static final String TASK_DETAIL_USER_ASSIGNEE_WRAP = "td-user-assignee-wrap";
	static final String TASK_DETAIL_USER_ASSIGNEE_LBL = "td-user-assignee-lbl";
	static final String TASK_DETAIL_USER_OWNER_WRAP = "td-user-owner-wrap";
	static final String TASK_DETAIL_USER_OWNER_LBL = "td-user-owner-lbl";
	static final String TASK_DETAIL_USER_ASSIGNEE = "td-user-assignee";
	static final String TASK_DETAIL_USER_ASSIGNEE_BTN = "td-user-assignee-btn";
	static final String TASK_DETAIL_USER_OWNER = "td-user-owner";
	static final String TASK_DETAIL_USER_OWNER_BTN = "td-user-owner-btn";
	static final String TASK_DETAIL_USER_RELATED_WRAP = "td-user-rl-wrap";
	static final String TASK_DETAIL_USER_RELATED_LBL_WRAP = "td-user-rl-lbl-wrap";
	static final String TASK_DETAIL_USER_RELATED_LBL = "td-user-rl-lbl";
	static final String TASK_DETAIL_USER_RELATED_SEEMORE = "td-user-rl-see-more";
	static final String TASK_DETAIL_USER_RELATED_BTN_ADD = "td-user-rl-btn-add";
	static final String TASK_DETAIL_TASK_EVENT_BTN = "td-task-event-btn";
	static final String TASK_DETAIL_USER_RELATED = "td-user-rl";
	static final String TASK_DETAIL_USER_RELATED_CONTAINER = "td-user-rl-container";
	static final String TASK_DETAIL_USER_RELATED_ITEM = "td-user-rl-i";
	static final String TASK_DETAIL_GROUP_RELATED_ITEM = "td-group-rl-i";
	static final String TASK_DETAIL_HISTORY_RELATED_ITEM = "td-history-rl-i";
	static final String TASK_DETAIL_USER_RELATED_ITEM_REMOVE = "td-user-rl-i-remove";
	static final String TASK_DETAIL_USER_RELATED_ITEM_NAME = "td-user-rl-i-name";
	static final String TASK_DETAIL_USER_RELATED_ITEM_NAME_HISTORY = "td-user-rl-i-name-history";
	static final String TASK_DETAIL_USER_RELATED_GROUP = "td-user-rl-group";
	static final String TASK_DETAIL_USER_RELATED_GROUP_LBL = "td-user-rl-group-lbl";
	

	static final String BUTTON_CONFIRM_OK = "btn-confirm-ok";
	static final String BUTTON_CONFIRM_CANCEL = "btn-confirm-cancel";
	

	static final String TASK_EVENT_POPUP = "te-popup";
	static final String TASK_EVENT_POPUP_ITEM = "te-popup-i";
	static final String TASK_EVENT_POPUP_ITEM_CONTENT = "te-popup-i-content";
	static final String TASK_EVENT_POPUP_ITEM_ICON = "te-popup-i-icon";
	static final String TASK_EVENT_POPUP_ITEM_ICON_ADD = "te-popup-i-icon-add";
	static final String TASK_EVENT_POPUP_ITEM_ICON_REMOVE = "te-popup-i-icon-remove";
	static final String TASK_EVENT_POPUP_ITEM_TIME = "te-popup-i-time";
	

	static final String SEARCH_NO_RESULT_LBL = "search-no-result-lbl";
	static final String SEARCH_RESULT_WRAP = "search-result-wrap";
	static final String SEARCH_TOTAL_RESULT_LBL = "search-total-result-lbl";

	static final String SEARCH_BOX_WRAP = "search-box-wrap";
	static final String SEARCH_BOX = "search-box";

	static final String SEARCH_ITEM = "search-item";
	static final String SEARCH_ITEM_BM = "search-item-bm";
	static final String SEARCH_ITEM_ICON = "search-item-icon";
	static final String SEARCH_ITEM_LABEL = "search-item-lbl";
	static final String SEARCH_ITEM_TITLE = "search-item-ttl";
	

	static final String FORM_HTML_DATA = "form-html-data";
	static final String PROCESS_INS_DIAGRAM = "process-ins-diagram";
	static final String PROCESS_INS_CONTAINER = "process-ins-container";
	static final String COLLAPSE_PANEL_LAYOUT = "collapse-panel";
	static final String COLLAPSE_PANEL_ITEM = "collapse-panel-item";
	static final String COLLAPSE_PANEL_ITEM_HEAD = "collapse-panel-item-head";
	static final String COLLAPSE_PANEL_ITEM_TITLE = "collapse-panel-item-ttl";
	static final String COLLAPSE_PANEL_ITEM_BTN = "collapse-panel-item-btn";
	static final String COLLAPSE_PANEL_ITEM_CONTENT = "collapse-panel-item-content";
	static final String PROCESS_INS_DEL_REASON_TXT = "process-ins-del-reason-txt";
	static final String PROCESS_INS_DEL_REASON_FORM = "process-ins-del-reason-form";
	
	

	static final String CHAT_BOX_WRAP = "chat-box-wrap";
	static final String CHAT_BOX = "chat-box";

	static final String CHAT_BOX_COLLAPSE = "chat-box-collapse";

	static final String HELP_WRAP = "help-wrap";
	static final String HELP_POPUP = "help-popup";
	static final String HELP_POPUP_CONTENT = "help-popup-content";
	static final String HELP_POPUP_BACKGROUND = "help-popup-bg";
	static final String HELP_BTN = "help-btn";
	static final String HELP_BTN_LOADED = "help-btn-loaded";
	static final String HELP_BTN_CLOSE = "help-btn-close";
	static final String TASK_DETAIL_MAIN_CONTAINER = "td-main-container";
	
	

	static final String PROCESS_DEFINITION_ITEM = "pd-item";
	static final String PROCESS_DEFINITION_ITEM_INFO = "pd-item-info";
	static final String PROCESS_DEFINITION_ITEM_NAME_WRAP = "pd-item-name-wrap";
	static final String PROCESS_DEFINITION_ITEM_SHORT_NAME = "pd-item-short-name";
	static final String PROCESS_DEFINITION_ITEM_SHORT_NAME_LBL = "pd-item-short-name-llb";
	static final String PROCESS_DEFINITION_ITEM_NAME = "pd-item-name";
	static final String PROCESS_DEFINITION_ITEM_NAME_LBL = "pd-item-name-lbl";
	static final String PROCESS_DEFINITION_ITEM_PUB_DATE = "pd-item-pub-date";
	static final String PROCESS_DEFINITION_ITEM_STATUS = "pd-item-status";
	static final String PROCESS_DEFINITION_ITEM_STATUS_SUSPEND = "pd-item-suspend";
	static final String PROCESS_DEFINITION_ITEM_STATUS_USING = "pd-item-using";

	static final String PROCESS_DEFINITION_ITEM_IMAGE_WRAP = "pd-item-img-wrap";
	static final String PROCESS_DEFINITION_ITEM_IMAGE = "pd-item-img";
	static final String PROCESS_DEFINITION_ITEM_BUTTONS = "pd-item-btns";
	static final String PROCESS_DEFINITION_ITEM_BUTTON = "pd-item-btn";
	static final String PROCESS_DEFINITION_ITEM_DESC = "pd-item-desc";
	static final String PROCESS_DEFINITION_ITEM_DESC_LBL = "pd-item-desc-lbl";
	static final String PROCESS_DEFINITION_ITEM_NAME_LBL_D = "pd-item-name-lbl-d";
	static final String PROCESS_DEFINITION_ITEM_BTN_VIEW = "pd-item-btn-view";
	
	

	static final String PROCESS_DEFINITION_TXT_SEARCH = "pd-item-txt-search";
	static final String FORM_GEN_LBL = "e-form-lbl";
	

	static final String FILE_LIST_HTML = "file-list-html";
	static final String FILE_LIST_HTML_LINK = "file-list-html-link";
	
	
	

	static final String CALC_BTN_WRAP = "calc-btn-wrap";
	static final String CALC_BTN = "calc-btn";
	

	static final String FORMULA_CONTAINER = "formula-container";
	static final String FORMULA_WRAP = "formula-wrap";
	static final String FORMULA_ITEM = "formula-item";
	static final String FORMULA_REMOVE = "formula-remove";
	static final String FORMULA_REMOVE_BTN = "formula-remove-btn";
	
	
	static final String FORMULA_ITEM_FIELD_WRAP = "formula-item-fw";
	static final String TRUONG_TT_SEACH = "truong-tt-s";
	static final String TRUONG_TT_SEACH_FIELD = "truong-tt-s-field";

	static final String GROUP_COLUMN_CELL = "group-column-cell";
	static final String GROUP_COLUMN_CELL_LBL = "group-column-cell-lbl";

	static final String IMPORT_WINDOWN_CONTENT = "import-window-content";
	static final String BTN_UPLOAD_BPMN = "btn-upload-bpmn";

	static final String PRO_DEF_DESC_FORM = "prodef-desc-form";
	static final String PRO_DEF_DESC_TXT = "prodef-desc-txt";
	static final String PRO_DEF_CATE_TAB = "prodef-cate-tab";
	static final String PRO_DEF_TAB_BTN = "prodef-tab-btn";
	static final String PRO_DEF_TAB_BTN_ACTIVE = "prodef-tab-btn-active";
	

	static final String BLOG_WRAP = "blog-wrap";
	static final String BLOG_BTN = "blog-btn";
	

	static final String NOTIFICATION_BLOCK = "notif-block";
	static final String NOTIFICATION_ITEM = "notif-item";
	static final String NOTIFICATION_ITEM_LBL = "notif-item-lbl";
	static final String NOTIFICATION_ITEM_BTN = "notif-item-btn";

	static final String NOTIFICATION_WRAP = "notif-wrap";
	static final String NOTIFICATION_BTN = "notif-btn";
	static final String NOTIFICATION_LBL = "notif-lbl";
	static final String NOTIFICATION_ICON = "notif-icon";
	static final String NOTIFICATION_CONTAINER = "notif-container";
	static final String NOTIFICATION_LIST_ITEM = "notif-list-item";
	static final String NOTIFICATION_LIST_ITEM_CLICKED = "notif-list-item-clicked";
	static final String N_LIST_ITEM_UIMG_WRAP = "n-list-item-uimgs";
	static final String N_LIST_ITEM_UIMG = "n-list-item-uimg";
	static final String N_LIST_ITEM_SHORT_NAME = "n-list-item-shortname";
	static final String N_LIST_ITEM_CONTENT = "n-list-item-content";
	static final String N_LIST_ITEM_CONTENT_LBL = "n-list-item-content-lbl";
	static final String N_LIST_ITEM_BTN = "n-list-item-btn";

	static final String N_LIST_VIEW_MORE_WRAP = "n-list-view-more-wrap";
	static final String N_LIST_VIEW_MORE_BTN = "n-list-view-more-btn";

	static final String NOTIFICATION_CONTAINER_WRAP = "notif-container-wrap";
}

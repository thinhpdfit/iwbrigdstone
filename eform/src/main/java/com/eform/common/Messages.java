package com.eform.common;

import org.activiti.engine.repository.ProcessDefinition;

public class Messages {
	//Home
	public static final String UI_VIEW_HOME = "ui.view.home";
	//#
	public static final String UI_VIEW_NEW = "ui.view.new";
	public static final String UI_VIEW_ACTIVE = "ui.view.acticve";
	public static final String UI_VIEW_OFF = "ui.view.off";
	public static final String UI_VIEW_COMPLETED = "ui.view.completed";
	public static final String UI_VIEW_PROCESSING = "ui.view.processing";
	public static final String UI_ITEM = "ui.view.item";
	
	
	public static final String UI_VIEW_TABLE_HEADER_ID = "ui.view.table.header.id";
	public static final String UI_VIEW_TABLE_HEADER_NAME = "ui.view.table.header.name";
	public static final String UI_VIEW_TABLE_HEADER_NOTE = "ui.view.table.header.note";
	public static final String UI_VIEW_TABLE_HEADER_STARTTIME = "ui.view.table.header.starttime";
	public static final String UI_VIEW_TABLE_HEADER_ENDTIME = "ui.view.table.header.endtime";
	public static final String UI_VIEW_TABLE_HEADER_ACTIONS = "ui.view.table.header.actions";
	public static final String UI_VIEW_TABLE_HEADER_PROCESSDEFINITION = "ui.view.table.header.processdefinition";
	public static final String UI_VIEW_TABLE_HEADER_TYPE = "ui.view.table.header.type";
	public static final String UI_VIEW_TABLE_HEADER_REFERENCEFIELD = "ui.view.table.header.referencefield";
	public static final String UI_VIEW_TABLE_HEADER_FILTERBYORGANIZATION = "ui.view.table.header.filterbyorganization";
	public static final String UI_VIEW_FIELDINFO = "ui.view.fielddinfo";
	public static final String UI_VIEW_CELLINFO = "ui.view.ui.view.cellinfo";
	public static final String UI_VIEW_TABLE_DISPLAY_BUTTON = "ui.view.table.display.button";
	
	//error
	public static final String UI_MESSAGES_ISREADY = "ui.messages.isready";
	public static final String UI_MESSAGES_REQUIREMENT = "ui.messages.requirement";
	public static final String UI_MESSAGES_SYSTEM = "ui.messages.system";
	public static final String UI_MESSAGES_UPDATESUCCESSFUL = "ui.messages.updatesuccessful";
	//Label
	public static final String UI_LABEL_ID = "ui.label.id";
	public static final String UI_LABEL_ID_ERROR_MESSAGES_MAXLENG20 = "ui.label.id.error.messages.maxleng20";
	public static final String UI_LABEL_ID_ERROR_MESSAGES_MAXLENG2000 = "ui.label.id.error.messages.maxleng2000";
	public static final String UI_LABEL_ID_ERROR_MESSAGES_CHARATER09ZA = "ui.label.id.error.messages.charater09za";
	public static final String UI_LABEL_ID_ERROR_MESSAGES_NUMBERLESSTHAN = "ui.label.id.error.messages.numberlessthan";
	
	public static final String UI_LABEL_NAME = "ui.label.name";
	
	public static final String UI_LABEL_WIDTH = "ui.label.width";
	public static final String UI_LABEL_WIDTH_EX = "ui.label.width.ex";
	public static final String UI_LABEL_COLUMN = "ui.label.column";
	public static final String UI_LABEL_ROW = "ui.label.row";
	public static final String UI_LABEL = "ui.label";
	public static final String UI_LABEL_TEXT = "ui.label.text";
	public static final String UI_LABEL_SUBFORM = "ui.label.subform";
	public static final String UI_LABEL_ASSETFIELD = "ui.label.assetfield";
	public static final String UI_LABEL_REFERENCEBY = "ui.label.referenceby";
	public static final String UI_LABEL_FIELD = "ui.label.field";
	public static final String UI_LABEL_FORMULA = "ui.label.formula";
	
	//bottom
	public static final String UI_BUTTON_CREATE = "ui.button.create";
	public static final String UI_BUTTON_UPDATE = "ui.button.update";
	public static final String UI_BUTTON_DELETE = "ui.button.delete";
	public static final String UI_BUTTON_GIRL_CREATE = "ui.button.girl.create";
	public static final String UI_BUTTON_GIRL_UNDO = "ui.button.girl.undo";
	public static final String UI_BUTTON_BACK = "ui.button.back";
	public static final String UI_BUTTON_ADDCOLUMN = "ui.button.addcolumn";
	public static final String UI_BUTTON_ADDFORMUlA = "ui.button.addformula";
	
	//ProcessDefinition
	public static final String UI_VIEW_PROCESSDEFINITION = "ui.view.processdefinition";
	public static final String UI_VIEW_PROCESSDEFINITION_TITLE = "ui.view.processdefinition.title";
	public static final String UI_VIEW_PROCESSDEFINITION_TITLE_SMALL = "ui.view.processdefinition.title.small";
	public static final String UI_VIEW_PROCESSDEFINITION_DETAIL = "ui.view.processdefinition.detail";
	public static final String UI_VIEW_PROCESSDEFINITION_DETAIL_TITLE = "ui.view.processdefinition.detail.title";
	public static final String UI_VIEW_PROCESSDEFINITION_DETAIL_TITLE_SMALL = "ui.view.processdefinition.detail.title.small";
	
	//ProcessInstance
	public static final String UI_VIEW_PROCESSINSTANCE = "ui.view.processinstance";
	public static final String UI_VIEW_PROCESSINSTANCE_TITLE = "ui.view.processinstance.title";
	public static final String UI_VIEW_PROCESSINSTANCE_TITLE_SMALL = "ui.view.processinstance.title.small";
	public static final String UI_VIEW_PROCESSINSTANCE_DETAIL = "ui.view.processinstance.detail";
	public static final String UI_VIEW_PROCESSINSTANCE_DETAIL_TITLE = "ui.view.processinstance.detail.title";
	public static final String UI_VIEW_PROCESSINSTANCE_DETAIL_TITLE_SMALL = "ui.view.processinstance.detail.title.small";
	
	//Task
	public static final String UI_VIEW_TASK = "ui.view.task";
	public static final String UI_VIEW_TASK_TITLE = "ui.view.task.title";
	public static final String UI_VIEW_TASK_TITLE_SMALL = "ui.view.task.title.small";
	public static final String UI_VIEW_TASK_DETAIL = "ui.view.task.detail";
	public static final String UI_VIEW_TASK_DETAIL_TITLE = "ui.view.task.detail.title";
	public static final String UI_VIEW_TASK_DETAIL_TITLE_SMALL = "ui.view.task.detail.title.small";
	
	//Modeler
	public static final String UI_VIEW_MODELER = "ui.view.modeler";
	public static final String UI_VIEW_MODELER_ADD = "ui.view.modeler.add";
	public static final String UI_VIEW_MODELER_TITLE = "ui.view.modeler.title";
	public static final String UI_VIEW_MODELER_TITLE_SMALL = "ui.view.modeler.title.small";
	public static final String UI_VIEW_MODELER_DETAIL = "ui.view.modeler.detail";
	public static final String UI_VIEW_MODELER_DETAIL_TITLE = "ui.view.modeler.detail.title";
	public static final String UI_VIEW_MODELER_DETAIL_TITLE_SMALL = "ui.view.modeler.detail.title.small";
	
	//Dashboard
	public static final String UI_VIEW_DASHBOARD = "ui.view.dashboard";
	public static final String UI_VIEW_DASHBOARD_TITLE = "ui.view.dashboard.title";
	public static final String UI_VIEW_DASHBOARD_TITLE_SMALL = "ui.view.dashboard.title.small";
	public static final String UI_VIEW_DASHBOARD_DETAIL = "ui.view.dashboard.detail";
	public static final String UI_VIEW_DASHBOARD_DETAIL_TITLE = "ui.view.dashboard.detail.title";
	public static final String UI_VIEW_DASHBOARD_DETAIL_TITLE_SMALL = "ui.view.dashboard.detail.title.small";
	

	public static final String UI_VIEW_DASHBOARD_TASKOWNER_TITLE = "ui.view.dashboard.taskowner.title";
	public static final String UI_VIEW_DASHBOARD_TASKASSIGN_TITLE = "ui.view.dashboard.taskassign.title";
	public static final String UI_VIEW_DASHBOARD_TASKFINISHED_TITLE = "ui.view.dashboard.taskfinished.title";
	public static final String UI_VIEW_DASHBOARD_TASKWAITING_TITLE = "ui.view.dashboard.taskwaiting.title";
	public static final String UI_VIEW_DASHBOARD_TASKQUEUE_TITLE = "ui.view.dashboard.taskqueue.title";
	public static final String UI_VIEW_DASHBOARD_TASKLIST_TITLE = "ui.view.dashboard.tasklist.title";
	public static final String UI_VIEW_DASHBOARD_TASKLIST_EMPTY = "ui.view.dashboard.tasklist.empty";
	public static final String UI_VIEW_DASHBOARD_PROCESSLIST_TITLE = "ui.view.dashboard.processlist.title";

	//BieuMau
	public static final String UI_VIEW_BIEUMAU = "ui.view.bieumau";
	public static final String UI_VIEW_BIEUMAU_TITLE = "ui.view.bieumau.title";
	public static final String UI_VIEW_BIEUMAU_TITLE_SMALL = "ui.view.bieumau.title.small";
	public static final String UI_VIEW_BIEUMAU_DETAIL = "ui.view.bieumau.detail";
	public static final String UI_VIEW_BIEUMAU_DETAIL_TITLE = "ui.view.bieumau.detail.title";
	public static final String UI_VIEW_BIEUMAU_DETAIL_TITLE_SMALL = "ui.view.bieumau.detail.title.small";
	public static final String UI_VIEW_BIEUMAU_ID = "ui.view.bieumau.id";
	public static final String UI_VIEW_BIEUMAU_MA = "ui.view.bieumau.ma";
	public static final String UI_VIEW_BIEUMAU_TEN = "ui.view.bieumau.ten";
	public static final String UI_VIEW_BIEUMAU_LOAIBIEUMAU = "ui.view.bieumau.loaibieumau";
	public static final String UI_VIEW_BIEUMAU_TRANGTHAI = "ui.view.bieumau.trangthai";
	public static final String UI_VIEW_BIEUMAU_JASPERJRXML = "ui.view.bieumau.jasperjrxml";
	public static final String UI_VIEW_BIEUMAU_JASPER = "ui.view.bieumau.jasper";
	public static final String UI_VIEW_BIEUMAU_SOHANG = "ui.view.bieumau.sohang";
	public static final String UI_VIEW_BIEUMAU_SOCOT = "ui.view.bieumau.socot";
	public static final String UI_VIEW_BIEUMAU_BUTTONS = "ui.view.bieumau.buttons";
	//DanhMuc
	public static final String UI_VIEW_DANHMUC = "ui.view.danhmuc";
	public static final String UI_VIEW_DANHMUC_TITLE = "ui.view.danhmuc.title";
	public static final String UI_VIEW_DANHMUC_TITLE_SMALL = "ui.view.danhmuc.title.small";
	public static final String UI_VIEW_DANHMUC_DETAIL = "ui.view.danhmuc.detail";
	public static final String UI_VIEW_DANHMUC_DETAIL_TITLE = "ui.view.danhmuc.detail.title";
	public static final String UI_VIEW_DANHMUC_DETAIL_TITLE_SMALL = "ui.view.danhmuc.detail.title.small";
	public static final String UI_VIEW_DANHMUC_ID = "ui.view.danhmuc.id";
	public static final String UI_VIEW_DANHMUC_MA = "ui.view.danhmuc.ma";
	public static final String UI_VIEW_DANHMUC_TEN = "ui.view.danhmuc.ten";
	public static final String UI_VIEW_DANHMUC_TRANGTHAI = "ui.view.danhmuc.trangthai";
	public static final String UI_VIEW_DANHMUC_THUTU = "ui.view.danhmuc.thutu";
	public static final String UI_VIEW_DANHMUC_LOAIDANHMUC = "ui.view.danhmuc.loaidanhmuc";
	public static final String UI_VIEW_DANHMUC_DANHMUC = "ui.view.danhmuc.danhmuc";
	//LoaiDanhMuc
	public static final String UI_VIEW_LOAIDANHMUC = "ui.view.loaidanhmuc";
	public static final String UI_VIEW_LOAIDANHMUC_TITLE = "ui.view.loaidanhmuc.title";
	public static final String UI_VIEW_LOAIDANHMUC_TITLE_SMALL = "ui.view.loaidanhmuc.title.small";
	public static final String UI_VIEW_LOAIDANHMUC_DETAIL = "ui.view.loaidanhmuc.detail";
	public static final String UI_VIEW_LOAIDANHMUC_DETAIL_TITLE = "ui.view.loaidanhmuc.detail.title";
	public static final String UI_VIEW_LOAIDANHMUC_DETAIL_TITLE_SMALL = "ui.view.loaidanhmuc.detail.title.small";
	public static final String UI_VIEW_LOAIDANHMUC_ID = "ui.view.loaidanhmuc.id";
	public static final String UI_VIEW_LOAIDANHMUC_MA = "ui.view.loaidanhmuc.ma";
	public static final String UI_VIEW_LOAIDANHMUC_TEN = "ui.view.loaidanhmuc.ten";
	public static final String UI_VIEW_LOAIDANHMUC_TRANGTHAI = "ui.view.loaidanhmuc.trangthai";
	public static final String UI_VIEW_LOAIDANHMUC_LOAIDANHMUC = "ui.view.loaidanhmuc.loaidanhmuc";

	//NhomQuyTrinh
	public static final String UI_VIEW_NHOMQUYTRINH = "ui.view.nhomquytrinh";
	public static final String UI_VIEW_NHOMQUYTRINH_ADD = "ui.view.nhomquytrinh.add";
	public static final String UI_VIEW_NHOMQUYTRINH_TITLE = "ui.view.nhomquytrinh.title";
	public static final String UI_VIEW_NHOMQUYTRINH_TITLE_SMALL = "ui.view.nhomquytrinh.title.small";
	public static final String UI_VIEW_NHOMQUYTRINH_DETAIL = "ui.view.nhomquytrinh.detail";
	public static final String UI_VIEW_NHOMQUYTRINH_DETAIL_TITLE = "ui.view.nhomquytrinh.detail.title";
	public static final String UI_VIEW_NHOMQUYTRINH_DETAIL_TITLE_SMALL = "ui.view.nhomquytrinh.detail.title.small";
	public static final String UI_VIEW_NHOMQUYTRINH_ID = "ui.view.nhomquytrinh.id";
	public static final String UI_VIEW_NHOMQUYTRINH_MA = "ui.view.nhomquytrinh.ma";
	public static final String UI_VIEW_NHOMQUYTRINH_TEN = "ui.view.nhomquytrinh.ten";
	public static final String UI_VIEW_NHOMQUYTRINH_TRANGTHAI = "ui.view.nhomquytrinh.trangthai";
	public static final String UI_VIEW_NHOMQUYTRINH_NHOMQUYTRINH = "ui.view.nhomquytrinh.nhomquytrinh";

}

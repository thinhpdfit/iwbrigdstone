package com.eform.common;

import java.io.ByteArrayOutputStream;

import java.text.SimpleDateFormat;

import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreateAsposeLic {
    public CreateAsposeLic() {
        super();
    }

    public static byte[] createLic(String serialNumberValue) throws ParserConfigurationException,
                                                       TransformerConfigurationException,
                                                       TransformerException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        DocumentBuilderFactory docFactory =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        
        Document doc = docBuilder.newDocument();
        Element element = doc.createElement("License");

        Element data = doc.createElement("Data");
        Element editionType = doc.createElement("EditionType");
        editionType.setTextContent("Enterprise");
        data.appendChild(editionType);
        Element serialNumber = doc.createElement("SerialNumber");
        serialNumber.setTextContent(serialNumberValue);
        data.appendChild(serialNumber);
        Element subscriptionExpiry = doc.createElement("SubscriptionExpiry");
        subscriptionExpiry.setTextContent(sdf.format(calendar.getTime()));
        data.appendChild(subscriptionExpiry);
        Element licenseExpiry = doc.createElement("LicenseExpiry");
        licenseExpiry.setTextContent(sdf.format(calendar.getTime()));
        data.appendChild(licenseExpiry);
        Element products = doc.createElement("Products");
        Element product = doc.createElement("Product");
        product.setTextContent("Aspose.Total for Java");
        products.appendChild(product);
        product = doc.createElement("Product");
        product.setTextContent("Aspose.Total");
        products.appendChild(product);
        product = doc.createElement("Product");
        product.setTextContent("Aspose.Total Product Family");
        products.appendChild(product);
        product = doc.createElement("Product");
        product.setTextContent("Aspose.Words for Java");
        products.appendChild(product);
        product = doc.createElement("Product");
        product.setTextContent("Aspose.Words");
        products.appendChild(product);
        product = doc.createElement("Product");
        product.setTextContent("Aspose.Words Product Family");
        products.appendChild(product);
        data.appendChild(products);
        element.appendChild(data);
        Element signature = doc.createElement("Signature");
        signature.setTextContent(getKey());
        element.appendChild(signature);
        doc.appendChild(element);


        TransformerFactory transformerFactory =
            TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        ByteArrayOutputStream out=new ByteArrayOutputStream();
        StreamResult result =
            new StreamResult(out);
        transformer.transform(source, result);
        return out.toByteArray();
    }

    private static String getKey() {
        return "uo0c1YVqE6Z7Yol9CWKypO695o/ezqqbxzjtpNh071SVWuaSC7jMxeXnTZ2ScJc6r2MA29rFPUVcn5RXTPakkrefVb744QjJXcqEBz+XaaVva8jS08r9gnh3EpqhGvQP6YoytzLVrbUM5CcpEK0Gv4uWqQQmjRFnoWCe/0nMLBI=";
    }
}
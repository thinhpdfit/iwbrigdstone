package com.eform.common.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.IdentityService;

import com.eform.common.SpringApplicationContext;
import com.eform.dao.HtQuyenServices;
import com.eform.model.entities.GroupQuyen;
import com.eform.model.entities.HtQuyen;
import com.eform.service.GroupQuyenServices;

public class AuthenticationUtils {
	public static Map<Long, Boolean> getQuyen(String code,String viewName,  List<String> groupIds, HtQuyenServices quyenService, GroupQuyenServices groupQuyenServices){
		Map<Long, Boolean> quyenMap = new HashMap();
		List<HtQuyen> qList = quyenService.getHtQuyenFind(code,viewName, null, null, null, 0, 1);
		if(qList != null && !qList.isEmpty()){
			HtQuyen q = qList.get(0);
			List<GroupQuyen> groupQuyenList=groupQuyenServices.getGroupQuyenFind(groupIds, q, -1, -1);
			if(groupQuyenList != null && !groupQuyenList.isEmpty()){
				for(GroupQuyen groupQuyen:groupQuyenList){
					if(groupQuyen.getAction() != null){
						int action = groupQuyen.getAction().intValue();
						for(CommonUtils.EAction a : CommonUtils.EAction.values()){
							if((a.getCode()&action) == a.getCode()){
								quyenMap.put(new Long(a.getCode()), true);
							}
							else{
								if(!quyenMap.containsKey(new Long(a.getCode()))){
									quyenMap.put(new Long(a.getCode()), false);
								}
							}
								
						}
					}
				}
			}
		}
		return quyenMap;
	}
	
	public static Map<Long, Boolean> getQuyen(String code, String viewName){
		IdentityService identityService = SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
		HtQuyenServices quyenService = SpringApplicationContext.getApplicationContext().getBean(HtQuyenServices.class);
		GroupQuyenServices groupQuyenServices = SpringApplicationContext.getApplicationContext().getBean(GroupQuyenServices.class);
		List<String> groupIds = CommonUtils.getGroupId(identityService);
		return getQuyen(code, viewName, groupIds, quyenService, groupQuyenServices);
	}
	
	public static Map<Long, Boolean> getQuyen(String code){
		IdentityService identityService = SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
		HtQuyenServices quyenService = SpringApplicationContext.getApplicationContext().getBean(HtQuyenServices.class);
		GroupQuyenServices groupQuyenServices = SpringApplicationContext.getApplicationContext().getBean(GroupQuyenServices.class);
		List<String> groupIds = CommonUtils.getGroupId(identityService);
		return getQuyen(code, null, groupIds, quyenService, groupQuyenServices);
	}
	
	public static boolean hasPermission(String code,String viewName,  List<String> groupIds, HtQuyenServices quyenService, GroupQuyenServices groupQuyenServices){
		List<HtQuyen> qList = quyenService.getHtQuyenFind(code,viewName, null, null, null, 0, 1);
		if(qList != null && !qList.isEmpty()){
			HtQuyen q = qList.get(0);
			List<GroupQuyen> groupQuyenList=groupQuyenServices.getGroupQuyenFind(groupIds, q, -1, -1);
			if(groupQuyenList != null && !groupQuyenList.isEmpty()){
				return true;
			}
		}
		return false;
	}
	
	public static boolean hasPermission(String code){
		IdentityService identityService = SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
		HtQuyenServices htQuyenService = SpringApplicationContext.getApplicationContext().getBean(HtQuyenServices.class);
		GroupQuyenServices groupQuyenServices = SpringApplicationContext.getApplicationContext().getBean(GroupQuyenServices.class);
		List<String> groupIds = CommonUtils.getGroupId(identityService);
		return hasPermission(code, null, groupIds, htQuyenService, groupQuyenServices);
	}
}

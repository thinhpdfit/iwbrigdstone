package com.eform.common.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;

import com.eform.common.Constants;
import com.eform.common.SpringApplicationContext;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BieuMauService;
import com.eform.service.BmHangService;
import com.eform.service.BmOService;
import com.eform.service.BmOTruongThongTinService;
import com.eform.service.HtDonViServices;

public class ChartUtils {
	public static String getGChartData(List<String> keys, List<Map<String, Object>> results, Map<String, String> aliasName){
		if(results != null && !results.isEmpty() && keys != null && !keys.isEmpty()){
			JSONArray jsonArr = new JSONArray();
			JSONArray jsonHeaderArr = new JSONArray();
			for(String k : keys){
				jsonHeaderArr.put(aliasName.get(k));
			}
			jsonArr.put(jsonHeaderArr);
			for(Map<String, Object> row : results){
				JSONArray jsonDataArr = new JSONArray();
				for(String k : keys){
					Object value = row.get(k);
					jsonDataArr.put(value);
				}
				jsonArr.put(jsonDataArr);
			}
			return jsonArr.toString();
		}
		return "";
	}
	
	public static Workbook exportExcel(List<Map<String, Object>> results, List<String> keyList, Map<String, String> aliasName, String tieuDe){
		try {
			
			if(results != null && !results.isEmpty() && keyList != null && aliasName != null){
				Workbook workbook = new XSSFWorkbook();
				Sheet sheet = workbook.createSheet("Báo cáo thống kê");
				int ri = 0; 
				//Title row
				Row titleRow = sheet.createRow(ri);
				Cell titleCell = titleRow.createCell(0);
				titleCell.setCellValue(tieuDe);
				ri++;
				//Header row
				Row headerRow = sheet.createRow(ri);
				CellStyle cellHeaderStyle = workbook.createCellStyle();
				cellHeaderStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				cellHeaderStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
				cellHeaderStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
				cellHeaderStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				cellHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				ri++;
				int ci = 0;
				for(String key : keyList){
					Cell cell = headerRow.createCell(ci);
					cell.setCellValue(aliasName.get(key));
					cell.setCellStyle(cellHeaderStyle);
					ci++;
				}
				//Rows
				CellStyle cellStyle = workbook.createCellStyle();
				cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
				cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
				cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				for(Map<String, Object> r : results){
					if(r != null && !r.isEmpty()){
						ci = 0;
						Row row = sheet.createRow(ri);
						for(String key : keyList){
							if(r.get(key) != null){
								Cell cell = row.createCell(ci);
								cell.setCellStyle(cellStyle);
								if(r.get(key) instanceof BigDecimal){
									BigDecimal bd = (BigDecimal) r.get(key);
									cell.setCellValue(bd.longValueExact());
								}else if(r.get(key) instanceof java.util.Date){
									java.util.Date dataValue = (Date) r.get(key);
									cell.setCellValue(dataValue);
								}else{
									cell.setCellValue(r.get(key).toString());
								}
							}
							ci++;
						}
						ri++;
					}
				}
				for(int colNum = 0; colNum < headerRow.getLastCellNum(); colNum++){   
					workbook.getSheetAt(0).autoSizeColumn(colNum);
				}
				sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, headerRow.getLastCellNum()-1));
				return workbook;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//Lay tat ca truong thong tin cua bieu mau, bao gom ca bieu mau con
	public static Map<String, List<TruongThongTin>> getTruongThongTin(BieuMau bieuMau, BieuMauService bieuMauService){
    	Map<String, List<TruongThongTin>> mapTruongTT = new HashMap();
    	
    	Map<BieuMau, String> paths = new HashMap<BieuMau, String>();
    	if(bieuMau != null){
    		List<BieuMau> process = new ArrayList<BieuMau>();
    		process.add(bieuMau);
    		while(!process.isEmpty()){
    			BieuMau bm = process.remove(0);
    			List<BmO> bmOList = bieuMauService.getBmOFind(null, null, null, bm, null, null, -1, -1);
    			for(BmO o : bmOList){
    				if(o.getLoaiO() != null && o.getLoaiO().intValue() == CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()){
    					String path = paths.get(bm);
    					if(path == null){
    						path = "!";
    					}
    					List<BmOTruongThongTin> oTruongThongTinList = bieuMauService.getBmOTruongThongTinFind(null, null, o, null, -1, -1);
    					List<TruongThongTin> truongTTList = mapTruongTT.get(path);
    					if(truongTTList == null){
    						truongTTList = new ArrayList<TruongThongTin>();
    					}
    					for (BmOTruongThongTin oTruongThongTin : oTruongThongTinList) {
    						truongTTList.add(oTruongThongTin.getTruongThongTin());
    					}
    					mapTruongTT.put(path, truongTTList);
    				}else if(o.getLoaiO() != null && o.getLoaiO().intValue() == CommonUtils.ELoaiO.BIEU_MAU.getCode() && o.getBieuMauCon() != null){
    					String path = "";
    					if(paths.get(bm) != null){
    						path = paths.get(bm) + ",";
    					}
//    					path += o.getMa();
    					path += o.getBieuMauCon().getMa();
    					paths.put(o.getBieuMauCon(), path);
    					process.add(o.getBieuMauCon());
    				}
    			}
    		}
    		
    	}
    	return mapTruongTT;
    }
	
	public static List<TruongThongTin> getTruongThongTinByBieuMau(List<String> maBieuMauList, BieuMauService bieuMauService){
		List<TruongThongTin> truongThongTinList = new ArrayList();
		if(maBieuMauList != null){
			for(String maBieuMau : maBieuMauList){
				List<BieuMau> bieuMauList = bieuMauService.findByCode(maBieuMau);
	    		if(bieuMauList != null){
	    			BieuMau bm = bieuMauList.get(0);
	    			List<BmO> bmOList = bieuMauService.getBmOFind(null, null, null, bm, null, null, -1, -1);
        			for(BmO o : bmOList){
        				if(o.getLoaiO() != null && o.getLoaiO().intValue() == CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()){
        					List<BmOTruongThongTin> oTruongThongTinList = bieuMauService.getBmOTruongThongTinFind(null, null, o, null, -1, -1);
        					for (BmOTruongThongTin oTruongThongTin : oTruongThongTinList) {
        						truongThongTinList.add(oTruongThongTin.getTruongThongTin());
        					}
        				}
        			}
	    		}
			}
		}
		return truongThongTinList;
	}
	
	public static List<TruongThongTin> getTruongThongTinAll(BieuMau bieuMau, BieuMauService bieuMauService){
    	List<TruongThongTin> truongThongTinList = new ArrayList();
    	if(bieuMau != null){
    		List<BieuMau> process = new ArrayList<BieuMau>();
    		process.add(bieuMau);
    		while(!process.isEmpty()){
    			BieuMau bm = process.remove(0);
    			List<BmO> bmOList = bieuMauService.getBmOFind(null, null, null, bm, null, null, -1, -1);
    			for(BmO o : bmOList){
    				if(o.getLoaiO() != null && o.getLoaiO().intValue() == CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()){
    					List<BmOTruongThongTin> oTruongThongTinList = bieuMauService.getBmOTruongThongTinFind(null, null, o, null, -1, -1);
    					for (BmOTruongThongTin oTruongThongTin : oTruongThongTinList) {
    						truongThongTinList.add(oTruongThongTin.getTruongThongTin());
    					}
    				}else if(o.getLoaiO() != null && o.getLoaiO().intValue() == CommonUtils.ELoaiO.BIEU_MAU.getCode() && o.getBieuMauCon() != null){
    					process.add(o.getBieuMauCon());
    				}
    			}
    		}
    	}
    	return truongThongTinList;
    }
	
	public static List<TruongThongTin> getTruongThongTinAll(String maBieuMau, BieuMauService bieuMauService){
    	List<TruongThongTin> truongThongTinList = new ArrayList();
    	if(maBieuMau != null){
    		List<BieuMau> bieuMauList = bieuMauService.findByCode(maBieuMau);
    		if(bieuMauList != null){
    			List<BieuMau> process = new ArrayList<BieuMau>();
        		process.add(bieuMauList.get(0));
        		while(!process.isEmpty()){
        			BieuMau bm = process.remove(0);
        			List<BmO> bmOList = bieuMauService.getBmOFind(null, null, null, bm, null, null, -1, -1);
        			for(BmO o : bmOList){
        				if(o.getLoaiO() != null && o.getLoaiO().intValue() == CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()){
        					List<BmOTruongThongTin> oTruongThongTinList = bieuMauService.getBmOTruongThongTinFind(null, null, o, null, -1, -1);
        					for (BmOTruongThongTin oTruongThongTin : oTruongThongTinList) {
        						truongThongTinList.add(oTruongThongTin.getTruongThongTin());
        					}
        				}else if(o.getLoaiO() != null && o.getLoaiO().intValue() == CommonUtils.ELoaiO.BIEU_MAU.getCode() && o.getBieuMauCon() != null){
        					process.add(o.getBieuMauCon());
        				}
        			}
        		}
    		}
    		
    	}
    	return truongThongTinList;
    }
	
	public static List<BieuMau> getBieuMauCon(BieuMau bieuMau, BieuMauService bieuMauService){
    	List<BieuMau> bieuMauList = new ArrayList();
    	if(bieuMau != null){
			List<BmO> bmOList = bieuMauService.getBmOFind(null, null, null, bieuMau, null, null, -1, -1);
			for(BmO o : bmOList){
				if(o.getLoaiO() != null && o.getLoaiO().intValue() == CommonUtils.ELoaiO.BIEU_MAU.getCode() && o.getBieuMauCon() != null){
					bieuMauList.add(o.getBieuMauCon());
				}
			}
    	}
    	return bieuMauList;
    }
	
	public static String getReportHtml(List<Map<String, Object>> results, List<String> keyList,
			Map<String, String> aliasName, Map<String, TruongThongTin> truongThongTinKeyMap,
			Map<String, String> keyMap){
		String html = "";
		try {
			
			
			BieuMauService bieuMauService = SpringApplicationContext.getApplicationContext().getBean(BieuMauService.class);
			BmHangService bmHangService = SpringApplicationContext.getApplicationContext().getBean(BmHangService.class);
			BmOService bmOService = SpringApplicationContext.getApplicationContext().getBean(BmOService.class);
			BmOTruongThongTinService bmOTruongThongTinService = SpringApplicationContext.getApplicationContext().getBean(BmOTruongThongTinService.class);
			IdentityService identityService = SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
			HtDonViServices htDonViServices = SpringApplicationContext.getApplicationContext().getBean(HtDonViServices.class);
			
			if(results != null && !results.isEmpty() && keyList != null && aliasName != null){
				html += "<table style='width: 100%; margin: auto; border-collapse: collapse; table-layout: fixed;'>";
				String htmlTmp = "";
				boolean isHeader = true;
				//Header
				htmlTmp += "<tr>";
				for(String key : keyList){
					html += "<th style='border: 1px solid #f1f1f1; padding: 5px; text-align: center;'>";
					html += aliasName.get(key);
					html += "</th>";
				}
				html += "</tr>";
				//End header
				
				//Table content
				for(Map<String, Object> row : results){
					htmlTmp += "<tr>";
					if(row != null && !row.isEmpty()){
						for(String key : keyList){
							
							TruongThongTin truongTT = truongThongTinKeyMap.get(keyMap.get(key));
							
							String value ="";
							DecimalFormat decf = new DecimalFormat("#,###,###,###.###");
						
							
							if(truongTT != null){
								if(truongTT.getKieuDuLieu() != null){
									int kieuDuLieu = truongTT.getKieuDuLieu().intValue();
									
									Object objecValue = row.get(key);
									
									if (CommonUtils.EKieuDuLieu.TEXT.getMa() == kieuDuLieu) {
										if(objecValue != null){
											value = (String)objecValue;
										}
									} else if (CommonUtils.EKieuDuLieu.EMAIL.getMa() == kieuDuLieu) {
										if(objecValue != null){
											value = (String)objecValue;
										}
									} else if (CommonUtils.EKieuDuLieu.TEXT_AREA.getMa() == kieuDuLieu) {
										if(objecValue != null){
											value= (String)objecValue;
										}
									} else if (CommonUtils.EKieuDuLieu.NUMBER.getMa() == kieuDuLieu) {
										if(objecValue != null){
											Double dValue = null;
											if(objecValue instanceof String){
												dValue = new Double((String) objecValue);
											}else if(objecValue instanceof Double){
												dValue = (Double) objecValue;
											}
											if(dValue != null){
												value= decf.format(dValue);
											}
										}
									} else if (CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa() == kieuDuLieu) {
										if(objecValue != null){
											Double dValue = null;
											if(objecValue instanceof String){
												dValue = new Double((String) objecValue);
											}else if(objecValue instanceof Double){
												dValue = (Double) objecValue;
											}
											if(dValue != null){
												value= decf.format(dValue);
											}
										}
									} else if (CommonUtils.EKieuDuLieu.DATE.getMa() == kieuDuLieu) {
										if(objecValue instanceof Date){
											Date d = (Date) objecValue;
											SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
											value= df.format(d);
										}
									} else if (CommonUtils.EKieuDuLieu.CHECKBOX.getMa() == kieuDuLieu) {
										if(objecValue instanceof Boolean){
											Boolean v = (Boolean)objecValue;
											if(v != null && v){
												value= "&#9745;";
											}else{
												value= "&#9744;";
											}
										}
									} else if (CommonUtils.EKieuDuLieu.DANH_MUC.getMa() == kieuDuLieu) {
										if(objecValue instanceof String){
											String v = (String)objecValue;
											List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(v, null, null, null, null, 0, 1);
											if(danhMucList != null && !danhMucList.isEmpty()){
												DanhMuc dm = danhMucList.get(0);
												value= dm.getTen();
											}
										}
									} else if (CommonUtils.EKieuDuLieu.RADIO_BUTTON.getMa() == kieuDuLieu) {
										if(objecValue instanceof String){
											String v = (String)objecValue;
											List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(v, null, null, null, null, 0, 1);
											if(danhMucList != null && !danhMucList.isEmpty()){
												DanhMuc dm = danhMucList.get(0);
												value= dm.getTen();
											}
										}
									} else if (CommonUtils.EKieuDuLieu.LUA_CHON_NHIEU.getMa() == kieuDuLieu) {
										if(objecValue instanceof String){
										String valueTmp = (String) objecValue;
										if (valueTmp != null && !valueTmp.isEmpty() && valueTmp.trim().length() > 2) {
											valueTmp = valueTmp.trim().substring(1, valueTmp.length() - 1);
											if (!valueTmp.isEmpty()) {
												String[] arr = valueTmp.split(",");
												if (arr != null && arr.length > 0) {
													LinkedHashSet<String> hs = new LinkedHashSet<String>();
													for (String v : arr) {
														if(v != null){
															v = v.trim();
															List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(v, null, null, null, null, 0, 1);
															if(danhMucList != null && !danhMucList.isEmpty()){
																DanhMuc dm = danhMucList.get(0);
																value+= dm.getTen()+"; ";
															}
														}
														
													}
												}
											}
										}
										}
									} else if (CommonUtils.EKieuDuLieu.USER.getMa() == kieuDuLieu) {
										if(objecValue instanceof String){
											String userId = (String) objecValue;
											User u = identityService.createUserQuery().userId(userId).singleResult();
											if(u!= null){
												value= u.getFirstName()+" "+u.getLastName();
											}
											
										}
									} else if (CommonUtils.EKieuDuLieu.GROUP.getMa() == kieuDuLieu) {
										if(objecValue instanceof String){
											String groupId = (String) objecValue;
											Group g = identityService.createGroupQuery().groupId(groupId).singleResult();
											if(g!= null){
												value= g.getName();
											}
											
										}
									} else if (CommonUtils.EKieuDuLieu.XAC_THUC_FILE.getMa() == kieuDuLieu) {
										if(objecValue instanceof String){
											//value= (String) objecValue;
										}
									} else if (CommonUtils.EKieuDuLieu.FILE.getMa() == kieuDuLieu) {
										if(objecValue instanceof String){
											//value= (String) objecValue;
										}
									} else if (CommonUtils.EKieuDuLieu.EDITOR.getMa() == kieuDuLieu) {
										if(objecValue instanceof String){
											value= (String) objecValue;
										}
									} else if (CommonUtils.EKieuDuLieu.DON_VI.getMa() == kieuDuLieu) {
										if(objecValue instanceof String){
											String str = (String) objecValue;
											List<HtDonVi> htDonViList = htDonViServices.getHtDonViFind(str, null,null, null, 0, 1);
											if(htDonViList!= null && !htDonViList.isEmpty()){
												HtDonVi dv = htDonViList.get(0);
												value= dv.getTen();
											}
											
										}
									}else if (CommonUtils.EKieuDuLieu.COMPLETED_USER.getMa() == kieuDuLieu) {
										if(objecValue instanceof String){
											String userId = (String) objecValue;
											User u = identityService.createUserQuery().userId(userId).singleResult();
											if(u!= null){
												value= u.getFirstName()+" "+u.getLastName();
											}
										}
									} 
								}
							}else{
								if(row.get(key) != null){
									if(row.get(key) instanceof BigDecimal){
										//Format BigDecimal
										BigDecimal bd = (BigDecimal) row.get(key);
										bd = bd.setScale(2, BigDecimal.ROUND_DOWN);
										DecimalFormat df = new DecimalFormat();
										df.setMaximumFractionDigits(2);
										df.setMinimumFractionDigits(0);
										df.setGroupingUsed(false);
										String result = df.format(bd);
										
										value = result;
									}else if(row.get(key) instanceof java.util.Date || row.get(key) instanceof java.sql.Date){
										SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
										value = df.format(row.get(key));
									}else{
										value = row.get(key).toString();
									}
								}
							}
							
							htmlTmp += "<td style='border: 1px solid #f1f1f1; padding: 5px;'>";
							htmlTmp += value;
							htmlTmp += "</td>";
						}
					}
					htmlTmp += "</tr>";
				}
				//end table content
				
				html += htmlTmp + "</table>";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return html;
	}
}

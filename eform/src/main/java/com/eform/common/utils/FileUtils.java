package com.eform.common.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Element;

import com.aspose.words.Document;
import com.aspose.words.ImportFormatMode;
import com.eform.common.Constants;
import com.eform.common.CreateAsposeLic;
import com.eform.sharepoint.service.SharepointServices;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Button;


public class FileUtils {
	public static void createIfNotExistFolder(String path){
		try {
			File file = new File(path);
	        if (!file.exists()) {
	        	file.mkdir();
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isExist(String path){
		try {
			File file = new File(path);
	        if (file.exists()) {
	        	return true;
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	public static void downloadWithButton(Button btn, String filePath){
		try {
			Resource res = new FileResource(new File(filePath));
			FileDownloader fileDownloader = new FileDownloader(res);
			fileDownloader.extend(btn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void downloadWithButton(Button btn, InputStream input, String fileName){
		try {
			Resource res = getStreamResource(input, fileName);
			FileDownloader fileDownloader = new FileDownloader(res);
			fileDownloader.extend(btn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static StreamResource getStreamResource(InputStream input, String fileName) {
        StreamResource.StreamSource source = new StreamResource.StreamSource() {
			private static final long serialVersionUID = 1L;
			public InputStream getStream() {
                return input;
            }
        };
      StreamResource resource = new StreamResource (source, fileName);
      return resource;
}
	
	public static boolean removeFile(String filePath){
		try {
			File file = new File(filePath);
			return file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static String getFileExtension(String fileName){
		try {
			if(fileName != null && fileName.lastIndexOf(".") > 1){
				return fileName.substring(fileName.lastIndexOf(".")+1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void createPdf(String wordFilePath, String outputPdfPath) throws TransformerConfigurationException, ParserConfigurationException, TransformerException, Exception{
		new com.aspose.words.License().setLicense(new ByteArrayInputStream(CreateAsposeLic.createLic("00000000-0000-0000-0000-00000000000")));
		Document doc = new Document(wordFilePath);   
		Document newDoc = new Document();
		doc.appendDocument(newDoc, ImportFormatMode.USE_DESTINATION_STYLES);
		doc.save(outputPdfPath, com.aspose.words.SaveFormat.PDF);
	}
	
	
	public static void uploadToDocumentList(String filePath, String fileName, String title, String name){
		try {
			String username= Constants.SPSV_USERNAME;
			String password= Constants.SPSV_PASSWORD;
			String folder = Constants.SPSV_FOLDER;
			String urlRoot = Constants.SPSV_URL;

			SharepointServices sharepointServices = new SharepointServices("http","intranet.mine.vn",80,"/_vti_bin",username,password);
			sharepointServices.createFolder(folder);
			
			List<Element> fields = new ArrayList<Element>();
			fields.add(sharepointServices.createFieldInformation("Title", "Text", fileName));
			
			File file = new File(filePath);
			List<String> destinationUrls = new ArrayList<String>();
			String url = urlRoot+folder+"/"+fileName;
			destinationUrls.add(url);
			sharepointServices.copyIntoItems(file.getName(), destinationUrls, fields, file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static List<String> getTypeAlow(String typeAlowStr){
		List<String> typeAlow = new ArrayList<String>();
		if(typeAlowStr != null && !typeAlowStr.trim().isEmpty()){
			String [] arr = typeAlowStr.split(";");
			if(arr != null && arr.length >0){
				for(int i = 0; i < arr.length; i++){
					if(arr[i] != null){
						typeAlow.add(arr[i].trim());
					}
				}
			}
		}
		return typeAlow; 
	}
}

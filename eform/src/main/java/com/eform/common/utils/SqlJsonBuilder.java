package com.eform.common.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;

import com.eform.common.Constants;
import com.eform.model.entities.TruongThongTin;
import com.eform.ui.view.report.type.FilterObject;

public class SqlJsonBuilder {
	
	
	public static Map<String, Object> getReportSqlQuery(List<String> groupByPath, Map<String, TruongThongTin> truongThongTinKeyMap, 
			List<String> functionPath, 
			List<String> processInsIdList, 
			List<FilterObject> filterList, 
			Date fromDate, 
			Date toDate){
		List<Object> params = new ArrayList<Object>();
		List<Integer> types = new ArrayList<Integer>();
		String sql = "";
		if(groupByPath != null && !groupByPath.isEmpty()){
			String groupBy = "";
			sql += "SELECT";
			for(String str : groupByPath){
				String catsExp = "";
				if(truongThongTinKeyMap != null){
					TruongThongTin truongTT = truongThongTinKeyMap.get(str);
					if(truongTT != null && truongTT.getKieuDuLieu() != null && truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
						catsExp = "::timestamptz";
					}
				}
				String alias = str.replace(" ", "").replace(",", "_");
				groupBy += alias + ",";
				sql += "(ho_so_json::json#>>'{"+str+"}')"+catsExp+" as " + alias +",";
			}
			groupBy = groupBy.substring(0, groupBy.length()-1);//Bỏ dấu , cuối cùng
			if(functionPath != null && !functionPath.isEmpty()){
				for(String str : functionPath){
					Pattern p = Pattern.compile("([a-zA-Z0-9_-]+)#([0-9]+)");
					Matcher m = p.matcher(str);
					if(m.matches()){
						Integer fn = new Integer(m.group(2));
						String path = m.group(1);
						String alias = path.replace(" ", "").replace(",", "_");
						if(fn.intValue() == CommonUtils.ESummaryFunction.AVG.getCode()){
							sql += "AVG((ho_so_json::json#>>'{"+ path +"}')::decimal) as " + alias + "_AVG" +",";
						}else if(fn.intValue() == CommonUtils.ESummaryFunction.SUM.getCode()){
							sql += "SUM((ho_so_json::json#>>'{"+ path +"}')::decimal) as " + alias + "_SUM" +",";
						}else if(fn.intValue() == CommonUtils.ESummaryFunction.MAX.getCode()){
							sql += "MAX((ho_so_json::json#>>'{"+ path +"}')::decimal) as " + alias + "_MAX" +",";
						}else if(fn.intValue() == CommonUtils.ESummaryFunction.MIN.getCode()){
							sql += "MIN((ho_so_json::json#>>'{"+ path +"}')::decimal) as " + alias + "_MIN" +",";
						}else if(fn.intValue() == CommonUtils.ESummaryFunction.COUNT.getCode()){
							sql += "COUNT((ho_so_json::json#>>'{"+ path +"}')) as " + alias + "_COUNT" +",";
						}else if(fn.intValue() == CommonUtils.ESummaryFunction.COUNT_DISTINCT.getCode()){
							sql += "COUNT(DISTINCT (ho_so_json::json#>>'{"+ path +"}')) as " + alias + "_COUNT_DISTINCT" +",";
						}
					}
				}
			}
			sql = sql.substring(0, sql.length()-1);//Bỏ dấu , cuối cùng
			sql += " FROM ho_so ";
			boolean hasWhereClause = false;
			if(processInsIdList != null && !processInsIdList.isEmpty()){
				String inParam = "(";
				for(String str : processInsIdList){
					inParam += "'"+str + "',";
				}
				inParam = inParam.substring(0, inParam.length()-1);//Bỏ dấu , cuối cùng
				inParam += ")";
				if(!hasWhereClause){
					sql += " WHERE ";
					hasWhereClause = true;
				}else{
					sql += " AND ";
				}
				sql += " process_instance_id in " + inParam;
			}
			if(fromDate != null){
				if(!hasWhereClause){
					sql += " WHERE ";
					hasWhereClause = true;
				}else{
					sql += " AND ";
				}
				sql += " ngay_tao >= ? ";
				params.add(new java.sql.Date(fromDate.getTime()));
				types.add(java.sql.Types.DATE);
			}
			if(toDate != null){
				if(!hasWhereClause){
					sql += " WHERE ";
					hasWhereClause = true;
				}else{
					sql += " AND ";
				}
				sql += " ngay_tao <= ? ";
				params.add(new java.sql.Date(toDate.getTime()));
				types.add(java.sql.Types.DATE);
			}
			
			if(filterList != null && !filterList.isEmpty()){
				for(FilterObject filter : filterList){
					if(!hasWhereClause){
						sql += " WHERE ";
						hasWhereClause = true;
					}else{
						sql += " AND ";
					}
					CommonUtils.EComparison comparison = CommonUtils.EComparison.getComparison(filter.getComparison());
					Long kieuDL = filter.getKieuDuLieu();
					Object value = filter.getValue();
					String castExp = "";
					if(kieuDL != null){
						if(kieuDL.intValue() == CommonUtils.EKieuDuLieu.NUMBER.getMa() || kieuDL.intValue() == CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa()){
							castExp = "::decimal";
						}else if(kieuDL.intValue() == CommonUtils.EKieuDuLieu.DATE.getMa() || kieuDL.intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
							castExp = "::timestamptz";
							SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
							if(value != null){
								try {
									String dateStr = (String)value;
									value = df.parse(dateStr);
								} catch (Exception e) {
									// TODO: handle exception
								}
							}
						}
					}
					sql += " (ho_so_json::json#>>'{"+filter.getPath()+"}')"+castExp+" "+comparison.getSymbol()+" ? ";
					if(comparison.getCode() == CommonUtils.EComparison.LIKE.getCode()){
						params.add("%"+value+"%");
					}else{
						params.add(value);
					}
					
					types.add(FormUtils.getSQLType(kieuDL));
				}
			}
			sql += " GROUP BY " + groupBy;
			sql += " ORDER BY " + groupBy;
		}
		Map<String, Object> returnValueMap = new HashMap<String, Object>();
		returnValueMap.put("SQL", sql);
		if(params != null && !params.isEmpty()){
			Object[] objParams = new Object[params.size()];
			int i = 0;
			for(Object obj : params){
				objParams[i] = obj;
				i++;
			}
			returnValueMap.put("PARAMS", objParams);
		}
		if(types != null && !types.isEmpty()){
			int[] typeArr = new int[types.size()];
			int i = 0;
			for(int type : types){
				typeArr[i] = type;
				i++;
			}
			returnValueMap.put("TYPES", typeArr);
		}
		return returnValueMap;
	}
	
	public static Map<String, Object> getHoSoJsonSqlQuery(Map<String, Integer> jsonKeyMap, Map<String, Object> paramMap,List<HistoricProcessInstance> proInsList, int limit, int offset){
		String sql = "";
		List<Object> paramList = new ArrayList();
		List<Integer> paramTypeList = new ArrayList();
		
		if(jsonKeyMap != null && !jsonKeyMap.isEmpty()){
			for(Map.Entry<String, Integer> entry : jsonKeyMap.entrySet()){
				String key = entry.getKey();
				Integer value = entry.getValue();
				sql += "(ho_so_json->>'"
						+ key
						+ "')";
				
				if(value != null && value.intValue() == java.sql.Types.DATE){
					sql += "::timestamp";
				}else if(value != null && value.intValue() == java.sql.Types.FLOAT){
					sql += "::numeric";
				}else if(value != null && value.intValue() == java.sql.Types.INTEGER){
					sql += "::numeric";
				}
				
				sql += " AS "+key+",";
			}
		}
		sql+=" id, task_id, process_instance_id, trang_thai ";
		
		String wherePart = "";
		
		if(paramMap != null && !paramMap.isEmpty()){
			for(Map.Entry<String, Object> entry : paramMap.entrySet()){
				String key = entry.getKey();
				Object value = entry.getValue();
				
				if(value instanceof String){
					if (wherePart.isEmpty()) {
		                wherePart = "WHERE ";
		            } else {
		                wherePart += "AND ";
		            }
					
					wherePart += key+" like ? ";
					paramList.add("%"+value.toString().trim()+"%");
					paramTypeList.add(java.sql.Types.VARCHAR);
				}else if(value instanceof Long){
					if (wherePart.isEmpty()) {
		                wherePart = "WHERE ";
		            } else {
		                wherePart += "AND ";
		            }
					
					wherePart += key+" = ? ";
					paramList.add(value);
					paramTypeList.add(java.sql.Types.NUMERIC);
				}else if(value instanceof Map){
					Map<String, Object> valueMap = (Map<String, Object>) value;
					
					Object from = valueMap.get("FROM");
					if(from != null){
						if (wherePart.isEmpty()) {
			                wherePart = "WHERE ";
			            } else {
			                wherePart += "AND ";
			            }
						
						wherePart += key+" >= ? ";
						paramList.add(from);
						if(from instanceof Date){
							paramTypeList.add(java.sql.Types.DATE);
						}else if(from instanceof Long){
							paramTypeList.add(java.sql.Types.NUMERIC);
						}else{
							paramTypeList.add(java.sql.Types.VARCHAR);
						}
						
					}
					Object to = valueMap.get("TO");
					if(to != null){
						if (wherePart.isEmpty()) {
			                wherePart = "WHERE ";
			            } else {
			                wherePart += "AND ";
			            }
						
						wherePart += key+" <= ? ";
						paramList.add(to);
						if(to instanceof Date){
							paramTypeList.add(java.sql.Types.DATE);
						}else if(to instanceof Long){
							paramTypeList.add(java.sql.Types.NUMERIC);
						}else{
							paramTypeList.add(java.sql.Types.VARCHAR);
						}
					}
				}
			}
		}
		
		if(proInsList != null && !proInsList.isEmpty()){
			if (wherePart.isEmpty()) {
                wherePart = "WHERE ";
            } else {
                wherePart += "AND ";
            }
			List<String> proInsIds = new ArrayList();
			String inStr = "(";
			for(HistoricProcessInstance proIns : proInsList){
				inStr+= "'"+proIns.getId() +"',";
			}
			inStr = inStr.substring(0, inStr.length()-1)+") ";
			wherePart += " process_instance_id in "+inStr; 
		}
		
		sql = "SELECT * FROM (SELECT "
			+ sql
			+ " FROM ho_so order by ngay_cap_nhat DESC, ngay_tao DESC, id DESC) json_data "
			+ wherePart;
		
		if(limit > 0){
			sql += " limit "+limit;
		}
		if(offset > 0){
			sql += " offset "+offset;
		}
		
		Map<String, Object> sqlQuery = new HashMap();
		sqlQuery.put("SQL", sql);
		sqlQuery.put("PARAM", paramList.toArray());
		if(paramTypeList != null){
			int[] typeArr = new int[paramTypeList.size()];
			int i = 0;
			for(int type : paramTypeList){
				typeArr[i] = type;
				i++;
			}
			sqlQuery.put("TYPE", typeArr);
		}
		return sqlQuery;
	}
			
	
}

package com.eform.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.springframework.security.core.context.SecurityContextHolder;

import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.type.EFormTruongThongTin;
import com.eform.model.entities.HtQuyen;
import com.eform.security.custom.ActivitiUserDetails;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

public class CommonUtils {
    public static enum EKieuDuLieu {
        TEXT(0, "Ký tự", false),
        NUMBER(1, "Số thực", false),
        SO_NGUYEN(2, "Số nguyên", false),
        DATE(3, "Thời gian", false),
        DANH_MUC(4, "Danh mục", true),
        FILE(5, "File", false),
        CHECKBOX(6, "Ô lựa chọn", false),
        RADIO_BUTTON(7, "Radio", true),
        EMAIL(8, "Email", false),
        TEXT_AREA(9, "Đoạn văn bản", false),
        LUA_CHON_NHIEU(10, "Lựa chọn nhiều", true),
        XAC_THUC_FILE(11, "Xác thực file", false),
        EDITOR(12, "Editor", false),
        USER(13, "Người dùng", false),
        GROUP(14, "Nhóm người dùng", false),
        HELP_TEXT(15, "Thông tin trợ giúp", false),
        CHUC_VU(16, "Chức vụ", false),
        DON_VI(17, "Đơn vị", false),
        COMPLETED_USER(18, "Người dùng thực hiện", false),
        DON_VI_USER(19, "Đơn vị người dùng thực hiện", false),
    	SERVICE(20, "Service", false);
    	
        private final int ma;
        private final String ten;
        private final boolean coDanhMuc;

        private EKieuDuLieu(int ma, String ten, boolean coDanhMuc) {
            this.ma = ma;
            this.ten = ten;
            this.coDanhMuc = coDanhMuc;
        }

		public int getMa() {
			return ma;
		}

		public String getTen() {
			return ten;
		}

		public boolean isCoDanhMuc() {
			return coDanhMuc;
		}
		
		public static boolean isCoDanhMuc(int ma){
            for (EKieuDuLieu kieuDL : values()) {
                if (kieuDL.ma == ma) {
                    return kieuDL.isCoDanhMuc();
                }
            }
            return false;
		}
		
		public static String getTen(int ma){
            for (EKieuDuLieu kieuDL : values()) {
                if (kieuDL.ma == ma) {
                    return kieuDL.getTen();
                }
            }
            return "";
		}
       
    }

    public static enum ELoaiHienThi {
        INPUT(0, "Ô nhập"),
        INPUT_WITH_TEXT(1, "Ô nhập và nhãn");
        private final int code;
        private final String name;

        private ELoaiHienThi(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum ETrangThaiHoSo {
    	NEW(1, "Mới tạo"),
    	IN_PROCESS(2, "Đang xử lý"),
        ENDED(3, "Đã hoàn thành");
        private final int code;
        private final String name;

        private ETrangThaiHoSo(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
        
        public static ETrangThaiHoSo getTrangThai(Long trangThai){
        	if(trangThai != null){
        		for(ETrangThaiHoSo e : ETrangThaiHoSo.values()){
        			if(e.getCode() == trangThai.intValue()){
        				return e;
        			}
        		}
        	}
        	return ETrangThaiHoSo.NEW;
        }
    }
    
    
    public static enum EViTriHienThi {
        TABLE(1, "Table"),
        SEARCH_FORM(2, "Search Form");
        private final int code;
        private final String name;

        private EViTriHienThi(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum ENotificationStatus {
        NEW(0, "Mới"),
        READED(1, "Đã xem"),
        CLICKED(2, "Đã click");
        private final int code;
        private final String name;

        private ENotificationStatus(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum ETimeDetail {
        DATE(0, "Ngày tháng"),
        DATE_AND_TIME(1, "Ngày giờ");
        private final int code;
        private final String name;

        private ETimeDetail(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum ENotificationType {
        COMMENT_TASK(1, "Bình luận");
        private final int code;
        private final String name;

        private ENotificationType(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    
    public static enum EReadOnly {
    	FALSE(0, "True"),
    	TRUE(1, "False");
        private final int code;
        private final String name;

        private EReadOnly(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum EFilterType {
    	BY_DON_VI_DANG_NHAP("DON_VI_DANG_NHAP", "Đơn vị đăng nhập");
        private final String code;
        private final String name;

        private EFilterType(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    
    public static enum ESolrSearchType{
        BIEU_MAU("BM", "Biểu mẫu"),
        PROCESS("PR", "Process definition"),
        MODELER("MD", "Modeler"),
        HO_SO("HS", "Hồ sơ");
        private final String code;
        private final String name;

        private ESolrSearchType(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
        
        public static ESolrSearchType getSolrSearchType(String code){
        	for(ESolrSearchType p : ESolrSearchType.values()){
        		if(p.getCode().equals(code)){
        			return p;
        		}
        	}
        	return null;
        }
    }
    
    public static enum EPriority {
        LOW(0, "Thấp", "priority-low"),
        MEDIUM(1, "Trung bình", "priority-medium"),
        HIGH(2, "Cao", "priority-high"),
        VERY_HIGH(3, "Hàng đầu", "priority-very-high");
        private final int code;
        private final String name;
        private final String styleName;

        private EPriority(int code, String name, String styleName) {
            this.code = code;
            this.name = name;
            this.styleName = styleName;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
        
        
        
        public String getStyleName() {
			return styleName;
		}

		public static EPriority getPriority(int priority){
        	for(EPriority p : EPriority.values()){
        		if(p.getCode() == priority){
        			return p;
        		}
        	}
        	return EPriority.LOW;
        }
        
        
    }
    
    public static enum EMenu {
        MENU(1, "Là menu"),
        NO_MENU(0, "Không là menu");
        private final int code;
        private final String name;

        private EMenu(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum EDinhKyTongHop {
        NGAY(0, "Ngày"),
        //TUAN(1, "Tuần"),
        THANG(2, "Tháng"),
        //QUY(3, "Quý"),
        //NUA_NAM(4, "Nửa năm"),
        NAM(5, "Năm");
        private final int code;
        private final String name;

        private EDinhKyTongHop(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum ESummaryFunction {
        SUM(0, "SUM", "Sum", false),
        AVG(1, "AVG", "Average", false),
        MIN(2, "MIN", "Minimum", false),
        MAX(3, "MAX", "Maximum", false),
        COUNT(4, "COUNT", "Count", true),
        COUNT_DISTINCT(5, "COUNT_DISTINCT", "Count distinct", true);
        private final int code;
        private final String shortName;
        private final String name;
        private final Boolean useForString;

        private ESummaryFunction(int code, String shortName, String name, Boolean useForString) {
            this.code = code;
            this.shortName = shortName;
            this.name = name;
            this.useForString = useForString;
        }
        
        public static ESummaryFunction getESummaryFunction(Long code){
        	if (code != null) {
                int value = code.intValue();
                for (ESummaryFunction fn : values()) {
                    if (fn.code == value) {
                        return fn;
                    }
                }
            }
            return null;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

		public String getShortName() {
			return shortName;
		}

		public Boolean getUseForString() {
			return useForString;
		}
		
		
        
    }
    
    public static enum EPattern {
    	EMAIL_PATTERN("EMAIL_PATTERN", "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        private final String key;
        private final String value;

        private EPattern(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }
    
    public static enum EGioiHanThoiGian {
        KHONG_GIOI_HAN(0, "Không giới hạn"),
        THOI_GIAN_KHI_NHAP_FORM(1, "Thời gian khi nhập biểu mẫu"),
        THOI_GIAN_XAC_DINH(2, "Thời gian xác định");
        private final int code;
        private final String name;

        private EGioiHanThoiGian(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

    public static enum ELoaiBieuMau {
        BM_THUONG(0, "Biểu mẫu thường"),
        BM_CON(1, "Biểu mẫu con");
        private final int code;
        private final String name;

        private ELoaiBieuMau(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
        
        public static ELoaiBieuMau getLoaiBieuMau(Long code){
        	if (code != null) {
                int value = code.intValue();
                for (ELoaiBieuMau loaiBm : values()) {
                    if (loaiBm.code == value) {
                        return loaiBm;
                    }
                }
            }
            return ELoaiBieuMau.BM_THUONG;
        }
    }
    
    public static enum GroupType {
    	SECURITY_ROLE("security-role", "security-role"),
    	ASSIGNMENT("assignment", "assignment");
    	
        private final String code;
        private final String name;

        private GroupType(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
        
    }

    public static enum ELoaiHang {
        KHONG_LAP(0, "Không lặp"),
        LAP(1, "Lặp");
        private final int code;
        private final String name;

        private ELoaiHang(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

    public static enum ELoaiO {
        TRUONG_THONG_TIN(0, "Trường thông tin"),
        BIEU_MAU(1, "Biểu mẫu"),
        TEXT(2, "Văn bản");
        private final int code;
        private final String name;

        private ELoaiO(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum ELoaiThoiGian {
        TUONG_DOI(0, "Tương đối"),
        CO_DINH(1, "Cố định");
        private final int code;
        private final String name;

        private ELoaiThoiGian(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum EBatBuoc {
        KHONG_BAT_BUOC(0, "Không bắt buộc"),
        BAT_BUOC(1, "Bắt buộc");
        private final int code;
        private final String name;

        private EBatBuoc(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum EKieuReport {
        TABLE(0, "Bảng dữ liệu"),
        CHART(1, "Đồ thị");
        private final int code;
        private final String name;

        private EKieuReport(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum ELoaiDoThi {
        BAR("BAR", "Bar chart"),
        GROUP_BAR("GROUP_BAR", "Group bar chart"),
        PIE("PIE", "Pie"),
        LINE("LINE", "Line"),
        AREA("AREA", "Area");
        private final String code;
        private final String name;

        private ELoaiDoThi(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum EComparison {
        EQUAL(0, "Bằng", "=", 14),
        NOT_EQUAL(1, "Khác", "!=", 14),
        LESS_THAN(2, "Nhỏ hơn", "<", 6),
        LESS_THAN_OR_EQUAL(3, "Nhỏ hơn hoặc bằng", "<=", 6),
        GREATER_THAN(4, "Lớn hơn", ">", 6),
        GREATER_THANOR_EQUAL(5, "Lớn hơn hoặc bằng", ">=", 6),
        LIKE(6, "Like", "LIKE", 8);	
        private final int code;
        private final String name;
        private final String symbol;
        private final int type;//2 number; 4 date; 8 string

        private EComparison(int code, String name, String symbol, int type) {
            this.code = code;
            this.name = name;
            this.symbol = symbol;
            this.type = type;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
        
        
        
        public String getSymbol() {
			return symbol;
		}
        
		public int getType() {
			return type;
		}

		public static EComparison getComparison(Long code){
        	if (code != null) {
                int value = code.intValue();
                for (EComparison loaiToanTu : values()) {
                    if (loaiToanTu.code == value) {
                        return loaiToanTu;
                    }
                }
            }
            return null;
        }
    }
    
    public static enum EReportLayout {
        MAC_DINH(0, "Mặc định");
        private final int code;
        private final String name;

        private EReportLayout(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public static enum ELoaiHienThiHtThamSo {
        TEXT_FIELD(1, "Text Field"),
        TEXT_AREA(2, "Text Area"),
        COMBOBOX(3, "Combobox"),
        CHECKBOX(4, "Checkbox");
        private final int code;
        private final String name;

        private ELoaiHienThiHtThamSo(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    
    public enum EAction {
        THEM(1, "Thêm"),
        SUA(2, "Sửa"),
        XOA(4, "Xóa"),
        PHE_DUYET(8,"Phê duyệt"),
        DEPLOY(16,"Deploy"),
        START(32, "Start");
        final int code;
        private final String name;

        private EAction(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
        
        
    }
    
    
    public static EFormBoHoSo getEFormBoHoSo(String xml){
    	try {
    		if(xml == null){
    			return null;
    		}
    		JAXBContext jaxbContext =
                    JAXBContext.newInstance(EFormBoHoSo.class);
                Unmarshaller jaxbUnmarshaller =
                    jaxbContext.createUnmarshaller();
                ByteArrayInputStream inputStream =
                    new ByteArrayInputStream(xml.getBytes("UTF-8"));
                return (EFormBoHoSo)jaxbUnmarshaller.unmarshal(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static EFormHoSo getEFormHoSo(String xml){
    	try {
    		if(xml == null){
    			return null;
    		}
    		JAXBContext jaxbContext =
                    JAXBContext.newInstance(EFormHoSo.class);
                Unmarshaller jaxbUnmarshaller =
                    jaxbContext.createUnmarshaller();
                ByteArrayInputStream inputStream =
                    new ByteArrayInputStream(xml.getBytes("UTF-8"));
                return (EFormHoSo)jaxbUnmarshaller.unmarshal(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    
    public static String getEFormHoSoString(EFormHoSo hs){
    	try {
    		if(hs == null){
    			return null;
    		}
    		JAXBContext jaxbContext = JAXBContext.newInstance(EFormHoSo.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            jaxbMarshaller.marshal(hs, outputStream);
            return new String(outputStream.toByteArray(), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static String getEFormHoSoString(EFormHoSo hs, List<String> pathList, Integer thuTu){
    	if(pathList != null && !pathList.isEmpty() && thuTu != null){
    		EFormHoSo hsLoopItem = null;
    		while(!pathList.isEmpty()){
    			ArrayList<EFormHoSo> childList = hs.getHoSoList();
    			String currentPath = pathList.get(0);
    			pathList = pathList.subList(1, pathList.size());
    			
    			if("LOOP".equals(currentPath)){
    				ArrayList<EFormTruongThongTin> truongThongTinList = hs.getTruongThongTinList();
    				ArrayList<EFormTruongThongTin> truongThongTinListCopy = new ArrayList();
    				for(EFormTruongThongTin ttt : truongThongTinList){
    					if(thuTu.equals(ttt.getThuTu())){
    						EFormTruongThongTin tttCopy = new EFormTruongThongTin();
    						tttCopy.setGiaTriChu(ttt.getGiaTriChu());
    						tttCopy.setGiaTriNgay(ttt.getGiaTriNgay());
    						tttCopy.setGiaTriSo(ttt.getGiaTriSo());
    						tttCopy.setMaTruongThongTin(ttt.getMaTruongThongTin());
    						truongThongTinListCopy.add(tttCopy);
    					}
    				}
    				hsLoopItem = new EFormHoSo();
    				hsLoopItem.setHoSoList(childList);
    				hsLoopItem.setTruongThongTinList(truongThongTinListCopy);
    				hsLoopItem.setMaBieuMau(hs.getMaBieuMau());
    				hsLoopItem.setTaskId(hs.getTaskId());
    				hsLoopItem.setThuTuHang(hs.getThuTuHang());
    			}else if(childList != null && !childList.isEmpty()){
    				for(EFormHoSo child : childList){
    					if(child != null && currentPath.equals(child.getMaBieuMau())){
    						if(pathList.isEmpty()){
    							if(thuTu.equals(child.getThuTu())){
    								hsLoopItem = child;
    					    	}
    						}else{
    							hs = child;
    						}
    					}
    				}
    			}
    			
        	}
    		
    		if(hsLoopItem != null){
    			return getEFormHoSoString(hsLoopItem);
    		}
    	}
    	return getEFormHoSoString(hs);
    }
    
    
    public static EFormHoSo getEFormHoSoByLoopPath(EFormHoSo hs, List<String> pathList){
    	if(pathList != null && !pathList.isEmpty()){
    		while(!pathList.isEmpty()){
    			ArrayList<EFormHoSo> childList = hs.getHoSoList();
    			String currentPathTmp = pathList.get(0);
    			pathList = pathList.subList(1, pathList.size());
    			
    			Pattern p = Pattern.compile("(.+)@([0-9]+)");
				Matcher m = p.matcher(currentPathTmp);
    			if(m.matches()){
    				String currentPath = m.group(1);
    				Integer thuTu = new Integer(m.group(2));
    				

        			if("LOOP".equals(currentPath)){
        				ArrayList<EFormTruongThongTin> truongThongTinList = hs.getTruongThongTinList();
        				ArrayList<EFormTruongThongTin> truongThongTinListCopy = new ArrayList();
        				for(EFormTruongThongTin ttt : truongThongTinList){
        					if(thuTu.equals(ttt.getThuTu())){
        						EFormTruongThongTin tttCopy = new EFormTruongThongTin();
        						tttCopy.setGiaTriChu(ttt.getGiaTriChu());
        						tttCopy.setGiaTriNgay(ttt.getGiaTriNgay());
        						tttCopy.setGiaTriSo(ttt.getGiaTriSo());
        						tttCopy.setMaTruongThongTin(ttt.getMaTruongThongTin());
        						truongThongTinListCopy.add(tttCopy);
        					}
        				}
        				EFormHoSo hsLoopItem = new EFormHoSo();
        				hsLoopItem.setHoSoList(childList);
        				hsLoopItem.setTruongThongTinList(truongThongTinListCopy);
        				hsLoopItem.setMaBieuMau(hs.getMaBieuMau());
        				hsLoopItem.setTaskId(hs.getTaskId());
        				hsLoopItem.setThuTuHang(hs.getThuTuHang());
        				return hsLoopItem;
        			}else if(childList != null && !childList.isEmpty()){
        				for(EFormHoSo child : childList){
        					if(child != null && currentPath.equals(child.getMaBieuMau())){
        						if(thuTu.equals(child.getThuTu())){
        							hs = child;
    					    	}
        					}
        				}
        			}
    			}
        	}
    	}
    	return hs;
    }
    
    
    public static EFormHoSo getEFormHoSoByLoopPath(EFormHoSo hs, List<String> pathList, List<EFormTruongThongTin> truongThongTinLoop){
    	if(truongThongTinLoop != null){
    		if(pathList != null && !pathList.isEmpty()){
        		while(!pathList.isEmpty()){
        			ArrayList<EFormHoSo> childList = hs.getHoSoList();
        			String currentPathTmp = pathList.get(0);
        			pathList = pathList.subList(1, pathList.size());
        			
        			Pattern p = Pattern.compile("(.+)@([0-9]+)");
    				Matcher m = p.matcher(currentPathTmp);
        			if(m.matches()){
        				String currentPath = m.group(1);
        				Integer thuTu = new Integer(m.group(2));
        				

            			if("LOOP".equals(currentPath)){
            				ArrayList<EFormTruongThongTin> truongThongTinList = hs.getTruongThongTinList();
            				ArrayList<EFormTruongThongTin> truongThongTinListCopy = new ArrayList();
            				for(EFormTruongThongTin ttt : truongThongTinList){
            					if(thuTu.equals(ttt.getThuTu())){
            						EFormTruongThongTin tttCopy = new EFormTruongThongTin();
            						tttCopy.setGiaTriChu(ttt.getGiaTriChu());
            						tttCopy.setGiaTriNgay(ttt.getGiaTriNgay());
            						tttCopy.setGiaTriSo(ttt.getGiaTriSo());
            						tttCopy.setMaTruongThongTin(ttt.getMaTruongThongTin());
            						truongThongTinListCopy.add(tttCopy);
            					}
            				}
            				EFormHoSo hsLoopItem = new EFormHoSo();
            				hsLoopItem.setHoSoList(childList);
            				hsLoopItem.setTruongThongTinList(truongThongTinListCopy);
            				hsLoopItem.setMaBieuMau(hs.getMaBieuMau());
            				hsLoopItem.setTaskId(hs.getTaskId());
            				hsLoopItem.setThuTuHang(hs.getThuTuHang());
            				truongThongTinLoop.addAll(hsLoopItem.getTruongThongTinList());
            				return hsLoopItem;
            			}else if(childList != null && !childList.isEmpty()){
            				for(EFormHoSo child : childList){
            					if(child != null && currentPath.equals(child.getMaBieuMau())){
            						if(thuTu.equals(child.getThuTu())){
            							hs = child;
            							truongThongTinLoop.addAll(hs.getTruongThongTinList());
        					    	}
            					}
            				}
            			}
        			}
            	}
        	}
    	}
    	return hs;
    }
    
    
    public static EFormHoSo getEFormHoSo(EFormHoSo hs, List<String> pathList, Integer thuTu){
    	if(pathList != null && !pathList.isEmpty() && thuTu != null){
    		EFormHoSo hsLoopItem = null;
    		while(!pathList.isEmpty()){
    			ArrayList<EFormHoSo> childList = hs.getHoSoList();
    			String currentPath = pathList.get(0);
    			pathList = pathList.subList(1, pathList.size());
    			
    			if("LOOP".equals(currentPath)){
    				ArrayList<EFormTruongThongTin> truongThongTinList = hs.getTruongThongTinList();
    				ArrayList<EFormTruongThongTin> truongThongTinListCopy = new ArrayList();
    				for(EFormTruongThongTin ttt : truongThongTinList){
    					if(thuTu.equals(ttt.getThuTu())){
    						EFormTruongThongTin tttCopy = new EFormTruongThongTin();
    						tttCopy.setGiaTriChu(ttt.getGiaTriChu());
    						tttCopy.setGiaTriNgay(ttt.getGiaTriNgay());
    						tttCopy.setGiaTriSo(ttt.getGiaTriSo());
    						tttCopy.setMaTruongThongTin(ttt.getMaTruongThongTin());
    						truongThongTinListCopy.add(tttCopy);
    					}
    				}
    				hsLoopItem = new EFormHoSo();
    				hsLoopItem.setHoSoList(childList);
    				hsLoopItem.setTruongThongTinList(truongThongTinListCopy);
    				hsLoopItem.setMaBieuMau(hs.getMaBieuMau());
    				hsLoopItem.setTaskId(hs.getTaskId());
    				hsLoopItem.setThuTuHang(hs.getThuTuHang());
    			}else if(childList != null && !childList.isEmpty()){
    				for(EFormHoSo child : childList){
    					if(child != null && currentPath.equals(child.getMaBieuMau())){
    						if(pathList.isEmpty()){
    							if(thuTu.equals(child.getThuTu())){
    								hsLoopItem = child;
    					    	}
    						}else{
    							hs = child;
    						}
    					}
    				}
    			}
    			
        	}
    		
    		if(hsLoopItem != null){
    			return hsLoopItem;
    		}
    	}
    	return hs;
    }
    
    
    public static String getEFormBoHoSoString(EFormBoHoSo hs){
    	try {

    		if(hs == null){
    			return null;
    		}
    		JAXBContext jaxbContext = JAXBContext.newInstance(EFormBoHoSo.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            jaxbMarshaller.marshal(hs, outputStream);
            return new String(outputStream.toByteArray(), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    
    public static String getUserLogedIn() {
		try {
			Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (obj != null && obj instanceof ActivitiUserDetails) {
				ActivitiUserDetails user = (ActivitiUserDetails) obj;
				return user.getUsername();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
    
    public static boolean isAdmin(IdentityService identityService){
    	try {
			Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (obj != null && obj instanceof ActivitiUserDetails) {
				ActivitiUserDetails user = (ActivitiUserDetails) obj;
				if(user != null && user.getUsername() != null){
					List<Group> groups =identityService.createGroupQuery().groupMember(user.getUsername()).list();
					if(groups != null && !groups.isEmpty()){
						for(Group g : groups){
							if(GroupType.SECURITY_ROLE.getCode().equals(g.getType())){
								return true;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
    }
    
    public static List<String> getGroupId(IdentityService identityService) {
		List<String> group = new ArrayList<String>();
		try {
			List<Group> groups = identityService.createGroupQuery().groupMember(CommonUtils.getUserLogedIn()).list();
			for(Group g: groups){
				group.add(g.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return group;
	}

	public static void sortHtQuyenList(List<HtQuyen> list) {
		Collections.sort(list, new Comparator<HtQuyen>() {
			@Override
			public int compare(final HtQuyen object1, final HtQuyen object2) {
				return object1.getMa().compareTo(object2.getMa());
			}
		});
	}
	
	public static String getRootPath(){
		try {
			String host = UI.getCurrent().getPage().getLocation().getHost();
			String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
			int port = UI.getCurrent().getPage().getLocation().getPort();
			String scheme = UI.getCurrent().getPage().getLocation().getScheme();
			String path = "";
			if(port != -1){
				path = scheme+"://"+host+":"+port+contextPath;
			}else{
				path = scheme+"://"+host+contextPath;
			}
			
			return path;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}

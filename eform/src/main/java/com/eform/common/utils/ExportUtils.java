package com.eform.common.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.SubProcess;
import org.activiti.bpmn.model.UserTask;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.RepositoryService;

import com.eform.common.SpringApplicationContext;
import com.eform.common.type.bieumau.BieuMauExportModel;
import com.eform.common.type.bieumau.BieuMauHangModel;
import com.eform.common.type.bieumau.BieuMauModel;
import com.eform.common.type.bieumau.BieuMauOModel;
import com.eform.common.type.bieumau.BieuMauOTruongThongTinModel;
import com.eform.common.type.bieumau.DanhMucModel;
import com.eform.common.type.bieumau.LoaiDanhMucModel;
import com.eform.common.type.bieumau.TruongThongTinModel;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BieuMauService;
import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmHang;
import com.eform.sync.model.entities.SyncBmO;
import com.eform.sync.model.entities.SyncBmOTruongThongTin;
import com.eform.sync.model.entities.SyncDanhMuc;
import com.eform.sync.model.entities.SyncLoaiDanhMuc;
import com.eform.sync.model.entities.SyncTruongThongTin;
import com.eform.sync.service.SynchronizationService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ExportUtils {
	
	
	public static String exportModeler(String modelDataId) throws Exception{
		SynchronizationService synchronizationService = SpringApplicationContext.getApplicationContext().getBean(SynchronizationService.class);
		RepositoryService repositoryService = SpringApplicationContext.getApplicationContext().getBean(RepositoryService.class);
		JsonNode editorNode = new ObjectMapper().readTree(repositoryService.getModelEditorSource(modelDataId));
		BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
		BpmnModel bpmnModel = jsonConverter.convertToBpmnModel(editorNode);
		if(bpmnModel != null && bpmnModel.getMainProcess() != null){
			List<SyncBieuMau> bieuMauInProcess = new ArrayList();
			List<SyncLoaiDanhMuc> loaiDanhMucInProcess = new ArrayList(); //Danh muc button
			
			List<org.activiti.bpmn.model.Process> listProcess = bpmnModel.getProcesses();
			for (org.activiti.bpmn.model.Process process : listProcess) {
				Collection<FlowElement> elements = process.getFlowElements();
				List<FlowElement> listElements = new ArrayList(elements);
				while(!listElements.isEmpty()){
					FlowElement elementObj = listElements.remove(0);
					if (elementObj instanceof UserTask) {
						UserTask element = (UserTask) elementObj;
						String formKey = element.getFormKey();
						if (formKey != null) {
							String buttonKey = null;
							Pattern p = Pattern.compile("(.+)#(.+)");
							Matcher m = p.matcher(formKey);
							if(m.matches()){
								formKey = m.group(1);
								buttonKey = m.group(2);
							}
							if(buttonKey != null){
								SyncLoaiDanhMuc loaiDanhMucBtn = getLoaiDanhMucFromButtonKey(buttonKey);
								if(loaiDanhMucBtn != null && !loaiDanhMucInProcess.contains(loaiDanhMucBtn)){
									loaiDanhMucInProcess.add(loaiDanhMucBtn);
								}
							}
							List<SyncBieuMau> bieuMauList = synchronizationService.findBieuMauByCode(formKey);
							if (bieuMauList != null && !bieuMauList.isEmpty()) {
								SyncBieuMau bieuMau = bieuMauList.get(0);
								if(!bieuMauInProcess.contains(bieuMau)){
									bieuMauInProcess.add(bieuMau);
								}
							}
						}
					}else if (elementObj instanceof org.activiti.bpmn.model.StartEvent) {
						org.activiti.bpmn.model.StartEvent element = (org.activiti.bpmn.model.StartEvent) elementObj;
						String formKey = element.getFormKey();
						if (formKey != null) {
							String buttonKey = null;
							Pattern p = Pattern.compile("(.+)#(.+)");
							Matcher m = p.matcher(formKey);
							if(m.matches()){
								formKey = m.group(1);
								buttonKey = m.group(2);
							}
							if(buttonKey != null){
								SyncLoaiDanhMuc loaiDanhMucBtn = getLoaiDanhMucFromButtonKey(buttonKey);
								if(loaiDanhMucBtn != null && !loaiDanhMucInProcess.contains(loaiDanhMucBtn)){
									loaiDanhMucInProcess.add(loaiDanhMucBtn);
								}
							}
							List<SyncBieuMau> bieuMauList = synchronizationService.findBieuMauByCode(formKey);
							if (bieuMauList != null && !bieuMauList.isEmpty()) {
								SyncBieuMau bieuMau = bieuMauList.get(0);
								if(!bieuMauInProcess.contains(bieuMau)){
									bieuMauInProcess.add(bieuMau);
								}
							}
						}
					}else if(elementObj instanceof SubProcess){
						SubProcess element = (SubProcess) elementObj;
						Collection<FlowElement> subProcessElements = element.getFlowElements();
						List<FlowElement> subProcessListElement = new ArrayList(subProcessElements);
						listElements.addAll(subProcessListElement);
					}
				}
			}
			return exportBieuMauAll(bieuMauInProcess, loaiDanhMucInProcess);
		}
		return null;
	}
	
	public static String exportBieuMauAll(List<SyncBieuMau> bieuMauList, List<SyncLoaiDanhMuc> loaiDanhMucList){
		if(bieuMauList != null){
			getBieuMauAll(bieuMauList);
			if(loaiDanhMucList == null){
				loaiDanhMucList = new ArrayList();
			}
			
			List<SyncTruongThongTin> truongThongTinList = new ArrayList();
			
			ArrayList<BieuMauModel> bieuMauModelList = new ArrayList();
			if(bieuMauList != null){
				for (SyncBieuMau bm : bieuMauList) {
					BieuMauModel bmModel = getBieuMauModel(bm, truongThongTinList, loaiDanhMucList);
					bieuMauModelList.add(bmModel);
				}
			}
			
			ArrayList<TruongThongTinModel> truongThongTinModelList = new ArrayList();
			if(truongThongTinList != null){
				for (SyncTruongThongTin truongThongTin : truongThongTinList) {
					truongThongTinModelList.add(new TruongThongTinModel(truongThongTin));
				}
			}
			
			ArrayList<LoaiDanhMucModel> loaiDanhMucModelList = new ArrayList();
			if(loaiDanhMucList != null){
				getLoaiDanhMucAll(loaiDanhMucList);
				for (SyncLoaiDanhMuc loaiDanhMuc : loaiDanhMucList) {
					loaiDanhMucModelList.add(getLoaiDanhMucModel(loaiDanhMuc));
				}
			}
			
			BieuMauExportModel bmExportModel = new BieuMauExportModel();
			
			bmExportModel.setBieuMauList(bieuMauModelList);
			bmExportModel.setTruongThongTinList(truongThongTinModelList);
			bmExportModel.setLoaiDanhMucList(loaiDanhMucModelList);
			
			return getXmlFromObject(bmExportModel, BieuMauExportModel.class);
		}
		return null;
	}
	
	public static String exportBieuMau(SyncBieuMau bieuMau){
		List<SyncLoaiDanhMuc> loaiDanhMucList = new ArrayList();
		List<SyncTruongThongTin> truongThongTinList = new ArrayList();
		BieuMauModel bmModel = getBieuMauModel(bieuMau, truongThongTinList, loaiDanhMucList);
		
		ArrayList<TruongThongTinModel> truongThongTinModelList = new ArrayList();
		if(truongThongTinList != null){
			for (SyncTruongThongTin truongThongTin : truongThongTinList) {
				truongThongTinModelList.add(new TruongThongTinModel(truongThongTin));
			}
		}
		
		ArrayList<LoaiDanhMucModel> loaiDanhMucModelList = new ArrayList();
		if(loaiDanhMucList != null){
			getLoaiDanhMucAll(loaiDanhMucList);
			for (SyncLoaiDanhMuc loaiDanhMuc : loaiDanhMucList) {
				loaiDanhMucModelList.add(getLoaiDanhMucModel(loaiDanhMuc));
			}
		}
		
		ArrayList<BieuMauModel> bieuMauList = new ArrayList();
		bieuMauList.add(bmModel);
		
		BieuMauExportModel bmExportModel = new BieuMauExportModel();
		bmExportModel.setBieuMauList(bieuMauList);
		bmExportModel.setTruongThongTinList(truongThongTinModelList);
		bmExportModel.setLoaiDanhMucList(loaiDanhMucModelList);
		
		return getXmlFromObject(bmExportModel, BieuMauExportModel.class);
	}
	
	public static BieuMauModel getBieuMauModel(SyncBieuMau bieuMau, List<SyncTruongThongTin> truongThongTinList, List<SyncLoaiDanhMuc> loaiDanhMucList){
		if(bieuMau != null){
			SynchronizationService synchronizationService = SpringApplicationContext.getApplicationContext().getBean(SynchronizationService.class);
			BieuMauService bieuMauService = SpringApplicationContext.getApplicationContext().getBean(BieuMauService.class);
			
			BieuMauModel bieuMauModel = new BieuMauModel(bieuMau);
			
			List<SyncBmHang> hangList = synchronizationService.getBmHangFind(null, null, null, bieuMau, -1, -1);
			ArrayList<BieuMauHangModel> hangModelList = new ArrayList();
			bieuMauModel.setHangList(hangModelList);
			if(hangList != null){
				for (SyncBmHang bmHang : hangList) {
					BieuMauHangModel hangModel = new BieuMauHangModel(bmHang);
					hangModelList.add(hangModel);
					ArrayList<BieuMauOModel> oModelList = new ArrayList();
					hangModel.setoList(oModelList);
					List<SyncBmO> oList = synchronizationService.getBmOFind(null, null, null, bieuMau, bmHang, null, -1, -1);
					if(oList != null){
						for (SyncBmO bmO : oList) {
							BieuMauOModel oModel = new BieuMauOModel(bmO);
							oModelList.add(oModel);
							List<SyncBmOTruongThongTin> bmOTruongThongTinList = synchronizationService.getBmOTruongThongTinFind(bieuMau, bmHang, bmO, null, -1, -1);
							ArrayList<BieuMauOTruongThongTinModel> oTruongThongTinModelList = new ArrayList();
							oModel.setoTruongThongTinList(oTruongThongTinModelList);
							if(bmOTruongThongTinList != null){
								for (SyncBmOTruongThongTin bmOTruongThongTin : bmOTruongThongTinList) {
									BieuMauOTruongThongTinModel bmOTruongThongTinModel = new BieuMauOTruongThongTinModel(bmOTruongThongTin);
									oTruongThongTinModelList.add(bmOTruongThongTinModel);
									if(bmOTruongThongTin.getTruongThongTin() != null){
										SyncTruongThongTin truongTT = bmOTruongThongTin.getTruongThongTin();
										
										if(!truongThongTinList.contains(truongTT)){
											truongThongTinList.add(truongTT);
										}
										if(truongTT.getLoaiDanhMuc() != null && !loaiDanhMucList.contains(truongTT.getLoaiDanhMuc())){
											loaiDanhMucList.add(truongTT.getLoaiDanhMuc());
										}
									}
								}
							}
						}
					}
				}
			}
			return bieuMauModel;
		}
		return null;
	}
	
	public static LoaiDanhMucModel getLoaiDanhMucModel(SyncLoaiDanhMuc loaiDanhMuc){
		if(loaiDanhMuc != null){
			SynchronizationService synchronizationService = SpringApplicationContext.getApplicationContext().getBean(SynchronizationService.class);
			LoaiDanhMucModel loaiDanhMucModel = new LoaiDanhMucModel(loaiDanhMuc);
			List<SyncDanhMuc> danhMucList = synchronizationService.getDanhMucFind(null, null, null, loaiDanhMuc, null, -1, -1);
			ArrayList<DanhMucModel> danhMucModelList = new ArrayList();
			if(danhMucList != null){
				for (SyncDanhMuc danhMuc : danhMucList) {
					danhMucModelList.add(new DanhMucModel(danhMuc));
				}
			}
			loaiDanhMucModel.setDanhMucList(danhMucModelList);
			return loaiDanhMucModel;
		}
		return null;
	}
	
	public static void getLoaiDanhMucAll(List<SyncLoaiDanhMuc> loaiDanhMucList){
		List<SyncLoaiDanhMuc> loaiDanhMucListTmp = new ArrayList(loaiDanhMucList);
		while(!loaiDanhMucListTmp.isEmpty()){
			SyncLoaiDanhMuc current = loaiDanhMucListTmp.remove(0);
			if(current.getLoaiDanhMuc() != null){
				loaiDanhMucListTmp.add(current.getLoaiDanhMuc());
				if(!loaiDanhMucList.contains(current.getLoaiDanhMuc())){
					loaiDanhMucList.add(current.getLoaiDanhMuc());
				}
			}
		}
	}
	
	
	public static List<SyncBieuMau> getBieuMauAll(List<SyncBieuMau> bieuMauList){
		SynchronizationService synchronizationService = SpringApplicationContext.getApplicationContext().getBean(SynchronizationService.class);
		List<SyncBieuMau> bieuMauTmpList = new ArrayList(bieuMauList);
		while(!bieuMauTmpList.isEmpty()){
			SyncBieuMau bmCurrent = bieuMauTmpList.remove(0);
			if(!bieuMauList.contains(bmCurrent)){
				bieuMauList.add(bmCurrent);
			}
			List<SyncBmO> bmOList = synchronizationService.getBmOFind(null, null, new Long(CommonUtils.ELoaiO.BIEU_MAU.getCode()), bmCurrent, null, null, -1, -1);
			if(bmOList != null){
				for (SyncBmO bmO : bmOList) {
					bieuMauTmpList.add(bmO.getBieuMauCon());
				}
			}
		}
		Collections.reverse(bieuMauList);
		return bieuMauList;
	}
	
	public static SyncLoaiDanhMuc getLoaiDanhMucFromButtonKey(String btnStr){
		if(btnStr != null){
			Pattern p = Pattern.compile("(.+):\\[(.+)\\]");
        	Matcher m = p.matcher(btnStr);
        	if(!m.matches()){
        		SynchronizationService synchronizationService = SpringApplicationContext.getApplicationContext().getBean(SynchronizationService.class);
        		List<SyncLoaiDanhMuc> loaiDanhMucList = synchronizationService.getLoaiDanhMucFind(btnStr, null, null, null, 0, 1);
        		if(loaiDanhMucList != null && !loaiDanhMucList.isEmpty()){
        			return loaiDanhMucList.get(0);
        		}
        	}
		}
		return null;
	}
	
	public static Object getObjectFromXml(String xml, Class clazz){
    	try {
    		if(xml == null){
    			return null;
    		}
    		JAXBContext jaxbContext =
                    JAXBContext.newInstance(clazz);
                Unmarshaller jaxbUnmarshaller =
                    jaxbContext.createUnmarshaller();
                ByteArrayInputStream inputStream =
                    new ByteArrayInputStream(xml.getBytes("UTF-8"));
                return jaxbUnmarshaller.unmarshal(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static String getXmlFromObject(Object hs, Class clazz){
    	try {
    		if(hs == null){
    			return null;
    		}
    		JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            jaxbMarshaller.marshal(hs, outputStream);
            return new String(outputStream.toByteArray(), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static BieuMauExportModel getBieuMauExport(String str){
    	if(str != null){
    		BieuMauExportModel exportModel = (BieuMauExportModel) getObjectFromXml(str, BieuMauExportModel.class);
    		return exportModel;
    	}
    	return null;
    }
    
    private static String readFileAsString(String filePath) throws IOException {
        StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }
    public static void importTest(){
    	try {
			String content = readFileAsString("E:/Workspace/Test/export.xml");
			importBieuMau(getBieuMauExport(content));
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public static void importBieuMau(String exportModel){
//    	System.out.println(exportModel);
    	importBieuMau(getBieuMauExport(exportModel));
    }
    public static void importBieuMau(BieuMauExportModel exportModel){
    	if(exportModel != null && exportModel.getBieuMauList() != null){
    		List<LoaiDanhMucModel> loaiDanhMucModelList = exportModel.getLoaiDanhMucList();
    		List<SyncLoaiDanhMuc> loaiDanhMucList = new ArrayList();
    		List<SyncDanhMuc> danhMucList = new ArrayList();
    		if(loaiDanhMucModelList != null){
    			for (LoaiDanhMucModel loaiDanhMucModel : loaiDanhMucModelList) {
    				SyncLoaiDanhMuc loaiDanhMuc = loaiDanhMucModel.getEntity();
					loaiDanhMucList.add(loaiDanhMuc);
					if(loaiDanhMucModel.getDanhMucList() != null){
						for (DanhMucModel danhMucModel : loaiDanhMucModel.getDanhMucList()) {
							SyncDanhMuc danhMuc = danhMucModel.getEntity();
							danhMuc.setLoaiDanhMuc(loaiDanhMuc);
							danhMucList.add(danhMuc);
						}
					}
				}
    		}
    		
    		List<TruongThongTinModel> truongThongTinModelList = exportModel.getTruongThongTinList();
    		List<SyncTruongThongTin> truongThongTinList = new ArrayList();
    		if(truongThongTinModelList != null){
    			for (TruongThongTinModel truongThongTinModel : truongThongTinModelList) {
    				SyncTruongThongTin truongThongTin = truongThongTinModel.getEntity();
					truongThongTinList.add(truongThongTin);
				}
    		}
    		
    		List<BieuMauModel> bieuMauModelList = exportModel.getBieuMauList();
    		List<SyncBieuMau> bieuMauList = new ArrayList();
    		List<SyncBmHang> bmHangList = new ArrayList();
    		List<SyncBmO> bmOList = new ArrayList();
    		List<SyncBmOTruongThongTin> bmOTruongThongTinList = new ArrayList();
    		
    		for (BieuMauModel bieuMauModel : bieuMauModelList) {
    			SyncBieuMau bieuMau = bieuMauModel.getEntity();
    			bieuMauList.add(bieuMau);
    			if(bieuMauModel.getHangList() != null){
    				for (BieuMauHangModel bieuMauHangModel : bieuMauModel.getHangList()) {
    					SyncBmHang bmHang = bieuMauHangModel.getEntity();
						bmHang.setBieuMau(bieuMau);
						bmHangList.add(bmHang);
						if(bieuMauHangModel.getoList() != null){
							for(BieuMauOModel bieuMauOModel : bieuMauHangModel.getoList()){
								SyncBmO bmO = bieuMauOModel.getEntity();
								bmO.setBmHang(bmHang);
								bmO.setBieuMau(bieuMau);
								bmOList.add(bmO);
								if(bieuMauOModel.getoTruongThongTinList() != null){
									for (BieuMauOTruongThongTinModel oTruongTTModel : bieuMauOModel.getoTruongThongTinList()) {
										SyncBmOTruongThongTin bmOTruongThongTin = oTruongTTModel.getEntity();
										bmOTruongThongTin.setBieuMau(bieuMau);
										bmOTruongThongTin.setBmHang(bmHang);
										bmOTruongThongTin.setBmO(bmO);
										bmOTruongThongTinList.add(bmOTruongThongTin);
									}
								}
							}
						}
					}
    			}
			}
    		SynchronizationService synchronizationService = SpringApplicationContext.getApplicationContext().getBean(SynchronizationService.class);
    		synchronizationService.importBieuMau(loaiDanhMucList, danhMucList, truongThongTinList, bieuMauList, bmHangList, bmOList, bmOTruongThongTinList);
    		
    	}
    }
}

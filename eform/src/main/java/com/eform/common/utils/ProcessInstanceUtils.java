package com.eform.common.utils;

import org.activiti.engine.HistoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.TaskInfo;

import com.eform.common.SpringApplicationContext;

public class ProcessInstanceUtils {

	
	public static String getParentInstance(String id, HistoryService historyService) {
		try {
			HistoricProcessInstance proIns = historyService.createHistoricProcessInstanceQuery().processInstanceId(id).singleResult();
			if(proIns != null){
					if(proIns.getSuperProcessInstanceId() == null){
				return proIns.getSuperProcessInstanceId();
			}else{
				HistoricProcessInstance superProIns = historyService.createHistoricProcessInstanceQuery().processInstanceId(proIns.getSuperProcessInstanceId()).singleResult();
				while (superProIns != null && superProIns.getSuperProcessInstanceId() != null) {
					superProIns = historyService.createHistoricProcessInstanceQuery().processInstanceId(superProIns.getSuperProcessInstanceId()).singleResult();
				}

				if(superProIns != null){
					return superProIns.getId();
				}
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//Lay task cha co process ins id
	public static TaskInfo getParentTaskIdInProcess(String taskId){
		try {
			TaskService taskService = SpringApplicationContext.getApplicationContext().getBean(TaskService.class);
			HistoryService historyService = SpringApplicationContext.getApplicationContext().getBean(HistoryService.class);
			//Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
			
			HistoricTaskInstance task = historyService.createHistoricTaskInstanceQuery().taskId(taskId).singleResult();
			
			while(task != null && task.getParentTaskId() != null && task.getProcessInstanceId() == null){
				//task = taskService.createTaskQuery().taskId(task.getParentTaskId()).singleResult();
				task = historyService.createHistoricTaskInstanceQuery().taskId(task.getParentTaskId()).singleResult();
			}
			if(task != null){
				return task;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

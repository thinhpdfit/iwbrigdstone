package com.eform.common.utils;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.activiti.bpmn.model.FormProperty;
import org.activiti.bpmn.model.FormValue;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.type.EFormTruongThongTin;
import com.eform.common.workflow.ITrangThai;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BieuMauService;
import com.eform.service.DanhMucService;
import com.eform.service.LoaiDanhMucService;

public class FormUtils {

	private BieuMauService bieuMauService;
	private LoaiDanhMucService loaiDanhMucService;
	private DanhMucService danhMucService;
	
	public FormUtils(BieuMauService bieuMauService, LoaiDanhMucService loaiDanhMucService, DanhMucService danhMucService){
		this.bieuMauService = bieuMauService;
		this.loaiDanhMucService = loaiDanhMucService;
		this.danhMucService = danhMucService;
	}
	
	public List<FormProperty> getAllFormProperty(BieuMau bm, String parentId){
		List<FormProperty> formPropertyListTmp = new ArrayList<FormProperty>();
		try {

			List<BmOTruongThongTin> truongThongTinList = bieuMauService.getBmOTruongThongTinFind(bm, null, null, null, -1, -1);
			for(BmOTruongThongTin oTruongTT : truongThongTinList){
				if(oTruongTT.getTruongThongTin() != null){
					FormProperty formProperty = new FormProperty();
					String key = parentId != null ? (parentId+"_"+oTruongTT.getTruongThongTin().getMa()) : (oTruongTT.getTruongThongTin().getMa());
					formProperty.setId(key);
					formProperty.setName(oTruongTT.getTruongThongTin().getTen());
					formProperty.setReadable(true);
					formProperty.setWriteable(true);
					formProperty.setDatePattern("dd/MM/yyyy HH:mm");
//					formProperty.setVariable(parentId+"_"+oTruongTT.getTruongThongTin().getMa());
					formProperty.setType(getFormPropertyType(oTruongTT.getTruongThongTin().getKieuDuLieu()));
					if(oTruongTT.getTruongThongTin().getKieuDuLieu() != null && 
							oTruongTT.getTruongThongTin().getLoaiDanhMuc() != null &&
							CommonUtils.EKieuDuLieu.isCoDanhMuc(oTruongTT.getTruongThongTin().getKieuDuLieu().intValue())){
						WorkflowManagement<DanhMuc> wf = new WorkflowManagement<DanhMuc>(EChucNang.E_FORM_DANH_MUC.getMa());
						List<Long> trangThai = null;
						if(wf.getTrangThaiSuDung() != null){
							trangThai = new ArrayList<Long>();
							trangThai.add(new Long(wf.getTrangThaiSuDung().getId()));
						}
						List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(null, null, trangThai, oTruongTT.getTruongThongTin().getLoaiDanhMuc(), null, -1, -1);
						if(danhMucList != null && !danhMucList.isEmpty()){
							List<FormValue> formValues = new ArrayList<FormValue>();
							for(DanhMuc dm : danhMucList){
								FormValue formValue = new FormValue();
								formValue.setId(dm.getMa());
                                formValue.setName(dm.getTen());
                                formValues.add(formValue);
							}
							formProperty.setFormValues(formValues);
						}
						
					}
					formPropertyListTmp.add(formProperty);
				}
			}
			List<BmO> bmOList = bieuMauService.getBmOFind(null, null, new Long(CommonUtils.ELoaiO.BIEU_MAU.getCode()), bm, null, null, -1, -1);
			if(bmOList != null && !bmOList.isEmpty()){
				for(BmO bmO : bmOList){
					if(bmO.getBieuMauCon() != null){
//						String ma = bmO.getMa() != null ? bmO.getMa() : "";
						String ma = bmO.getBieuMauCon().getMa() != null ? bmO.getBieuMauCon().getMa() : "";
						parentId = parentId != null ? parentId+"_"+ma : ma;
						formPropertyListTmp.addAll(getAllFormProperty(bmO.getBieuMauCon(), parentId));
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formPropertyListTmp;
	}
	
	public List<FormProperty> getButtonProperty(String btnStr){
		List<FormProperty> formPropertyListTmp = new ArrayList<FormProperty>();
		try {
			if(btnStr != null){
				Pattern p = Pattern.compile("(.+):\\[(.+)\\]");
	        	Matcher m = p.matcher(btnStr);
	        	if(m.matches()){
        			String btnGroup = m.group(1).trim();
	        		String btns = m.group(2);
	        		String btnArr[] = btns.split(";");
	        		if(btnArr != null && btnArr.length >0){
    	    			FormProperty formProperty = new FormProperty();
    					formProperty.setId(btnGroup);
    					formProperty.setName("Nhóm button");
    					formProperty.setReadable(true);
    					formProperty.setWriteable(true);
    					formProperty.setDatePattern("dd/MM/yyyy HH:mm");
    					formProperty.setType(Constants.ENUM_FORM_TYPE);
	        			
	        			
	        			Pattern p1 = Pattern.compile("(.+):(.+)");
	        			List<FormValue> formValues = new ArrayList<FormValue>();
	        	    	for(int i =0; i< btnArr.length; i++){
	        	    		Matcher m1 = p1.matcher(btnArr[i]);
	        	    		if(m1.matches()){
    							FormValue formValue = new FormValue();
    							formValue.setId(m1.group(1).trim());
                                formValue.setName(m1.group(2).trim());
                                formValues.add(formValue);
	        	    		}
	        	    	}
	        	    	formProperty.setFormValues(formValues);
	        	    	formPropertyListTmp.add(formProperty);
	        		}
	        	}else{
	        		return getButtonPropertyFromDanhMuc(btnStr);
	        	}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formPropertyListTmp;
	}
	
	public List<FormProperty> getButtonPropertyFromDanhMuc(String maLoaiDanhMuc){
		List<FormProperty> formPropertyListTmp = new ArrayList<FormProperty>();
		try {
			if(maLoaiDanhMuc != null){
				WorkflowManagement<LoaiDanhMuc> ldmWM = new WorkflowManagement<LoaiDanhMuc>(EChucNang.E_FORM_LOAI_DANH_MUC.getMa());
				ITrangThai ldmTrangThaiSD = ldmWM.getTrangThaiSuDung();
				List<Long> ldmTrangThaiList = null;
				if(ldmTrangThaiSD != null){
					ldmTrangThaiList = new ArrayList<Long>();
					ldmTrangThaiList.add(new Long(ldmTrangThaiSD.getId()));
				}
				List<LoaiDanhMuc> loaiDanhMucList = loaiDanhMucService.getLoaiDanhMucFind(maLoaiDanhMuc, null, ldmTrangThaiList, null, 0, 1);
				if(loaiDanhMucList != null && !loaiDanhMucList.isEmpty()){
					LoaiDanhMuc loaiDanhMuc = loaiDanhMucList.get(0);
					
					FormProperty formProperty = new FormProperty();
					formProperty.setId(loaiDanhMuc.getMa());
					formProperty.setName(loaiDanhMuc.getTen());
					formProperty.setReadable(true);
					formProperty.setWriteable(true);
					formProperty.setDatePattern("dd/MM/yyyy HH:mm");
					formProperty.setType(Constants.ENUM_FORM_TYPE);
					WorkflowManagement<DanhMuc> dmWM = new WorkflowManagement<DanhMuc>(EChucNang.E_FORM_DANH_MUC.getMa());
					ITrangThai dmTrangThaiSD = dmWM.getTrangThaiSuDung();
					List<Long> dmTrangThaiList = null;
					if(ldmTrangThaiSD != null){
						dmTrangThaiList = new ArrayList<Long>();
						dmTrangThaiList.add(new Long(dmTrangThaiSD.getId()));
					}
					List<DanhMuc> danhMucList = danhMucService.getDanhMucFind(null, null, dmTrangThaiList, loaiDanhMuc, null, -1, -1);
					if(danhMucList != null && !danhMucList.isEmpty()){
						List<FormValue> formValues = new ArrayList<FormValue>();
						for(DanhMuc dm : danhMucList){
							FormValue formValue = new FormValue();
							formValue.setId(dm.getMa());
                            formValue.setName(dm.getTen());
                            formValues.add(formValue);
						}
						formProperty.setFormValues(formValues);
					}
					formPropertyListTmp.add(formProperty);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formPropertyListTmp;
	}
	
	
	public String getFormPropertyType(Long kieuDuLieu){
		if(kieuDuLieu != null){
			if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.CHECKBOX.getMa()){
				return Constants.BOOLEAN_FORM_TYPE;
			}else if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
				return Constants.DATE_FORM_TYPE;
			}else if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.NUMBER.getMa()){
				return Constants.DOUBLE_FORM_TYPE;
			}else if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa()){
				return Constants.LONG_FORM_TYPE;
			}else if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.TEXT.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.TEXT_AREA.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.EDITOR.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.EMAIL.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.FILE.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.LUA_CHON_NHIEU.getMa()){
				return Constants.STRING_FORM_TYPE;
			}else if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.RADIO_BUTTON.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.DANH_MUC.getMa()){
				return Constants.ENUM_FORM_TYPE;
			}
		}
		return Constants.STRING_FORM_TYPE;
	}
	
	
	public static int getSQLType(Long kieuDuLieu){
		if(kieuDuLieu != null){
			if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.CHECKBOX.getMa()){
				return java.sql.Types.BOOLEAN;
			}else if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
				return java.sql.Types.DATE;
			}else if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.NUMBER.getMa()){
				return java.sql.Types.FLOAT;
			}else if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa()){
				return java.sql.Types.INTEGER;
			}else if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.TEXT.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.TEXT_AREA.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.EDITOR.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.EMAIL.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.FILE.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.LUA_CHON_NHIEU.getMa()){
				return java.sql.Types.VARCHAR;
			}else if(kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.RADIO_BUTTON.getMa() ||
					kieuDuLieu.intValue() == CommonUtils.EKieuDuLieu.DANH_MUC.getMa()){
				return java.sql.Types.VARCHAR;
			}
		}
		return java.sql.Types.VARCHAR;
	}
	
	public static EFormBoHoSo getBoHs(EFormBoHoSo eBoHS, EFormHoSo hs, Boolean isComplete, String taskId, String loopPath, Integer loopIndex, String dataLoopPath){
		try {
			if(eBoHS == null){
				eBoHS = new EFormBoHoSo();
			}
			
			if(hs != null){
				eBoHS.setHoSo(hs);
				
				if(isComplete){
					ArrayList<EFormHoSo> historyList = eBoHS.getLichSuList();
					if(historyList == null){
						historyList = new ArrayList<EFormHoSo>();
					}
					
					EFormHoSo history = hs.clone();
					history.setLoopItemPath(loopPath);
					history.setLoopItemIndex(loopIndex);
					history.setTaskId(taskId);
					history.setDataLoopPath(dataLoopPath);
					historyList.add(history);
					eBoHS.setLichSuList(historyList);
				}
				return eBoHS;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static EFormBoHoSo getBoHs(EFormBoHoSo eBoHS, EFormHoSo hs, Boolean isComplete, String taskId){
		return getBoHs(eBoHS, hs, isComplete, taskId, null, null, null);
	}
	
	
	public static void updateEFormHoSo(EFormHoSo hsRoot, EFormHoSo hsNew, List<String> pathList, String dataLoopPath, Integer thuTu){
		
		if(dataLoopPath != null){
    		String[] pathArr = dataLoopPath.split("#");
			List<String> pathListTmp = java.util.Arrays.asList(pathArr);
			if(pathListTmp != null && !pathListTmp.isEmpty()){
	    		while(!pathListTmp.isEmpty()){
	    			ArrayList<EFormHoSo> childList = hsRoot.getHoSoList();
	    			String currentPathTmp = pathListTmp.get(0);
	    			pathListTmp = pathListTmp.subList(1, pathListTmp.size());
	    			
	    			Pattern p = Pattern.compile("(.+)@([0-9]+)");
					Matcher m = p.matcher(currentPathTmp);
	    			if(m.matches()){
	    				String currentPath = m.group(1);
	    				Integer tt = new Integer(m.group(2));
	    				if(childList != null && !childList.isEmpty()){
	        				for(EFormHoSo child : childList){
	        					if(child != null && currentPath.equals(child.getMaBieuMau())){
	        						if(tt.equals(child.getThuTu())){
	        							updateEFormHoSo(child, hsNew, pathList, thuTu);
	    					    	}
	        					}
	        				}
	        			}
	    			}
	    		}
			}
    	}else{
    		updateEFormHoSo(hsRoot, hsNew, pathList, thuTu);
    	}
		
    }
	
	
	
	public static void updateEFormHoSo(EFormHoSo hs, EFormHoSo hsNew, List<String> pathList, Integer thuTu){
		if(pathList != null && !pathList.isEmpty() && thuTu != null){
    		while(!pathList.isEmpty()){
    			ArrayList<EFormHoSo> childList = hs.getHoSoList();
    			String currentPath = pathList.get(0);
    			pathList = pathList.subList(1, pathList.size());
    			
    			if("LOOP".equals(currentPath)){
    				if(hs.getTruongThongTinList() != null){
    					hs.getTruongThongTinList().clear();
    					hs.getTruongThongTinList().addAll(hsNew.getTruongThongTinList());
    				}
    			}else if(childList != null && !childList.isEmpty()){
    				boolean match = false;
    				for(EFormHoSo child : childList){
    					if(child != null && currentPath.equals(child.getMaBieuMau())){
    						if(pathList.isEmpty()){
    							if(thuTu.equals(child.getThuTu())){
    								child.setHoSoList(hsNew.getHoSoList());
    								child.setTruongThongTinList(hsNew.getTruongThongTinList());
    					    	}
    						}else{
    							hs = child;
    						}
    						match =true;
    					}
    				}
    				if(!match){
    					childList.add(hsNew);
    				}
    			}else{
    				childList = new ArrayList();
    				childList.add(hsNew);
    				hs.setHoSoList(childList);
    			}
    			
        	}
    	}else{
    		if(hs == null){
    			hs = hsNew;
    		}else{
    			hs.setHoSoList(hsNew.getHoSoList());
    			hs.setTruongThongTinList(hsNew.getTruongThongTinList());
    		}
    	}
    }
	
	
	
	
	public static void updateHoSo(EFormHoSo hs, EFormHoSo eHoSoItem, List<String> pathList, Integer thuTu){
		ArrayList<EFormTruongThongTin> truongThongTinCurrentList = eHoSoItem.getTruongThongTinList();
		if(pathList != null && !pathList.isEmpty() && thuTu != null){
			ArrayList<EFormHoSo> childList = hs.getHoSoList();
    		while(pathList != null && !pathList.isEmpty()){
    			String currentPath = pathList.get(0);
    			pathList = pathList.subList(1, pathList.size());
    			if("LOOP".equals(currentPath)){
    				ArrayList<EFormTruongThongTin> truongThongTinList = hs.getTruongThongTinList();
    				ArrayList<EFormTruongThongTin> truongThongTinChangedList = new ArrayList();
    				for(EFormTruongThongTin ttt : truongThongTinList){
    					if(thuTu.equals(ttt.getThuTu())){
    						for(EFormTruongThongTin tttLoop : truongThongTinCurrentList){
    							if(ttt.getMaTruongThongTin() != null && ttt.getMaTruongThongTin().equals(tttLoop.getMaTruongThongTin())){
    								ttt.setGiaTriChu(tttLoop.getGiaTriChu());
    								ttt.setGiaTriNgay(tttLoop.getGiaTriNgay());
    								ttt.setGiaTriSo(tttLoop.getGiaTriSo());
    								ttt.setMaTruongThongTin(tttLoop.getMaTruongThongTin());
    								truongThongTinChangedList.add(tttLoop);
    							}
    						}
    					}
    				}
    				
    				ArrayList<EFormTruongThongTin> truongThongTinAdditionList = new ArrayList(truongThongTinCurrentList);
    				truongThongTinAdditionList.removeAll(truongThongTinChangedList);
    				if(truongThongTinAdditionList != null){
    					for(EFormTruongThongTin tttAdd : truongThongTinAdditionList){
    						tttAdd.setThuTu(thuTu);
    					}
    					truongThongTinList.addAll(truongThongTinAdditionList);
    				}
    				
    			}else if(childList != null && !childList.isEmpty()){
    				for(EFormHoSo child : childList){
    					if(child != null && currentPath.equals(child.getMaBieuMau())){
    						updateHoSo(child, eHoSoItem, pathList, thuTu);
    					}
    				}
    			}
    		}
		}else{
			if(thuTu.equals(hs.getThuTu())){
				ArrayList<EFormTruongThongTin> truongThongTinList = hs.getTruongThongTinList();
				ArrayList<EFormTruongThongTin> truongThongTinChangedList = new ArrayList();
				for(EFormTruongThongTin ttt : truongThongTinList){
					for(EFormTruongThongTin tttLoop : truongThongTinCurrentList){
						if(ttt.equals(tttLoop)){
							ttt.setGiaTriChu(tttLoop.getGiaTriChu());
							ttt.setGiaTriNgay(tttLoop.getGiaTriNgay());
							ttt.setGiaTriSo(tttLoop.getGiaTriSo());
							ttt.setMaTruongThongTin(tttLoop.getMaTruongThongTin());
							truongThongTinChangedList.add(tttLoop);
						}
					}
				}
				ArrayList<EFormTruongThongTin> truongThongTinAdditionList = new ArrayList(truongThongTinCurrentList);
				truongThongTinAdditionList.removeAll(truongThongTinChangedList);
				truongThongTinList.addAll(truongThongTinAdditionList);
			}
		}
	}
	
	
	
	
	
	public static JSONObject getHoSoJson(EFormHoSo loop) {
		try {

			List<EFormTruongThongTin> eTruongThongTinList = loop.getTruongThongTinList();

			JSONObject jsonObj = new JSONObject();
			if (eTruongThongTinList != null) {
				Map<String, JSONObject> loopJson = new HashMap();
				for (EFormTruongThongTin eTruongThongtin : eTruongThongTinList) {
					Object value = null;
					if (eTruongThongtin.getGiaTriChu() != null) {
						value = eTruongThongtin.getGiaTriChu();
					} else if (eTruongThongtin.getGiaTriNgay() != null) {
						value = eTruongThongtin.getGiaTriNgay();
					} else if (eTruongThongtin.getGiaTriSo() != null) {
						value = eTruongThongtin.getGiaTriSo();
					}
					String key = eTruongThongtin.getMaTruongThongTin();
					if(eTruongThongtin.getThuTu() != null){
						JSONObject list = loopJson.get(eTruongThongtin.getThuTu()+"");
						if(list == null){
							list = new JSONObject();
							loopJson.put(eTruongThongtin.getThuTu()+"", list);
						}
						list.put(key, value);
					}else{
						jsonObj.put(key, value);
					}
					
				}
				if(!loopJson.isEmpty()){
					JSONArray arr = new JSONArray();
					for(Map.Entry<String, JSONObject> entry : loopJson.entrySet()){
						if(entry.getValue() != null){
							arr.put(entry.getValue());
						}
					}
					jsonObj.put("LOOP", arr);
				}
				List<EFormHoSo> nhomList = loop.getHoSoList();
				if (nhomList != null) {
					for (EFormHoSo nhom : nhomList) {
						if(nhom.getThuTu() != null){
							JSONArray jsonArr = null;
							if(!jsonObj.has(nhom.getMaBieuMau()) || jsonObj.get(nhom.getMaBieuMau()) == null){
								jsonArr = new JSONArray();
								jsonObj.put(nhom.getMaBieuMau(), jsonArr);
							}else{
								jsonArr = jsonObj.getJSONArray(nhom.getMaBieuMau());
							}
							jsonArr.put(getHoSoJson(nhom));
						}else{
							jsonObj.put(nhom.getMaBieuMau(), getHoSoJson(nhom));
						}
					}
				}

			}
			return jsonObj;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static void getDataLoop(EFormHoSo loop, Map<String, List<String>> loopPathMap, String currentPath) {
		try {

			List<EFormTruongThongTin> eTruongThongTinList = loop.getTruongThongTinList();
			if(currentPath == null){
				currentPath = "";
			}else{
				currentPath += loop.getMaBieuMau()+"#";
			}
			
			if (eTruongThongTinList != null) {
				List<String> thuTuList = new ArrayList();
				for (EFormTruongThongTin eTruongThongtin : eTruongThongTinList) {
					String key = eTruongThongtin.getMaTruongThongTin();
					if(eTruongThongtin.getThuTu() != null && !thuTuList.contains(currentPath+"#LOOP#"+eTruongThongtin.getThuTu())){
						thuTuList.add(currentPath+"#LOOP#"+eTruongThongtin.getThuTu());
//						thuTuList.add(currentPath+"#LOOP");
					}
				}
				if(!thuTuList.isEmpty()){
					loopPathMap.put(currentPath+"LOOP", thuTuList);
				}
			}
			List<EFormHoSo> nhomList = loop.getHoSoList();
			if (nhomList != null) {
				for (EFormHoSo nhom : nhomList) {
					if(nhom.getThuTu() != null){
						String loopPathMapTmp = currentPath + nhom.getMaBieuMau();
						List<String> list = loopPathMap.get(loopPathMapTmp);
						if(list == null){
							list = new ArrayList();
							loopPathMap.put(loopPathMapTmp, list);
						}
						if(!list.contains(loopPathMapTmp + "#"+ nhom.getThuTu())){
							list.add(loopPathMapTmp + "#"+ nhom.getThuTu());
//							list.add(loopPathMapTmp);
						}
						loopPathMap.put(loopPathMapTmp, list);
						getDataLoop(nhom, loopPathMap, currentPath);
					}else{
						getDataLoop(nhom, loopPathMap, currentPath);
					}
				}
			}

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void updateHoSoFromJson(Map<String, Object> valueMap, JSONObject jsonObj, BieuMau bmCurrent, String path, BieuMauService bieuMauService) {
		try {
			if(jsonObj != null){
				for(String key : jsonObj.keySet()){
					Object value = jsonObj.get(key);
					if(value instanceof JSONArray){
						JSONArray arr = (JSONArray) value;
						int stt = 1;
						for(int i = 0; i<arr.length(); i++){
							String pathTmp =path + "STT"+stt;
							stt++;
							JSONObject json = arr.getJSONObject(i);
							updateHoSoFromJson(valueMap, json, bmCurrent, pathTmp, bieuMauService);
						}
					}else if(value instanceof JSONObject){
						JSONObject json = (JSONObject) value;
						List<BieuMau> bmList= bieuMauService.getBieuMauFind(key, null, null, null, 0, 1);
						if(bmList!= null && !bmList.isEmpty()){
							BieuMau bm = bmList.get(0);
							String pathTmp =path+ bm.getId();
							updateHoSoFromJson(valueMap, json, bm, pathTmp, bieuMauService);
						}
					}else{
						List<TruongThongTin> tttList = bieuMauService.getTruongThongTinFind(key, null, null, null, null, 0, 1);
						if(tttList != null && !tttList.isEmpty()){
							TruongThongTin ttt = tttList.get(0);
							List<BmOTruongThongTin> oTruongThongTinList = bieuMauService.getBmOTruongThongTinFind(bmCurrent, null, null, ttt, 0, 1);
							if(oTruongThongTinList != null && !oTruongThongTinList.isEmpty()){
								BmOTruongThongTin o = oTruongThongTinList.get(0);
								String pathTmp = path+"O"+o.getId();
								if(valueMap.containsKey(pathTmp)){
									valueMap.put(pathTmp, value);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static EFormHoSo updateHoSoGiaTri(EFormHoSo hoSoOld, EFormHoSo hoSoNew, List<String> removeKeyList){
		try {
			if(hoSoOld != null && hoSoNew != null){
				Map<String, EFormHoSo> hoSoOldMap = new HashMap();
				Map<String, EFormHoSo> hoSoNewMap = new HashMap();
				
				
				
				
				List<EFormHoSo> process = new ArrayList<EFormHoSo>();
				process.add(hoSoOld);
				hoSoOld.setMaTemp("BMROOT");
				hoSoOld.setMaBieuMau("BMROOT");
				hoSoOldMap.put("BMROOT", hoSoOld);
				while(!process.isEmpty()){
					EFormHoSo current = process.remove(0);
					if (current.getHoSoList() != null) {
						List<EFormHoSo> removeHoSo = new ArrayList();
						List<EFormHoSo> hoSoDereaseIndex = new ArrayList();//giam thu tu ho so
	                    for (EFormHoSo child : current.getHoSoList()) {
	                    	String path = current.getMaTemp()+ "#" + (child.getThuTu() != null ? "STT" + child.getThuTu() :  "") + "BM" + child.getMaBieuMau();
	                    	child.setMaTemp(path);
	                    	process.add(child);
	                    	
	                    	String pathTmp = current.getMaTemp()+ "#" + (child.getThuTu() != null ? "STT" + child.getThuTu() :  "");
	                    	Pattern p = Pattern.compile(current.getMaTemp()+"#STT([0-9]+)");
	                    	if(removeKeyList != null && !removeKeyList.isEmpty()){
	                    		for(String key : removeKeyList){
	                    			Matcher m = p.matcher(key);
	                    			if(key.equals(pathTmp)){
	                    				removeHoSo.add(child);
	                    			}else{
	                    				hoSoOldMap.put(path, child);
	                    				if(child.getThuTu() != null && m.matches()){
		                    				Integer thuTu = new Integer(m.group(1));
		                    				if(child.getThuTu().intValue() > thuTu.intValue()){
		                    					hoSoDereaseIndex.add(child);
		                    				}
	                    				}
	                    			}
	                    		}
	                    	}else{
	                    		hoSoOldMap.put(path, child);
	                    	}
	                    	
	                    	
	                    }
	                    if(hoSoDereaseIndex != null){
	                    	for(EFormHoSo hs : hoSoDereaseIndex){
	                    		hs.setThuTu(hs.getThuTu() - 1);
	                    	}
	                    }
	                    current.getHoSoList().removeAll(removeHoSo);
	                    
	                    
					}
					if(current.getTruongThongTinList() != null){
                		List<EFormTruongThongTin> truongThongTinRemove = new ArrayList();
                		List<EFormTruongThongTin> truongTTDereaseIndex = new ArrayList();//giam thu tu truongThong tin
                		for(EFormTruongThongTin truongThongTin : current.getTruongThongTinList()){
                			String pathTmp = current.getMaTemp() + "#"+ (truongThongTin.getThuTu() != null ? "STT" + truongThongTin.getThuTu() :  "");
                			if(removeKeyList != null && removeKeyList.contains(pathTmp)){
                				truongThongTinRemove.add(truongThongTin);
                			}
                			Pattern p = Pattern.compile(current.getMaTemp()+"#STT([0-9]+)");
                			for(String key : removeKeyList){
                    			Matcher m = p.matcher(key);
                    			if(key.equals(pathTmp)){
                    				truongThongTinRemove.add(truongThongTin);
                    			}else{
                    				if(truongThongTin.getThuTu() != null && m.matches()){
	                    				Integer thuTu = new Integer(m.group(1));
	                    				if(truongThongTin.getThuTu().intValue() > thuTu.intValue()){
	                    					truongTTDereaseIndex.add(truongThongTin);
	                    				}
                    				}
                    			}
                    		}
                			
                			
                		}
                		if(truongTTDereaseIndex != null){
	                    	for(EFormTruongThongTin truongTT : truongTTDereaseIndex){
	                    		truongTT.setThuTu(truongTT.getThuTu() - 1);
	                    	}
	                    }
                		current.getTruongThongTinList().removeAll(truongThongTinRemove);
                	}
				}
				
				process.clear();
				process.add(hoSoNew);
				hoSoNew.setMaTemp("BMROOT");
				hoSoNew.setMaBieuMau("BMROOT");
				hoSoNewMap.put("BMROOT", hoSoNew);
				while(!process.isEmpty()){
					EFormHoSo current = process.remove(0);
					if (current.getHoSoList() != null) {
	                    for (EFormHoSo child : current.getHoSoList()) {
	                    	String path = current.getMaTemp() + "#"+(child.getThuTu() != null ? "STT" + child.getThuTu() :  "") + "BM" + child.getMaBieuMau();
	                    	child.setMaTemp(path);
	                    	process.add(child);
	                    	hoSoNewMap.put(path, child);
	                    }
					}
				}
				
				
				if(hoSoOldMap != null && hoSoNewMap != null){
					for(Entry<String, EFormHoSo> entry : hoSoNewMap.entrySet()){
						String key = entry.getKey();
						EFormHoSo hoSoItemOld = hoSoOldMap.get(key);
						EFormHoSo hoSoItemNew = entry.getValue();
						if(hoSoItemOld != null && hoSoItemNew != null){
							//update data
							if(hoSoItemOld.getTruongThongTinList() != null){
								if(hoSoItemNew.getTruongThongTinList() != null){
									hoSoItemOld.getTruongThongTinList().removeAll(hoSoItemNew.getTruongThongTinList());
									hoSoItemOld.getTruongThongTinList().addAll(hoSoItemNew.getTruongThongTinList());
								}
							}else{
								hoSoItemOld.setTruongThongTinList(hoSoItemNew.getTruongThongTinList());
							}
						}else if(hoSoItemNew != null){
							if(key.lastIndexOf("#") > 0){
								String keyTmp = key.substring(0, key.lastIndexOf("#"));
								EFormHoSo hoSoItemOldTmp = hoSoOldMap.get(keyTmp);
								if(hoSoItemOldTmp != null){
									ArrayList<EFormHoSo> list = hoSoItemOldTmp.getHoSoList();
									if(list == null){
										list = new ArrayList();
										hoSoItemOldTmp.setHoSoList(list);
									}
									list.add(hoSoItemNew);
								}
							}
						}
					}
				}
				return hoSoOld;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static EFormBoHoSo getBoHoSo(String hoSoGiaTri){
        try {
            if(hoSoGiaTri == null){
                return null;
            }
            JAXBContext jaxbContext =
                JAXBContext.newInstance(EFormBoHoSo.class);
            Unmarshaller jaxbUnmarshaller =
                jaxbContext.createUnmarshaller();
            ByteArrayInputStream inputStream =
                new ByteArrayInputStream(hoSoGiaTri.getBytes("UTF-8"));
            return (EFormBoHoSo)jaxbUnmarshaller.unmarshal(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
	
	public static Map<String, EFormTruongThongTin> getHoSoValueMap(String hoSoGiaTri){
		EFormBoHoSo boHoSo = getBoHoSo(hoSoGiaTri);
        Map<String, EFormTruongThongTin> valueMap = new HashMap();
        if(boHoSo == null){
            return valueMap;
        }
        EFormHoSo hoSoRoot = boHoSo.getHoSo();
        if (hoSoRoot != null) {
        	hoSoRoot.setMaTemp("ROOT");
            List<EFormHoSo> list = new ArrayList();
            list.add(hoSoRoot);
            while(!list.isEmpty()){
                EFormHoSo current = list.remove(0);
                List<EFormTruongThongTin> truongThongTinList = current.getTruongThongTinList();
                
                if(truongThongTinList != null){
                    for(EFormTruongThongTin truongThongTin : truongThongTinList){
                        String key = current.getMaTemp() +"_"+ (truongThongTin.getThuTu() != null ? "#tt"+truongThongTin.getThuTu()+"_" : "")+truongThongTin.getMaTruongThongTin();
                        valueMap.put(key, truongThongTin);
                    }
                }
                if(current.getHoSoList() != null){
                	for(EFormHoSo hs : current.getHoSoList()){
                		String key = current.getMaTemp() + "_" + (hs.getThuTu() != null ? "#tt" + hs.getThuTu() +"_" : "") + hs.getMaBieuMau();
                		hs.setMaTemp(key);
                	}
                    list.addAll(current.getHoSoList());
                }
            }
        }
        return valueMap;
    }
    
    public static List<Map<String, Object>> getValueByKey(String key, boolean coThuTu, Map<String, ? extends Object> hoSoValueMap){
    	List<Map<String, Object>> result = new ArrayList();
    	Map<String, Map<String, Object>> resultMap = new HashMap();

    	String keyExp = key;
    	if(coThuTu){
    		String sttEpx = "(#tt([0-9]+))";
    		if(keyExp != null && !keyExp.isEmpty()){
    			keyExp = sttEpx + "_" + keyExp;
    		}else{
    			keyExp = sttEpx;
    		}
    	}
    	keyExp += "(_(.*))*";
		Pattern p = Pattern.compile(keyExp);
		//System.out.println("---------------------------------");
		int g = coThuTu ? 4 : 2;
    	for(Entry<String, ? extends Object> entry : hoSoValueMap.entrySet()){
    		String k = entry.getKey();
    		Matcher m = p.matcher(k);
    		Object v = entry.getValue();
			if(m.matches()){
				String keyTmp = m.group(g) != null ? m.group(g) : key;
				String thuTu = coThuTu ? m.group(2) : "0";
				Map<String, Object> map = resultMap.get(thuTu);
				if(map == null){
					map = new HashMap();
					map.put("#STT", thuTu);
					resultMap.put(thuTu, map);
					result.add(map);
				}
				map.put(keyTmp, v);
				//System.out.println(keyTmp);
			}
    	}
    	return result;
    }
    
    public static Map<String, Map<String, Object>> getValueMapByKey(String key, boolean coThuTu, Map<String, ? extends Object> hoSoValueMap){
    	Map<String, Map<String, Object>> resultMap = new HashMap();
    	String keyExp = key;
    	if(coThuTu){
    		String sttEpx = "(#tt([0-9]+))";
    		if(keyExp != null && !keyExp.isEmpty()){
    			keyExp = sttEpx + "_" + keyExp;
    		}else{
    			keyExp = sttEpx;
    		}
    	}
    	keyExp += "(_(.*))*";
		Pattern p = Pattern.compile(keyExp);
		//System.out.println("---------------------------------");
		int g = coThuTu ? 4 : 2;
    	for(Entry<String, ? extends Object> entry : hoSoValueMap.entrySet()){
    		String k = entry.getKey();
    		Matcher m = p.matcher(k);
    		Object v = entry.getValue();
			if(m.matches()){
				String keyTmp = m.group(g) != null ? m.group(g) : key;
				String thuTu = coThuTu ? m.group(2) : "0";
				Map<String, Object> map = resultMap.get(thuTu);
				if(map == null){
					map = new HashMap();
					map.put("#STT", thuTu);
					resultMap.put(thuTu, map);
				}
				map.put(keyTmp, v);
				//System.out.println(keyTmp);
			}
    	}
    	return resultMap;
    }
}

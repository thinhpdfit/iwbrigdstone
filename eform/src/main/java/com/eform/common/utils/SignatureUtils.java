package com.eform.common.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.io.Resource;

import com.eform.common.Constants;
import com.eform.common.SpringApplicationContext;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.PrivateKeySignature;

public class SignatureUtils {

	public static void signPdf(String src, String dest, String userName, int version)
	        throws IOException, DocumentException, GeneralSecurityException {
	        String keystore_password = "123456";
	        String key_password = "123456";
	        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	        KeyStore ks = KeyStore.getInstance("pkcs12", "BC");
	        
	        Resource resource = SpringApplicationContext.getApplicationContext().getResource("classpath:private/test.p12");
	        InputStream is = resource.getInputStream();

	        ks.load(is, keystore_password.toCharArray());
	        String alias = (String)ks.aliases().nextElement();
	        PrivateKey pk = (PrivateKey) ks.getKey(alias, key_password.toCharArray());
	        Certificate[] chain = ks.getCertificateChain(alias);
	        // reader and stamper
	        PdfReader reader = new PdfReader(src);
	        FileOutputStream os = new FileOutputStream(dest);
	        PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0');
	        // appearance
	        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
//	        appearance.setImage(Image.getInstance(RESOURCE));
	        Date date = new Date();
	        SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
	        appearance.setLayer2Text(StringUtils.convertUTF8ToNoSign(userName)+"\n"+df.format(date));
	        appearance.setReason(userName);
	        appearance.setReasonCaption("Signees: ");
//	        appearance.setLocation("Foobar");
	        
	        int lastPage = reader.getNumberOfPages();
	        int h = 60;//chieu cao chu ky
	        int w = 150;//chieu rong chu ky
	        int n = 3;//so chu ky tren 1 dong
	        
	        int r = (int) Math.ceil((double)version/(double)n);
	        
//	        int lx = 70+w*(version - r*n - 1);
//	        int ly = 650-h*r;
//	        int ux = 70+w+w*(version- r*n - 1);
//	        int uy = 710-h*r;
	        
	        int lx = 70+w*(version - n*(r-1) - 1);
	        int ly = 650-h*r;
	        int ux = lx + w;
	        int uy = ly + 60;
	        
	        appearance.setVisibleSignature(new Rectangle(lx, ly, ux, uy), lastPage, "version_"+version);
	        // digital signature
	        ExternalSignature es = new PrivateKeySignature(pk, "SHA-256", "BC");
	        ExternalDigest digest = new com.itextpdf.text.pdf.security.BouncyCastleDigest();
	        MakeSignature.signDetached(appearance, digest, es, chain, null, null, null, 0, CryptoStandard.CMS);
	    }
}

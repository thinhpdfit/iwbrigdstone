package com.eform.common.utils;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.utils.CommonUtils.EAction;
import com.eform.ui.view.process.ProcessDefinitionListView;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.themes.Reindeer;

public class ProcessDefinitionUtils {
	public static Component buildItem(ProcessDefinition proDef, int i, ClickListener click, String [] colors, RepositoryService repositoryService, Map<Long, Boolean> mapQuyen) {
		if(proDef != null){
			
			String nameStr = "#Không tên";
			if(proDef.getName() != null && !proDef.getName().isEmpty()){
				nameStr = proDef.getName();
			}
			
			String iconStr = "#";
			if(nameStr.length() > 0){
				iconStr =nameStr.substring(0, 1);
			}
			
			CssLayout item = new CssLayout();
			item.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM);
			
			
			CssLayout itemInfo = new CssLayout();
			itemInfo.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_INFO);
			item.addComponent(itemInfo);
			
			Label statusLbl = new Label();
			statusLbl.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_STATUS);
			if(proDef.isSuspended()){
				statusLbl.setDescription("Ngừng sử dụng");
				statusLbl.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_STATUS_SUSPEND);
			}else{
				statusLbl.setDescription("Đang sử dụng");
				statusLbl.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_STATUS_USING);
			}
			itemInfo.addComponent(statusLbl);
			
			CssLayout imageWrap = new CssLayout();
			imageWrap.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_IMAGE_WRAP);
			itemInfo.addComponent(imageWrap);
			
			Image img = getProcessImage(proDef, repositoryService);
			if(img != null){
				imageWrap.addComponent(img);
			}
			
			
			CssLayout descWrap = new CssLayout();
			descWrap.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_DESC);
			itemInfo.addComponent(descWrap);
			
			String desc = "#Không có mô tả";
			if(proDef.getDescription() != null && !proDef.getDescription().isEmpty()){
				desc = proDef.getDescription();
			}
			
			Label lblName = new Label(nameStr);
			lblName.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_NAME_LBL_D);
			descWrap.addComponent(lblName);
			
			Label lblDesc = new Label(desc);
			lblDesc.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_DESC_LBL);
			descWrap.addComponent(lblDesc);
			
			Button btnView = new Button();
			btnView.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_BTN_VIEW);
			btnView.setId(ProcessDefinitionListView.BTN_VIEW_ID);
			btnView.addClickListener(click);
			descWrap.addComponent(btnView);
			btnView.setData(proDef);
			
			CssLayout itemNameWrap = new CssLayout();
			itemNameWrap.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_NAME_WRAP);
			item.addComponent(itemNameWrap);
			
			CssLayout itemShortName= new CssLayout();
			itemShortName.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_SHORT_NAME);
			itemNameWrap.addComponent(itemShortName);
			
			String color = "#94D6FF";//mac dinh
			if(colors != null && colors.length >0){
				color = colors[iconStr.toCharArray()[0] % colors.length];
			}
			
			Label shortName = new Label();
			shortName.setValue("<span style='background:"+color+"'>"+iconStr+"</span>");
			shortName.setContentMode(ContentMode.HTML);
			shortName.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_SHORT_NAME_LBL);
			itemShortName.addComponent(shortName);
			
			CssLayout itemName= new CssLayout();
			itemName.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_NAME);
			itemNameWrap.addComponent(itemName);
			
			Label name = new Label(nameStr);
			name.setDescription(nameStr);
			name.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_NAME_LBL);
			itemName.addComponent(name);
			
			if(proDef.getDeploymentId() != null){
				Deployment dep = repositoryService.createDeploymentQuery().deploymentId(proDef.getDeploymentId()).singleResult();
				if(dep != null && dep.getDeploymentTime() != null){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
					String time = df.format(dep.getDeploymentTime());
					Label publishDate = new Label(time);
					publishDate.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_PUB_DATE);
					itemName.addComponent(publishDate);
				}
			}
			
			CssLayout actionWrap = getActionWrap(proDef, click, mapQuyen);
			if(actionWrap != null){
				itemNameWrap.addComponent(actionWrap);
			}
			
			return item;
		}
		return null;
	}
	
	public static CssLayout getActionWrap(ProcessDefinition process, ClickListener click,  Map<Long, Boolean> mapQuyen){
		CssLayout actionWrap = new CssLayout();
		actionWrap.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_BUTTONS);
		

        if(process != null && !process.isSuspended()){
        	if(mapQuyen != null && mapQuyen.get(new Long(EAction.START.getCode())) != null && mapQuyen.get(new Long(EAction.START.getCode()))){
        		Button btnStart = new Button();
    	        btnStart.setId(ProcessDefinitionListView.BTN_START_ID);
    	        btnStart.setDescription("Khởi tạo quy trình");
    	        btnStart.setIcon(FontAwesome.PLAY);
    	        btnStart.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_BUTTON);
    	        btnStart.addClickListener(click);
    	        btnStart.setData(process);
    	        btnStart.addStyleName(Reindeer.BUTTON_LINK);
    	        actionWrap.addComponent(btnStart);
        	}
        	
        	if(mapQuyen != null && mapQuyen.get(new Long(EAction.PHE_DUYET.getCode())) != null && mapQuyen.get(new Long(EAction.PHE_DUYET.getCode()))){
        		 Button btnSuspend = new Button();
 		        btnSuspend.setDescription("Ngừng sử dụng");
 		        btnSuspend.setId(ProcessDefinitionListView.BTN_REFUSE_ID);
 		        btnSuspend.setIcon(FontAwesome.LEVEL_DOWN);
 		        btnSuspend.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_BUTTON);
 		        btnSuspend.addClickListener(click);
 		        btnSuspend.setData(process);
 		        btnSuspend.addStyleName(Reindeer.BUTTON_LINK);
 		        actionWrap.addComponent(btnSuspend);
 		        
 		        Button btnEdit = new Button();
 		        btnEdit.setDescription("Sửa thông tin");
 		        btnEdit.setId(ProcessDefinitionListView.BTN_EDIT_ID);
 		        btnEdit.setIcon(FontAwesome.PENCIL);
 		       	btnEdit.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_BUTTON);
 		       	btnEdit.addClickListener(click);
 		       	btnEdit.setData(process);
 		       	btnEdit.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnEdit);
        	}
		}else{
			if(mapQuyen != null && mapQuyen.get(new Long(EAction.PHE_DUYET.getCode())) != null && mapQuyen.get(new Long(EAction.PHE_DUYET.getCode()))){
				Button btnActive = new Button();
				btnActive.setDescription("Sử dụng");
		        btnActive.setId(ProcessDefinitionListView.BTN_APPROVE_ID);
		        btnActive.setIcon(FontAwesome.CHECK);
		        btnActive.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_BUTTON);
		        btnActive.addClickListener(click);
		        btnActive.setData(process);
		        btnActive.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnActive);
			}
			if(mapQuyen != null && mapQuyen.get(new Long(EAction.XOA.getCode())) != null && mapQuyen.get(new Long(EAction.XOA.getCode()))){
		        Button btnDelete = new Button();
		        btnDelete.setDescription("Xóa");
		        btnDelete.setId(ProcessDefinitionListView.BTN_DELETE_ID);
		        btnDelete.setIcon(FontAwesome.REMOVE);
		        btnDelete.addStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_BUTTON);
		        btnDelete.addClickListener(click);
		        btnDelete.setData(process);
		        btnDelete.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnDelete);
			}
		}
        
       
		return actionWrap;
	}
	
	
	public static Image getProcessImage(ProcessDefinition item, RepositoryService repositoryService){
		try {
			Image image = new Image();
			image.setStyleName(ExplorerLayout.PROCESS_DEFINITION_ITEM_IMAGE);
			ProcessDefinitionEntity pde = (ProcessDefinitionEntity) repositoryService.createProcessDefinitionQuery().processDefinitionId(item.getId()).singleResult();
			repositoryService.getResourceAsStream(pde.getDeploymentId(), pde.getDiagramResourceName());
			InputStream is = repositoryService.getResourceAsStream(pde.getDeploymentId(), pde.getDiagramResourceName());
	        final StreamResource streamResource = new StreamResource(
	                new StreamSource() {
						private static final long serialVersionUID = 2954259443667100494L;
						@Override
	                    public InputStream getStream() {
	                        return is;
	                    }
	                }, item.getDiagramResourceName());
	        streamResource.setCacheTime(0);
	    	image.setSource(streamResource);
	    	return image;
		} catch (Exception e) {
			System.err.println("Diagram resource not found!");
		}
		return null;
	}
}

package com.eform.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;

import com.eform.common.ExplorerLayout;
import com.eform.common.SpringApplicationContext;
import com.eform.common.utils.CommonUtils.EAction;
import com.eform.ui.view.process.ModelerListView;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.colorpicker.Color;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.themes.Reindeer;

public class ModelerUtils implements ModelDataJsonConstants{
	public static Component buildItem(Model model, int i, ClickListener click, RepositoryService repositoryService, String [] colors, Map<Long, Boolean> mapQuyen) {
		if(model != null){
			
			String nameStr = "#Không tên";
			if(model.getName() != null && !model.getName().isEmpty()){
				nameStr = model.getName();
			}
			
			String iconStr = "#";
			if(nameStr.length() > 0){
				iconStr =nameStr.substring(0, 1);
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
	        
			String descStr = "#Không có mô tả";
			String color = "#94D6FF";//mac dinh
			try {
				JsonNode modelObjectNode = objectMapper.readTree(model.getMetaInfo());
				if(modelObjectNode.get("description")!= null){
					String descTmp = modelObjectNode.get("description").textValue();
					if(descTmp != null && !descTmp.isEmpty()){
						descStr = descTmp;
					}
				}
				if(modelObjectNode.get("color")!= null){
					Color cl = new Color(modelObjectNode.get("color").intValue());
					color = cl.getCSS();
				}else{
					if(colors != null && colors.length >0){
						color = colors[iconStr.toCharArray()[0] % colors.length];
					}
				}
				
				if(modelObjectNode.get("icon")!= null){
					try {
						FontAwesome f = FontAwesome.fromCodepoint(modelObjectNode.get("icon").intValue());
						if(f != null){
							iconStr = f.getHtml();
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			CssLayout item = new CssLayout();
			item.addStyleName(ExplorerLayout.APP_ITEM);
			item.addStyleName(ExplorerLayout.ANIMATE_BOUNCE);
			
			CssLayout iconWrap = new CssLayout();
			iconWrap.setWidth("100%");
			iconWrap.addStyleName(ExplorerLayout.APP_ITEM_ICON_WRAP);

			item.addComponent(iconWrap);
			
			
			
			
			
			Label icon = new Label();
			icon.addStyleName(ExplorerLayout.APP_ITEM_ICON);
			icon.setValue("<span style='background:"+color+"'>"+iconStr+"</span>");
			icon.setContentMode(ContentMode.HTML);
			iconWrap.addComponent(icon);
			
			
			
			CssLayout descWrap = new CssLayout();
			descWrap.setWidth("100%");
			descWrap.addStyleName(ExplorerLayout.APP_ITEM_DESC_WRAP);
			iconWrap.addComponent(descWrap);
			
			if(mapQuyen != null && mapQuyen.get(new Long(EAction.SUA.getCode())) != null && mapQuyen.get(new Long(EAction.SUA.getCode()))){
				Button btnEditModeler = new Button();
				btnEditModeler.addStyleName(ExplorerLayout.BTN_EDIT_MODELER);
				btnEditModeler.setId(ModelerListView.BTN_EDIT_MODEL_ID);
				btnEditModeler.addClickListener(click);
				descWrap.addComponent(btnEditModeler);
				btnEditModeler.setData(model);
			}
			
			
			Label desc = new Label(descStr);
			desc.addStyleName(ExplorerLayout.APP_ITEM_DESC);
			desc.setContentMode(ContentMode.HTML);
			descWrap.addComponent(desc);
			
			CssLayout infoWrap = new CssLayout();
			infoWrap.setWidth("100%");
			infoWrap.addStyleName(ExplorerLayout.APP_ITEM_INFO_WRAP);
			item.addComponent(infoWrap);
			
			Label name = new Label(nameStr);
			name.addStyleName(ExplorerLayout.APP_ITEM_NAME);
			infoWrap.addComponent(name);
			
			
			CssLayout actionWrap = new CssLayout();
			actionWrap.setWidth("100%");
			actionWrap.addStyleName(ExplorerLayout.APP_ITEM_ACTION_WRAP);
			
			
			infoWrap.addComponent(actionWrap);
			
			actionWrap.addComponent(getActionWrap(model, click, mapQuyen, repositoryService));
			
			return item;
		}
		return null;
	}
	
	public static Component getActionWrap(Model itemId, ClickListener click, Map<Long, Boolean> mapQuyen, RepositoryService repositoryService){
		HorizontalLayout actionWrap = new HorizontalLayout();
		actionWrap.addStyleName(ExplorerLayout.APP_ITEM_ACTION_WRAP);
        
		if(mapQuyen != null && mapQuyen.get(new Long(EAction.SUA.getCode())) != null && mapQuyen.get(new Long(EAction.SUA.getCode()))){
	        Button btnEditModel = new Button();
	        btnEditModel.setDescription("Sửa");
	        btnEditModel.setId(ModelerListView.BTN_EDIT_ID);
	        btnEditModel.setIcon(FontAwesome.PENCIL_SQUARE_O);
	        btnEditModel.addStyleName(ExplorerLayout.APP_ITEM_ACTION);
	        btnEditModel.addClickListener(click);
	        btnEditModel.setData(itemId);
	        btnEditModel.addStyleName(Reindeer.BUTTON_LINK);
	        actionWrap.addComponent(btnEditModel);
	        
	        Button btnExport = new Button();
	        btnExport.setDescription("Export");
	        btnExport.setId(ModelerListView.BTN_EXPORT_ID);
	        btnExport.setIcon(FontAwesome.CLOUD_DOWNLOAD);
	        btnExport.addStyleName(ExplorerLayout.APP_ITEM_ACTION);
	        btnExport.addClickListener(click);
	        btnExport.setData(itemId);
	        btnExport.addStyleName(Reindeer.BUTTON_LINK);
	        actionWrap.addComponent(btnExport);
	        
//	        Button btnExportFull = new Button();
//	        btnExportFull.setDescription("Export Full");
//	        btnExportFull.setId(ModelerListView.BTN_EXPORT_ALL_ID);
//	        btnExportFull.setIcon(FontAwesome.TH_LARGE);
//	        btnExportFull.addStyleName(ExplorerLayout.APP_ITEM_ACTION);
//	        btnExportFull.addClickListener(click);
//	        btnExportFull.setData(itemId);
//	        btnExportFull.addStyleName(Reindeer.BUTTON_LINK);
//	        actionWrap.addComponent(btnExportFull);
	       
	        exportModel(itemId.getId(), btnExport, repositoryService);
		}
        
		if(mapQuyen != null && mapQuyen.get(new Long(EAction.XOA.getCode())) != null && mapQuyen.get(new Long(EAction.XOA.getCode()))){
	        Button btnDel = new Button();
	        btnDel.setDescription("Xóa");
	    	btnDel.setId(ModelerListView.BTN_DEL_ID);
	    	btnDel.addStyleName(ExplorerLayout.APP_ITEM_ACTION);
	    	btnDel.addStyleName(Reindeer.BUTTON_LINK);
	    	btnDel.setData(itemId);
	    	btnDel.setIcon(FontAwesome.REMOVE);
	    	btnDel.addClickListener(click);
	        actionWrap.addComponent(btnDel);
		}
        
		if(mapQuyen != null && mapQuyen.get(new Long(EAction.DEPLOY.getCode())) != null && mapQuyen.get(new Long(EAction.DEPLOY.getCode()))){
	        Button btnDeploy = new Button();
	        btnDeploy.setDescription("Deploy");
	        btnDeploy.setId(ModelerListView.BTN_DEPLOY_ID);
	        btnDeploy.addStyleName(ExplorerLayout.APP_ITEM_ACTION);
	        btnDeploy.addStyleName(Reindeer.BUTTON_LINK);
	        btnDeploy.setData(itemId);
	        btnDeploy.setIcon(FontAwesome.CHECK);
	        btnDeploy.addClickListener(click);
	        actionWrap.addComponent(btnDeploy);
		}

       
        return actionWrap;
	}
	
	
	protected static void exportModel(String modelDataId, Button btn, RepositoryService repositoryService) {
		try {
			byte[] bpmnBytes = null;
			String filename = null;
			JsonNode editorNode = new ObjectMapper().readTree(repositoryService.getModelEditorSource(modelDataId));
			BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
			BpmnModel model = jsonConverter.convertToBpmnModel(editorNode);
			if(model != null && model.getMainProcess() != null){
				filename = model.getMainProcess().getId() + ".bpmn20.xml";
				bpmnBytes = new BpmnXMLConverter().convertToXML(model);
				ByteArrayInputStream in = new ByteArrayInputStream(bpmnBytes);
				FileUtils.downloadWithButton(btn, in, filename);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static boolean deployUploadedXml(String content) throws Exception {
		byte[] b = content.getBytes();
		RepositoryService repositoryService = SpringApplicationContext.getApplicationContext().getBean(RepositoryService.class);
		XMLInputFactory xif = XmlUtil.createSafeXmlInputFactory();
		InputStreamReader in = new InputStreamReader(new ByteArrayInputStream(b), "UTF-8");
		XMLStreamReader xtr = xif.createXMLStreamReader(in);
		BpmnModel bpmnModel = new BpmnXMLConverter().convertToBpmnModel(xtr);

		if (bpmnModel.getMainProcess() != null && bpmnModel.getMainProcess().getId() != null && !bpmnModel.getLocationMap().isEmpty()) {
			String processName = null;
			if (StringUtils.isNotEmpty(bpmnModel.getMainProcess().getName())) {
				processName = bpmnModel.getMainProcess().getName();
			} else {
				processName = bpmnModel.getMainProcess().getId();
			}

			Model modelData = repositoryService.newModel();
			ObjectNode modelObjectNode = new ObjectMapper().createObjectNode();
			modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME, processName);
			modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, 1);
			modelData.setMetaInfo(modelObjectNode.toString());
			modelData.setName(processName);

			repositoryService.saveModel(modelData);

			BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
			ObjectNode editorNode = jsonConverter.convertToJson(bpmnModel);

			repositoryService.addModelEditorSource(modelData.getId(), editorNode.toString().getBytes("utf-8"));
			return true;
		}
		return false;
	}
	
	public static boolean deployUploadedJson(String content) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode modelJson = (ObjectNode) objectMapper.readTree(content);
		
		if(modelJson != null
				&& modelJson.get(ModelDataJsonConstants.MODEL_ID) != null
				&& modelJson.get(ModelDataJsonConstants.MODEL_NAME) != null
				&& modelJson.get("model") != null){
			RepositoryService repositoryService = SpringApplicationContext.getApplicationContext().getBean(RepositoryService.class);

			String modelName = modelJson.get(ModelDataJsonConstants.MODEL_NAME).asText();
			
			Model modelData = repositoryService.newModel();
			ObjectNode modelObjectNode = new ObjectMapper().createObjectNode();
			modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME, modelName);
			modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, 1);
			modelData.setMetaInfo(modelObjectNode.toString());
			modelData.setName(modelName);
			
			repositoryService.saveModel(modelData);
			
			JsonNode editorNode = modelJson.get("model");
			repositoryService.addModelEditorSource(modelData.getId(), editorNode.toString().getBytes("utf-8"));
			return true;
		}
		return false;
	}
	
	public static ObjectNode getEditorJson(String modelId) {
		ObjectMapper objectMapper = new ObjectMapper();
		RepositoryService repositoryService = SpringApplicationContext.getApplicationContext().getBean(RepositoryService.class);
		
		ObjectNode modelNode = null;
	    
	    Model model = repositoryService.getModel(modelId);
	      
	    if (model != null) {
	      try {
	        if (StringUtils.isNotEmpty(model.getMetaInfo())) {
	          modelNode = (ObjectNode) objectMapper.readTree(model.getMetaInfo());
	        } else {
	          modelNode = objectMapper.createObjectNode();
	          modelNode.put(MODEL_NAME, model.getName());
	        }
	        modelNode.put(MODEL_ID, model.getId());
	        ObjectNode editorJsonNode = (ObjectNode) objectMapper.readTree(
	            new String(repositoryService.getModelEditorSource(model.getId()), "utf-8"));
	        modelNode.put("model", editorJsonNode);
	      } catch (Exception e) {
	        e.printStackTrace();
	      }
	    }
	    return modelNode;
	  }
	
}

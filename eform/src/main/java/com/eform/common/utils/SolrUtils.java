package com.eform.common.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

public class SolrUtils {
	
	public static String getPdfFileContent(FileInputStream pdfFis){
		try {
			PdfReader pdfReader = new PdfReader(pdfFis);
			PdfReaderContentParser pdfParser = new PdfReaderContentParser(pdfReader);
			StringBuilder stringBuilder = new StringBuilder();
			TextExtractionStrategy strategy;
			for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
				strategy = pdfParser.processContent(i, new SimpleTextExtractionStrategy());
				stringBuilder.append(strategy.getResultantText());
			}
			return stringBuilder.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getDocFileContent(FileInputStream docFis){
		HWPFDocument hwpfDocument = null;
		WordExtractor wordExtractor = null;
		try {
			hwpfDocument = new HWPFDocument(docFis);
			wordExtractor = new WordExtractor(hwpfDocument);
			return wordExtractor.getText();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(hwpfDocument != null){
					hwpfDocument.close();
				}
				if(hwpfDocument != null){
					hwpfDocument.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	public static String getDocxFileContent(FileInputStream docxFis){
		XWPFDocument xwpfDocument = null;
		XWPFWordExtractor xwpfWordExtractor = null;
		try {
			xwpfDocument = new XWPFDocument(docxFis);
			xwpfWordExtractor = new XWPFWordExtractor(xwpfDocument);
			String docxContent = xwpfWordExtractor.getText();
			return xwpfWordExtractor.getText();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(xwpfDocument != null){
					xwpfDocument.close();
				}
				if(xwpfWordExtractor != null){
					xwpfWordExtractor.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	public static String getFileContent(FileInputStream input, String ext){
		if("doc".equalsIgnoreCase(ext)){
			return getDocFileContent(input);
		}else if("docx".equalsIgnoreCase(ext)){
			return getDocxFileContent(input);
		}else if("pdf".equalsIgnoreCase(ext)){
			return getPdfFileContent(input);
		}
		return null;
	}
	
	public static void updateSolr(Object id, String content, String urlString) throws SolrServerException, IOException {
		
		SolrClient solr = new HttpSolrClient.Builder(urlString).build();
		SolrInputDocument docxSolrDocument = new SolrInputDocument();
		docxSolrDocument.addField("id", id);
		docxSolrDocument.addField("content", content);
		UpdateResponse response = solr.add(docxSolrDocument);
		solr.commit();
	}
	
	public static void deleteFromSolr(String id, String urlString) throws SolrServerException, IOException{
		SolrClient solr = new HttpSolrClient.Builder(urlString).build();
		solr.deleteById(id);
		solr.commit();
	}
	
	public static List<Object> search(String search, String urlString) throws SolrServerException, IOException {

		SolrClient solr = new HttpSolrClient.Builder(urlString).build();
		SolrQuery query = new SolrQuery();
		query.set("q", "content:\""+search+"\"");
		QueryResponse response = solr.query(query);
		SolrDocumentList list = response.getResults();
		List<Object> results = new ArrayList();
		for (SolrDocument document : list) {
			results.add(document.getFieldValue("id"));
		}
		return results;
	}
	
}

package com.eform.common.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.User;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskInfo;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.SpringApplicationContext;
import com.eform.common.type.EFormTruongThongTin;
import com.eform.model.entities.CauHinhHienThi;
import com.eform.model.entities.HoSo;
import com.eform.model.entities.ProcinstInfo;
import com.eform.service.CauHinhHienThiService;
import com.eform.service.HoSoService;
import com.eform.service.ProcinstInfoService;
import com.eform.ui.view.task.TaskListView;
import com.google.api.client.util.Lists;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.themes.Reindeer;

public class TaskUtils {

	public static Component buildItem(HistoricTaskInstance task, int i, ClickListener click, RepositoryService repositoryService, IdentityService identityService) {
		CssLayout item = new CssLayout();
		item.setStyleName(ExplorerLayout.TASK_ITEM);
		item.setWidth("100%");
		
		CssLayout imgWrap = new CssLayout();
		imgWrap.setStyleName(ExplorerLayout.TASK_IMAGE_WRAP);
		Label lbl = new Label(i+"");
		lbl.setStyleName(ExplorerLayout.TASK_IMAGE);
		imgWrap.addComponent(lbl);
		
		Label priority = new Label();
		priority.addStyleName(ExplorerLayout.TASK_PRIORITY_STAR);
		CommonUtils.EPriority pri = CommonUtils.EPriority.getPriority(task.getPriority());
		priority.setDescription("Mức độ ưu tiên "+ pri.getName().toLowerCase());
		priority.addStyleName(pri.getStyleName());
		if(pri.equals(CommonUtils.EPriority.LOW)){
			priority.setValue(FontAwesome.STAR_O.getHtml());
		}else{
			priority.setValue(FontAwesome.STAR.getHtml());
		}
		priority.setContentMode(ContentMode.HTML);

		imgWrap.addComponent(priority);
		item.addComponent(imgWrap);
		
		CssLayout infoWrap = new CssLayout();
		infoWrap.setStyleName(ExplorerLayout.TASK_INFO_WRAP);
		item.addComponent(infoWrap);
		
		CssLayout taskNameWrap = new CssLayout();
		taskNameWrap.setStyleName(ExplorerLayout.TASK_NAME_WRAP);
		infoWrap.addComponent(taskNameWrap);
		String nameStr = "#Không tên";
		if(task.getName() != null && !task.getName().isEmpty()){
			nameStr = task.getName();
		}
		Button name = new Button(nameStr);
		name.addStyleName(Reindeer.BUTTON_LINK);
		name.addStyleName(ExplorerLayout.TASK_NAME);
		name.setId(TaskListView.BTN_DETAIL_ID);
		name.setData(task);
		name.addClickListener(click);
		taskNameWrap.addComponent(name);

		if(task.getProcessDefinitionId() != null){
			ProcessDefinition pro = repositoryService.createProcessDefinitionQuery().processDefinitionId(task.getProcessDefinitionId()).singleResult();
			Label proIns = new Label(pro != null ? pro.getName() : "");
			proIns.setStyleName(ExplorerLayout.TASK_PROC_INS);
			taskNameWrap.addComponent(proIns);
		}
		
		SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
		String time = "";
		if(task.getCreateTime() != null){
			time = df.format(task.getCreateTime());
		}
		
		Label createTime = new Label(time);
		createTime.setStyleName(ExplorerLayout.TASK_CREATE_TIME);
		taskNameWrap.addComponent(createTime);
		
		CssLayout taskMoreInfoWrap = new CssLayout();
		taskMoreInfoWrap.setStyleName(ExplorerLayout.TASK_MORE_INFO_WRAP);
		infoWrap.addComponent(taskMoreInfoWrap);
		
		SimpleDateFormat df1 = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
		String endTime = "";
		if(task.getEndTime() != null){
			endTime = "Hoàn thành ngày "+df1.format(task.getEndTime());
		}else{
			endTime = "#Không có thời gian hoàn thành";
		}
		
		if(task.getAssignee() != null){
			User assignee = identityService.createUserQuery().userId(task.getAssignee()).singleResult();
			if(assignee != null){
				endTime += " bởi <span class='his-task-assignee'>"+assignee.getFirstName()+" "+assignee.getLastName()+"</span>";
			}
		}
		Label duedate = new Label(endTime);
		duedate.setContentMode(ContentMode.HTML);
		duedate.setIcon(FontAwesome.CLOCK_O);
		duedate.setStyleName(ExplorerLayout.TASK_DUEDATE);
		taskMoreInfoWrap.addComponent(duedate);
		
		CssLayout taskDescWrap = new CssLayout();
		taskDescWrap.setStyleName(ExplorerLayout.TASK_DESC_WRAP);
		infoWrap.addComponent(taskDescWrap);
		
		Label desc = new Label();
		desc.setStyleName(ExplorerLayout.TASK_ITEM_DESC);
		desc.setContentMode(ContentMode.HTML);
		String descContent = task.getDescription();
		
		if(descContent == null || descContent.trim().isEmpty()){
			descContent = "#Không có mô tả";
			desc.addStyleName(ExplorerLayout.TASK_ITEM_NO_DESC);
		}
		desc.setValue(descContent);
		
		taskDescWrap.addComponent(desc);
		
		
		
		CssLayout taskActionWrap = new CssLayout();
		taskActionWrap.setStyleName(ExplorerLayout.TASK_ACTION_WRAP);
		infoWrap.addComponent(taskActionWrap);
		
		Button btnDetail = new Button("Chi tiết");
		btnDetail.setId(TaskListView.BTN_DETAIL_ID);
		btnDetail.setData(task);
		btnDetail.addClickListener(click);
		btnDetail.setStyleName(ExplorerLayout.BTN_TASK_DETAIL);
		taskActionWrap.addComponent(btnDetail);
		
		return item;
	}
	
	
	public static Component buildItem(TaskInfo task, int i, ClickListener click, RepositoryService repositoryService) {
		CssLayout item = new CssLayout();
		item.setStyleName(ExplorerLayout.TASK_ITEM);
		item.setWidth("100%");
		
		CssLayout imgWrap = new CssLayout();
		imgWrap.setStyleName(ExplorerLayout.TASK_IMAGE_WRAP);
		Label lbl = new Label(i+"");
		lbl.setStyleName(ExplorerLayout.TASK_IMAGE);
		imgWrap.addComponent(lbl);
		
		Label priority = new Label();
		priority.addStyleName(ExplorerLayout.TASK_PRIORITY_STAR);
		CommonUtils.EPriority pri = CommonUtils.EPriority.getPriority(task.getPriority());
		priority.setDescription("Mức độ ưu tiên "+ pri.getName().toLowerCase());
		priority.addStyleName(pri.getStyleName());
		if(pri.equals(CommonUtils.EPriority.LOW)){
			priority.setValue(FontAwesome.STAR_O.getHtml());
		}else{
			priority.setValue(FontAwesome.STAR.getHtml());
		}
		priority.setContentMode(ContentMode.HTML);

		imgWrap.addComponent(priority);
		item.addComponent(imgWrap);
		
		CssLayout infoWrap = new CssLayout();
		infoWrap.setStyleName(ExplorerLayout.TASK_INFO_WRAP);
		item.addComponent(infoWrap);
		
		CssLayout taskNameWrap = new CssLayout();
		taskNameWrap.setStyleName(ExplorerLayout.TASK_NAME_WRAP);
		infoWrap.addComponent(taskNameWrap);
		String nameStr = "#Không tên";
		if(task.getName() != null && !task.getName().isEmpty()){
			nameStr = task.getName();
		}
		Label name = new Label(nameStr);
		name.setStyleName(ExplorerLayout.TASK_NAME);
		taskNameWrap.addComponent(name);

		if(task.getProcessDefinitionId() != null){
			ProcessDefinition pro = repositoryService.createProcessDefinitionQuery().processDefinitionId(task.getProcessDefinitionId()).singleResult();
			Label proIns = new Label(pro != null ? pro.getName() : "");
			proIns.setStyleName(ExplorerLayout.TASK_PROC_INS);
			taskNameWrap.addComponent(proIns);
		}
		
		SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
		String time = "";
		if(task.getCreateTime() != null){
			time = df.format(task.getCreateTime());
		}
		
		Label createTime = new Label(time);
		createTime.setStyleName(ExplorerLayout.TASK_CREATE_TIME);
		taskNameWrap.addComponent(createTime);
		
		CssLayout taskMoreInfoWrap = new CssLayout();
		taskMoreInfoWrap.setStyleName(ExplorerLayout.TASK_MORE_INFO_WRAP);
		infoWrap.addComponent(taskMoreInfoWrap);
		
		SimpleDateFormat df1 = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
		String duedateStr = "";
		if(task.getDueDate() != null){
			duedateStr = "Hạn thực hiện: "+df1.format(task.getDueDate());
		}else{
			duedateStr = "#Không có hạn thực hiện";
		}
		Label duedate = new Label(duedateStr);
		duedate.setIcon(FontAwesome.CLOCK_O);
		duedate.setStyleName(ExplorerLayout.TASK_DUEDATE);
		taskMoreInfoWrap.addComponent(duedate);
		
		CssLayout taskDescWrap = new CssLayout();
		taskDescWrap.setStyleName(ExplorerLayout.TASK_DESC_WRAP);
		infoWrap.addComponent(taskDescWrap);
		
		Label desc = new Label();
		desc.setStyleName(ExplorerLayout.TASK_ITEM_DESC);
		
		String descContent = task.getDescription();
		
		if(descContent == null || descContent.trim().isEmpty()){
			descContent = "#Không có mô tả";
			desc.addStyleName(ExplorerLayout.TASK_ITEM_NO_DESC);
		}
		desc.setValue(descContent);
		
		taskDescWrap.addComponent(desc);
		
		
		
		CssLayout taskActionWrap = new CssLayout();
		taskActionWrap.setStyleName(ExplorerLayout.TASK_ACTION_WRAP);
		infoWrap.addComponent(taskActionWrap);
		
		Button btnDetail = new Button("Chi tiết");
		btnDetail.setId(TaskListView.BTN_DETAIL_ID);
		btnDetail.setData(task);
		btnDetail.addClickListener(click);
		btnDetail.setStyleName(ExplorerLayout.BTN_TASK_DETAIL);
		taskActionWrap.addComponent(btnDetail);
		
		return item;
	}
	

	
	public static CssLayout buildTaskListByProcess(List<Task> taskList, RepositoryService repositoryService, String colors[], ClickListener click, String buttonId){
		if(taskList != null && !taskList.isEmpty()){
			CssLayout container = new CssLayout();
			container.setWidth("100%");
			container.addStyleName(ExplorerLayout.PROCESS_DEF_CONTAINER);
			
			Set<String> proDefIdList = new HashSet<String>();
			Map<String, List<Task>> taskProcessDefMap = new HashMap();
			
			Map<String, List<Task>> subTaskMap = new HashMap();
			
			List<Task> otherTasks = new ArrayList();
			List<String> taskIds = new ArrayList();
			for(Task task : taskList){
				taskIds.add(task.getId());
			}
			
			for(Task task : taskList){
				if(task.getProcessDefinitionId() != null){
					List<Task> tasks = taskProcessDefMap.get(task.getProcessDefinitionId());
					if(tasks == null && task.getProcessInstanceId() != null){
						tasks = new ArrayList<Task>();
						taskProcessDefMap.put(task.getProcessDefinitionId(), tasks);
					}
					if(tasks != null){
						tasks.add(task);	
					}
					if(!proDefIdList.contains(task.getProcessDefinitionId())){
						proDefIdList.add(task.getProcessDefinitionId());
					}
					
				}
				//subtask
				if(task.getParentTaskId() != null){
					if(taskIds.contains(task.getParentTaskId())){
						List<Task> subtaskList = subTaskMap.get(task.getParentTaskId());
						if(subtaskList == null){
							subtaskList = new ArrayList<Task>();
							subTaskMap.put(task.getParentTaskId(), subtaskList);
						}
						subtaskList.add(task);
					}else{
						otherTasks.add(task);
					}
				}
			}
			if(proDefIdList != null && !proDefIdList.isEmpty()){
				List<ProcessDefinition> processDefList = repositoryService.createProcessDefinitionQuery().processDefinitionIds(proDefIdList).orderByDeploymentId().desc().list();
				if(processDefList != null && !processDefList.isEmpty()){
					for(ProcessDefinition p : processDefList){
						CssLayout proItem =  TaskUtils.buildProcessList(p, taskProcessDefMap.get(p.getId()),subTaskMap, colors, click, buttonId);
						if(proItem != null){
							container.addComponent(proItem);
						}
					}
				}
			}
			if(otherTasks != null && !otherTasks.isEmpty()){
				CssLayout proItem =  TaskUtils.buildProcessList(null, otherTasks, subTaskMap, colors, click, buttonId);
				if(proItem != null){
					container.addComponentAsFirst(proItem);
				}
			}
			return container;
		}
		return null;
}
	
	public static CssLayout buildProcessList(ProcessDefinition p, List<Task> tasks,Map<String, List<Task>> subTaskMap, String colors[], ClickListener click, String buttonId){
		CssLayout itemWrap = new CssLayout();
		itemWrap.setWidth("100%");
		itemWrap.addStyleName(ExplorerLayout.PROCESS_DEF_ITEM_WRAP);
		
		String name = "#Không tên";
		String desc = "#Không có mô tả";
		if(p != null){
			if(p.getName() != null && !p.getName().isEmpty()){
				name = p.getName();
			}
			
			if(p.getDescription() != null && !p.getDescription().isEmpty()){
				desc = p.getDescription();
			}
		}else{
			name = "Công việc được giao trực tiếp";
			desc = "Công việc được tạo và giao trực tiếp tới bạn";
			itemWrap.addStyleName("other-tasks");
		}
			
			
		CssLayout item = new CssLayout();
		item.setWidth("100%");
		item.addStyleName(ExplorerLayout.PROCESS_DEF_WRAP);
		itemWrap.addComponent(item);
		
		
		CssLayout iconWrap = new CssLayout();
		iconWrap.addStyleName(ExplorerLayout.PROCESS_DEF_ICON_WRAP);
		
		Label icon = new Label();
		icon.addStyleName(ExplorerLayout.PROCESS_DEF_ICON);
		
		String shortName = "#";
		if(name.length() > 0){
			shortName =name.substring(0, 1);
		}
		String color = "#03A9F4";//default
		if(colors != null && colors.length > 0){
			int shortNameNum = shortName.toCharArray()[0];
			shortNameNum = shortNameNum % colors.length;
			color = colors[shortNameNum];
		}
		
		icon.setValue("<span style='background:"+color+"'>"+shortName+"</span>");
		icon.setContentMode(ContentMode.HTML);
		iconWrap.addComponent(icon);
		item.addComponent(iconWrap);
		
		CssLayout proInfoWrap = new CssLayout();
		proInfoWrap.addStyleName(ExplorerLayout.PROCESS_DEF_INFO_WRAP);
		item.addComponent(proInfoWrap);
		
		Label lblName = new Label(name);
		lblName.addStyleName(ExplorerLayout.PROCESS_DEF_NAME);
		proInfoWrap.addComponent(lblName);
		
		
		Label lblDesc = new Label(desc);
		lblDesc.addStyleName(ExplorerLayout.PROCESS_DEF_DESC);
		proInfoWrap.addComponent(lblDesc);
		
		
		CssLayout numberWrap = new CssLayout();
		numberWrap.addStyleName(ExplorerLayout.PROCESS_DEF_NUMBER_WRAP);
		
		int numberTmp = 0;
		if(tasks != null){
			numberTmp = tasks.size();
		}
		
		Label number = new Label(numberTmp + "");
		number.addStyleName(ExplorerLayout.PROCESS_DEF_NUMBER);
		numberWrap.addComponent(number);
		item.addComponent(numberWrap);
		
		
		if(tasks != null && !tasks.isEmpty()){
			
			CssLayout taskWrap = new CssLayout();
			taskWrap.setWidth("100%");
			taskWrap.addStyleName(ExplorerLayout.PROCESS_TASK_WRAP);
			itemWrap.addComponent(taskWrap);
			
			int i = 0;
			for(Task t : tasks){
				CssLayout tItem =  initTaskItem(t, subTaskMap, ++i, click, buttonId);
				if(tItem != null){
					taskWrap.addComponent(tItem);
				}
			}
		}
		
		return itemWrap;	
		
	}
	
	public static CssLayout initTaskItem(Task t, Map<String, List<Task>> subTaskMap, int i, ClickListener click, String buttonId){
		if(t != null){
			
			CssLayout item = new CssLayout();
			item.setWidth("100%");
			item.addStyleName(ExplorerLayout.PROCESS_TASK_ITEM);
			
			CssLayout taskIcon = new CssLayout();
			taskIcon.addStyleName(ExplorerLayout.PROCESS_TASK_ICON);
			item.addComponent(taskIcon);
			
			Label index = new Label(i+"");
			index.addStyleName(ExplorerLayout.PROCESS_TASK_INDEX);
			taskIcon.addComponent(index);
			
			CommonUtils.EPriority pri = CommonUtils.EPriority.getPriority(t.getPriority());
			
			Label priority = new Label();
			priority.addStyleName(ExplorerLayout.PROCESS_TASK_PRIORITY_STAR);
			priority.setDescription("Mức độ ưu tiên "+ pri.getName().toLowerCase());
			priority.addStyleName(pri.getStyleName());
			if(pri.equals(CommonUtils.EPriority.LOW)){
				priority.setValue(FontAwesome.STAR_O.getHtml());
			}else{
				priority.setValue(FontAwesome.STAR.getHtml());
			}
			priority.setContentMode(ContentMode.HTML);
			taskIcon.addComponent(priority);
			
			CssLayout taskInfo = new CssLayout();
			taskInfo.addStyleName(ExplorerLayout.PROCESS_TASK_INFO);
			item.addComponent(taskInfo);
			
			CssLayout taskNameWrap = new CssLayout();
			taskNameWrap.addStyleName(ExplorerLayout.PROCESS_TASK_NAME_WRAP);
			taskInfo.addComponent(taskNameWrap);
			
			
			CssLayout taskName = new CssLayout();
			taskName.addStyleName(ExplorerLayout.PROCESS_TASK_NAME);
			taskNameWrap.addComponent(taskName);
			
			String nameStr = "#Không tên";
			if(t.getName() != null && !t.getName().isEmpty()){
				nameStr = t.getName();
			}
			Button name = new Button(nameStr);
			name.addStyleName(Reindeer.BUTTON_LINK);
			name.addStyleName(ExplorerLayout.PROCESS_TASK_NAME_LINK);
			name.setData(t);
			name.addClickListener(click);
			name.setId(buttonId);
			taskName.addComponent(name);
			
			CssLayout taskCreateTime = new CssLayout();
			taskCreateTime.addStyleName(ExplorerLayout.PROCESS_TASK_CREATE_TIME);
			taskNameWrap.addComponent(taskCreateTime);
			
			SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
			String time = "";
			if(t.getCreateTime() != null){
				time = df.format(t.getCreateTime());
			}
			
			Label createTime = new Label(time);
			createTime.setStyleName(ExplorerLayout.PROCESS_TASK_CREATE_TIME_LBL);
			taskCreateTime.addComponent(createTime);
			
			
			CssLayout taskDescWrap = new CssLayout();
			taskDescWrap.addStyleName(ExplorerLayout.PROCESS_TASK_DESC_WRAP);
			taskInfo.addComponent(taskDescWrap);
			
			String descStr = "#Không có mô tả";
			if(t.getDescription() != null && !t.getDescription().isEmpty()){
				descStr = t.getDescription();
			}
			
			Label desc = new Label(descStr);
			desc.setContentMode(ContentMode.HTML);
			desc.addStyleName(ExplorerLayout.PROCESS_TASK_DESC);
			taskDescWrap.addComponent(desc);
			
			if(t.getProcessInstanceId() != null){
				ProcinstInfoService procinstInfoService = SpringApplicationContext.getApplicationContext().getBean(ProcinstInfoService.class);
				ProcinstInfo procinstInfo = procinstInfoService.findByProcinstIdAndInfoKey(t.getProcessInstanceId(), "DESC");
				if(procinstInfo != null && procinstInfo.getInfo() != null && !procinstInfo.getInfo().isEmpty()){
					CssLayout processInsInfoWrap = new CssLayout();
					processInsInfoWrap.addStyleName(ExplorerLayout.PROCESS_TASK_PRO_INS_INFO_WRAP);
					taskInfo.addComponent(processInsInfoWrap);
					Label note = new Label(procinstInfo.getInfo());
					note.setContentMode(ContentMode.HTML);
					note.addStyleName(ExplorerLayout.PROCESS_TASK_DESC);
					processInsInfoWrap.addComponent(note);
				}
			}
			
			CssLayout taskDuedateWrap = new CssLayout();
			taskDuedateWrap.addStyleName(ExplorerLayout.PROCESS_TASK_DUEDATE_WRAP);
			taskInfo.addComponent(taskDuedateWrap);
			
			if(t.getDueDate() != null){
				Label duedate = new Label("Hạn thực hiện: "+df.format(t.getDueDate()));
				duedate.addStyleName(ExplorerLayout.PROCESS_TASK_DUEDATE_LBL);
				Date now = new Date();
				if(now.after(t.getDueDate())){
					duedate.addStyleName("late");
				}
				taskDuedateWrap.addComponent(duedate);
			}
			
			if(subTaskMap != null){
				CssLayout subTaskContainer = buildSubtaskContainer(subTaskMap, t, click, buttonId);
				if(subTaskContainer != null){
					item.addComponent(subTaskContainer);
				}
			}
			
			//get cau hinh hien thi
			CauHinhHienThiService cauHinhHienThiService = SpringApplicationContext.getApplicationContext().getBean(CauHinhHienThiService.class);

			HistoryService historyService = SpringApplicationContext.getApplicationContext().getBean(HistoryService.class);
			HoSoService hoSoService = SpringApplicationContext.getApplicationContext().getBean(HoSoService.class);
			List<CauHinhHienThi> cauHinhList = cauHinhHienThiService.findByProcessDefId(t.getProcessDefinitionId());
			String processParentId = ProcessInstanceUtils.getParentInstance(t.getProcessInstanceId(), historyService);
			List<HoSo> hoSoList = null;
			if (processParentId != null) {
				hoSoList = hoSoService.getHoSoFind(null, null, null, processParentId, 0, 1);
			} else {
				hoSoList = hoSoService.getHoSoFind(null, null, null, t.getProcessInstanceId(), 0, 1);
			}
			HoSo hoSo = null;
			if (hoSoList != null && !hoSoList.isEmpty()) {
				hoSo = hoSoList.get(0);
			}
			
			if(cauHinhList != null && !cauHinhList.isEmpty()){
				CssLayout taskInfoWrap = new CssLayout();
				taskInfoWrap.setWidth("100%");
				taskInfoWrap.setStyleName("task-field-info-wrap");
				for(CauHinhHienThi cauHinhHienThi : cauHinhList){
					Label label = new Label();
					label.setStyleName("task-field-info");
					label.setContentMode(ContentMode.HTML);
					String content = "<span class='ttl'>"+cauHinhHienThi.getTitle() +"</span> ";
					if(hoSo != null && cauHinhHienThi.getKey() != null && hoSo.getHoSoGiaTri() != null){
						String keyArr [] = cauHinhHienThi.getKey().split("#");
						if(keyArr != null){
							List<String> keyList = new ArrayList(Arrays.asList(keyArr));
							keyList.set(0, "ROOT");
							Map<String, EFormTruongThongTin> truongTTMap = FormUtils.getHoSoValueMap(hoSo.getHoSoGiaTri());
							Map<String, Object> valueMapTmp = new HashMap(truongTTMap);
							while(!keyList.isEmpty()){
								String str=keyList.remove(0);
								if(keyList.isEmpty()){
									Object obj = valueMapTmp.get(str);
									if(obj instanceof EFormTruongThongTin){
										EFormTruongThongTin value = (EFormTruongThongTin) obj;
										content += "<span class='vl'>";
										if(value.getGiaTriChu() != null){
											content += value.getGiaTriChu();
										}else if(value.getGiaTriNgay() != null){
											SimpleDateFormat dfTmp = new SimpleDateFormat("dd/MM/yyyy");
											content += dfTmp.format(value.getGiaTriNgay());
										}else if(value.getGiaTriSo() != null){
											content += value.getGiaTriSo();
										}
										content += "</span>";
									}
								}else{
									List<Map<String, Object>> result = FormUtils.getValueByKey(str, false, valueMapTmp);
									if(result != null && !result.isEmpty()){
										valueMapTmp = result.get(0);
									}
								}
								
							}
						}
					}
					label.setValue(content);
					taskInfoWrap.addComponent(label);
				}
				taskDescWrap.addComponent(taskInfoWrap);
			}
			
			
			return item;
		}
		return null;
	}
	
	public static CssLayout buildSubtaskContainer(Map<String, List<Task>> subTaskMap, Task task, ClickListener click, String buttonId){
		List<Task> subtaskList = subTaskMap.get(task.getId());
		if(subtaskList != null){
			CssLayout subtaskContainer = new CssLayout();
			subtaskContainer.addStyleName("subtask-container");
			int i =0;
			for (Task t : subtaskList) {
				CssLayout subtaskItem = buildSubtaskItem(++i, t, click, buttonId);
				CssLayout sub = buildSubtaskContainer(subTaskMap, t, click, buttonId);
				if(sub != null){
					subtaskItem.addComponent(sub);
				}
				subtaskContainer.addComponent(subtaskItem);
			}
			return subtaskContainer;
		}
		return null;
		
	}
	
	public static CssLayout buildSubtaskItem(int i, Task t, ClickListener click, String buttonId){
		CssLayout subtaskItem = new CssLayout();
		subtaskItem.addStyleName("subtask-item");
		
		CssLayout taskName = new CssLayout();
		taskName.addStyleName("subtask-item-name-wrap");
		subtaskItem.addComponent(taskName);
		
		Label index = new Label(i+"");
		index.addStyleName("subtask-index");
		taskName.addComponent(index);
		
		String nameStr = "#Không tên";
		if(t.getName() != null && !t.getName().isEmpty()){
			nameStr = t.getName();
		}
		
		Button name = new Button(nameStr);
		name.addStyleName(Reindeer.BUTTON_LINK);
		name.addStyleName("subtask-item-name");
		name.setData(t);
		name.addClickListener(click);
		name.setId(buttonId);
		taskName.addComponent(name);
		
		SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
		String time = "";
		if(t.getCreateTime() != null){
			time = df.format(t.getCreateTime());
		}
		
		Label createTime = new Label(time);
		createTime.setStyleName("subtask-createtime");
		taskName.addComponent(createTime);
		
		return subtaskItem;
	}
	
	
	public void sortList(List<Object> objList) {
		Collections.sort(objList, comparator);
	}

	private Comparator<Object> comparator = new Comparator<Object>() {

		@Override
		public int compare(Object o1, Object o2) {
			Date o1Date = getDateFromMethod(o1);
			Date o2Date = getDateFromMethod(o2);
			if (o1Date != null && o2Date != null) {
				if (o1Date.before(o2Date))
					return -1;
				else if (o1Date.after(o2Date))
					return 1;

				else
					return 0;
			} else
				return -1;
		}
	};

	private Date getDateFromMethod(Object o) {
		try {
			if (o instanceof HistoricProcessInstance) {
				return ((HistoricProcessInstance) o).getStartTime();
			} else if (o instanceof HistoricTaskInstance) {
				return ((HistoricTaskInstance) o).getCreateTime();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

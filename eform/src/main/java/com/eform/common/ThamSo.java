package com.eform.common;

public class ThamSo {
	public static final String PAGE_SIZE = "PAGE_SIZE";
	public static final String APP_PAGE_SIZE = "APP_PAGE_SIZE";

	public static final String POPUP_PAGE_SIZE = "POPUP_PAGE_SIZE";
	public static final String PRO_DEF_PAGE_SIZE = "PRO_DEF_PAGE_SIZE";
	public static final String FOLDER_UPLOAD_PATH = "FOLDER_UPLOAD_PATH";
	public static final String DASHBOAD_CHARTS = "DASHBOAD_CHARTS";
	public static final String COLOR_LIST = "COLOR_LIST";
	public static final String GENERAL_REPORT = "GENERAL_REPORT";
	public static final String PAGE_SIZE_CUSTOM = "PAGE_SIZE_CUSTOM";
	public static final String DAY_NUMBER_REPORT = "DAY_NUMBER_REPORT";
	public static final String PROCESS_REPORT = "PROCESS_REPORT";
	public static final String PROCESS_REPORT_TITLE = "PROCESS_REPORT_TITLE";
	public static final String EMAIL_PATTERN = "EMAIL_PATTERN";
	
	public static final String MAIL_HOST_NAME = "MAIL_HOST_NAME";
	public static final String MAIL_SMTP_PORT = "MAIL_SMTP_PORT";
	public static final String MAIL_SSL_ON_CONNECT = "MAIL_SSL_ON_CONNECT";
	public static final String MAIL_FROM_ADDRESS = "MAIL_FROM_ADDRESS";
	public static final String MAIL_ACCOUNT = "MAIL_ACCOUNT";
	public static final String MAIL_PASSWORD = "MAIL_PASSWORD";
	public static final String MAIL_CHARSET = "MAIL_CHARSET";
	public static final String SOLRSEARCH_URL = "SOLRSEARCH_URL";
	public static final String HELP_DOC_PATH = "HELP_DOC_PATH";
	public static final String LOG_PATH = "LOG_PATH";
	public static final String CHAT_USER_URL = "CHAT_USER_URL";
	public static final String REMOVE_ALL_DATA = "REMOVE_ALL_DATA";
	public static final String UPDATE_SOLR = "UPDATE_SOLR";
	
	

	public static final String MAIL_NOTIC_TEMPLATE = "MAIL_NOTIC_TEMPLATE";
	public static final String MAIL_NOTIC_SUBJECT = "MAIL_NOTIC_SUBJECT";
	public static final String TASK_DETAIL_URL = "TASK_DETAIL_URL";
	
}

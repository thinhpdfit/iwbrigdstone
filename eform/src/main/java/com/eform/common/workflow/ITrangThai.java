package com.eform.common.workflow;

public interface ITrangThai {
    int getId();
    String getTen();
    String getStyleName();
}
package com.eform.common.workflow;

import java.util.List;

public abstract class Workflow<E>{
    protected E object;
    protected ITrangThai trangThai;
    
    protected boolean pheDuyet;
    protected boolean tuChoi;    
    protected String pheDuyetTen;    
    protected String tuChoiTen;  
    protected String styleName;
    protected boolean sua;
    protected boolean xoa;
    
    public Workflow(){
        super();
    }
    public abstract void init(E object) throws Exception;

    public abstract List<ITrangThai> getTrangThaiList() throws Exception;

    public abstract ITrangThai getTrangThaiSuDung() throws Exception;

    public abstract E pheDuyet() throws Exception;

    public abstract E tuChoi() throws Exception;

    public E getObject() {
        return object;
    }

    public ITrangThai getTrangThai() {
        return trangThai;
    }

    public boolean isPheDuyet() {
        return pheDuyet;
    }

    public boolean isTuChoi() {
        return tuChoi;
    }

    public boolean isSua() {
        return sua;
    }

    public boolean isXoa() {
        return xoa;
    }

    public String getPheDuyetTen() {
        return pheDuyetTen;
    }

    public String getTuChoiTen() {
        return tuChoiTen;
    }
	public String getStyleName() {
		return styleName;
	}
	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}
    
    
}

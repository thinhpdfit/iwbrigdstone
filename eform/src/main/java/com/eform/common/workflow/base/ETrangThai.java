package com.eform.common.workflow.base;

import com.eform.common.workflow.ITrangThai;

public enum ETrangThai implements ITrangThai {
    MOI_TAO(0, "Mới tạo", 1,"Sử dụng", null,null, "status-new", true,true),
    DANG_SU_DUNG(1, "Đang sử dụng", null,null, -1,"Ngừng sử dụng", "status-using",false,false),
    NGUNG_SU_DUNG(-1, "Ngừng sử dụng", 1,"Sử dụng", null,null, "status-susspended",true,false);
    final int id;
    final String ten;
    final Integer pheDuyetId;
    final String  pheDuyetTen;
    final Integer tuChoiId;
    final String tuChoiTen;
    final String styleName;
    final boolean sua;
    final boolean xoa;

    private ETrangThai(int id, String ten, Integer pheDuyetId,String  pheDuyetTen,
                       Integer tuChoiId,String tuChoiTen, String styleName, boolean sua,boolean xoa) {
        this.id = id;
        this.ten = ten;
        this.pheDuyetId = pheDuyetId;
        this.pheDuyetTen = pheDuyetTen;
        this.tuChoiId = tuChoiId;
        this.tuChoiTen = tuChoiTen;
        this.styleName = styleName;
        this.sua = sua;
        this.xoa = xoa;
    }

    public String getTen() {
        return ten;
    }

    public static ETrangThai getTrangThai(Integer trangThaiId) {
        if (trangThaiId != null) {
            int value = trangThaiId.intValue();
            for (ETrangThai trangThai : values()) {
                if (trangThai.id == value) {
                    return trangThai;
                }
            }
        }
        return ETrangThai.MOI_TAO;
    }

    public static ETrangThai getTrangThai(Long trangThaiId) {
        if (trangThaiId != null) {
            int value = trangThaiId.intValue();
            for (ETrangThai trangThai : values()) {
                if (trangThai.id == value) {
                    return trangThai;
                }
            }
        }
        return ETrangThai.MOI_TAO;
    }

    public boolean isTrangThai(Integer trangThaiId) {
        if (trangThaiId != null) {
            return getId()==trangThaiId.intValue();
        }else if(getId()==MOI_TAO.getId()){
            return true;
        }
        return false;
    }

    public boolean isTrangThai(Long trangThaiId) {
        if (trangThaiId != null) {
            return getId()==trangThaiId.intValue();
        }else if(getId()==MOI_TAO.getId()){
            return true;
        }
        return false;
    }

    public boolean isPheDuyet() {
        return pheDuyetId != null;
    }

    public ETrangThai getTrangThaiPheDuyet() {
        return getTrangThai(pheDuyetId);
    }

    public boolean isTuChoi() {
        return tuChoiId != null;
    }

    public ETrangThai getTrangThaiTuChoi() {
        return getTrangThai(tuChoiId);
    }

    public int getId() {
        return id;
    }

    public boolean isSua() {
        return sua;
    }

    public boolean isXoa() {
        return xoa;
    }
    public String getTuChoiTen() {
        return tuChoiTen;
    }

    public String getPheDuyetTen() {
        return pheDuyetTen;
    }

	public String getStyleName() {
		// TODO Auto-generated method stub
		return styleName;
	}
}

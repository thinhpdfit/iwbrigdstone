package com.eform.common.workflow;

import java.util.List;

import com.eform.common.EChucNang;

public class WorkflowManagement<E> {
    private final String maChucNang;
    private final String workflowPackage;
    private Class workflowClass;

    public WorkflowManagement(String maChucNang) {
        super();
        this.maChucNang = maChucNang;
        if(EChucNang.E_FORM_PROC_INST.equals(this.maChucNang)){
        	workflowPackage = "com.eform.common.workflow.processinstance";
        }else{
        	workflowPackage = "com.eform.common.workflow.base";
        }
    }

    private void getWorkflowClass() {
        if (workflowClass == null) {
            try {
                workflowClass =
                        Class.forName(workflowPackage + "." + "Workflow");
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
        }
    }

    public Workflow<E> getWorkflow(E object) {
        getWorkflowClass();
        if (workflowClass != null) {
            try {
                Workflow<E> workflow =
                    (Workflow<E>)workflowClass.newInstance();
                workflow.init(object);
                return workflow;
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<ITrangThai> getTrangThaiList() {
        getWorkflowClass();
        if (workflowClass != null) {
            try {
                Workflow<E> workflow =
                    (Workflow<E>)workflowClass.newInstance();
                return workflow.getTrangThaiList();
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
        }
        return null;
    }

    public ITrangThai getTrangThaiSuDung() {
        getWorkflowClass();
        if (workflowClass != null) {
            try {
                Workflow<E> workflow =
                    (Workflow<E>)workflowClass.newInstance();
                return workflow.getTrangThaiSuDung();
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
        }
        return null;
    }
}

package com.eform.common.workflow.base;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.eform.common.workflow.ITrangThai;

public class Workflow<E> extends com.eform.common.workflow.Workflow<E> {

    public Workflow() {
        super();
    }

    public void init(E object) throws Exception {
        ETrangThai trangThai = getTrangThai(object);
        if (trangThai == null) {
            setTrangThai(object, ETrangThai.MOI_TAO);
            trangThai = getTrangThai(object);
        }
        if (trangThai != null) {
            this.trangThai = trangThai;
            this.object = object;
            this.pheDuyet = trangThai.isPheDuyet();
            this.pheDuyetTen = trangThai.getPheDuyetTen();
            this.tuChoi = trangThai.isTuChoi();
            this.tuChoiTen = trangThai.getTuChoiTen();
            this.styleName = trangThai.getStyleName();
            this.sua = trangThai.isSua();
            this.xoa = trangThai.isXoa();
        }
    }

    public List<ITrangThai> getTrangThaiList() {
        List<ITrangThai> list = new ArrayList<ITrangThai>();
        for (ETrangThai trangThai : ETrangThai.values()) {
            list.add(trangThai);
        }
        return list;
    }
    
    public ITrangThai getTrangThaiSuDung(){
        return ETrangThai.DANG_SU_DUNG;
    }   

    public E pheDuyet() throws Exception {
        ETrangThai trangThai = getTrangThai(getObject());
        if (trangThai != null && trangThai.isPheDuyet()) {
            object =
                    setTrangThai(getObject(), trangThai.getTrangThaiPheDuyet());
            return object;
        }
        return null;
    }

    public E tuChoi() throws Exception {
        ETrangThai trangThai = getTrangThai(getObject());
        if (trangThai != null && trangThai.isTuChoi()) {
            object = setTrangThai(getObject(), trangThai.getTrangThaiTuChoi());
            return object;
        }
        return null;
    }

    private static <E> E setTrangThai(E object,
                                      ETrangThai trangThai) throws Exception {
        if (object != null && trangThai != null) {
            Class objClass = object.getClass();
            Field[] fields = objClass.getDeclaredFields();
            for (Field field : fields) {
                if (field != null && field.getName().equals("trangThai")) {
                    field.setAccessible(true);
                    String fieldName = field.getType().getName();
                    if ("java.lang.Integer".equals(fieldName)) {
                        field.set(object, new Integer(trangThai.getId()));
                    } else if ("java.lang.Long".equals(fieldName)) {
                        field.set(object, new Long(trangThai.getId()));
                    } else {
                        return null;
                    }
                    return object;
                }
            }
        }
        return null;
    }

    private static <E> ETrangThai getTrangThai(E object) throws Exception {
        if (object != null) {
            Class objClass = object.getClass();
            Field[] fields = objClass.getDeclaredFields();
            for (Field field : fields) {
                if (field != null && field.getName().equals("trangThai")) {
                    field.setAccessible(true);
                    Object trangThaiObject = field.get(object);
                    ETrangThai trangThai = null;
                    if (trangThaiObject instanceof Integer) {
                        trangThai =
                                ETrangThai.getTrangThai((Integer)trangThaiObject);
                    } else if (trangThaiObject instanceof Long) {
                        trangThai =
                                ETrangThai.getTrangThai((Long)trangThaiObject);
                    }
                    return trangThai;
                }
            }
        }
        return null;
    }
}

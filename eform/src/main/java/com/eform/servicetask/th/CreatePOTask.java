package com.eform.servicetask.th;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.eform.common.SpringApplicationContext;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.FormUtils;
import com.eform.common.utils.HoSoUtils;
import com.eform.common.utils.ProcessInstanceUtils;
import com.eform.model.entities.HoSo;
import com.eform.service.HoSoService;

import org.activiti.engine.HistoryService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.history.HistoricProcessInstance;

public class CreatePOTask implements JavaDelegate{
	private Expression serviceUrl;
	private Expression maPOPath;
	private Expression maPOKey;
	private Expression maPROPath;
	private Expression vungPath;
	private Expression khuVucPath;
	private Expression dsdPath;
	private Expression hangHoaPath;
	private Expression hangHoaMaPath;
	private Expression hangHoaQuyCachPath;
	private Expression hangHoaSoLuongPath;
	
	
	private HoSoService hoSoService;
	protected HistoryService historyService;
	


	private HttpUriRequest createPORequest(DelegateExecution execution)
			throws Exception {
		XmlUtils xml = null;
		String data = (String) execution.getVariable("CORE#DATA");
		if (data != null) {
			xml = new XmlUtils(data);
			String uriString = (String) serviceUrl.getValue(execution);
			String maPRO = null;
			Element maPRODataE = getFirst(xml, (String) maPROPath.getValue(execution));
			if (maPRODataE != null) {
				maPRO = maPRODataE.getAttribute("giatrichu");
			}
			String vung = null;
			Element vungDataE = getFirst(xml, (String) vungPath.getValue(execution));
			if (vungDataE != null) {
				vung = vungDataE.getAttribute("giatrichu");
			}
			String khuVuc = null;
			Element khuVucDataE = getFirst(xml, (String) khuVucPath.getValue(execution));
			if (khuVucDataE != null) {
				khuVuc = khuVucDataE.getAttribute("giatrichu");
			}
			String dsd = null;
			Element dsdDataE = getFirst(xml, (String) dsdPath.getValue(execution));
			if (dsdDataE != null) {
				dsd = dsdDataE.getAttribute("giatrichu");
			}

		URI uri = new URI(uriString);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		Element envelopeE = doc.createElement("soap:Envelope");
		envelopeE.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		envelopeE.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		envelopeE.setAttribute("xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");
		envelopeE.setAttribute("xmlns:ns1", "/eform/service/sap");
		doc.appendChild(envelopeE);

		Element bodyE = doc.createElement("soap:Body");
		envelopeE.appendChild(bodyE);
		Element createPORequestE = doc.createElement("ns1:createPORequest");
		bodyE.appendChild(createPORequestE);

		Element maPROE = doc.createElement("ns1:maPRO");
		createPORequestE.appendChild(maPROE);
		maPROE.setTextContent(maPRO);
		Element vungE = doc.createElement("ns1:vung");
		createPORequestE.appendChild(vungE);
		maPROE.setTextContent(vung);
		Element khuVucE = doc.createElement("ns1:khuVuc");
		createPORequestE.appendChild(khuVucE);
		khuVucE.setTextContent(khuVuc);
		Element dsdE = doc.createElement("ns1:dsd");
		createPORequestE.appendChild(dsdE);
		dsdE.setTextContent(dsd);

		String hangHoaMaKey = (String) hangHoaMaPath.getValue(execution);
		String hangHoaQuyCachKey = (String) hangHoaQuyCachPath.getValue(execution);
		String hangHoaSoLuongKey = (String) hangHoaSoLuongPath.getValue(execution);
		List<Element> hangHoaParentList = getNhom(xml, hangHoaPath==null?null:getPathList((String) hangHoaPath.getValue(execution)));
		for (Element hangHoaParent : hangHoaParentList) {
			List<Map<String, String>> hangHoaList = new ArrayList<Map<String, String>>();
			List<Element> truongthongtinList = XmlUtils.getElements(hangHoaParent, "truongthongtin");
			for (Element truongthongtin : truongthongtinList) {
				String maTruongthongtin = truongthongtin.getAttribute("ma");
				String thuTuTruongthongtin = truongthongtin.getAttribute("thutu");
				try {
					if (maTruongthongtin != null) {
						if (maTruongthongtin.equals(hangHoaMaKey)) {
							Integer thuTuInteger = Integer.parseInt(thuTuTruongthongtin) - 1;
							for (int i = hangHoaList.size(), n = thuTuInteger.intValue(); i <= n; i++) {
								hangHoaList.add(new HashMap<String, String>());
							}
							Map<String, String> hangHoa = hangHoaList.get(thuTuInteger.intValue());
							hangHoa.put("ma", truongthongtin.getAttribute("giatrichu"));
						}
						if (maTruongthongtin.equals(hangHoaQuyCachKey)) {
							Integer thuTuInteger = Integer.parseInt(thuTuTruongthongtin) - 1;
							for (int i = hangHoaList.size(), n = thuTuInteger.intValue(); i <= n; i++) {
								hangHoaList.add(new HashMap<String, String>());
							}
							Map<String, String> hangHoa = hangHoaList.get(thuTuInteger.intValue());
							hangHoa.put("quyCach", truongthongtin.getAttribute("giatrichu"));
						} 
						if (maTruongthongtin.equals(hangHoaSoLuongKey)) {
							Integer thuTuInteger = Integer.parseInt(thuTuTruongthongtin) - 1;
							for (int i = hangHoaList.size(), n = thuTuInteger.intValue(); i <= n; i++) {
								hangHoaList.add(new HashMap<String, String>());
							}
							Map<String, String> hangHoa = hangHoaList.get(thuTuInteger.intValue());
							hangHoa.put("soLuong", truongthongtin.getAttribute("giatriso"));
						}
					}
				} catch (Exception e) {

				}
			}
			for (int i = 0, n = hangHoaList.size(); i < n; i++) {
				Map<String, String> hangHoa = hangHoaList.get(i);
				Element hangHoaE = doc.createElement("ns1:hangHoa");
				createPORequestE.appendChild(hangHoaE);

				Element maE = doc.createElement("ns1:ma");
				hangHoaE.appendChild(maE);
				maE.setTextContent(hangHoa.get("ma"));
				Element quyCachE = doc.createElement("ns1:quyCach");
				hangHoaE.appendChild(quyCachE);
				quyCachE.setTextContent(hangHoa.get("quyCach"));
				Element soLuongE = doc.createElement("ns1:soLuong");
				hangHoaE.appendChild(soLuongE);
				soLuongE.setTextContent(hangHoa.get("soLuong"));
			}
		}
		String requestXml = XmlUtils.toString(doc, false);
		System.out.println(requestXml);
		StringEntity entity = new StringEntity(requestXml, "UTF-8");
		RequestBuilder builder = RequestBuilder.post().addHeader("Content-Type", "text/xml; charset=utf-8")
				.setEntity(entity).setUri(uri);
		HttpUriRequest request = builder.build();
		return request;
		}
		return null;
	}

	public void createPO(DelegateExecution execution) throws Exception {
		HttpUriRequest request = createPORequest(execution);
		if (request != null) {
			HttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse response = httpClient.execute(request);
		int responseCode = response.getStatusLine().getStatusCode();
		if (responseCode == 200) {
			XmlUtils xml = new XmlUtils(response.getEntity().getContent());
			Element envelopeE = xml.getFirstElement("Envelope"); 
			Element bodyE = XmlUtils.getFirstElement(envelopeE, "Body");
			Element createPOResponseE = XmlUtils.getFirstElement(bodyE, "createPOResponse");
			Element maPOE = XmlUtils.getFirstElement(createPOResponseE, "maPO");
			if(maPOE!=null){
				execution.setVariable((String) maPOKey.getValue(execution), maPOE.getTextContent());
				String data = (String) execution.getVariable("CORE#DATA");
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document doc = db.parse(new ByteArrayInputStream(data.getBytes("UTF-8")));
				List<Element> currentList = XmlUtils.getElements(doc.getChildNodes(), "hoso");
				List<String> pathList = getPathList((String) maPOPath.getValue(execution));
				for (int i = 0, n = pathList.size() - 1; i < n; i++) {
					String path = pathList.get(i);
					List<Element> nextList = new ArrayList();
					for (Element current : currentList) {
						List<Element> childList = XmlUtils.getElements(current, "nhom");
						if (childList != null) {
							for (Element child : childList) {
								String ma = child.getAttribute("ma");
								if (ma != null && ma.equals(path)) {
									nextList.add(child);
								}
							}
						}
					}
					if (nextList.size() == 0) {
						for (Element current : currentList) {
							Element child = doc.createElement("nhom");
							child.setAttribute("ma", path);
							current.appendChild(child);
							nextList.add(child);
						}
					}
					currentList = nextList;
				}
				String lastPath = pathList.get(pathList.size() - 1);
				List<Element> nextList = new ArrayList();
				for (Element current : currentList) {
					List<Element> childList = XmlUtils.getElements(current, "truongthongtin");
					if (childList != null) {
						for (Element child : childList) {
							String ma = child.getAttribute("ma");
							if (ma != null && ma.equals(lastPath)) {
								nextList.add(child);
							}
						}
					}
				}
				if (nextList.size() == 0) {
					for (Element current : currentList) {
						Element child = doc.createElement("truongthongtin");
						child.setAttribute("ma", lastPath);
						current.appendChild(child);
						nextList.add(child);
					}
				}
				for (Element truongthongtin : nextList) {
					truongthongtin.setAttribute("giatrichu", maPOE.getTextContent());
				}
				

				String hoSoGiaTri = XmlUtils.toString(doc, true);
				execution.setVariable("CORE#DATA", hoSoGiaTri);
				
				try {
					if(hoSoGiaTri != null){
						String processInstanceId = execution.getProcessInstanceId();
						List<HoSo> hoSoList = null;
						String supserParentCurrentId = null;
						String processParentId = ProcessInstanceUtils.getParentInstance(processInstanceId, historyService);
						if (processParentId != null) {
							hoSoList = hoSoService.getHoSoFind(null, null, null, processParentId, 0, 1);
						} else {
							hoSoList = hoSoService.getHoSoFind(null, null, null, processInstanceId, 0, 1);
						}
						if (hoSoList != null && !hoSoList.isEmpty()) {
							HoSo hoSoCurrent = hoSoList.get(0);
							if(hoSoCurrent != null){
								String boHS = hoSoCurrent.getHoSoGiaTri();
								EFormBoHoSo eBoHS = CommonUtils.getEFormBoHoSo(boHS);
								EFormHoSo hs = CommonUtils.getEFormHoSo(hoSoGiaTri);
								EFormBoHoSo eFormBoHS = FormUtils.getBoHs(eBoHS, hs, true, execution.getId());
								if (eFormBoHS != null) {
									boHS = CommonUtils.getEFormBoHoSoString(eFormBoHS);
									hoSoCurrent.setHoSoGiaTri(boHS);
									hoSoService.save(hoSoCurrent);
								}
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println(response.getStatusLine().getStatusCode());
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		}
		}
	}

	private List<String> getPathList(String pathString) {
		List<String> list = new ArrayList<String>();
		if(pathString!=null){
		String pathTmp = pathString;
		while (pathTmp.contains(".")) {
			list.add(pathTmp.substring(0, pathTmp.indexOf(".")));
			pathTmp = pathTmp.substring(pathTmp.indexOf(".") + 1);
		}
		list.add(pathTmp);
		}
		return list;
	}

	private Element getFirst(XmlUtils xml, String pathString) {
		if (pathString != null && pathString.isEmpty() == false) {
			List<String> list = getPathList(pathString);
			if (list.size() > 0) {
				List<Element> parentList = getNhom(xml, list.subList(0, list.size() - 1));
				String lastPath = list.get(list.size() - 1);
				for (Element parent : parentList) {
					List<Element> childList = XmlUtils.getElements(parent, "truongthongtin");
					if (childList != null) {
						for (Element child : childList) {
							String ma = child.getAttribute("ma");
							if (ma != null && ma.equals(lastPath)) {
								return child;
							}
						}
					}
				}
			}
		}
		return null;
	}

	private List<Element> getNhom(XmlUtils xml, List<String> paths) {
		List<Element> currentList = new ArrayList<Element>();
		currentList.addAll(xml.getElements("hoso"));
		if(paths!=null){
		for (String path : paths) {
			if(path.isEmpty()==false){
			List<Element> nextList = new ArrayList<Element>();
			for (Element current : currentList) {
				List<Element> childList = XmlUtils.getElements(current, "nhom");
				if (childList != null) {
					for (Element child : childList) {
						String ma = child.getAttribute("ma");
						if (ma != null && ma.equals(path)) {
							nextList.add(child);
						}
					}
				}
			}
			currentList = nextList;
			}
		}
		}
		return currentList;
	}
	@Override
	public void execute(DelegateExecution execution) throws Exception {
		hoSoService = SpringApplicationContext.getApplicationContext().getBean(HoSoService.class);
		historyService = SpringApplicationContext.getApplicationContext().getBean(HistoryService.class);
		createPO(execution);
	}
	
	
//	public static void main(String[] args) {
//		try {
//			CreatePOTask test = new CreatePOTask();
//			test.serviceUrl = new Expression("http://127.0.0.1:8080/eform/ws/dms");
//			test.maPOPath = new Expression("ma_po");
//			test.maPOKey = new Expression("ma_pro");
//			test.maPROPath = new Expression("ma_pro");
//			test.vungPath = new Expression("vung");
//			test.khuVucPath = new Expression("khu_vuc");
//			test.dsdPath = new Expression("dsd");
//			test.hangHoaPath = new Expression("hang_hoa");
//			test.hangHoaMaPath = new Expression("ma");
//			test.hangHoaQuyCachPath = new Expression("quy_cach");
//			test.hangHoaSoLuongPath = new Expression("so_luong");
//			test.execute(new DelegateExecution());
//		} catch (Exception e) {
//
//		}
//	}

}

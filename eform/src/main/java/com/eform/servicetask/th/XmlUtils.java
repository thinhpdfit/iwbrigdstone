package com.eform.servicetask.th;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlUtils {
	public Document doc;

	public XmlUtils(InputStream is) throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		this.doc = db.parse(is);
	}

	public XmlUtils(String xml) throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		this.doc = db.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));
	}

	public Element getFirstElement(String tagName) {
		if (doc != null) {
			return getFirstElement(doc.getChildNodes(), tagName);
		}
		return null;
	}

	public static Element getFirstElement(Element parent, String tagName) {
		if (parent != null) {
			NodeList nodeList = parent.getChildNodes();
			return getFirstElement(nodeList, tagName);
		}
		return null;
	}

	public static Element getFirstElement(NodeList nodeList, String tagName) {
		if (nodeList != null) {
			for (int i = 0, n = nodeList.getLength(); i < n; i++) {
				Node node = nodeList.item(i);
				if (node instanceof Element) {
					Element element = (Element) node;
					String tagNameCurrent = element.getTagName();
					while (tagNameCurrent.contains(":")) {
						tagNameCurrent = tagNameCurrent.substring(tagNameCurrent.indexOf(":") + 1);
					}
					if (tagNameCurrent.equals(tagName)) {
						return element;
					}
				}
			}
		}
		return null;
	}

	public List<Element> getElements(String tagName) {
		if (doc != null) {
			return getElements(doc.getChildNodes(), tagName);
		}
		return null;
	}

	public static List<Element> getElements(Element parent, String tagName) {
		if (parent != null) {
			NodeList nodeList = parent.getChildNodes();
			return getElements(nodeList, tagName);
		}
		return null;
	}

	public static List<Element> getElements(NodeList nodeList, String tagName) {
		if (nodeList != null) {
			List<Element> result=new ArrayList<Element>();
			for (int i = 0, n = nodeList.getLength(); i < n; i++) {
				Node node = nodeList.item(i);
				if (node instanceof Element) {
					Element element = (Element) node;
					String tagNameCurrent = element.getTagName();
					while (tagNameCurrent.contains(":")) {
						tagNameCurrent = tagNameCurrent.substring(tagNameCurrent.indexOf(":") + 1);
					}
					if (tagNameCurrent.equals(tagName)) {
						result.add(element);
					}
				}
			}
			return result;
		}
		return null;
	}

	
	public String toString() {
		try {
			return toString(doc, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String toString(boolean omitXmlDeclaration) throws Exception {
		return toString(doc, omitXmlDeclaration);
	}

	public static String toString(Document doc, boolean omitXmlDeclaration) throws Exception {
		if (doc != null) {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			if (omitXmlDeclaration) {
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			}
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String str = writer.getBuffer().toString();
			return str;
		}
		return null;
	}
}

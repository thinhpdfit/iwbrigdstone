package com.eform.servicetask;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eform.common.SpringApplicationContext;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.utils.CommonUtils;
import com.eform.model.entities.HoSo;
import com.eform.service.BieuMauService;
import com.eform.service.HoSoService;
import com.eform.ui.form.FormGenerate;

@Component
public class FormDataTask implements JavaDelegate {
	@Autowired
	private BieuMauService bieuMauService;
	@Autowired
	private HoSoService hoSoService;
	private Expression formKey;
	private Expression formHtmlData;

	  public void execute(DelegateExecution execution) throws Exception {
		  if(bieuMauService == null){
			  bieuMauService=SpringApplicationContext.getApplicationContext().getBean(BieuMauService.class);
		  }
		  
		  if(hoSoService == null){
			  hoSoService=SpringApplicationContext.getApplicationContext().getBean(HoSoService.class);
		  }
		  
		  String formKeyStr=(String) formKey.getValue(execution);
		  if(formKeyStr  != null){
			  String processInstanceId = execution.getProcessInstanceId();
			  
			  List<HoSo> hoSoList = hoSoService.getHoSoFind(null, null, null, processInstanceId, 0, 1);
			  
			  if (hoSoList != null && !hoSoList.isEmpty()) {
					HoSo hoSo = hoSoList.get(0);
					String hoSoXml = null;
					String boHoSoXml = hoSo.getHoSoGiaTri();
					EFormBoHoSo boHS = CommonUtils.getEFormBoHoSo(boHoSoXml);
					if(boHS != null && boHS.getHoSo() != null){
						EFormHoSo hs = boHS.getHoSo();
						hoSoXml = CommonUtils.getEFormHoSoString(hs);
					}
					FormGenerate formGenerate = new FormGenerate(formKeyStr, hoSoXml);
					String htmlData = formGenerate.getHtmlData();
					execution.setVariable((String) formHtmlData.getValue(execution), htmlData);
			  }
		  }
	  }
	}
package com.eform.servicetask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.identity.User;

import com.eform.common.SpringApplicationContext;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.type.EFormTruongThongTin;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.FormUtils;
import com.eform.common.utils.ProcessInstanceUtils;
import com.eform.model.entities.HoSo;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BieuMauService;
import com.eform.service.HoSoService;
import com.eform.service.HtDonViServices;

public class MultiInstanceProcessData implements JavaDelegate {
	private BieuMauService bieuMauService;
	private HoSoService hoSoService;
	protected HistoryService historyService;
	private HtDonViServices htDonViServices;
	private IdentityService identityService;
	private RuntimeService runtimeService;
	

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		bieuMauService = SpringApplicationContext.getApplicationContext().getBean(BieuMauService.class);
		hoSoService = SpringApplicationContext.getApplicationContext().getBean(HoSoService.class);
		runtimeService = SpringApplicationContext.getApplicationContext().getBean(RuntimeService.class);
		historyService = SpringApplicationContext.getApplicationContext().getBean(HistoryService.class);
		htDonViServices = SpringApplicationContext.getApplicationContext().getBean(HtDonViServices.class);
		identityService = SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
		
		Object elementVarObj = runtimeService.getVariable(execution.getId(), "element_var");
		Map<String, Object> elementVarObj1 = runtimeService.getVariables(execution.getId());
		if(elementVarObj != null && elementVarObj instanceof String){
			String pathTmp = (String) elementVarObj;
			Pattern pa = Pattern.compile("(.*)#([0-9]+)");
	    	Matcher ma = pa.matcher(pathTmp);
	    	if(ma.matches()){
				String currentPath = ma.group(1);
				int currentThuTu = new Integer(ma.group(2));
				String processInstanceId = execution.getProcessInstanceId();
				List<HoSo> hoSoList = null;
				String processParentId = ProcessInstanceUtils.getParentInstance(processInstanceId, historyService);
				if (processParentId != null) {
					hoSoList = hoSoService.getHoSoFind(null, null, null, processParentId, 0, 1);
				} else {
					hoSoList = hoSoService.getHoSoFind(null, null, null, processInstanceId, 0, 1);
				}
				if (hoSoList != null && !hoSoList.isEmpty()) {
					HoSo hoSoCurrent = hoSoList.get(0);
					String boHoSoXml = hoSoCurrent.getHoSoGiaTri();
					EFormBoHoSo boHS = CommonUtils.getEFormBoHoSo(boHoSoXml);
					if (boHS != null && boHS.getHoSo() != null) {
						EFormHoSo hs = boHS.getHoSo();
						List<String> pathList = null;
						if(currentPath != null){
							String[] pathArr = currentPath.split("#");
				    		pathList = java.util.Arrays.asList(pathArr);
						}
						String dataLoopPath  = null;
						Object dataLoopPathObj =  runtimeService.getVariable(execution.getId(), "dataLoopPath");
						if(dataLoopPathObj != null && dataLoopPathObj instanceof String){
							dataLoopPath = (String) dataLoopPathObj;
						}
						
						if(dataLoopPath != null){
							String[] pathArr = dataLoopPath.split("#");
							List<String> pathListTmp = java.util.Arrays.asList(pathArr);
							hs = CommonUtils.getEFormHoSoByLoopPath(hs, pathListTmp);
						}
						
						EFormHoSo hoSoLoopItem = CommonUtils.getEFormHoSo(hs, pathList, currentThuTu);
						if(hoSoLoopItem != null){
							Map<String, Object> variables = getVariables(hoSoLoopItem);
							execution.setVariables(variables);	
							Map<String, List<String>> loopMap = new HashMap();
							FormUtils.getDataLoop(hoSoLoopItem, loopMap, null);
							if(!loopMap.isEmpty()){
								execution.setVariables(loopMap);	
							}
						}
					}
				}
			}
		}
	}
	
	public Map<String, Object> getVariables(EFormHoSo hoSoLoopItem){
		Map<String, Object> variables = new HashMap();
		if(hoSoLoopItem != null){
			List<EFormHoSo> process = new ArrayList();
			process.add(hoSoLoopItem);
			while(!process.isEmpty()){
				EFormHoSo loop = process.remove(0);
				loop.setMaTemp("");
				if(loop.getTruongThongTinList() != null){
					for(EFormTruongThongTin truongTT : loop.getTruongThongTinList()){
						if(truongTT != null){
							if(truongTT.getGiaTriChu() != null){
								variables.put(truongTT.getMaTruongThongTin(), truongTT.getGiaTriChu());
								//lay thong  tin lien quan(nguoi dung, don vi, ...)
								if(truongTT.getMaTruongThongTin() != null){
									List<TruongThongTin> truongTTList = bieuMauService.getTruongThongTinFind(truongTT.getMaTruongThongTin(), null, null, null, null, 0, 1);
									if(truongTTList != null && !truongTTList.isEmpty()){
										TruongThongTin truongThongTin = truongTTList.get(0);
										if(truongThongTin != null && truongThongTin.getKieuDuLieu() != null && truongThongTin.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DON_VI.getMa()){
											String dsDonVi = "";
	        								String nguoiDaiDien = "";
	        								String lanhDaoDonVi = "";
	        								Map<String, List<String>> chucVuUserMap = new HashMap();
	        								if(truongTT.getGiaTriChu() != null){
	        									String maDv = truongTT.getGiaTriChu();
	        									List<HtDonVi> donViList = htDonViServices.findByCode(maDv);
	        									HtDonVi donViCurrent = null;
	        									if(donViList != null && !donViList.isEmpty()){
	        										donViCurrent = donViList.get(0);
	        										if(donViCurrent != null && donViCurrent.getNguoiDaiDien() != null){
	        											nguoiDaiDien = donViCurrent.getNguoiDaiDien();
	        										}
	        									}
	        									List<User> users = identityService.createUserQuery().list();
	        									if(users != null && !users.isEmpty()){
	        										for(User u : users){
	        											String maDvTmp = identityService.getUserInfo(u.getId(), "HT_DON_VI");
	        											if(maDvTmp!= null && maDvTmp.equals(maDv)){
	        												dsDonVi+=u.getId()+",";
	        												String maCvTmp = identityService.getUserInfo(u.getId(), "HT_CHUC_VU");
	            											if(donViCurrent != null && donViCurrent.getHtChuVuCaoNhat() != null && donViCurrent.getHtChuVuCaoNhat().getMa().equals(maCvTmp)){
	            												lanhDaoDonVi = u.getId();
	            											}
	            											
	            											if(maCvTmp != null){
	            												List<String> userChucVu = chucVuUserMap.get(maCvTmp);
	            												if(userChucVu == null){
	            													userChucVu = new ArrayList();
	            													chucVuUserMap.put(maCvTmp, userChucVu);
	            												}
	            												if(!userChucVu.contains(u.getId())){
	            													userChucVu.add(u.getId());
	            												}
	            											}
	        											}
	        										}
	        									}
	        									if(dsDonVi.lastIndexOf(",") > 0){
	        										dsDonVi = dsDonVi.substring(0, dsDonVi.lastIndexOf(","));
	        									}
	        								}
	        								variables.put(truongTT.getMaTruongThongTin()+"_DS", dsDonVi);
	        								variables.put(truongTT.getMaTruongThongTin()+"_DAI_DIEN", nguoiDaiDien);
	        								variables.put(truongTT.getMaTruongThongTin()+"_LANH_DAO", lanhDaoDonVi);
	        								
	        								if(chucVuUserMap != null && !chucVuUserMap.isEmpty()){
	        									for(Map.Entry<String, List<String>> e : chucVuUserMap.entrySet()){
	        										if(e.getKey() != null){
	        											String userChucVuStr = "";
	        											List<String> userIds = e.getValue();
	        											if(userIds != null && userIds.isEmpty()){
	        												for(String str : userIds){
	        													userChucVuStr+=str+",";
	        												}
	        											}
	        											if(userChucVuStr.lastIndexOf(",") > 0){
	        												userChucVuStr = userChucVuStr.substring(0, userChucVuStr.lastIndexOf(","));
	                									}
	        											variables.put(truongTT.getMaTruongThongTin()+"_"+e.getKey().trim(), userChucVuStr);
	        										}
	        									}
	        								}
										}else if(truongThongTin != null && truongThongTin.getKieuDuLieu() != null && truongThongTin.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
											if(truongTT.getGiaTriNgay() != null){
	        									Date date = truongTT.getGiaTriNgay();
	        									SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
	        									variables.put(truongTT.getMaTruongThongTin()+"_ISO8601", df.format(date));
	        								}
										}
									}
								}
							}else if(truongTT.getGiaTriNgay() != null){
								variables.put(truongTT.getMaTruongThongTin(), truongTT.getGiaTriNgay());
								Date date = truongTT.getGiaTriNgay();
								SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
								variables.put(truongTT.getMaTruongThongTin()+"_ISO8601", df.format(date));
							}else if(truongTT.getGiaTriSo() != null){
								variables.put(truongTT.getMaTruongThongTin(), truongTT.getGiaTriSo());
							}
						}
					}
				}
				if(loop.getHoSoList() != null){
					for(EFormHoSo hs : loop.getHoSoList()){
						hs.setMaTemp(loop.getMaTemp() +"_"+ hs.getMaBieuMau());
						process.add(hs);
					}
					
				}
			}
		}
		return variables;
	}

}

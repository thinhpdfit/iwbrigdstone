package com.eform.servicetask.office;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.runtime.ProcessInstance;

import com.eform.common.SpringApplicationContext;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.type.EFormTruongThongTin;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.ProcessInstanceUtils;
import com.eform.model.entities.DocumentOrder;
import com.eform.model.entities.HoSo;
import com.eform.service.DocumentOrderService;
import com.eform.service.HoSoService;

public class CheckDocumentNumber implements JavaDelegate {
	private Expression documentNumberKey;
	private Expression bookKey;

	private HoSoService hoSoService;
	protected HistoryService historyService;
	private RuntimeService runtimeService;
	private DocumentOrderService documentOrderService;

	public void execute(DelegateExecution execution) throws Exception {
		boolean checkDocNumber = false;
		
		try {
			hoSoService = SpringApplicationContext.getApplicationContext().getBean(HoSoService.class);
			runtimeService = SpringApplicationContext.getApplicationContext().getBean(RuntimeService.class);
			historyService = SpringApplicationContext.getApplicationContext().getBean(HistoryService.class);
			documentOrderService = SpringApplicationContext.getApplicationContext().getBean(DocumentOrderService.class);
			
			String documentNumberKeyStr = (String) documentNumberKey.getValue(execution);
			String bookKeyStr = (String) bookKey.getValue(execution);
			
			if(documentNumberKeyStr != null && bookKeyStr != null){
				List<ProcessInstance> proInsList = runtimeService.createProcessInstanceQuery().processDefinitionId(execution.getProcessDefinitionId()).list();
				
				if(proInsList != null){
					List<ProcessInstance> proInsListTmp = new ArrayList();
					for (ProcessInstance processInstance : proInsList) {
						if(!processInstance.getId().equals(execution.getProcessInstanceId())){
							proInsListTmp.add(processInstance);
						}
					}
					
					String processInstanceId = execution.getProcessInstanceId();
					String processParentId = ProcessInstanceUtils.getParentInstance(processInstanceId, historyService);
					if (processParentId == null) {
						processParentId = processInstanceId;
					} 
					
					List<HoSo> hoSoList = hoSoService.getHoSoFind(null, null, null, processParentId, 0, 1);
					if(hoSoList != null && !hoSoList.isEmpty()){
						HoSo hoSoCurrent = hoSoList.get(0);
						String boHoSoXml = hoSoCurrent.getHoSoGiaTri();
						EFormBoHoSo boHS = CommonUtils.getEFormBoHoSo(boHoSoXml);
						EFormHoSo hoSo = boHS.getHoSo();
						Double soVBDen = null;
						String soVanBan = null;
						if(hoSo != null && hoSo.getTruongThongTinList() != null){
							List<EFormTruongThongTin> truongTTList = hoSo.getTruongThongTinList();
							EFormTruongThongTin truongTTSoDen = null;
							for(EFormTruongThongTin truongTT : truongTTList){
								if(documentNumberKeyStr.equals(truongTT.getMaTruongThongTin())){
									truongTTSoDen = truongTT;
									soVBDen = truongTT.getGiaTriSo();
								}
								if(bookKeyStr.equals(truongTT.getMaTruongThongTin())){
									soVanBan = truongTT.getGiaTriChu();
								}
							}
							
							if(soVanBan != null){
								Calendar cal = Calendar.getInstance();
								int currentYear = cal.get(Calendar.YEAR);
								String documentBookStr = currentYear+"/"+soVanBan;
								List<DocumentOrder> documentOrderList = documentOrderService.findByDocumentBook(documentBookStr);
								if(soVBDen != null){
									//check exist
									if(documentOrderList != null && !documentOrderList.isEmpty()){
										DocumentOrder current = documentOrderList.get(0);
										if(current != null){
											Long currentNumber = current.getOrder();
											if(soVBDen > currentNumber){
												checkDocNumber = true;
												execution.setVariable(documentNumberKeyStr, soVBDen);
											}
										}
									}else{
										checkDocNumber = true;
										execution.setVariable(documentNumberKeyStr, soVBDen);
									}
								}else{
									checkDocNumber = true;
									//generate doc number
									DocumentOrder current = null;
									if(documentOrderList != null && !documentOrderList.isEmpty()){
										current = documentOrderList.get(0);
									}
									if(current == null){
										current = new DocumentOrder();
										current.setDocumentBook(documentBookStr);
									}
									if(current.getOrder() == null){
										current.setOrder(0L);
									}
									Long nextNumber =  current.getOrder() + 1L;
									current.setOrder(nextNumber);
									documentOrderService.save(current);
									execution.setVariable(documentNumberKeyStr, nextNumber);
									
									if(truongTTSoDen==null){
										truongTTSoDen = new EFormTruongThongTin();
										truongTTSoDen.setMaTruongThongTin(documentNumberKeyStr);
										hoSo.getTruongThongTinList().add(truongTTSoDen);
									}
									
									if(truongTTSoDen != null){
										truongTTSoDen.setGiaTriSo(new Double(nextNumber));
									}
									
								}
								System.out.println(boHS);
								String hoSoXml = CommonUtils.getEFormBoHoSoString(boHS);
								hoSoCurrent.setHoSoGiaTri(hoSoXml);
								hoSoService.save(hoSoCurrent);
							}
							
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		execution.setVariable("CHECK_DOC_NUMBER", checkDocNumber);
	}
}
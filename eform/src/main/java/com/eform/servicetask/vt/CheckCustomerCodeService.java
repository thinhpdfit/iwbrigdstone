package com.eform.servicetask.vt;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;

public class CheckCustomerCodeService implements JavaDelegate {
	private Expression customerCodeKey;
	private Expression checkCustomerCodeKey;

	public void execute(DelegateExecution execution) throws Exception {

		String customerCodeKeyStr = (String) customerCodeKey.getValue(execution);
		String checkCustomerCodeKeyStr = (String) checkCustomerCodeKey.getValue(execution);
		boolean check = false;
		try {
			if (customerCodeKeyStr != null && !customerCodeKeyStr.isEmpty()) {
				String code = (String) execution.getVariable(customerCodeKeyStr);
				if ("KH0001".equals(code) 
						|| "KH0002".equals(code)
						|| "KH0003".equals(code)) {
					check = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(checkCustomerCodeKeyStr == null || checkCustomerCodeKeyStr.isEmpty()){
			checkCustomerCodeKeyStr = "CHECK_CODE";
		}
		execution.setVariable(checkCustomerCodeKeyStr, check);
	}
}
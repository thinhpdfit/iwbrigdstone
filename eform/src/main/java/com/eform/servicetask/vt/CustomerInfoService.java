package com.eform.servicetask.vt;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;

import com.Beans.CustomerInfo;

public class CustomerInfoService implements JavaDelegate {
	private Expression usdAmoutKey;

	public void execute(DelegateExecution execution) throws Exception {

		String usdAmoutKeyStr = (String) usdAmoutKey.getValue(execution);
		CustomerInfo info = new CustomerInfo();
		try {
			if (usdAmoutKeyStr != null && !usdAmoutKeyStr.isEmpty()) {
				info.setIncome((Long) execution.getVariable(usdAmoutKeyStr));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		execution.setVariable("customerInfo", info);
	}
}
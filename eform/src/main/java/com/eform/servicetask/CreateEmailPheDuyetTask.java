package com.eform.servicetask;

import java.io.File;
import java.util.List;
import java.util.Random;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.eform.common.SpringApplicationContext;
import com.eform.common.ThamSo;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.utils.CommonUtils;
import com.eform.model.entities.EmailPheDuyet;
import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;
import com.eform.model.entities.HoSo;
import com.eform.model.entities.HtThamSo;
import com.eform.security.custom.ActivitiUserDetails;
import com.eform.service.BieuMauService;
import com.eform.service.EmailPheDuyetServices;
import com.eform.service.FileManagementService;
import com.eform.service.HoSoService;
import com.eform.service.HtThamSoService;
import com.eform.ui.form.FormGenerate;

@Component
public class CreateEmailPheDuyetTask implements JavaDelegate {
	@Autowired
	private EmailPheDuyetServices emailPheDuyetServices;
	private RuntimeService runtimeService;
	private FileManagementService fileManagementService;
	private HoSoService hoSoService;
	private BieuMauService bieuMauService;
	private HtThamSoService htThamSoService;

	private Expression activityId;
	private Expression idVar;
	private Expression refCodeVar;

	private Expression attach;
	private Expression mailTo;
	private Expression subject;
	private Expression formKey;
	
	private Expression key;
	private Expression approveValue;
	private Expression refuseValue;
	
	private String uploadPath;

	public String getUserName() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (obj != null) {
			ActivitiUserDetails user = (ActivitiUserDetails) obj;
			return user.getUsername();
		}
		return "";
	}

	public String createRefCode() {
		String refCode = "";
		char[] charList = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
				'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a',
				'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
				'w', 'x', 'y', 'z' };
		Random rand = new Random();
		while (refCode.length() < 20) {
			refCode += charList[rand.nextInt(charList.length)];
		}
		return refCode;
	}

	public void execute(DelegateExecution execution) throws Exception {
		emailPheDuyetServices = SpringApplicationContext.getApplicationContext().getBean(EmailPheDuyetServices.class);
		runtimeService = SpringApplicationContext.getApplicationContext().getBean(RuntimeService.class);
		fileManagementService = SpringApplicationContext.getApplicationContext().getBean(FileManagementService.class);
		hoSoService = SpringApplicationContext.getApplicationContext().getBean(HoSoService.class);
		bieuMauService = SpringApplicationContext.getApplicationContext().getBean(BieuMauService.class);
		htThamSoService = SpringApplicationContext.getApplicationContext().getBean(HtThamSoService.class);
		uploadPath = "";
		
		HtThamSo uploadPathParam = htThamSoService.getHtThamSoFind(ThamSo.FOLDER_UPLOAD_PATH);
		if(uploadPathParam != null){
			uploadPath = uploadPathParam.getGiaTri();
		}
		
		String hostName = "";
		String fromAddress = "";
		String account = "";
		String mailPass = "";
		String mailCharset = "";
		boolean sslOnConnect = true;
		int smtpPort = 0;
		
		HtThamSo hostNameParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_HOST_NAME);
		if(hostNameParam != null){
			hostName = hostNameParam.getGiaTri();
		}
		
		HtThamSo fromAddressParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_FROM_ADDRESS);
		if(fromAddressParam != null){
			fromAddress = fromAddressParam.getGiaTri();
		}
		
		HtThamSo accountParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_ACCOUNT);
		if(accountParam != null){
			account = accountParam.getGiaTri();
		}
		HtThamSo mailPassParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_PASSWORD);
		if(mailPassParam != null){
			mailPass = mailPassParam.getGiaTri();
		}
		
		HtThamSo mailCharsetParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_CHARSET);
		if(mailCharsetParam != null){
			mailCharset = mailCharsetParam.getGiaTri();
		}
		
		HtThamSo sslOnConnectParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_SSL_ON_CONNECT);
		if(sslOnConnectParam != null && "1".equals(sslOnConnectParam.getGiaTri())){
			sslOnConnect = true;
		}
		
		HtThamSo smtpPortParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_SSL_ON_CONNECT);
		if(smtpPortParam != null && smtpPortParam.getGiaTri() != null && smtpPortParam.getGiaTri().matches("[0-9]+")){
			smtpPort = Integer.parseInt(smtpPortParam.getGiaTri());
		}
		
		
		
		String processInstanceId = execution.getProcessInstanceId();
		
		
		String refCode = createRefCode();

		
		
		String mailToStr = null;
		if(mailTo != null && mailTo.getValue(execution) != null){
			mailToStr = (String) mailTo.getValue(execution);
		}
		if(mailToStr != null && !mailToStr.isEmpty()){
			String subjectStr = (String) subject.getValue(execution);
			
			HtmlEmail email = new HtmlEmail();
			email.setHostName(hostName);
			email.setSmtpPort(smtpPort);
			email.setSSLOnConnect(sslOnConnect);
			email.setFrom(fromAddress);
			email.setAuthentication(account, mailPass);
			email.setCharset(mailCharset);
			email.setSubject(subjectStr);
			email.addTo(mailToStr);

			String attachStr = null;
			if (attach != null && attach.getValue(execution) != null) {
				attachStr = (String) attach.getValue(execution);
				String attachArr[] = attachStr.split(",");
				if (attachArr != null && attachArr.length > 0) {
					for (int i = 0; i < attachArr.length; i++) {
						if (attachArr[i] != null && !attachArr[i].trim().isEmpty()) {
							Object value = runtimeService.getVariable(processInstanceId, attachArr[i].trim());
							if (value != null && value instanceof String) {
								String str = (String) value;
								String fileIfArr[] = str.split(",");
								if (fileIfArr != null && fileIfArr.length > 0) {
									for (int j = 0; j < fileIfArr.length; j++) {
										if (fileIfArr[j] != null && fileIfArr[j].trim().matches("[0-9]+")) {
											FileManagement fileManagement = fileManagementService
													.findOne(new Long(fileIfArr[j].trim()));
											FileVersion versionMax = fileManagementService.getMaxVersion(fileManagement);
											if (versionMax != null && versionMax.getTrangThai() != null
													&& versionMax.getTrangThai().intValue() == 1
													&& versionMax.getSignedPath() != null) {
												try {
													File file = new File(uploadPath + versionMax.getSignedPath());
													email.attach(file);
												} catch (Exception e) {
													e.printStackTrace();
												}
												
											} else if (versionMax != null && versionMax.getUploadPath() != null) {
												try {
													File file = new File(uploadPath + versionMax.getUploadPath());
													email.attach(file);
												} catch (Exception e) {
													e.printStackTrace();
												}
												
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			
			String htmlData = "";
			  if(formKey  != null && formKey.getValue(execution) != null){
				  String formKeyStr=(String) formKey.getValue(execution);
				  List<HoSo> hoSoList = hoSoService.getHoSoFind(null, null, null, processInstanceId, 0, 1);
				  
				  if (hoSoList != null && !hoSoList.isEmpty()) {
						HoSo hoSo = hoSoList.get(0);
						String hoSoXml = null;
						String boHoSoXml = hoSo.getHoSoGiaTri();
						EFormBoHoSo boHS = CommonUtils.getEFormBoHoSo(boHoSoXml);
						if(boHS != null && boHS.getHoSo() != null){
							EFormHoSo hs = boHS.getHoSo();
							hoSoXml = CommonUtils.getEFormHoSoString(hs);
						}
						FormGenerate formGenerate = new FormGenerate(formKeyStr, hoSoXml);
						htmlData += formGenerate.getHtmlData();
						
				  }
			  }
			  
			  	String keyStr = "";
				String approveStr = "";
				String refuseStr = "";
				if(key != null){
					keyStr = (String) key.getValue(execution);
					
				}
				if(approveValue != null){
					approveStr = (String) approveValue.getValue(execution);
					if(keyStr != null && !keyStr.isEmpty() && approveStr != null && !approveStr.isEmpty()){
						approveStr = "?k="+keyStr+"&v="+approveStr;
					}
				}
				if(refuseValue != null){
					refuseStr = (String) refuseValue.getValue(execution);
					if(keyStr != null && !keyStr.isEmpty() && refuseStr != null && !refuseStr.isEmpty()){
						refuseStr = "?k="+keyStr+"&v="+refuseStr;
					}
				}
				
				String activityIdValue = null;
				if(activityId != null && activityId.getValue(execution) != null){
					activityIdValue = (String) activityId.getValue(execution);
				}
				System.out.println("User name:" + getUserName());
				EmailPheDuyet emailPheDuyet = new EmailPheDuyet();
				emailPheDuyet.setProcessInstanceId(execution.getProcessInstanceId());
				emailPheDuyet.setActivityId(activityIdValue);
				emailPheDuyet.setRefCode(refCode);
				emailPheDuyet.setTrangThai(new Long(1));
				emailPheDuyet.setTruongKySo(attachStr);
				emailPheDuyet = emailPheDuyetServices.update(emailPheDuyet);
				if(idVar != null){
					execution.setVariable((String) idVar.getValue(execution), emailPheDuyet.getId());
				}
				if(refCodeVar != null){
					execution.setVariable((String) refCodeVar.getValue(execution), emailPheDuyet.getRefCode());
				}
				String path = CommonUtils.getRootPath()+"/mail-approve/approve/";
				String id = emailPheDuyet.getId()+"";
				htmlData += "<br><a href='"+path+refCode+"/"+id+"/1"+approveStr+"'>Phê duyệt</a>";
				htmlData += "<br><a href='"+path+refCode+"/"+id+"/0"+refuseStr+"'>Từ chối</a>";
				email.setHtmlMsg(htmlData);
				email.send();
		}
		
		
	}
}
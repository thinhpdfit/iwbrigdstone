package com.eform.sync.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.sync.dao.SyncBieuMauDao;
import com.eform.sync.dao.SyncBmHangDao;
import com.eform.sync.dao.SyncBmODao;
import com.eform.sync.dao.SyncBmOTruongThongTinDao;
import com.eform.sync.dao.SyncDanhMucDao;
import com.eform.sync.dao.SyncLoaiDanhMucDao;
import com.eform.sync.dao.SyncTruongThongTinDao;
import com.eform.sync.dao.SynchronizationDao;
import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmHang;
import com.eform.sync.model.entities.SyncBmO;
import com.eform.sync.model.entities.SyncBmOTruongThongTin;
import com.eform.sync.model.entities.SyncDanhMuc;
import com.eform.sync.model.entities.SyncLoaiDanhMuc;
import com.eform.sync.model.entities.SyncTruongThongTin;

@Service
@Transactional(transactionManager = "eformTransactionManager")
public class SynchonizationServiceImpl implements SynchronizationService{

	@Autowired
	private SynchronizationDao synchronizationDao;@Autowired
	private SyncBieuMauDao bieuMauDao;
	@Autowired
	private SyncBmODao bmODao;
	@Autowired
	private SyncBmHangDao bmHangDao;
	@Autowired
	private SyncBmOTruongThongTinDao bmOTruongThongTinDao;
	@Autowired
	private SyncTruongThongTinDao truongThongTinDao;
	@Autowired
	private SyncDanhMucDao danhMucDao;
	@Autowired
	private SyncLoaiDanhMucDao loaiDanhMucDao;


	@Override
	public void insert(Object obj) {
		synchronizationDao.insert(obj);
	}

	@Override
	public void insert(List<Object> objList) {
		synchronizationDao.insertAll(objList);
	}

	@Override
	public void importBieuMau(List<SyncLoaiDanhMuc> loaiDanhMuc, List<SyncDanhMuc> danhMucList,
			List<SyncTruongThongTin> truongThongTinList, List<SyncBieuMau> bieuMauList, List<SyncBmHang> hangList, List<SyncBmO> oList,
			List<SyncBmOTruongThongTin> oTruongTTList) {
		if(bieuMauList != null){
			List<Long> bmIds = new ArrayList();
			for (SyncBieuMau bm : bieuMauList) {
				bmIds.add(0, bm.getId());
			}
			for(Long bmId : bmIds){
				synchronizationDao.deleteAllByBieuMauId(bmId);
			}
		}
		synchronizationDao.insertAll(loaiDanhMuc);
		synchronizationDao.insertAll(danhMucList);
		synchronizationDao.insertAll(truongThongTinList);
		synchronizationDao.insertAll(bieuMauList);
		synchronizationDao.insertAll(hangList);
		synchronizationDao.insertAll(oList);
		synchronizationDao.insertAll(oTruongTTList);
		
	}

	@Override
	public List<SyncBieuMau> findBieuMauByCode(String code) {
		return bieuMauDao.findByCode(code);
	}

	@Override
	public List<SyncBmHang> getBmHangFind(String ma, String ten, Long loaiHang, SyncBieuMau bieuMau, int firstResult,
			int maxResults) {
		return bmHangDao.getBmHangFind(ma, ten, loaiHang, bieuMau, firstResult, maxResults);
	}

	@Override
	public List<SyncBmO> getBmOFind(String ma, String ten, Long loaiO, SyncBieuMau bieuMau, SyncBmHang bmHang,
			SyncBieuMau bieuMauCon, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return bmODao.getBmOFind(ma, ten, loaiO, bieuMau, bmHang, bieuMauCon, firstResult, maxResults);
	}

	@Override
	public List<SyncBmOTruongThongTin> getBmOTruongThongTinFind(SyncBieuMau bieuMau, SyncBmHang bmHang, SyncBmO bmO,
			SyncTruongThongTin truongTT, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return bmOTruongThongTinDao.getBmOTruongThongTinFind(bieuMau, bmHang, bmO, truongTT, firstResult, maxResults);
	}

	@Override
	public List<SyncLoaiDanhMuc> getLoaiDanhMucFind(String ma, String ten, List<Long> trangThai,
			SyncLoaiDanhMuc loaiDanhMuc, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return loaiDanhMucDao.getLoaiDanhMucFind(ma, ten, trangThai, loaiDanhMuc, firstResult, maxResults);
	}

	@Override
	public List<SyncDanhMuc> getDanhMucFind(String ma, String ten, List<Long> trangThai, SyncLoaiDanhMuc loaiDanhMuc,
			SyncDanhMuc danhMuc, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return danhMucDao.getDanhMucFind(ma, ten, trangThai, loaiDanhMuc, danhMuc, firstResult, maxResults);
	}
}

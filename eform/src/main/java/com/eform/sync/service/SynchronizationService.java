package com.eform.sync.service;

import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;
import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmHang;
import com.eform.sync.model.entities.SyncBmO;
import com.eform.sync.model.entities.SyncBmOTruongThongTin;
import com.eform.sync.model.entities.SyncDanhMuc;
import com.eform.sync.model.entities.SyncLoaiDanhMuc;
import com.eform.sync.model.entities.SyncTruongThongTin;

public interface SynchronizationService {
	public void insert(Object obj);
	public void insert(List<Object> objList);
	public void importBieuMau(List<SyncLoaiDanhMuc> loaiDanhMuc,
			List<SyncDanhMuc> danhMucList, 
			List<SyncTruongThongTin> truongThongTinList, 
			List<SyncBieuMau> bieuMauList,
			List<SyncBmHang> hangList,
			List<SyncBmO> oList,
			List<SyncBmOTruongThongTin> oTruongTTList);
	
	public List<SyncBieuMau> findBieuMauByCode(String code);
	public List<SyncBmHang> getBmHangFind(String ma, String ten, Long loaiHang, SyncBieuMau bieuMau, int firstResult, int maxResults);
	public List<SyncBmO> getBmOFind(String ma, String ten, Long loaiO, SyncBieuMau bieuMau, SyncBmHang bmHang, SyncBieuMau bieuMauCon, int firstResult, int maxResults);
	public List<SyncBmOTruongThongTin> getBmOTruongThongTinFind(SyncBieuMau bieuMau, SyncBmHang bmHang, SyncBmO bmO , SyncTruongThongTin truongTT, int firstResult, int maxResults);
	public List<SyncLoaiDanhMuc> getLoaiDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			SyncLoaiDanhMuc loaiDanhMuc,
			int firstResult, 
			int maxResults);
	public List<SyncDanhMuc> getDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			SyncLoaiDanhMuc loaiDanhMuc,
			SyncDanhMuc danhMuc, 
			int firstResult, 
			int maxResults);
}

package com.eform.sync.dao;

import java.util.List;

import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmHang;


public interface SyncBmHangDaoCustom {
	public int deleteByBieuMau(SyncBieuMau bieuMau);
    List<SyncBmHang> getBmHangFind(String ma, String ten, Long loaiHang, SyncBieuMau bieuMau, int firstResult, int maxResults);
    Long getBmHangFind(String ma, String ten, Long loaiHang, SyncBieuMau bieuMau);
    public void insertWithId(List<SyncBmHang> list);
}

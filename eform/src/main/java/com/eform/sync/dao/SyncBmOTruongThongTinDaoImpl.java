package com.eform.sync.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmHang;
import com.eform.sync.model.entities.SyncBmO;
import com.eform.sync.model.entities.SyncBmOTruongThongTin;
import com.eform.sync.model.entities.SyncTruongThongTin;

@Repository

public class SyncBmOTruongThongTinDaoImpl implements SyncBmOTruongThongTinDaoCustom{

	@PersistenceContext private EntityManager em;
	
	@Override
	
	public int deleteByBieuMau(SyncBieuMau bieuMau) {
		if (bieuMau!= null) {
			Query query = em.createQuery("DELETE FROM SyncBmOTruongThongTin o WHERE o.bieuMau = :bieuMau");
            query.setParameter("bieuMau", bieuMau);
            int i = query.executeUpdate();
            return i;
        }
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SyncBmOTruongThongTin> getBmOTruongThongTinFind(SyncBieuMau bieuMau, SyncBmHang bmHang, SyncBmO bmO, SyncTruongThongTin truongTT,
			int firstResult, int maxResults) {
		String sql = "";
        
        if (bieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bieuMau = :bieuMau ");
        }
        if (bmHang!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bmHang = :bmHang ");
        }
        
        if (bmO!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bmO = :bmO ");
        }
        
        if (truongTT != null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.truongThongTin = :truongTT ");
        }
        Query query = em.createQuery("SELECT o FROM SyncBmOTruongThongTin o "+ sql);

        if (bieuMau!= null) {
            query.setParameter("bieuMau", bieuMau);
        }
        if (bmHang!= null) {
            query.setParameter("bmHang", bmHang);
        }

        if (bmO!= null) {
            query.setParameter("bmO", bmO);
        }
        if (truongTT!= null) {
            query.setParameter("truongTT", truongTT);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}

	@Override
	public Long getBmOTruongThongTinFind(SyncBieuMau bieuMau, SyncBmHang bmHang, SyncBmO bmO, SyncTruongThongTin truongTT) {
		String sql = "";
		if (bieuMau!= null) {
			
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bieuMau = :bieuMau ");
        }
        if (bmHang!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bmHang = :bmHang ");
        }
        
        if (bmO!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bmO = :bmO ");
        }
        
        if (truongTT != null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.truongThongTin = :truongTT ");
        }
        Query query = em.createQuery("SELECT count(o) FROM SyncBmOTruongThongTin o "+ sql);

        if (bieuMau!= null) {
            query.setParameter("bieuMau", bieuMau);
        }
        if (bmHang!= null) {
            query.setParameter("bmHang", bmHang);
        }

        if (bmO!= null) {
            query.setParameter("bmO", bmO);
        }
        if (truongTT!= null) {
            query.setParameter("truongTT", truongTT);
        }
        return (Long) query.getSingleResult();
	}

	@Override
	public List<SyncTruongThongTin> findTruongThongtinByBieuMau(List<SyncBieuMau> bieuMauList, Boolean tableDisplay, Boolean searchFormDisplay) {
		 String sql = "";
		 if(bieuMauList != null && !bieuMauList.isEmpty()){
			 if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
		 sql = (sql +"o.bieuMau in :bieuMauList ");
		 }
		 if (tableDisplay!= null) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"o.truongThongTin.tableDisplay = :tableDisplay ");
			 }	
		 if (searchFormDisplay!= null) {
	            if (sql.isEmpty()) {
	                sql = "WHERE ";
	            } else {
	                sql = (sql +"AND ");
	            }
	            sql = (sql +"o.truongThongTin.searchFormDisplay = :searchFormDisplay ");
			 }	
        Query query = em.createQuery((("SELECT o.truongThongTin FROM SyncBmOTruongThongTin o "+ sql)+" ORDER BY o.truongThongTin.tableDisplayOrder, o.truongThongTin.searchFormDisplayOrder "));
        if(bieuMauList != null && !bieuMauList.isEmpty()){
        	query.setParameter("bieuMauList", bieuMauList);
        }
        if (tableDisplay!= null) {
            if(tableDisplay){
            	query.setParameter("tableDisplay", 1L);
            }else{
            	query.setParameter("tableDisplay", 0L);
            }
		 }	
        if (searchFormDisplay!= null) {
        	if(searchFormDisplay){
            	query.setParameter("searchFormDisplay", 1L);
            }else{
            	query.setParameter("searchFormDisplay", 0L);
            }
		 }	
		return query.getResultList();
	}

	@Override
	public void insertWithId(List<SyncBmOTruongThongTin> list) {
		for (SyncBmOTruongThongTin bmOTruongThongTin : list) {
			em.merge(bmOTruongThongTin);
		}
	}
	
	
	
}


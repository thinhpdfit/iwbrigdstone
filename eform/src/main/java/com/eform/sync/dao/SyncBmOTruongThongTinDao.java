package com.eform.sync.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.sync.model.entities.SyncBmOTruongThongTin;

public interface SyncBmOTruongThongTinDao extends CrudRepository<SyncBmOTruongThongTin, Long>, SyncBmOTruongThongTinDaoCustom{

}

package com.eform.sync.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmHang;
import com.eform.sync.model.entities.SyncBmO;
import com.eform.sync.model.entities.SyncBmOTruongThongTin;

@Repository
public class SyncBieuMauDaoImpl implements SyncBieuMauDaoCustom{

	@PersistenceContext 
	private EntityManager em;
	
	@Autowired
	SyncBmODao bmODao;
	@Autowired
	SyncBmOTruongThongTinDao bmOTruongThongTinDao;
	@Autowired
	SyncBmHangDao bmHangDao;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SyncBieuMau> findByCode(String ma) {
		if(ma == null || ma.isEmpty()){
			return null;
		}
		String sql = "select ldm from SyncBieuMau ldm where UPPER(ma) = :ma";
		return  em.createQuery(sql).setParameter("ma", ma.toUpperCase()).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SyncBieuMau> findByCode(List<String> maList){
		if(maList == null || maList.isEmpty()){
			return new ArrayList<SyncBieuMau>();
		}
		String sql = "select ldm from SyncBieuMau ldm where ldm.ma in :maList";
		return  em.createQuery(sql).setParameter("maList", maList).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
    public List<SyncBieuMau> getBieuMauFind(String ma, String ten, Long loaiBieuMau, List<Long> trangThai, int firstResult, int maxResults) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (loaiBieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiBieuMau = :loaiBieuMau ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        Query query = em.createQuery((("SELECT o FROM SyncBieuMau o "+ sql)+" ORDER BY o.id DESC"));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (loaiBieuMau!= null) {
            query.setParameter("loaiBieuMau", loaiBieuMau);
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }

	@Override
    public Long getBieuMauFind(String ma, String ten, Long loaiBieuMau, List<Long> trangThai) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (loaiBieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiBieuMau = :loaiBieuMau ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        Query query = em.createQuery((("SELECT count(o) FROM SyncBieuMau o "+ sql)+""));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (loaiBieuMau!= null) {
            query.setParameter("loaiBieuMau", loaiBieuMau);
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        return ((Long) query.getSingleResult());
    }
	
	@Override
	@Transactional(transactionManager="eformTransactionManager")
	public void updateBieuMau(SyncBieuMau obj, List<SyncBmO> bmOList, List<SyncBmHang> bmHangList,
			List<SyncBmOTruongThongTin> oTruongTTList) {
		if (obj != null) {
			if (obj.getId() != null) {
				em.merge(obj);
				
//				bmOTruongThongTinDao.deleteByBieuMau(obj);
				em.createQuery("DELETE FROM SyncBmOTruongThongTin o WHERE o.bieuMau = :bieuMau").setParameter("bieuMau", obj).executeUpdate();

//				bmODao.deleteByBieuMau(obj);
				em.createQuery("DELETE FROM SyncBmO o WHERE o.bieuMau = :bieuMau").setParameter("bieuMau", obj).executeUpdate();
				
//				bmHangDao.deleteByBieuMau(obj);
				em.createQuery("DELETE FROM SyncBmHang o WHERE o.bieuMau = :bieuMau").setParameter("bieuMau", obj).executeUpdate();
				
				em.createQuery("DELETE FROM SyncFormula o WHERE o.bieuMau = :bieuMau").setParameter("bieuMau", obj).executeUpdate();
				
			} else {
				em.persist(obj);
			}
			
			if(bmHangList != null){
				for(SyncBmHang bmHang : bmHangList){
					bmHangDao.save(bmHang);
				}
			}
			
			if(bmOList != null){
				for(SyncBmO bmO : bmOList){
					bmODao.save(bmO);
				}
			}
			
			if(oTruongTTList != null){
				for(SyncBmOTruongThongTin oTruongTT : oTruongTTList){
					bmOTruongThongTinDao.save(oTruongTT);
				}
			}
		}
	}

	@Override
	public void deleteBieuMau(SyncBieuMau obj) {
		if (obj != null) {
			if (obj.getId() != null) {
				bmOTruongThongTinDao.deleteByBieuMau(obj);
				bmODao.deleteByBieuMau(obj);
				bmHangDao.deleteByBieuMau(obj);
				em.createQuery("DELETE FROM SyncFormula o WHERE o.bieuMau = :bieuMau").setParameter("bieuMau", obj).executeUpdate();
				em.remove(obj);
			} 
		}
	}

	@SuppressWarnings("unchecked")
	@Override
    public List<SyncBieuMau> getBieuMauFind(String ma, List<String> maList, String ten, Long loaiBieuMau, List<Long> trangThai, int firstResult, int maxResults) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((maList!= null)&&(maList.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) in :maList ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (loaiBieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiBieuMau = :loaiBieuMau ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        Query query = em.createQuery((("SELECT o FROM SyncBieuMau o "+ sql)+" ORDER BY o.id DESC "));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((maList!= null)&&(maList.isEmpty() == false)) {
            query.setParameter("maList", maList);
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (loaiBieuMau!= null) {
            query.setParameter("loaiBieuMau", loaiBieuMau);
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }

	@Override
    public Long getBieuMauFind(String ma, List<String> maList, String ten, Long loaiBieuMau, List<Long> trangThai) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((maList!= null)&&(maList.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ma in :maList ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (loaiBieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiBieuMau = :loaiBieuMau ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        Query query = em.createQuery((("SELECT count(o) FROM SyncBieuMau o "+ sql)+""));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((maList!= null)&&(maList.isEmpty() == false)) {
            query.setParameter("maList", maList);
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (loaiBieuMau!= null) {
            query.setParameter("loaiBieuMau", loaiBieuMau);
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        return ((Long) query.getSingleResult());
    }

	@Override
	public void insertWithId(List<SyncBieuMau> list) {
		for (SyncBieuMau bieuMau : list) {
			em.merge(bieuMau);
		}
	}


}
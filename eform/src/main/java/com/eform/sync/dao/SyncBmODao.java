package com.eform.sync.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.sync.model.entities.SyncBmO;

public interface SyncBmODao extends CrudRepository<SyncBmO, Long>, SyncBmODaoCustom{

}

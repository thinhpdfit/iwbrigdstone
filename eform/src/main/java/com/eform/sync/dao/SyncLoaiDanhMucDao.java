package com.eform.sync.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.model.entities.LoaiDanhMuc;

public interface SyncLoaiDanhMucDao extends CrudRepository<LoaiDanhMuc, Long>, SyncLoaiDanhMucDaoCustom{
}

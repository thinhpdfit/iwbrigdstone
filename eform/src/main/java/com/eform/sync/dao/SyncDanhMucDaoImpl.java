package com.eform.sync.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.sync.model.entities.SyncDanhMuc;
import com.eform.sync.model.entities.SyncLoaiDanhMuc;

@Repository
public class SyncDanhMucDaoImpl implements SyncDanhMucDaoCustom{

	@PersistenceContext private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SyncDanhMuc> findByCode(String ma) {
		if(ma == null || ma.isEmpty()){
			return null;
		}
		String sql = "select ldm from SyncDanhMuc ldm where UPPER(ma) = :ma";
		return  em.createQuery(sql).setParameter("ma", ma.toUpperCase()).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SyncDanhMuc> getDanhMucFind(String ma, String ten, List<Long> trangThai, SyncLoaiDanhMuc loaiDanhMuc,
			SyncDanhMuc danhMuc, int firstResult, int maxResults) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        if (loaiDanhMuc!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiDanhMuc = :loaiDanhMuc ");
        }
        if (danhMuc!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.danhMuc = :danhMuc ");
        }
        Query query = em.createQuery((("SELECT o FROM SyncDanhMuc o "+ sql)+" ORDER BY o.id DESC "));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if (loaiDanhMuc!= null) {
            query.setParameter("loaiDanhMuc", loaiDanhMuc);
        }
        if (danhMuc!= null) {
            query.setParameter("danhMuc", danhMuc);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}

	@Override
	public Long getDanhMucFind(String ma, String ten, List<Long> trangThai, SyncLoaiDanhMuc loaiDanhMuc, SyncDanhMuc danhMuc) {
        String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ma) like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"UPPER(o.ten) like :ten ");
        }
        if (trangThai!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.trangThai in :trangThai ");
        }
        if (loaiDanhMuc!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiDanhMuc = :loaiDanhMuc ");
        }
        if (danhMuc!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.danhMuc = :danhMuc ");
        }
        Query query = em.createQuery((("SELECT count(o) FROM SyncDanhMuc o "+ sql)+""));
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma.toUpperCase());
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten.toUpperCase());
        }
        if (trangThai!= null) {
            query.setParameter("trangThai", trangThai);
        }
        if (loaiDanhMuc!= null) {
            query.setParameter("loaiDanhMuc", loaiDanhMuc);
        }
        if (danhMuc!= null) {
            query.setParameter("danhMuc", danhMuc);
        }
        return ((Long) query.getSingleResult());
	}

	@Override
	public void insertWithId(List<SyncDanhMuc> ldanhMuc) {
		for (SyncDanhMuc danhMuc : ldanhMuc) {
			em.merge(danhMuc);
		}

		em.flush();
	}

}


package com.eform.sync.dao;

import java.util.List;

import com.eform.sync.model.entities.SyncLoaiDanhMuc;

public interface SyncLoaiDanhMucDaoCustom{
	public List<SyncLoaiDanhMuc> findByCode(String ma);
	public List<SyncLoaiDanhMuc> getLoaiDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			SyncLoaiDanhMuc loaiDanhMuc,
			int firstResult, 
			int maxResults);
	 public Long getLoaiDanhMucFind(
			 String ma, 
			 String ten, 
			 List<Long> trangThai, 
			 SyncLoaiDanhMuc loaiDanhMuc);
	 
	 public void insertWithId(List<SyncLoaiDanhMuc> loaiDanhMuc);

}

package com.eform.sync.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.model.entities.TruongThongTin;

@Transactional(transactionManager="eformTransactionManager")
public interface SyncTruongThongTinDao extends CrudRepository<TruongThongTin, Long>, SyncTruongThongTinDaoCustom{

}

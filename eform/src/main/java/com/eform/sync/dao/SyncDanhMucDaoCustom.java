package com.eform.sync.dao;

import java.util.List;

import com.eform.sync.model.entities.SyncDanhMuc;
import com.eform.sync.model.entities.SyncLoaiDanhMuc;

public interface SyncDanhMucDaoCustom {
	public List<SyncDanhMuc> findByCode(String ma);
	public List<SyncDanhMuc> getDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			SyncLoaiDanhMuc loaiDanhMuc,
			SyncDanhMuc danhMuc, 
			int firstResult, 
			int maxResults);

	public Long getDanhMucFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			SyncLoaiDanhMuc loaiDanhMuc, 
			SyncDanhMuc danhMuc);

	 public void insertWithId(List<SyncDanhMuc> ldanhMuc);
}

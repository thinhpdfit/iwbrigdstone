package com.eform.sync.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmHang;
import com.eform.sync.model.entities.SyncBmO;

@Repository

public class SyncBmODaoImpl implements SyncBmODaoCustom{

	@PersistenceContext private EntityManager em;
	
	@Override
	public int deleteByBieuMau(SyncBieuMau bieuMau) {
		if (bieuMau!= null) {
			Query query = em.createQuery("DELETE FROM SyncBmO o WHERE o.bieuMau = :bieuMau");
            query.setParameter("bieuMau", bieuMau);
            return query.executeUpdate();
        }
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SyncBmO> getBmOFind(String ma, String ten, Long loaiO, SyncBieuMau bieuMau, SyncBmHang bmHang, SyncBieuMau bieuMauCon, int firstResult,
			int maxResults) {
		String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ma like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ten like :ten ");
        }
        if (loaiO!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiO = :loaiO ");
        }
        if (bieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bieuMau = :bieuMau ");
        }
        if (bmHang!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bmHang = :bmHang ");
        }
        if (bieuMauCon!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bieuMauCon = :bieuMauCon ");
        }
        Query query = em.createQuery("SELECT o FROM SyncBmO o "+ sql+" order by o.thuTu ");
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma);
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten);
        }
        if (loaiO!= null) {
            query.setParameter("loaiO", loaiO);
        }
        if (bieuMau!= null) {
            query.setParameter("bieuMau", bieuMau);
        }
        if (bmHang!= null) {
            query.setParameter("bmHang", bmHang);
        }
        if (bieuMauCon!= null) {
            query.setParameter("bieuMauCon", bieuMauCon);
        }
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
	}

	@Override
	public Long getBmOFind(String ma, String ten, Long loaiO, SyncBieuMau bieuMau, SyncBmHang bmHang) {
		String sql = "";
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ma like :ma ");
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.ten like :ten ");
        }
        if (loaiO!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.loaiO = :loaiO ");
        }
        if (bieuMau!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bieuMau = :bieuMau ");
        }
        if (bmHang!= null) {
            if (sql.isEmpty()) {
                sql = "WHERE ";
            } else {
                sql = (sql +"AND ");
            }
            sql = (sql +"o.bmHang = :bmHang ");
        }
        Query query = em.createQuery("SELECT count(o) FROM SyncBmO o "+ sql);
        if ((ma!= null)&&(ma.isEmpty() == false)) {
            query.setParameter("ma", ma);
        }
        if ((ten!= null)&&(ten.isEmpty() == false)) {
            query.setParameter("ten", ten);
        }
        if (loaiO!= null) {
            query.setParameter("loaiO", loaiO);
        }
        if (bieuMau!= null) {
            query.setParameter("bieuMau", bieuMau);
        }
        if (bmHang!= null) {
            query.setParameter("bmHang", bmHang);
        }
        return (Long) query.getSingleResult();
	}

	@Override
	public void insertWithId(List<SyncBmO> list) {
		for (SyncBmO bmO : list) {
			em.merge(bmO);
		}
	}
	
	

}


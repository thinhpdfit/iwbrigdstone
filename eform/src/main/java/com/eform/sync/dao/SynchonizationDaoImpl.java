package com.eform.sync.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class SynchonizationDaoImpl implements SynchronizationDao{

	@PersistenceContext private EntityManager em;

	@Override
	public void insert(Object obj) {
		em.persist(obj);
	}

	@Override
	public void insertAll(List<? extends Object> objList) {
		if(objList != null){
			for (Object object : objList) {
				em.merge(object);
				//em.persist(object);
			}
			em.flush();
		}
	}

	@Override
	public void deleteAllByBieuMauId(Long bieuMauId) {
		String sql1 = "delete from BmOTruongThongTin o where o.bieuMau.id = :bieuMauId";
		String sql2 = "delete from BmO o where o.bieuMau.id = :bieuMauId";
		String sql3 = "delete from BmHang o where o.bieuMau.id = :bieuMauId";
		String sql4 = "delete from BieuMau o where o.id = :bieuMauId";
		em.createQuery(sql1).setParameter("bieuMauId", bieuMauId).executeUpdate();
		em.createQuery(sql2).setParameter("bieuMauId", bieuMauId).executeUpdate();
		em.createQuery(sql3).setParameter("bieuMauId", bieuMauId).executeUpdate();
		em.createQuery(sql4).setParameter("bieuMauId", bieuMauId).executeUpdate();
	}
}

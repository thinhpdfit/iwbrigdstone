package com.eform.sync.dao;

import java.util.List;

import com.eform.sync.model.entities.SyncLoaiDanhMuc;
import com.eform.sync.model.entities.SyncTruongThongTin;


public interface SyncTruongThongTinDaoCustom {
	
	public List<SyncTruongThongTin> findByCode(String ma);
	
	public List<SyncTruongThongTin> getTruongThongTinFind(
			String ma, 
			String ten, 
			List<Long> trangThai, 
			SyncLoaiDanhMuc loaiDanhMuc, 
			List<Long> kieuDuLieu,
			List<String> viTriHienThi, 
			int firstResult, 
			int maxResults);
	
	public Long getTruongThongTinFind(
    		String ma, 
    		String ten, 
    		List<Long> trangThai, 
    		SyncLoaiDanhMuc loaiDanhMuc,
			List<Long> kieuDuLieu,
			List<String> viTriHienThi);
	public void insertWithId(List<SyncTruongThongTin> list);
}

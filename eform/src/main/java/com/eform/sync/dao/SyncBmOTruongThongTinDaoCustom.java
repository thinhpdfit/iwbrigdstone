package com.eform.sync.dao;

import java.util.List;

import com.eform.model.entities.BmO;
import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmHang;
import com.eform.sync.model.entities.SyncBmO;
import com.eform.sync.model.entities.SyncBmOTruongThongTin;
import com.eform.sync.model.entities.SyncTruongThongTin;


public interface SyncBmOTruongThongTinDaoCustom {
	public int deleteByBieuMau(SyncBieuMau bieuMau);
    List<SyncBmOTruongThongTin> getBmOTruongThongTinFind(SyncBieuMau bieuMau, SyncBmHang bmHang, SyncBmO bmO , SyncTruongThongTin truongTT, int firstResult, int maxResults);
    Long getBmOTruongThongTinFind(SyncBieuMau bieuMau, SyncBmHang bmHang, SyncBmO bmO, SyncTruongThongTin truongTT);
    List<SyncTruongThongTin> findTruongThongtinByBieuMau(List<SyncBieuMau> bieuMauList, Boolean tableDisplay, Boolean searchFormDisPlay);
    public void insertWithId(List<SyncBmOTruongThongTin> list);
}

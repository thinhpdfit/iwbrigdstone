package com.eform.sync.dao;

import java.util.List;

public interface SynchronizationDao {

	public void insert(Object obj);
	public void insertAll(List<? extends Object> objList);
	public void deleteAllByBieuMauId(Long bieuMauId);
}

package com.eform.sync.dao;

import org.springframework.data.repository.CrudRepository;

import com.eform.sync.model.entities.SyncBieuMau;

public interface SyncBieuMauDao extends CrudRepository<SyncBieuMau, Long>, SyncBieuMauDaoCustom{

}

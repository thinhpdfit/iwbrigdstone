package com.eform.sync.dao;

import java.util.List;

import com.eform.model.entities.BmO;
import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmHang;
import com.eform.sync.model.entities.SyncBmO;


public interface SyncBmODaoCustom {
	public int deleteByBieuMau(SyncBieuMau bieuMau);
	List<SyncBmO> getBmOFind(String ma, String ten, Long loaiO, SyncBieuMau bieuMau, SyncBmHang bmHang, SyncBieuMau bieuMauCon, int firstResult, int maxResults);
    Long getBmOFind(String ma, String ten, Long loaiO, SyncBieuMau bieuMau, SyncBmHang bmHang);

	 public void insertWithId(List<SyncBmO> list);
}

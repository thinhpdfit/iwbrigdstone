package com.eform.sync.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eform.sync.model.entities.SyncDanhMuc;

@Repository
public interface SyncDanhMucDao extends CrudRepository<SyncDanhMuc, Long>, SyncDanhMucDaoCustom{

}

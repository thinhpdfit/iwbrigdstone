package com.eform.sync.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.eform.sync.model.entities.SyncBmHang;


@Transactional(transactionManager="eformTransactionManager")
public interface SyncBmHangDao extends CrudRepository<SyncBmHang, Long>, SyncBmHangDaoCustom{

}

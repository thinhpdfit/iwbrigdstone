package com.eform.sync.dao;

import java.util.List;

import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncBmHang;
import com.eform.sync.model.entities.SyncBmO;
import com.eform.sync.model.entities.SyncBmOTruongThongTin;

public interface SyncBieuMauDaoCustom {
	public List<SyncBieuMau> findByCode(String ma);
	
	public List<SyncBieuMau> findByCode(List<String> maList);
	
	public List<SyncBieuMau> getBieuMauFind(
			String ma, 
			String ten, 
			Long loaiBieuMau, 
			List<Long> trangThai, 
			int firstResult, 
			int maxResults);

    public Long getBieuMauFind(
    		String ma, 
    		String ten, 
    		Long loaiBieuMau, 
    		List<Long> trangThai);
    
    public void updateBieuMau(
    		SyncBieuMau obj, 
    		List<SyncBmO> bmOList, 
    		List<SyncBmHang> bmHangList, 
    		List<SyncBmOTruongThongTin> oTruongTTList);
    
    public void deleteBieuMau(SyncBieuMau obj);
    
	public List<SyncBieuMau> getBieuMauFind(
			String ma, 
			List<String> maList, 
			String ten, 
			Long loaiBieuMau, 
			List<Long> trangThai, 
			int firstResult, 
			int maxResults);

    public Long getBieuMauFind(
    		String ma, 
    		List<String> maList, 
    		String ten, 
    		Long loaiBieuMau, 
    		List<Long> trangThai);
    public void insertWithId(List<SyncBieuMau> list);
}

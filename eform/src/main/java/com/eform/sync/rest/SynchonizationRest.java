package com.eform.sync.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.eform.common.utils.ExportUtils;
import com.eform.common.utils.ModelerUtils;
import com.eform.sync.type.SyncModel;
import com.eform.sync.type.SyncResultModel;

@RestController
@RequestMapping(path="/sync")
public class SynchonizationRest {

	@RequestMapping(path = { "/", ""}, method = RequestMethod.POST)
	public ResponseEntity<SyncResultModel> sync(@RequestBody SyncModel syncModel, HttpServletRequest req, HttpServletResponse res){
		SyncResultModel result = new SyncResultModel();
		try {
			if(syncModel != null){
				if(syncModel.getBieuMauStr() != null){
					ExportUtils.importBieuMau(syncModel.getBieuMauStr());
				}
				if(syncModel.getModolerStr() != null){
					ModelerUtils.deployUploadedJson(syncModel.getModolerStr());
				}
				result.setSuccess(true);
				result.setLog("success");
				return new ResponseEntity<SyncResultModel>(result, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setLog(e.getMessage());
		}
		result.setSuccess(false);
		return new ResponseEntity<SyncResultModel>(result, HttpStatus.BAD_REQUEST);
	}

}

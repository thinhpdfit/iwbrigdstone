
package com.eform.sync.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.eform.common.type.jpa.JSONObjectUserType;
import com.eform.model.entities.BieuMau;

@Entity
@Table(name = "BIEU_MAU")
@Cacheable(false)
@TypeDefs({ @TypeDef(name = "CustomJsonObject", typeClass = JSONObjectUserType.class) })
public class SyncBieuMau
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "LOAI_BIEU_MAU")
    private Long loaiBieuMau;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @Column(name = "JASPER_JRXML")
    private String jasperJrxml;
    @Column(name = "JASPER")
    private byte[] jasper;
    @Column(name = "SO_HANG")
    private Long soHang;
    @Column(name = "SO_COT")
    private Long soCot;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getLoaiBieuMau() {
        return loaiBieuMau;
    }

    public void setLoaiBieuMau(Long loaiBieuMau) {
        this.loaiBieuMau = loaiBieuMau;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

    public String getJasperJrxml() {
        return jasperJrxml;
    }

    public void setJasperJrxml(String jasperJrxml) {
        this.jasperJrxml = jasperJrxml;
    }

    public byte[] getJasper() {
        return jasper;
    }

    public void setJasper(byte[] jasper) {
        this.jasper = jasper;
    }

	public Long getSoHang() {
		return soHang;
	}

	public void setSoHang(Long soHang) {
		this.soHang = soHang;
	}

	public Long getSoCot() {
		return soCot;
	}

	public void setSoCot(Long soCot) {
		this.soCot = soCot;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyncBieuMau other = (SyncBieuMau) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	public SyncBieuMau() {
		super();
	}

	public SyncBieuMau(BieuMau bm) {
		super();
		this.id = bm.getId();
		this.ma = bm.getMa();
		this.ten = bm.getTen();
		this.loaiBieuMau = bm.getLoaiBieuMau();
		this.trangThai = bm.getTrangThai();
		this.jasperJrxml = bm.getJasperJrxml();
		this.jasper = bm.getJasper();
		this.soHang = bm.getSoHang();
		this.soCot = bm.getSoCot();
	}
	
	

}

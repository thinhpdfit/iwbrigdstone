
package com.eform.sync.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BM_O_TRUONG_THONG_TIN")
@Cacheable(false)
public class SyncBmOTruongThongTin
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "THU_TU")
    private Long thuTu;
    @ManyToOne
    @JoinColumn(name = "BIEU_MAU_ID")
    private SyncBieuMau bieuMau;
    @ManyToOne
    @JoinColumn(name = "BM_HANG_ID")
    private SyncBmHang bmHang;
    @ManyToOne
    @JoinColumn(name = "BM_O_ID")
    private SyncBmO bmO;
    @ManyToOne
    @JoinColumn(name = "TRUONG_THONG_TIN_ID")
    private SyncTruongThongTin truongThongTin;

    @Column(name = "READONLY")
    private Long readOnly;
    @Column(name = "constrant")
    private String constrant;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof SyncBmOTruongThongTin)) {
            areEqual = true;
            final SyncBmOTruongThongTin otherBmOTruongThongTin = ((SyncBmOTruongThongTin) other);
            if ((this.id == null)||(otherBmOTruongThongTin.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBmOTruongThongTin.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        SyncBmOTruongThongTin cloneBmOTruongThongTin = new SyncBmOTruongThongTin();
        cloneBmOTruongThongTin.setId(getId());
        cloneBmOTruongThongTin.setThuTu(getThuTu());
        return cloneBmOTruongThongTin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public SyncBieuMau getBieuMau() {
        return bieuMau;
    }

    public void setBieuMau(SyncBieuMau bieuMau) {
        this.bieuMau = bieuMau;
    }

    public SyncBmHang getBmHang() {
        return bmHang;
    }

    public void setBmHang(SyncBmHang bmHang) {
        this.bmHang = bmHang;
    }

    public SyncBmO getBmO() {
        return bmO;
    }

    public void setBmO(SyncBmO bmO) {
        this.bmO = bmO;
    }

    public SyncTruongThongTin getTruongThongTin() {
        return truongThongTin;
    }

    public void setTruongThongTin(SyncTruongThongTin truongThongTin) {
        this.truongThongTin = truongThongTin;
    }

	public Long getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(Long readOnly) {
		this.readOnly = readOnly;
	}

	public String getConstrant() {
		return constrant;
	}

	public void setConstrant(String constrant) {
		this.constrant = constrant;
	}
    
    

}

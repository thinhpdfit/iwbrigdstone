package com.eform.sync.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.eform.model.entities.BmHang;

@Entity
@Table(name = "BM_HANG")
@Cacheable(false)
public class SyncBmHang
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "LOAI_HANG")
    private Long loaiHang;
    @Column(name = "THU_TU")
    private Long thuTu;
    @ManyToOne
    @JoinColumn(name = "BIEU_MAU_ID")
    private SyncBieuMau bieuMau;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof SyncBmHang)) {
            areEqual = true;
            final SyncBmHang otherBmHang = ((SyncBmHang) other);
            if ((this.id == null)||(otherBmHang.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBmHang.id.equals(this.id));
        }
        return areEqual;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getLoaiHang() {
        return loaiHang;
    }

    public void setLoaiHang(Long loaiHang) {
        this.loaiHang = loaiHang;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public SyncBieuMau getBieuMau() {
        return bieuMau;
    }

    public void setBieuMau(SyncBieuMau bieuMau) {
        this.bieuMau = bieuMau;
    }

	public SyncBmHang() {
		super();
	}

	public SyncBmHang(BmHang hang) {
		super();
		this.id = hang.getId();
		this.ma = hang.getMa();
		this.ten = hang.getTen();
		this.loaiHang = hang.getLoaiHang();
		this.thuTu = hang.getThuTu();
		if(hang.getBieuMau() != null){
			SyncBieuMau bm = new SyncBieuMau(hang.getBieuMau());
			this.bieuMau = bm;
		}
		
	}

    
}

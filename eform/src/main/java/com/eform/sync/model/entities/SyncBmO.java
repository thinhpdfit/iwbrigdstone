
package com.eform.sync.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BM_O")
@Cacheable(false)
@SequenceGenerator(name = "BM_O_SEQ_GEN", sequenceName = "BM_O_SEQ", initialValue = 1)
public class SyncBmO
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "GHEP_COT")
    private Long ghepCot;
    @Column(name = "GHEP_HANG")
    private Long ghepHang;
    @Column(name = "LOAI_O")
    private Long loaiO;
    @Column(name = "THU_TU")
    private Long thuTu;
    @Column(name = "VI_TRI")
    private Long viTri;
    @ManyToOne
    @JoinColumn(name = "BIEU_MAU_ID")
    private SyncBieuMau bieuMau;
    @ManyToOne
    @JoinColumn(name = "BIEU_MAU_CON_ID")
    private SyncBieuMau bieuMauCon;
    @ManyToOne
    @JoinColumn(name = "BM_HANG_ID")
    private SyncBmHang bmHang; 

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof SyncBmO)) {
            areEqual = true;
            final SyncBmO otherBmO = ((SyncBmO) other);
            if ((this.id == null)||(otherBmO.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBmO.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        SyncBmO cloneBmO = new SyncBmO();
        cloneBmO.setId(getId());
        cloneBmO.setMa(getMa());
        cloneBmO.setTen(getTen());
        cloneBmO.setGhepCot(getGhepCot());
        cloneBmO.setGhepHang(getGhepHang());
        cloneBmO.setLoaiO(getLoaiO());
        cloneBmO.setThuTu(getThuTu());
        cloneBmO.setViTri(getViTri());
        return cloneBmO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getGhepCot() {
        return ghepCot;
    }

    public void setGhepCot(Long ghepCot) {
        this.ghepCot = ghepCot;
    }

    public Long getGhepHang() {
        return ghepHang;
    }

    public void setGhepHang(Long ghepHang) {
        this.ghepHang = ghepHang;
    }

    public Long getLoaiO() {
        return loaiO;
    }

    public void setLoaiO(Long loaiO) {
        this.loaiO = loaiO;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public SyncBieuMau getBieuMau() {
        return bieuMau;
    }

    public void setBieuMau(SyncBieuMau bieuMau) {
        this.bieuMau = bieuMau;
    }

    public SyncBieuMau getBieuMauCon() {
        return bieuMauCon;
    }

    public void setBieuMauCon(SyncBieuMau bieuMauCon) {
        this.bieuMauCon = bieuMauCon;
    }

    public SyncBmHang getBmHang() {
        return bmHang;
    }

    public void setBmHang(SyncBmHang bmHang) {
        this.bmHang = bmHang;
    }

	public Long getViTri() {
		return viTri;
	}

	public void setViTri(Long viTri) {
		this.viTri = viTri;
	}

    
}

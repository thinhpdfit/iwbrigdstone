
package com.eform.sync.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOAI_DANH_MUC")
@Cacheable(false)

public class SyncLoaiDanhMuc
    implements Serializable
{
    @Id
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @ManyToOne
    @JoinColumn(name = "LOAI_DANH_MUC_ID")
    private SyncLoaiDanhMuc loaiDanhMuc;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof SyncLoaiDanhMuc)) {
            areEqual = true;
            final SyncLoaiDanhMuc otherLoaiDanhMuc = ((SyncLoaiDanhMuc) other);
            if ((this.id == null)||(otherLoaiDanhMuc.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherLoaiDanhMuc.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        SyncLoaiDanhMuc cloneLoaiDanhMuc = new SyncLoaiDanhMuc();
        cloneLoaiDanhMuc.setId(getId());
        cloneLoaiDanhMuc.setMa(getMa());
        cloneLoaiDanhMuc.setTen(getTen());
        cloneLoaiDanhMuc.setTrangThai(getTrangThai());
        return cloneLoaiDanhMuc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

    public SyncLoaiDanhMuc getLoaiDanhMuc() {
        return loaiDanhMuc;
    }

    public void setLoaiDanhMuc(SyncLoaiDanhMuc loaiDanhMuc) {
        this.loaiDanhMuc = loaiDanhMuc;
    }

}

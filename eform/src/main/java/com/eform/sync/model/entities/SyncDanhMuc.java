
package com.eform.sync.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DANH_MUC")
@Cacheable(false)
public class SyncDanhMuc
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @Column(name = "THU_TU")
    private Long thuTu;
    @ManyToOne
    @JoinColumn(name = "LOAI_DANH_MUC_ID")
    private SyncLoaiDanhMuc loaiDanhMuc;
    @ManyToOne
    @JoinColumn(name = "DANH_MUC_ID")
    private SyncDanhMuc danhMuc;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof SyncDanhMuc)) {
            areEqual = true;
            final SyncDanhMuc otherDanhMuc = ((SyncDanhMuc) other);
            if ((this.id == null)||(otherDanhMuc.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherDanhMuc.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        SyncDanhMuc cloneDanhMuc = new SyncDanhMuc();
        cloneDanhMuc.setId(getId());
        cloneDanhMuc.setMa(getMa());
        cloneDanhMuc.setTen(getTen());
        cloneDanhMuc.setTrangThai(getTrangThai());
        cloneDanhMuc.setThuTu(getThuTu());
        return cloneDanhMuc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

    public Long getThuTu() {
		return thuTu;
	}

	public void setThuTu(Long thuTu) {
		this.thuTu = thuTu;
	}

	public SyncLoaiDanhMuc getLoaiDanhMuc() {
        return loaiDanhMuc;
    }

    public void setLoaiDanhMuc(SyncLoaiDanhMuc loaiDanhMuc) {
        this.loaiDanhMuc = loaiDanhMuc;
    }

    public SyncDanhMuc getDanhMuc() {
        return danhMuc;
    }

    public void setDanhMuc(SyncDanhMuc danhMuc) {
        this.danhMuc = danhMuc;
    }

}

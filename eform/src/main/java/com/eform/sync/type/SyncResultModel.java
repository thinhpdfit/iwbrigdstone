package com.eform.sync.type;

public class SyncResultModel {
	private Boolean success;
	private String log;
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	public SyncResultModel() {
		super();
	}
	
}

package com.eform.sync.type;

public class SyncModel {
	private String bieuMauStr;
	private String modolerStr;
	public String getBieuMauStr() {
		return bieuMauStr;
	}
	public void setBieuMauStr(String bieuMauStr) {
		this.bieuMauStr = bieuMauStr;
	}
	public String getModolerStr() {
		return modolerStr;
	}
	public void setModolerStr(String modolerStr) {
		this.modolerStr = modolerStr;
	}
	public SyncModel() {
		super();
	}
	
	
}

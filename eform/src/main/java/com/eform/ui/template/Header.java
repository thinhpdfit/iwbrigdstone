package com.eform.ui.template;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.Picture;
import org.activiti.engine.identity.User;
import org.activiti.engine.task.Task;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.SpringApplicationContext;
import com.eform.common.ThamSo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.FileUtils;
import com.eform.common.utils.CommonUtils.ENotificationStatus;
import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.Notification;
import com.eform.security.CustomWebAuthenticationDetails;
import com.eform.security.custom.ActivitiUserDetails;
import com.eform.service.HtThamSoService;
import com.eform.service.NotificationService;
import com.eform.ui.view.SolrSearchView;
import com.eform.ui.view.system.ProfileView;
import com.eform.ui.view.task.InboxView;
import com.eform.ui.view.task.InvoledView;
import com.eform.ui.view.task.MyTaskView;
import com.eform.ui.view.task.QueueView;
import com.eform.ui.view.task.TaskDetailView;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;


public class Header extends CssLayout implements ClickListener{

	private static final long serialVersionUID = 1L;
	private final int WIDTH_FULL = 100;
	private final String BUTTON_TOGGLE_MENU_ID = "btnToggleMenu";
	private final String BUTTON_LOGOUT_ID = "btnLogout";
	private final String BUTTON_LOGOUT_GOOGLE_ID = "btnLogoutGoogle";
	private final String BUTTON_MY_PRO_ID = "btnMyProfile";
	

	private final String BUTTON_INBOX_ID = "btnInbox";
	private final String BUTTON_MY_TASK_ID = "btnMyTask";
	private final String BUTTON_QUEUE_ID = "btnQueue";
	private final String BUTTON_INVOLED_ID = "btnInvoled";
	private final String BUTTON_HELP_ID = "btnHelp";
	private final String BUTTON_NOTIF_ITEM_ID = "btnNotifItem";
	private final String BUTTON_NOTIF_ID = "btnNotif";
	private final String BUTTON_VIEW_MORE_ID = "btnViewMoreNotif";
	
	private CssLayout headerLeft;
	private CssLayout headerRight;
	private CssLayout mainMenuBarWrap;
	private CssLayout logo;
	private boolean isSmallMenu = false;
	protected IdentityService identityService;
	private TaskService taskService;
	private HtThamSoService htThamSoService;
	private Window helpWindow;
	private NotificationService notificationService;
	protected HistoryService historyService;
	
	private Label notificationNumber;
	private CssLayout notificationContainer;
	private int notifLoaded;
	
	public Header(){
		init();
	}
	
	public void init(){
		notifLoaded = 0;
		taskService = SpringApplicationContext.getApplicationContext().getBean(TaskService.class);
		historyService = SpringApplicationContext.getApplicationContext().getBean(HistoryService.class);
		identityService = SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
		htThamSoService = SpringApplicationContext.getApplicationContext().getBean(HtThamSoService.class);
		notificationService = SpringApplicationContext.getApplicationContext().getBean(NotificationService.class);
		setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		setStyleName(ExplorerLayout.HEADER);
		initHeaderLeft();
		initHeaderRight();
	}
	
	public void initHeaderLeft(){
		headerLeft = new CssLayout();
		headerLeft.setStyleName(ExplorerLayout.HEADER_LEFT);

// bridgestone Logo
		try {
			CustomLayout bLogo=new CustomLayout(new ByteArrayInputStream("<svg viewBox=\"0 0 638.928 445.271\" id=\"shape-bridgestone-logo\" width=\"100%\" height=\"100%\"><title>bridgestone-logo</title><path fill=\"none\" d=\"M36 36h566.928v373.271H36zM1.984 409.271h25.512M36 443.286v-25.512M1.984 36h25.512M36 1.984v25.512M636.943 36h-25.511m-8.504-34.016v25.512m34.015 381.775h-25.511m-8.504 34.015v-25.512M0 222.635h24m614.928 0h-24M319.464 0v24m0 421.271v-24\"></path><path d=\"M276.376 378.52l-5.577 20.386c-1.181.103-2.65.274-4.91.274-10.914 0-9.699-3.284-5.094-21.049 4.789-18.512 7.535-21.844 14.367-21.844 6.543 0 6.582 4.398 4.938 9.967h16.615c2.717-10.412.897-19.644-18.553-19.644-21.615 0-27.521 4.559-34.482 31.521-6.479 25.07-4.352 31.782 19.02 31.782 11.109 0 15.737-.815 22.522-2.083l7.798-29.311h-16.644zm39.603 20.088c-2.166 0-2.931-.631-2.54-2.403l3.785-14.771h22.242l2.537-9.677h-22.252l2.527-9.854c.648-2.57 2.47-4.763 6.061-4.763h17.76l2.564-9.671h-25.9c-10.739 0-13.916 1.908-18.109 18.072l-7.674 29.517c-2.372 9.935.562 14.212 8.684 14.212h22.287c4.404 0 7.223-2.329 8.59-5.948l1.845-4.714h-22.407zm254.294 0c-2.191 0-2.93-.631-2.518-2.403l3.777-14.771h22.227l2.502-9.677h-22.223l2.5-9.854c.676-2.57 2.502-4.763 6.086-4.763h17.832l2.475-9.671h-25.865c-10.73 0-13.957 1.908-18.129 18.072l-7.682 29.517c-2.367 9.935.574 14.212 8.682 14.212h22.34c4.369 0 7.221-2.329 8.574-5.948l1.834-4.714h-22.412zM218.362 347.47h-22.227l-15.526 59.962c-.256.997.337 1.839 1.363 1.839H201.8c24.902 0 29.074-4.745 35.924-31.142 5.944-22.988 2.258-30.659-19.362-30.659zm-14.225 51.232h-4.566l10.713-41.527c1.404 0 1.924-.069 5.354-.069 9.64 0 9.64 3.201 5.026 21.023-4.654 17.975-7.336 20.573-16.527 20.573zm325.533-51.36c-12.816 0-18.785.294-22.24.128l-15.525 59.962c-.277.997.328 1.839 1.355 1.839h14.857l13.5-52.096c1.367 0 3.422-.13 5.527-.13 7.234 0 8.176 2.561 6.279 9.947l-10.439 40.439c-.264.997.316 1.839 1.316 1.839h14.875l10.943-42.278c3.794-14.656-2.663-19.65-20.448-19.65zm-128.777.128l-2.531 10.031h16.48c.209 0 .209.212.018.319-2.635 1.971-4.262 4.839-4.998 7.803l-10.82 41.809c-.254.997.332 1.839 1.309 1.839h15.988l13.393-51.77h8.191c4.588 0 7.416-1.913 8.949-5.918l1.549-4.113h-47.528zm-231.502 61.801l15.531-59.959c.269-1.001-.342-1.842-1.334-1.842h-15.73l-15.527 59.962c-.25.997.341 1.839 1.35 1.839h15.71zm304.595-62.652c-18.701 0-26.83 3.329-34.23 31.777-6.35 24.579-1.236 31.713 17.781 31.713 19.369 0 27.029-3.961 34.188-31.713 5.228-20.117 4.15-31.777-17.739-31.777zm-13.775 53.15c-7.104 0-8.203-2.604-3.199-21.86 4.098-16.062 8.102-21.621 14.443-21.621 7.137 0 7.938 3.809 3.363 21.621-4.595 17.604-9.076 21.86-14.607 21.86zm-318.139-15.261c-.55-1.855-1.137-3.224-2.021-4.303-.123-.125-.15-.33.045-.397 7.113-2.179 11.784-6.659 13.742-14.266 2.61-10.079-3.75-18.072-19.742-18.072h-23.781l-15.527 59.962c-.286.997.313 1.839 1.347 1.839h14.837l13.5-52.096c1.453 0 2.773-.069 6.239-.069 5.734 0 7.85 3.553 6.605 8.322-1.625 6.286-6.498 9.813-12.298 9.813h-2.188l6.2 29.931c.727 2.883 2.211 4.099 5.53 4.099h13.283l-5.771-24.763zm202.373 3.816c-6.286 15.053 1.896 21.882 18.625 21.882 15.24 0 23.758-3.613 27.365-17.667 1.221-4.78.428-9.953-2.477-13.169-5.727-6.183-16.713-7.374-20.576-10.966-1.955-1.844-1.955-4.4-1.375-6.594 1.055-4.042 3.738-6.244 8.154-6.244 6.893 0 7.105 4.225 5.387 9.274h14.797c3.502-10.506-1.1-18.736-18.493-18.736-14.689 0-22.611 5.762-25.466 16.778-.768 3.008-.471 7.953 1.322 11.022 4.04 6.95 14.721 7.76 20.618 11.879 2.551 1.79 2.647 4.202 1.972 6.853-1.333 5.152-3.896 7.604-9.069 7.604-4.938 0-7.931-2.873-5.079-11.917h-15.705zM82.612 376.643c6.493-1.373 12.268-5.425 14.285-13.016 2.547-9.633-3.597-16.144-18.754-16.144H51.675l-15.549 59.988c-.258 1.007.342 1.849 1.372 1.849h24.037c19.34 0 27.762-4.144 30.682-17.434 1.79-8.209-2.92-13.668-9.604-14.904-.171-.025-.171-.291-.001-.339zm-10.394-19.515c6.593 0 8.739 2.629 7.509 7.466-1.209 4.663-5.34 7.985-10.271 7.985H61.85l3.961-15.385c.918-.019 3.576-.066 6.407-.066zm-8.747 41.61h-8.429l4.448-17.206h8.276c6.565 0 8.677 3.937 7.469 8.776-1.209 4.554-5.49 8.43-11.764 8.43zm149.343-213.23l35.599-124.907c3.487-12.827 17.655-24.569 32.021-24.569h108.914c1.822 0 2.383 2.379.982 3.346-56.586 38.772-124.936 96.24-174.421 147.76-1.274 1.37-3.593.261-3.095-1.63zm86.06 74.972l35.235-123.27c.536-1.868-1.486-3.214-2.984-1.927-51.865 44.61-114.422 109.747-153.561 159.042-.982 1.271-.496 3.153 1.236 3.153h152.53c64.338 0 99.51-19.958 112.334-67.564 10.391-38.774-9.906-65.221-42.447-72.057-.561-.118-1.076-1.226 0-1.393 27.496-4.706 55.182-15.753 65.066-55.798 10.684-43.319-22.42-67.094-60.537-64.636l-58.32 204.153c-3.152 11.027-14.967 20.295-26.182 20.295h-22.37z\"></path></svg>".getBytes()));
			bLogo.setStyleName(ExplorerLayout.LOGO_WRAP);
			headerLeft.addComponent(bLogo);
		} catch (IOException e) {
			// TODO Auto-generated catch blocks
			e.printStackTrace();
		}
		
		// default logo
//		logo = new CssLayout();
//		logo.setStyleName(ExplorerLayout.LOGO_WRAP);
//		headerLeft.addComponent(logo);
		//End logo
		
		Button button = new Button();
		button.setIcon(FontAwesome.NAVICON);
	    button.addStyleName(ExplorerLayout.BUTTON_TOGGLE_MENU);
	    button.addStyleName(Reindeer.BUTTON_LINK);
	    button.setId(BUTTON_TOGGLE_MENU_ID);
	    button.addClickListener(this);
		headerLeft.addComponent(button);
		
		addComponent(headerLeft);
	}
	
	public void initHeaderRight(){
		headerRight = new CssLayout();
		headerRight.setStyleName(ExplorerLayout.HEADER_RIGHT);
		addComponent(headerRight);
		
//		CssLayout searchBox = new CssLayout();
//		searchBox.setStyleName(ExplorerLayout.HEADER_SEARCH_BOX);
//		headerRight.addComponent(searchBox);
		
		
//		TextField txtSearch = new TextField();
//		txtSearch.setWidth("100%");
//		txtSearch.setInputPrompt("Tìm kiếm...");
//		txtSearch.addStyleName(ExplorerLayout.HEADER_SEARCH_BOX_TXT);
//		searchBox.addComponent(txtSearch);
//		
//		Label backgroundBlur = new Label();
//		backgroundBlur.setContentMode(ContentMode.HTML);
//		backgroundBlur.addStyleName(ExplorerLayout.SEARCH_BG_BLUR);
//		searchBox.addComponent(backgroundBlur);
//		txtSearch.addValueChangeListener(new ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				Object valueObj = event.getProperty().getValue();
//				if(valueObj != null && valueObj instanceof String){
//					String value = (String) valueObj;
//					if(value != null && !value.trim().isEmpty()){
//						UI.getCurrent().getNavigator().navigateTo(SolrSearchView.VIEW_NAME+"/"+value);
//						txtSearch.clear();
//					}
//					
//				}
//			}
//		});
		
		CssLayout helpWrap = new CssLayout();
		helpWrap.addStyleName(ExplorerLayout.HELP_WRAP);
		headerRight.addComponent(helpWrap);
		
		Button helpBtn = new Button();
		helpBtn.addClickListener(this);
		helpBtn.setId(BUTTON_HELP_ID);
		helpBtn.setDescription("Trợ giúp");
		helpBtn.setIcon(FontAwesome.QUESTION);
		helpBtn.addStyleName(ExplorerLayout.HELP_BTN);
		helpWrap.addComponent(helpBtn);
		
		helpWindow = new Window("Trợ giúp");
		helpWindow.setWidth("90%");
		helpWindow.setHeight("90%");
		helpWindow.setModal(true);
		helpWindow.addStyleName(ExplorerLayout.HELP_POPUP);
		helpWindow.center();
		
		try {
			
			CssLayout wrap = new CssLayout();
			wrap.setWidth("100%");
			wrap.addStyleName(ExplorerLayout.HELP_POPUP_CONTENT);
			HtThamSo thamSo = htThamSoService.getHtThamSoFind(ThamSo.HELP_DOC_PATH);
			if(thamSo != null && thamSo.getGiaTri() != null){
				String path = thamSo.getGiaTri();
				BrowserFrame browser = new BrowserFrame(null, new ThemeResource(path));
				browser.setWidth("100%");
				browser.setHeight("100%");
				wrap.addComponent(browser);
				helpWindow.setContent(wrap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
//		CssLayout blogWrap = new CssLayout();
//		blogWrap.addStyleName(ExplorerLayout.BLOG_WRAP);
//		headerRight.addComponent(blogWrap);
//		
//		Link blogBtn = new Link();
//		blogBtn.setDescription("Blog");
//		blogBtn.setResource(new ExternalResource("/blog/index.html"));
//		blogBtn.setTargetName("_blank");
//		blogBtn.setIcon(FontAwesome.BOOK);
//		blogBtn.addStyleName(ExplorerLayout.BLOG_BTN);
//		blogWrap.addComponent(blogBtn);
		
		
		CssLayout headerRightContent = new CssLayout();
		headerRightContent.setStyleName(ExplorerLayout.HEADER_RIGHT_CONTENT);
		headerRight.addComponent(headerRightContent);
		
		headerRightContent.addComponent(initMessageBar());
		
		headerRightContent.addComponent(initNotification());
		
		CssLayout userWrap = new CssLayout();
		userWrap.setStyleName(ExplorerLayout.USER_WRAP);
		headerRightContent.addComponent(userWrap);
		
		CssLayout userImg = new CssLayout();
		userImg.addStyleName(ExplorerLayout.USER_IMG);
		userWrap.addComponent(userImg);
		
		String uid = CommonUtils.getUserLogedIn();
		if(uid != null){
			User u = identityService.createUserQuery().userId(uid).singleResult();
			if(u!= null){
				try {
					Picture uPic = identityService.getUserPicture(uid);
					if(uPic != null){
						Image userPic = new Image();
						SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
						String filename = "user-pic-" + df.format(new Date());
						userPic.setSource(FileUtils.getStreamResource(uPic.getInputStream(), filename));
						userPic.addStyleName(ExplorerLayout.USER_PIC_HEADER);
						userImg.addComponent(userPic);
					}else{
						String shortName = "#";
						String firstName = u.getFirstName();
						String lastName = u.getLastName();
						if(firstName != null && !firstName.trim().isEmpty() && lastName != null && !lastName.trim().isEmpty()){
							shortName =  firstName.trim().substring(0, 1) + lastName.trim().substring(0, 1);
						}
						Label lbl = new Label(shortName);
						lbl.addStyleName(ExplorerLayout.USER_PIC_SHORT_NAME);
						userImg.addComponent(lbl);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		Label userName = new Label(getName());
		userName.addStyleName(ExplorerLayout.USER_NAME);
		userWrap.addComponent(userName);
		
		VerticalLayout userOption = new VerticalLayout();
		userOption.addStyleName(ExplorerLayout.USER_OPTION_WRAP);
		userWrap.addComponent(userOption);
		
		Button btnUserProfile = new Button("Trang cá nhân");
		btnUserProfile.setId(BUTTON_MY_PRO_ID);
		btnUserProfile.addClickListener(this);
		btnUserProfile.addStyleName(Reindeer.BUTTON_LINK);
		btnUserProfile.setIcon(FontAwesome.USER);
		btnUserProfile.addStyleName(ExplorerLayout.USER_OPTION_BTN);
		userOption.addComponent(btnUserProfile);
		
		//
		Button btnLogout = new Button("Đăng xuất");
		
		btnLogout.addStyleName(Reindeer.BUTTON_LINK);
		btnLogout.addStyleName(ExplorerLayout.USER_OPTION_BTN);
		btnLogout.setIcon(FontAwesome.LOCK);
		btnLogout.addClickListener(this);
		userOption.addComponent(btnLogout);
		
		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && auth.isAuthenticated()) {
			Object obj = auth.getDetails();
			if (obj instanceof CustomWebAuthenticationDetails) {
				CustomWebAuthenticationDetails details = (CustomWebAuthenticationDetails) obj;
				if (details != null
						&& details.checkAuthProviderType(CustomWebAuthenticationDetails.EAuthProvider.GOOGLE)) {
					btnLogout.setId(BUTTON_LOGOUT_GOOGLE_ID);
				}else{
					btnLogout.setId(BUTTON_LOGOUT_ID);
				}
			}
		}
		
	}
	
	public CssLayout initNotification(){
		CssLayout notificationWrap = new CssLayout();
		notificationWrap.addStyleName(ExplorerLayout.NOTIFICATION_WRAP);
		
		CssLayout notificationIcon = new CssLayout();
		notificationIcon.addStyleName(ExplorerLayout.NOTIFICATION_ICON);
		notificationWrap.addComponent(notificationIcon);
		
		Button btn = new Button();
		btn.setDescription("Thông báo");
		btn.setIcon(FontAwesome.COMMENTING);
		btn.addStyleName(ExplorerLayout.NOTIFICATION_BTN);
		btn.setId(BUTTON_NOTIF_ID);
		btn.addClickListener(this);
		notificationIcon.addComponent(btn);
		
		List<Notification> newNotifList = notificationService.getNotificationFind(null, null, CommonUtils.getUserLogedIn(), null, null, 0, 10);
		notifLoaded = newNotifList.size();
		List<Long> trangThaiList = new ArrayList();
		trangThaiList.add(new Long(ENotificationStatus.NEW.getCode()));
		Long notifCount = notificationService.getNotificationFind(null, null, CommonUtils.getUserLogedIn(), null, trangThaiList);
		
		String notifCountStr = notifCount+"";
		if(notifCount > 99){
			notifCountStr = "99+";
		}
		
		notificationNumber = new Label(notifCountStr);
		notificationNumber.addStyleName(ExplorerLayout.NOTIFICATION_LBL);
		notificationIcon.addComponent(notificationNumber);
		
		
		CssLayout notificationContainerWrap = new CssLayout();
		notificationContainerWrap.addStyleName(ExplorerLayout.NOTIFICATION_CONTAINER_WRAP);
		notificationWrap.addComponent(notificationContainerWrap);
		
		if(newNotifList != null && !newNotifList.isEmpty()){
			notificationContainer = new CssLayout();
			notificationContainer.addStyleName(ExplorerLayout.NOTIFICATION_CONTAINER);
			notificationContainerWrap.addComponent(notificationContainer);
			
			for(Notification n : newNotifList){
				CssLayout notifItem = initNotifItem(n);
				if(notifItem != null){
					notificationContainer.addComponent(notifItem);
				}
			}
		}
		
		Long countAll = notificationService.getNotificationFind(null, null, CommonUtils.getUserLogedIn(), null, null);
		if(countAll != null && countAll.intValue() > 10){
			CssLayout viewMoreWrap = new CssLayout();
			viewMoreWrap.addStyleName(ExplorerLayout.N_LIST_VIEW_MORE_WRAP);
			Button btnViewMore = new Button("Xem thêm");
			btnViewMore.addStyleName(Reindeer.BUTTON_LINK);
			btnViewMore.addStyleName(ExplorerLayout.N_LIST_VIEW_MORE_BTN);
			btnViewMore.addClickListener(this);
			btnViewMore.setId(BUTTON_VIEW_MORE_ID);
			viewMoreWrap.addComponent(btnViewMore);
			notificationContainerWrap.addComponent(viewMoreWrap);
		}
		
		return notificationWrap;
	}
	
	public void addNotification(Notification notif){
		if(notif != null && notificationContainer != null){
			CssLayout notifItem = initNotifItem(notif);
			if(notifItem != null){
				notificationContainer.addComponent(notifItem, 0);
			}
			notifLoaded++;
			List<Long> trangThaiList = new ArrayList();
			trangThaiList.add(new Long(ENotificationStatus.NEW.getCode()));
			Long notifCount = notificationService.getNotificationFind(null, null, notif.getUserId(), null, trangThaiList);
			
			String notifCountStr = notifCount+"";
			if(notifCount > 99){
				notifCountStr = "99+";
			}
			if(notificationNumber != null){
				notificationNumber.setValue(notifCountStr);
			}
		}
	}
	
	public CssLayout initNotifItem(Notification notif){
		if(notif.getFromUserId() != null){
			User u = identityService.createUserQuery().userId(notif.getFromUserId()).singleResult();
			if(u!= null){
				try {
					CssLayout notifListItem = new CssLayout();
					notifListItem.setId("notif-i-"+notif.getId());
					notifListItem.addStyleName(ExplorerLayout.NOTIFICATION_LIST_ITEM);
					if(notif.getTrangThai() != null && notif.getTrangThai().intValue() == ENotificationStatus.CLICKED.getCode()){
						notifListItem.addStyleName(ExplorerLayout.NOTIFICATION_LIST_ITEM_CLICKED);
					}
					
					
					CssLayout userImg = new CssLayout();
					userImg.addStyleName(ExplorerLayout.N_LIST_ITEM_UIMG_WRAP);
					notifListItem.addComponent(userImg);
					
					Picture uPic = identityService.getUserPicture(notif.getFromUserId());
					if(uPic != null){
						Image userPic = new Image();
						SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
						String filename = "user-pic-" + df.format(new Date());
						userPic.setSource(FileUtils.getStreamResource(uPic.getInputStream(), filename));
						userPic.addStyleName(ExplorerLayout.N_LIST_ITEM_UIMG);
						userImg.addComponent(userPic);
					}else{
						String shortName = "#";
						String firstName = u.getFirstName();
						String lastName = u.getLastName();
						if(firstName != null && !firstName.trim().isEmpty() && lastName != null && !lastName.trim().isEmpty()){
							shortName =  firstName.trim().substring(0, 1) + lastName.trim().substring(0, 1);
						}
						Label lbl = new Label(shortName);
						lbl.addStyleName(ExplorerLayout.N_LIST_ITEM_SHORT_NAME);
						userImg.addComponent(lbl);
					}
					
					CssLayout notifContent = new CssLayout();
					notifContent.addStyleName(ExplorerLayout.N_LIST_ITEM_CONTENT);
					notifListItem.addComponent(notifContent);
					
					String content = "<span class='n-item-user'>"+u.getFirstName()+" "+u.getLastName()+"</span>";
					
					if(notif.getType() != null){
						if(notif.getType().intValue() == CommonUtils.ENotificationType.COMMENT_TASK.getCode()){
							content+=" <span class='n-item-content-type'>đã bình luận về công việc</span>";
							if(notif.getRefKey() != null){
								org.activiti.engine.task.Comment commentObj = taskService.getComment(notif.getRefKey());
								if(commentObj != null){
									HistoricTaskInstance task = historyService.createHistoricTaskInstanceQuery().taskId(commentObj.getTaskId()).singleResult();
									
									String taskName = "#Không tên";
									if(task != null && task.getName() != null && !task.getName().isEmpty()){
										taskName = task.getName();
									}
									content+=" <span class='n-item-task'>"+taskName+"</span>";
								}
							}
						}
					}
					
					if(notif.getNotificationTime() != null){
						SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
						content+=" <span class='n-item-time'>"+df.format(notif.getNotificationTime())+"</span>";
					}
					
					Label lblContent = new Label(content);
					lblContent.addStyleName(ExplorerLayout.N_LIST_ITEM_CONTENT_LBL);
					lblContent.setContentMode(ContentMode.HTML);
					notifContent.addComponent(lblContent);
					
					Button btn = new Button();
					btn.setId(BUTTON_NOTIF_ITEM_ID);
					btn.addStyleName(ExplorerLayout.N_LIST_ITEM_BTN);
					btn.addClickListener(this);
					btn.setData(notif);
					notifListItem.addComponent(btn);
					
					return notifListItem;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
	public CssLayout initMessageBar(){
		long queueCount = 0L;
		
		
		List<Group> groups = identityService.createGroupQuery().groupMember(CommonUtils.getUserLogedIn()).list();
		if(groups != null && !groups.isEmpty()){
			List<String> groupIds = new ArrayList<String>();
			for(Group g: groups){
				groupIds.add(g.getId());
			}
			if(groupIds != null && !groupIds.isEmpty()){
				queueCount = taskService.createTaskQuery().taskCandidateGroupIn(groupIds).count();
			}
		}
		
		//count
		long inboxCount = taskService.createTaskQuery().taskAssignee(CommonUtils.getUserLogedIn()).count();
		
		long mytaskCount = taskService.createTaskQuery().taskOwner(CommonUtils.getUserLogedIn()).count();
		long involesCount = taskService.createTaskQuery().taskInvolvedUser(CommonUtils.getUserLogedIn()).count();

		
		CssLayout messageBar = new CssLayout();
		messageBar.setStyleName(ExplorerLayout.MESSAGE_BAR);
		
		
		
		if(inboxCount > 0){
			CssLayout inboxWrap = new CssLayout();
			inboxWrap.setStyleName(ExplorerLayout.MESSAGE_ITEM);
			messageBar.addComponent(inboxWrap);
			
			Button inbox = new Button();
			inbox.setIcon(FontAwesome.ENVELOPE_O);
			inbox.setStyleName(ExplorerLayout.MESSAGE_BUTTON);
			inbox.setId(BUTTON_INBOX_ID);
			inbox.setDescription("Inbox");
			inbox.addClickListener(this);
			inboxWrap.addComponent(inbox);
			
			Label inboxNum = new Label(inboxCount+"");
			inboxNum.setStyleName(ExplorerLayout.MESSAGE_NUMBER);
			inboxWrap.addComponent(inboxNum);
		}
		
		
		
		
		
		if(mytaskCount > 0){
			CssLayout myTaskWrap = new CssLayout();
			myTaskWrap.setStyleName(ExplorerLayout.MESSAGE_ITEM);
			messageBar.addComponent(myTaskWrap);
			
			Button myTask = new Button();
			myTask.setIcon(FontAwesome.USER);
			myTask.setStyleName(ExplorerLayout.MESSAGE_BUTTON);
			myTask.setId(BUTTON_MY_TASK_ID);
			myTask.addClickListener(this);
			myTask.setDescription("My task");
			myTaskWrap.addComponent(myTask);
			
			Label myTaskNum = new Label(mytaskCount+"");
			myTaskNum.setStyleName(ExplorerLayout.MESSAGE_NUMBER);
			myTaskWrap.addComponent(myTaskNum);
		}
		
		
		
		if(queueCount > 0){
			CssLayout queueWrap = new CssLayout();
			queueWrap.setStyleName(ExplorerLayout.MESSAGE_ITEM);
			messageBar.addComponent(queueWrap);
			
			Button queue = new Button();
			queue.setIcon(FontAwesome.USERS);
			queue.setStyleName(ExplorerLayout.MESSAGE_BUTTON);
			queue.setId(BUTTON_QUEUE_ID);
			queue.addClickListener(this);
			queue.setDescription("Queue");
			queueWrap.addComponent(queue);
			
			Label queueNum = new Label(queueCount+"");
			queueNum.setStyleName(ExplorerLayout.MESSAGE_NUMBER);
			queueWrap.addComponent(queueNum);
		}
		
		
		
		if(involesCount > 0){

			CssLayout involeWrap = new CssLayout();
			involeWrap.setStyleName(ExplorerLayout.MESSAGE_ITEM);
			messageBar.addComponent(involeWrap);
			
			Button invole = new Button();
			invole.setIcon(FontAwesome.USER_PLUS);
			invole.setStyleName(ExplorerLayout.MESSAGE_BUTTON);
			invole.setId(BUTTON_INVOLED_ID);
			invole.addClickListener(this);
			invole.setDescription("Invole");
			involeWrap.addComponent(invole);
			
			Label involeNum = new Label(involesCount+"");
			involeNum.setStyleName(ExplorerLayout.MESSAGE_NUMBER);
			involeWrap.addComponent(involeNum);
		}
		
		
		return messageBar;
		
	}

	public String getName(){
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(obj != null && obj instanceof ActivitiUserDetails){
			ActivitiUserDetails user = (ActivitiUserDetails) obj;
			return user.getFirstName() + " " + user.getLastName();
		}
		return "";
	}
	
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button btn = event.getButton();
			if(btn == null){
				return;
			}
			if(BUTTON_LOGOUT_ID.equals(btn.getId())){
				String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
				UI.getCurrent().getPage().setLocation(contextPath+"/logout");
			}else if(BUTTON_TOGGLE_MENU_ID.equals(btn.getId())){
				if(mainMenuBarWrap != null){
					if(!isSmallMenu){
						mainMenuBarWrap.setWidth("55px");
					}else{
						mainMenuBarWrap.setWidth("240px");
					}
					isSmallMenu = !isSmallMenu;
				}
			}else if(BUTTON_MY_PRO_ID.equals(btn.getId())){
				UI.getCurrent().getNavigator().navigateTo(ProfileView.VIEW_NAME);
			}else if(BUTTON_INBOX_ID.equals(btn.getId())){
				UI.getCurrent().getNavigator().navigateTo(InboxView.VIEW_NAME);
			}else if(BUTTON_MY_TASK_ID.equals(btn.getId())){
				UI.getCurrent().getNavigator().navigateTo(MyTaskView.VIEW_NAME);
			}else if(BUTTON_QUEUE_ID.equals(btn.getId())){
				UI.getCurrent().getNavigator().navigateTo(QueueView.VIEW_NAME);
			}else if(BUTTON_INVOLED_ID.equals(btn.getId())){
				UI.getCurrent().getNavigator().navigateTo(InvoledView.VIEW_NAME);
			}else if(BUTTON_HELP_ID.equals(btn.getId())){
				UI.getCurrent().addWindow(helpWindow);
			}else if(BUTTON_NOTIF_ID.equals(btn.getId())){
				notificationService.markAsRead();
				notificationNumber.setValue("0");
			}else if(BUTTON_VIEW_MORE_ID.equals(btn.getId())){
				List<Notification> newNotifList = notificationService.getNotificationFind(null, null, CommonUtils.getUserLogedIn(), null, null, notifLoaded, 10);
				if(newNotifList != null){
					notifLoaded += newNotifList.size();
					for(Notification n : newNotifList){
						CssLayout notifItem = initNotifItem(n);
						if(notifItem != null){
							notificationContainer.addComponent(notifItem);
						}
					}
				}
			}else if(BUTTON_NOTIF_ITEM_ID.equals(btn.getId())){
				if(btn.getData() != null && btn.getData() instanceof Notification){
					Notification notif = (Notification) btn.getData();
					if(notif.getRefKey() != null){
						org.activiti.engine.task.Comment commentObj = taskService.getComment(notif.getRefKey());
						HistoricTaskInstance task = historyService.createHistoricTaskInstanceQuery().taskId(commentObj.getTaskId()).singleResult();
						if(task!=null && task.getEndTime() != null){
							UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW_HISTORY+"/"+commentObj.getTaskId()+"/cmt-"+commentObj.getId());
						}else{
							UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+commentObj.getTaskId()+"/cmt-"+commentObj.getId());	
						}
					}
					notif.setTrangThai(new Long(ENotificationStatus.CLICKED.getCode()));
					notificationService.save(notif);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void setMainMenuBarWrap(CssLayout mainMenuBarWrap) {
		this.mainMenuBarWrap = mainMenuBarWrap;
	}


	public Label getNotificationNumber() {
		return notificationNumber;
	}

	public CssLayout getNotificationContainer() {
		return notificationContainer;
	}

	
}

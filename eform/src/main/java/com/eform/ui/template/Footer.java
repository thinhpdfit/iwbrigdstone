package com.eform.ui.template;

import com.eform.common.ExplorerLayout;
import com.eform.common.SpringApplicationContext;
import com.eform.common.ThamSo;
import com.eform.common.utils.CommonUtils;
import com.eform.model.entities.HtThamSo;
import com.eform.service.HtThamSoService;
import com.vaadin.server.ExternalResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;

public class Footer extends CssLayout{

	private static final long serialVersionUID = 1L;

	private final int WIDTH_FULL = 100;
	private HtThamSoService htThamSoService;
	
	public Footer(){
		htThamSoService = SpringApplicationContext.getApplicationContext().getBean(HtThamSoService.class);
		init();
	}
	
	public void init(){
		setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		setStyleName(ExplorerLayout.FOOTER);
		Label label = new Label("Copyright by Lac Long Quan Software Solution &copy; 2016", ContentMode.HTML);
		label.setStyleName(ExplorerLayout.FOOTER_LABEL);
		addComponent(label);
		try {
			HtThamSo thamSo = htThamSoService.getHtThamSoFind(ThamSo.CHAT_USER_URL);
			if(thamSo != null && thamSo.getGiaTri() != null && !thamSo.getGiaTri().isEmpty()){
				CssLayout chatBox = new CssLayout();
				chatBox.addStyleName(ExplorerLayout.CHAT_BOX_WRAP);
				addComponent(chatBox);
				
				Label collapseChatBox = new Label();
				collapseChatBox.addStyleName(ExplorerLayout.CHAT_BOX_COLLAPSE);
				chatBox.addComponent(collapseChatBox);
				
				String path = CommonUtils.getRootPath()+thamSo.getGiaTri();
				BrowserFrame browser = new BrowserFrame(null, new ExternalResource(path));
				browser.addStyleName(ExplorerLayout.CHAT_BOX);
		    	browser.setWidth("100%");
		    	browser.setHeight("100%");
		    	chatBox.addComponent(browser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package com.eform.ui.template;

import java.util.List;

import com.eform.common.ExplorerLayout;
import com.eform.dao.GroupQuyenDao;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.Tree;

public class MainContainer extends CssLayout{


	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;
	
	private CssLayout menubarWrap;
	private MainMenuBar mainMenuBar;
	private CssLayout mainWrap;
	private CssLayout main;
	
	private GroupQuyenDao groupQuyenDao;
	private List<String>  groups;
	
	private CssLayout notificationBlock;
	
	public MainContainer(GroupQuyenDao groupQuyenDao, List<String>  groups){
		this.groupQuyenDao = groupQuyenDao;
		this.groups= groups;
		init();
		initMenu();
		initMainMenuBar();
		initMainWrap();
		initMain();
		initNotificationBlock();
		
	}
	
	public void init(){
		setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		setStyleName(ExplorerLayout.CONTAINER);
	}
	public void initMenu(){
		menubarWrap = new CssLayout();
		menubarWrap.setWidth("240px");
		menubarWrap.setStyleName(ExplorerLayout.MAIN_MENU_BAR);
		addComponent(menubarWrap);
	}
	
	public void initMainMenuBar(){
		mainMenuBar = new MainMenuBar(groupQuyenDao, groups);
		menubarWrap.addComponent(mainMenuBar);
	}
	
	public void initMainWrap(){
		mainWrap = new CssLayout();
		mainWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		mainWrap.setStyleName(ExplorerLayout.MAIN_WRAP);
		addComponent(mainWrap);
	}

	public void initMain(){
		main = new CssLayout();
		main.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		main.setStyleName(ExplorerLayout.MAIN_CONTENT);
		mainWrap.addComponent(main);
	}
	
	public ComponentContainer getViewContainer(){
		return main;
	}

	public CssLayout getMainMenuBarWrap() {
		return menubarWrap;
	}
	
	public MainMenuBar getMainMenuBar(){
		return mainMenuBar;
	}
	
	public Tree getMenuTree(){
		return mainMenuBar.getMenuTree();
	}
	
    public void initNotificationBlock(){
    	notificationBlock = new CssLayout();
    	notificationBlock.addStyleName(ExplorerLayout.NOTIFICATION_BLOCK);

    	CssLayout soundBlock = new CssLayout();
    	soundBlock.setId("sound");
    	notificationBlock.addComponent(soundBlock);
    	addComponent(notificationBlock);
    }
    
    public CssLayout getNotificationBlock(){
    	return notificationBlock;
    }
    

}

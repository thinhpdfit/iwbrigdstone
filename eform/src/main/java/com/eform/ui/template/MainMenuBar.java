package com.eform.ui.template;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.eform.common.ExplorerLayout;
import com.eform.common.utils.CommonUtils;
import com.eform.dao.GroupQuyenDao;
import com.eform.model.entities.GroupQuyen;
import com.eform.model.entities.HtQuyen;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;


public class MainMenuBar extends VerticalLayout implements ValueChangeListener, ItemClickListener{

	private static final long serialVersionUID = 1L;
	
	private GroupQuyenDao groupQuyenDao;
	private List<String> groups;
	private Map<String, HtQuyen> viewNameQuyenMap;
	
	private Tree menu;
	private String currentView;
	private HtQuyen currentQuyen;
	
	public MainMenuBar(GroupQuyenDao groupQuyenDao, List<String> groups){
		this.groups = groups;
		this.groupQuyenDao = groupQuyenDao;
		setId(ExplorerLayout.MAIN_MENU_BAR_ID);
		setStyleName(ExplorerLayout.MAIN_MENU_BAR_CONTENT);
		viewNameQuyenMap = new HashMap<String, HtQuyen>();
		init();
	}
	
	public void init(){
	    setWidth("240px");
	    if(groups != null && !groups.isEmpty()){
	    	initTree();
	    }
	}
	public void initTree(){
		menu = new Tree();
		menu.setStyleName(ExplorerLayout.MENU_TREE);
		
		addComponent(menu);
		List<Long> trangThaiSD = new ArrayList<Long>();
		trangThaiSD.add(1L);
		List<GroupQuyen> groupQuyenLst = groupQuyenDao.getGroupQuyenFind(groups,null, -1, -1);
		if(groupQuyenLst != null && !groupQuyenLst.isEmpty()){
			List<HtQuyen> quyenList = new ArrayList();
			for(GroupQuyen gq : groupQuyenLst){
				if(gq != null && gq.getHtQuyen() != null && gq.getHtQuyen().getMenu().intValue() == CommonUtils.EMenu.MENU.getCode() &&  gq.getHtQuyen().getTrangThai() != null && gq.getHtQuyen().getTrangThai().intValue() == 1 && checkParentUse(gq.getHtQuyen())){
					quyenList.add(gq.getHtQuyen());
				}
			}
			if(quyenList != null && !quyenList.isEmpty()){
				CommonUtils.sortHtQuyenList(quyenList);
				BeanItemContainer<HtQuyen> container = new BeanItemContainer<HtQuyen>(HtQuyen.class, quyenList);
				menu.setContainerDataSource(container);
				menu.setItemCaptionPropertyId("ten");
				menu.setImmediate(true);
				menu.addValueChangeListener(this);
				menu.addItemClickListener(this);
				
				if(quyenList != null && !quyenList.isEmpty()){
					//Pattern p = Pattern.compile("([^/]+)(/(.+))+");
					
					for(HtQuyen quyen : quyenList){
						viewNameQuyenMap.put(quyen.getViewName(), quyen);
						if(quyen.getViewName() != null){
//							Matcher m = p.matcher(quyen.getViewName());
//							if(m.matches()){
//								viewNameQuyenMap.put(m.group(1), quyen);
//							}
							viewNameQuyenMap.put(quyen.getViewName(), quyen);
						}
						
						if(quyen.getMenu() != null && quyen.getMenu().intValue() == CommonUtils.EMenu.MENU.getCode()){
							menu.addItem(quyen);
							if(quyen.getIcon() != null){
								menu.setItemIcon(quyen, FontAwesome.fromCodepoint(quyen.getIcon().intValue()));
							}
							menu.setParent(quyen, quyen.getHtQuyen());
						}
					}
					for(HtQuyen quyen : quyenList){
						menu.expandItemsRecursively(quyen);
						if(!menu.hasChildren(quyen)){
							menu.setChildrenAllowed(quyen, false);
						}else{
							menu.collapseItem(quyen);
						}
					}
				}
			}
		}
		menu.setData(viewNameQuyenMap);
	
	}
	
	public boolean checkParentUse(HtQuyen qTmp){
		try {
			if(qTmp.getTrangThai() == null || qTmp.getTrangThai().intValue() != 1){
				return false;
			}
			while(qTmp.getHtQuyen() != null){
				qTmp = qTmp.getHtQuyen();
				if(qTmp.getTrangThai() == null || qTmp.getTrangThai().intValue() != 1){
					return false;
				}
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public Tree getMenuTree(){
		return menu;
	}
	
	public String getCurrentView() {
		return currentView;
	}

	public void setCurrentView(String currentView) {
		this.currentView = currentView;
	}

	public HtQuyen getCurrentQuyen() {
		return currentQuyen;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		System.out.println("ValueChangeEvent");
		try {
			HtQuyen current = (HtQuyen) event.getProperty().getValue();
			
			if(current != null){
				currentQuyen = current;
				if(current.getViewName() != null && !current.getViewName().equals(currentView)){
					currentView = current.getViewName();
					getUI().getNavigator().navigateTo(currentView);
				}
				
				
				
				if(current.getHtQuyen() == null && !menu.isExpanded(current)){
					menu.expandItem(current);
				}
				
				while(current.getHtQuyen() != null  && !menu.isExpanded(current.getHtQuyen())){
					menu.expandItem(current.getHtQuyen());
					current = current.getHtQuyen();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();;
		}
	}
	
	

	@Override
	public void itemClick(ItemClickEvent event) {
		System.out.println("ClickEvent");
	}

	
}

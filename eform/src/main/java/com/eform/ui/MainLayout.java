package com.eform.ui;

import java.util.List;

import com.eform.common.ExplorerLayout;
import com.eform.dao.GroupQuyenDao;
import com.eform.ui.template.Footer;
import com.eform.ui.template.Header;
import com.eform.ui.template.MainContainer;
import com.eform.ui.template.MainMenuBar;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;

public class MainLayout extends VerticalLayout{
	private static final long serialVersionUID = 1L;

	private final int WIDTH_FULL = 100;
	
	private Header header;
	private MainContainer mainContainer;
	private Footer footer;
	
	private GroupQuyenDao groupQuyenDao;
	private List<String> groups;
	
	
	public MainLayout(GroupQuyenDao groupQuyenDao, List<String> groups){
		this.groupQuyenDao = groupQuyenDao;
		this.groups = groups;
		setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addStyleName(ExplorerLayout.WRAPPER);
		init();
	}
	
	public void init(){
		header = new Header();
		addComponent(header);
		
		mainContainer = new MainContainer(groupQuyenDao, groups);
		addComponent(mainContainer);
		
		header.setMainMenuBarWrap(mainContainer.getMainMenuBarWrap());
		
		footer = new Footer();
		addComponent(footer);
	}
	
	public ComponentContainer getViewContainer(){
		return mainContainer.getViewContainer();
	}
	
	public MainMenuBar getMainMenuBar(){
		return mainContainer.getMainMenuBar();
	}
	
	public Tree getMenuTree(){
		return mainContainer.getMenuTree();
	}
	public CssLayout getNotificationBlock(){
    	return mainContainer.getNotificationBlock();
    }

	public Header getHeader() {
		return header;
	}
	
	
}

package com.eform.ui.view.task;

import com.eform.common.ExplorerLayout;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class HistoricTaskCategoryNavigatorBar extends CssLayout implements ClickListener{
	private static final long serialVersionUID = -2839634219714835627L;
	
	public static final String BTN_ARCHIVED_ID = "btnArchived";
	public static final String BTN_FINISHED_ID = "btnFinished";
	public static final String BTN_CANDIDATE_FINISHED_ID = "btnCandidateFinished";
	
	private String btnActiveId;
	private String title;
	private String smallTitle;
	
	public HistoricTaskCategoryNavigatorBar(String title,String smallTitle, String btnActiveId){
		addStyleName(ExplorerLayout.TASK_NAV_WRAP);
		setWidth("100%");
		this.btnActiveId = btnActiveId;
		this.title = title;
		this.smallTitle = smallTitle;
		initContent();
	}
	
	public void initContent(){
		
		if(title != null && smallTitle != null && !smallTitle.isEmpty()){
			title += "<span class='"+ExplorerLayout.TASK_NAV_SMALL_TITLE+"'>"+smallTitle+"</span>";
		}
		
		Label lblTitle = new Label(title);
		lblTitle.addStyleName(ExplorerLayout.TASK_NAV_TITLE);
		lblTitle.setContentMode(ContentMode.HTML);
		addComponent(lblTitle);
		
		CssLayout btnContainer = new CssLayout();
		btnContainer.addStyleName(ExplorerLayout.TASK_NAV_BTN_CONTAINER);
		addComponent(btnContainer);
		
		btnContainer.addComponent(createButton("Finished", FontAwesome.CHECK_CIRCLE, BTN_FINISHED_ID, BTN_FINISHED_ID.equals(btnActiveId)));
		btnContainer.addComponent(createButton("Archived", FontAwesome.BELL, BTN_ARCHIVED_ID, BTN_ARCHIVED_ID.equals(btnActiveId)));
		btnContainer.addComponent(createButton("Candidate Finished", FontAwesome.GROUP, BTN_CANDIDATE_FINISHED_ID, BTN_CANDIDATE_FINISHED_ID.equals(btnActiveId)));
	}
	
	public CssLayout createButton(String caption, FontAwesome icon, String id, boolean active){
		CssLayout btnWrap = new CssLayout();
		btnWrap.addStyleName(ExplorerLayout.TASK_NAV_BTN_WRAP);
		
		Button btn = new Button();
		btn.addStyleName(ExplorerLayout.TASK_NAV_BTN);
		btn.setDescription(caption);
		btn.setIcon(icon);
		btn.setId(id);
		btn.addClickListener(this);
		
		if(active){
			btn.addStyleName(ExplorerLayout.TASK_NAV_BTN_ACTIVE);
		}
		btnWrap.addComponent(btn);
		
		return btnWrap;
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_ARCHIVED_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(ArchivedView.VIEW_NAME);
		}else if(BTN_FINISHED_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(FinishedView.VIEW_NAME);
		} if(BTN_CANDIDATE_FINISHED_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(CandidateFinishedView.VIEW_NAME);
		}
	}
}

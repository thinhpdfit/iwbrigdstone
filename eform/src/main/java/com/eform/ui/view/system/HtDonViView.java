package com.eform.ui.view.system;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.workflow.ITrangThai;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.HtDonViDao;
import com.eform.model.entities.HtDonVi;
import com.eform.ui.custom.ButtonAction;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.StatusCommentLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = HtDonViView.VIEW_NAME)
public class HtDonViView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "ht-don-vi";
	static final String MAIN_TITLE = "Đơn vị";
	private static final String SMALL_TITLE = "";

	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	private static final String BTN_ADDNEW_ID = "themMoi";
	private static final String BTN_ADD_CHILD_ID = "btnAddChild";
	private final HtDonViView currentView = this;
	
	@Autowired
	HtDonViDao htDonViDao;
	private List<HtDonVi> htDonViList;
	private TreeTable treeTable;
	private WorkflowManagement<HtDonVi> htDonViWM;
	
	private final PropertysetItem searchItem = new PropertysetItem();
	private final FieldGroup searchFieldGroup = new FieldGroup();
	private Button btnAddNew;
	@Autowired
	private MessageSource messageSource;
	
	@PostConstruct
    void init() {
		htDonViWM = new WorkflowManagement<HtDonVi>(null);
		htDonViList = Lists.newArrayList(htDonViDao.findAll());
    	searchItem.addItemProperty("ma", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("ten", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("trangThai", new ObjectProperty<ComboboxItem>(new ComboboxItem()));
    	searchFieldGroup.setItemDataSource(searchItem);
    	
		initMainTitle();
		initSearchForm();
		initDataContent();
        
		btnAddNew = new Button();
    	btnAddNew.setIcon(FontAwesome.PLUS);
    	btnAddNew.addClickListener(this);
    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
    	btnAddNew.setDescription("Tạo mới đơn vị");
    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
    	btnAddNew.setId(BTN_ADDNEW_ID);
		addComponent(btnAddNew);
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		setWidth("100%");
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		
		CssFormLayout searchForm = new CssFormLayout(-1, 60);
		
		TextField txtMa = new TextField("Mã đơn vị");
		searchForm.addFeild(txtMa);
		
		TextField txtTen = new TextField("Tên đơn vị");
		searchForm.addFeild(txtTen);
    	
    	List<ITrangThai> trangThaiListTmp = htDonViWM.getTrangThaiList();
    	List<ComboboxItem> trangThaiLoaiDanhMucList = new ArrayList<>();
    	if(trangThaiListTmp != null){
    		for (ITrangThai trangThai : trangThaiListTmp) {
    			trangThaiLoaiDanhMucList.add(new ComboboxItem(trangThai.getTen(), new Long(trangThai.getId())));
			}
    	}
    	
    	BeanItemContainer<ComboboxItem> cbbTrangThaiContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, trangThaiLoaiDanhMucList);
		ComboBox cbbTrangThai = new ComboBox("Trạng thái", cbbTrangThaiContainer);
		cbbTrangThai.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbTrangThai.setItemCaptionPropertyId("text");
    	searchForm.addFeild(cbbTrangThai);

		searchFieldGroup.bind(txtMa, "ma");
		searchFieldGroup.bind(txtTen, "ten");
		searchFieldGroup.bind(cbbTrangThai, "trangThai");
		
		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(currentView);
		searchForm.addButton(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(currentView);
		searchForm.addButton(btnRefresh);
		
		addComponent(searchForm);
	}
	
	public void initDataContent(){
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		

		List<ITrangThai> trangThaiList = htDonViWM.getTrangThaiList();
		if(trangThaiList != null && !trangThaiList.isEmpty()){
			dataWrap.addComponent(new StatusCommentLayout(trangThaiList));
		}
		
		treeTable = new TreeTable();
		treeTable.addStyleName(ExplorerLayout.DATA_TABLE);
		treeTable.setSizeFull();
		treeTable.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		
		treeTable.addContainerProperty("ma", String.class, "");
		treeTable.addContainerProperty("ten", String.class, "");
		treeTable.addContainerProperty("action", HorizontalLayout.class, null);
		treeTable.setColumnWidth("action", 130);
		treeTable.setColumnHeader("ma", "Mã");
		treeTable.setColumnHeader("ten", "Tên");
		treeTable.setColumnHeader("action", "Thao tác");

		treeTable.setColumnWidth("action", 220);
		treeTable.setRowHeaderMode(RowHeaderMode.INDEX);
		
		treeTable.setCellStyleGenerator(new TreeTable.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if("action".equals(propertyId) ){
                    return "left-aligned";
                }else if("trangThai".equals(propertyId)){
                    return "left-aligned ";
                }

            	HtDonVi current = (HtDonVi) itemId;
				Workflow<HtDonVi> wf = htDonViWM.getWorkflow(current);
				return wf.getStyleName();
			}
			
		});
		treeTable.removeAllItems();
		setContentTreeTable();
		dataWrap.addComponent(treeTable);
	}
	
	public void setContentTreeTable(){
		if(htDonViList == null){
			return;
		}
		for(HtDonVi q : htDonViList){
			addItemTreeTable(q);
		}
		for(HtDonVi q : htDonViList){
			treeTable.setParent(q, q.getHtDonVi());
		}
		
		for (Object itemId: treeTable.getContainerDataSource().getItemIds()) {
			treeTable.setCollapsed(itemId, false);
			
		    if (!treeTable.hasChildren(itemId)){
		    	treeTable.setChildrenAllowed(itemId, false);
		    }
		}
	}
	
	public void addItemTreeTable(HtDonVi q){
		if(q != null){
			Workflow<HtDonVi> wf = htDonViWM.getWorkflow(q);
			
			HorizontalLayout actionWrap = new HorizontalLayout();
			actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
	        
			new ButtonAction<HtDonVi>().getButtonAction(actionWrap, wf, currentView);
	        
	        
        	Button btnAddChild = new Button();
        	btnAddChild.setId(BTN_ADD_CHILD_ID);
        	btnAddChild.setData(q);
        	btnAddChild.setIcon(FontAwesome.PLUS);
        	btnAddChild.setDescription("Thêm đơn vị con");
        	btnAddChild.addStyleName(ExplorerLayout.BUTTON_ACTION);
        	btnAddChild.addStyleName(Reindeer.BUTTON_LINK);
        	btnAddChild.addClickListener(currentView);
	        actionWrap.addComponent(btnAddChild);
	        
	        
			treeTable.addItem(new Object[]{q.getMa(), q.getTen(), actionWrap}, q);
			
			
		}
		
	}
	public void reloadData(){
		treeTable.removeAllItems();
		setContentTreeTable();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			if(BTN_SEARCH_ID.equals(event.getButton().getId())){
				try {
				if(searchFieldGroup.isValid()){
					searchFieldGroup.commit();
					String ma = (String) searchItem.getItemProperty("ma").getValue();
					ma = ma != null ? "%"+ma.trim()+"%" : null;
					String ten = (String) searchItem.getItemProperty("ten").getValue();
					ten = ten != null ? "%"+ten.trim()+"%" : null;
					ComboboxItem trangThai = (ComboboxItem) searchItem.getItemProperty("trangThai").getValue();
					List<Long> trangThaiList = null;
					if(trangThai != null && trangThai.getValue() != null){
						trangThaiList = new ArrayList<Long>();
						trangThaiList.add((Long) trangThai.getValue());
					}
					htDonViList = htDonViDao.getHtDonViFind(ma, ten,null, trangThaiList,  -1, -1);
					treeTable.removeAllItems();
					setContentTreeTable();
				}
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
		    	searchFieldGroup.clear();
		    	htDonViList = htDonViDao.getHtDonViFind(null, null,null, null,  -1, -1);
		    	treeTable.removeAllItems();
				setContentTreeTable();
			}else if(ButtonAction.BTN_EDIT_ID.equals(event.getButton().getId())){
				HtDonVi currentRow = (HtDonVi) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(HtDonViDetailView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getMa().toLowerCase());
			}else if(ButtonAction.BTN_DEL_ID.equals(event.getButton().getId())){
				HtDonVi currentRow = (HtDonVi) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(HtDonViDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getMa().toLowerCase());
			}else if(ButtonAction.BTN_APPROVE_ID.equals(event.getButton().getId())){
				HtDonVi currentRow = (HtDonVi) event.getButton().getData();
	//	        UI.getCurrent().getNavigator().navigateTo(HtDonViDetailView.VIEW_NAME+"/"+Constants.ACTION_APPROVE+"/"+currentRow.getMa().toLowerCase());
				Workflow<HtDonVi> htDonViWf = htDonViWM.getWorkflow(currentRow);
				HtDonVi current = htDonViWf.pheDuyet();
				htDonViDao.save(current);
				reloadData();
				Notification notf = new Notification("Thông báo", htDonViWf.getPheDuyetTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(Page.getCurrent());
			}else if(ButtonAction.BTN_REFUSE_ID.equals(event.getButton().getId())){
				HtDonVi currentRow = (HtDonVi) event.getButton().getData();
		        //UI.getCurrent().getNavigator().navigateTo(HtDonViDetailView.VIEW_NAME+"/"+Constants.ACTION_REFUSE+"/"+currentRow.getMa().toLowerCase());
				Workflow<HtDonVi> htDonViWf = htDonViWM.getWorkflow(currentRow);
				HtDonVi current = htDonViWf.tuChoi();
				htDonViDao.save(current);
				reloadData();
				Notification notf = new Notification("Thông báo", htDonViWf.getTuChoiTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(Page.getCurrent());
			}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
				 UI.getCurrent().getNavigator().navigateTo(HtDonViDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD);
			}else if(BTN_ADD_CHILD_ID.equals(event.getButton().getId())){
				HtDonVi currentRow = (HtDonVi) event.getButton().getData();
				 UI.getCurrent().getNavigator().navigateTo(HtDonViDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD+"/"+currentRow.getMa().toLowerCase());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra", Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
		}
	}

}

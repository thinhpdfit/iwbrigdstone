package com.eform.ui.view.task;

import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;

import com.vaadin.ui.Button.ClickListener;
import com.eform.common.ExplorerLayout;
import com.eform.ui.custom.paging.TaskPaginationBar;
import com.vaadin.ui.CssLayout;

public class TaskListContainer extends CssLayout{
	
	private CssLayout taskContainer;
	private TaskPaginationBar paginationBar;
	
	public TaskListContainer(TaskService taskService,RepositoryService repositoryService, String category, List<String> groupIds, String userId,  ClickListener clickListener){
		addStyleName(ExplorerLayout.TASK_LIST_WRAP);
		setWidth("100%");
		taskContainer = new CssLayout();
		taskContainer.addStyleName(ExplorerLayout.DATA_WRAP);
		taskContainer.setWidth("100%");
		addComponent(taskContainer);
		
		paginationBar = new TaskPaginationBar(taskContainer, taskService, repositoryService, category, groupIds, userId, clickListener);
		if(paginationBar.getRowCount() > 0){
			CssLayout pagingWrap = new CssLayout();
			pagingWrap.setWidth("100%");
			pagingWrap.addStyleName(ExplorerLayout.PAGING_WRAP);
			pagingWrap.addComponent(paginationBar);
			addComponent(pagingWrap);
		}
	}
}

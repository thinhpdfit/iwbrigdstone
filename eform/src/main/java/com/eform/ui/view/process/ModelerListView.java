package com.eform.ui.view.process;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.type.bieumau.BieuMauExportModel;
import com.eform.common.utils.AuthenticationUtils;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.ExportUtils;
import com.eform.dao.HtQuyenServices;
import com.eform.model.entities.NhomQuyTrinh;
import com.eform.service.GroupQuyenServices;
import com.eform.service.NhomQuyTrinhService;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.UploadBpmnInfo;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = ModelerListView.VIEW_NAME)
public class ModelerListView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;

	public static final String VIEW_NAME = "app";
	public static final String MAIN_TITLE = "Xây dựng quy trình";
	public static final String SMALL_TITLE = "Danh sách quy trình và tìm kiếm";
	public static final String BTN_DETAIL_ID = "detail";
	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	

	public static final String BTN_VIEW_ID = "xem";
	public static final String BTN_EDIT_ID = "sua";
	public static final String BTN_EDIT_MODEL_ID = "suaModel";
	public static final String BTN_DEL_ID = "xoa";
	public static final String BTN_DEPLOY_ID = "deploy";
	public static final String BTN_ADDNEW_ID = "themMoi";
	public static final String BTN_IMPORT_ID = "import";
	public static final String BTN_EXPORT_ID = "export";
	public static final String BTN_EXPORT_ALL_ID = "export-all";
	
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;
	private Button btnAddNew;
	private Button btnImport;
	private final PropertysetItem searchItem = new PropertysetItem();
	private final FieldGroup searchFieldGroup = new FieldGroup();
	private ModelerContainer container;
	
	private List<NhomQuyTrinh> nhomQuyTrinhList;
	@Autowired
	private NhomQuyTrinhService nhomQuyTrinhService;
	@Autowired
	private GroupQuyenServices groupQuyenServices;
	@Autowired
	private HtQuyenServices htQuyenService;
	@Autowired
	private IdentityService identityService;

	private Map<Long, Boolean> mapQuyen;
	
	private Window importWindow;
	
	@PostConstruct
    void init() {
		messageSourceAccessor= new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());
		mapQuyen = AuthenticationUtils.getQuyen(null ,VIEW_NAME, CommonUtils.getGroupId(identityService), htQuyenService, groupQuyenServices);
		nhomQuyTrinhList = nhomQuyTrinhService.getNhomQuyTrinhFind(null, null, null, null, -1, -1);
		searchItem.addItemProperty("name", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("category", new ObjectProperty<NhomQuyTrinh>(new NhomQuyTrinh()));
    	searchFieldGroup.setItemDataSource(searchItem);
		initMainTitle();
		initSearchForm();
		initContent();
		initImportWindow();
		btnAddNew = new Button();
    	btnAddNew.setIcon(FontAwesome.PLUS);
    	btnAddNew.addClickListener(this);
    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
    	btnAddNew.setDescription(messageSourceAccessor.getMessage(Messages.UI_VIEW_MODELER_ADD));
    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
    	btnAddNew.setId(BTN_ADDNEW_ID);
		addComponent(btnAddNew);
		
		
		btnImport = new Button();
		btnImport.setIcon(FontAwesome.CLOUD_UPLOAD);
		btnImport.addClickListener(this);
		btnImport.addStyleName(Reindeer.BUTTON_LINK);
		btnImport.setDescription("Import");
		btnImport.addStyleName(ExplorerLayout.BUTTON_IMPORT);
		btnImport.setId(BTN_IMPORT_ID);
		addComponent(btnImport);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		
	}
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSourceAccessor.getMessage(Messages.UI_VIEW_HOME));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		
		CssFormLayout searchForm = new CssFormLayout(-1, 60);

		TextField txtTen = new TextField("Tên quy trình");
		searchForm.addFeild(txtTen);
		
		BeanItemContainer<NhomQuyTrinh> container = new BeanItemContainer<NhomQuyTrinh>(NhomQuyTrinh.class, nhomQuyTrinhList);
    	ComboBox cbbCategory = new ComboBox("Ứng dụng", container);
    	cbbCategory.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbCategory.setItemCaptionPropertyId("ten");
    	searchForm.addFeild(cbbCategory);

		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(this);
		searchForm.addButton(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(this);
		searchForm.addButton(btnRefresh);
		
		searchFieldGroup.bind(txtTen, "name");
		searchFieldGroup.bind(cbbCategory, "category");
		
		addComponent(searchForm);
	}
	
	public void initImportWindow(){
		importWindow = new Window("Import model");
		importWindow.setModal(true);
		importWindow.setWidth("270px");
		importWindow.setHeight("170px");
		
		CssLayout importContent = new CssLayout();
		importContent.setWidth("100%");
		importContent.addStyleName(ExplorerLayout.IMPORT_WINDOWN_CONTENT);
		
		UploadBpmnInfo uploadBpmnInfo = new UploadBpmnInfo(importWindow, container);
		Upload btnUploadBpmn = new Upload(null, uploadBpmnInfo);
		btnUploadBpmn.addStyleName(ExplorerLayout.BTN_UPLOAD_BPMN);
		btnUploadBpmn.setImmediate(true);
		btnUploadBpmn.setButtonCaption("Upload bpmn file");
		btnUploadBpmn.addStartedListener(uploadBpmnInfo);
		btnUploadBpmn.addFinishedListener(uploadBpmnInfo);
		btnUploadBpmn.setReceiver(uploadBpmnInfo);
		btnUploadBpmn.addSucceededListener(uploadBpmnInfo);
		importContent.addComponent(btnUploadBpmn);
		
		importWindow.setContent(importContent);
	}
	
	public void initContent(){
		container = new ModelerContainer(repositoryService, this, mapQuyen);
		addComponent(container);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_SEARCH_ID.equals(event.getButton().getId())){
			try {
				if(searchFieldGroup.isValid()){
					searchFieldGroup.commit();
					String name = (String) searchItem.getItemProperty("name").getValue();
					name = name != null ? name.trim() : "";
					
					String nhomQTStr = null;
					NhomQuyTrinh nhomQT = (NhomQuyTrinh) searchItem.getItemProperty("category").getValue();
					if(nhomQT != null){
						nhomQTStr = nhomQT.getMa();
					}
					container.getPaginationBar().search(name, nhomQTStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
			searchFieldGroup.clear();
			container.getPaginationBar().search(null, null);
		}else if(BTN_EDIT_ID.equals(event.getButton().getId())){
			Model currentRow = (Model) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(ModelerDetailView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getId());
		}else if(BTN_EDIT_MODEL_ID.equals(event.getButton().getId())){
			Model current = (Model) button.getData();
			String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
			UI.getCurrent().getPage().open(contextPath+"/modeler.html?modelId="+current.getId(), "_blank"); //setLocation(contextPath+"/modeler.html?modelId="+current.getId());
		}else if(BTN_DEL_ID.equals(event.getButton().getId())){
			Model currentRow = (Model) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(ModelerDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getId());
		}else if(BTN_DEPLOY_ID.equals(event.getButton().getId())){
			Model currentRow = (Model) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(ModelerDetailView.VIEW_NAME+"/"+Constants.ACTION_DEPLOY+"/"+currentRow.getId());
		}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
			UI.getCurrent().getNavigator().navigateTo(ModelerDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD);
		}else if(BTN_IMPORT_ID.equals(event.getButton().getId())){
			UI.getCurrent().addWindow(importWindow);
		}else if(BTN_EXPORT_ALL_ID.equals(event.getButton().getId())){
			Model currentRow = (Model) event.getButton().getData();
			if(currentRow != null){
				try {
					String str = ExportUtils.exportModeler(currentRow.getId());
					System.out.println(str);
					
					BieuMauExportModel bieuMauExport =  ExportUtils.getBieuMauExport(str);
					System.out.println("123");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

}

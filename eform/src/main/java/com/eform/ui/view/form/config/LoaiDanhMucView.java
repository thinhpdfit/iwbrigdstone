package com.eform.ui.view.form.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.workflow.ITrangThai;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.service.LoaiDanhMucService;
import com.eform.ui.custom.ButtonAction;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.StatusCommentLayout;
import com.eform.ui.custom.paging.LoaiDanhMucPaginationBar;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/*
 * Ẩn loại danh mục (tạm thời chưa sử dụng)
 * */


@SpringView(name = LoaiDanhMucView.VIEW_NAME)
public class LoaiDanhMucView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "loai-danh-muc";
	static final String MAIN_TITLE = "Loại danh mục";
	private static final String SMALL_TITLE = "";

	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	private static final String BTN_VIEW_ID = "xem";
	private static final String BTN_ADDNEW_ID = "themMoi";
	private final LoaiDanhMucView currentView = this;
	
	@Autowired
	LoaiDanhMucService loaiDanhMucService;
	private List<LoaiDanhMuc> loaiDanhMucSearchList;
	private CssLayout mainTitleWrap;
	private BeanItemContainer<LoaiDanhMuc> container;
	private Table table = new Table();
	private WorkflowManagement<LoaiDanhMuc> loaiDanhMucWM;
	
	private final PropertysetItem searchItem = new PropertysetItem();
	private final FieldGroup searchFieldGroup = new FieldGroup();
	private Button btnAddNew;
	
	private LoaiDanhMucPaginationBar paginationBar;
	@Autowired
	private MessageSource messageSource;
	
	@PostConstruct
    void init() {
		loaiDanhMucWM = new WorkflowManagement<LoaiDanhMuc>(EChucNang.E_FORM_LOAI_DANH_MUC.getMa());
		
		loaiDanhMucSearchList = loaiDanhMucService.getLoaiDanhMucFind(null, null, null, null, -1, -1);
		
		container = new BeanItemContainer<LoaiDanhMuc>(LoaiDanhMuc.class, new ArrayList());
		paginationBar = new LoaiDanhMucPaginationBar(container, loaiDanhMucService);
    	
    	searchItem.addItemProperty("ma", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("ten", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("trangThai", new ObjectProperty<ComboboxItem>(new ComboboxItem()));
    	searchItem.addItemProperty("loaiDanhMuc", new ObjectProperty<LoaiDanhMuc>(new LoaiDanhMuc()));
    	searchFieldGroup.setItemDataSource(searchItem);
    	
		initMainTitle();
		initSearchForm();
		initDataContent();
		
		btnAddNew = new Button();
    	btnAddNew.setIcon(FontAwesome.PLUS);
    	btnAddNew.addClickListener(this);
    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
    	btnAddNew.setDescription("Tạo mới loại danh mục");
    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
    	btnAddNew.setId(BTN_ADDNEW_ID);
		addComponent(btnAddNew);
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		
		CssFormLayout searchForm = new CssFormLayout(-1, 60);
		
		TextField txtMaLoaiDanhMuc = new TextField("Mã loại danh mục");
		searchForm.addFeild(txtMaLoaiDanhMuc);
		
		TextField txtTenLoaiDanhMuc = new TextField("Tên loại danh mục");
		searchForm.addFeild(txtTenLoaiDanhMuc);
		
//		BeanItemContainer<LoaiDanhMuc> cbbLoaiDMContainer = new BeanItemContainer<LoaiDanhMuc>(LoaiDanhMuc.class, loaiDanhMucSearchList);
//		ComboBox cbbLoaiDanhMuc = new ComboBox("Loại danh mục cha", cbbLoaiDMContainer);
//    	cbbLoaiDanhMuc.setItemCaptionMode(ItemCaptionMode.PROPERTY);
//    	cbbLoaiDanhMuc.setItemCaptionPropertyId("ten");
//    	searchForm.addFeild(cbbLoaiDanhMuc);
    	
    	
    	List<ITrangThai> trangThaiListTmp = loaiDanhMucWM.getTrangThaiList();
    	List<ComboboxItem> trangThaiLoaiDanhMucList = new ArrayList<>();
    	if(trangThaiListTmp != null){
    		for (ITrangThai trangThai : trangThaiListTmp) {
    			trangThaiLoaiDanhMucList.add(new ComboboxItem(trangThai.getTen(), new Long(trangThai.getId())));
			}
    	}
    	
    	BeanItemContainer<ComboboxItem> cbbTrangThaiContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, trangThaiLoaiDanhMucList);
		ComboBox cbbTrangThai = new ComboBox("Trạng thái", cbbTrangThaiContainer);
		cbbTrangThai.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbTrangThai.setItemCaptionPropertyId("text");
    	searchForm.addFeild(cbbTrangThai);

		searchFieldGroup.bind(txtMaLoaiDanhMuc, "ma");
		searchFieldGroup.bind(txtTenLoaiDanhMuc, "ten");
//		searchFieldGroup.bind(cbbLoaiDanhMuc, "loaiDanhMuc");
		searchFieldGroup.bind(cbbTrangThai, "trangThai");
		
		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(currentView);
		searchForm.addButton(btnSearch);
		paginationBar.fixEnterHotKey(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(currentView);
		searchForm.addButton(btnRefresh);
		
		addComponent(searchForm);
	}
	
	public void initDataContent(){
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
		List<ITrangThai> trangThaiList = loaiDanhMucWM.getTrangThaiList();
		if(trangThaiList != null && !trangThaiList.isEmpty()){
			dataWrap.addComponent(new StatusCommentLayout(trangThaiList));
		}
		
		table.setContainerDataSource(container);
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setResponsive(true);
		table.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		table.setColumnHeader("ma", "Mã");
		table.setColumnHeader("ten", "Tên");
		table.setColumnHeader("loaiDanhMuc", "Loại danh mục");
		table.addGeneratedColumn("loaiDanhMuc", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				@SuppressWarnings("unchecked")
				Property<LoaiDanhMuc> prop = source.getItem(itemId).getItemProperty(columnId);
				LoaiDanhMuc loaiDanhMuc = prop.getValue();
				if(loaiDanhMuc != null){
					return loaiDanhMuc.getTen();
				}
				return null;
			}
		});
		
		
		table.addGeneratedColumn("action", new ColumnGenerator() { 
			private static final long serialVersionUID = 1L;

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				LoaiDanhMuc current = (LoaiDanhMuc) itemId;
				Workflow<LoaiDanhMuc> wf = loaiDanhMucWM.getWorkflow(current);
				
				HorizontalLayout actionWrap = new HorizontalLayout();
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);

		        new ButtonAction<LoaiDanhMuc>().getButtonAction(actionWrap, wf, currentView);
		        return actionWrap;
		    }
		});

		table.setColumnWidth("action", 170);
		table.setColumnHeader("action", "Thao tác");
		table.setRowHeaderMode(RowHeaderMode.INDEX);

		table.setVisibleColumns("ma", "ten", "loaiDanhMuc", "action");
		

		table.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if ("ma".equals(propertyId)) {
                    return "left-aligned";
                }else if("ten".equals(propertyId)){
                    return "left-aligned";
                }else if("trangThai".equals(propertyId)){
                    return "center-aligned ";
                }else if("loaiDanhMuc".equals(propertyId)){
                    return "left-aligned";
                }else if("action".equals(propertyId)){
                    return "left-aligned";
                }
            	LoaiDanhMuc current = (LoaiDanhMuc) itemId;
				Workflow<LoaiDanhMuc> wf = loaiDanhMucWM.getWorkflow(current);
				return wf.getStyleName();
			}
			
		});

		dataWrap.addComponent(table);
		dataWrap.addComponent(paginationBar);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if(BTN_SEARCH_ID.equals(event.getButton().getId())){
			try {
				if(searchFieldGroup.isValid()){
					searchFieldGroup.commit();
					String ma = (String) searchItem.getItemProperty("ma").getValue();
					ma = ma != null ? "%"+ma.trim()+"%" : null;
					String ten = (String) searchItem.getItemProperty("ten").getValue();
					ten = ten != null ? "%"+ten.trim()+"%" : null;
					ComboboxItem trangThai = (ComboboxItem) searchItem.getItemProperty("trangThai").getValue();
					List<Long> trangThaiList = null;
					if(trangThai != null && trangThai.getValue() != null){
						trangThaiList = new ArrayList<Long>();
						trangThaiList.add((Long) trangThai.getValue());
					}
//					LoaiDanhMuc loaiDanhMuc = (LoaiDanhMuc) searchItem.getItemProperty("loaiDanhMuc").getValue();
//					if(loaiDanhMuc != null && loaiDanhMuc.getId() == null){
//						loaiDanhMuc = null;
//					}
//					paginationBar.search(ma, ten, trangThaiList, loaiDanhMuc);
					paginationBar.search(ma, ten, trangThaiList, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
	    	searchFieldGroup.clear();
	    	paginationBar.search(null, null, null, null);
		}else if(BTN_VIEW_ID.equals(event.getButton().getId())){
			LoaiDanhMuc currentRow = (LoaiDanhMuc) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getMa().toLowerCase());
		}else if(ButtonAction.BTN_EDIT_ID.equals(event.getButton().getId())){
			LoaiDanhMuc currentRow = (LoaiDanhMuc) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucDetailView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getMa().toLowerCase());
		}else if(ButtonAction.BTN_DEL_ID.equals(event.getButton().getId())){
			LoaiDanhMuc currentRow = (LoaiDanhMuc) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getMa().toLowerCase());
		}else if(ButtonAction.BTN_APPROVE_ID.equals(event.getButton().getId())){
			try {
				LoaiDanhMuc currentRow = (LoaiDanhMuc) event.getButton().getData();
				//phe duyet nhanh
				Workflow<LoaiDanhMuc> loaiDanhMucWf = loaiDanhMucWM.getWorkflow(currentRow);
				
				LoaiDanhMuc current = loaiDanhMucWf.pheDuyet();
				
				loaiDanhMucService.save(current);
				paginationBar.reloadData();
				
				Notification notf = new Notification("Thông báo", loaiDanhMucWf.getPheDuyetTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(Page.getCurrent());
			} catch (Exception e) {
				e.printStackTrace();
			}
	        //UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucDetailView.VIEW_NAME+"/"+Constants.ACTION_APPROVE+"/"+currentRow.getMa().toLowerCase());
		}else if(ButtonAction.BTN_REFUSE_ID.equals(event.getButton().getId())){
			
			try {
				LoaiDanhMuc currentRow = (LoaiDanhMuc) event.getButton().getData();
				//tuchoi nhanh
				Workflow<LoaiDanhMuc> loaiDanhMucWf = loaiDanhMucWM.getWorkflow(currentRow);
				LoaiDanhMuc current = loaiDanhMucWf.tuChoi();
				
				loaiDanhMucService.save(current);
				paginationBar.reloadData();
				
				Notification notf = new Notification("Thông báo", loaiDanhMucWf.getTuChoiTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(Page.getCurrent());
			} catch (Exception e) {
				e.printStackTrace();
			}
	       // UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucDetailView.VIEW_NAME+"/"+Constants.ACTION_REFUSE+"/"+currentRow.getMa().toLowerCase());
		}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
	        UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD);
		}
	}

}

package com.eform.ui.view.process;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricIdentityLink;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.ProcessInstanceUtils;
import com.eform.common.utils.TaskUtils;
import com.eform.model.entities.HoSo;
import com.eform.service.HoSoService;
import com.eform.ui.custom.CollapsePanelLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.ProcessDiagramViewer;
import com.eform.ui.form.FormGenerate;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = MyProcessInstanceDetailView.VIEW_NAME)
public class MyProcessInstanceDetailView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "my-instance-detail";
	private static String MAIN_TITLE = "My Instance Detail";
	private static String SMALL_TITLE = "";

	private CssLayout formContainer;
	
	private String action;
	
	private HistoricProcessInstance processInstanceCurrent;

	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_ACTION_ID = "btn-action";
	private static final String BUTTON_TL_DETAIL_ID = "btn-tl-detail";
	private static final String BUTTON_SUBPROCESS_DETAIL_ID = "btn-subprocess-detail";
	private static final String BUTTON_SUBTASK_ID = "btn-subtask-detail";
	private Button btnBack;
	private Button btnAction;

	@Autowired
	private RepositoryService repositoryService;	
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private HoSoService hoSoService;
	
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;
	private HoSo hoSoCurrent;
	private Window taskEventWindow;

	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();
	
	
	@PostConstruct
    void init() {
		messageSourceAccessor= new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());
		MAIN_TITLE = messageSourceAccessor.getMessage(Messages.UI_VIEW_PROCESSINSTANCE_DETAIL_TITLE);
		SMALL_TITLE = messageSourceAccessor.getMessage(Messages.UI_VIEW_PROCESSINSTANCE_DETAIL_TITLE_SMALL);
		initMainTitle();
	}
	
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_:]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				String id = m.group(3);
				processInstanceCurrent = historyService.createHistoricProcessInstanceQuery().processInstanceId(id).singleResult();
				if(processInstanceCurrent != null){
					
					String processParentId = getParentInstance(processInstanceCurrent);
					List<HoSo> hsList = null;
					if (processParentId != null) {
						hsList = hoSoService.getHoSoFind(null, null, null, processParentId, 0, 1);
					} else {
						hsList = hoSoService.getHoSoFind(null, null, null, processInstanceCurrent.getId(), 0,
								1);
					}
					if(hsList != null && !hsList.isEmpty()){
						hoSoCurrent = hsList.get(0);
					}
				}
				initTaskEventWindow();
				initDetailForm();
				
				if(Constants.ACTION_DEL.equals(action)){
					initDeleteReasonForm();
				}
			}
		}
		initBtnGroup();
	}
	
	
	public void initTaskEventWindow() {
		taskEventWindow = new Window("Chi tiết");
		taskEventWindow.setWidth(90, Unit.PERCENTAGE);
		taskEventWindow.setHeight(90, Unit.PERCENTAGE);
		taskEventWindow.center();
		taskEventWindow.setModal(true);
	}
	
	public String getParentInstance(HistoricProcessInstance proIns) {
		try {
			
			if(proIns.getSuperProcessInstanceId() == null){
				return proIns.getSuperProcessInstanceId();
			}else{
				HistoricProcessInstance superProIns = historyService.createHistoricProcessInstanceQuery().processInstanceId(proIns.getSuperProcessInstanceId()).singleResult();
				while (superProIns != null && superProIns.getSuperProcessInstanceId() != null) {
					superProIns = historyService.createHistoricProcessInstanceQuery().processInstanceId(superProIns.getSuperProcessInstanceId()).singleResult();
				}

				if(superProIns != null){
					return superProIns.getId();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSourceAccessor.getMessage(Messages.UI_VIEW_HOME));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(MyProcessInstancesTreeTableView.MAIN_TITLE);
		bcItem.setViewName(MyProcessInstancesTreeTableView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
	
	public void initDetailForm(){
		formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.addStyleName(ExplorerLayout.DETAIL_WRAP);
    	formContainer.addStyleName(ExplorerLayout.PROCESS_INS_CONTAINER);
    	
    	CollapsePanelLayout collapseWrap = new CollapsePanelLayout();
    	formContainer.addComponent(collapseWrap);
    	
    	CssLayout dataWrap = initHistoricWrap(processInstanceCurrent);
    	collapseWrap.addCollapsePanelItem("Chi tiết công việc đã thực hiện theo tiến trình", dataWrap);
		
		ProcessDiagramViewer svgDiagram = new ProcessDiagramViewer(processInstanceCurrent.getProcessDefinitionId() ,processInstanceCurrent.getId());
		svgDiagram.addStyleName(ExplorerLayout.PROCESS_INS_DIAGRAM);
		collapseWrap.addCollapsePanelItem("Quy trình công việc thực hiện", svgDiagram);
    	
    	addComponent(formContainer);
	}
	
	public void initDeleteReasonForm(){
		fieldGroup.setItemDataSource(itemValue);
		
		
		FormLayout deleteFormWrap = new FormLayout();
		deleteFormWrap.setWidth("100%");
		deleteFormWrap.addStyleName(ExplorerLayout.PROCESS_INS_DEL_REASON_FORM);
		addComponent(deleteFormWrap);
		
		TextField txtReason = new TextField("Lý do");
		txtReason.setWidth("100%");
		deleteFormWrap.addComponent(txtReason);
		
		txtReason.setRequired(true);
		txtReason.setRequiredError("Không được để trống");
		txtReason.addStyleName(ExplorerLayout.PROCESS_INS_DEL_REASON_TXT);
		txtReason.setNullRepresentation("");
		txtReason.setNullSettingAllowed(true);
		txtReason.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", 0, 2000, true));
		
		itemValue.addItemProperty("reason", new ObjectProperty<String>(null, String.class));
    	fieldGroup.bind(txtReason, "reason");
		
	}
	
	public void initBtnGroup(){
		CssLayout buttons = new CssLayout();
		buttons.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		buttons.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);
		
		if(Constants.ACTION_DEL.equals(action)){
			btnAction = new Button("Xóa");
			btnAction.setDescription("Xóa");
			btnAction.addClickListener(this);
			btnAction.setId(BUTTON_ACTION_ID);
			btnAction.addStyleName(ExplorerLayout.BUTTON_DANGER);
			btnAction.addStyleName(ExplorerLayout.BTN_INIT_GRID_BM);
			buttons.addComponent(btnAction);
		}else if(Constants.ACTION_APPROVE.equals(action)){
			btnAction = new Button("Sử dụng");
			btnAction.setDescription("Sử dụng");
			btnAction.addClickListener(this);
			btnAction.setId(BUTTON_ACTION_ID);
			btnAction.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
			btnAction.addStyleName(ExplorerLayout.BTN_INIT_GRID_BM);
			buttons.addComponent(btnAction);
		}else if(Constants.ACTION_REFUSE.equals(action)){
			btnAction = new Button("Ngừng sử dụng");
			btnAction.setDescription("Ngừng sử dụng");
			btnAction.addClickListener(this);
			btnAction.setId(BUTTON_ACTION_ID);
			btnAction.addStyleName(ExplorerLayout.BUTTON_WARNING);
			btnAction.addStyleName(ExplorerLayout.BTN_INIT_GRID_BM);
			buttons.addComponent(btnAction);
		}
		
		btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
		
		addComponent(buttons);
	}
	
	public CssLayout initHistoricWrap(HistoricProcessInstance processInstance){
		SimpleDateFormat dayMonthDf = new SimpleDateFormat(Constants.DATE_FORMAT_DAY_MONTH_TEXT);
		SimpleDateFormat timeDf = new SimpleDateFormat(Constants.DATE_FORMAT_TIME);
		List<String> years = new ArrayList();
		
		CssLayout wrap = new CssLayout();
		wrap.setWidth("100%");
		wrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_WRAP);
		
		SimpleDateFormat dfYear = new SimpleDateFormat(Constants.DATE_FORMAT_YEAR);
		
		if(processInstanceCurrent.getSuperProcessInstanceId() != null){
			HistoricProcessInstance superProcess = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceCurrent.getSuperProcessInstanceId()).singleResult();
			if(superProcess != null){

				CssLayout startYear = new CssLayout();
				startYear.setWidth("100%");
				startYear.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_YEAR_WRAP);
				wrap.addComponent(startYear);
				
				
				String yearStar = dfYear.format(superProcess.getStartTime());
				years.add(yearStar);
				Label startTime = new Label(yearStar);
				startTime.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_YEAR);
				startYear.addComponent(startTime);
				
				
				CssLayout item = initProcessItem(superProcess, years, true);
				if(item != null){
					wrap.addComponent(item);
				}
				
			}
		}
		
		
		
		if(processInstance.getStartTime() != null){
			String year = dfYear.format(processInstance.getStartTime());
			if(!years.contains(year)){
				years.add(year);
				
				CssLayout createYear = new CssLayout();
				createYear.setWidth("100%");
				createYear.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_YEAR_WRAP);
				wrap.addComponent(createYear);
				
				Label createTime = new Label(year);
				createTime.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_YEAR);
				createYear.addComponent(createTime);
			}
			
			
			
			CssLayout startTimeWrap = new CssLayout();
			startTimeWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_ITEM);
			startTimeWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_WRAP);
			wrap.addComponent(startTimeWrap);
			
			String endTime = "<span class='task-tl-date-day'>"+dayMonthDf.format(processInstance.getStartTime())+"</span>";
			endTime += "<span class='task-tl-date-time'>"+timeDf.format(processInstance.getStartTime())+"</span>";
			
			Label itemDate = new Label(endTime);
			itemDate.setContentMode(ContentMode.HTML);
			itemDate.addStyleName(ExplorerLayout.TASK_TIME_LINE_DATE);
			itemDate.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_DATE);
			startTimeWrap.addComponent(itemDate);
			
			CssLayout startEvent = new CssLayout();
			startEvent.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_TITLE_WRAP);
			
			String proNameTmp= "<span class='task-tl-process-name no-name'>#Không tên</span>";
	    	if(processInstanceCurrent.getProcessDefinitionId() != null){
				ProcessDefinition pro = repositoryService.createProcessDefinitionQuery().processDefinitionId(processInstanceCurrent.getProcessDefinitionId()).singleResult();
				if(pro != null){
					String proName = "#Không tên";
					if(pro.getName() != null){
						proName = pro.getName();
					}
					proNameTmp = "<span class='task-tl-process-ins-name'>"+proName+"</span>";
				}
			}
	    	proNameTmp += "<span class='task-tl-process-ins-id'>("+ processInstanceCurrent.getId()+")</span>";
	    			
			Label startTitle = new Label("Bắt đầu "+proNameTmp);
			startTitle.setContentMode(ContentMode.HTML);
			startTitle.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_TITLE);
			startTitle.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_TITLE);
			startEvent.addComponent(startTitle);
			startTimeWrap.addComponent(startEvent);
		}
		CssLayout mainContentWrap = new CssLayout();
		mainContentWrap.setWidth("100%");
		mainContentWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_MAIN);
		
		List<HistoricTaskInstance> tasks = historyService.createHistoricTaskInstanceQuery()
			      .processInstanceId(processInstance.getId())
			      .orderByTaskCreateTime().asc()
			      .list();
		List<Object> allItem = new ArrayList<Object>();
		if (tasks != null && !tasks.isEmpty()) {
			allItem.addAll(tasks);
		}
		//get sub process
		List<HistoricProcessInstance> subHistoricProcessInstanceList = historyService.createHistoricProcessInstanceQuery().superProcessInstanceId(processInstance.getId()).orderByProcessInstanceStartTime().asc().list();
		if(subHistoricProcessInstanceList != null){
			allItem.addAll(subHistoricProcessInstanceList);
		}
		//sort by create date
		new TaskUtils().sortList(allItem);
		
		
		for(Object itemObj : allItem){
			
			if(itemObj instanceof HistoricTaskInstance){
				HistoricTaskInstance task = (HistoricTaskInstance) itemObj;
				
				if(task.getCreateTime() != null && years != null){
					String year = dfYear.format(task.getCreateTime());
					if(!years.contains(year)){
						years.add(year);
						
						CssLayout createYear = new CssLayout();
						createYear.setWidth("100%");
						createYear.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_YEAR_WRAP);
						wrap.addComponent(createYear);
						
						Label createTime = new Label(year);
						createTime.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_YEAR);
						createYear.addComponent(createTime);
					}
				}
				
				
				CssLayout item = initHistoricTaskItem(task, years);
				if(item != null){
					wrap.addComponent(item);
				}
			}else if(itemObj instanceof HistoricProcessInstance){
				HistoricProcessInstance proInst = (HistoricProcessInstance) itemObj;
				
				if(proInst.getStartTime() != null && years != null){
					String year = dfYear.format(proInst.getStartTime());
					if(!years.contains(year)){
						years.add(year);
						
						CssLayout createYear = new CssLayout();
						createYear.setWidth("100%");
						createYear.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_YEAR_WRAP);
						wrap.addComponent(createYear);
						
						Label createTime = new Label(year);
						createTime.addStyleName(ExplorerLayout.TASK_TIME_LINE_START_YEAR);
						createYear.addComponent(createTime);
					}
				}
			
				CssLayout item = initProcessItem(proInst, years, false);
				if(item != null){
					wrap.addComponent(item);
				}
			}
		}
		
		if(processInstance.getEndTime() != null){
			CssLayout endTimeWrap = new CssLayout();
			endTimeWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_ITEM);
			endTimeWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_FINISH_WRAP);
			wrap.addComponent(endTimeWrap);
			
			String endTime = "<span class='task-tl-date-day'>"+dayMonthDf.format(processInstance.getEndTime())+"</span>";
			endTime += "<span class='task-tl-date-time'>"+timeDf.format(processInstance.getEndTime())+"</span>";
			
			Label itemDate = new Label(endTime);
			itemDate.setContentMode(ContentMode.HTML);
			itemDate.addStyleName(ExplorerLayout.TASK_TIME_LINE_DATE);
			itemDate.addStyleName(ExplorerLayout.TASK_TIME_LINE_FINISH_DATE);
			endTimeWrap.addComponent(itemDate);
			
			CssLayout endEvent = new CssLayout();
			endEvent.addStyleName(ExplorerLayout.TASK_TIME_LINE_FINISH_TITLE_WRAP);
			Label endTitle = new Label("Hoàn thành");
			endTitle.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_TITLE);
			endTitle.addStyleName(ExplorerLayout.TASK_TIME_LINE_FINISH_TITLE);
			endEvent.addComponent(endTitle);
			endTimeWrap.addComponent(endEvent);
		}
		
		return wrap;
	}
	
	
	public CssLayout initProcessItem(HistoricProcessInstance proInst, List<String> years, boolean isSuper){
		CssLayout item = new CssLayout();
		item.setWidth("100%");
		item.addStyleName(ExplorerLayout.TASK_TIME_LINE_ITEM);
		
		CssLayout itemInfo = new CssLayout();
		itemInfo.setWidth("100%");
		itemInfo.addStyleName(ExplorerLayout.TASK_TIME_LINE_INFO);
		item.addComponent(itemInfo);
		
		CssLayout itemDateWrap = new CssLayout();
		itemDateWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_DATE_WRAP);
		itemInfo.addComponent(itemDateWrap);
		
		String createTime = "#";
		if(proInst.getStartTime() != null){
			SimpleDateFormat dayMonthDf = new SimpleDateFormat(Constants.DATE_FORMAT_DAY_MONTH_TEXT);
			SimpleDateFormat timeDf = new SimpleDateFormat(Constants.DATE_FORMAT_TIME);

			createTime = "<span class='task-tl-date-day'>"+dayMonthDf.format(proInst.getStartTime())+"</span>";
			createTime += "<span class='task-tl-date-time'>"+timeDf.format(proInst.getStartTime())+"</span>";
		}
		
		Label itemDate = new Label(createTime);
		itemDate.setContentMode(ContentMode.HTML);
		itemDate.addStyleName(ExplorerLayout.TASK_TIME_LINE_DATE);
		itemDateWrap.addComponent(itemDate);
		
		CssLayout event = new CssLayout();
		event.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT);
		itemInfo.addComponent(event);
		
		CssLayout eventTtl = new CssLayout();
		eventTtl.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_TITLE_WRAP);
		event.addComponent(eventTtl);
		
		
		String proNameTmp= "<span class='task-tl-subprocess no-name'>#Không tên</span>";
    	if(proInst.getProcessDefinitionId() != null){
			ProcessDefinition pro = repositoryService.createProcessDefinitionQuery().processDefinitionId(proInst.getProcessDefinitionId()).singleResult();
			if(pro != null){
				String proName = "#Không tên";
				if(pro.getName() != null){
					proName = pro.getName();
				}
				proNameTmp = "<span class='task-tl-subprocess-name'>"+proName+"</span>";
			}
		}
    	proNameTmp += "<span class='task-tl-subprocess-ins-id'>("+ proInst.getId()+")</span>";
		
		Button btnViewSubprocess = new Button(proNameTmp);
		btnViewSubprocess.setHtmlContentAllowed(true);
		btnViewSubprocess.setId(BUTTON_SUBPROCESS_DETAIL_ID);
		btnViewSubprocess.addClickListener(this);
		btnViewSubprocess.setData(proInst);
		btnViewSubprocess.addStyleName(Reindeer.BUTTON_LINK);
		btnViewSubprocess.addStyleName(ExplorerLayout.TASK_TIME_LINE_SUBPROCESS_BTN);
		eventTtl.addComponent(btnViewSubprocess);
		
		
		if(isSuper){
			item.addStyleName(ExplorerLayout.TASK_TIME_LINE_SUPER_WRAP);
			itemDateWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_DATE_SUPER_WRAP);
		}else{

			item.addStyleName(ExplorerLayout.TASK_TIME_LINE_SUBPROCESS_WRAP);
			itemDateWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_DATE_SUBPROCESS_WRAP);
			
			CssLayout endTimeWrap = new CssLayout();
			endTimeWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_FINISH_WRAP);
			item.addComponent(endTimeWrap);
			SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
			String lblEndTime = null;
			if(proInst.getEndTime() != null){
				lblEndTime = "Hoàn thành ngày <span class='task-tl-item-fn-date'>"+df.format(proInst.getEndTime())+"</span>";
			}else{
				lblEndTime = "Đang thực hiện";
			}
			Label endTime = new Label(lblEndTime);
			endTime.setContentMode(ContentMode.HTML);
			endTime.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_FINISH);
			endTimeWrap.addComponent(endTime);
		}
		
		return item;
	}
	
	public CssLayout initHistoricTaskItem(HistoricTaskInstance task, List<String> years){
		
		CssLayout item = new CssLayout();
		item.setWidth("100%");
		item.addStyleName(ExplorerLayout.TASK_TIME_LINE_ITEM);
		
		CssLayout itemInfo = new CssLayout();
		itemInfo.setWidth("100%");
		itemInfo.addStyleName(ExplorerLayout.TASK_TIME_LINE_INFO);
		item.addComponent(itemInfo);
		
		CssLayout itemDateWrap = new CssLayout();
		itemDateWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_DATE_WRAP);
		itemInfo.addComponent(itemDateWrap);
		
		String createTime = "#";
		if(task.getCreateTime() != null){
			SimpleDateFormat dayMonthDf = new SimpleDateFormat(Constants.DATE_FORMAT_DAY_MONTH_TEXT);
			SimpleDateFormat timeDf = new SimpleDateFormat(Constants.DATE_FORMAT_TIME);

			createTime = "<span class='task-tl-date-day'>"+dayMonthDf.format(task.getCreateTime())+"</span>";
			createTime += "<span class='task-tl-date-time'>"+timeDf.format(task.getCreateTime())+"</span>";
		}
		
		Label itemDate = new Label(createTime);
		itemDate.setContentMode(ContentMode.HTML);
		itemDate.addStyleName(ExplorerLayout.TASK_TIME_LINE_DATE);
		itemDateWrap.addComponent(itemDate);
		
		CssLayout event = new CssLayout();
		event.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT);
		itemInfo.addComponent(event);
		
		CssLayout eventTtl = new CssLayout();
		eventTtl.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_TITLE_WRAP);
		event.addComponent(eventTtl);
		
		String eventTtlStr = "#Không tên";
		if(task.getName() != null && !task.getName().isEmpty()){
			eventTtlStr = task.getName();
		}
		Label title = new Label(eventTtlStr);
		title.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_TITLE);
		eventTtl.addComponent(title);
		
		
		
		if(task.getOwner() != null){
			CssLayout eventOwner = new CssLayout();
			eventOwner.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_OWNER_WRAP);
			event.addComponent(eventOwner);
			
			User u = identityService.createUserQuery().userId(task.getOwner()).singleResult();
			if(u != null){
				String eventOwnerStr = u.getFirstName()+" "+u.getLastName();
				Label owner = new Label(eventOwnerStr);
				owner.setIcon(FontAwesome.USER);
				owner.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_OWNER);
				eventOwner.addComponent(owner);
			}
		}
		
		
		if(task.getDescription() != null && !task.getDescription().isEmpty()){
			CssLayout eventDesc = new CssLayout();
			eventDesc.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_DESC_WRAP);
			event.addComponent(eventDesc);
			
			Label desc = new Label(task.getDescription());
			desc.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_DESC);
			eventDesc.addComponent(desc);
		}
		
		
		
		String assignee = "";
		if(task.getAssignee() != null){
			User u = identityService.createUserQuery().userId(task.getAssignee()).singleResult();
			if(u != null){
				assignee = "<span class='tl-task-u'>"+ FontAwesome.USER.getHtml()+u.getFirstName()+" "+u.getLastName()+"</span>";
			}
		}else{
			List<HistoricIdentityLink> idLink = historyService.getHistoricIdentityLinksForTask(task.getId());
			if(idLink != null && !idLink.isEmpty()){
				for(HistoricIdentityLink link : idLink){
					if(link.getGroupId() != null){
						Group g = identityService.createGroupQuery().groupId(link.getGroupId()).singleResult();
						if(g != null){
							assignee += "<span class='tl-task-g'>"+ FontAwesome.USERS.getHtml()+g.getName()+"</span>";
						}
					} 
					if(link.getUserId() != null){
						User u = identityService.createUserQuery().userId(link.getUserId()).singleResult();
						if(u != null){
							assignee = "<span class='tl-task-u'>"+ FontAwesome.USER.getHtml()+u.getFirstName()+" "+u.getLastName()+"</span>";
						}
					}
				}
				
			}
		}
		
		
		try {
			List<HistoricTaskInstance> process = new ArrayList();
			process.add(task);
			Map<String, List<HistoricTaskInstance>> parentChildMap = new HashMap();
			while(!process.isEmpty()){
				HistoricTaskInstance taskTmp = process.remove(0);
				List<HistoricTaskInstance> subTaskListTmp = historyService.createHistoricTaskInstanceQuery().taskParentTaskId(taskTmp.getId()).orderByHistoricTaskInstanceEndTime().asc().list();
				if(subTaskListTmp != null && !subTaskListTmp.isEmpty()){
					parentChildMap.put(taskTmp.getId(), subTaskListTmp);
					process.addAll(subTaskListTmp);
				}
			}
			
			if(parentChildMap != null && !parentChildMap.isEmpty()){
				CssLayout subtaskContainer = buildSubtaskContainer(parentChildMap, task);
				if(subtaskContainer != null){
					subtaskContainer.addStyleName("tl-subtask-wrap");
					item.addComponent(subtaskContainer);
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
		
		CssLayout duedateWrap = new CssLayout();
		if(task.getDueDate() != null){
			duedateWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_DUEDATE_WRAP);
			String duedateStr = df.format(task.getDueDate());
			if(assignee != null && !assignee.isEmpty()){
				duedateStr = "<span class='task-tl-item-duedate-ttl'>hạn thực hiện</span> <span class='task-tl-item-duedate'>"+duedateStr+"</span>";
			}
			Label duedateLbl = new Label(duedateStr);
			duedateLbl.setContentMode(ContentMode.HTML);
			duedateLbl.addStyleName(ExplorerLayout.TASK_TIME_LINE_DUEDATE);
			duedateWrap.addComponent(duedateLbl);
		}
		
		if(task.getEndTime() != null){
			
			if(task.getDueDate() != null && task.getEndTime().after(task.getDueDate())){
				duedateWrap.addStyleName("late");
				item.addComponent(duedateWrap);
			}
			
			if(task.getFormKey() != null && !task.getFormKey().isEmpty()){
				CssLayout eventDetail = new CssLayout();
				eventDetail.setWidth("100%");
				eventDetail.addStyleName(ExplorerLayout.TASK_TIME_LINE_ITEM_DETAIL_WRAP);
				item.addComponent(eventDetail);
				
				Button btnDetail = new Button();
				btnDetail.setDescription("Chi tiết");
				btnDetail.setIcon(FontAwesome.FILE_TEXT);
				btnDetail.addStyleName(ExplorerLayout.TASK_TIME_LINE_ITEM_DETAIL_BTN);
				btnDetail.addClickListener(this);
				btnDetail.setData(task);
				btnDetail.setId(BUTTON_TL_DETAIL_ID);
				eventDetail.addComponent(btnDetail);
			}
			
			
			CssLayout endTimeWrap = new CssLayout();
			endTimeWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_FINISH_WRAP);
			item.addComponent(endTimeWrap);
			
			String lblEndTime = df.format(task.getEndTime());
			if(assignee != null && !assignee.isEmpty()){
				lblEndTime = "<span class='task-tl-item-assignee'>"+assignee + "</span> hoàn thành ngày <span class='task-tl-item-fn-date'>"+lblEndTime+"</span>";
			}
			Label endTime = new Label(lblEndTime);
			endTime.setContentMode(ContentMode.HTML);
			endTime.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_FINISH);
			endTimeWrap.addComponent(endTime);
			
			if(task.getDueDate() != null && !task.getEndTime().after(task.getDueDate())){
				item.addComponent(duedateWrap);
			}
		}else{
			Date now = new Date();
			if(task.getDueDate() != null && now.after(task.getDueDate())){
				duedateWrap.addStyleName("late");
				item.addComponent(duedateWrap);
			}
			
			CssLayout runningWrap = new CssLayout();
			runningWrap.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_RUNNING_WRAP);
			item.addComponent(runningWrap);
			String lblRunningStr = "đang thực hiện";
			if(assignee != null && !assignee.isEmpty()){
				lblRunningStr = "<span class='task-tl-item-assignee'>"+assignee + "</span> " + lblRunningStr;
			}
			Label lblRunning = new Label(lblRunningStr);
			lblRunning.setContentMode(ContentMode.HTML);
			lblRunning.addStyleName(ExplorerLayout.TASK_TIME_LINE_EVENT_RUNNING);
			runningWrap.addComponent(lblRunning);
			
			if(task.getDueDate() != null && !now.after(task.getDueDate())){
				item.addComponent(duedateWrap);
			}
		}
		return item;
	}
	
	public CssLayout buildSubtaskContainer(Map<String, List<HistoricTaskInstance>> subTaskMap, TaskInfo task){
		List<HistoricTaskInstance> subtaskList = subTaskMap.get(task.getId());
		if(subtaskList != null){
			CssLayout subtaskContainer = new CssLayout();
			subtaskContainer.addStyleName("tl-subtask-container");
			int i =0;
			for (TaskInfo t : subtaskList) {
				CssLayout subtaskItem = buildSubtaskItem(++i, t);
				CssLayout sub = buildSubtaskContainer(subTaskMap, t);
				if(sub != null){
					subtaskItem.addComponent(sub);
				}
				subtaskContainer.addComponent(subtaskItem);
			}
			return subtaskContainer;
		}
		return null;
		
	}
	
	public CssLayout buildSubtaskItem(int i, TaskInfo t){
		CssLayout subtaskItem = new CssLayout();
		subtaskItem.addStyleName("tl-subtask-item");
		
		Button btnDetail = new Button(i+"");
		btnDetail.addStyleName("tl-btn-detail-subtask");
		btnDetail.addClickListener(this);
		btnDetail.setId(BUTTON_SUBTASK_ID);
		btnDetail.setData(t);
		btnDetail.setDescription("Chi tiết");
		
		subtaskItem.addComponent(btnDetail);
		
		String nameStr = "#Không tên";
		if(t.getName() != null && !t.getName().isEmpty()){
			nameStr = t.getName();
		}
		
		Label title = new Label(nameStr);
		title.addStyleName("tl-subtask-name");
		subtaskItem.addComponent(title);
		
		return subtaskItem;
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BUTTON_CANCEL_ID.equals(button.getId())){
				UI.getCurrent().getNavigator().navigateTo(MyProcessInstancesTreeTableView.VIEW_NAME);
			}else if(BUTTON_SUBPROCESS_DETAIL_ID.equals(button.getId())){
				HistoricProcessInstance currentRow = (HistoricProcessInstance) button.getData();
				String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
				UI.getCurrent().getPage().open(contextPath + "#!" +MyProcessInstanceDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId(), "_blank");
			}else if(BUTTON_TL_DETAIL_ID.equals(button.getId()) || BUTTON_SUBTASK_ID.equals(button.getId())){
				if(button.getData() != null &&  button.getData() instanceof HistoricTaskInstance){
					HistoricTaskInstance task = (HistoricTaskInstance) button.getData();
					String formKey = task.getFormKey();
					if(formKey == null && task.getParentTaskId() != null){
						TaskInfo taskTmp = ProcessInstanceUtils.getParentTaskIdInProcess(task.getId());
						if(taskTmp != null){
							formKey = taskTmp.getFormKey();
						}
					}
					if(formKey != null && !formKey.isEmpty()){
						
						Pattern p = Pattern.compile("([0-9a-zA-Z_-]+)#([0-9a-zA-Z_-]+)");
						Matcher m = p.matcher(formKey.trim());
						if(m.matches()){
							formKey = m.group(1);
						}
						
						CssLayout container = new CssLayout();
						container.setWidth("100%");
						container.addStyleName(ExplorerLayout.TASK_TIME_LINE_ITEM_DETAIL_CONTAINER);
						
						if(formKey != null){
							String hoSoXml = null;
							if (hoSoCurrent != null) {
								String boHoSoXml = hoSoCurrent.getHoSoGiaTri();
								EFormBoHoSo boHS = CommonUtils.getEFormBoHoSo(boHoSoXml);
								//TODO view form data history
								if(boHS != null && boHS.getLichSuList() != null && !boHS.getLichSuList().isEmpty()){
									for(EFormHoSo hs : boHS.getLichSuList()){
										if(hs != null && task.getId().equals(hs.getTaskId())){
											List<String> pathList = null;
											Integer currentThuTu = hs.getLoopItemIndex();
											if(hs.getLoopItemPath() != null){
												String[] pathArr = hs.getLoopItemPath().split("#");
									    		pathList = java.util.Arrays.asList(pathArr);
											}
											if(hs.getDataLoopPath() != null){
												String[] pathArr = hs.getDataLoopPath().split("#");
												List<String> pathListTmp = java.util.Arrays.asList(pathArr);
												hs = CommonUtils.getEFormHoSoByLoopPath(hs, pathListTmp);
											}
											hoSoXml = CommonUtils.getEFormHoSoString(hs, pathList, currentThuTu);
											break;
										}
									}
		 						}
							}
							if(hoSoXml != null){
								FormGenerate form = new FormGenerate(formKey, true, hoSoXml);
								form.setWidth("100%");
								
								container.addComponent(form);
							}else{
								Label label = new Label("Không có thông tin lịch sử công việc");
								label.addStyleName("tl-lbl-subtask-empty");
								container.addComponent(label);
							}
							
							taskEventWindow.setContent(container);
							String caption = "Chi tiết";
							if(task.getName() != null){
								caption += " "+task.getName();
							}
							taskEventWindow.setCaption(caption);
							UI.getCurrent().addWindow(taskEventWindow);
						}
					}
				}
				
			}else if(Constants.ACTION_DEL.equals(action)){
				try {
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						String reason = (String) itemValue.getItemProperty("reason").getValue();
						runtimeService.deleteProcessInstance(processInstanceCurrent.getId(), reason);
						Notification notf = new Notification("Thông báo","Xóa thành công");
						notf.setDelayMsec(3000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
						UI.getCurrent().getNavigator().navigateTo(MyProcessInstancesTreeTableView.VIEW_NAME);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}else if(Constants.ACTION_APPROVE.equals(action)){
				Notification notf = new Notification("Thông báo","Sử dụng thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				
				runtimeService.activateProcessInstanceById(processInstanceCurrent.getId());
				
				notf.show(Page.getCurrent());
				UI.getCurrent().getNavigator().navigateTo(MyProcessInstancesTreeTableView.VIEW_NAME);
			}else if(Constants.ACTION_REFUSE.equals(action)){
				Notification notf = new Notification("Thông báo","Ngừng sử dụng thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				
				runtimeService.suspendProcessInstanceById(processInstanceCurrent.getId());
				
				notf.show(Page.getCurrent());
				UI.getCurrent().getNavigator().navigateTo(MyProcessInstancesTreeTableView.VIEW_NAME);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

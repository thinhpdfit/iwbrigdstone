package com.eform.ui.view.report.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.eform.common.ThamSo;
import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.NhomQuyTrinh;
import com.eform.service.HtThamSoService;
import com.eform.service.NhomQuyTrinhService;

@Controller
@RequestMapping("/report")
public class DashboardReportController {
	
	@Autowired
	private NhomQuyTrinhService nhomQuyTrinhService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	protected transient FormService formService;
	@Autowired
	private HtThamSoService htThamSoService;
	
	@RequestMapping(value = "/general", method=RequestMethod.GET)
	public String general(HttpServletRequest request, Model model){
		List<Long> trangThaiSuDung = new ArrayList<Long>();
		trangThaiSuDung.add(1L);
		
		//Get danh sách nhóm quy trình
		List<NhomQuyTrinh> nhomQuyTrinhList = nhomQuyTrinhService.getNhomQuyTrinhFind(null, null, trangThaiSuDung, null, -1, -1);
		
		//Lay danh sách processsdefinition
		List<ProcessDefinition> processDefAll = repositoryService.createProcessDefinitionQuery().list();
		
		//Get danh sách quy trình theo nhóm
		Map<NhomQuyTrinh, List<ProcessDefinition>> nhomQuyTrinhProcessDefMap = new HashMap<NhomQuyTrinh, List<ProcessDefinition>>();
		
		List<ProcessDefinition> noCateList = new ArrayList<ProcessDefinition>();
		if(processDefAll != null){
			for(ProcessDefinition p : processDefAll){
				Deployment deployment = repositoryService.createDeploymentQuery().deploymentId(p.getDeploymentId()).singleResult();
				if(deployment != null && (deployment.getCategory() == null || deployment.getCategory().trim().isEmpty())){
					noCateList.add(p);
				}else if(deployment != null){
					for(NhomQuyTrinh nhomQuyTrinh : nhomQuyTrinhList){
						List<ProcessDefinition> pList = nhomQuyTrinhProcessDefMap.get(nhomQuyTrinh);
						if(pList == null){
							pList = new ArrayList<ProcessDefinition>();
							nhomQuyTrinhProcessDefMap.put(nhomQuyTrinh, pList);
						}
						if(deployment != null && deployment.getCategory().equals(nhomQuyTrinh.getMa())){
							pList.add(p);
						}
					}
				}
			}
		}
		if(noCateList != null && !noCateList.isEmpty()){
			nhomQuyTrinhProcessDefMap.put(null, noCateList);
		}
		
		//Đếm số lượng process instance đã hoàn thành và chưa hoàn thành
		Map<ProcessDefinition, Map<String, Long>> processDefStatusMap = new HashMap<ProcessDefinition, Map<String, Long>>();
		if(nhomQuyTrinhProcessDefMap != null){
			nhomQuyTrinhProcessDefMap.forEach((k,v)->{
				if(v != null){
					v.forEach(p->{
						Map<String, Long> statusCount = new HashMap<String, Long>();
						Long totalCount = historyService.createHistoricProcessInstanceQuery().processDefinitionId(p.getId()).count();
						Long finishedCount = historyService.createHistoricProcessInstanceQuery().processDefinitionId(p.getId()).finished().count();
						statusCount.put("RUNNING", totalCount - finishedCount);
						statusCount.put("FINISHED", finishedCount);
						processDefStatusMap.put(p, statusCount);
					});
				}
			});
		}
		JSONArray jsonArr = new JSONArray();
		HtThamSo colorHtThamSo = htThamSoService.getHtThamSoFind(ThamSo.COLOR_LIST);
		if(colorHtThamSo != null && colorHtThamSo.getGiaTri() != null){
			String colorArr[] = colorHtThamSo.getGiaTri().split(",");
			if(colorArr != null  && colorArr.length >0){
				for(int i =0; i<colorArr.length ; i++){
					if(colorArr[i] != null){
						jsonArr.put(colorArr[i].trim());
					}
				}
			}

			model.addAttribute("colors", jsonArr.toString());
		}
		model.addAttribute("jsonData", getJSONData(nhomQuyTrinhProcessDefMap, processDefStatusMap));
		return "report-dashboard";
	}
	
	public String getJSONData(Map<NhomQuyTrinh, List<ProcessDefinition>> nhomQuyTrinhProcessDefMap, 
			Map<ProcessDefinition, Map<String, Long>> processDefStatusMap){
		try {
			JSONObject jsonRoot = new JSONObject();
			jsonRoot.put("name", "root");
			JSONArray rootChild = new JSONArray();
			jsonRoot.put("children", rootChild);
			if(nhomQuyTrinhProcessDefMap != null){
				nhomQuyTrinhProcessDefMap.forEach((nhomQuyTrinh,processDefList)->{
					
					if(processDefList != null && !processDefList.isEmpty()){
						String name = "Uncategorized";
						if(nhomQuyTrinh != null){
							name = nhomQuyTrinh.getTen();
						}
						JSONObject jsonNhomQuyTrinh = new JSONObject();
						rootChild.put(jsonNhomQuyTrinh);
						jsonNhomQuyTrinh.put("name", name);
						JSONArray nhomQuyTrinhChild = new JSONArray();
						jsonNhomQuyTrinh.put("children", nhomQuyTrinhChild);
						
						processDefList.forEach(p->{
							if(p != null){
								JSONObject jsonProcessDef = new JSONObject();
								jsonProcessDef.put("name", (p.getName() != null ? p.getName() : "")+" ("+p.getDeploymentId()+")");
								nhomQuyTrinhChild.put(jsonProcessDef);
								JSONArray processDefChild = new JSONArray();
								jsonProcessDef.put("children", processDefChild);
								
								Map<String, Long> statusCount = processDefStatusMap.get(p);
								JSONObject jsonStatusCount = new JSONObject();
								jsonStatusCount.put("name", "Đã hoàn thành");
								jsonStatusCount.put("size", statusCount.get("FINISHED"));
								processDefChild.put(jsonStatusCount);
								
								jsonStatusCount = new JSONObject();
								jsonStatusCount.put("name", "Chưa hoàn thành");
								jsonStatusCount.put("size", statusCount.get("RUNNING"));
								processDefChild.put(jsonStatusCount);
							}
						});
					}
				});
			}
			return jsonRoot.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}

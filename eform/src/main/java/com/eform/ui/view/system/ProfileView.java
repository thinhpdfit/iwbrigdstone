package com.eform.ui.view.system;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Picture;
import org.activiti.engine.identity.User;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.ThamSo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.FileUtils;
import com.eform.common.utils.StringUtils;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.HtChucVuDao;
import com.eform.model.entities.HtChucVu;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.HtThamSo;
import com.eform.service.HtDonViServices;
import com.eform.service.HtThamSoService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.UploadFileInfo;
import com.eform.ui.view.dashboard.DashboardView;
import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SpringView(name = ProfileView.VIEW_NAME)
public class ProfileView  extends VerticalLayout implements View, ClickListener{

	
	private static final long serialVersionUID = 1L;

	private static final String BTN_UPDATE_ID = "btn-update";
	private static final String BTN_CHANGE_PIC = "btn-change-pic";

	private static final String BTN_CHANGE_PASS_ID = "btn-change-pass";
	private static final String BTN_UPDATE_IMAGE_ID = "btn-update-image";
	
	private final int WIDTH_FULL = 100;
	public static final String VIEW_NAME = "thong-tin-tai-khoan";
	public static final String MAIN_TITLE = "Thông tin tài khoản";
	private static final String SMALL_TITLE = "";
	

	@Autowired
	protected IdentityService identityService;

	@Autowired
	private HtDonViServices htDonViServices;
	@Autowired
	private HtChucVuDao htChucVuDao;
	
	private User userCurrent;
	private Image userPic;

	private final FieldGroup fieldGroup = new FieldGroup();
	private final FieldGroup infoFieldGroup = new FieldGroup();
	private final PropertysetItem infoItem = new PropertysetItem();
	

	private final FieldGroup changePassFieldGroup = new FieldGroup();
	private final PropertysetItem changePassItem = new PropertysetItem();
	

	private HtDonVi donVi;
	private HtChucVu chucVu;
	private Window changePicWindow;
	private Label userPosition;
	private Label userUnit;
	private Label userName;
	
	private String urlNewImage;
	private String mimeType;
	@Autowired
	private HtThamSoService htThamSoService;
	private String uploadPath;
	@Autowired
	private MessageSource messageSource;
	
	@PostConstruct
    void init() {
		uploadPath = "";
		
		HtThamSo uploadPathParam = htThamSoService.getHtThamSoFind(ThamSo.FOLDER_UPLOAD_PATH);
		if(uploadPathParam != null){
			uploadPath = uploadPathParam.getGiaTri();
		}
		
		
		initMainTitle();
		infoFieldGroup.setItemDataSource(infoItem);
		changePassFieldGroup.setItemDataSource(changePassItem);
		String id = CommonUtils.getUserLogedIn();
		userCurrent = identityService.createUserQuery().userId(id).singleResult();
		if(userCurrent != null){
			userDetail(userCurrent);
    		String maDonVi = identityService.getUserInfo(userCurrent.getId(), "HT_DON_VI");
    		if(maDonVi != null){
    			List<HtDonVi> list = htDonViServices.findByCode(maDonVi);
    			if(list != null && !list.isEmpty()){
    				donVi = list.get(0);
    			}
    		}
    		String maChucVu = identityService.getUserInfo(userCurrent.getId(), "HT_CHUC_VU");
    		if(maChucVu != null){
    			List<HtChucVu> list = htChucVuDao.findByCode(maChucVu);
    			if(list != null && !list.isEmpty()){
    				chucVu = list.get(0);
    			}
    		}
    		initPopupChangePic();
			initContent();
		}
	}
	
	@Override
	public void enter(ViewChangeEvent event) {

	}

	private void userDetail(User user) {
		if (user != null) {
			BeanItem<User> item = new BeanItem<User>(user);
			fieldGroup.setItemDataSource(item);
		}

	}
	
	public void initMainTitle(){
		PageBreadcrumbInfo home = new PageBreadcrumbInfo();
    	home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
    	home.setViewName(DashboardView.VIEW_NAME);
    	
    	PageBreadcrumbInfo current = new PageBreadcrumbInfo();
    	current.setTitle(this.MAIN_TITLE);
    	current.setViewName(this.VIEW_NAME);		
    			
    	addComponent(new PageBreadcrumbLayout(home, current));
	}
	
	public void initContent(){
		CssLayout pageWrap = new CssLayout();
		pageWrap.setWidth("100%");
		pageWrap.addStyleName(ExplorerLayout.USER_PAGE_WRAP);
		addComponent(pageWrap);
		
		CssLayout generalInfoWrap = new CssLayout();
		generalInfoWrap.setWidth("30%");
		generalInfoWrap.addStyleName(ExplorerLayout.USER_GENERAL_INFO_WRAP);
		pageWrap.addComponent(generalInfoWrap);
		
		CssLayout userPicWrap = new CssLayout();
		userPicWrap.setWidth("100%");
		userPicWrap.addStyleName(ExplorerLayout.USER_PIC_WRAP);
		generalInfoWrap.addComponent(userPicWrap);
		
		Button btnChangePic = new Button();
		btnChangePic.setIcon(FontAwesome.PENCIL);
		btnChangePic.addStyleName(ExplorerLayout.BTN_CHANGE_PIC);
		btnChangePic.setId(BTN_CHANGE_PIC);
		btnChangePic.setDescription("Đổi ảnh đại diện");
		btnChangePic.addClickListener(this);
		userPicWrap.addComponent(btnChangePic);
		
		String userNameStr = userCurrent.getFirstName()+" "+userCurrent.getLastName();
		userPic = new Image();
		try {
			if(userCurrent.isPictureSet()){
				Picture pic = identityService.getUserPicture(userCurrent.getId());
				
				SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				String filename = "user-pic-" + df.format(new Date());
				userPic.setSource(FileUtils.getStreamResource(pic.getInputStream(), filename));
				userPic.addStyleName(ExplorerLayout.USER_PIC);
				userPicWrap.addComponent(userPic);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		userName = new Label(userNameStr);
		userName.addStyleName(ExplorerLayout.LBL_USER_NAME);
		generalInfoWrap.addComponent(userName);
		
		userPosition = new Label();
		if(chucVu != null){
			userPosition.setValue(chucVu.getTen());
			userPosition.addStyleName(ExplorerLayout.LBL_USER_POSITION);
			generalInfoWrap.addComponent(userPosition);
		}
		userUnit = new Label();
		
		if(donVi != null){
			userUnit.setValue(donVi.getTen());
			userUnit.addStyleName(ExplorerLayout.LBL_USER_UNIT);
			generalInfoWrap.addComponent(userUnit);
		}
		
		CssLayout userDetailWrap = new CssLayout();
		userDetailWrap.setWidth("70%");
		userDetailWrap.addStyleName(ExplorerLayout.USER_DETAIL_INFO_WRAP);
		pageWrap.addComponent(userDetailWrap);
		
		
		
		
    	CssLayout userInfoTitleWrap = new CssLayout();
    	userInfoTitleWrap.addStyleName(ExplorerLayout.LBL_USER_INFO_TTL_WRAP);
    	userDetailWrap.addComponent(userInfoTitleWrap);
    	
    	Label userInfoTtl = new Label("Thông tin tài khoản");
    	userInfoTtl.addStyleName(ExplorerLayout.LBL_USER_INFO_TTL);
    	userInfoTitleWrap.addComponent(userInfoTtl);
    	
    	TabSheet userInfoTab = new TabSheet();
    	userInfoTab.addStyleName(ExplorerLayout.USER_INFO_TAB);
    	userInfoTab.setWidth("100%");
    	userDetailWrap.addComponent(userInfoTab);
    	
    	VerticalLayout userDetailForm = new VerticalLayout();
    	userDetailForm.setSpacing(true);
    	userInfoTab.addTab(userDetailForm, "Thông tin cá nhân");
    	
		TextField txtId = new TextField("Id");
    	txtId.setRequired(true);
    	txtId.setSizeFull();
    	txtId.setNullRepresentation("");
    	
    	TextField txtFirstName = new TextField("Họ đệm");
    	txtFirstName.setRequired(true);
    	txtFirstName.setSizeFull();
    	txtFirstName.setNullRepresentation("");
    	txtFirstName.setRequiredError("Không được để trống");
    	txtFirstName.addValidator(new StringLengthValidator("Số ký tự tối đa là 50", 1, 50, false)); 
    	
    	TextField txtLastName = new TextField("Tên");
    	txtLastName.setRequired(true);
    	txtLastName.setSizeFull();
    	txtLastName.setNullRepresentation("");
    	txtLastName.setRequiredError("Không được để trống");
    	txtLastName.addValidator(new StringLengthValidator("Số ký tự tối đa là 50", 1, 50, false)); 
    	
    	
    	TextField txtEmail = new TextField("Email");
    	txtEmail.setRequired(true);
    	txtEmail.setSizeFull();
    	txtEmail.setNullRepresentation("");
    	txtEmail.setRequiredError("Không được để trống");
    	txtEmail.addValidator(new StringLengthValidator("Số ký tự tối đa là 250", 1, 250, false));
    	txtEmail.addValidator(new RegexpValidator("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", "Email không đúng."));
    	
    	
    	List<Long> trangThaiDv = null;
    	WorkflowManagement<HtDonVi> dvWM = new WorkflowManagement<HtDonVi>(EChucNang.HT_DON_VI.getMa());
    	if(dvWM.getTrangThaiSuDung() != null){
    		trangThaiDv = new ArrayList();
    		trangThaiDv.add(new Long(dvWM.getTrangThaiSuDung().getId()));
    	}
    	List<HtDonVi> htDonViList = htDonViServices.getHtDonViFind(null,null, null, trangThaiDv, -1, -1);
    	BeanItemContainer<HtDonVi> containerDv = new BeanItemContainer<HtDonVi>(HtDonVi.class, htDonViList);
    	ComboBox cbbDonVi = new ComboBox("Đơn vị", containerDv);
    	cbbDonVi.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbDonVi.setRequired(true);
    	cbbDonVi.setRequiredError("Không được để trống");
    	cbbDonVi.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbDonVi.setItemCaptionPropertyId("ten");
    	
    	if(htDonViList.contains(donVi)){
    		donVi = htDonViList.get(htDonViList.indexOf(donVi));
    	}
    	
    	infoItem.addItemProperty("donVi", new ObjectProperty<HtDonVi>(donVi, HtDonVi.class));
    	infoFieldGroup.bind(cbbDonVi, "donVi");
    	
    	List<Long> trangThaiCv = null;
    	WorkflowManagement<HtChucVu> cvWM = new WorkflowManagement<HtChucVu>(EChucNang.HT_CHUC_VU.getMa());
    	if(cvWM.getTrangThaiSuDung() != null){
    		trangThaiCv = new ArrayList();
    		trangThaiCv.add(new Long(cvWM.getTrangThaiSuDung().getId()));
    	}
    	
    	List<HtChucVu> htCvList = htChucVuDao.getHtChucVuFind(null, null, trangThaiCv, -1, -1);
    	BeanItemContainer<HtChucVu> containerCv = new BeanItemContainer<HtChucVu>(HtChucVu.class, htCvList);
    	ComboBox cbbChucVu = new ComboBox("Chức vụ", containerCv);
    	cbbChucVu.setRequired(true);
    	cbbChucVu.setRequiredError("Không được để trống");
    	cbbChucVu.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbChucVu.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbChucVu.setItemCaptionPropertyId("ten");
    	
    	if(htCvList.contains(chucVu)){
    		chucVu = htCvList.get(htCvList.indexOf(chucVu));
    	}
    	
    	infoItem.addItemProperty("chucVu", new ObjectProperty<HtChucVu>(chucVu, HtChucVu.class));
    	infoFieldGroup.bind(cbbChucVu, "chucVu");
    	
    	fieldGroup.bind(txtId, "id");
    	fieldGroup.bind(txtFirstName, "firstName");
    	fieldGroup.bind(txtLastName, "lastName");
    	fieldGroup.bind(txtEmail, "email");
    	
    	fieldGroup.getField("id").setReadOnly(true);
    	
    	userDetailForm.addComponent(txtId);
    	userDetailForm.addComponent(txtFirstName);
    	userDetailForm.addComponent(txtLastName);
    	userDetailForm.addComponent(txtEmail);
    	userDetailForm.addComponent(cbbDonVi);
    	userDetailForm.addComponent(cbbChucVu);
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	Button btnUpdate = new Button("Cập nhật");
    	btnUpdate.setId(BTN_UPDATE_ID);
    	btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnUpdate.addClickListener(this);
		buttons.addComponent(btnUpdate);
		
		userDetailForm.addComponent(buttons);
		
		
		VerticalLayout changePassForm = new VerticalLayout();
		changePassForm.setSpacing(true);
    	userInfoTab.addTab(changePassForm, "Đổi mật khẩu");
    	
    	PasswordField pCurrentPass = new PasswordField("Mật khẩu hiện tại");
    	pCurrentPass.setRequired(true);
    	
    	pCurrentPass.setSizeFull();
    	pCurrentPass.setNullRepresentation("");
    	pCurrentPass.setRequiredError("Không được để trống");
    	pCurrentPass.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					String currentPass = (String) event.getProperty().getValue();
					if(currentPass != null && !currentPass.isEmpty() && StringUtils.getSHA512(currentPass).equals(userCurrent.getPassword())){
						pCurrentPass.setComponentError(null);
					}else{
						pCurrentPass.setComponentError(new UserError("Mật khẩu hiện tại không đúng!"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
    	
    	PasswordField pNewPass = new PasswordField("Mật khẩu mới");
    	pNewPass.setRequired(true);
    	pNewPass.setSizeFull();
    	pNewPass.setNullRepresentation("");
    	pNewPass.setRequiredError("Không được để trống");
    	
    	PasswordField pNewPass2 = new PasswordField("Xác nhận mật khẩu mới");
    	pNewPass2.setRequired(true);
    	pNewPass2.setSizeFull();
    	pNewPass2.setNullRepresentation("");
    	pNewPass2.setRequiredError("Không được để trống");
    	pNewPass2.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					String newPass2 = (String) event.getProperty().getValue();
					if(newPass2 != null && !newPass2.isEmpty()){
						String newPass = pNewPass.getValue();
						if(!newPass2.equals(newPass)){
							pNewPass2.setComponentError(new UserError("Mật khẩu mới không trùng nhau!"));
						}else{
							pNewPass2.setComponentError(null);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

    	changePassItem.addItemProperty("current", new ObjectProperty<String>(""));
    	changePassFieldGroup.bind(pCurrentPass, "current");
    	
    	changePassItem.addItemProperty("newPass", new ObjectProperty<String>(""));
    	changePassFieldGroup.bind(pNewPass, "newPass");
    	
    	changePassItem.addItemProperty("newPass2", new ObjectProperty<String>(""));
    	changePassFieldGroup.bind(pNewPass2, "newPass2");
    	
    	changePassForm.addComponent(pCurrentPass);
    	changePassForm.addComponent(pNewPass);
    	changePassForm.addComponent(pNewPass2);
    	
    	HorizontalLayout changePassbuttons = new HorizontalLayout();
    	changePassbuttons.setSpacing(true);
    	changePassbuttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	Button btnChangePass = new Button("Đổi mật khẩu");
    	btnChangePass.setId(BTN_CHANGE_PASS_ID);
    	btnChangePass.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
    	btnChangePass.addClickListener(this);
		changePassbuttons.addComponent(btnChangePass);
		
		changePassForm.addComponent(changePassbuttons);
		
	}
	
	public void initPopupChangePic(){
		VerticalLayout content = new VerticalLayout();
		content.setSpacing(true);
		content.addStyleName(ExplorerLayout.CHANGE_PIC_POPUP_CONTENT);
		content.setWidth("100%");
		
		CssLayout userImageWrap = new CssLayout();
		userImageWrap.addStyleName(ExplorerLayout.USER_PIC_POPUP_WRAP);
		content.addComponent(userImageWrap);
		
		Image image = new Image();
		userImageWrap.addComponent(image);
		image.addStyleName(ExplorerLayout.USER_PIC_POPUP);
		
		Long fileSize = 1000000L;
		List<String> fileAllows = new ArrayList();
		fileAllows.add("png");
		fileAllows.add("jpg");
		fileAllows.add("jpeg");
		fileAllows.add("gif");
		

		CssLayout btnUploadImageWrap = new CssLayout();
		btnUploadImageWrap.addStyleName(ExplorerLayout.BTN_UPLOAD_PIC_POPUP_WRAP);
		content.addComponent(btnUploadImageWrap);
		
		UploadFileInfo uploadInfo = new UploadFileInfo(fileSize, fileAllows, uploadPath);
		
		Upload upload = new Upload(null, uploadInfo);
		upload.setButtonCaption("Chọn ảnh");
		upload.setImmediate(true);
		upload.setStyleName(ExplorerLayout.BTN_UPLOAD_PIC_POPUP);
		upload.addStartedListener(uploadInfo);
		upload.addFailedListener(uploadInfo);
		upload.addFinishedListener(uploadInfo);
		content.addComponent(upload);
		
		btnUploadImageWrap.addComponent(upload);
		upload.addSucceededListener(new SucceededListener() {
			@Override
			public void uploadSucceeded(SucceededEvent event) {
				try {
					urlNewImage = uploadPath+event.getFilename();
					mimeType = event.getMIMEType();
					InputStream input = new FileInputStream(urlNewImage);
					SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
					String filename = "user-pic-" + df.format(new Date());
					image.setSource(FileUtils.getStreamResource(input, filename));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		
		CssLayout btnUpdateImageWrap = new CssLayout();
		btnUpdateImageWrap.addStyleName(ExplorerLayout.BTN_UPLOAD_PIC_POPUP_WRAP);
		content.addComponent(btnUpdateImageWrap);
		
		Button btnUpdateImage = new Button("Cập nhật ảnh đại diện");
		btnUpdateImage.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnUpdateImage.setId(BTN_UPDATE_IMAGE_ID);
		btnUpdateImage.addClickListener(this);
		btnUpdateImageWrap.addComponent(btnUpdateImage);
		
		changePicWindow = new Window("Cập nhật ảnh đại diện");
		changePicWindow.setWidth(400, Unit.PIXELS);
		changePicWindow.setHeight(400, Unit.PIXELS);
		changePicWindow.center();
		changePicWindow.setModal(true);
		changePicWindow.setContent(content);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BTN_UPDATE_ID.equals(button.getId())){
				if(fieldGroup.isValid() && infoFieldGroup.isValid()){
					fieldGroup.commit();
					infoFieldGroup.commit();
					
					HtDonVi donVi = (HtDonVi) infoItem.getItemProperty("donVi").getValue();
					HtChucVu chucVu = (HtChucVu) infoItem.getItemProperty("chucVu").getValue();
			
					
					identityService.saveUser(userCurrent);
					identityService.setUserInfo(userCurrent.getId(), "HT_DON_VI", donVi.getMa());
					identityService.setUserInfo(userCurrent.getId(), "HT_CHUC_VU", chucVu.getMa());
					
					userPosition.setValue(chucVu.getTen());
					userName.setValue(userCurrent.getFirstName() +" "+userCurrent.getLastName());
					userUnit.setValue(donVi.getTen());
					
					Notification notf = new Notification("Thông báo", "Cập nhật thông tin thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					
				}
				
				
			}else if(BTN_CHANGE_PASS_ID.equals(button.getId())){
				if(changePassFieldGroup.isValid()){
					changePassFieldGroup.commit();
					String newPass = (String) changePassItem.getItemProperty("newPass").getValue();
					userCurrent.setPassword(StringUtils.getSHA512(newPass));
					identityService.saveUser(userCurrent);
					
					Notification notf = new Notification("Thông báo", "Đổi mật khẩu thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					
					changePassFieldGroup.clear();
				}
			}else if(BTN_CHANGE_PIC.equals(button.getId())){
				UI.getCurrent().addWindow(changePicWindow);
			}else if(BTN_UPDATE_IMAGE_ID.equals(button.getId())){
				try {
					if(urlNewImage != null){
						File file = new File(urlNewImage);
						InputStream input = new FileInputStream(file);
						Picture pic = new Picture(IOUtils.toByteArray(input), mimeType);
						identityService.setUserPicture(userCurrent.getId(), pic);
						
						SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
						String filename = "user-pic-" + df.format(new Date());
						userPic.setSource(FileUtils.getStreamResource(pic.getInputStream(), filename));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				changePicWindow.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra", Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
		}
		
	}

}

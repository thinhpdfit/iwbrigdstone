package com.eform.ui.view.report.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.eform.common.Constants;
import com.eform.common.ThamSo;
import com.eform.common.utils.CommonUtils;
import com.eform.model.entities.HtThamSo;
import com.eform.service.HtThamSoService;
import com.jcraft.jsch.MAC;


@Controller
@RequestMapping("/report")
public class ProcessReportController {
	@Autowired
	private HistoryService historyService;
	@Autowired
	protected transient FormService formService;
	@Autowired
	private HtThamSoService htThamSoService;
	@Autowired
	private RepositoryService repositoryService;
	
	@RequestMapping(value = "/process", method=RequestMethod.GET)
	public String amcharts(@RequestParam(value = "p", required = false) String processId, 
			@RequestParam(value = "f", required = false) Long fromDate,
			@RequestParam(value = "t", required = false) Long toDate,
			@RequestParam(value = "d", required = false) Long dinhKy,
			HttpServletRequest request, 
			Model model){
		try {
			HtThamSo dayNumberStr = htThamSoService.getHtThamSoFind(ThamSo.DAY_NUMBER_REPORT);
			int dateNumber = 15;//mac dinh
			if(dayNumberStr != null && dayNumberStr.getGiaTri() != null && dayNumberStr.getGiaTri().trim().matches("[0-9]+")){
				dateNumber= Integer.parseInt(dayNumberStr.getGiaTri().trim());
			}
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat dfp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			
			Date from = null;
			Date to = null;
			if(fromDate != null && toDate != null){
				from = new Date(fromDate);
				to =  new Date(toDate);

				dateNumber = (int) ((toDate - fromDate) / (1000 * 60 * 60 * 24)) + 1;
				System.out.println("from "+dfp.format(from));
				System.out.println("to "+dfp.format(to));
				
			}else{
				from = new Date();
				to =  new Date();
			}
			
			
			
			Date dateBTmp = null;
			Date dateATmp = null;
			
			if(dinhKy == null){
				dinhKy = new Long(CommonUtils.EDinhKyTongHop.NGAY.getCode());
			}
			
			if(dinhKy.intValue() == CommonUtils.EDinhKyTongHop.NGAY.getCode()){
				dateBTmp = dfp.parse(df.format(to) + " 23:59:59");
				dateATmp = dfp.parse(df.format(to) + " 00:00:00");
				
			}else if(dinhKy.intValue() == CommonUtils.EDinhKyTongHop.THANG.getCode()){
				dateBTmp = dfp.parse(df.format(to) + " 23:59:59");
				dateATmp = dfp.parse(df.format(to) + " 00:00:00");
				
				Calendar c = Calendar.getInstance();
				c.setTime(dateBTmp);
				c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
				dateBTmp = c.getTime();
				
				
				
				Calendar c1 = Calendar.getInstance();
				c1.setTime(dateATmp);
				c1.set(Calendar.DAY_OF_MONTH, c1.getActualMinimum(Calendar.DAY_OF_MONTH));
				dateATmp = c1.getTime();
				
				Calendar c2 = Calendar.getInstance();
				c2.setTime(from);
				
				int years = c.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
				
				dateNumber = c.get(Calendar.MONTH)+(years *12) - c2.get(Calendar.MONTH) + 1;
				
			}else if(dinhKy.intValue() == CommonUtils.EDinhKyTongHop.NAM.getCode()){
				dateBTmp = dfp.parse(df.format(to) + " 23:59:59");
				dateATmp = dfp.parse(df.format(to) + " 00:00:00");
				
				Calendar c = Calendar.getInstance();
				c.setTime(dateBTmp);
				c.set(Calendar.DAY_OF_MONTH, 31);
				c.set(Calendar.MONTH, 11);
				dateBTmp = c.getTime();
				
				Calendar c1 = Calendar.getInstance();
				c1.setTime(dateATmp);
				c1.set(Calendar.DAY_OF_YEAR, 1);
				dateATmp = c1.getTime();
				
				Calendar c2 = Calendar.getInstance();
				c2.setTime(from);
				dateNumber = c.get(Calendar.YEAR) - c2.get(Calendar.YEAR) + 1;
			}else{
				dateBTmp = dfp.parse(df.format(to) + " 23:59:59");
				dateATmp = dfp.parse(df.format(to) + " 00:00:00");
			}
			

			System.out.println("dateBTmp"+dfp.format(dateBTmp));
			System.out.println("dateATmp"+dfp.format(dateATmp));
		
			
			JSONArray jsonArray = new JSONArray();
			
			Calendar calB = Calendar.getInstance();
			calB.setTime(dateBTmp);
			
			Calendar calA = Calendar.getInstance();
			calA.setTime(dateATmp);
			
			Calendar calCurrent = Calendar.getInstance();
			calCurrent.setTime(to);
			
			
			if(dinhKy.intValue() == CommonUtils.EDinhKyTongHop.NGAY.getCode()){
				calB.add(Calendar.DATE, - dateNumber);
				calA.add(Calendar.DATE, -dateNumber);
				calCurrent.add(Calendar.DATE, -dateNumber);
			}else if(dinhKy.intValue() == CommonUtils.EDinhKyTongHop.THANG.getCode()){
				calB.add(Calendar.MONTH, - dateNumber);
				calA.add(Calendar.MONTH, -dateNumber);
				calCurrent.add(Calendar.MONTH, -dateNumber);
			}else if(dinhKy.intValue() == CommonUtils.EDinhKyTongHop.NAM.getCode()){
				calB.add(Calendar.YEAR, - dateNumber);
				calA.add(Calendar.YEAR, -dateNumber);
				calCurrent.add(Calendar.YEAR, -dateNumber);
			}else{
				calB.add(Calendar.DATE, - dateNumber);
				calA.add(Calendar.DATE, -dateNumber);
				calCurrent.add(Calendar.DATE, -dateNumber);
			}

			SimpleDateFormat dfY = new SimpleDateFormat("YYYY");
			SimpleDateFormat dfMY = new SimpleDateFormat("MM/YYYY");
			SimpleDateFormat dfDM = new SimpleDateFormat("dd/MM");
			SimpleDateFormat dfD = new SimpleDateFormat("dd");
			SimpleDateFormat dfM = new SimpleDateFormat("MM");
			for(int i = 1; i<= dateNumber; i++){
				
				
				Date dateCurrent = null;
				Date dateB = null;
				Date dateA = null;
				String dateStr = "";
				
				if(dinhKy.intValue() == CommonUtils.EDinhKyTongHop.NGAY.getCode()){
					calCurrent.add(Calendar.DATE, 1);
					dateCurrent = calCurrent.getTime();
					
					calB.add(Calendar.DATE, 1);
					dateB = calB.getTime();
					
					calA.add(Calendar.DATE, 1);
					dateA = calA.getTime();
					
					
					if(i == 1 || i == dateNumber){
						dateStr = dfDM.format(dateCurrent);
					}else{
						dateStr = dfD.format(dateCurrent);
					}
				}else if(dinhKy.intValue() == CommonUtils.EDinhKyTongHop.THANG.getCode()){
					calCurrent.add(Calendar.MONTH, 1);
					dateCurrent = calCurrent.getTime();
					
					calB.add(Calendar.MONTH, 1);
					dateB = calB.getTime();
					
					calA.add(Calendar.MONTH, 1);
					dateA = calA.getTime();
					dateStr = dfMY.format(dateCurrent);
					if(i == 1 || i == dateNumber){
						dateStr = dfMY.format(dateCurrent);
					}else{
						dateStr = dfM.format(dateCurrent);
					}
				}else if(dinhKy.intValue() == CommonUtils.EDinhKyTongHop.NAM.getCode()){
					calCurrent.add(Calendar.YEAR, 1);
					dateCurrent = calCurrent.getTime();
					
					calB.add(Calendar.YEAR, 1);
					dateB = calB.getTime();
					
					calA.add(Calendar.YEAR, 1);
					dateA = calA.getTime();
					dateStr = dfY.format(dateCurrent);
				}else{
					calCurrent.add(Calendar.DATE, 1);
					dateCurrent = calCurrent.getTime();
					
					calB.add(Calendar.DATE, 1);
					dateB = calB.getTime();
					
					calA.add(Calendar.DATE, 1);
					dateA = calA.getTime();
					dateStr = dfDM.format(dateCurrent);
				}
				
				Long total = 0L;
				Long finished = 0L;
				Long inProcess = 0L;
				if(processId != null && !processId.isEmpty()){
					total = historyService.createHistoricProcessInstanceQuery().processDefinitionId(processId).startedBefore(dateB).startedAfter(dateA).count();
					finished = historyService.createHistoricProcessInstanceQuery().processDefinitionId(processId).finishedBefore(dateB).finishedAfter(dateA).count();
					inProcess = historyService.createHistoricProcessInstanceQuery().processDefinitionId(processId).startedBefore(dateB).startedAfter(dateA).unfinished().count();
				}else{
					total = historyService.createHistoricProcessInstanceQuery().startedBefore(dateB).startedAfter(dateA).count();
					finished = historyService.createHistoricProcessInstanceQuery().finishedBefore(dateB).finishedAfter(dateA).count();
					inProcess = historyService.createHistoricProcessInstanceQuery().startedBefore(dateB).startedAfter(dateA).unfinished().count();
				}
				
				
				
				JSONObject json = new JSONObject();
				
				json.put("date", dateStr);
				

				json.put("total", total);
				json.put("finished", finished);
				json.put("inprocess", inProcess);
				if(i == dateNumber){
					json.put("alpha", 0.4);
					json.put("bulletClass", "lastBullet");
				}
				jsonArray.put(json);
			}
			
			model.addAttribute("jsonData", jsonArray.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "processreport";
	}
	
	@RequestMapping(value = "/process-gc", method=RequestMethod.GET)
	public String index(HttpServletRequest request, Model model){
		HtThamSo dayNumberStr = htThamSoService.getHtThamSoFind(ThamSo.DAY_NUMBER_REPORT);
		int dateNumber = 15;//mac dinh
		if(dayNumberStr != null && dayNumberStr.getGiaTri() != null && dayNumberStr.getGiaTri().trim().matches("[0-9]+")){
			dateNumber= Integer.parseInt(dayNumberStr.getGiaTri().trim());
		}
		JSONArray jsonArray = new JSONArray();
		Calendar calB = Calendar.getInstance();
		calB.add(Calendar.DATE, -dateNumber);
		Calendar calA = Calendar.getInstance();
		calA.add(Calendar.DATE, -dateNumber);
		Calendar calCurrent = Calendar.getInstance();
		calCurrent.add(Calendar.DATE, -(dateNumber+1));
		JSONArray titleArray = new JSONArray();
		titleArray.put("Ngày");
		titleArray.put("Tổng số");
		titleArray.put("Đã hoàn thành");
		titleArray.put("Đang thực hiện");
		jsonArray.put(titleArray);
		SimpleDateFormat df1 = new SimpleDateFormat(Constants.DATE_FORMAT_DAY_MONTH);
		for(int i = 0; i< dateNumber; i++){
			calCurrent.add(Calendar.DATE, 1);
			Date dateCurrent = calCurrent.getTime();
			
			calB.add(Calendar.DATE, 1);
			Date dateB = calB.getTime();
			
			calA.add(Calendar.DATE, -1);
			Date dateA = calA.getTime();
			Long total = historyService.createHistoricProcessInstanceQuery().startedBefore(dateB).count();
			Long finished = historyService.createHistoricProcessInstanceQuery().finishedBefore(dateB).finishedAfter(dateA).count();
			Long inProcess = total - finished;
			
			JSONArray dataArray = new JSONArray();
			
			dataArray.put(df1.format(dateCurrent));
			

			dataArray.put(total);
			dataArray.put(finished);
			dataArray.put(inProcess);
			
			jsonArray.put(dataArray);
		}
		
		model.addAttribute("jsonData", jsonArray.toString());
		return "processreport";
	}
}

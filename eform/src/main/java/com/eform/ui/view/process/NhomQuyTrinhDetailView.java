package com.eform.ui.view.process;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.NhomQuyTrinh;
import com.eform.service.NhomQuyTrinhService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.colorpicker.Color;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = NhomQuyTrinhDetailView.VIEW_NAME)
public class NhomQuyTrinhDetailView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "chi-tiet-nhom-quy-trinh";
	private static final String MAIN_TITLE = "Chi tiết ứng dụng";
	private static final String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_APPROVE_ID = "btn-approve";
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_ADD_ID = "btn-edit";
	private static final String BUTTON_DEL_ID = "btn-del";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_REFUSE_ID = "btn-refuse";
	private NhomQuyTrinh nhomQuyTrinhCurrent;
	private WorkflowManagement<NhomQuyTrinh> nhomQuyTrinhWM;
	Workflow<NhomQuyTrinh> nhomQuyTrinhWf;;
	private String action;
	
	private final FieldGroup fieldGroup = new FieldGroup();
	private CssLayout formContainer;
	private Button btnUpdate;
	private Button btnBack;
	

	@Autowired
	private NhomQuyTrinhService nhomQuyTrinhService;
	@Autowired
	private RepositoryService repositoryService;
	
	private Table processDefTable;
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;
    @PostConstruct
    void init() {
    	messageSourceAccessor= new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());
    	nhomQuyTrinhWM = new WorkflowManagement<NhomQuyTrinh>(EChucNang.E_FORM_NHOM_QUY_TRINH.getMa());
    	
    	initMainTitle();
    }

    @Override
    public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				if(Constants.ACTION_ADD.equals(action)){
					nhomQuyTrinhCurrent = new NhomQuyTrinh();
					nhomQuyTrinhWf = nhomQuyTrinhWM.getWorkflow(nhomQuyTrinhCurrent);
					nhomQuyTrinhDetail(nhomQuyTrinhCurrent);

					initDetailForm();
				}else{
					String ma = m.group(3);
					List<NhomQuyTrinh> listLdm = nhomQuyTrinhService.findByCode(ma);
					if(listLdm != null && !listLdm.isEmpty()){
						nhomQuyTrinhCurrent = listLdm.get(0);
						nhomQuyTrinhWf = nhomQuyTrinhWM.getWorkflow(nhomQuyTrinhCurrent);
						nhomQuyTrinhDetail(nhomQuyTrinhCurrent);

						initDetailForm();
					}
					
				}
				
			}
		}

    }
    
    public void initDetailForm(){
    	
    	formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	FormLayout form = new FormLayout();
    	form.setMargin(true);
    	
    	TextField txtMa = new TextField("Mã ứng dụng");
    	txtMa.focus();
    	txtMa.setRequired(true);
    	txtMa.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void textChange(TextChangeEvent event) {

				TextField txt = (TextField) fieldGroup.getField("ma");
				System.out.println(event.getText());
				if(!checkTrungMa(event.getText(), action)){
					txt.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
				}else{
					txt.setComponentError(null);
				}
			}
    	});

    	txtMa.setTextChangeEventMode(TextChangeEventMode.LAZY);
    	
    	txtMa.setSizeFull();
    	txtMa.setNullRepresentation("");
    	txtMa.setRequiredError("Không được để trống");
    	txtMa.addValidator(new StringLengthValidator("Số ký tự tối đa là 20", 1, 20, false));   
    	txtMa.addValidator(new RegexpValidator("[0-9a-zA-Z_]+", "Chỉ chấp nhận các ký tự 0-9 a-z A-Z _"));
    	
    	TextField txtTen = new TextField("Tên ứng dụng");
    	txtTen.setRequired(true);
    	txtTen.setSizeFull();
    	txtTen.setNullRepresentation("");
    	txtTen.setRequiredError("Không được để trống");
    	txtTen.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", 1, 2000, false)); 
    	
    	fieldGroup.bind(txtMa, "ma");
    	fieldGroup.bind(txtTen, "ten");
    	
    	txtMa.setReadOnly("xem".equals(action));
    	txtTen.setReadOnly("xem".equals(action));
    	
    	form.addComponent(txtMa);
    	form.addComponent(txtTen);
    	
    	if(!Constants.ACTION_ADD.equals(action)){
    		initProDefTable();
    		form.addComponent(processDefTable);
    	}
    	
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	if(!Constants.ACTION_VIEW.equals(action)){
    		btnUpdate = new Button();
    		btnUpdate.addClickListener(this);
    		buttons.addComponent(btnUpdate);
    		if(Constants.ACTION_EDIT.equals(action)){
        		btnUpdate.setCaption("Cập nhật");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_EDIT_ID);
        	}else if(Constants.ACTION_ADD.equals(action)){
        		btnUpdate.setId(BUTTON_ADD_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption("Thêm mới");
        	}else if(Constants.ACTION_DEL.equals(action)){
        		btnUpdate.setId(BUTTON_DEL_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
        		btnUpdate.setCaption("Xóa");
        	}else if(Constants.ACTION_APPROVE.equals(action)){
        		btnUpdate.setId(BUTTON_APPROVE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption(nhomQuyTrinhWf.getPheDuyetTen());
        	}else if(Constants.ACTION_REFUSE.equals(action)){
        		btnUpdate.setCaption(nhomQuyTrinhWf.getTuChoiTen());
        		btnUpdate.setId(BUTTON_REFUSE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_WARNING);
        	}
    	}
    	
    	btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
    	form.addComponent(buttons);

    	formContainer.addComponent(form);
    	addComponent(formContainer);
    }
    
    public void initProDefTable(){
    	List<Model> modelList = repositoryService.createModelQuery().modelCategory(nhomQuyTrinhCurrent.getMa()).list();
    	BeanItemContainer<Model> modelContainer = new BeanItemContainer<Model>(Model.class, modelList);

    	processDefTable = new Table("Danh sách quy trình");
    	processDefTable.addStyleName(ExplorerLayout.DATA_TABLE);
    	processDefTable.setSizeFull();
    	processDefTable.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	processDefTable.setContainerDataSource(modelContainer);
    	
    	
    	processDefTable.addGeneratedColumn("name", new ColumnGenerator(){
			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				Model current = (Model) itemId;
				if(current != null && current.getName() != null && !current.getName().isEmpty()){
					return current.getName();
				}
				return "#Không tên";
			}
		});
    	
    	processDefTable.addGeneratedColumn("desc", new ColumnGenerator(){
			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				ObjectMapper objectMapper = new ObjectMapper();
				String descStr = "#Không có mô tả";
				Model current = (Model) itemId;
				if(current != null){
					try {
						JsonNode modelObjectNode = objectMapper.readTree(current.getMetaInfo());
						if(modelObjectNode.get("description")!= null){
							String descTmp = modelObjectNode.get("description").textValue();
							if(descTmp != null && !descTmp.isEmpty()){
								descStr = descTmp;
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				return descStr;
			}
		});
    	
    	processDefTable.setColumnHeader("name", "Tên quy trình");
    	processDefTable.setColumnHeader("desc", "Mô tả");
    	processDefTable.setVisibleColumns("name", "desc");
    	
    }
    
    private void nhomQuyTrinhDetail(NhomQuyTrinh nhomQuyTrinh) {
        if (nhomQuyTrinh == null) {
        	nhomQuyTrinh = new NhomQuyTrinh();
        }
        BeanItem<NhomQuyTrinh> item = new BeanItem<NhomQuyTrinh>(nhomQuyTrinh);
        fieldGroup.setItemDataSource(item);
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSourceAccessor.getMessage(Messages.UI_VIEW_HOME));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(NhomQuyTrinhView.MAIN_TITLE);
		bcItem.setViewName(NhomQuyTrinhView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}

	public NhomQuyTrinh getNhomQuyTrinhCurrent() {
		return nhomQuyTrinhCurrent;
	}

	public void setNhomQuyTrinhCurrent(NhomQuyTrinh nhomQuyTrinhCurrent) {
		this.nhomQuyTrinhCurrent = nhomQuyTrinhCurrent;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BUTTON_CANCEL_ID.equals(button.getId())){
				UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhView.VIEW_NAME);
			}else{
				if(Constants.ACTION_EDIT.equals(action)){
					Notification notf = new Notification("Thông báo", "Cập nhật thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						NhomQuyTrinh current = ((BeanItem<NhomQuyTrinh>) fieldGroup.getItemDataSource()).getBean();
						notf.show(Page.getCurrent());
						nhomQuyTrinhService.save(current);
						UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhView.VIEW_NAME);
						
					}
				}else if(Constants.ACTION_ADD.equals(action)){
					Notification notf = new Notification("Thông báo", "Thêm mới thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						NhomQuyTrinh current = ((BeanItem<NhomQuyTrinh>) fieldGroup.getItemDataSource()).getBean();
						notf.show(Page.getCurrent());
						nhomQuyTrinhService.save(current);
						UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhView.VIEW_NAME);
						
					}
				}else if(Constants.ACTION_DEL.equals(action)){
					Notification notf = new Notification("Thông báo", "Xóa thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						notf.show(Page.getCurrent());
						nhomQuyTrinhService.delete(((BeanItem<NhomQuyTrinh>) fieldGroup.getItemDataSource()).getBean());
						UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhView.VIEW_NAME);
					}
				}else if(Constants.ACTION_APPROVE.equals(action)){
					Notification notf = new Notification("Thông báo", nhomQuyTrinhWf.getPheDuyetTen()+" thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						NhomQuyTrinh current = nhomQuyTrinhWf.pheDuyet();
						notf.show(Page.getCurrent());
						nhomQuyTrinhService.save(current);
						UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhView.VIEW_NAME);
					}
				}else if(Constants.ACTION_REFUSE.equals(action)){
					Notification notf = new Notification("Thông báo", nhomQuyTrinhWf.getTuChoiTen()+" thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						NhomQuyTrinh current = nhomQuyTrinhWf.tuChoi();
						notf.show(Page.getCurrent());
						nhomQuyTrinhService.save(current);
						UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhView.VIEW_NAME);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        
	}
	
	public boolean checkTrungMa(String ma, String act){
		try {
			if(ma == null || ma.isEmpty()){
				return false;
			}
			if(Constants.ACTION_ADD.equals(act)){
				List<NhomQuyTrinh> list = nhomQuyTrinhService.findByCode(ma);
				if(list == null || list.isEmpty()){
					return true;
				}
			}else if(!Constants.ACTION_DEL.equals(act)){
				List<NhomQuyTrinh> list = nhomQuyTrinhService.findByCode(ma);
				if(list == null || list.size() <= 1){
					return true;
				}
			}else if(Constants.ACTION_DEL.equals(act)){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
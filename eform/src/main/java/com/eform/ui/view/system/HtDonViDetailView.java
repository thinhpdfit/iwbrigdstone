package com.eform.ui.view.system;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.HtChucVuDao;
import com.eform.dao.HtDonViDao;
import com.eform.model.entities.HtChucVu;
import com.eform.model.entities.HtDonVi;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = HtDonViDetailView.VIEW_NAME)
public class HtDonViDetailView extends VerticalLayout implements View, ClickListener {

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "chi-tiet-don-vi";
	private static final String MAIN_TITLE = "Chi tiết đơn vị";
	private static final String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_APPROVE_ID = "btn-approve";
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_ADD_ID = "btn-edit";
	private static final String BUTTON_DEL_ID = "btn-del";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_REFUSE_ID = "btn-refuse";
	
	private HtDonVi htDonViCurrent;
	private CssLayout mainTitleWrap;
	private WorkflowManagement<HtDonVi> htDonViWM;
	Workflow<HtDonVi> htDonViWf;;
	private String action;
	
	private final FieldGroup fieldGroup = new FieldGroup();
	private CssLayout formContainer;
	private Button btnUpdate;
	private Button btnBack;
	private HtDonVi parent;
	

	@Autowired
	private HtDonViDao htDonViDao;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private HtChucVuDao htChucVuDao;

    @PostConstruct
    void init() {
    	htDonViWM = new WorkflowManagement<HtDonVi>(null);
    	initMainTitle();
    }

    @Override
    public void enter(ViewChangeEvent event) {
    	UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				if(Constants.ACTION_ADD.equals(action)){
					htDonViCurrent = new HtDonVi();
					htDonViWf = htDonViWM.getWorkflow(htDonViCurrent);
					String ma = m.group(3);
					List<HtDonVi> list = htDonViDao.findByCode(ma);
					if(list != null && !list.isEmpty()){
						parent = list.get(0);
					}
					htDonViCurrent.setHtDonVi(parent);
					htDonViDetail(htDonViCurrent);
					
					initDetailForm();
				}else{
					String ma = m.group(3);
					List<HtDonVi> list = htDonViDao.findByCode(ma);
					if(list != null && !list.isEmpty()){
						htDonViCurrent = list.get(0);
						htDonViWf = htDonViWM.getWorkflow(htDonViCurrent);
						htDonViDetail(htDonViCurrent);
						parent = htDonViCurrent.getHtDonVi();
						initDetailForm();
					}
					
				}
				
			}
		}
    }
    
    private void htDonViDetail(HtDonVi htDonVi) {
        if (htDonVi == null) {
        	htDonVi = new HtDonVi();
        }
        BeanItem<HtDonVi> item = new BeanItem<HtDonVi>(htDonVi);
        fieldGroup.setItemDataSource(item);
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(HtDonViView.MAIN_TITLE);
		bcItem.setViewName(HtDonViView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
    
    public void initDetailForm(){
    	
    	formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	FormLayout form = new FormLayout();
    	form.setMargin(true);
    	
    	List<HtDonVi> htDonViList = htDonViDao.getHtDonViFind(null,null, null, null, -1, -1);
    	List<HtDonVi> childList = new ArrayList<HtDonVi>();
    	if(htDonViList != null && !htDonViList.isEmpty()){
    		List<HtDonVi> list = new ArrayList<HtDonVi>();
        	list.add(htDonViCurrent);
        	while(!list.isEmpty()){
        		HtDonVi current = list.remove(0);
        		for(HtDonVi q : htDonViList){
        			if(q.getHtDonVi() != null && q.getHtDonVi().equals(current)){
        				list.add(q);
        				childList.add(q);
        			}
        		}
        	}
    	}
    	childList.add(htDonViCurrent);
    	
    	htDonViList.removeAll(childList);
    	
    	BeanItemContainer<HtDonVi> dvChaContainer = new BeanItemContainer<HtDonVi>(HtDonVi.class, htDonViList);
    	ComboBox cbbParent = new ComboBox("Đơn vị cha", dvChaContainer);
    	cbbParent.setNullSelectionAllowed(true);
    	cbbParent.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbParent.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbParent.setItemCaptionPropertyId("ten");
//    	cbbParent.setPageLength(0);
    	
    	
    	TextField txtMa = new TextField("Mã đơn vị");
    	txtMa.focus();
    	txtMa.setRequired(true);
    	txtMa.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void textChange(TextChangeEvent event) {
				TextField txt = (TextField) fieldGroup.getField("ma");
				System.out.println(event.getText());
				if(!checkTrungMa(event.getText(), action)){
					txt.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
				}else{
					txt.setComponentError(null);
				}
			}
    	});

    	txtMa.setTextChangeEventMode(TextChangeEventMode.LAZY);
    	
    	txtMa.setSizeFull();
    	txtMa.setNullRepresentation("");
    	txtMa.setRequiredError("Không được để trống");
    	txtMa.addValidator(new StringLengthValidator("Số ký tự tối đa là 20", 1, 20, false));   
    	txtMa.addValidator(new RegexpValidator("[0-9a-zA-Z_]+", "Chỉ chấp nhận các ký tự 0-9 a-z A-Z _"));
    	
    	TextField txtTen = new TextField("Tên đơn vị");
    	txtTen.setRequired(true);
    	txtTen.setSizeFull();
    	txtTen.setNullRepresentation("");
    	txtTen.setRequiredError("Không được để trống");
    	txtTen.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", 1, 2000, false));
    	
    	List<User> userList = identityService.createUserQuery().list();
    	
    	ComboBox cbbUser = new ComboBox("Người đại diện");
    	if(userList != null && !userList.isEmpty()){
    		for(User u : userList){
    			cbbUser.addItem(u.getId());
    			cbbUser.setItemCaption(u.getId(), u.getFirstName() + " " + u.getLastName());
    		}
    	}
    	
    	cbbUser.setNullSelectionAllowed(true);
    	cbbUser.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	
    	
    	List<Long> trangThaiCv = null;
    	WorkflowManagement<HtChucVu> cvWM = new WorkflowManagement<HtChucVu>(EChucNang.HT_CHUC_VU.getMa());
    	if(cvWM.getTrangThaiSuDung() != null){
    		trangThaiCv = new ArrayList();
    		trangThaiCv.add(new Long(cvWM.getTrangThaiSuDung().getId()));
    	}
    	
    	List<HtChucVu> htCvList = htChucVuDao.getHtChucVuFind(null, null, trangThaiCv, -1, -1);
    	
    	ComboBox cbbChucVu = new ComboBox("Chức vụ cao nhất");
    	if(htCvList != null && !htCvList.isEmpty()){
    		for(HtChucVu cv : htCvList){
    			cbbChucVu.addItem(cv);
    			cbbChucVu.setItemCaption(cv, cv.getTen());
    		}
    	}
    	
    	cbbChucVu.setNullSelectionAllowed(true);
    	cbbChucVu.setWidth(WIDTH_FULL, Unit.PERCENTAGE);

    	fieldGroup.bind(cbbParent, "htDonVi");
    	fieldGroup.bind(txtMa, "ma");
    	fieldGroup.bind(txtTen, "ten");
    	fieldGroup.bind(cbbUser, "nguoiDaiDien");
    	fieldGroup.bind(cbbChucVu, "htChuVuCaoNhat");
    	
    	cbbParent.setReadOnly("xem".equals(action));
    	txtMa.setReadOnly("xem".equals(action));
    	txtTen.setReadOnly("xem".equals(action));
    	cbbUser.setReadOnly("xem".equals(action));
    	cbbChucVu.setReadOnly("xem".equals(action));

    	form.addComponent(cbbParent);
    	form.addComponent(txtMa);
    	form.addComponent(txtTen);
    	form.addComponent(cbbUser);
    	form.addComponent(cbbChucVu);
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	if(!Constants.ACTION_VIEW.equals(action)){
    		btnUpdate = new Button();
    		btnUpdate.addClickListener(this);
    		buttons.addComponent(btnUpdate);
    		if(Constants.ACTION_EDIT.equals(action)){
        		btnUpdate.setCaption("Cập nhật");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_EDIT_ID);
        	}else if(Constants.ACTION_ADD.equals(action)){
        		btnUpdate.setId(BUTTON_ADD_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption("Thêm mới");
        	}else if(Constants.ACTION_DEL.equals(action)){
        		btnUpdate.setId(BUTTON_DEL_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
        		btnUpdate.setCaption("Xóa");
        	}else if(Constants.ACTION_APPROVE.equals(action)){
        		btnUpdate.setId(BUTTON_APPROVE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption(htDonViWf.getPheDuyetTen());
        	}else if(Constants.ACTION_REFUSE.equals(action)){
        		btnUpdate.setCaption(htDonViWf.getTuChoiTen());
        		btnUpdate.setId(BUTTON_REFUSE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_WARNING);
        	}
    	}
    	
    	btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
    	form.addComponent(buttons);

    	formContainer.addComponent(form);
    	addComponent(formContainer);
    }

public boolean checkTrungMa(String ma, String act){
	try {
		if(ma == null || ma.isEmpty()){
			return false;
		}
		if(Constants.ACTION_ADD.equals(act)){
			List<HtDonVi> list = htDonViDao.findByCode(ma);
			if(list == null || list.isEmpty()){
				return true;
			}
		}else if(!Constants.ACTION_DEL.equals(act)){
			List<HtDonVi> list = htDonViDao.findByCode(ma);
			if(list == null || list.size() <= 1){
				return true;
			}
		}else if(Constants.ACTION_DEL.equals(act)){
			return true;
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return false;
}

@SuppressWarnings("unchecked")
@Override
public void buttonClick(ClickEvent event) {
	try {
		Button button = event.getButton();
		if(BUTTON_CANCEL_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(HtDonViView.VIEW_NAME);
		}else{
			if(Constants.ACTION_EDIT.equals(action)){
				Notification notf = new Notification("Thông báo", "Cập nhật thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					HtDonVi current = ((BeanItem<HtDonVi>) fieldGroup.getItemDataSource()).getBean();
					notf.show(Page.getCurrent());
					htDonViDao.save(current);
					UI.getCurrent().getNavigator().navigateTo(HtDonViView.VIEW_NAME);
					
				}
			}else if(Constants.ACTION_ADD.equals(action)){
				Notification notf = new Notification("Thông báo", "Thêm mới thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					HtDonVi current = ((BeanItem<HtDonVi>) fieldGroup.getItemDataSource()).getBean();
					notf.show(Page.getCurrent());
					htDonViDao.save(current);
					UI.getCurrent().getNavigator().navigateTo(HtDonViView.VIEW_NAME);
					
				}
			}else if(Constants.ACTION_DEL.equals(action)){
				Notification notf = new Notification("Thông báo", "Xóa thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					notf.show(Page.getCurrent());
					htDonViDao.delete(((BeanItem<HtDonVi>) fieldGroup.getItemDataSource()).getBean());
					UI.getCurrent().getNavigator().navigateTo(HtDonViView.VIEW_NAME);
				}
			}else if(Constants.ACTION_APPROVE.equals(action)){
				Notification notf = new Notification("Thông báo", htDonViWf.getPheDuyetTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					HtDonVi current = htDonViWf.pheDuyet();
					notf.show(Page.getCurrent());
					htDonViDao.save(current);
					UI.getCurrent().getNavigator().navigateTo(HtDonViView.VIEW_NAME);
				}
			}else if(Constants.ACTION_REFUSE.equals(action)){
				Notification notf = new Notification("Thông báo", htDonViWf.getTuChoiTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					HtDonVi current = htDonViWf.tuChoi();
					notf.show(Page.getCurrent());
					htDonViDao.save(current);
					UI.getCurrent().getNavigator().navigateTo(HtDonViView.VIEW_NAME);
				}
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
    
}
}
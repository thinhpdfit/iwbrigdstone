package com.eform.ui.view.process;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.task.TaskDefinition;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.ThamSo;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.SolrUtils;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.CauHinhHienThi;
import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HoSo;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.HtQuyen;
import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.ProcinstInfo;
import com.eform.service.BieuMauService;
import com.eform.service.CauHinhHienThiService;
import com.eform.service.DeploymentHtDonViService;
import com.eform.service.HoSoService;
import com.eform.service.HtDonViServices;
import com.eform.service.HtThamSoService;
import com.eform.service.ProcinstInfoService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.ProcessDiagramViewer;
import com.eform.ui.form.FormGenerate;
import com.eform.ui.view.BaseView;
import com.eform.ui.view.dashboard.DashboardView;
import com.eform.ui.view.report.type.FilterObject;
import com.eform.ui.view.task.TaskDetailView;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SpringView(name = ProcessDefinitionDetailView.VIEW_NAME)
public class ProcessDefinitionDetailView extends VerticalLayout implements View, ClickListener, BaseView{

private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "process-difinition";
	private HtQuyen htQuyen;
	
	private static String MAIN_TITLE;
	private static String SMALL_TITLE;
	
	private String prevViewName;

	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_UPDATE_ID = "btn-update";
	private static final String BUTTON_ADD_FILED_ID = "btn-add-field";
	private static final String BUTTON_OPEN_POPUP_ADD_FILED_ID = "btn-open-popup-add-field";
	private static final String BUTTON_REMOVE_FIELD_ID = "btn-remove-field";
	
	private String action;
	private Button btnBack;
	private Button btnUpdate;
	
	private CssLayout formContainer;
	private TextField txtDesc;
	private HorizontalLayout formStarEventWrap;
	private FormGenerate formGenerate;
	
	private User userCurrent;
	
	@Autowired
	protected IdentityService identityService;
	private final FieldGroup fieldGroup = new FieldGroup();
	
	@Autowired
	private RepositoryService repositoryService;	
	
	@Autowired
	protected TaskService taskService;
	
	@Autowired
	protected RuntimeService runtimeService;

	@Autowired
	private FormService formService;
	
	@Autowired
	private BieuMauService bieuMauService;
	
	@Autowired
	private HoSoService hoSoService;
	@Autowired
	private DeploymentHtDonViService deploymentHtDonViService;
	@Autowired
	private HtDonViServices htDonViServices;
	
	private ProcessDefinition processDefinitionCurrent;
	
	private TabSheet tabInfo;
	private TreeTable donViTreeTable;
	private CssLayout contentWrap;

	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;

	@Autowired
	private HtThamSoService htThamSoService;
	private String solrUrl;
	@Autowired
	private ProcinstInfoService procinstInfoService;
	@Autowired
	private CauHinhHienThiService cauHinhHienThiService;
	
	private Window fieldWindow;
	private final PropertysetItem fieldItemValue = new PropertysetItem();
	private final FieldGroup fieldFieldGroup = new FieldGroup();
	private BeanItemContainer<ComboboxItem> truongThongTinContainer; 
	private BeanItemContainer<ComboboxItem> truongHienThiContainer;
	private ProcessDefinitionDetailView currentView = this;
	
	public 
	
	@PostConstruct
    void init() {
		messageSourceAccessor= new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());
		MAIN_TITLE = messageSourceAccessor.getMessage(Messages.UI_VIEW_PROCESSDEFINITION_DETAIL_TITLE);
		SMALL_TITLE = messageSourceAccessor.getMessage(Messages.UI_VIEW_PROCESSDEFINITION_DETAIL_TITLE_SMALL);
		HtThamSo solrUrlThamSo = htThamSoService.getHtThamSoFind(ThamSo.SOLRSEARCH_URL);
		String id = CommonUtils.getUserLogedIn();
		userCurrent = identityService.createUserQuery().userId(id).singleResult();
		if(solrUrlThamSo != null && solrUrlThamSo.getGiaTri()!= null){
			solrUrl = solrUrlThamSo.getGiaTri();
		}
		initMainTitle();
    }
	
	
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_:]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				if(Constants.ACTION_ADD.equals(action)){
					initDetailForm();
				}else{
					String id = m.group(3);
					processDefinitionCurrent = repositoryService.createProcessDefinitionQuery().processDefinitionId(id).singleResult();
					if(processDefinitionCurrent != null){
						BeanItem<ProcessDefinition> item = new BeanItem<ProcessDefinition>(processDefinitionCurrent);
				        fieldGroup.setItemDataSource(item);
				        
						initDetailForm();
						boolean hasStartFormKey = processDefinitionCurrent.hasStartFormKey();
						if(hasStartFormKey){
							String formKey = formService.getStartFormKey(processDefinitionCurrent.getId());
							if(formKey != null && !formKey.isEmpty()){
								initFormStarEventWrap(formKey);
							}
						}
						initBtnGroup();
					}else {
						Notification notf = new Notification("Thông báo", "Quy trình không tồn tại");
						notf.setDelayMsec(3000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
					}
				}

				initConfigFieldWindow();
					
			}
		}
		
	}
	
	public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSourceAccessor.getMessage(Messages.UI_VIEW_HOME));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(ProcessDefinitionListView.MAIN_TITLE);
		bcItem.setViewName(ProcessDefinitionListView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
		
		contentWrap = new CssLayout();
		contentWrap.setWidth("100%");
		contentWrap.addStyleName(ExplorerLayout.PROCESS_DETAIL_CONTENT);
		addComponent(contentWrap);
		
		PageTitleLayout titleWrap = new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false);
		titleWrap.addStyleName(ExplorerLayout.TITLE_WRAP_PROCESS_DETAIL);
		contentWrap.addComponent(titleWrap);
	}
	
	public void initConfigFieldWindow(){
		List<BieuMau> bmList = new ArrayList();
		fieldFieldGroup.setItemDataSource(fieldItemValue);
		if(processDefinitionCurrent != null){
			ProcessDefinitionEntity processDefEntity = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService).getDeployedProcessDefinition(processDefinitionCurrent.getId());
			List<ActivityImpl> activities = processDefEntity.getActivities();
			List<String> fomrKeyList = new ArrayList<String>();
			for(ActivityImpl act : activities){
				Object obj = act.getProperty("taskDefinition");
				if(obj != null && obj instanceof TaskDefinition){
					TaskDefinition taskDef = (TaskDefinition) obj;
					if(taskDef.getFormKeyExpression() != null){
						String formKey = taskDef.getFormKeyExpression().getExpressionText();
						if(formKey != null && !formKey.isEmpty()){
							Pattern p = Pattern.compile("(.+)#(.+)");
							Matcher m = p.matcher(formKey);
							if (m.matches()) {
								formKey = m.group(1);
							}
							fomrKeyList.add(formKey);
						}
					}
				}
			}
			bmList = bieuMauService.findByCode(fomrKeyList);
		}
		
		List<ComboboxItem> fieldList = new ArrayList();
		
		Map<BieuMau, String> pathMap = new HashMap<BieuMau, String>();
		while(!bmList.isEmpty()){
			BieuMau bm = bmList.remove(0);
			List<BmO> bmOList = bieuMauService.getBmOFind(null, null, null, bm, null, null, -1, -1);
			for (BmO o : bmOList) {
				String path = "";
				if(pathMap.get(bm) != null){
					path = pathMap.get(bm);
				}
				if(o.getLoaiO() != null && o.getLoaiO().intValue() == CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()){
					if(!path.isEmpty()){
						path+="#";
					}
					path += bm.getMa();
					List<BmOTruongThongTin> oTruongThongTinList = bieuMauService.getBmOTruongThongTinFind(null, null, o, null, -1, -1);
					for (BmOTruongThongTin oTruongThongTin : oTruongThongTinList) {
						ComboboxItem item = new ComboboxItem();
						item.setText(oTruongThongTin.getTruongThongTin().getTen());
						item.setValue(path+"#"+oTruongThongTin.getTruongThongTin().getMa());
						fieldList.add(item);
					}
				}else if(o.getLoaiO() != null && o.getLoaiO().intValue() == CommonUtils.ELoaiO.BIEU_MAU.getCode() && o.getBieuMauCon() != null){
					bmList.add(o.getBieuMauCon());
					pathMap.put(o.getBieuMauCon(), path);
				}
			}
		}
		
		truongThongTinContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, fieldList);
		
    	FormLayout form = new FormLayout();
    	form.setWidth("100%");
    	form.setMargin(true);
    	form.setSpacing(true);
    	
    	ComboBox truongThongTinCbb = new ComboBox("Trường thông tin", truongThongTinContainer);
    	truongThongTinCbb.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	truongThongTinCbb.setItemCaptionPropertyId("text");
    	truongThongTinCbb.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	truongThongTinCbb.setRequired(true);
    	truongThongTinCbb.setRequiredError("Không được để trống");
    	form.addComponent(truongThongTinCbb);
    	fieldItemValue.addItemProperty("truongThongTin", new ObjectProperty<ComboboxItem>(null, ComboboxItem.class));
		fieldFieldGroup.bind(truongThongTinCbb, "truongThongTin");
    	
    	TextField txtAliasName = new TextField("Tên hiển thị");
    	txtAliasName.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	txtAliasName.setNullRepresentation("");
    	txtAliasName.setRequired(true);
    	txtAliasName.setRequiredError("Không được để trống");
    	form.addComponent(txtAliasName);
    	fieldItemValue.addItemProperty("alias", new ObjectProperty<String>(null, String.class));
    	fieldFieldGroup.bind(txtAliasName, "alias");
    	
    	truongThongTinCbb.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				ComboboxItem obj = (ComboboxItem) event.getProperty().getValue();
				if(obj != null && obj.getText() != null){
					txtAliasName.setValue(obj.getText());
				}
			}
		});
    	Button addGroup = new Button("Thêm");
		addGroup.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
		addGroup.addClickListener(this);
		addGroup.setId(BUTTON_ADD_FILED_ID);
		form.addComponent(addGroup);
    	
    	fieldWindow = new Window("Trường thông tin");
    	fieldWindow.setWidth(600, Unit.PIXELS);
    	fieldWindow.setHeight(200, Unit.PIXELS);
    	fieldWindow.setResizable(true);
    	fieldWindow.center();
    	fieldWindow.setModal(true);
    	fieldWindow.setContent(form);
    }
	
	public void initDetailForm(){
		formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	formContainer.setStyleName(ExplorerLayout.PROCESS_DETAIL_CONTAINER);
    	
    	tabInfo = new TabSheet();
    	tabInfo.addStyleName(ExplorerLayout.PROCESS_DETAIL_TAB);
    	formContainer.addComponent(tabInfo);
    	
    	CssLayout formRowGeneral = new CssLayout();
		formRowGeneral.setWidth("100%");
		formRowGeneral.addStyleName(ExplorerLayout.FORM_ROW);
		
		tabInfo.addTab(formRowGeneral, "Thông tin chung");
    	

    	CssLayout svgDiagramWrap = new CssLayout();
    	svgDiagramWrap.setWidth("100%");
    	svgDiagramWrap.addStyleName(ExplorerLayout.FORM_ROW);
		
    	tabInfo.addTab(svgDiagramWrap, "Các bước thực hiện");
    	
    	ProcessDiagramViewer svgDiagram = new ProcessDiagramViewer(processDefinitionCurrent.getId(), null);
    	svgDiagram.addStyleName(ExplorerLayout.DETAIL_ROW_WRAP);
    	svgDiagramWrap.addComponent(svgDiagram);
    	
    	
    	FormLayout formData = new FormLayout();
    	formData.addStyleName(ExplorerLayout.DETAIL_ROW_WRAP);
    	formData.addStyleName(ExplorerLayout.PROCESS_DETAIL);
    	formData.setMargin(true);
    	
    	String name = "#Không tên";
    	if(processDefinitionCurrent.getName() != null && !processDefinitionCurrent.getName().isEmpty()){
    		name = processDefinitionCurrent.getName();
    	}
    	
    	Label ten = new Label(name);
    	ten.setCaption("Tên quy trình");
    	ten.setSizeFull();
    	formData.addComponent(ten);
    	
    	String desc = "#Không có mô tả";
    	if(processDefinitionCurrent.getDescription() != null && !processDefinitionCurrent.getDescription().isEmpty()){
    		desc = processDefinitionCurrent.getDescription();
    	}
    	
    	Label moTa = new Label(desc);
    	moTa.setCaption("Mô tả");
    	moTa.setSizeFull();
    	formData.addComponent(moTa);
    	
    	initDonViList();
    	formData.addComponent(donViTreeTable);
    	formRowGeneral.addComponent(formData);
    	
    	CssLayout fieldDisplayConfig = new CssLayout();
    	fieldDisplayConfig.setWidth("100%");
    	fieldDisplayConfig.addStyleName("field-display-config-wrap");
    	fieldDisplayConfig.addStyleName(ExplorerLayout.FORM_ROW);
		
    	if(Constants.ACTION_START.equals(action) || Constants.ACTION_EDIT.equals(action)){
    		tabInfo.addTab(fieldDisplayConfig, "Cấu hình hiển thị");
    		
    		List<CauHinhHienThi> list = cauHinhHienThiService.findByProcessDefId(processDefinitionCurrent.getId());
    		List<ComboboxItem> truongHienThiList = new ArrayList();
    		if(list != null){
    			for(CauHinhHienThi item : list){
    				ComboboxItem i = new ComboboxItem();
    				i.setText(item.getTitle());
    				i.setValue(item.getKey());
    				truongHienThiList.add(i);
    			}
    		}
    		
    		truongHienThiContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, truongHienThiList);
    		
    		Table tblField = new Table();
    		tblField.setContainerDataSource(truongHienThiContainer);
    		tblField.addStyleName(ExplorerLayout.DATA_TABLE);
    		tblField.setWidth("100%");
    		tblField.addGeneratedColumn("ten", new ColumnGenerator(){
    			private static final long serialVersionUID = 1L;

    			@Override
    			public Object generateCell(Table source, Object itemId, Object columnId) {
    				ComboboxItem current = (ComboboxItem) itemId;
    				return current.getValue();
    			}
    		});
    		
    		tblField.addGeneratedColumn("alias", new ColumnGenerator(){
    			private static final long serialVersionUID = 1L;

    			@Override
    			public Object generateCell(Table source, Object itemId, Object columnId) {
    				ComboboxItem current = (ComboboxItem) itemId;
    				return current.getText();
    			}
    		});
    		
    		tblField.setColumnWidth("action", 60);
    		tblField.addGeneratedColumn("action", new ColumnGenerator(){
    			private static final long serialVersionUID = 1L;

    			@Override
    			public Object generateCell(Table source, Object itemId, Object columnId) {
    				ComboboxItem current = (ComboboxItem) itemId;
    				Button buttonRemoveGroup = new Button();
    				buttonRemoveGroup.setId(BUTTON_REMOVE_FIELD_ID);
    				buttonRemoveGroup.addClickListener(currentView);
    				buttonRemoveGroup.setIcon(FontAwesome.REMOVE);
    				buttonRemoveGroup.setStyleName(ExplorerLayout.BTN_REMOVE_TBL_RPT);
    				buttonRemoveGroup.setData(current);
    				buttonRemoveGroup.addStyleName(ExplorerLayout.REMOVE_BTN_ACT);
    				return buttonRemoveGroup;
    			}
    		});
    		
    		tblField.setCellStyleGenerator(new Table.CellStyleGenerator(){
    			private static final long serialVersionUID = 1L;
    			@Override
    			public String getStyle(Table source, Object itemId, Object propertyId) {
    				if("action".equals(propertyId)){
                        return "center-aligned";
                    }
    				return "left-aligned";
    			}
    		});
    		
    		tblField.setColumnHeader("ten", "Trường thông tin");
    		tblField.setColumnHeader("alias", "Tên hiển thị");
    		tblField.setColumnHeader("action", "");
    		if(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action)){
    			tblField.setVisibleColumns("ten", "alias");
    		}else{
    			tblField.setVisibleColumns("ten", "alias", "action");
    		}
    		fieldDisplayConfig.addComponent(tblField);
    		
    		//TODO set nut
    		CssLayout buttons = new CssLayout();
    		buttons.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    		buttons.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);
    		
    		Button btnAddField = new Button("Thêm trường hiển thị");
    		btnAddField.addClickListener(this);
    		btnAddField.setId(BUTTON_OPEN_POPUP_ADD_FILED_ID);
    		btnAddField.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
    		buttons.addComponent(btnAddField);

    		fieldDisplayConfig.addComponent(buttons);
    	}
    	
		
		
    	
    	if(Constants.ACTION_START.equals(action)){
    		FormLayout descForm = new FormLayout();
        	descForm.addStyleName(ExplorerLayout.PRO_DEF_DESC_FORM);
        	descForm.setWidth("100%");
        	
        	formContainer.addComponent(descForm);
        	txtDesc = new TextField("Ghi chú");
        	txtDesc.addStyleName(ExplorerLayout.PRO_DEF_DESC_TXT);
        	txtDesc.setInputPrompt("Ghi chú...");
        	txtDesc.setWidth("100%");
        	descForm.addComponent(txtDesc);
    	}
    	
    	contentWrap.addComponent(formContainer);
	}
	
	public void initFormStarEventWrap(String formKey){
		formStarEventWrap = new HorizontalLayout();
		addComponent(formStarEventWrap);
		formStarEventWrap.setMargin(true);
		formStarEventWrap.setSpacing(true);
		formStarEventWrap.setWidth("100%");
		formStarEventWrap.addStyleName(ExplorerLayout.FORM_START_WRAP);
		formGenerate = new FormGenerate(formKey);
		formGenerate.setWidth("100%");
		formStarEventWrap.addComponent(formGenerate);
	}
	
	
	public void initDonViList(){
		
		List<DeploymentHtDonVi> deploymentHtDonViList =deploymentHtDonViService.getDeploymentHtDonViFind( processDefinitionCurrent.getDeploymentId());
		
		List<Long> trangThaiSD = null;
		WorkflowManagement<HtDonVi> htDonviWf = new WorkflowManagement<HtDonVi>(EChucNang.HT_DON_VI.getMa());
		if(htDonviWf.getTrangThaiSuDung() != null){
			trangThaiSD = new ArrayList();
			trangThaiSD.add(new Long(htDonviWf.getTrangThaiSuDung().getId()));
		}
		
		List<HtDonVi> donViAll = htDonViServices.getHtDonViFind(null, null, null, trangThaiSD, -1, -1);
		
		donViTreeTable = new TreeTable("Đơn vị được áp dụng");
    	donViTreeTable.setWidth("100%");
    	donViTreeTable.addContainerProperty("ma", CheckBox.class, null);
    	donViTreeTable.addContainerProperty("ten", String.class, null);
    	donViTreeTable.setColumnHeader("ma", "Mã");
    	donViTreeTable.setColumnHeader("ten", "Tên");
    	donViTreeTable.setVisibleColumns("ma", "ten");
		
		List<HtDonVi> donViSelected = new ArrayList();
		
		if(deploymentHtDonViList != null && !deploymentHtDonViList.isEmpty()){
			for(DeploymentHtDonVi item : deploymentHtDonViList){
				donViSelected.add(item.getHtDonVi());
			}
		}
		
		for(HtDonVi dv : donViAll){
			CheckBox checkBox =new CheckBox();
			checkBox.setCaption(dv.getMa());
			if(donViSelected.contains(dv)){
				checkBox.setValue(true);
			}
			checkBox.setReadOnly(true);
			donViTreeTable.addItem(new Object[]{checkBox, dv.getTen()},dv);
			donViTreeTable.setCollapsed(dv, false);
			donViTreeTable.setChildrenAllowed(dv, false);
		}
		
		for(HtDonVi donVi:donViAll){
			if(donVi.getHtDonVi()!=null){
				donViTreeTable.setChildrenAllowed(donVi.getHtDonVi(), true);
				donViTreeTable.setParent(donVi, donVi.getHtDonVi());
				
			}
		}
		
	}
	
	public void initBtnGroup(){
		
		//TODO set nut
		CssLayout buttons = new CssLayout();
		buttons.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		buttons.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);
		
		if(Constants.ACTION_START.equals(action)){
			btnUpdate = new Button("Khởi tạo quy trình");
			btnUpdate.addClickListener(this);
			btnUpdate.setId(BUTTON_UPDATE_ID);
			btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
			buttons.addComponent(btnUpdate);
		}else if(Constants.ACTION_ACTIVE.equals(action)){
			btnUpdate = new Button("Sử dụng");
			btnUpdate.addClickListener(this);
			btnUpdate.setId(BUTTON_UPDATE_ID);
			btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
			buttons.addComponent(btnUpdate);
		}else if(Constants.ACTION_SUSPEND.equals(action)){
			btnUpdate = new Button("Ngừng sử dụng");
			btnUpdate.addClickListener(this);
			btnUpdate.setId(BUTTON_UPDATE_ID);
			btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
			buttons.addComponent(btnUpdate);
		}else if(Constants.ACTION_DEL.equals(action)){
			btnUpdate = new Button("Xóa");
			btnUpdate.addClickListener(this);
			btnUpdate.setId(BUTTON_UPDATE_ID);
			btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
			buttons.addComponent(btnUpdate);
		}else if(Constants.ACTION_EDIT.equals(action)){
			btnUpdate = new Button("Sửa");
			btnUpdate.addClickListener(this);
			btnUpdate.setId(BUTTON_UPDATE_ID);
			btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
			buttons.addComponent(btnUpdate);
		}
		
		
		btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
		
		addComponent(buttons);
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Authentication.setAuthenticatedUserId(CommonUtils.getUserLogedIn());
			Button button = event.getButton();
			if(BUTTON_CANCEL_ID.equals(button.getId())){
				if(prevViewName == null){
					prevViewName = ProcessDefinitionListView.VIEW_NAME;
				}
				UI.getCurrent().getNavigator().navigateTo(prevViewName);
			}else if(BUTTON_OPEN_POPUP_ADD_FILED_ID.equals(button.getId())){
				UI.getCurrent().addWindow(fieldWindow);
			}else if(BUTTON_ADD_FILED_ID.equals(button.getId())){
				if(fieldFieldGroup.isValid()){
					fieldFieldGroup.commit();
					fieldWindow.close();
					String alias = (String) fieldItemValue.getItemProperty("alias").getValue();
					ComboboxItem truongTTPath = (ComboboxItem) fieldItemValue.getItemProperty("truongThongTin").getValue();

					ComboboxItem item = new ComboboxItem();
					item.setText(alias);
					item.setValue(truongTTPath.getValue().toString());
					truongHienThiContainer.addBean(item);
					fieldFieldGroup.clear();
				}
			}else if(BUTTON_REMOVE_FIELD_ID.equals(button.getId())){
				ComboboxItem current = (ComboboxItem) button.getData();
				truongHienThiContainer.removeItem(current);
			}else if(BUTTON_UPDATE_ID.equals(button.getId())){
				if(Constants.ACTION_START.equals(action)){
					try {
						String maDonVi = identityService.getUserInfo(userCurrent.getId(), "HT_DON_VI");
						Date date = new Date();
						SimpleDateFormat df = new SimpleDateFormat("yyyy");
						String year = df.format(date);
						if(processDefinitionCurrent.hasStartFormKey() && formGenerate != null){
							if(formGenerate.validateForm()){
								//Map<String, String> properties = formGenerate.getValueMap();
								//ProcessInstance processInstance = formService.submitStartFormData(processDefinitionCurrent.getId(), properties);
								Map<String, Object> variables = formGenerate.getFormVariableMap();
								ProcessInstance processInstance = runtimeService.startProcessInstanceById(processDefinitionCurrent.getId(), variables);
								
								try {
									if(txtDesc != null && txtDesc.getValue() != null){
										String desc = txtDesc.getValue();
										ProcinstInfo procinstInfo = new ProcinstInfo();
										procinstInfo.setInfoKey("DESC");
										procinstInfo.setInfo(desc);
										procinstInfo.setProcinstId(processInstance.getId());
										procinstInfo.setCode(maDonVi+"-"+year+"-"+processInstance.getId());
										procinstInfoService.save(procinstInfo);
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								runtimeService.setProcessInstanceName(processInstance.getId(), processDefinitionCurrent.getName());
								
								String hsXml = formGenerate.getFormData();
								EFormBoHoSo eBoHS = new EFormBoHoSo();
								EFormHoSo hs = CommonUtils.getEFormHoSo(hsXml);
								eBoHS.setHoSo(hs);
								String boHsXml = CommonUtils.getEFormBoHoSoString(eBoHS);
								
								HoSo hoSo = new HoSo();
								hoSo.setNgayTao(new Timestamp(date.getTime()));
								hoSo.setHoSoGiaTri(boHsXml);
								hoSo.setNguoiTao(CommonUtils.getUserLogedIn());
								hoSo.setProcessInstanceId(processInstance.getId());
								hoSoService.save(hoSo);
								
								
								
								Notification notf = new Notification("Thông báo", "Start thành công");
								notf.setDelayMsec(3000);
								notf.setPosition(Position.TOP_CENTER);
								notf.show(Page.getCurrent());
								
								List<Task> taskList = taskService.createTaskQuery().processInstanceId(processInstance.getId()).taskAssignee(CommonUtils.getUserLogedIn()).active().orderByTaskPriority().desc().orderByTaskCreateTime().desc().list();
								if(taskList != null && !taskList.isEmpty()){
									Task currentRow = taskList.get(0);
									UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
								}else{
									UI.getCurrent().getNavigator().navigateTo(MyProcessInstanceDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+processInstance.getId());
								}
							}
						}else{
							
							ProcessInstance proInst = runtimeService.startProcessInstanceById(processDefinitionCurrent.getId());
							try {
								if(txtDesc != null && txtDesc.getValue() != null){
									String desc = txtDesc.getValue();
									ProcinstInfo procinstInfo = new ProcinstInfo();
									procinstInfo.setInfoKey("DESC");
									procinstInfo.setInfo(desc);
									procinstInfo.setProcinstId(proInst.getId());
									procinstInfo.setCode(maDonVi+"-"+year+"-"+proInst.getId());
									procinstInfoService.save(procinstInfo);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							runtimeService.setProcessInstanceName(proInst.getId(), processDefinitionCurrent.getName());
							Notification notf = new Notification("Thông báo", "Start thành công");
							notf.setDelayMsec(3000);
							notf.setPosition(Position.TOP_CENTER);
							notf.show(Page.getCurrent());
							List<Task> taskList = taskService.createTaskQuery().processInstanceId(proInst.getId()).taskAssignee(CommonUtils.getUserLogedIn()).active().orderByTaskPriority().desc().orderByTaskCreateTime().desc().list();
							if(taskList != null && !taskList.isEmpty()){
								Task currentRow = taskList.get(0);
								UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
							}else{
								UI.getCurrent().getNavigator().navigateTo(MyProcessInstanceDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+proInst.getId());
							}
						}
						try {
							List<ComboboxItem> data = truongHienThiContainer.getItemIds();
							if(data != null){
								List<CauHinhHienThi> list = new ArrayList();
								for(ComboboxItem item : data){
									CauHinhHienThi i = new CauHinhHienThi();
									i.setKey(item.getValue().toString());
									i.setTitle(item.getText());
									i.setProcessDefId(processDefinitionCurrent.getId());
									list.add(i);
								}
								cauHinhHienThiService.save(list, processDefinitionCurrent.getId());
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					} catch (Exception e) {
						Notification notf = new Notification("Thông báo", e.getMessage(), Notification.Type.WARNING_MESSAGE);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
						notf.setDelayMsec(20000);
						UI.getCurrent().getNavigator().navigateTo(ModelerListView.VIEW_NAME);
						e.printStackTrace();
					}
					
				}else if(Constants.ACTION_ACTIVE.equals(action)){
					repositoryService.activateProcessDefinitionById(processDefinitionCurrent.getId());
					Notification notf = new Notification("Thông báo", "Sử dụng thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionListView.VIEW_NAME);
				}else if(Constants.ACTION_SUSPEND.equals(action)){
					repositoryService.suspendProcessDefinitionById(processDefinitionCurrent.getId());
					Notification notf = new Notification("Thông báo", "Ngừng sử dụng thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionListView.VIEW_NAME);
				}else if(Constants.ACTION_DEL.equals(action)){
					String solrId = CommonUtils.ESolrSearchType.PROCESS.getCode()+"_"+processDefinitionCurrent.getDeploymentId();
					repositoryService.deleteDeployment(processDefinitionCurrent.getDeploymentId());
					Notification notf = new Notification("Thông báo", "Xóa thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionListView.VIEW_NAME);
					try {
						SolrUtils.deleteFromSolr(solrId, solrUrl);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else if(Constants.ACTION_EDIT.equals(action)){
					List<ComboboxItem> data = truongHienThiContainer.getItemIds();
					if(data != null){
						List<CauHinhHienThi> list = new ArrayList();
						for(ComboboxItem item : data){
							CauHinhHienThi i = new CauHinhHienThi();
							i.setKey(item.getValue().toString());
							i.setTitle(item.getText());
							i.setProcessDefId(processDefinitionCurrent.getId());
							list.add(i);
						}
						cauHinhHienThiService.save(list, processDefinitionCurrent.getId());
					}
					
					Notification notf = new Notification("Thông báo", "Cập nhật thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionListView.VIEW_NAME);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra!");
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
		}
	}

	@Override
	public String getPrevViewName() {
		return prevViewName;
	}

	@Override
	public void setPrevViewName(String prevViewName) {
		this.prevViewName = prevViewName;
	}

	@Override
	public String getViewName() {
		return VIEW_NAME;
	}

}

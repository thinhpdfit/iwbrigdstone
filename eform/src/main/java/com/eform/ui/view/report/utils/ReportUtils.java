package com.eform.ui.view.report.utils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.eform.common.ExplorerLayout;
import com.eform.common.SpringApplicationContext;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.CommonUtils.EKieuDuLieu;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BieuMauService;
import com.eform.service.HtDonViServices;
import com.eform.ui.view.report.type.FilterObject;
import com.eform.ui.view.report.type.v2.VisibleColumn;
import com.vaadin.data.Item;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;

public class ReportUtils {
	public static Map<String, Object> getBieuMauTreeMap(BieuMau bieuMau, List<String> bieuMauList){
		BieuMauService bieuMauService = SpringApplicationContext.getApplicationContext().getBean(BieuMauService.class);
		Map<String, Object> bieuMauTreeMap = new HashMap();
		if(bieuMau != null && bieuMauList.contains(bieuMau.getMa())){
			List<Map<String, Object>> process = new ArrayList();
			Map<String, Object> map = new HashMap();
			map.put("BM", bieuMau);
			map.put("MA_NHOM", "ROOT");
    		process.add(map);
    		while(!process.isEmpty()){
    			Map<String, Object> current = process.remove(0);
    			BieuMau bm = (BieuMau) current.get("BM");
    			String maNhom = (String) current.get("MA_NHOM");
    			BieuMau parent = (BieuMau) current.get("PARENT");
    			String parentNhom = (String) current.get("PARENT_NHOM");
    			List<BmO> bmOList = bieuMauService.getBmOFind(null, null, null, bm, null, null, -1, -1);
    			List<TruongThongTin> truongTTList = new ArrayList();
    			bieuMauTreeMap.put(maNhom, bm);
    			bieuMauTreeMap.put(maNhom+"_TTT", truongTTList);
    			bieuMauTreeMap.put(maNhom+"_PARENT", parent);
    			bieuMauTreeMap.put(maNhom+"_PARENT_NHOM", parentNhom);
    			for(BmO o : bmOList){
    				if(o.getLoaiO() != null 
    						&& o.getLoaiO().intValue() == CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()){
    					List<BmOTruongThongTin> oTruongThongTinList = bieuMauService.getBmOTruongThongTinFind(null, null, o, null, -1, -1);
    					for (BmOTruongThongTin oTruongThongTin : oTruongThongTinList) {
    						truongTTList.add(oTruongThongTin.getTruongThongTin());
    					}
    				}else if(o.getLoaiO() != null 
    						&& o.getLoaiO().intValue() == CommonUtils.ELoaiO.BIEU_MAU.getCode() 
    						&& o.getBieuMauCon() != null){
    					Map<String, Object> subMap = new HashMap();
    					subMap.put("BM", o.getBieuMauCon());
    					String maNhomTmp = o.getTen() != null && !o.getTen().trim().isEmpty() ? o.getTen() : o.getBieuMauCon().getMa();
    					subMap.put("MA_NHOM", maNhomTmp);
    					subMap.put("PARENT", bm);
    					subMap.put("PARENT_NHOM", maNhom);
    					process.add(subMap);
    				}
    			}
    		}
		}
		return bieuMauTreeMap;
	}
	
	public static Table createTableViewer(List<Map<String, Object>> resultList, List<VisibleColumn> visibleColumnList){
		IdentityService identityService = SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
		BieuMauService bieuMauService = SpringApplicationContext.getApplicationContext().getBean(BieuMauService.class);
		HtDonViServices htDonViServices = SpringApplicationContext.getApplicationContext().getBean(HtDonViServices.class);
		Table table = new Table();
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setWidth("100%");
		for(VisibleColumn column : visibleColumnList){
			String ma = column.getTruongThongTin().getMa();
			Long kieuDL = column.getTruongThongTin().getKieuDuLieu();
			if(kieuDL != null && EKieuDuLieu.DANH_MUC.getMa() == kieuDL.intValue()){
				table.addContainerProperty(ma, String.class, null);
				table.addGeneratedColumn(ma, new ColumnGenerator(){
					private static final long serialVersionUID = 1L;
					@Override
					public Object generateCell(Table source, Object itemId, Object columnId) {
						Item current = table.getItem(itemId);
						if(current != null && current.getItemProperty(columnId) != null && current.getItemProperty(columnId).getValue() != null){
							String key = (String)current.getItemProperty(columnId).getValue();
							List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(key, null, null, null, null, 0, 1);
							if(danhMucList != null && !danhMucList.isEmpty()){
								DanhMuc dm = danhMucList.get(0);
								return dm.getTen();
							}
						}
						return "";
					}
				});
			}else if(kieuDL != null && (EKieuDuLieu.NUMBER.getMa() == kieuDL.intValue() || EKieuDuLieu.SO_NGUYEN.getMa() == kieuDL.intValue())){
				table.addContainerProperty(ma, String.class, null);
				table.addGeneratedColumn(ma, new ColumnGenerator(){
					private static final long serialVersionUID = 1L;
					@Override
					public Object generateCell(Table source, Object itemId, Object columnId) {
						Item current = table.getItem(itemId);
						if(current != null && current.getItemProperty(columnId) != null && current.getItemProperty(columnId).getValue() != null){
							Object v = current.getItemProperty(columnId).getValue();
							DecimalFormat df = new DecimalFormat("#,###,###,###.###");
							if(v instanceof String){
								Double d = new Double((String)v);
								return df.format(d);
							}else if(v instanceof Double){
								Double d =(Double) v;
								return df.format(d);
							}else{
								return v;
							}
						}
						return "";
					}
				});
			}else if(kieuDL != null && EKieuDuLieu.DATE.getMa() == kieuDL.intValue()){
				table.addContainerProperty(ma, Timestamp.class, null);
				table.addGeneratedColumn(ma, new ColumnGenerator(){
					private static final long serialVersionUID = 1L;
					@Override
					public Object generateCell(Table source, Object itemId, Object columnId) {
						Item current = table.getItem(itemId);
						if(current != null && current.getItemProperty(columnId) != null && current.getItemProperty(columnId).getValue() != null){
							Date v = (Date) current.getItemProperty(columnId).getValue();
							SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
							return df.format(v);
						}
						return "";
					}
				});
			}else if(kieuDL != null && EKieuDuLieu.USER.getMa() == kieuDL.intValue()){
				table.addContainerProperty(ma, String.class, null);
				table.addGeneratedColumn(ma, new ColumnGenerator(){
					private static final long serialVersionUID = 1L;
					@Override
					public Object generateCell(Table source, Object itemId, Object columnId) {
						Item current = table.getItem(itemId);
						if(current != null && current.getItemProperty(columnId) != null && current.getItemProperty(columnId).getValue() != null){
							String userId = (String) current.getItemProperty(columnId).getValue();
							User u = identityService.createUserQuery().userId(userId).singleResult();
							if(u!= null){
								return u.getFirstName()+" "+u.getLastName();
							}
						}
						return "";
					}
				});
			}else if(kieuDL != null && EKieuDuLieu.GROUP.getMa() == kieuDL.intValue()){
				table.addContainerProperty(ma, String.class, null);
				table.addGeneratedColumn(ma, new ColumnGenerator(){
					private static final long serialVersionUID = 1L;
					@Override
					public Object generateCell(Table source, Object itemId, Object columnId) {
						Item current = table.getItem(itemId);
						if(current != null && current.getItemProperty(columnId) != null && current.getItemProperty(columnId).getValue() != null){
							String groupId = (String) current.getItemProperty(columnId).getValue();
							Group g = identityService.createGroupQuery().groupId(groupId).singleResult();
							if(g!= null){
								return g.getName();
							}
						}
						return "";
					}
				});
			}else if(kieuDL != null && EKieuDuLieu.DON_VI.getMa() == kieuDL.intValue()){
				table.addContainerProperty(ma, String.class, null);
				table.addGeneratedColumn(ma, new ColumnGenerator(){
					private static final long serialVersionUID = 1L;
					@Override
					public Object generateCell(Table source, Object itemId, Object columnId) {
						Item current = table.getItem(itemId);
						if(current != null && current.getItemProperty(columnId) != null && current.getItemProperty(columnId).getValue() != null){
							String str = (String) current.getItemProperty(columnId).getValue();
							List<HtDonVi> htDonViList = htDonViServices.getHtDonViFind(str, null,null, null, 0, 1);
							if(htDonViList!= null && !htDonViList.isEmpty()){
								HtDonVi dv = htDonViList.get(0);
								return dv.getTen();
							}
						}
						return "";
					}
				});
			}else if(kieuDL != null && EKieuDuLieu.CHECKBOX.getMa() == kieuDL.intValue()){
				table.addContainerProperty(ma, String.class, null);
				table.addGeneratedColumn(ma, new ColumnGenerator(){
					private static final long serialVersionUID = 1L;
					@Override
					public Object generateCell(Table source, Object itemId, Object columnId) {
						Item current = table.getItem(itemId);
						CheckBox chk = new CheckBox();
						if(current != null && current.getItemProperty(columnId) != null && current.getItemProperty(columnId).getValue() != null){
							Object objecValue = current.getItemProperty(columnId).getValue();
							if(objecValue instanceof String && ((String) objecValue).equals("true")){
								chk.setValue(true);
							}
						}
						chk.setReadOnly(true);
						return chk;
					}
				});
			}else if(kieuDL != null && EKieuDuLieu.LUA_CHON_NHIEU.getMa() == kieuDL.intValue()){
				table.addContainerProperty(ma, String.class, null);
				table.addGeneratedColumn(ma, new ColumnGenerator(){
					private static final long serialVersionUID = 1L;
					@Override
					public Object generateCell(Table source, Object itemId, Object columnId) {
						Item current = table.getItem(itemId);
						if(current != null && current.getItemProperty(columnId) != null && current.getItemProperty(columnId).getValue() != null){
							String valueTmp = (String) current.getItemProperty(columnId).getValue();
							if (valueTmp != null && !valueTmp.isEmpty() && valueTmp.trim().length() > 2) {
								valueTmp = valueTmp.trim().substring(1, valueTmp.length() - 1);
								if (!valueTmp.isEmpty()) {
									String[] arr = valueTmp.split(",");
									if (arr != null && arr.length > 0) {
										LinkedHashSet<String> hs = new LinkedHashSet<String>();
										String value = "";
										for (String v : arr) {
											if(v != null){
												v = v.trim();
												List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(v, null, null, null, null, 0, 1);
												if(danhMucList != null && !danhMucList.isEmpty()){
													DanhMuc dm = danhMucList.get(0);
													if(!value.isEmpty()){
														value+="; ";
													}
													value+= dm.getTen();
												}
											}
										}
										return value;
									}
								}
							}
						}
						return "";
					}
				});
			}else{
				table.addContainerProperty(ma, String.class, null);
			}
			table.setColumnHeader(ma, column.getTenHienThi());
		}
		for(Map<String, Object> resultItem : resultList){
			Object newItemId = table.addItem();
			Item row = table.getItem(newItemId);
			for(VisibleColumn column : visibleColumnList){
				String k = column.getTruongThongTin().getMa();
				Object v = resultItem.get(k.toLowerCase());
				row.getItemProperty(k).setValue(v);
			}
		}
		return table;
	}
	
	public static String createSqlString(Map<String, Object> bieuMauTreeMap, List<String> processInstanceIdList, List<FilterObject> filterList, List<VisibleColumn> visibleColumnList, Date fromDate, Date toDate, Map<String, Object> paramMap){
		try {
			
			List<String> nhomList = new ArrayList();
			nhomList.add("ROOT");
			String select = "";
			String columnCurrent = "ho_so_json";
			String tableCurrent = "ho_so";
			String fullSql = "";
			while(!nhomList.isEmpty()){
				
				
				String sqlTmp = select;
				String current = nhomList.remove(0);
				if(bieuMauTreeMap.get(current+"_TTT") != null){
					List<TruongThongTin> truongTTList = (List<TruongThongTin>) bieuMauTreeMap.get(current+"_TTT");
					for(TruongThongTin truongTT : truongTTList){
						String maTruongTT = truongTT.getMa();
						String catsExp = "";
						if(truongTT.getKieuDuLieu() != null && truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
							catsExp = "::timestamptz";;
						}
						if(!select.isEmpty()){
							select += ", ";
						}
						select += maTruongTT;
						if(!sqlTmp.isEmpty()){
							sqlTmp += ", ";
						}
						sqlTmp += "("+columnCurrent+"#>>'{"+maTruongTT+"}')"+catsExp+" as " + maTruongTT;
					}
				}
				
				
				String child = getChild(bieuMauTreeMap, current);
				if(child != null){
					nhomList.add(child);
					if(!sqlTmp.isEmpty()){
						sqlTmp += ", ";
					}
					sqlTmp += "json_array_elements("+columnCurrent+"#>'{"+child+"}') as "+child;
					columnCurrent = child;
				}
				
				sqlTmp = current+"_SQL" +" AS(SELECT "+sqlTmp;
				
				sqlTmp+=",id from "+tableCurrent;
				if("ROOT".equals(current)){
					String whereClause = "";
					if(processInstanceIdList != null && !processInstanceIdList.isEmpty()){
						whereClause += " process_instance_id in (:process_instance_id_list_param)";
						paramMap.put("process_instance_id_list_param", processInstanceIdList);
					}
					
					if(fromDate != null){
						if(!whereClause.isEmpty()){
							whereClause += " AND ";
						}
						whereClause += " ngay_tao >= :ngay_tao_from_param ";
						paramMap.put("ngay_tao_from_param", fromDate);
					}
					if(toDate != null){
						if(!whereClause.isEmpty()){
							whereClause += " AND ";
						}
						whereClause += " ngay_tao <= :ngay_tao_to_param ";
						paramMap.put("ngay_tao_to_param", toDate);
					}
					sqlTmp += " WHERE " + whereClause;
				}
				sqlTmp += ")";

				tableCurrent = current+"_SQL";
				if(!fullSql.isEmpty()){
					fullSql += ", ";
				}
				fullSql += sqlTmp;
			}
			
			String whereStr = "";
			if(filterList != null){
				for(FilterObject filter : filterList){
					if(!whereStr.isEmpty()){
						whereStr += " AND ";
					}
					CommonUtils.EComparison comparison = CommonUtils.EComparison.getComparison(filter.getComparison());
					Long kieuDL = filter.getKieuDuLieu();
					String castExp = "";
					if(kieuDL != null){
						if(kieuDL.intValue() == CommonUtils.EKieuDuLieu.NUMBER.getMa() 
								|| kieuDL.intValue() == CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa()){
							castExp = "::numeric";
						}else if(kieuDL.intValue() == CommonUtils.EKieuDuLieu.DATE.getMa() 
								|| kieuDL.intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
							castExp = "::timestamptz";
						}
					}
					Object value = filter.getValue();
					paramMap.put(filter.getPath()+"_PARAM", value);
					whereStr += filter.getPath()+castExp+" "+comparison.getSymbol()+" :"+filter.getPath()+"_PARAM";
				}
				if(!whereStr.isEmpty()){
					whereStr = " WHERE "+whereStr;
				}
			}
			
			String columnStr = "id ";
			for(VisibleColumn vc : visibleColumnList){
				if(!columnStr.isEmpty()){
					columnStr += ", ";
				}
				columnStr += vc.getTruongThongTin().getMa();
			}
			
			fullSql = "WITH " +fullSql 
					+ " SELECT "+columnStr
					+ " FROM "+tableCurrent+whereStr
					+ " GROUP BY "+columnStr;
			return fullSql;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String createSqlStringById(String fieldId,Map<String, Object> bieuMauTreeMap, List<String> processInstanceIdList, List<FilterObject> filterList, List<VisibleColumn> visibleColumnList, Date fromDate, Date toDate, Map<String, Object> paramMap){
		try {
			
			List<String> nhomList = new ArrayList();
			nhomList.add("ROOT");
			String select = "";
			String columnCurrent = "ho_so_json";
			String tableCurrent = "ho_so";
			String fullSql = "";
			while(!nhomList.isEmpty()){
				
				
				String sqlTmp = select;
				String current = nhomList.remove(0);
				if(bieuMauTreeMap.get(current+"_TTT") != null){
					List<TruongThongTin> truongTTList = (List<TruongThongTin>) bieuMauTreeMap.get(current+"_TTT");
					for(TruongThongTin truongTT : truongTTList){
						String maTruongTT = truongTT.getMa();
						String catsExp = "";
						if(truongTT.getKieuDuLieu() != null && truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
							catsExp = "::timestamptz";;
						}
						if(!select.isEmpty()){
							select += ", ";
						}
						select += maTruongTT;
						if(!sqlTmp.isEmpty()){
							sqlTmp += ", ";
						}
						sqlTmp += "("+columnCurrent+"#>>'{"+maTruongTT+"}')"+catsExp+" as " + maTruongTT;
					}
				}
				
				
				String child = getChild(bieuMauTreeMap, current);
				if(child != null){
					nhomList.add(child);
					if(!sqlTmp.isEmpty()){
						sqlTmp += ", ";
					}
					sqlTmp += "json_array_elements("+columnCurrent+"#>'{"+child+"}') as "+child;
					columnCurrent = child;
				}
				
				sqlTmp = current+"_SQL" +" AS(SELECT "+sqlTmp;
				
				sqlTmp+=",id from "+tableCurrent;
				if("ROOT".equals(current)){
					String whereClause = "";
					if(processInstanceIdList != null && !processInstanceIdList.isEmpty()){
						whereClause += " process_instance_id in (:process_instance_id_list_param)";
						paramMap.put("process_instance_id_list_param", processInstanceIdList);
					}
					
					if(fromDate != null){
						if(!whereClause.isEmpty()){
							whereClause += " AND ";
						}
						whereClause += " ngay_tao >= :ngay_tao_from_param ";
						paramMap.put("ngay_tao_from_param", fromDate);
					}
					if(toDate != null){
						if(!whereClause.isEmpty()){
							whereClause += " AND ";
						}
						whereClause += " ngay_tao <= :ngay_tao_to_param ";
						paramMap.put("ngay_tao_to_param", toDate);
					}
					if(fieldId!=null) {
						if(!whereClause.isEmpty()){
							whereClause += " AND ";
						}
						whereClause += " id =:id_param ";
						paramMap.put("id_param", Integer.valueOf(fieldId));
					}
					sqlTmp += " WHERE " + whereClause;
				}
				sqlTmp += ")";

				tableCurrent = current+"_SQL";
				if(!fullSql.isEmpty()){
					fullSql += ", ";
				}
				fullSql += sqlTmp;
			}
			
			String whereStr = "";
			if(filterList != null){
				for(FilterObject filter : filterList){
					if(!whereStr.isEmpty()){
						whereStr += " AND ";
					}
					CommonUtils.EComparison comparison = CommonUtils.EComparison.getComparison(filter.getComparison());
					Long kieuDL = filter.getKieuDuLieu();
					String castExp = "";
					if(kieuDL != null){
						if(kieuDL.intValue() == CommonUtils.EKieuDuLieu.NUMBER.getMa() 
								|| kieuDL.intValue() == CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa()){
							castExp = "::numeric";
						}else if(kieuDL.intValue() == CommonUtils.EKieuDuLieu.DATE.getMa() 
								|| kieuDL.intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
							castExp = "::timestamptz";
						}
					}
					Object value = filter.getValue();
					paramMap.put(filter.getPath()+"_PARAM", value);
					whereStr += filter.getPath()+castExp+" "+comparison.getSymbol()+" :"+filter.getPath()+"_PARAM";
				}
				if(!whereStr.isEmpty()){
					whereStr = " WHERE "+whereStr;
				}
			}
			
			String columnStr = "id ";
			for(VisibleColumn vc : visibleColumnList){
				if(!columnStr.isEmpty()){
					columnStr += ", ";
				}
				columnStr += vc.getTruongThongTin().getMa();
			}
			
			fullSql = "WITH " +fullSql 
					+ " SELECT "+columnStr
					+ " FROM "+tableCurrent+whereStr
					+ " GROUP BY "+columnStr;
			return fullSql;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getChild(Map<String, Object> bieuMauTreeMap, String parent){
		for(Map.Entry<String, Object> entry : bieuMauTreeMap.entrySet()){
			Object v = entry.getValue();
			String k = entry.getKey();
			if(v instanceof String){
				String str = (String) v;
				if(str.equals(parent)){
					if(k.lastIndexOf("_PARENT_NHOM") > 0){
						return k.substring(0, k.lastIndexOf("_PARENT_NHOM"));
					}
				}
			}
		}
		return null;
	}
	
	public static Workbook exportExcel(String tieuDe, List<Map<String, Object>> resultList, List<VisibleColumn> visibleColumnList){
		try {
			IdentityService identityService = SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
			BieuMauService bieuMauService = SpringApplicationContext.getApplicationContext().getBean(BieuMauService.class);
			HtDonViServices htDonViServices = SpringApplicationContext.getApplicationContext().getBean(HtDonViServices.class);
			Workbook workbook = new XSSFWorkbook();
			Sheet sheet = workbook.createSheet("Báo cáo thống kê");
			int ri = 0; 
			//Title row
			Row titleRow = sheet.createRow(ri);
			Cell titleCell = titleRow.createCell(0);
			titleCell.setCellValue(tieuDe);
			ri++;
			//Header row
			Row headerRow = sheet.createRow(ri);
			CellStyle cellHeaderStyle = workbook.createCellStyle();
			cellHeaderStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			cellHeaderStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
			cellHeaderStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			cellHeaderStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			cellHeaderStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			ri++;
			int ci = 0;
			for(VisibleColumn column : visibleColumnList){
				Cell cell = headerRow.createCell(ci);
				cell.setCellValue(column.getTenHienThi());
				cell.setCellStyle(cellHeaderStyle);
				ci++;
			}
			//Rows
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
			cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			
			for(Map<String, Object> resultItem : resultList){
				ci = 0;
				Row row = sheet.createRow(ri);
				for(VisibleColumn column : visibleColumnList){
					Cell cell = row.createCell(ci);
					cell.setCellStyle(cellStyle);
					String k = column.getTruongThongTin().getMa();
					Object v = resultItem.get(k.toLowerCase());
					ci++;
					Long kieuDL = column.getTruongThongTin().getKieuDuLieu();
					if(v != null){
						if(kieuDL != null && EKieuDuLieu.DANH_MUC.getMa() == kieuDL.intValue()){
							if(v != null && v instanceof String){
								String key = (String)v;
								List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(key, null, null, null, null, 0, 1);
								if(danhMucList != null && !danhMucList.isEmpty()){
									DanhMuc dm = danhMucList.get(0);
									cell.setCellValue(dm.getTen());
								}
							}
						}else if(kieuDL != null && (EKieuDuLieu.NUMBER.getMa() == kieuDL.intValue() || EKieuDuLieu.SO_NGUYEN.getMa() == kieuDL.intValue())){
							if(v != null){
								DecimalFormat df = new DecimalFormat("#,###,###,###.###");
								if(v instanceof String){
									Double d = new Double((String)v);
									cell.setCellValue(df.format(d));
								}else if(v instanceof Double){
									Double d =(Double) v;
									cell.setCellValue(df.format(d));
								}
							}
						}else if(kieuDL != null && EKieuDuLieu.DATE.getMa() == kieuDL.intValue()){
							if(v != null && v instanceof Date){
								Date date = (Date) v;
								SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
								cell.setCellValue(df.format(date));
							}
						}else if(kieuDL != null && EKieuDuLieu.USER.getMa() == kieuDL.intValue()){
							if(v != null && v instanceof String){
								String userId = (String) v;
								User u = identityService.createUserQuery().userId(userId).singleResult();
								if(u!= null){
									cell.setCellValue(u.getFirstName()+" "+u.getLastName());
								}
							}
						}else if(kieuDL != null && EKieuDuLieu.GROUP.getMa() == kieuDL.intValue()){
							if(v != null && v instanceof String){
								String groupId = (String) v;
								Group g = identityService.createGroupQuery().groupId(groupId).singleResult();
								if(g!= null){
									cell.setCellValue(g.getName());
								}
							}
						}else if(kieuDL != null && EKieuDuLieu.DON_VI.getMa() == kieuDL.intValue()){
							if(v != null && v instanceof String){
								String str = (String) v;
								List<HtDonVi> htDonViList = htDonViServices.getHtDonViFind(str, null,null, null, 0, 1);
								if(htDonViList!= null && !htDonViList.isEmpty()){
									HtDonVi dv = htDonViList.get(0);
									cell.setCellValue(dv.getTen());
								}
							}
						}else if(kieuDL != null && EKieuDuLieu.CHECKBOX.getMa() == kieuDL.intValue()){
							if(v != null && v instanceof String && ((String) v).equals("true")){
								cell.setCellValue(true);
							}else{
								cell.setCellValue(false);
							}
						}else if(kieuDL != null && EKieuDuLieu.LUA_CHON_NHIEU.getMa() == kieuDL.intValue()){
							if(v != null && v instanceof String){
								String valueTmp = (String) v;
								if (valueTmp != null && !valueTmp.isEmpty() && valueTmp.trim().length() > 2) {
									valueTmp = valueTmp.trim().substring(1, valueTmp.length() - 1);
									if (!valueTmp.isEmpty()) {
										String[] arr = valueTmp.split(",");
										if (arr != null && arr.length > 0) {
											LinkedHashSet<String> hs = new LinkedHashSet<String>();
											String value = "";
											for (String item : arr) {
												if(item != null){
													item = item.trim();
													List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(item, null, null, null, null, 0, 1);
													if(danhMucList != null && !danhMucList.isEmpty()){
														DanhMuc dm = danhMucList.get(0);
														if(!value.isEmpty()){
															value+="; ";
														}
														value+= dm.getTen();
													}
												}
											}
											cell.setCellValue(value);
										}
									}
								}
							}
						}else{
							if(v instanceof BigDecimal){
								BigDecimal bd = (BigDecimal) v;
								cell.setCellValue(bd.longValueExact());
							}if(v instanceof String){
								String str = (String) v;
								cell.setCellValue(str);
							}else if(v instanceof java.util.Date){
								java.util.Date dataValue = (Date) v;
								cell.setCellValue(dataValue);
							}else{
								cell.setCellValue(v.toString());
							}
						}
					}
				}
				ri++;
			}
			for(int i =0; i< ci; i++){
				sheet.autoSizeColumn(i);
			}
			return workbook;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}

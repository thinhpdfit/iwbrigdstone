package com.eform.ui.view.form.config;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.service.LoaiDanhMucService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = LoaiDanhMucDetailNewView.VIEW_NAME)
public class LoaiDanhMucDetailNewView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "dropdown-detail";
	private static final String MAIN_TITLE = "Chi tiết loại danh mục";
	private static final String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_APPROVE_ID = "btn-approve";
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_ADD_ID = "btn-edit";
	private static final String BUTTON_DEL_ID = "btn-del";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_REFUSE_ID = "btn-refuse";
	private LoaiDanhMuc loaiDanhMucCurrent;
	private WorkflowManagement<LoaiDanhMuc> loaiDanhMucWM;
	Workflow<LoaiDanhMuc> loaiDanhMucWf;;
	private String action;
	
	private List<LoaiDanhMuc> loaiDanhMucList;
	private final FieldGroup fieldGroup = new FieldGroup();
	private CssLayout formContainer;
	private Button btnUpdate;
	private Button btnBack;
	
	

	private PropertysetItem danhMucItemValue = new PropertysetItem();
	private FieldGroup danhMucFieldGroup = new FieldGroup();
	
	@Autowired
	private LoaiDanhMucService loaiDanhMucService;
	@Autowired
	private MessageSource messageSource;
	
    @PostConstruct
    void init() {
    	loaiDanhMucWM = new WorkflowManagement<LoaiDanhMuc>(EChucNang.E_FORM_LOAI_DANH_MUC.getMa());
    	loaiDanhMucList = Lists.newArrayList(loaiDanhMucService.findAll());
    	danhMucFieldGroup.setItemDataSource(danhMucItemValue);
    	initMainTitle();
    }

    @Override
    public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				if(Constants.ACTION_ADD.equals(action)){
					loaiDanhMucCurrent = new LoaiDanhMuc();
					loaiDanhMucWf = loaiDanhMucWM.getWorkflow(loaiDanhMucCurrent);
					loaiDanhMucDetail(loaiDanhMucCurrent);

					initDetailForm();
				}else{
					String ma = m.group(3);
					List<LoaiDanhMuc> listLdm = loaiDanhMucService.findByCode(ma);
					if(listLdm != null && !listLdm.isEmpty()){
						loaiDanhMucCurrent = listLdm.get(0);
						loaiDanhMucWf = loaiDanhMucWM.getWorkflow(loaiDanhMucCurrent);
						loaiDanhMucDetail(loaiDanhMucCurrent);

						initDetailForm();
					}
					
				}
				
			}
		}

    }
    
    public void initDetailForm(){
    	
    	formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	FormLayout form = new FormLayout();
    	form.setMargin(true);
    	
    	TextField txtMa = new TextField("Mã loại danh mục");
    	txtMa.focus();
    	txtMa.setRequired(true);
    	txtMa.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void textChange(TextChangeEvent event) {

				TextField txt = (TextField) fieldGroup.getField("ma");
				System.out.println(event.getText());
				if(!checkTrungMa(event.getText(), action)){
					txt.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
				}else{
					txt.setComponentError(null);
				}
			}
    	});

    	txtMa.setTextChangeEventMode(TextChangeEventMode.LAZY);
    	
    	txtMa.setSizeFull();
    	txtMa.setNullRepresentation("");
    	txtMa.setRequiredError("Không được để trống");
    	txtMa.addValidator(new StringLengthValidator("Số ký tự tối đa là 20", 1, 20, false));   
    	txtMa.addValidator(new RegexpValidator("[0-9a-zA-Z_]+", "Chỉ chấp nhận các ký tự 0-9 a-z A-Z _"));
    	
    	TextField txtTen = new TextField("Tên loại danh mục");
    	txtTen.setRequired(true);
    	txtTen.setSizeFull();
    	txtTen.setNullRepresentation("");
    	txtTen.setRequiredError("Không được để trống");
    	txtTen.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", 1, 2000, false)); 
    	
    	fieldGroup.bind(txtMa, "ma");
    	fieldGroup.bind(txtTen, "ten");
    	
    	txtMa.setReadOnly("xem".equals(action));
    	txtTen.setReadOnly("xem".equals(action));
    	
    	form.addComponent(txtMa);
    	form.addComponent(txtTen);

    	
    	
    	
    	
    	
    	
    	
    	
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	if(!Constants.ACTION_VIEW.equals(action)){
    		btnUpdate = new Button();
    		btnUpdate.addClickListener(this);
    		buttons.addComponent(btnUpdate);
    		if(Constants.ACTION_EDIT.equals(action)){
        		btnUpdate.setCaption("Cập nhật");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_EDIT_ID);
        	}else if(Constants.ACTION_ADD.equals(action)){
        		btnUpdate.setId(BUTTON_ADD_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption("Thêm mới");
        	}else if(Constants.ACTION_DEL.equals(action)){
        		btnUpdate.setId(BUTTON_DEL_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
        		btnUpdate.setCaption("Xóa");
        	}else if(Constants.ACTION_APPROVE.equals(action)){
        		btnUpdate.setId(BUTTON_APPROVE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption(loaiDanhMucWf.getPheDuyetTen());
        	}else if(Constants.ACTION_REFUSE.equals(action)){
        		btnUpdate.setCaption(loaiDanhMucWf.getTuChoiTen());
        		btnUpdate.setId(BUTTON_REFUSE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_WARNING);
        	}
    	}
    	
    	btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
    	form.addComponent(buttons);

    	formContainer.addComponent(form);
    	addComponent(formContainer);
    }
    
    private void loaiDanhMucDetail(LoaiDanhMuc loaiDanhMuc) {
        if (loaiDanhMuc == null) {
        	loaiDanhMuc = new LoaiDanhMuc();
        }
        BeanItem<LoaiDanhMuc> item = new BeanItem<LoaiDanhMuc>(loaiDanhMuc);
        fieldGroup.setItemDataSource(item);
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(LoaiDanhMucView.MAIN_TITLE);
		bcItem.setViewName(LoaiDanhMucView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}

	public LoaiDanhMuc getLoaiDanhMucCurrent() {
		return loaiDanhMucCurrent;
	}

	public void setLoaiDanhMucCurrent(LoaiDanhMuc loaiDanhMucCurrent) {
		this.loaiDanhMucCurrent = loaiDanhMucCurrent;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BUTTON_CANCEL_ID.equals(button.getId())){
				UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucView.VIEW_NAME);
			}else{
				if(Constants.ACTION_EDIT.equals(action)){
					Notification notf = new Notification("Thông báo", "Cập nhật thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						LoaiDanhMuc current = ((BeanItem<LoaiDanhMuc>) fieldGroup.getItemDataSource()).getBean();
						notf.show(Page.getCurrent());
						loaiDanhMucService.save(current);
						UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucView.VIEW_NAME);
						
					}
				}else if(Constants.ACTION_ADD.equals(action)){
					Notification notf = new Notification("Thông báo", "Thêm mới thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						LoaiDanhMuc current = ((BeanItem<LoaiDanhMuc>) fieldGroup.getItemDataSource()).getBean();
						notf.show(Page.getCurrent());
						loaiDanhMucService.save(current);
						UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucView.VIEW_NAME);
						
					}
				}else if(Constants.ACTION_DEL.equals(action)){
					loaiDanhMucService.delete(((BeanItem<LoaiDanhMuc>) fieldGroup.getItemDataSource()).getBean());
					UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucView.VIEW_NAME);
					
					Notification notf = new Notification("Thông báo", "Xóa thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
				}else if(Constants.ACTION_APPROVE.equals(action)){
					Notification notf = new Notification("Thông báo", loaiDanhMucWf.getPheDuyetTen()+" thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						LoaiDanhMuc current = loaiDanhMucWf.pheDuyet();
						notf.show(Page.getCurrent());
						loaiDanhMucService.save(current);
						UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucView.VIEW_NAME);
					}
				}else if(Constants.ACTION_REFUSE.equals(action)){
					Notification notf = new Notification("Thông báo", loaiDanhMucWf.getTuChoiTen()+" thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						LoaiDanhMuc current = loaiDanhMucWf.tuChoi();
						notf.show(Page.getCurrent());
						loaiDanhMucService.save(current);
						UI.getCurrent().getNavigator().navigateTo(LoaiDanhMucView.VIEW_NAME);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        
	}
	
	public boolean checkTrungMa(String ma, String act){
		try {
			if(ma == null || ma.isEmpty()){
				return false;
			}
			if(Constants.ACTION_ADD.equals(act)){
				List<LoaiDanhMuc> list = loaiDanhMucService.findByCode(ma);
				if(list == null || list.isEmpty()){
					return true;
				}
			}else if(!Constants.ACTION_DEL.equals(act)){
				List<LoaiDanhMuc> list = loaiDanhMucService.findByCode(ma);
				if(list == null || list.size() <= 1){
					return true;
				}
			}else if(Constants.ACTION_DEL.equals(act)){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
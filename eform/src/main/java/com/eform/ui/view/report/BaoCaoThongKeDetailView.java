package com.eform.ui.view.report;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.task.TaskDefinition;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.TemporaryFileDownloadResource;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.ChartUtils;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.SqlJsonBuilder;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.BaoCaoThongKeDonViDao;
import com.eform.dao.HtQuyenServices;
import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.BaoCaoThongKeDonVi;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BaoCaoThongKeService;
import com.eform.service.BieuMauService;
import com.eform.service.DeploymentHtDonViService;
import com.eform.service.HoSoService;
import com.eform.service.HtDonViServices;
import com.eform.service.TruongThongTinService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.eform.ui.view.report.type.FilterObject;
import com.eform.ui.view.report.type.FunctionObject;
import com.eform.ui.view.report.type.GroupObject;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.ColumnHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SpringView(name = BaoCaoThongKeDetailView.VIEW_NAME)
public class BaoCaoThongKeDetailView extends VerticalLayout implements View, ClickListener {

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "chi-tiet-bctk";
	private static final String MAIN_TITLE = "Thông tin báo cáo thống kê";
	private static final String SMALL_TITLE = "";

	private final int WIDTH_FULL = 100;
	private static final String BUTTON_CREATE_REPORT_ID = "btn-create-report";
	private static final String BUTTON_DOWNLOAD_EXCEL_ID = "btn-download-excel";
	private static final String BUTTON_CREATE_CHART_ID = "btn-create-chart";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_ADD_FILTER_ID = "btn-add-filter";
	private static final String BUTTON_ADD_FN_ID = "btn-add-fn";
	private static final String BUTTON_ADD_GROUP_ID = "btn-add-group";
	private static final String BUTTON_REMOVE_FILTER_ID = "btn-remove-filter";
	private static final String BUTTON_REMOVE_FN_ID = "btn-remove-fn";
	private static final String BUTTON_REMOVE_GROUP_ID = "btn-remove-group";
	private static final String BUTTON_SHOW_FILTER_POPUP_ID = "btn-show-filter-popup";
	private static final String BUTTON_SHOW_GROUP_POPUP_ID = "btn-show-group-popup";
	private static final String BUTTON_SHOW_FN_POPUP_ID = "btn-show-fn-popup";
	private static final String BUTTON_UPDATE_ID = "btn-update";
	private static final String BUTTON_UPDATE_POPUP_ID = "btn-update-popup";
	private static final String BUTTON_DEL_ID = "btn-del";
	

	private WorkflowManagement<BaoCaoThongKe> baoCaoThongKeWM;
	private Workflow<BaoCaoThongKe> baoCaoThongKeWf;;

	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private TruongThongTinService truongThongTinService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private BieuMauService bieuMauService;
	@Autowired
	private BaoCaoThongKeService baoCaoThongKeService;
	@Autowired
	private HoSoService hoSoService;

	private CssLayout mainTitleWrap;
	private String action;
	private BaoCaoThongKe baoCaoThongKeCurrent;

	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();

	private final PropertysetItem filterItemValue = new PropertysetItem();
	private final FieldGroup filterFieldGroup = new FieldGroup();

	private final PropertysetItem groupItemValue = new PropertysetItem();
	private final FieldGroup groupFieldGroup = new FieldGroup();
	

	private final PropertysetItem functionItemValue = new PropertysetItem();
	private final FieldGroup functionFieldGroup = new FieldGroup();
	
	private final PropertysetItem saveItemValue = new PropertysetItem();
	private final FieldGroup saveFieldGroup = new FieldGroup();
	
	private BaoCaoThongKeDetailView currentView = this;

	private BeanItemContainer<FilterObject> filterContainer;
	private BeanItemContainer<GroupObject> groupContainer;
	private BeanItemContainer<FunctionObject> functionContainer;
	private BeanItemContainer<ComboboxItem> truongThongTinContainer; //combobox truong thong tin popup

	private Window groupWindow;
	private Window functionWindow;
	private Window filterWindow;
	private Window saveWindow;
	private Window htmlTableWindow;
	private Label contentHtml;
	Button btnDownloadExcel;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private DeploymentHtDonViService deploymentHtDonViService;
	@Autowired
	private HtDonViServices htDonViServices;
	private Set<String> deploymentIds;

	private Map<String, TruongThongTin> truongThongTinKeyMap;
	private HtDonVi htDonViCurrent;
	
	private TreeTable donViTreeTable;
	@Autowired
	private HtQuyenServices htQuyenServices;
	@Autowired
	private BaoCaoThongKeDonViDao baoCaoThongKeDonViDao;
	private Map<HtDonVi,Boolean> donViSelected;
	
	
	@PostConstruct
	void init() {
			
		baoCaoThongKeWM = new WorkflowManagement<BaoCaoThongKe>(EChucNang.E_FORM_BAO_CAO_THONG_KE.getMa());
		fieldGroup.setItemDataSource(itemValue);
		filterFieldGroup.setItemDataSource(filterItemValue);
		groupFieldGroup.setItemDataSource(groupItemValue);
		functionFieldGroup.setItemDataSource(functionItemValue);
		saveFieldGroup.setItemDataSource(saveItemValue);
		truongThongTinContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, new ArrayList());
		btnDownloadExcel = new Button();
		initMainTitle();
    	donViSelected = new HashMap();
		
		String userId = CommonUtils.getUserLogedIn();
		String maDonVi = identityService.getUserInfo(userId, "HT_DON_VI");
		deploymentIds = new HashSet<String>();
		if(maDonVi != null){
			List<HtDonVi> dvList = htDonViServices.getHtDonViFind(maDonVi,null, null, null, 0, 1);
			if(!dvList.isEmpty()){
				htDonViCurrent = dvList.get(0);
				List<DeploymentHtDonVi> deploymentHtDonViList = deploymentHtDonViService.getDeploymentHtDonViFind(null, dvList.get(0));
				if(deploymentHtDonViList != null && !deploymentHtDonViList.isEmpty()){
					for(DeploymentHtDonVi item : deploymentHtDonViList){
						deploymentIds.add(item.getDeploymentId());
					}
				}
			}
		}
		
	}

	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				if(Constants.ACTION_ADD.equals(action)){
					baoCaoThongKeCurrent = new BaoCaoThongKe();
					
				}else{
					String ma = m.group(3);
					List<BaoCaoThongKe> baoCaoThongKeList = baoCaoThongKeService.findByCode(ma);
					if(baoCaoThongKeList != null && !baoCaoThongKeList.isEmpty()){
						baoCaoThongKeCurrent = baoCaoThongKeList.get(0);
					}else{
						baoCaoThongKeCurrent = new BaoCaoThongKe();
					}
				}
				baoCaoThongKeWf = baoCaoThongKeWM.getWorkflow(baoCaoThongKeCurrent);
				initFilterWindow();
				initFunctionWindow();
				initGroupWindow();
				initSaveWindow();
				initHtmlViewer();
				initDetailForm();
				
			}
		}
	}
	
	public void initFilterWindow(){
    	FormLayout form = new FormLayout();
    	form.setWidth("100%");
    	form.setMargin(true);
    	form.setSpacing(true);
    	
    	ComboBox truongThongTinCbb = new ComboBox("Trường thông tin", truongThongTinContainer);
    	truongThongTinCbb.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	truongThongTinCbb.setItemCaptionPropertyId("text");
    	truongThongTinCbb.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	truongThongTinCbb.setRequired(true);
    	truongThongTinCbb.setRequiredError("Không được để trống");
    	form.addComponent(truongThongTinCbb);
    	filterItemValue.addItemProperty("truongThongTin", new ObjectProperty<ComboboxItem>(null, ComboboxItem.class));
		filterFieldGroup.bind(truongThongTinCbb, "truongThongTin");
    	
    	
    	ComboBox comparisonCbb = new ComboBox("Toán tử");
    	comparisonCbb.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	comparisonCbb.setRequired(true);
    	comparisonCbb.setRequiredError("Không được để trống");
    	form.addComponent(comparisonCbb);
    	filterItemValue.addItemProperty("comparison", new ObjectProperty<Long>(null, Long.class));
		filterFieldGroup.bind(comparisonCbb, "comparison");
    	
    	TextField valueTxt = new TextField("Giá trị");
    	valueTxt.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	valueTxt.setNullRepresentation("");
    	form.addComponent(valueTxt);
    	filterItemValue.addItemProperty("value", new ObjectProperty<Object>(null, Object.class));
		filterFieldGroup.bind(valueTxt, "value");
		valueTxt.setVisible(true);
		
		PopupDateField dateField = new PopupDateField("Giá trị");
		
		dateField.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		
    	form.addComponent(dateField);
    	filterItemValue.addItemProperty("date", new ObjectProperty<Date>(null, Date.class));
		filterFieldGroup.bind(dateField, "date");
		dateField.setVisible(false);
		
		
		truongThongTinCbb.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				ComboboxItem current = (ComboboxItem) event.getProperty().getValue(); 
				comparisonCbb.removeAllItems();
				if(current != null){
					TruongThongTin truongTT = truongThongTinKeyMap.get(current.getValue());
					if(truongTT != null && truongTT.getKieuDuLieu() != null){
						if(truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
							valueTxt.setVisible(false);
							dateField.setVisible(true);
							dateField.setRequired(true);
							dateField.setRequiredError("Không được để trống");
					    	valueTxt.setRequired(false);
					    	for (CommonUtils.EComparison item : CommonUtils.EComparison.values()) {
								if((4&item.getType()) == 4){
									comparisonCbb.addItem(new Long(item.getCode()));
						    		comparisonCbb.setItemCaption(new Long(item.getCode()), item.getName());
								}
							}
						}else{
							dateField.setRequired(false);
							valueTxt.setVisible(true);
							dateField.setVisible(false);
					    	valueTxt.setRequired(true);
					    	valueTxt.setRequiredError("Không được để trống");
							if(truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa()){
								valueTxt.setConverter(Integer.class);
								for (CommonUtils.EComparison item : CommonUtils.EComparison.values()) {
									if((2&item.getType()) == 2){
										comparisonCbb.addItem(new Long(item.getCode()));
							    		comparisonCbb.setItemCaption(new Long(item.getCode()), item.getName());
									}
								}
							}else if(truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.NUMBER.getMa()){
								valueTxt.setConverter(BigDecimal.class);
								for (CommonUtils.EComparison item : CommonUtils.EComparison.values()) {
									if((2&item.getType()) == 2){
										comparisonCbb.addItem(new Long(item.getCode()));
							    		comparisonCbb.setItemCaption(new Long(item.getCode()), item.getName());
									}
								}
							}else{
								valueTxt.setConverter(String.class);
								for (CommonUtils.EComparison item : CommonUtils.EComparison.values()) {
									if((8&item.getType()) == 8){
										comparisonCbb.addItem(new Long(item.getCode()));
							    		comparisonCbb.setItemCaption(new Long(item.getCode()), item.getName());
									}
								}
							}
						}
					}
				}
			}
		});
		
		Button addFilter = new Button("Thêm");
		addFilter.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
		addFilter.addClickListener(this);
		addFilter.setId(BUTTON_ADD_FILTER_ID);
		form.addComponent(addFilter);
    	
    	filterWindow = new Window("Điều kiện lọc");
    	filterWindow.setWidth(600, Unit.PIXELS);
    	filterWindow.setHeight(300, Unit.PIXELS);
    	filterWindow.setResizable(true);
    	filterWindow.center();
    	filterWindow.setModal(true);
    	filterWindow.setContent(form);
    }
	
	public void initDonViTreeTable(){
		donViTreeTable = new TreeTable("Gửi tới đơn vị");
		
		donViTreeTable.setWidth("100%");
		
		donViTreeTable.addContainerProperty("ma", CheckBox.class, null);
    	donViTreeTable.addContainerProperty("ten", String.class, null);
    	donViTreeTable.setColumnHeader("ma", "Mã");
    	donViTreeTable.setColumnHeader("ten", "Tên");
		
    	donViTreeTable.setVisibleColumns("ma", "ten");
		
    	//List<HtDonVi> htDonViList = getHtDonViCon(htDonViCurrent);
    	List<HtDonVi> htDonViList = getHtDonViCon(null);
		
    	if(baoCaoThongKeCurrent.getId() != null){
    		List<BaoCaoThongKeDonVi> baoCaoThongKeDonViList = baoCaoThongKeDonViDao.findByBaoCaoThongKe(baoCaoThongKeCurrent);
    		
    		if(baoCaoThongKeDonViList != null){
    			
    			for(BaoCaoThongKeDonVi bctkdv : baoCaoThongKeDonViList){
    				donViSelected.put(bctkdv.getHtDonVi(), true);
    			}
    		}
    		
    	}
		
		
		
		for(HtDonVi donVi:htDonViList){
			Boolean check=donViSelected.get(donVi);
			CheckBox checkBox =new CheckBox();
			checkBox.setCaption(donVi.getMa());
			checkBox.setValue(check!=null&&check);
			
			checkBox.addValueChangeListener(new ValueChangeListener(){
				@Override
				public void valueChange(ValueChangeEvent event) {
					Boolean check=(Boolean) event.getProperty().getValue();
					if(check!=null&&check){
						donViSelected.put(donVi,true);
					}else{
						donViSelected.put(donVi,false);
					}
				}
			});
			donViTreeTable.addItem(new Object[]{checkBox, donVi.getTen()},donVi);
			donViTreeTable.setChildrenAllowed(donVi, false);
			
		}
		for(HtDonVi donVi:htDonViList){
			if(donVi.getHtDonVi()!=null){
				donViTreeTable.setChildrenAllowed(donVi.getHtDonVi(), true);
				donViTreeTable.setParent(donVi, donVi.getHtDonVi());
			}
		}
		
	}
	
	
	public List<HtDonVi> getHtDonViCon(HtDonVi htDonVi){
		List<HtDonVi> htDonViList = new ArrayList();
		if(htDonVi != null){
			List<HtDonVi> htDonViAll = htDonViServices.getHtDonViFind(null,null, null, null, -1, -1);
			if(htDonViAll != null && !htDonViAll.isEmpty()){
				List<HtDonVi> process = new ArrayList();
				process.add(htDonVi);
				while(!process.isEmpty()){
					HtDonVi current = process.remove(0);
					for(HtDonVi dv : htDonViAll){
						if(dv != null && dv.getHtDonVi() != null && dv.getHtDonVi().equals(current)){
							process.add(dv);
							htDonViList.add(dv);
						}
					}
				}
			}
			
		}else{
			htDonViList = htDonViServices.getHtDonViFind(null,null, null, null, -1, -1);
		}
		return htDonViList;
	}
	
	public void initGroupWindow(){
    	FormLayout form = new FormLayout();
    	form.setWidth("100%");
    	form.setMargin(true);
    	form.setSpacing(true);
    	
    	ComboBox truongThongTinCbb = new ComboBox("Trường thông tin", truongThongTinContainer);
    	truongThongTinCbb.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	truongThongTinCbb.setItemCaptionPropertyId("text");
    	truongThongTinCbb.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	truongThongTinCbb.setRequired(true);
    	truongThongTinCbb.setRequiredError("Không được để trống");
    	form.addComponent(truongThongTinCbb);
    	groupItemValue.addItemProperty("truongThongTin", new ObjectProperty<ComboboxItem>(null, ComboboxItem.class));
		groupFieldGroup.bind(truongThongTinCbb, "truongThongTin");
    	
    	TextField txtAliasName = new TextField("Tên hiển thị");
    	txtAliasName.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	txtAliasName.setNullRepresentation("");
    	txtAliasName.setRequired(true);
    	txtAliasName.setRequiredError("Không được để trống");
    	form.addComponent(txtAliasName);
    	groupItemValue.addItemProperty("alias", new ObjectProperty<String>(null, String.class));
    	groupFieldGroup.bind(txtAliasName, "alias");
    	
    	truongThongTinCbb.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				ComboboxItem obj = (ComboboxItem) event.getProperty().getValue();
				if(obj != null && obj.getText() != null){
					txtAliasName.setValue(obj.getText());
				}
			}
		});

		Button addGroup = new Button("Thêm");
		addGroup.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
		addGroup.addClickListener(this);
		addGroup.setId(BUTTON_ADD_GROUP_ID);
		form.addComponent(addGroup);
    	
    	groupWindow = new Window("Nhóm");
    	groupWindow.setWidth(600, Unit.PIXELS);
    	groupWindow.setHeight(200, Unit.PIXELS);
    	groupWindow.setResizable(true);
    	groupWindow.center();
    	groupWindow.setModal(true);
    	groupWindow.setContent(form);
    }
	
	public void initFunctionWindow(){
    	FormLayout form = new FormLayout();
    	form.setWidth("100%");
    	form.setMargin(true);
    	form.setSpacing(true);
    	
    	ComboBox truongThongTinCbb = new ComboBox("Trường thông tin", truongThongTinContainer);
    	truongThongTinCbb.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	truongThongTinCbb.setItemCaptionPropertyId("text");
    	truongThongTinCbb.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	truongThongTinCbb.setRequired(true);
    	truongThongTinCbb.setRequiredError("Không được để trống");
    	form.addComponent(truongThongTinCbb);
    	functionItemValue.addItemProperty("truongThongTin", new ObjectProperty<ComboboxItem>(null, ComboboxItem.class));
    	functionFieldGroup.bind(truongThongTinCbb, "truongThongTin");
    	
    	ComboBox cbbFn = new ComboBox("Phương thức tính");
    	
    	cbbFn.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbFn.setRequired(true);
    	cbbFn.setRequiredError("Không được để trống");
    	form.addComponent(cbbFn);
    	functionItemValue.addItemProperty("function", new ObjectProperty<Long>(null, Long.class));
    	functionFieldGroup.bind(cbbFn, "function");
    	
    	TextField txtAliasName = new TextField("Tên hiển thị");
    	txtAliasName.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	txtAliasName.setNullRepresentation("");
    	txtAliasName.setRequired(true);
    	txtAliasName.setRequiredError("Không được để trống");
    	form.addComponent(txtAliasName);
    	functionItemValue.addItemProperty("alias", new ObjectProperty<String>(null, String.class));
    	functionFieldGroup.bind(txtAliasName, "alias");
    	
    	
    	truongThongTinCbb.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				ComboboxItem current = (ComboboxItem) event.getProperty().getValue(); 
				cbbFn.removeAllItems();
				if(current != null){
					TruongThongTin truongTT = truongThongTinKeyMap.get(current.getValue());
					if(truongTT != null && truongTT.getKieuDuLieu() != null){
						if(truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.NUMBER.getMa() 
								|| truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa()){
							for (CommonUtils.ESummaryFunction item : CommonUtils.ESummaryFunction.values()) {
					    		cbbFn.addItem(new Long(item.getCode()));
					    		cbbFn.setItemCaption(new Long(item.getCode()), item.getName());
							}
						}else{
							for (CommonUtils.ESummaryFunction item : CommonUtils.ESummaryFunction.values()) {
								if(item.getUseForString()){
									cbbFn.addItem(new Long(item.getCode()));
						    		cbbFn.setItemCaption(new Long(item.getCode()), item.getName());
								}
							}
						}
					}
				}
				
			}
    	});

		Button addGroup = new Button("Thêm");
		addGroup.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
		addGroup.addClickListener(this);
		addGroup.setId(BUTTON_ADD_FN_ID);
		form.addComponent(addGroup);
    	
		functionWindow = new Window("Phương thức tính");
		functionWindow.setWidth(600, Unit.PIXELS);
		functionWindow.setHeight(250, Unit.PIXELS);
		functionWindow.setResizable(true);
		functionWindow.center();
		functionWindow.setModal(true);
		functionWindow.setContent(form);
    }
	
	public void initHtmlViewer(){
		VerticalLayout content = new VerticalLayout();
		content.setWidth("100%");
    	content.setSpacing(true);
    	content.setMargin(true);
		
    	HorizontalLayout htmlContainer = new HorizontalLayout();
    	htmlContainer.setWidth("100%");
    	
    	contentHtml = new Label("");
    	contentHtml.setContentMode(ContentMode.HTML);
    	contentHtml.setWidth("100%");
    	contentHtml.setStyleName(ExplorerLayout.HTML_DATA_LBL);
    	htmlContainer.addComponent(contentHtml);
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	buttons.addStyleName(ExplorerLayout.BUTTONS_CENTER);
    	
		
		btnDownloadExcel.setCaption("Tải excel");
		btnDownloadExcel.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnDownloadExcel.setId(BUTTON_DOWNLOAD_EXCEL_ID);
		btnDownloadExcel.addClickListener(this);
		buttons.addComponent(btnDownloadExcel);
		
		content.addComponent(htmlContainer);
		content.addComponent(buttons);
    	
    	htmlTableWindow = new Window("Báo cáo thống kê");
    	htmlTableWindow.setWidth(900, Unit.PIXELS);
    	htmlTableWindow.setHeight(500, Unit.PIXELS);
    	htmlTableWindow.setResizable(true);
    	htmlTableWindow.center();
    	htmlTableWindow.setModal(true);
    	htmlTableWindow.setContent(content);
    }
	
	public void initMainTitle(){
		PageBreadcrumbInfo home = new PageBreadcrumbInfo();
    	home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
    	home.setViewName(DashboardView.VIEW_NAME);
    	
    	PageBreadcrumbInfo current = new PageBreadcrumbInfo();
    	current.setTitle(MAIN_TITLE);
    	current.setViewName(VIEW_NAME);		
    			
    	addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
	
	public void initSaveWindow(){
    	saveFieldGroup.setItemDataSource(saveItemValue);
    	FormLayout form = new FormLayout();
    	form.setWidth("100%");
    	form.setMargin(true);
    	form.setSpacing(true);
    	
    	TextField txtMa = new TextField("Mã báo cáo thống kê");
    	txtMa.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	txtMa.setNullRepresentation("");
    	form.addComponent(txtMa);
    	txtMa.setRequired(true);
    	txtMa.setRequiredError("Không được để trống");
    	txtMa.addValidator(new StringLengthValidator("Số ký tự tối đa là 20", 1, 20, false));   
    	txtMa.addValidator(new RegexpValidator("[0-9a-zA-Z_]+", "Chỉ chấp nhận các ký tự 0-9 a-z A-Z _"));
    	String maValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			maValue = baoCaoThongKeCurrent.getMa();
		}else{
			txtMa.addTextChangeListener(new TextChangeListener() {
				private static final long serialVersionUID = 1L;

				@Override
				public void textChange(TextChangeEvent event) {

					if(!checkTrungMa(event.getText(), action)){
						txtMa.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
					}else{
						txtMa.setComponentError(null);
					}
					
				}

				
	    	});
		}
    	saveItemValue.addItemProperty("ma", new ObjectProperty<String>(maValue, String.class));
    	saveFieldGroup.bind(txtMa, "ma");
    	txtMa.setReadOnly(!Constants.ACTION_ADD.equals(action));
    	
    	
    	
    	TextField txtTen = new TextField("Tên báo cáo thống kê");
    	txtTen.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	txtTen.setNullRepresentation("");
    	form.addComponent(txtTen);
    	txtTen.setRequired(true);
    	txtTen.setRequiredError("Không được để trống");
    	String tenValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			tenValue = baoCaoThongKeCurrent.getTen();
		}
    	saveItemValue.addItemProperty("ten", new ObjectProperty<String>(tenValue, String.class));
    	saveFieldGroup.bind(txtTen, "ten");
    	
    	String lbl = "Lưu";
    	if(Constants.ACTION_APPROVE.equals(action)){
			lbl = baoCaoThongKeWf.getPheDuyetTen();
		}else if(Constants.ACTION_REFUSE.equals(action)){
			lbl = baoCaoThongKeWf.getTuChoiTen();
		}
    	
    	Button btnSave = new Button(lbl);
    	btnSave.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
    	btnSave.addClickListener(this);
    	btnSave.setId(BUTTON_UPDATE_POPUP_ID);
    	form.addComponent(btnSave);
    	
    	
    	saveWindow = new Window(lbl);
    	saveWindow.setWidth(500, Unit.PIXELS);
    	saveWindow.setHeight(210, Unit.PIXELS);
    	saveWindow.setResizable(true);
    	saveWindow.center();
    	saveWindow.setModal(true);
    	saveWindow.setContent(form);
    }
	 public boolean checkTrungMa(String ma, String act){
			try {
				if(ma == null || ma.isEmpty()){
					return false;
				}
				if(Constants.ACTION_ADD.equals(act) || Constants.ACTION_COPY.equals(act)){
					List<BaoCaoThongKe> list = baoCaoThongKeService.findByCode(ma);
					if(list == null || list.isEmpty()){
						return true;
					}
				}else if(!Constants.ACTION_DEL.equals(act)){
					List<BaoCaoThongKe> list = baoCaoThongKeService.findByCode(ma);
					if(list == null || list.size() <= 1){
						return true;
					}
				}else if(Constants.ACTION_DEL.equals(act)){
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}
	
	public void initDetailForm(){
		//Beans container
		BeanItemContainer<BieuMau> containerBm = new BeanItemContainer<BieuMau>(BieuMau.class, new ArrayList());
		filterContainer = new BeanItemContainer<FilterObject>(FilterObject.class, new ArrayList());
		groupContainer = new BeanItemContainer<GroupObject>(GroupObject.class, new ArrayList());
		functionContainer = new BeanItemContainer<FunctionObject>(FunctionObject.class, new ArrayList());
		truongThongTinKeyMap = new HashMap<String, TruongThongTin>();
		
		//buttons
    	Button btnCreateReport = new Button();
    	Button btnCreateChart = new Button();
		Button btnUpdate = new Button();
		
		CssLayout formContainer = new CssLayout();
		formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	
    	FormLayout form = new FormLayout();
    	form.setMargin(true);
    	
    	//Tieu de bao cao thong ke
    	TextField txtTieuDe = new TextField("Tiêu đề");
    	txtTieuDe.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	txtTieuDe.setNullRepresentation("");
    	txtTieuDe.setRequired(true);
    	txtTieuDe.setRequiredError("Không được để trống");
    	String tieuDeValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			tieuDeValue = baoCaoThongKeCurrent.getTieuDe();
		}
    	itemValue.addItemProperty("tieuDe", new ObjectProperty<String>(tieuDeValue, String.class));
    	fieldGroup.bind(txtTieuDe, "tieuDe");
    	txtTieuDe.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
    	
    	//Kieu do thi
    	ComboBox cbbChartTypes = new ComboBox("Kiểu đồ thị");
		for (CommonUtils.ELoaiDoThi item : CommonUtils.ELoaiDoThi.values()) {
			cbbChartTypes.addItem(item.getCode());
			cbbChartTypes.setItemCaption(item.getCode(), item.getName());
		}
		cbbChartTypes.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		cbbChartTypes.setVisible(false);
		String chartTypeValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			chartTypeValue = baoCaoThongKeCurrent.getKieuDoThi();
		}
		itemValue.addItemProperty("chartType", new ObjectProperty<String>(chartTypeValue, String.class));
		fieldGroup.bind(cbbChartTypes, "chartType");
		cbbChartTypes.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
		
		//Kieu bao cao thong ke
		ComboBox cbbKieuReport = new ComboBox("Kiểu báo cáo");
		for (CommonUtils.EKieuReport item : CommonUtils.EKieuReport.values()) {
			cbbKieuReport.addItem(new Long(item.getCode()));
			cbbKieuReport.setItemCaption(new Long(item.getCode()), item.getName());
		}
		cbbKieuReport.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		cbbKieuReport.setRequired(true);
		cbbKieuReport.setRequiredError("Không được để trống");
		cbbKieuReport.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				Long selected = (Long) event.getProperty().getValue();
				if(selected != null && selected.intValue() == CommonUtils.EKieuReport.CHART.getCode()){
					cbbChartTypes.setVisible(true);
					cbbChartTypes.setRequired(true);
					cbbChartTypes.setRequiredError("Không được để trống");
					btnCreateChart.setVisible(true);
					btnCreateReport.setVisible(false);
				}else{
					cbbChartTypes.setVisible(false);
					cbbChartTypes.setRequired(false);
					btnCreateChart.setVisible(false);
					btnCreateReport.setVisible(true);
				}
			}
		});
		Long kieuReportValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			kieuReportValue = baoCaoThongKeCurrent.getKieuBaoCao();
		}
		itemValue.addItemProperty("kieuReport", new ObjectProperty<Long>(kieuReportValue, Long.class));
		fieldGroup.bind(cbbKieuReport, "kieuReport");
		cbbKieuReport.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
	
		//Process definition
		ProcessDefinitionQuery processQuery = repositoryService.createProcessDefinitionQuery();
		if(deploymentIds!=null&& !deploymentIds.isEmpty()){
			processQuery = processQuery.deploymentIds(deploymentIds);
		}
		List<ProcessDefinition> processDefinitionList = processQuery.orderByProcessDefinitionName().asc().list();
		BeanItemContainer<ProcessDefinition> container = new BeanItemContainer<ProcessDefinition>(ProcessDefinition.class, processDefinitionList);
    	ComboBox cbbQuyTrinh = new ComboBox("Quy trình", container);
    	cbbQuyTrinh.setRequired(true);
    	cbbQuyTrinh.setRequiredError("Không được để trống");
    	cbbQuyTrinh.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbQuyTrinh.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbQuyTrinh.setItemCaptionPropertyId("name");
    	cbbQuyTrinh.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -7320899139765211724L;
			@Override
			public void valueChange(ValueChangeEvent event) {
				ProcessDefinition selected = (ProcessDefinition) event.getProperty().getValue();
				List<BieuMau> bmList = new ArrayList<BieuMau>();
				if(selected != null){
					ProcessDefinitionEntity processDefEntity = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService).getDeployedProcessDefinition(selected.getId());
					List<ActivityImpl> activities = processDefEntity.getActivities();
					List<String> fomrKeyList = new ArrayList<String>();
					for(ActivityImpl act : activities){
						Object obj = act.getProperty("taskDefinition");
						if(obj != null && obj instanceof TaskDefinition){
							TaskDefinition taskDef = (TaskDefinition) obj;
							if(taskDef.getFormKeyExpression() != null){
								String formKey = taskDef.getFormKeyExpression().getExpressionText();
								if(formKey != null && !formKey.isEmpty()){
									Pattern p = Pattern.compile("(.+)#(.+)");
									Matcher m = p.matcher(formKey);
									if (m.matches()) {
										formKey = m.group(1);
									}
									fomrKeyList.add(formKey);
								}
							}
						}
					}
					bmList = bieuMauService.findByCode(fomrKeyList);
				}
				containerBm.removeAllItems();
				containerBm.addAll(bmList);
			}
		});
    	
    	ProcessDefinition processValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			String processIdTmp = baoCaoThongKeCurrent.getProcessDefId();
			if(processIdTmp != null){
				if(processDefinitionList != null){
					for(ProcessDefinition pd : processDefinitionList){
						if(pd != null && processIdTmp.equals(pd.getId())){
							processValue = pd;
							break;
						}
					}
				}
			}
		}
    	itemValue.addItemProperty("process", new ObjectProperty<ProcessDefinition>(processValue, ProcessDefinition.class));
		fieldGroup.bind(cbbQuyTrinh, "process");
		cbbQuyTrinh.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
		
		//Bieu mau
		ComboBox cbbBieuMau = new ComboBox("Biểu mẫu", containerBm);
		cbbBieuMau.setRequired(true);
    	cbbBieuMau.setRequiredError("Không được để trống");
    	cbbBieuMau.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbBieuMau.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbBieuMau.setItemCaptionPropertyId("ten");
    	List<ComboboxItem> truongThongTinList = new ArrayList<ComboboxItem>();
    	cbbBieuMau.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				BieuMau selected = (BieuMau) event.getProperty().getValue();
				truongThongTinList.clear();
				if(selected != null){
					Map<String, List<TruongThongTin>> truongThongTinMap = ChartUtils.getTruongThongTin(selected, bieuMauService);
					if(truongThongTinMap != null && !truongThongTinMap.isEmpty()){
						for(Map.Entry<String, List<TruongThongTin>> entry : truongThongTinMap.entrySet()){
							String key = entry.getKey() != "!" ? entry.getKey() +"," : "";
							if(entry.getValue() != null){
								for(TruongThongTin truongThongTin : entry.getValue()){
									String ten = key+truongThongTin.getTen();
									String ma = key+truongThongTin.getMa();
									ComboboxItem item = new ComboboxItem(ten, ma);
									truongThongTinList.add(item);
									truongThongTinKeyMap.put(ma, truongThongTin);
								}
							}
						}
					}
				}
				truongThongTinContainer.removeAllItems();
				truongThongTinContainer.addAll(truongThongTinList);
			}
    	});
    	
    	BieuMau bmValue = null;
    	if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
    		bmValue = baoCaoThongKeCurrent.getBieuMau();
    	}
		itemValue.addItemProperty("bieuMau", new ObjectProperty<BieuMau>(bmValue, BieuMau.class));
		fieldGroup.bind(cbbBieuMau, "bieuMau");
		cbbBieuMau.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
		
		//Group
		Table tblGroup = new Table("Nhóm");
		tblGroup.addStyleName(ExplorerLayout.DATA_TABLE);
		tblGroup.setContainerDataSource(groupContainer);
//		tblGroup.setColumnHeaderMode(ColumnHeaderMode.HIDDEN);
		tblGroup.setWidth("100%");
		tblGroup.addGeneratedColumn("ten", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				GroupObject current = (GroupObject) itemId;
				if(current != null && current.getTruongThongTin() != null){
					return current.getTruongThongTin().getTen();
				}
				return "";
			}
		});
		
		tblGroup.addGeneratedColumn("alias", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				GroupObject current = (GroupObject) itemId;
				if(current != null && current.getAliasName() != null){
					return current.getAliasName();
				}
				return "";
			}
		});
		
		tblGroup.setColumnWidth("action", 60);
		tblGroup.addGeneratedColumn("action", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				GroupObject current = (GroupObject) itemId;
				Button buttonRemoveGroup = new Button();
				buttonRemoveGroup.setId(BUTTON_REMOVE_GROUP_ID);
				buttonRemoveGroup.addClickListener(currentView);
				buttonRemoveGroup.setIcon(FontAwesome.REMOVE);
				buttonRemoveGroup.setStyleName(ExplorerLayout.BTN_REMOVE_TBL_RPT);
				buttonRemoveGroup.setData(current);
				buttonRemoveGroup.addStyleName(ExplorerLayout.REMOVE_BTN_ACT);
				return buttonRemoveGroup;
			}
		});
		
		tblGroup.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;
			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if("action".equals(propertyId)){
                    return "center-aligned";
                }
				return "left-aligned";
			}
		});
		
		tblGroup.setColumnHeader("ten", "Trường thông tin");
		tblGroup.setColumnHeader("alias", "Tên hiển thị");
		tblGroup.setColumnHeader("action", "");
		if(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action)){
			tblGroup.setVisibleColumns("ten", "alias");
		}else{
			tblGroup.setVisibleColumns("ten", "alias", "action");
		}
		
		//Truong thong tin gia tri
		Table tblFunction = new Table("Phương thức tính");
		tblFunction.setContainerDataSource(functionContainer);
		tblFunction.addStyleName(ExplorerLayout.DATA_TABLE);
//		tblFunction.setColumnHeaderMode(ColumnHeaderMode.HIDDEN);
		tblFunction.setWidth("100%");
		tblFunction.addGeneratedColumn("function", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				FunctionObject current = (FunctionObject) itemId;
				if(current != null && current.getFunction() != null){
					CommonUtils.ESummaryFunction fn = CommonUtils.ESummaryFunction.getESummaryFunction(current.getFunction());
					if(fn != null){
						return fn.getName();
					}
				}
				return "";
			}
		});
		tblFunction.addGeneratedColumn("ten", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				FunctionObject current = (FunctionObject) itemId;
				if(current != null && current.getTruongThongTin() != null){
					return current.getTruongThongTin().getTen();
				}
				return "";
			}
		});
		tblFunction.addGeneratedColumn("alias", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				FunctionObject current = (FunctionObject) itemId;
				if(current != null && current.getAliasName() != null){
					return current.getAliasName();
				}
				return "";
			}
		});
		tblFunction.setColumnWidth("action", 60);
		tblFunction.addGeneratedColumn("action", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				FunctionObject current = (FunctionObject) itemId;
				Button buttonRemoveFunction = new Button();
				buttonRemoveFunction.setId(BUTTON_REMOVE_FN_ID);
				buttonRemoveFunction.addClickListener(currentView);
				buttonRemoveFunction.setIcon(FontAwesome.REMOVE);
				buttonRemoveFunction.setStyleName(ExplorerLayout.BTN_REMOVE_TBL_RPT);
				buttonRemoveFunction.setData(current);
				buttonRemoveFunction.addStyleName(ExplorerLayout.REMOVE_BTN_ACT);
				return buttonRemoveFunction;
			}
		});

		
		tblFunction.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;
			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if("action".equals(propertyId)){
                    return "center-aligned";
                }
				return "left-aligned";
			}
		});
		tblFunction.setColumnHeader("function", "Phương thức");
		tblFunction.setColumnHeader("ten", "Trường thông tin");
		tblFunction.setColumnHeader("alias", "Tên hiển thị");
		tblFunction.setColumnHeader("action", "");
		tblFunction.setVisibleColumns("function", "ten", "alias", "action");
		if(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action)){
			tblFunction.setVisibleColumns("function", "ten", "alias");
		}else{
			tblFunction.setVisibleColumns("function", "ten", "alias", "action");
		}
		
		//Table filter
		Table filterTable = new Table("Lọc giá trị");
		filterTable.setContainerDataSource(filterContainer);
		filterTable.setColumnHeaderMode(ColumnHeaderMode.HIDDEN);
		filterTable.addStyleName(ExplorerLayout.DATA_TABLE);
		filterTable.setWidth("100%");
		filterTable.addGeneratedColumn("comparison", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				FilterObject current = (FilterObject) itemId;
				CommonUtils.EComparison toanTu = CommonUtils.EComparison.getComparison(current.getComparison());
				if(toanTu != null){
					return toanTu.getName();
				}
				return null;
			}
		});
		filterTable.addGeneratedColumn("value", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				FilterObject current = (FilterObject) itemId;
				
				if(current != null && current.getValue() != null && current.getValue() instanceof java.util.Date){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
					java.util.Date date = (Date) current.getValue();
					return df.format(date);
				}else if(current != null && current.getValue() != null){
					return current.getValue();
				}
				return null;
			}
		});
		filterTable.setColumnWidth("action", 60);
		filterTable.addGeneratedColumn("action", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				FilterObject current = (FilterObject) itemId;
				Button buttonRemoveFilter = new Button();
				buttonRemoveFilter.setId(BUTTON_REMOVE_FILTER_ID);
				buttonRemoveFilter.addClickListener(currentView);
				buttonRemoveFilter.setIcon(FontAwesome.REMOVE);
				buttonRemoveFilter.setStyleName(ExplorerLayout.BTN_REMOVE_TBL_RPT);
				buttonRemoveFilter.setData(current);
				buttonRemoveFilter.addStyleName(ExplorerLayout.REMOVE_BTN_ACT);
				return buttonRemoveFilter;
			}
		});
		
		filterTable.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;
			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if("action".equals(propertyId)){
                    return "center-aligned";
                }
				return "left-aligned";
			}
		});
		if(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action)){
			filterTable.setVisibleColumns("name", "comparison", "value");
		}else{
			filterTable.setVisibleColumns("name", "comparison", "value", "action");
		}
		
		//Lay thong tin da luu: filter, group, function
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			try {
				List<GroupObject> groupObjectList = new ArrayList<GroupObject>();
				List<FilterObject> filterObjectList = new ArrayList<FilterObject>();
				List<FunctionObject> functionObjectList = new ArrayList<FunctionObject>();
				
	    		JSONObject giaTri = baoCaoThongKeCurrent.getGiaTri();
	    		JSONArray groupArr = giaTri.getJSONArray("group");
	    		if(groupArr != null){
	    			for(int i =0 ; i< groupArr.length(); i++){
	    				JSONObject json = groupArr.getJSONObject(i);
	    				GroupObject obj= new GroupObject();
	    				obj.setAliasName(json.getString("name"));
	    				String key = json.getString("key");
	    				obj.setPath(key);
	    				String maTruongThongTin = null;
	    				if(key.contains(",")){
	    					Pattern p = Pattern.compile("(([0-9a-zA-Z-_]+),)+([0-9a-zA-Z-_]+)");
	    					Matcher m = p.matcher(key);
	    					if(m.matches()){
	    						maTruongThongTin = m.group(3);
	    					}
	    				}else{
	    					maTruongThongTin = key;
	    				}
	    				
	    				if(maTruongThongTin != null){
	    					List<TruongThongTin> list = truongThongTinService.findByCode(maTruongThongTin);
	    					if(list != null && !list.isEmpty()){
	    						obj.setTruongThongTin(list.get(0));
	    					}
	    				}
	    				groupObjectList.add(obj);
	        		}
	    		}
	    		JSONArray filterArr = giaTri.getJSONArray("filter");
	    		if(filterArr != null){
	    			for(int i =0 ; i< filterArr.length(); i++){
	    				JSONObject json = filterArr.getJSONObject(i);
	    				FilterObject obj= new FilterObject();
	    				obj.setPath(json.getString("path"));
	    				obj.setComparison(json.getLong("comparison"));
	    				Long kieuDL = json.getLong("kieudulieu");
	    				obj.setKieuDuLieu(kieuDL);
	    				obj.setName(json.getString("name"));
	    				obj.setValue(json.get("value"));
	    				
	    				filterObjectList.add(obj);
	        		}
	    		}
	    		JSONArray functionArr = giaTri.getJSONArray("function");
	    		if(functionArr != null){
	    			for(int i =0 ; i< functionArr.length(); i++){
	    				JSONObject json = functionArr.getJSONObject(i);
	    				FunctionObject obj= new FunctionObject();
	    				obj.setAliasName(json.getString("name"));
	    				obj.setFunction(json.getLong("function"));
	    				String key = json.getString("key");
	    				obj.setPath(key);
	    				String maTruongThongTin = null;
	    				if(key.contains(",")){
	    					Pattern p = Pattern.compile("(([0-9a-zA-Z-_]+),)+([0-9a-zA-Z-_]+)");
	    					Matcher m = p.matcher(key);
	    					if(m.matches()){
	    						maTruongThongTin = m.group(3);
	    					}
	    				}else{
	    					maTruongThongTin = key;
	    				}
	    				
	    				if(maTruongThongTin != null){
	    					List<TruongThongTin> list = truongThongTinService.findByCode(maTruongThongTin);
	    					if(list != null && !list.isEmpty()){
	    						obj.setTruongThongTin(list.get(0));
	    					}
	    				}
	    				functionObjectList.add(obj);
	        		}
	    		}
	    		
	    		groupContainer.addAll(groupObjectList);
	    		filterContainer.addAll(filterObjectList);
	    		functionContainer.addAll(functionObjectList);
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
		
		//Tu ngay
		PopupDateField fromDate = new PopupDateField("Từ ngày");
		fromDate.addStyleName(ExplorerLayout.FORM_CONTROL);
		fromDate.setWidth("100%");
		Date fromDateValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null && baoCaoThongKeCurrent.getTuNgay() != null){
			fromDateValue = new Date(baoCaoThongKeCurrent.getTuNgay().getTime());
    	}
		itemValue.addItemProperty("fromDate", new ObjectProperty<Date>(fromDateValue, Date.class));
		fieldGroup.bind(fromDate, "fromDate");
		fromDate.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
		//Den ngay
		PopupDateField toDate = new PopupDateField("Đến ngày");
		toDate.addStyleName(ExplorerLayout.FORM_CONTROL);
		toDate.setWidth("100%");
		Date toDateValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null && baoCaoThongKeCurrent.getDenNgay() != null){
			toDateValue = new Date(baoCaoThongKeCurrent.getDenNgay().getTime());
    	}
		itemValue.addItemProperty("toDate", new ObjectProperty<Date>(toDateValue, Date.class));
		fieldGroup.bind(toDate, "toDate");
		toDate.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
		
		form.addComponent(txtTieuDe);
    	form.addComponent(cbbKieuReport);
    	//form.addComponent(cbbReportLayout);
    	form.addComponent(cbbChartTypes);
    	form.addComponent(cbbQuyTrinh);
    	form.addComponent(cbbBieuMau);
    	
    	

    	Button btnAddGroup = new Button();
    	btnAddGroup.setDescription("Thêm group");
    	btnAddGroup.setIcon(FontAwesome.PLUS);
    	btnAddGroup.setStyleName(ExplorerLayout.BTN_ADD_RP);
    	btnAddGroup.setId(BUTTON_SHOW_GROUP_POPUP_ID);
    	btnAddGroup.addClickListener(this);

    	
    	if(!Constants.ACTION_DEL.equals(action) && !Constants.ACTION_VIEW.equals(action)){
    		form.addComponent(btnAddGroup);
    	}
    	form.addComponent(tblGroup);
    	
    	Button btnAddFunction = new Button();
    	btnAddFunction.setIcon(FontAwesome.PLUS);
    	btnAddFunction.setStyleName(ExplorerLayout.BTN_ADD_RP);
    	btnAddFunction.setDescription("Thêm phương thức tính");
    	btnAddFunction.setId(BUTTON_SHOW_FN_POPUP_ID);
    	btnAddFunction.addClickListener(this);

    	
    	if(!Constants.ACTION_DEL.equals(action) && !Constants.ACTION_VIEW.equals(action)){
    		form.addComponent(btnAddFunction);
    	}
    	form.addComponent(tblFunction);
    	
    	Button btnAddFilter = new Button();
    	btnAddFilter.setDescription("Thêm điều kiện lọc");
    	btnAddFilter.setIcon(FontAwesome.PLUS);
    	btnAddFilter.setStyleName(ExplorerLayout.BTN_ADD_RP);
    	btnAddFilter.setId(BUTTON_SHOW_FILTER_POPUP_ID);
    	btnAddFilter.addClickListener(this);

    	if(!Constants.ACTION_DEL.equals(action) && !Constants.ACTION_VIEW.equals(action)){
    		form.addComponent(btnAddFilter);
    	}
    	
    	form.addComponent(filterTable);
    	
    	form.addComponent(fromDate);
    	form.addComponent(toDate);
    	
    	initDonViTreeTable();
    	form.addComponent(donViTreeTable);
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	
    	if(!Constants.ACTION_VIEW.equals(action) && !Constants.ACTION_DEL.equals(action)){
    		btnCreateReport.setCaption("Tạo báo cáo");
    		btnCreateReport.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
    		btnCreateReport.setId(BUTTON_CREATE_REPORT_ID);
    		btnCreateReport.addClickListener(this);
    		buttons.addComponent(btnCreateReport);
    		boolean isChart = false;
    		if(kieuReportValue != null && kieuReportValue.intValue() == CommonUtils.EKieuReport.CHART.getCode()){
    			isChart = true;
    		}
    		btnCreateReport.setVisible(!isChart);
    		
    		btnCreateChart.setCaption("Tạo biểu đồ");
    		btnCreateChart.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
    		btnCreateChart.setId(BUTTON_CREATE_CHART_ID);
    		btnCreateChart.addClickListener(this);
    		buttons.addComponent(btnCreateChart);
    		btnCreateChart.setVisible(isChart);
    		
    	}
    	
    	if(!Constants.ACTION_VIEW.equals(action)){
    		btnUpdate = new Button();
    		btnUpdate.addClickListener(this);
    		buttons.addComponent(btnUpdate);
    		if(Constants.ACTION_EDIT.equals(action)){
    			btnUpdate.setCaption("Lưu");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_UPDATE_ID);
        	}else if(Constants.ACTION_ADD.equals(action) || Constants.ACTION_COPY.equals(action)){
        		btnUpdate.setCaption("Lưu");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_UPDATE_ID);
        	}else if(Constants.ACTION_DEL.equals(action)){
        		btnUpdate.setId(BUTTON_DEL_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
        		btnUpdate.setCaption("Xóa");
        	}else if(Constants.ACTION_APPROVE.equals(action)){
        		btnUpdate.setId(BUTTON_UPDATE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption(baoCaoThongKeWf.getPheDuyetTen());
        	}else if(Constants.ACTION_REFUSE.equals(action)){
        		btnUpdate.setCaption(baoCaoThongKeWf.getTuChoiTen());
        		btnUpdate.setId(BUTTON_UPDATE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_WARNING);
        	}
    	}
    	
    	Button btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
    	form.addComponent(buttons);
    	formContainer.addComponent(form);
    	addComponent(formContainer);
    	
    	CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BUTTON_CANCEL_ID.equals(button.getId())){
				UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeView.VIEW_NAME);
			}else if(BUTTON_SHOW_FILTER_POPUP_ID.equals(button.getId())){
				UI.getCurrent().addWindow(filterWindow);
			}else if(BUTTON_SHOW_FN_POPUP_ID.equals(button.getId())){
				UI.getCurrent().addWindow(functionWindow);
			}else if(BUTTON_SHOW_GROUP_POPUP_ID.equals(button.getId())){
				UI.getCurrent().addWindow(groupWindow);
			}else if(BUTTON_ADD_FILTER_ID.equals(button.getId())){
				if(filterFieldGroup.isValid()){
					filterFieldGroup.commit();
					filterWindow.close();
					Object value = filterItemValue.getItemProperty("value").getValue();
					Date valueDate = (Date) filterItemValue.getItemProperty("date").getValue();
					Long comparison = (Long) filterItemValue.getItemProperty("comparison").getValue();
					ComboboxItem truongTTPath = (ComboboxItem) filterItemValue.getItemProperty("truongThongTin").getValue();

					FilterObject filterObject = new FilterObject();
					filterObject.setName(truongTTPath.getText());
					filterObject.setPath(truongTTPath.getValue().toString());
					filterObject.setComparison(comparison);
					filterObject.setValue(value);
					TruongThongTin truongTT = truongThongTinKeyMap.get(truongTTPath.getValue().toString());
					if(truongTT != null && truongTT.getKieuDuLieu() != null){
						filterObject.setKieuDuLieu(truongTT.getKieuDuLieu());
						if( truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
							SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
							if(valueDate != null){
								filterObject.setValue(df.format(valueDate));
							}
							
						}
					}
					filterContainer.addBean(filterObject);
					filterFieldGroup.clear();
				}
			}else if(BUTTON_ADD_GROUP_ID.equals(button.getId())){
				if(groupFieldGroup.isValid()){
					groupFieldGroup.commit();
					groupWindow.close();
					String alias = (String) groupItemValue.getItemProperty("alias").getValue();
					ComboboxItem truongTTPath = (ComboboxItem) groupItemValue.getItemProperty("truongThongTin").getValue();

					GroupObject groupObject = new GroupObject();
					groupObject.setAliasName(alias);
					groupObject.setPath(truongTTPath.getValue().toString());
					TruongThongTin truongTT = truongThongTinKeyMap.get(truongTTPath.getValue().toString());
					groupObject.setTruongThongTin(truongTT);
					groupContainer.addBean(groupObject);
					groupFieldGroup.clear();
				}
			}else if(BUTTON_ADD_FN_ID.equals(button.getId())){
				if(functionFieldGroup.isValid()){
					functionFieldGroup.commit();
					functionWindow.close();
					String alias = (String) functionItemValue.getItemProperty("alias").getValue();
					Long function = (Long) functionItemValue.getItemProperty("function").getValue();
					ComboboxItem truongTTPath = (ComboboxItem) functionItemValue.getItemProperty("truongThongTin").getValue();

					FunctionObject fnObject = new FunctionObject();
					fnObject.setAliasName(alias);
					fnObject.setFunction(function);
					fnObject.setPath(truongTTPath.getValue().toString());
					TruongThongTin truongTT = truongThongTinKeyMap.get(truongTTPath.getValue().toString());
					fnObject.setTruongThongTin(truongTT);
					functionContainer.addBean(fnObject);
					functionFieldGroup.clear();
				}
			}else if(BUTTON_REMOVE_FILTER_ID.equals(button.getId())){
				FilterObject current = (FilterObject) button.getData();
				filterContainer.removeItem(current);
			}else if(BUTTON_REMOVE_FN_ID.equals(button.getId())){
				FunctionObject current = (FunctionObject) button.getData();
				functionContainer.removeItem(current);
			}else if(BUTTON_REMOVE_GROUP_ID.equals(button.getId())){
				GroupObject current = (GroupObject) button.getData();
				groupContainer.removeItem(current);
			}else if(BUTTON_UPDATE_ID.equals(button.getId())){
				if(fieldGroup.isValid()){
					UI.getCurrent().addWindow(saveWindow);
				}
			}else if(BUTTON_DEL_ID.equals(button.getId())){
				baoCaoThongKeService.delete(baoCaoThongKeCurrent);
				Notification notf = new Notification("Thông báo", "Xóa thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(UI.getCurrent().getPage());
				UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeView.VIEW_NAME);
			}else if(BUTTON_CREATE_REPORT_ID.equals(button.getId()) 
					|| BUTTON_DOWNLOAD_EXCEL_ID.equals(button.getId())
					|| BUTTON_CREATE_CHART_ID.equals(button.getId()) 
					|| BUTTON_UPDATE_POPUP_ID.equals(button.getId())){
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					Long loaiReport = (Long) itemValue.getItemProperty("kieuReport").getValue();
					//Long layout = (Long) itemValue.getItemProperty("layout").getValue();
					ProcessDefinition processDef = (ProcessDefinition) itemValue.getItemProperty("process").getValue();
					Date fromDate = (Date) itemValue.getItemProperty("fromDate").getValue();
					Date toDate = (Date) itemValue.getItemProperty("toDate").getValue();
					String chartType = (String) itemValue.getItemProperty("chartType").getValue();
					String tieuDe = (String) itemValue.getItemProperty("tieuDe").getValue();
					BieuMau bieuMau = (BieuMau) itemValue.getItemProperty("bieuMau").getValue();
					
					List<FilterObject> filterList =  filterContainer.getItemIds();
					List<GroupObject> groupList =  groupContainer.getItemIds();
					List<FunctionObject> functionList =  functionContainer.getItemIds();
					
					
					if(BUTTON_UPDATE_POPUP_ID.equals(button.getId())){
						if(saveFieldGroup.isValid()){
							saveWindow.close();
							saveFieldGroup.commit();
							String ma = (String) saveItemValue.getItemProperty("ma").getValue();
							String ten = (String) saveItemValue.getItemProperty("ten").getValue();
							
							Date date = new Date();
							baoCaoThongKeCurrent.setMa(ma);
							baoCaoThongKeCurrent.setTen(ten);
							if(Constants.ACTION_ADD.equals(action)){
								baoCaoThongKeCurrent.setNgayTao(new Timestamp(date.getTime()));
								baoCaoThongKeCurrent.setNguoiTao(CommonUtils.getUserLogedIn());
							}else{
								baoCaoThongKeCurrent.setNgayCapNhat(new Timestamp(date.getTime()));
								baoCaoThongKeCurrent.setNguoiCapNhat(CommonUtils.getUserLogedIn());
							}
							
							baoCaoThongKeCurrent.setKieuDoThi(chartType);
							//baoCaoThongKeCurrent.setLayout(layout);
							baoCaoThongKeCurrent.setKieuBaoCao(loaiReport);
							baoCaoThongKeCurrent.setBieuMau(bieuMau);
							baoCaoThongKeCurrent.setTieuDe(tieuDe);
							if(processDef != null){
								baoCaoThongKeCurrent.setProcessDefId(processDef.getId());
							}
							if(fromDate != null){
								baoCaoThongKeCurrent.setTuNgay(new Timestamp(fromDate.getTime()));
							}
							if(toDate != null){
								baoCaoThongKeCurrent.setDenNgay(new Timestamp(toDate.getTime()));
							}
							JSONObject json = new JSONObject();
							
							if(groupList != null){
								JSONArray arr = new JSONArray();
								for(GroupObject item : groupList){
									JSONObject fnJson = new JSONObject();
									fnJson.put("key", item.getPath());
									fnJson.put("name", item.getAliasName());
									arr.put(fnJson);
								}
								json.put("group", arr);
							}
							if(functionList != null){
								JSONArray arr = new JSONArray();
								for(FunctionObject fn : functionList){
									if(fn.getFunction() != null && fn.getPath() != null){
										JSONObject fnJson = new JSONObject();
										fnJson.put("key", fn.getPath());
										fnJson.put("name", fn.getAliasName());
										fnJson.put("function", fn.getFunction());
										arr.put(fnJson);
									}
								}
								json.put("function", arr);
							}
							
							if(filterList != null){
								JSONArray arr = new JSONArray();
								for(FilterObject item : filterList){
									JSONObject filterJson = new JSONObject();
									filterJson.put("name", item.getName());
									filterJson.put("value", item.getValue());
									filterJson.put("comparison", item.getComparison());
									filterJson.put("path", item.getPath());
									filterJson.put("kieudulieu", item.getKieuDuLieu());
									arr.put(filterJson);
								}
								
								json.put("filter", arr);
							}
							
							baoCaoThongKeCurrent.setGiaTri(json);
							
							String mss = "Lưu thành công";
							List<BaoCaoThongKeDonVi> baoCaoThongKeDonViList = new ArrayList();
							if(donViSelected != null){
								for(Entry<HtDonVi, Boolean> entry : donViSelected.entrySet()){
									if(entry.getValue() != null && entry.getValue()){
										BaoCaoThongKeDonVi baoCaoThongKeDonVi = new BaoCaoThongKeDonVi();
										baoCaoThongKeDonVi.setBaoCaoThongKe(baoCaoThongKeCurrent);
										baoCaoThongKeDonVi.setHtDonVi(entry.getKey());
										baoCaoThongKeDonViList.add(baoCaoThongKeDonVi);
									}
								}
							}
							if(Constants.ACTION_APPROVE.equals(action)){
								BaoCaoThongKe current = baoCaoThongKeWf.pheDuyet();
								current.setHtDonVi(htDonViCurrent);
								
								baoCaoThongKeService.save(current, baoCaoThongKeDonViList);
								mss = baoCaoThongKeWf.getPheDuyetTen() + " thành công";
							}else if(Constants.ACTION_REFUSE.equals(action)){
								BaoCaoThongKe current = baoCaoThongKeWf.tuChoi();
								current.setHtDonVi(htDonViCurrent);
								
								baoCaoThongKeService.save(current, baoCaoThongKeDonViList);
								mss = baoCaoThongKeWf.getTuChoiTen() + " thành công";
							}else{
								baoCaoThongKeCurrent.setHtDonVi(htDonViCurrent);
								baoCaoThongKeService.save(baoCaoThongKeCurrent, baoCaoThongKeDonViList);
							}
							
							Notification notf = new Notification("Thông báo", mss);
							notf.setDelayMsec(3000);
							notf.setPosition(Position.TOP_CENTER);
							notf.show(UI.getCurrent().getPage());
							
							if(Constants.ACTION_APPROVE.equals(action) || Constants.ACTION_REFUSE.equals(action)){
								UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeView.VIEW_NAME);
							}
							
						}
					}else if(groupList != null  && !groupList.isEmpty() && processDef != null ){
					
						List<String> keyList = new ArrayList<String>();
						List<String> chartDataKeyList = new ArrayList<String>();
						
						Map<String, String> aliasName = new HashMap<String, String>();
						
						
						//process instance list get by process definition
						List<String> processInstanceIdList = new ArrayList<String>();
						List<HistoricProcessInstance> historicProInsList = historyService.createHistoricProcessInstanceQuery().processDefinitionId(processDef.getId()).list();
						if(historicProInsList != null && !historicProInsList.isEmpty()){
							for(HistoricProcessInstance hpi : historicProInsList){
								processInstanceIdList.add(hpi.getId());
							}
						}
						

						Map<String, String> keyMap = new HashMap();
						
						List<String> groupByPath = new ArrayList<String>();
						if(groupList != null && !groupList.isEmpty()){
							int i =0;
							for(GroupObject g : groupList){
								i++;
								if(g.getPath() != null){
									groupByPath.add(g.getPath());
									keyMap.put(g.getPath().toLowerCase(), g.getPath());
									keyList.add(g.getPath().toLowerCase());
									aliasName.put(g.getPath().toLowerCase(), g.getAliasName());
									
									if(i == groupList.size()){
										chartDataKeyList.add(g.getPath().toLowerCase());
									}
								}
							}
						}
						
						
						List<String> functionPath = new ArrayList<String>();
						if(functionList != null && !functionList.isEmpty()){
							for(FunctionObject fn : functionList){
								if(fn.getPath() != null && fn.getFunction() != null){
									functionPath.add(fn.getPath()+"#"+fn.getFunction().intValue());
									CommonUtils.ESummaryFunction eFunction = CommonUtils.ESummaryFunction.getESummaryFunction(fn.getFunction());
									if(eFunction != null){
										String key = fn.getPath().toLowerCase() +"_"+eFunction.getShortName().toLowerCase();
										keyList.add(key);
										chartDataKeyList.add(key);
										aliasName.put(key, fn.getAliasName());
									}
								}
							}
						}
						
						Map<String, Object> valueMap = SqlJsonBuilder.getReportSqlQuery(groupByPath, truongThongTinKeyMap, functionPath, processInstanceIdList, filterList, fromDate, toDate);
						if(valueMap != null){
							String sql = (String) valueMap.get("SQL");
							Object[] objParams = (Object[]) valueMap.get("PARAMS");
							int[] types = (int[]) valueMap.get("TYPES");
							
							List<Map<String, Object>> results = hoSoService.getReportResult(sql, objParams, types);
							if(BUTTON_CREATE_REPORT_ID.equals(button.getId())){
								String html = ChartUtils.getReportHtml(results, keyList, aliasName, truongThongTinKeyMap, keyMap);
								contentHtml.setValue(html);
								UI.getCurrent().addWindow(htmlTableWindow);
							}else if(BUTTON_DOWNLOAD_EXCEL_ID.equals(button.getId())){
								Workbook workbook = ChartUtils.exportExcel(results, keyList, aliasName, tieuDe);
								File tempFile = File.createTempFile("tmp", ".xlsx");
								FileOutputStream fos = new FileOutputStream(tempFile);
								workbook.write(fos);
								fos.close();
								String fileName = "BCTK";
								if(tieuDe != null){
									fileName = tieuDe;
								}
							    TemporaryFileDownloadResource resourcetmp = new TemporaryFileDownloadResource(fileName+".xlsx", "application/vnd.ms-excel", tempFile);
							    UI.getCurrent().getPage().open(resourcetmp, "Báo cáo thống kê Excel", false);
							}else if(BUTTON_CREATE_CHART_ID.equals(button.getId())){
								Map<String, Object> chartDataMap = new HashMap<String, Object>();
								String jsonStr = ChartUtils.getGChartData(chartDataKeyList, results, aliasName);
								chartDataMap.put("CHART_LIB", "G");
								chartDataMap.put("JSON", jsonStr);
								chartDataMap.put("CHART_TYPE", chartType);
								VaadinSession.getCurrent().getSession().setAttribute("chartDataMap", chartDataMap);
								String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
								UI.getCurrent().getPage().open(contextPath+"/charts/review", "_blank");
							} 
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra!", Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(UI.getCurrent().getPage());
		}
	}

}

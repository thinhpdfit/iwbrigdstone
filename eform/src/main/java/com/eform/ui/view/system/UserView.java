package com.eform.ui.view.system;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.type.StatusModel;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.HtDonVi;
import com.eform.service.HtDonViServices;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.StatusCommentLayout;
import com.eform.ui.custom.paging.UserPaginationBar;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = UserView.VIEW_NAME)
public class UserView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "user";
	static final String MAIN_TITLE = "Người dùng";
	private static final String SMALL_TITLE = "";

	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	private static final String BTN_VIEW_ID = "xem";
	private static final String BTN_EDIT_ID = "sua";
	private static final String BTN_DEL_ID = "xoa";
	private static final String BTN_ADDNEW_ID = "themMoi";
	private final UserView currentView = this;
	
	private BeanItemContainer<User> container;
	private Table table = new Table();
	
	private final PropertysetItem searchItem = new PropertysetItem();
	private final FieldGroup searchFieldGroup = new FieldGroup();
	private Button btnAddNew;

	@Autowired
	protected IdentityService identityService;
	private UserPaginationBar paginationBar;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private HtDonViServices htDonViServices;
	
	@PostConstruct
    void init() {

		container = new BeanItemContainer<User>(User.class, new ArrayList());
		paginationBar = new UserPaginationBar(container, identityService);
    	searchItem.addItemProperty("id", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("firstName", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("lastName", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("lastName", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("email", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("group", new ObjectProperty<Group>(null, Group.class));
    	searchFieldGroup.setItemDataSource(searchItem);
		
		initMainTitle();
		initSearchForm();
		initDataContent();
		
		btnAddNew = new Button();
    	btnAddNew.setIcon(FontAwesome.PLUS);
    	btnAddNew.addClickListener(this);
    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
    	btnAddNew.setDescription("Tạo mới người dùng");
    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
    	btnAddNew.setId(BTN_ADDNEW_ID);
		addComponent(btnAddNew);
		
		
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		
		CssFormLayout searchForm = new CssFormLayout(-1, 60);

		TextField txtId = new TextField("Tên đăng nhập");
		searchForm.addFeild(txtId);
		TextField txtFirstName = new TextField("Họ đệm");
		searchForm.addFeild(txtFirstName);
		TextField txtLastName = new TextField("Tên");
		searchForm.addFeild(txtLastName);
		TextField txtEmail = new TextField("Email");
		searchForm.addFeild(txtEmail);

		
		List<Group> groups = identityService.createGroupQuery().list();
		
		BeanItemContainer<Group> gc = new BeanItemContainer<Group>(Group.class, groups);
		ComboBox cbbGroup = new ComboBox("Nhóm người dùng", gc);
		cbbGroup.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbGroup.setItemCaptionPropertyId("name");
		searchForm.addFeild(cbbGroup);
		
		WorkflowManagement<HtDonVi> dvWM = new WorkflowManagement<HtDonVi>(EChucNang.HT_DON_VI.getMa());
		List<Long> trangThai = null;
		if(dvWM.getTrangThaiSuDung() != null){
			trangThai = new ArrayList();
			trangThai.add(new Long(dvWM.getTrangThaiSuDung().getId()));
		}
		
		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(currentView);
		searchForm.addButton(btnSearch);
		paginationBar.fixEnterHotKey(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(currentView);
		searchForm.addButton(btnRefresh);
		
		searchFieldGroup.bind(txtId, "id");
		searchFieldGroup.bind(txtFirstName, "firstName");
		searchFieldGroup.bind(txtLastName, "lastName");
		searchFieldGroup.bind(txtEmail, "email");
		searchFieldGroup.bind(cbbGroup, "group");
		
		addComponent(searchForm);
	}
	
	public void initDataContent(){
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
		List<StatusModel> trangThaiList = new ArrayList();
		StatusModel status = new StatusModel();
		status.setName("Đang sử dụng");
		status.setStyleName("status-using");
		trangThaiList.add(status);
		
		status = new StatusModel();
		status.setName("Đã hết hạn");
		status.setStyleName("status-susspended");
		trangThaiList.add(status);
		
		dataWrap.addComponent(new StatusCommentLayout(trangThaiList));
		
		table.setContainerDataSource(container);
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setResponsive(true);
		table.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		table.setColumnHeader("id", "Tên đăng nhập");
		table.setColumnHeader("firstName", "Họ đệm");
		table.setColumnHeader("lastName", "Tên");
		table.setColumnHeader("email", "Email");
		table.setColumnHeader("donvi", "Đơn vị");
		table.setColumnHeader("group", "Nhóm người dùng");
		
		table.addGeneratedColumn("action", new ColumnGenerator() { 

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				HorizontalLayout actionWrap = new HorizontalLayout();
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
				
				
				Button btnView = new Button();
				btnView.setDescription("Xem");
		        btnView.setId(BTN_VIEW_ID);
		        btnView.setIcon(FontAwesome.EYE);
		        btnView.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnView.addClickListener(currentView);
		        btnView.setData(itemId);
		        btnView.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnView);
		        
		        
		        Button btnEdit = new Button();
		        btnEdit.setId(BTN_EDIT_ID);
		        btnEdit.setDescription("Sửa");
		        btnEdit.setIcon(FontAwesome.PENCIL_SQUARE_O);
		        btnEdit.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnEdit.addClickListener(currentView);
		        btnEdit.setData(itemId);
		        btnEdit.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnEdit);
		        
		        Button btnDel = new Button();
		    	btnDel.setId(BTN_DEL_ID);
		    	btnDel.setDescription("Xóa");
		    	btnDel.addStyleName(ExplorerLayout.BUTTON_ACTION);
		    	btnDel.addStyleName(Reindeer.BUTTON_LINK);
		    	btnDel.setData(itemId);
		    	btnDel.setIcon(FontAwesome.REMOVE);
		    	btnDel.addClickListener(currentView);
		        actionWrap.addComponent(btnDel);
		       
		        return actionWrap;
		    }
		});
		
		
		table.addGeneratedColumn("donvi", new ColumnGenerator() { 

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				if(itemId != null && itemId instanceof User){
					User u = (User) itemId;
					String maDonVi = identityService.getUserInfo(u.getId(), "HT_DON_VI");
					if(maDonVi != null){
		    			List<HtDonVi> list = htDonViServices.findByCode(maDonVi);
		    			if(list != null && !list.isEmpty() && list.get(0) != null){
		    				return list.get(0).getTen();
		    			}
		    		}
				}
				return "";
		    }
		});
		
		
		table.addGeneratedColumn("group", new ColumnGenerator() { 

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				if(itemId != null && itemId instanceof User){
					User u = (User) itemId;
					List<Group> gList = identityService.createGroupQuery().groupMember(u.getId()).list();
					if(gList != null && !gList.isEmpty()){
						CssLayout wrap = new CssLayout();
						wrap.addStyleName(ExplorerLayout.GROUP_COLUMN_CELL);
						for(Group g: gList){
							Label lbl = new Label(g.getName());
							lbl.addStyleName(ExplorerLayout.GROUP_COLUMN_CELL_LBL);
							wrap.addComponent(lbl);
						}
						return wrap;
					}
				}
				return null;
		    }
		});
		
		table.addGeneratedColumn("expiryDate", new ColumnGenerator() { 

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				if(itemId != null && itemId instanceof User){
					User u = (User) itemId;
					String expiryDateStr = identityService.getUserInfo(u.getId(), "EXPIRY_DATE");
					if(expiryDateStr != null && expiryDateStr.trim().matches("[0-9]+")){
		    			Date expiryDate = new Date(new Long(expiryDateStr.trim()));
		    			if(expiryDate != null){
		    				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		    				return df.format(expiryDate);
		    			}
		    		}
				}
				return "";
		    }
		});

		table.setColumnHeader("action", "Thao tác");
		table.setColumnHeader("expiryDate", "Ngày hết hạn");
		table.setRowHeaderMode(RowHeaderMode.INDEX);

		table.setVisibleColumns("id", "firstName", "lastName", "email","group", "donvi", "expiryDate", "action");
		

		table.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if(itemId != null && itemId instanceof User){
					User u = (User) itemId;
					String expiryDateStr = identityService.getUserInfo(u.getId(), "EXPIRY_DATE");
					if(expiryDateStr != null && expiryDateStr.trim().matches("[0-9]+")){
		    			Date expiryDate = new Date(new Long(expiryDateStr.trim()));
		    			if(expiryDate.before(new Date())){
		    				return "status-susspended";
		    			}
					}
				}
				
				return "status-using";
			}
			
		});

		dataWrap.addComponent(table);
		dataWrap.addComponent(paginationBar);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_SEARCH_ID.equals(event.getButton().getId())){
			try {
				if(searchFieldGroup.isValid()){
					searchFieldGroup.commit();
					String id = (String) searchItem.getItemProperty("id").getValue();
					id = id != null ? id.trim() : null;

					String firstName = (String) searchItem.getItemProperty("firstName").getValue();
					firstName = firstName != null ? firstName.trim() : "";
					
					String lastName = (String) searchItem.getItemProperty("lastName").getValue();
					lastName = lastName != null ? lastName.trim() : "";
					
					String email = (String) searchItem.getItemProperty("email").getValue();
					email = email != null ? email.trim() : "";
					
					Group group = (Group) searchItem.getItemProperty("group").getValue();
					
					String groupId = null;
					if(group!= null){
						groupId = group.getId();
					}
					
					paginationBar.search(id, firstName, lastName, email, null, groupId);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
			searchFieldGroup.clear();
			paginationBar.search(null, null, null, null, null, null);
		}else if(BTN_VIEW_ID.equals(event.getButton().getId())){
			User currentRow = (User) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(UserDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
		}else if(BTN_EDIT_ID.equals(event.getButton().getId())){
			User currentRow = (User) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(UserDetailView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getId());
		}else if(BTN_DEL_ID.equals(event.getButton().getId())){
			User currentRow = (User) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(UserDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getId());
		}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
			UI.getCurrent().getNavigator().navigateTo(UserDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD);
		}
	}

}

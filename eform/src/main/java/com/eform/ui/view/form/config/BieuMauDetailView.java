package com.eform.ui.view.form.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.json.JSONObject;
import org.noggit.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;

import com.eform.activiti.servicetask.custom;
import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.ThamSo;
import com.eform.common.type.CellDetail;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.SolrUtils;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.Formula;
import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BieuMauService;
import com.eform.service.BmHangService;
import com.eform.service.BmOService;
import com.eform.service.BmOTruongThongTinService;
import com.eform.service.DanhMucService;
import com.eform.service.FormulaService;
import com.eform.service.HtThamSoService;
import com.eform.service.LoaiDanhMucService;
import com.eform.service.TruongThongTinService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.paging.TruongThongTinPaginationBar;
import com.eform.ui.view.dashboard.DashboardView;
import com.google.api.client.json.Json;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SpringView(name = BieuMauDetailView.VIEW_NAME)
public class BieuMauDetailView extends VerticalLayout implements View, ClickListener {

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "chi-tiet-bieu-mau";
	private static String MAIN_TITLE = "Chi tiết biểu mẫu";
	private static String SMALL_TITLE = "";

	private final int WIDTH_FULL = 100;
	private static final String BUTTON_APPROVE_ID = "btn-approve";
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_ADD_ID = "btn-edit";
	private static final String BUTTON_DEL_ID = "btn-del";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_REFUSE_ID = "btn-refuse";
	private static final String BUTTON_OPEN_WINDOW = "btn-open-window";
	private static final String BUTTON_UNDO = "btn-undo";
	private static final String BUTTON_INIT_GRID = "btn-init-grid";
	private static final String BUTTON_GHEP_PHAI_ID = "btn-ghep-phai";
	private static final String BUTTON_TACH_CELL_ID = "btn-tach-trai";
	private static final String BUTTON_TACH_ROW_ID = "btn-tach-tren";
	private static final String BUTTON_GHEP_XUONG_ID = "btn-ghep-xuong";
	private static final String BUTTON_EDIT_CELL_ID = "btn-edit-cell";
	private static final String BUTTON_UPDATE_CELL_ID = "btn-update-cell";
	private static final String BUTTON_ADD_ROW_ID = "btn-add-row";
	private static final String BUTTON_ADD_COLUMN_ID = "btn-add-column";
	private static final String BUTTON_REMOVE_ROW_ID = "btn-remove-row";
	private static final String BTN_ADD_TRUONG_THONG_TIN = "btn-add-truong-thong-tin";
	private static final String BUTTON_ADD_TTT_POPUP_ID = "btn-add-ttt-popup";
	private static final String BTN_ADD_FORMULA = "btn-add-formula";
	private static final String BUTTON_REMOVE_FORMULA_ID = "btn-remove-formula";

	private BieuMau bieuMauCurrent;
	private WorkflowManagement<BieuMau> bieuMauWM;
	Workflow<BieuMau> bieuMauWf;
	private String action;

	private final FieldGroup fieldGroup = new FieldGroup();
	private CssLayout formContainer;
	private Button btnUpdate;
	private Button btnBack;
	private Button btnInitBmGrid;

	private final PropertysetItem initGridItem = new PropertysetItem();
	private final FieldGroup initGridGroup = new FieldGroup();

	private final PropertysetItem editCellItem = new PropertysetItem();
	private final FieldGroup editCellGroup = new FieldGroup();
	private Integer soHang;
	private Integer soCot;
	private Integer soHangHistory;
	private Integer soCotHistory;

	private Window window;
	private Window editCellWindow;
	private GridLayout bieuMauGrid;
	private Window addTruongThongTinWindow;
	private RichTextArea textArea;
	private TextField txtMa;
	private TruongThongTinPaginationBar paginationBar;
	private TextField filterMa;
	
	@Autowired
	private BieuMauService bieuMauService;
	@Autowired
	private BmHangService bmHangService;
	@Autowired
	private BmOService bmOService;
	@Autowired
	private FormulaService formulaService;
	@Autowired
	private BmOTruongThongTinService bmOTruongThongTinService;
	@Autowired
	private TruongThongTinService truongThongTinService;
	@Autowired
	private LoaiDanhMucService loaiDanhMucService;
	@Autowired
	private DanhMucService danhMucService;

	private final PropertysetItem cellIdtemValue = new PropertysetItem();
	private FieldGroup cellIdFieldGroup = new FieldGroup();
	private List<String> cellIdList = new ArrayList();
	private Map<Integer, String> cellIdMap = new HashMap();

	private CellDetail[][] hangCotArr;
	private CellDetail[][] hangCotArrHistory;

	private Map<String, Object> cellContentMap;
	private Map<String, Object> cellContentMapHistory;

	private CellDetail cellCurrent;

	// private Grid gridTruongThongtin;

	private TruongThongTinDetailView truongThongTinDetailView;

	private CssLayout buttonContainer;
	private Window addBtnWindow;
	private final PropertysetItem btnItem = new PropertysetItem();
	private List<Map<String, String>> btnList;
	private String maNhom;
	private Map<Long, Boolean> selectedLapHang;
	private Map<Long, Boolean> selectedLapHangHistory;
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;
	@Autowired
	private HtThamSoService htThamSoService;

	private String solrUrl;
	private List<TruongThongTin> tttSelectionList;
	private List<TruongThongTin> tttReadOnlyList;
	private List<TruongThongTin> tttFilterList;
	private Map<TruongThongTin, String> constrantsMap;
	private String maNhomCurrent;
	private Long valueDisplayButton;
	private final PropertysetItem formulaItem = new PropertysetItem();
	private final FieldGroup formulaFeildGroup = new FieldGroup();

	private Map<String, Map<String, String>> formulaMap;

	private CssLayout formulaContainer;

	private List<TruongThongTin> truongThongTinAddedList;// truong thong tin da them vao form

	@PostConstruct
	void init() {
		soHang = 0;
		soCot = 0;
		selectedLapHang = new HashMap();
		cellIdFieldGroup.setItemDataSource(cellIdtemValue);
		cellContentMap = new HashMap<String, Object>();
		bieuMauWM = new WorkflowManagement<BieuMau>(EChucNang.E_FORM_DANH_MUC.getMa());
		initGridItem.addItemProperty("soHang", new ObjectProperty<Integer>(soHang));
		initGridItem.addItemProperty("soCot", new ObjectProperty<Integer>(soCot));
		initGridGroup.setItemDataSource(initGridItem);
		tttSelectionList = new ArrayList();
		tttReadOnlyList = new ArrayList();
		tttFilterList = new ArrayList();
		constrantsMap = new HashMap();
		formulaFeildGroup.setItemDataSource(formulaItem);
		formulaMap = new HashMap();
		truongThongTinAddedList = new ArrayList<TruongThongTin>();
		messageSourceAccessor= new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());
		initMainTitle();
		addWindowInitGrid();
		initAddTruongThongTinWindow();
		HtThamSo solrUrlThamSo = htThamSoService.getHtThamSoFind(ThamSo.SOLRSEARCH_URL);
		if (solrUrlThamSo != null && solrUrlThamSo.getGiaTri() != null) {
			solrUrl = solrUrlThamSo.getGiaTri();
		}
		
	}

	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if (params != null) {
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if (m.matches()) {
				action = m.group(1);
				if (Constants.ACTION_ADD.equals(action)) {
					bieuMauCurrent = new BieuMau();
					bieuMauWf = bieuMauWM.getWorkflow(bieuMauCurrent);
					bieuMauDetail(bieuMauCurrent);
					initDetailForm();
					initGridBtnGroup();
					initBieuMauGrid();
					initActionBtnGroup();
				} else {
					String ma = m.group(3);
					List<BieuMau> list = bieuMauService.findByCode(ma);
					if (list != null && !list.isEmpty()) {
						bieuMauCurrent = list.get(0);
						bieuMauWf = bieuMauWM.getWorkflow(bieuMauCurrent);
						bieuMauDetail(bieuMauCurrent);
						initDetailForm();
						initGridBtnGroup();
						initBieuMauGrid();
						initActionBtnGroup();
						initEditGrid(bieuMauCurrent);
						refreshLuoi();
						List<Formula> formulaList = formulaService.getFormulaFind(bieuMauCurrent);
						if (formulaList != null) {
							initFormulaWrap();
							int i = 0;
							for (Formula f : formulaList) {
								Map<String, String> map = new HashMap();
								map.put("formula", f.getFormula());
								map.put("formulaPath", f.getFormulaPath());
								map.put("resultField", f.getResultField());
								formulaMap.put(i + "", map);
								CssLayout item = initFormulaItem(i + "");
								if (item != null) {
									formulaContainer.addComponent(item);
								}
								i++;
							}
						}
					}
				}
			}
		}
	}

	public void initMainTitle() {
		PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSourceAccessor.getMessage(Messages.UI_VIEW_HOME));
		home.setViewName(DashboardView.VIEW_NAME);

		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(BieuMauView.MAIN_TITLE);
		bcItem.setViewName(BieuMauView.VIEW_NAME);

		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		MAIN_TITLE=messageSourceAccessor.getMessage(Messages.UI_VIEW_BIEUMAU_DETAIL_TITLE);
		SMALL_TITLE=messageSourceAccessor.getMessage(Messages.UI_VIEW_BIEUMAU_DETAIL_TITLE_SMALL);
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);

		addComponent(new PageBreadcrumbLayout(home, bcItem, current));
		addComponent(new PageTitleLayout(this.MAIN_TITLE, this.SMALL_TITLE));
	}

	private void bieuMauDetail(BieuMau bieuMau) {
		if (bieuMau == null) {
			bieuMau = new BieuMau();
		}
		if (Constants.ACTION_COPY.equals(action)) {
			bieuMau.setMa(null);
		}
		BeanItem<BieuMau> item = new BeanItem<BieuMau>(bieuMau);
		fieldGroup.setItemDataSource(item);
	}

	public void initDetailForm() {

		formContainer = new CssLayout();
		formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
		FormLayout form = new FormLayout();
		form.setMargin(true);

		TextField txtMa = new TextField(messageSourceAccessor.getMessage(Messages.UI_LABEL_ID));
		txtMa.setRequired(true);
		txtMa.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void textChange(TextChangeEvent event) {
				TextField txt = (TextField) fieldGroup.getField("ma");
				System.out.println(event.getText());
				if (!checkTrungMa(event.getText(), action)) {
					txt.setComponentError(new UserError(messageSourceAccessor.getMessage(Messages.UI_MESSAGES_ISREADY)));
				} else {
					txt.setComponentError(null);
				}
			}
		});
		txtMa.setTextChangeEventMode(TextChangeEventMode.LAZY);
		txtMa.setSizeFull();
		txtMa.setNullRepresentation("");
		txtMa.setRequiredError(messageSourceAccessor.getMessage(Messages.UI_MESSAGES_REQUIREMENT));
		txtMa.addValidator(new StringLengthValidator(messageSourceAccessor.getMessage(Messages.UI_LABEL_ID_ERROR_MESSAGES_MAXLENG20), 1, 20, false));
		txtMa.addValidator(new RegexpValidator("[0-9a-zA-Z_]+",messageSourceAccessor.getMessage(Messages.UI_LABEL_ID_ERROR_MESSAGES_CHARATER09ZA)));
		TextField txtTen = new TextField(messageSourceAccessor.getMessage(Messages.UI_LABEL_NAME));
		txtTen.setRequired(true);
		txtTen.setSizeFull();
		txtTen.setNullRepresentation("");
		txtTen.setRequiredError(messageSourceAccessor.getMessage(Messages.UI_MESSAGES_REQUIREMENT));
		txtTen.addValidator(new StringLengthValidator(messageSourceAccessor.getMessage(Messages.UI_LABEL_ID_ERROR_MESSAGES_MAXLENG2000), 1, 2000, false));
		ComboBox cbbLoaiBieuMau = new ComboBox(messageSourceAccessor.getMessage(Messages.UI_VIEW_BIEUMAU_LOAIBIEUMAU));
		for (CommonUtils.ELoaiBieuMau item : CommonUtils.ELoaiBieuMau.values()) {
			cbbLoaiBieuMau.addItem(new Long(item.getCode()));
			cbbLoaiBieuMau.setItemCaption(new Long(item.getCode()), item.getName());
		}
		cbbLoaiBieuMau.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		cbbLoaiBieuMau.setRequired(true);
		cbbLoaiBieuMau.setRequiredError(messageSourceAccessor.getMessage(Messages.UI_MESSAGES_REQUIREMENT));

		TextField txtWidth = new TextField(messageSourceAccessor.getMessage(Messages.UI_LABEL_WIDTH));
		txtWidth.setSizeFull();
		txtWidth.setNullRepresentation("");
		txtWidth.setInputPrompt(messageSourceAccessor.getMessage(Messages.UI_LABEL_WIDTH_EX));
		/*button config*/
//		custom txtServiceConfig = new custom();
//		txtServiceConfig.setVisible(false);
//		FormLayout layout = new FormLayout();
//		final Window window = new Window("Service config", layout);
//		TextField url = new TextField("Url:");
//		layout.addComponent(url);
//
//		Button button = new Button("Service Config", new ClickListener() {
//			public void buttonClick(ClickEvent event) {
//				if (txtServiceConfig.getValue() != null)
//					url.setValue(txtServiceConfig.getValue().toString());
//				getUI().addWindow(window);
//			}
//		});
//
//		window.addCloseListener(new CloseListener() {
//			public void windowClose(CloseEvent e) {
//				String urlVal = url.getValue();
//				if (urlVal != null && !urlVal.equalsIgnoreCase("")) {
//					txtServiceConfig.setValue(new JSONObject(urlVal));
//				}
//
////	          try {
////	            fieldGroup.commit();
////	          } catch (CommitException ex) {
////	            ex.printStackTrace();
////	          }
//			}
//		});
//		window.center();
//		window.setWidth(null);
//		layout.setWidth(null);
//		layout.setMargin(true);
		
		custom formServicceConfig = new custom();
		formServicceConfig.setVisible(false);
		FormLayout layoutFormSvConfig = new FormLayout();
		final Window windowFormSvConfig = new Window("Form Service config", layoutFormSvConfig);
		TextField urlFormSvConfig = new TextField("Json:");
		layoutFormSvConfig.addComponent(urlFormSvConfig);
		
		HorizontalLayout fittingLayout = new HorizontalLayout();
	//	fittingLayout.setWidth(Sizeable.SIZE_UNDEFINED, 0); // Default



		
		
		// Create the selection component
		ListSelect select = new ListSelect("The List");

		// Add some items (here by the item ID as the caption)
		select.addItems("Mercury", "Venus", "Earth");

		select.setNullSelectionAllowed(false);

		// Show 5 items and a scrollbar if there are more
		select.setRows(5);
		fittingLayout.addComponent(select);
		
		ListSelect select1 = new ListSelect("The List");

		// Add some items (here by the item ID as the caption)
		select1.addItems("Mercury", "Venus", "Earth");

		select1.setNullSelectionAllowed(false);

		// Show 5 items and a scrollbar if there are more
		select1.setRows(5);
		fittingLayout.addComponent(select1);

		
		layoutFormSvConfig.addComponent(fittingLayout);
		Button buttonurlFormSvConfig = new Button("Form Config", new ClickListener() {
			public void buttonClick(ClickEvent event) {
				if (formServicceConfig.getValue() != null)
					urlFormSvConfig.setValue(formServicceConfig.getValue().toString());
				getUI().addWindow(windowFormSvConfig);
			}
		});

		windowFormSvConfig.addCloseListener(new CloseListener() {
			public void windowClose(CloseEvent e) {
				String urlVal = urlFormSvConfig.getValue();
				if (urlVal != null && !urlVal.equalsIgnoreCase("")) {
					formServicceConfig.setValue(new JSONObject(urlVal));
				}

//	          try {
//	            fieldGroup.commit();
//	          } catch (CommitException ex) {
//	            ex.printStackTrace();
//	          }
			}
		});
		windowFormSvConfig.center();
		windowFormSvConfig.setWidth(null);
		layoutFormSvConfig.setWidth(null);
		layoutFormSvConfig.setMargin(true);
		/*button config*/
		fieldGroup.bind(txtMa, "ma");
		fieldGroup.bind(txtTen, "ten");
		fieldGroup.bind(cbbLoaiBieuMau, "loaiBieuMau");
		fieldGroup.bind(txtWidth, "width");
		/*button config*/
//		fieldGroup.bind(txtServiceConfig, "serviceConfig");
		fieldGroup.bind(formServicceConfig, "formServicceConfig");
		/*button config*/
		txtMa.setReadOnly("xem".equals(action));
		txtTen.setReadOnly("xem".equals(action));
		cbbLoaiBieuMau.setReadOnly("xem".equals(action));
		txtWidth.setReadOnly("xem".equals(action));

		form.addComponent(txtMa);
		form.addComponent(txtTen);
		form.addComponent(cbbLoaiBieuMau);
		form.addComponent(txtWidth);
		/*button config*/
//		form.addComponent(txtServiceConfig);
		form.addComponent(formServicceConfig);
//		form.addComponent(button);
		form.addComponent(buttonurlFormSvConfig);
		
		/*button config*/

		formContainer.addComponent(form);
		addComponent(formContainer);
	}

	public boolean checkTrungMa(String ma, String act) {
		try {
			if (ma == null || ma.isEmpty()) {
				return false;
			}
			if (Constants.ACTION_ADD.equals(act) || Constants.ACTION_COPY.equals(act)) {
				List<BieuMau> list = bieuMauService.findByCode(ma);
				if (list == null || list.isEmpty()) {
					return true;
				}
			} else if (!Constants.ACTION_DEL.equals(act)) {
				List<BieuMau> list = bieuMauService.findByCode(ma);
				if (list == null || list.size() <= 1) {
					return true;
				}
			} else if (Constants.ACTION_DEL.equals(act)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public void getBieuMauChild(List<BmO> bmOList, List<BmHang> bmHangList, List<BmOTruongThongTin> bmOTruongTTList,
			BieuMau bieuMau) {
		if (soHang > 0 && soCot > 0 && hangCotArr != null) {
			BmHang bmHang = null;
			BmO bmO = null;
			BmOTruongThongTin oTruongTT = null;
			// int viTriO = 0; //Vi tri o trong bieu mau
			for (int hang = 0; hang < soHang; hang++) {
				bmHang = new BmHang();
				bmHang.setThuTu(new Long(hang));
				bmHang.setBieuMau(bieuMau);
				Boolean lap = selectedLapHang.get(new Long(hang));
				if (lap != null && lap) {
					bmHang.setLoaiHang(new Long(CommonUtils.ELoaiHang.LAP.getCode()));
				} else {
					bmHang.setLoaiHang(new Long(CommonUtils.ELoaiHang.KHONG_LAP.getCode()));
				}
				bmHangList.add(bmHang);
				for (int cot = 0; cot < soCot; cot++) {
					CellDetail cell = hangCotArr[hang][cot];
					if (cell != null && !cell.getDeleted()) {
						bmO = new BmO();
						bmO.setBieuMau(bieuMau);
						bmO.setBmHang(bmHang);
						bmO.setViTri(new Long(cell.getColIndex()));
						bmO.setGhepCot(new Long(cell.getColSpan()));
						bmO.setGhepHang(new Long(cell.getRowSpan()));
						bmO.setThuTu(new Long(cot));
						bmO.setLoaiO(cell.getCellType());
						bmO.setMa(cell.getCellId());
						bmOList.add(bmO);

						if (cell.getCellType() != null) {
							if (cell.getCellType().intValue() == CommonUtils.ELoaiO.BIEU_MAU.getCode()) {
								if (cellContentMap.get(hang + "_" + cot) instanceof BieuMau) {
									bmO.setBieuMauCon((BieuMau) cellContentMap.get(hang + "_" + cot));
//									System.out.println("ma: "+ cell.getGroupCode() );
									bmO.setTen(cell.getGroupCode());
									bmO.setDisplayButton(cell.getDisplayButton());
								}
							} else if (cell.getCellType().intValue() == CommonUtils.ELoaiO.TEXT.getCode()) {
								if (cellContentMap.get(hang + "_" + cot) instanceof String) {
									bmO.setTen((String) cellContentMap.get(hang + "_" + cot));
								}
							} else if (cell.getCellType().intValue() == CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()) {
								List<TruongThongTin> readOnlyList = cell.getReadOnly();
								List<TruongThongTin> filterList = cell.getFilter();
								Map<TruongThongTin, String> constrantMap = cell.getConstrantCode();
								if (cellContentMap.get(hang + "_" + cot) instanceof List) {
									List<TruongThongTin> listTruongTTLst = (List<TruongThongTin>) cellContentMap
											.get(hang + "_" + cot);
									int thuTu = 0;
									for (TruongThongTin truongTT : listTruongTTLst) {
										oTruongTT = new BmOTruongThongTin();
										oTruongTT.setBieuMau(bieuMau);
										oTruongTT.setBmO(bmO);
										oTruongTT.setBmHang(bmHang);
										oTruongTT.setThuTu(new Long(thuTu++));
										oTruongTT.setTruongThongTin(truongTT);
										if (readOnlyList != null && readOnlyList.contains(truongTT)) {
											oTruongTT.setReadOnly(new Long(CommonUtils.EReadOnly.TRUE.getCode()));
										} else {
											oTruongTT.setReadOnly(new Long(CommonUtils.EReadOnly.FALSE.getCode()));
										}

										if (filterList != null && filterList.contains(truongTT)) {
											oTruongTT.setFilterType(
													CommonUtils.EFilterType.BY_DON_VI_DANG_NHAP.getCode());
										}

										if (constrantMap != null && constrantMap.containsKey(truongTT)
												&& constrantMap.get(truongTT) != null
												&& !constrantMap.get(truongTT).isEmpty()) {
											oTruongTT.setConstrant(constrantMap.get(truongTT));
										}
										bmOTruongTTList.add(oTruongTT);
									}
								}

							}
						}
						// viTriO++;
					}
				}
			}
		}
	}

	public void initEditGrid(BieuMau bieuMau) {
		soHang = bieuMau.getSoHang() != null ? bieuMau.getSoHang().intValue() : 0;
		soCot = bieuMau.getSoCot() != null ? bieuMau.getSoCot().intValue() : 0;
		if (cellContentMap != null) {
			cellContentMap.clear();
		} else {
			cellContentMap = new HashMap<String, Object>();
		}

		hangCotArr = new CellDetail[soHang][soCot];

		List<BmHang> bmHangList = bmHangService.getBmHangFind(null, null, null, bieuMau, -1, -1);
		if (bmHangList != null && soCot > 0 && soHang > 0) {
			int hang = 0;
			System.out.println("hang: " + hang);
			Map<CellDetail, List<String>> cellChildMap = new HashMap<CellDetail, List<String>>();
			for (BmHang bmHang : bmHangList) {
				if (bmHang.getLoaiHang() != null
						&& bmHang.getLoaiHang().intValue() == CommonUtils.ELoaiHang.LAP.getCode()) {
					selectedLapHang.put(new Long(hang), true);
				} else {
					selectedLapHang.put(new Long(hang), false);
				}

				List<BmO> bmOList = bmOService.getBmOFind(null, null, null, null, bmHang, null, -1, -1);
				Map<String, BmO> thuTuOMap = new HashMap<String, BmO>();
				for (BmO o : bmOList) {
					thuTuOMap.put(o.getThuTu() + "", o);
				}
				if (bmOList != null && !bmOList.isEmpty()) {
					for (int cot = 0; cot < soCot; cot++) {
//						System.out.println("cot: " + cot);
						BmO bmO = thuTuOMap.get(cot + "");
						if (bmO != null) {
							CellDetail cell = new CellDetail();
							cell.setColIndex(cot);
							cell.setDeleted(false);
							cell.setRowIndex(hang);
							cell.setCellType(bmO.getLoaiO());
							cell.setRowSpan(bmO.getGhepHang() != null ? bmO.getGhepHang().intValue() : 1);
							cell.setColSpan(bmO.getGhepCot() != null ? bmO.getGhepCot().intValue() : 1);
							cell.setParent(null);

							hangCotArr[hang][cot] = cell;
							List<BmOTruongThongTin> oTruongTTList = bmOTruongThongTinService
									.getBmOTruongThongTinFind(null, null, bmO, null, -1, -1);
							if (bmO.getLoaiO() != null
									&& bmO.getLoaiO().intValue() == CommonUtils.ELoaiO.TEXT.getCode()) {
								cellContentMap.put(hang + "_" + cot, bmO.getTen());
							} else if (bmO.getLoaiO() != null
									&& bmO.getLoaiO().intValue() == CommonUtils.ELoaiO.BIEU_MAU.getCode()) {
								cellContentMap.put(hang + "_" + cot, bmO.getBieuMauCon());
								cell.setCellId(bmO.getMa());
								cell.setGroupCode(bmO.getTen());
							} else if (bmO.getLoaiO() != null
									&& bmO.getLoaiO().intValue() == CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()) {

								if (oTruongTTList != null && !oTruongTTList.isEmpty()) {
									List<TruongThongTin> truongTTLst = new ArrayList<TruongThongTin>();
									List<TruongThongTin> truongTTReadonly = new ArrayList();
									List<TruongThongTin> truongTTFilterList = new ArrayList();
									Map<TruongThongTin, String> truongThongTinConstrantMap = new HashMap();
									for (BmOTruongThongTin oTruongTT : oTruongTTList) {
										truongTTLst.add(oTruongTT.getTruongThongTin());
										if (oTruongTT.getReadOnly() != null && oTruongTT.getReadOnly()
												.intValue() == CommonUtils.EReadOnly.TRUE.getCode()) {
											truongTTReadonly.add(oTruongTT.getTruongThongTin());
										}
										if (oTruongTT.getConstrant() != null) {
											truongThongTinConstrantMap.put(oTruongTT.getTruongThongTin(),
													oTruongTT.getConstrant());
										}
										if (CommonUtils.EFilterType.BY_DON_VI_DANG_NHAP.getCode()
												.equals(oTruongTT.getFilterType())) {
											truongTTFilterList.add(oTruongTT.getTruongThongTin());
										}
									}
									cell.setReadOnly(truongTTReadonly);
									cell.setConstrantCode(truongThongTinConstrantMap);
									cell.setFilter(truongTTFilterList);
									cellContentMap.put(hang + "_" + cot, truongTTLst);
									truongThongTinAddedList.addAll(truongTTLst);
								}
							}

							// Tim con
							if (bmO.getGhepCot() > 1 && bmO.getGhepHang() == 1) {
								List<String> list = new ArrayList<String>();
								for (int i = 1; i < bmO.getGhepCot(); i++) {
									list.add(hang + "_" + (cot + i));
								}
								cellChildMap.put(cell, list);
							} else if (bmO.getGhepCot() == 1 && bmO.getGhepHang() > 1) {
								List<String> list = new ArrayList<String>();
								for (int i = 1; i < bmO.getGhepHang(); i++) {
									list.add((hang + i) + "_" + cot);
								}
								cellChildMap.put(cell, list);
							} else if (bmO.getGhepCot() > 1 && bmO.getGhepHang() > 1) {
								List<String> list = new ArrayList<String>();
								for (int y = 0; y < bmO.getGhepHang(); y++) {
									for (int x = 0; x < bmO.getGhepCot(); x++) {
										if (!(x == 0 && y == 0)) {
											list.add((hang + y) + "_" + (cot + x));
										}
									}
								}
								cellChildMap.put(cell, list);
							}
							// end tim con
						} else {
							CellDetail cellDeleted = new CellDetail();
							cellDeleted.setColIndex(cot);
							cellDeleted.setDeleted(true);
							cellDeleted.setRowIndex(hang);
							cellDeleted.setRowSpan(1);
							cellDeleted.setColSpan(1);
							cellDeleted.setParent(null);
							hangCotArr[hang][cot] = cellDeleted;
						}
					}
				}
				hang++;
			}
			if (cellChildMap != null && !cellChildMap.isEmpty()) {
				for (Entry<CellDetail, List<String>> entry : cellChildMap.entrySet()) {
					if (entry != null && entry.getKey() != null && entry.getValue() != null) {
						for (String str : entry.getValue()) {
							if (str != null && !str.isEmpty()) {
								String[] split = str.split("_");
								if (split != null && split.length == 2) {
									if (split[0].matches("[0-9]+") && split[1].matches("[0-9]+")) {
										CellDetail cell = hangCotArr[new Integer(split[0])][new Integer(split[1])];
										if (cell != null) {
											cell.setParent(entry.getKey());
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void initGridBtnGroup() {
		CssLayout buttons = new CssLayout();
		buttons.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		buttons.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);

		btnInitBmGrid = new Button(messageSourceAccessor.getMessage(Messages.UI_BUTTON_GIRL_CREATE));
		btnInitBmGrid.addClickListener(this);
		btnInitBmGrid.setId(BUTTON_OPEN_WINDOW);
		btnInitBmGrid.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnInitBmGrid.addStyleName(ExplorerLayout.BTN_INIT_GRID_BM);

		buttons.addComponent(btnInitBmGrid);

		Button btnUndo = new Button();
		btnUndo.setDescription(messageSourceAccessor.getMessage(Messages.UI_BUTTON_GIRL_UNDO));
		btnUndo.setIcon(FontAwesome.REPLY);
		btnUndo.addClickListener(this);
		btnUndo.setId(BUTTON_UNDO);
		btnUndo.addStyleName("btn-undo");
		buttons.addComponent(btnUndo);

		addComponent(buttons);
	}

	public void initActionBtnGroup() {

		CssLayout buttons = new CssLayout();
		buttons.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		buttons.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);
		if (!Constants.ACTION_VIEW.equals(action)) {
			btnUpdate = new Button();
			btnUpdate.addClickListener(this);
			buttons.addComponent(btnUpdate);
			if (Constants.ACTION_EDIT.equals(action)) {
				btnUpdate.setCaption(messageSourceAccessor.getMessage(Messages.UI_BUTTON_UPDATE));
				btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
				btnUpdate.setId(BUTTON_EDIT_ID);
			} else if (Constants.ACTION_ADD.equals(action) || Constants.ACTION_COPY.equals(action)) {
				btnUpdate.setId(BUTTON_ADD_ID);
				btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
				btnUpdate.setCaption(messageSourceAccessor.getMessage(Messages.UI_BUTTON_CREATE));
			} else if (Constants.ACTION_DEL.equals(action)) {
				btnUpdate.setId(BUTTON_DEL_ID);
				btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
				btnUpdate.setCaption(messageSourceAccessor.getMessage(Messages.UI_BUTTON_DELETE));
			} else if (Constants.ACTION_APPROVE.equals(action)) {
				btnUpdate.setId(BUTTON_APPROVE_ID);
				btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
				btnUpdate.setCaption(bieuMauWf.getPheDuyetTen());
			} else if (Constants.ACTION_REFUSE.equals(action)) {
				btnUpdate.setCaption(bieuMauWf.getTuChoiTen());
				btnUpdate.setId(BUTTON_REFUSE_ID);
				btnUpdate.addStyleName(ExplorerLayout.BUTTON_WARNING);
			}
		}

		btnBack = new Button(messageSourceAccessor.getMessage(Messages.UI_BUTTON_BACK));
		btnBack.addClickListener(this);
		btnBack.setId(BUTTON_CANCEL_ID);
		buttons.addComponent(btnBack);
		addComponent(buttons);
	}

	public void initAddTruongThongTinWindow() {
		VerticalLayout content = new VerticalLayout();
		content.setWidth("100%");
		content.setSpacing(true);
		content.setMargin(true);

		truongThongTinDetailView = new TruongThongTinDetailView();
		truongThongTinDetailView.initAddWindow(loaiDanhMucService, truongThongTinService, danhMucService);
		content.addComponent(truongThongTinDetailView);

		HorizontalLayout buttons = new HorizontalLayout();
		buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
		buttons.addStyleName(ExplorerLayout.BUTTONS_CENTER);
		Button btnAddTruongThongTin = new Button(messageSourceAccessor.getMessage(Messages.UI_BUTTON_CREATE));
		btnAddTruongThongTin.addClickListener(this);
		btnAddTruongThongTin.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnAddTruongThongTin.setId(BUTTON_ADD_TTT_POPUP_ID);
		buttons.addComponent(btnAddTruongThongTin);

		content.addComponent(buttons);

		addTruongThongTinWindow = new Window(messageSourceAccessor.getMessage(Messages.UI_VIEW_FIELDINFO));
		addTruongThongTinWindow.setWidth(900, Unit.PIXELS);
		addTruongThongTinWindow.setHeight(600, Unit.PIXELS);
		addTruongThongTinWindow.center();
		addTruongThongTinWindow.setModal(true);
		addTruongThongTinWindow.setContent(content);
	}
	private boolean flagMaChange=false;
	private String MaChange="";
	public void addWindowInitGrid() {
		FormLayout initGridForm = new FormLayout();

		Button btnUpdateCell = new Button(messageSourceAccessor.getMessage(Messages.UI_BUTTON_CREATE));
		btnUpdateCell.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnUpdateCell.setId(BUTTON_UPDATE_CELL_ID);

		initGridForm.setMargin(true);
		TextField txtSoHang = new TextField(messageSourceAccessor.getMessage(Messages.UI_LABEL_ROW));
		txtSoHang.setRequired(true);
		txtSoHang.setRequiredError(messageSourceAccessor.getMessage(Messages.UI_MESSAGES_REQUIREMENT));
		txtSoHang.setNullRepresentation("");
		txtSoHang.setConverter(Integer.class);
		txtSoHang.addValidator(new IntegerRangeValidator(messageSourceAccessor.getMessage(Messages.UI_LABEL_ID_ERROR_MESSAGES_NUMBERLESSTHAN)+" 999", 0, 999));
		initGridForm.addComponent(txtSoHang);

		TextField txtSoCot = new TextField(messageSourceAccessor.getMessage(Messages.UI_LABEL_COLUMN));
		txtSoCot.setRequired(true);
		txtSoCot.setNullRepresentation("");
		txtSoCot.setRequiredError(messageSourceAccessor.getMessage(Messages.UI_MESSAGES_REQUIREMENT));
		txtSoCot.setConverter(Integer.class);
		txtSoCot.setNullSettingAllowed(true);
		txtSoCot.addValidator(new IntegerRangeValidator(messageSourceAccessor.getMessage(Messages.UI_LABEL_ID_ERROR_MESSAGES_NUMBERLESSTHAN)+" 99", 0, 99));
		initGridForm.addComponent(txtSoCot);

		initGridGroup.bind(txtSoCot, "soCot");
		initGridGroup.bind(txtSoHang, "soHang");

		Button btnInitGrid = new Button(messageSourceAccessor.getMessage(Messages.UI_BUTTON_GIRL_CREATE));
		btnInitGrid.setId(BUTTON_INIT_GRID);
		btnInitGrid.addClickListener(this);
		btnInitGrid.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		initGridForm.addComponent(btnInitGrid);

		window = new Window(messageSourceAccessor.getMessage(Messages.UI_BUTTON_GIRL_CREATE));
		window.setWidth(300, Unit.PIXELS);
		window.setResizable(false);
		window.center();
		window.setModal(true);
		window.setContent(initGridForm);

		VerticalLayout editCellContent = new VerticalLayout();
		FormLayout editCellForm = new FormLayout();
		editCellForm.setMargin(true);
		editCellForm.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		editCellForm.addStyleName(ExplorerLayout.EDIT_CELL_FORM);

		ComboBox cbbLoaiO = new ComboBox(messageSourceAccessor.getMessage(Messages.UI_VIEW_TABLE_HEADER_TYPE));
		cbbLoaiO.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		for (CommonUtils.ELoaiO loaiO : CommonUtils.ELoaiO.values()) {
			cbbLoaiO.addItem(new Long(loaiO.getCode()));
			cbbLoaiO.setItemCaption(new Long(loaiO.getCode()), loaiO.getName());
		}

		cbbLoaiO.setRequired(true);
		cbbLoaiO.setRequiredError(messageSourceAccessor.getMessage(Messages.UI_MESSAGES_REQUIREMENT));
		editCellForm.addComponent(cbbLoaiO);

		textArea = new RichTextArea(messageSourceAccessor.getMessage(Messages.UI_LABEL));
		textArea.addStyleName(ExplorerLayout.TEXT_AREA);
		textArea.setWidth(100, Unit.PERCENTAGE);
		textArea.setHeight("350px");
		editCellForm.addComponent(textArea);

		// Mã nhóm
		TextField txtMaNhom = new TextField(messageSourceAccessor.getMessage(Messages.UI_LABEL_ID));
		txtMaNhom.setWidth("100%");
		txtMaNhom.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				maNhomCurrent = (String) event.getProperty().getValue();
			}
		});
		
		editCellForm.addComponent(txtMaNhom);
		ComboBox cbbDisplayButton = new ComboBox(messageSourceAccessor.getMessage(Messages.UI_VIEW_TABLE_DISPLAY_BUTTON));
		cbbDisplayButton.setWidth("40%");
		cbbDisplayButton.addItem(new Long(0));
		cbbDisplayButton.setItemCaption(new Long(0), "Hiển thị");
		cbbDisplayButton.addItem(new Long(1));
		cbbDisplayButton.setItemCaption(new Long(1), "Ẩn");
		cbbDisplayButton.setValue(new Long(0));
		cbbDisplayButton.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				valueDisplayButton = (Long) event.getProperty().getValue();
			}
		});
		editCellForm.addComponent(cbbDisplayButton);
		
		Grid gridBieuMau = new Grid();
		editCellForm.addComponent(gridBieuMau);

		gridBieuMau.setWidth(WIDTH_FULL, Unit.PERCENTAGE);

		List<Long> tranThaiBm = null;
		if (bieuMauWM.getTrangThaiSuDung() != null) {
			tranThaiBm = new ArrayList<Long>();
			tranThaiBm.add(new Long(bieuMauWM.getTrangThaiSuDung().getId()));
		}
		List<BieuMau> bmConList = bieuMauService.getBieuMauFind(null, null,
				new Long(CommonUtils.ELoaiBieuMau.BM_CON.getCode()), tranThaiBm, -1, -1);
		BeanItemContainer<BieuMau> bmContainer = new BeanItemContainer<BieuMau>(BieuMau.class, bmConList);

		gridBieuMau.setContainerDataSource(bmContainer);
		gridBieuMau.setSelectionMode(SelectionMode.SINGLE);
		gridBieuMau.setColumnOrder("ma", "ten");
		gridBieuMau.setColumns("ma", "ten");
		gridBieuMau.getColumn("ma").setHeaderCaption(messageSourceAccessor.getMessage(Messages.UI_LABEL_ID));
		gridBieuMau.getColumn("ten").setHeaderCaption(messageSourceAccessor.getMessage(Messages.UI_LABEL_NAME));

		gridBieuMau.addSelectionListener(selectionEvent -> {
			Object selected = gridBieuMau.getSelectionModel().getSelectedRows();
			if (selected != null) {
				btnUpdateCell.setData(selected);
			}
		});

		HeaderRow filteringHeader = gridBieuMau.appendHeaderRow();
		filterMa = getColumnFilter(gridBieuMau, "ma");
		TextField filterTen = getColumnFilter(gridBieuMau, "ten");
		filteringHeader.getCell("ma").setComponent(filterMa);
		filteringHeader.getCell("ten").setComponent(filterTen);

		Button btnAddTruongThongTin = new Button("Thêm mới");
		btnAddTruongThongTin.setId(BTN_ADD_TRUONG_THONG_TIN);
		btnAddTruongThongTin.addClickListener(this);
		CssLayout addTruongThongTinWrap = new CssLayout();
		addTruongThongTinWrap.setWidth("100%");
		addTruongThongTinWrap.addStyleName(ExplorerLayout.BTN_POPUP_GROUP_CENTER);
		addTruongThongTinWrap.addComponent(btnAddTruongThongTin);
		addTruongThongTinWrap.setVisible(false);
		editCellForm.addComponent(addTruongThongTinWrap);

		CssLayout tttTblWrap = new CssLayout();
		tttTblWrap.setWidth("100%");
		tttTblWrap.setVisible(false);
		editCellForm.addComponent(tttTblWrap);

		BeanItemContainer<TruongThongTin> container = new BeanItemContainer<TruongThongTin>(TruongThongTin.class,
				new ArrayList());

		List<Long> trangThai = null;
		WorkflowManagement<TruongThongTin> truongThongTinWM = new WorkflowManagement<TruongThongTin>(
				EChucNang.E_FORM_TRUONG_THONG_TIN.getMa());
		if (truongThongTinWM.getTrangThaiSuDung() != null) {
			trangThai = new ArrayList();
			trangThai.add(new Long(truongThongTinWM.getTrangThaiSuDung().getId()));
		}
		paginationBar = new TruongThongTinPaginationBar(container, truongThongTinService,
				ThamSo.POPUP_PAGE_SIZE);
		
		paginationBar.search(null, null, trangThai, null, null);

		CssLayout truongTTSearch = new CssLayout();
		truongTTSearch.addStyleName(ExplorerLayout.TRUONG_TT_SEACH);
		truongTTSearch.setWidth("100%");

		tttTblWrap.addComponent(truongTTSearch);

		CssLayout truongTTSearchItem = new CssLayout();
		truongTTSearchItem.addStyleName(ExplorerLayout.TRUONG_TT_SEACH_FIELD);
		truongTTSearch.addComponent(truongTTSearchItem);

		txtMa = new TextField(messageSourceAccessor.getMessage(Messages.UI_LABEL_ID));
		txtMa.setWidth("100%");
		truongTTSearchItem.addComponent(txtMa);

		truongTTSearchItem = new CssLayout();
		truongTTSearchItem.addStyleName(ExplorerLayout.TRUONG_TT_SEACH_FIELD);
		truongTTSearch.addComponent(truongTTSearchItem);
		TextField txtTen = new TextField(messageSourceAccessor.getMessage(Messages.UI_LABEL_NAME));
		txtTen.setWidth("100%");
		truongTTSearchItem.addComponent(txtTen);

		txtMa.addTextChangeListener(new TextChangeListener() {
			@Override
			public void textChange(TextChangeEvent event) {
				String ma = event.getText();
				MaChange=ma;
				flagMaChange=true;
				if (ma != null) {
					ma = "%" + ma.trim() + "%";
				}
				String ten = txtTen.getValue();
				if (ten != null) {
					ten = "%" + ten.trim() + "%";
				}
				List<Long> trangThaiSD = null;
				if (truongThongTinWM.getTrangThaiSuDung() != null) {
					trangThaiSD = new ArrayList();
					trangThaiSD.add(new Long(truongThongTinWM.getTrangThaiSuDung().getId()));
				}
				paginationBar.search(ma, ten, trangThaiSD, null, null);
				flagMaChange=false;
			}
		});

		txtTen.addTextChangeListener(new TextChangeListener() {
			@Override
			public void textChange(TextChangeEvent event) {
				String ten = event.getText();
				if (ten != null) {
					ten = "%" + ten.trim() + "%";
				}
				String ma = txtMa.getValue();
				if (ma != null) {
					ma = "%" + ma.trim() + "%";
				}
				List<Long> trangThaiSD = null;
				if (truongThongTinWM.getTrangThaiSuDung() != null) {
					trangThaiSD = new ArrayList();
					trangThaiSD.add(new Long(truongThongTinWM.getTrangThaiSuDung().getId()));
				}
				paginationBar.search(ma, ten, trangThaiSD, null, null);
			}
		});

		Table truongThongTinTbl = new Table();
		truongThongTinTbl.setWidth("100%");
		tttTblWrap.addComponent(truongThongTinTbl);

		truongThongTinTbl.setContainerDataSource(container);
		truongThongTinTbl.addStyleName(ExplorerLayout.DATA_TABLE);
		truongThongTinTbl.setSizeFull();
		truongThongTinTbl.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		truongThongTinTbl.setColumnHeader("ma", messageSourceAccessor.getMessage(Messages.UI_VIEW_TABLE_HEADER_ID));
		truongThongTinTbl.setColumnHeader("ten", messageSourceAccessor.getMessage(Messages.UI_VIEW_TABLE_HEADER_NAME));
		truongThongTinTbl.setColumnHeader("kieuDuLieu", messageSourceAccessor.getMessage(Messages.UI_VIEW_TABLE_HEADER_TYPE));

		truongThongTinTbl.addGeneratedColumn(" 	kieuDuLieu", new ColumnGenerator() {
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				TruongThongTin current = (TruongThongTin) itemId;
				if (current != null && current.getKieuDuLieu() != null) {
					return CommonUtils.EKieuDuLieu.getTen(current.getKieuDuLieu().intValue());
				}
				return null;
			}
		});

		truongThongTinTbl.addGeneratedColumn("readonly", new ColumnGenerator() {

			@Override
			public Object generateCell(final Table source, final Object itemId, Object columnId) {
				TruongThongTin current = (TruongThongTin) itemId;
				List<TruongThongTin> readOnlyList = cellCurrent.getReadOnly();
				
				CheckBox readOnly = new CheckBox();
				if (readOnlyList!=null && readOnlyList.contains(current)) {
					readOnly.setValue(true);
					tttReadOnlyList.add(current);
				}
				readOnly.addValueChangeListener(new ValueChangeListener() {

					@Override
					public void valueChange(ValueChangeEvent event) {
						Boolean chk = (Boolean) event.getProperty().getValue();
						if (chk != null && chk) {
							if(!tttReadOnlyList.contains(current)) {
								tttReadOnlyList.add(current);
//								CheckBox cb=(CheckBox)truongThongTinTbl.getColumnGenerator("selection");
//								cb.setValue(true);
							}
							
						} else {
							tttReadOnlyList.remove(current);
						}
					}
				});
				return readOnly;
			}
		});

		truongThongTinTbl.addGeneratedColumn("constraint", new ColumnGenerator() {

			@Override
			public Object generateCell(final Table source, final Object itemId, Object columnId) {
				TruongThongTin current = (TruongThongTin) itemId;

				if (current != null && current.getKieuDuLieu() != null) {
					if (current.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.CHUC_VU.getMa()
							|| current.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DON_VI.getMa()
							|| current.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.GROUP.getMa()
							|| current.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.USER.getMa()) {
						TextField txtConstrant = new TextField();
						txtConstrant.addValueChangeListener(new ValueChangeListener() {
							@Override
							public void valueChange(ValueChangeEvent event) {
								String value = (String) event.getProperty().getValue();
								if (value != null && !value.isEmpty()) {
									constrantsMap.put(current, value);
								}
							}
						});
						return txtConstrant;
					}

				}

				return null;
			}
		});
		truongThongTinTbl.addGeneratedColumn("filterType", new ColumnGenerator() {
			@Override
			public Object generateCell(final Table source, final Object itemId, Object columnId) {
				TruongThongTin current = (TruongThongTin) itemId;
				if (current != null && current.getKieuDuLieu() != null
						&& current.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.USER.getMa()) {
					CheckBox filterType = new CheckBox();
					filterType.addValueChangeListener(new ValueChangeListener() {
						@Override
						public void valueChange(ValueChangeEvent event) {
							Boolean chk = (Boolean) event.getProperty().getValue();
							if (chk != null && chk) {
								tttFilterList.add(current);
							} else {
								tttFilterList.remove(current);
							}
						}
					});
					return filterType;
				}
				return null;
			}
		});

		truongThongTinTbl.addGeneratedColumn("selection", new ColumnGenerator() {

			@Override
			public Object generateCell(final Table source, final Object itemId, Object columnId) {
				
				TruongThongTin current = (TruongThongTin) itemId;
				
					CheckBox selection = new CheckBox();
					
					if (flagMaChange && current.getMa().equalsIgnoreCase(MaChange)) {
						selection.setValue(true);
						tttSelectionList.add(current);
					}else if(!flagMaChange & current.getMa().equalsIgnoreCase(txtMa.getValue())) {
						selection.setValue(true);
						tttSelectionList.add(current);
					}
					selection.addValueChangeListener(new ValueChangeListener() {

						@Override
						public void valueChange(ValueChangeEvent event) {
							Boolean chk = (Boolean) event.getProperty().getValue();
							if (chk != null && chk) {
								if (!tttSelectionList.contains(current)) {
									tttSelectionList.add(current);
								}
							} else {
								tttSelectionList.remove(current);
							}
						}
					});
					return selection;
				
			
			}
		});

		tttTblWrap.addComponent(paginationBar);
		truongThongTinTbl.setColumnHeader("selection", "");
		truongThongTinTbl.setColumnHeader("readonly", "Readonly");
		truongThongTinTbl.setColumnHeader("constraint", messageSourceAccessor.getMessage(Messages.UI_VIEW_TABLE_HEADER_REFERENCEFIELD));
		truongThongTinTbl.setColumnHeader("filterType", messageSourceAccessor.getMessage(Messages.UI_VIEW_TABLE_HEADER_FILTERBYORGANIZATION));
		truongThongTinTbl.setRowHeaderMode(RowHeaderMode.INDEX);

		truongThongTinTbl.setVisibleColumns("selection", "ma", "ten", "kieuDuLieu", "readonly", "constraint",
				"filterType");

		btnUpdateCell.addClickListener(this);
		CssLayout btnWrap = new CssLayout();
		btnWrap.setWidth("100%");
		btnWrap.addStyleName(ExplorerLayout.BTN_POPUP_GROUP_CENTER);
		btnWrap.addComponent(btnUpdateCell);

		editCellContent.addComponent(editCellForm);
		editCellContent.addComponent(btnWrap);

		editCellItem.addItemProperty("loaiO", new ObjectProperty<Long>(new Long(CommonUtils.ELoaiO.TEXT.getCode())));
		editCellItem.addItemProperty("textVal", new ObjectProperty<String>(""));
		editCellGroup.setItemDataSource(editCellItem);
		textArea.setVisible(true);
		gridBieuMau.setVisible(false);
		txtMaNhom.setVisible(false);
		// gridTruongThongtin.setVisible(false);

		editCellGroup.bind(cbbLoaiO, "loaiO");
		editCellGroup.bind(textArea, "textVal");

		cbbLoaiO.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 904892993728377291L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (event.getProperty().getValue() != null) {
					Long value = (Long) event.getProperty().getValue();
					if (value != null && CommonUtils.ELoaiO.TEXT.getCode() == value.intValue()) {
						textArea.setVisible(true);
						gridBieuMau.setVisible(false);
						txtMaNhom.setVisible(false);
						cbbDisplayButton.setVisible(false);
						// gridTruongThongtin.setVisible(false);
						tttTblWrap.setVisible(false);
						addTruongThongTinWrap.setVisible(false);
					} else if (value != null && CommonUtils.ELoaiO.BIEU_MAU.getCode() == value.intValue()) {
						textArea.setVisible(false);
						gridBieuMau.setVisible(true);
						cbbDisplayButton.setVisible(true);
						//tuanna
						txtMaNhom.setVisible(true);
						// gridTruongThongtin.setVisible(false);
						tttTblWrap.setVisible(false);
						addTruongThongTinWrap.setVisible(false);
					} else if (value != null && CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode() == value.intValue()) {
						textArea.setVisible(false);
						gridBieuMau.setVisible(false);
						txtMaNhom.setVisible(false);
						cbbDisplayButton.setVisible(false);
						// gridTruongThongtin.setVisible(true);
						addTruongThongTinWrap.setVisible(true);
						tttTblWrap.setVisible(true);
						String ma = txtMa.getValue();
						if (ma != null) {
							ma = "%" + ma.trim() + "%";
						}
						String ten = txtTen.getValue();
						if (ten != null) {
							ten = "%" + ten.trim() + "%";
						}
						List<Long> trangThaiSD = null;
						if (truongThongTinWM.getTrangThaiSuDung() != null) {
							trangThaiSD = new ArrayList();
							trangThaiSD.add(new Long(truongThongTinWM.getTrangThaiSuDung().getId()));
						}
						paginationBar.search(ma, ten, trangThaiSD, null, null);
					}
				}
			}
		});

		editCellWindow = new Window( messageSourceAccessor.getMessage(Messages.UI_VIEW_CELLINFO));
		editCellWindow.setWidth(90, Unit.PERCENTAGE);
		editCellWindow.setHeight(90, Unit.PERCENTAGE);
		editCellWindow.center();
		editCellWindow.setModal(true);
		editCellWindow.setContent(editCellContent);
	}

	private TextField getColumnFilter(final Grid sample, final Object columnId) {
		TextField filter = new TextField();
		filter.setWidth("100%");
		filter.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = -458756607677822455L;
			SimpleStringFilter filter = null;

			@Override
			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) sample.getContainerDataSource();
				if (filter != null) {
					f.removeContainerFilter(filter);
				}
				filter = new SimpleStringFilter(columnId, event.getText(), true, true);
				f.addContainerFilter(filter);

				sample.cancelEditor();
			}
		});
		return filter;
	}

	public void initBieuMauGrid() {
		CssLayout bmGridWrap = new CssLayout();
		bmGridWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		bmGridWrap.addStyleName(ExplorerLayout.BIEU_MAU_GRID_WRAP);

		bieuMauGrid = new GridLayout();
		bieuMauGrid.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		bieuMauGrid.addStyleName(ExplorerLayout.BIEU_MAU_GRID);
		bmGridWrap.addComponent(bieuMauGrid);

		addComponent(bmGridWrap);

		formulaContainer = new CssLayout();
		formulaContainer.setWidth("100%");
		formulaContainer.addStyleName(ExplorerLayout.FORMULA_CONTAINER);

		addComponent(formulaContainer);
	}

	public void taoLuoi() {
		if (soCot != null && soHang != null && soCot > 0 && soHang > 0 && bieuMauGrid != null) {
			cellIdFieldGroup = new FieldGroup();
			cellIdFieldGroup.setItemDataSource(cellIdtemValue);
			hangCotArr = new CellDetail[soHang][soCot];
			bieuMauGrid.removeAllComponents();
			bieuMauGrid.setColumns(soCot + 1);
			bieuMauGrid.setRows(soHang + 1);
			int viTriO = 0;
			for (int h = -1; h < soHang; h++) {
				for (int c = 0; c <= soCot; c++) {
					if (h == -1) {
						if (c < soCot) {
							addCellFirstRow(bieuMauGrid, c, h);
						} else {
							addLastCell(bieuMauGrid, c, h);
						}
					} else {
						if (c < soCot) {
							CellDetail cellDetail = new CellDetail();
							cellDetail.setColIndex(c);
							cellDetail.setRowIndex(h);
							cellDetail.setDeleted(false);
							cellDetail.setColSpan(1);
							cellDetail.setRowSpan(1);
							hangCotArr[h][c] = cellDetail;

							addCellDetailContent(cellDetail, bieuMauGrid, c, h, c, h, null, viTriO);
							viTriO++;
						} else if (c == soCot) {
							addLastCell(bieuMauGrid, c, h);
						}
					}
				}
			}

		}

		initFormulaWrap();
	}

	public AbsoluteLayout addCellDetailContent(CellDetail cellDetail, GridLayout bieuMauGrid, int column1, int row1,
			int column2, int row2, String cellId, int viTriO) {
		AbsoluteLayout cellDetailWrap = new AbsoluteLayout();
		cellDetailWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		cellDetailWrap.setHeight(100, Unit.PIXELS);
		cellDetailWrap.setStyleName(ExplorerLayout.CELL_DETAIL_WRAP);

		Button btnEdit = new Button();
		btnEdit.setIcon(FontAwesome.PENCIL);
		btnEdit.setId(BUTTON_EDIT_CELL_ID);
		btnEdit.addStyleName(ExplorerLayout.BUTTON_EDIT_CELL);
		btnEdit.setWidth("24px");
		btnEdit.setHeight("24px");
		btnEdit.addClickListener(this);
		btnEdit.setData(cellDetail);
//		System.out.println(cellDetail);
		cellDetailWrap.addComponent(btnEdit, "bottom: 1px; right: 1px");
		if (cellDetail.getColSpan() > 1) {
			Button btnTachCell = new Button();
			btnTachCell.setIcon(FontAwesome.PLUS);
			btnTachCell.setId(BUTTON_TACH_CELL_ID);
			btnTachCell.addStyleName(ExplorerLayout.BUTTON_ADD_ROW);
			btnTachCell.setWidth("20px");
			btnTachCell.setHeight("20px");
			btnTachCell.addClickListener(this);
			btnTachCell.setData(cellDetail);

//		System.out.println(cellDetail);
			cellDetailWrap.addComponent(btnTachCell, "bottom: 16px; right: 26px");
		}
		if (cellDetail.getRowSpan() > 1) {
			Button btnTachCell = new Button();
			btnTachCell.setIcon(FontAwesome.PLUS);
			btnTachCell.setId(BUTTON_TACH_ROW_ID);
			btnTachCell.addStyleName(ExplorerLayout.BUTTON_ADD_ROW);
			btnTachCell.setWidth("20px");
			btnTachCell.setHeight("20px");
			btnTachCell.addClickListener(this);
			btnTachCell.setData(cellDetail);

//		System.out.println(cellDetail);
			cellDetailWrap.addComponent(btnTachCell, "top: 5px; right: 5px");
		}

		if (cellDetail.getColSpan() > 1) {
			Button btnTachCell = new Button();
			btnTachCell.setIcon(FontAwesome.PLUS);
			btnTachCell.setId(BUTTON_TACH_CELL_ID);
			btnTachCell.addStyleName(ExplorerLayout.BUTTON_ADD_ROW);
			btnTachCell.setWidth("20px");
			btnTachCell.setHeight("20px");
			btnTachCell.addClickListener(this);
			btnTachCell.setData(cellDetail);

//		System.out.println(cellDetail);
			cellDetailWrap.addComponent(btnTachCell, "bottom: 16px; right: 26px");
		}

		if (column2 < soCot - 1) {
			Button btnGhepSangPhai = new Button();
			btnGhepSangPhai.addClickListener(this);
			btnGhepSangPhai.setId(BUTTON_GHEP_PHAI_ID);
			btnGhepSangPhai.addStyleName(ExplorerLayout.BUTTON_MERGE_CELL);
			btnGhepSangPhai.addStyleName(ExplorerLayout.BUTTON_MERGE_RIGHT_CELL);
			btnGhepSangPhai.setIcon(FontAwesome.REMOVE);
			btnGhepSangPhai.setWidth("24px");
			btnGhepSangPhai.setHeight("24px");
			btnGhepSangPhai.setData(cellDetail);
			cellDetailWrap.addComponent(btnGhepSangPhai, "top: 50%; right: -12px");
		}

		if (row2 < soHang - 1 && row2 != soHang - 1) {
			Button btnGhepXuong = new Button();
			btnGhepXuong.setId(BUTTON_GHEP_XUONG_ID);
			btnGhepXuong.addClickListener(this);
			btnGhepXuong.setIcon(FontAwesome.REMOVE);
			btnGhepXuong.setWidth("24px");
			btnGhepXuong.setHeight("24px");
			btnGhepXuong.setData(cellDetail);
			btnGhepXuong.addStyleName(ExplorerLayout.BUTTON_MERGE_CELL);
			btnGhepXuong.addStyleName(ExplorerLayout.BUTTON_MERGE_BOTTOM_CELL);
			cellDetailWrap.addComponent(btnGhepXuong, "bottom: -12px; left: 50%");
		}
		bieuMauGrid.setColumnExpandRatio(column1, 1F);
//		System.out.println(column1+" "+ row1+" "+ column2+" "+ row2);
		bieuMauGrid.addComponent(cellDetailWrap, column1, row1 + 1, column2, row2 + 1);
		return cellDetailWrap;
	}

	public AbsoluteLayout addCellFirstRow(GridLayout bieuMauGrid, int column, int row) {

		AbsoluteLayout cellDetailWrap = new AbsoluteLayout();
		cellDetailWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		cellDetailWrap.setHeight(100, Unit.PIXELS);
		cellDetailWrap.setStyleName(ExplorerLayout.CELL_DETAIL_WRAP);
		if (row < soHang - 1) {
			Button btnAddColumn = new Button();
			btnAddColumn.setIcon(FontAwesome.PLUS);
			btnAddColumn.setDescription(messageSourceAccessor.getMessage(Messages.UI_BUTTON_ADDCOLUMN));
			btnAddColumn.setId(BUTTON_ADD_COLUMN_ID);
			btnAddColumn.addStyleName(ExplorerLayout.BUTTON_ADD_ROW);
			btnAddColumn.setWidth("24px");
			btnAddColumn.setHeight("24px");
			btnAddColumn.addClickListener(this);
			btnAddColumn.setData(column);
			cellDetailWrap.addComponent(btnAddColumn, "top: 50%; right: -12px");

//			Button btnRemoveColumn = new Button();
//			btnRemoveColumn.setIcon(FontAwesome.PLUS);
//			btnRemoveColumn.setDescription("Thêm cột");
//			btnRemoveColumn.setId(BUTTON_ADD_COLUMN_ID);
//			btnRemoveColumn.addStyleName(ExplorerLayout.BUTTON_ADD_ROW);
//			btnRemoveColumn.setWidth("24px");
//			btnRemoveColumn.setHeight("24px");
//			btnRemoveColumn.addClickListener(this);
//			btnRemoveColumn.setData(column);
//			cellDetailWrap.addComponent(btnRemoveColumn, "top: 50%; left: 50px");
		}
		bieuMauGrid.addComponent(cellDetailWrap, column, row + 1);
		return cellDetailWrap;
	}

	public AbsoluteLayout addLastCell(GridLayout bieuMauGrid, int column, int row) {
		AbsoluteLayout cellDetailWrap = new AbsoluteLayout();
		cellDetailWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		cellDetailWrap.setHeight(100, Unit.PIXELS);
		cellDetailWrap.setStyleName(ExplorerLayout.CELL_DETAIL_WRAP);

		if (row > -1) {
			Button btnAddRow = new Button();
			btnAddRow.setIcon(FontAwesome.PLUS);
			btnAddRow.setId(BUTTON_ADD_ROW_ID);
			btnAddRow.addStyleName(ExplorerLayout.BUTTON_ADD_ROW);
			btnAddRow.setWidth("24px");
			btnAddRow.setHeight("24px");
			btnAddRow.addClickListener(this);
			btnAddRow.setData(new Integer(row));
			cellDetailWrap.addComponent(btnAddRow, "bottom: 0; left: 50%;");

			Button btnRemoveRow = new Button();
			btnRemoveRow.setIcon(FontAwesome.MINUS);
			btnRemoveRow.setId(BUTTON_REMOVE_ROW_ID);
			btnRemoveRow.addStyleName("btn-remove-row");
			btnRemoveRow.setWidth("24px");
			btnRemoveRow.setHeight("24px");
			btnRemoveRow.addClickListener(this);
			btnRemoveRow.setData(new Integer(row));
			cellDetailWrap.addComponent(btnRemoveRow, "left: 0; top: 50%;");

//			TODO 
			CheckBox chkHangLap = new CheckBox();
			chkHangLap.addStyleName(ExplorerLayout.CHECK_BOX_LAP_HANG);
			cellDetailWrap.addComponent(chkHangLap, "top: 50%; left: 50%;");
			Boolean check = selectedLapHang.get(new Long(row));
			chkHangLap.setValue(check != null && check);
			chkHangLap.setReadOnly(Constants.ACTION_VIEW.equals(action));
			chkHangLap.addValueChangeListener(new ValueChangeListener() {
				@Override
				public void valueChange(ValueChangeEvent event) {
					Boolean check = (Boolean) event.getProperty().getValue();
					if (check != null && check) {
						selectedLapHang.put(new Long(row), true);
					} else {
						selectedLapHang.put(new Long(row), false);
					}
				}
			});
		}

		bieuMauGrid.setColumnExpandRatio(column, 0.5F);
		bieuMauGrid.addComponent(cellDetailWrap, column, row + 1);
		return cellDetailWrap;
	}

	public void ghepSangPhai(Button button) {
		try {
			cloneGrid();
			CellDetail cellDetail = (CellDetail) button.getData();
			if (cellDetail != null) {
				Integer rowIndex = cellDetail.getRowIndex();
				Integer colIndex = cellDetail.getColIndex();
				System.out.println("------------colIndex: " + colIndex);
				if (rowIndex != null && colIndex != null && hangCotArr != null) {
					CellDetail currentCell = hangCotArr[rowIndex.intValue()][colIndex.intValue()];
					if (currentCell != null) {
						int colSpan = currentCell.getColSpan();
						int rowSpan = currentCell.getRowSpan();
						if (colIndex.intValue() + colSpan < soCot) {
							CellDetail cellPhai = hangCotArr[rowIndex.intValue()][colIndex.intValue() + colSpan];
							if (cellPhai != null && !cellPhai.getDeleted() && cellPhai.getRowSpan() == rowSpan) {
								currentCell.setColSpan(colSpan + cellPhai.getColSpan());
								cellPhai.setDeleted(true); // Đã xóa
							//	cellPhai.setParent(currentCell);
								cellContentMap.remove(rowIndex.intValue() + "_" + colIndex.intValue() + colSpan);
								refreshLuoi();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void TachSangTrai(Button button) {
		try {
			cloneGrid();
			CellDetail cellDetail = (CellDetail) button.getData();
			if (cellDetail != null) {
				Integer rowIndex = cellDetail.getRowIndex();
				Integer colIndex = cellDetail.getColIndex();
				System.out.println("------------colIndex: " + colIndex);
				if (rowIndex != null && colIndex != null && hangCotArr != null) {
					CellDetail currentCell = hangCotArr[rowIndex.intValue()][colIndex.intValue()];
					if (currentCell != null) {
						int colSpan = currentCell.getColSpan();
						int rowSpan = currentCell.getRowSpan();
						if (colIndex.intValue() + 1 < soCot) {
							CellDetail cellPhai = hangCotArr[rowIndex.intValue()][colIndex.intValue() + 1];
							if (cellPhai != null && cellPhai.getDeleted() && cellPhai.getRowSpan() == rowSpan) {
								currentCell.setColSpan(1);
								cellPhai.setDeleted(false); // Đã xóa
								cellPhai.setColSpan(colSpan - 1);
								// cellPhai.setParent(currentCell);

							} else {
								hangCotArr[rowIndex.intValue()][colIndex.intValue()
										+ 1] = (CellDetail) hangCotArr[rowIndex.intValue()][colIndex.intValue()]
												.clone();
								cellPhai = (CellDetail) hangCotArr[rowIndex.intValue()][colIndex.intValue() + 1];
								currentCell.setColSpan(1);
								cellPhai.setColIndex(colIndex + 1);
								cellPhai.setColSpan(colSpan - 1);
							}
							refreshLuoi();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void TachHang(Button button) {
		try {
			cloneGrid();
 			CellDetail cellDetail = (CellDetail) button.getData();
			if (cellDetail != null) {
				Integer rowIndex = cellDetail.getRowIndex();
				Integer colIndex = cellDetail.getColIndex();

				if (rowIndex != null && colIndex != null && hangCotArr != null) {
					CellDetail currentCell = hangCotArr[rowIndex.intValue()][colIndex.intValue()];
					if (currentCell != null) {
						int colSpan = currentCell.getColSpan();
						int rowSpan = currentCell.getRowSpan();
						if (rowIndex.intValue() + 1 < soHang) {
							CellDetail cellDuoi = hangCotArr[rowIndex.intValue() + 1][colIndex.intValue()];
							if (cellDuoi != null && cellDuoi.getDeleted() && cellDuoi.getColSpan() == colSpan) {
								currentCell.setRowSpan(1);
								cellDuoi.setDeleted(false);
								cellDuoi.setRowSpan(rowSpan - 1);
								cellDuoi.setParent(currentCell);

							} else {
								currentCell.setRowSpan(1);
								hangCotArr[rowIndex.intValue() + 1][colIndex
										.intValue()] = (CellDetail) hangCotArr[rowIndex.intValue()][colIndex.intValue()]
												.clone();
								cellDuoi = hangCotArr[rowIndex.intValue() + 1][colIndex.intValue()];
								cellDuoi.setRowIndex(rowIndex + 1);
								cellDuoi.setRowSpan(rowSpan - 1);
								cellDuoi.setParent(currentCell);

							}
							refreshLuoi();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void ghepXuongDuoi(Button button) {
		try {
			cloneGrid();
			CellDetail cellDetail = (CellDetail) button.getData();
			if (cellDetail != null) {
				Integer rowIndex = cellDetail.getRowIndex();
				Integer colIndex = cellDetail.getColIndex();

				if (rowIndex != null && colIndex != null && hangCotArr != null) {
					CellDetail currentCell = hangCotArr[rowIndex.intValue()][colIndex.intValue()];
					if (currentCell != null) {
						int colSpan = currentCell.getColSpan();
						int rowSpan = currentCell.getRowSpan();
						if (rowIndex.intValue() + rowSpan < soHang) {
							CellDetail cellDuoi = hangCotArr[rowIndex.intValue() + rowSpan][colIndex.intValue()];
							if (cellDuoi != null && !cellDuoi.getDeleted() && cellDuoi.getColSpan() == colSpan) {
								currentCell.setRowSpan(rowSpan + cellDuoi.getRowSpan());
								cellDuoi.setDeleted(true); // Đã xóa
								cellContentMap.remove(rowIndex.intValue() + rowSpan + "_" + colIndex.intValue());
								// cellDuoi.setParent(currentCell);
								refreshLuoi();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void cloneGrid() {
		try {
			soCotHistory = soCot;
			soHangHistory = soHang;
			hangCotArrHistory = new CellDetail[soHang][soCot];
			for (int h = 0; h < soHang; h++) {
				for (int c = 0; c < soCot; c++) {
					CellDetail cell = (CellDetail) (hangCotArr[h] != null && hangCotArr[h][c] != null
							? hangCotArr[h][c].clone()
							: null);
					hangCotArrHistory[h][c] = cell;
				}
			}
			cellContentMapHistory = HashMapStringCopy(cellContentMap);
			selectedLapHangHistory = HashMapLongCopy(selectedLapHang);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public HashMap<String, Object> HashMapStringCopy(Map<String, Object> cellContentMap2) {
		HashMap<String, Object> copy = new HashMap<String, Object>();
		for (Map.Entry<String, Object> entry : cellContentMap2.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}
		return copy;
	}

	public HashMap<Long, Boolean> HashMapLongCopy(Map<Long, Boolean> selectedLapHang2) {
		HashMap<Long, Boolean> copy = new HashMap<Long, Boolean>();
		for (Map.Entry<Long, Boolean> entry : selectedLapHang2.entrySet()) {
			copy.put(entry.getKey(), (Boolean) entry.getValue());
		}
		return copy;
	}

	public void themHang(Button button) {
		try {
			cloneGrid();
			Integer rowIndex = (Integer) button.getData();
			CellDetail cell = null;
			if (rowIndex != null) {
				int rowCopyIndex = rowIndex.intValue();
				// danh sach cell copy
				CellDetail[] cellArrTmp = new CellDetail[soCot];
				for (int i = 0; i < soCot; i++) {
					CellDetail cellCopy = hangCotArr[rowCopyIndex][i];
					cell = new CellDetail();
					cell.setColIndex(i);
					cell.setRowIndex(rowCopyIndex + 1);
					cell.setRowSpan(1);
					// dat mac dinh
					cell.setDeleted(false);
					cell.setColSpan(1);
					cellArrTmp[i] = cell;

					if (cellCopy != null && rowCopyIndex < soHang - 1) {
						if (!cellCopy.getDeleted()) {
							if (cellCopy.getRowSpan() > 1) {
								cellCopy.setRowSpan(cellCopy.getRowSpan() + 1);
								// tao cell moi da deleted
								cell.setDeleted(true);
								cell.setColSpan(1);
								cell.setParent(cellCopy);
							} else {
								cell.setColSpan(cellCopy.getColSpan());
								cell.setDeleted(false);
							}
						} else {
							CellDetail cellDuoi = hangCotArr[rowCopyIndex + 1][i];
							if (cellDuoi != null && !cellDuoi.getDeleted()) {
								CellDetail parentCellCopy = getParentRoot(cellCopy);
								if (parentCellCopy != null && parentCellCopy.getColIndex() == i) {
									cell.setDeleted(false);
									cell.setColSpan(parentCellCopy != null ? parentCellCopy.getColSpan() : 1);
								} else {
									cell.setDeleted(true);
									cell.setColSpan(1);
									cell.setParent(cellArrTmp[parentCellCopy.getColIndex()]);
								}
							} else if (cellDuoi != null) {
								CellDetail parentCellCopy = getParentRoot(cellCopy);
								CellDetail parentCellDuoi = getParentRoot(cellDuoi);
								if (parentCellCopy == parentCellDuoi) {
									cell.setDeleted(true);
									cell.setColSpan(1);
									if (parentCellCopy.getColIndex() == i) {
										parentCellCopy.setRowSpan(parentCellCopy.getRowSpan() + 1);
									}
									cell.setParent(parentCellCopy);
								} else {
									if (parentCellCopy.getColIndex() == i) {
										cell.setDeleted(false);
										cell.setColSpan(parentCellCopy.getColSpan());
									} else {
										cell.setDeleted(true);
										cell.setColSpan(1);
										cell.setParent(cellArrTmp[parentCellCopy.getColIndex()]);
									}

								}
							}
						}
					}
				}
				if (cellArrTmp != null && cellArrTmp.length == soCot) {
					soHang += 1;
					if (cellContentMap != null) {
						List<String> removeMap = new ArrayList<String>();
						Map<String, Object> newMap = new HashMap<String, Object>();
						for (Entry<String, Object> entry : cellContentMap.entrySet()) {
							if (entry.getKey() != null) {
								String[] arr = entry.getKey().split("_");
								if (arr != null && arr.length == 2 && arr[0].matches("[0-9]+")) {
									if ((new Integer(arr[0])).intValue() > rowCopyIndex) {
										removeMap.add(entry.getKey());
										newMap.put((new Integer(arr[0]) + 1) + "_" + arr[1], entry.getValue());
									}
								}
							}
						}
						cellContentMap.keySet().removeAll(removeMap);
						cellContentMap.putAll(newMap);

					}
					CellDetail[][] hangCotArrTmp = new CellDetail[soHang][soCot];
					Map<Long, Boolean> selectedLapHangTmp = new HashMap<>();
					for (int i = 0; i < soHang; i++) {
						if (i <= rowCopyIndex) {
							hangCotArrTmp[i] = hangCotArr[i];
							selectedLapHangTmp.put(Long.valueOf(i), (Boolean) selectedLapHang.get(Long.valueOf(i)));
						} else if (i == rowCopyIndex + 1) {
							hangCotArrTmp[i] = cellArrTmp;
							selectedLapHangTmp.put(Long.valueOf(i), false);
						} else {
							for (int c = 0; c < soCot; c++) {
								if (hangCotArr[i - 1][c] != null) {
									hangCotArr[i - 1][c].setRowIndex(i);
								}
							}
							hangCotArrTmp[i] = hangCotArr[i - 1];
							selectedLapHangTmp.put(Long.valueOf(i), (Boolean) selectedLapHang.get(Long.valueOf(i - 1)));
						}
					}
					hangCotArr = hangCotArrTmp;

					selectedLapHang = selectedLapHangTmp;
					refreshLuoi();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void themCot(Button button) {
		try {
			cloneGrid();
			Integer columnIndexTmp = (Integer) button.getData();
			CellDetail cell = null;
			if (columnIndexTmp != null) {
				CellDetail[][] hangCotArrTmp = new CellDetail[soHang][soCot + 1];
				int columIndex = columnIndexTmp.intValue();
				for (int h = 0; h < soHang; h++) {
					for (int c = 0; c <= soCot; c++) {
						if (c <= columIndex) {
							hangCotArrTmp[h][c] = hangCotArr[h][c];
						} else if (c == columIndex + 1) {
							// Vi tri insert cell moi
							cell = new CellDetail();
							cell.setColIndex(c);
							cell.setRowIndex(h);
							cell.setDeleted(false);
							cell.setColSpan(1);
							cell.setRowSpan(1);
							hangCotArrTmp[h][c] = cell;

							if (columIndex == 0 && c < soCot) {
								// cot dau tien
								// copy cell ben phai
								CellDetail rightCell = hangCotArr[h][c];
								cell.setDeleted(rightCell.getDeleted());
								CellDetail parent = rightCell.getParent();
								cell.setParent(parent);
								if (parent != null) {
									parent.setColSpan(parent.getColSpan() + 1);
								}
							} else if (c < soCot) {
								CellDetail rightCell = hangCotArr[h][c];
								CellDetail leftCell = hangCotArr[h][c - 1];
								CellDetail leftCellParent = leftCell.getParent();
								CellDetail rightCellParent = rightCell.getParent();
								if ((rightCell.getDeleted() && leftCell.getDeleted() && leftCellParent != null
										&& leftCellParent.equals(rightCellParent))
										|| (rightCell.getDeleted() && !leftCell.getDeleted() && rightCellParent != null
												&& rightCellParent.equals(leftCell))) {
									cell.setDeleted(true);
									cell.setParent(rightCellParent);
									rightCellParent.setColSpan(rightCellParent.getColSpan() + 1);
								}
								if (rightCell.getDeleted() && leftCell.getDeleted()) {
									cell.setDeleted(true);
								}
							}
						} else {
							hangCotArrTmp[h][c] = hangCotArr[h][c - 1];
						}
					}
				}

				hangCotArr = hangCotArrTmp;
				soCot++;
				refreshLuoi();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void xoaHang(Button button) {
		try {
			cloneGrid();
			Integer rowIndex = (Integer) button.getData();
			CellDetail cell = null;
			if (rowIndex != null) {
				int rowCopyIndex = rowIndex.intValue();
				CellDetail[] cellArrTmp = new CellDetail[soCot];
				for (int i = 0; i < soCot; i++) {
					CellDetail cellCurrent = hangCotArr[rowCopyIndex][i];

					CellDetail parent = cellCurrent.getParent();
					if (parent != null) {
						parent.setRowSpan(parent.getRowSpan() - 1);
					}
					String key = rowCopyIndex + "_" + i;
					if (cellContentMap.containsKey(key))
						cellContentMap.remove(key);
				}
				for (int r = rowCopyIndex; r < soHang; r++) {
					if (r < soHang - 1) {
						for (int i = 0; i < soCot; i++) {
							CellDetail cellCurrent = hangCotArr[r + 1][i];
							cellCurrent.setRowIndex(cellCurrent.getRowIndex() - 1);
							String key = (r + 1) + "_" + i;
							String curKey = r + "_" + i;
							if (cellContentMap.containsKey(key)) {
								cellContentMap.put(curKey, cellContentMap.get(key));
								cellContentMap.remove(key);
							}
						}
						hangCotArr[r] = hangCotArr[r + 1];

					} else {
						hangCotArr[r] = null;
					}
				}
				soHang--;
				System.out.println("s");

				refreshLuoi();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public CellDetail getParentRoot(CellDetail cell) {
		if (cell == null || cell.getParent() == null) {
			return null;
		}
		CellDetail cellTmp = cell.getParent();
		while (cellTmp != null && cellTmp.getParent() != null) {
			cellTmp = cellTmp.getParent();
		}
		return cellTmp;
	}

	public void refreshLuoi() {
		if (hangCotArr != null && soHang >= 0 && soCot >= 0) {
			cellIdList.clear();
			cellIdFieldGroup = new FieldGroup();
			cellIdFieldGroup.setItemDataSource(cellIdtemValue);
			bieuMauGrid.removeAllComponents();
			bieuMauGrid.setColumns(soCot + 1);
			bieuMauGrid.setRows(soHang + 1);
			int viTriO = 0;
			for (int hang = -1; hang < soHang; hang++) {

				for (int cot = 0; cot <= soCot; cot++) {
					if (hang == -1) {
						if (cot < soCot) {
							addCellFirstRow(bieuMauGrid, cot, hang);
						} else {
							addLastCell(bieuMauGrid, cot, hang);
						}
					} else {
						if (cot < soCot) {
							CellDetail cellDetail = hangCotArr[hang][cot];
							if (cellDetail != null && cellDetail.getDeleted() != null && !cellDetail.getDeleted()) {
								String cellID = cellDetail.getCellId();
								if (cellIdMap != null && cellIdMap.get(viTriO) != null) {
									cellID = cellIdMap.get(viTriO);
									cellDetail.setCellId(cellID);
								}
								AbsoluteLayout cellDetailWrap = addCellDetailContent(cellDetail, bieuMauGrid, cot, hang,
										cot + cellDetail.getColSpan() - 1, hang + cellDetail.getRowSpan() - 1, cellID,
										viTriO);

								if (cellContentMap != null && cellContentMap.get(hang + "_" + cot) != null) {
									Label label = new Label();
									label.addStyleName(ExplorerLayout.LBL_CELL_CONTENT);
									Object obj = cellContentMap.get(hang + "_" + cot);
									if (obj instanceof String) {
										String str = (String) obj;
										label.setValue(str);
										label.setDescription("<b>"+messageSourceAccessor.getMessage(Messages.UI_LABEL_TEXT)+"</b>");
										label.setContentMode(ContentMode.HTML);
									} else if (obj instanceof BieuMau) {
										CellDetail cellDetailCurrent = hangCotArr[hang][cot];
										String bmLbl = "" + ((BieuMau) obj).getMa();

										String str = "<span class='cell-bm-info'> " + ((BieuMau) obj).getTen() + "("
												+ ((BieuMau) obj).getMa() + ")" + "</span>";

										if (cellDetailCurrent.getGroupCode() != null
												&& !cellDetailCurrent.getGroupCode().isEmpty()) {
											bmLbl += " - " + cellDetailCurrent.getGroupCode();
											str += "</br><b>Mã nhóm</b> <span class='cell-bm-group-code'>"
													+ cellDetailCurrent.getGroupCode() + "</span>";
										}
										label.setValue(bmLbl);
										label.setDescription("<b>"+messageSourceAccessor.getMessage(Messages.UI_LABEL_SUBFORM)+"</b>" + str);
										label.setContentMode(ContentMode.HTML);
									} else if (obj instanceof List) {

										List<TruongThongTin> truongTTReadOnly = null;
										List<TruongThongTin> truongTTFilter = null;
										Map<TruongThongTin, String> truongThongTinConstrants = null;
										try {
											CellDetail cellDetailCurrent = hangCotArr[hang][cot];
											if (cellDetailCurrent != null) {
												truongTTReadOnly = cellDetailCurrent.getReadOnly();
												truongThongTinConstrants = cellDetailCurrent.getConstrantCode();
												truongTTFilter = cellDetailCurrent.getFilter();
											}
										} catch (Exception e) {
											e.printStackTrace();
										}

										List<TruongThongTin> truongTTLst = (List<TruongThongTin>) obj;

										String str = "";
										String strDesc = "<b>"+messageSourceAccessor.getMessage(Messages.UI_LABEL_ASSETFIELD)+"</b>";
										for (TruongThongTin truongTT : truongTTLst) {
											if (truongTT != null) {
												str += "<span class='cell-ttt-code'>" + truongTT.getMa();
												strDesc += "</br>" + truongTT.getTen() + " (" + truongTT.getMa() + ")";
												if (truongTTReadOnly != null && truongTTReadOnly.contains(truongTT)) {
													strDesc += "<i> - ReadOnly</i>";
													str += FontAwesome.LOCK.getHtml();
												}
												if (truongThongTinConstrants != null
														&& truongThongTinConstrants.get(truongTT) != null
														&& !truongThongTinConstrants.get(truongTT).isEmpty()) {
													strDesc += "</br>" + messageSourceAccessor.getMessage(Messages.UI_LABEL_REFERENCEBY)+" <i>"
															+ truongThongTinConstrants.get(truongTT) + "</i>";
													str += FontAwesome.EXCHANGE.getHtml();
												}

												if (truongTTFilter != null && truongTTFilter.contains(truongTT)) {
													strDesc += "</br>" + messageSourceAccessor.getMessage(Messages.UI_VIEW_TABLE_HEADER_REFERENCEFIELD);
													str += FontAwesome.USERS.getHtml();
												}

												str += "</span>";
											}
										}
										label.setContentMode(ContentMode.HTML);
										label.setValue(str);
										label.setDescription(strDesc);
									}
									cellDetailWrap.addComponent(label, "left: 0; top: 0");
								}

							}
						} else if (cot == soCot) {
							addLastCell(bieuMauGrid, cot, hang);
						}
					}
					viTriO++;
				}

			}
		}
	}

	public void updateSolrBieuMau(BieuMau current) {
//		try {
//			SolrUtils.updateSolr(CommonUtils.ESolrSearchType.BIEU_MAU.getCode() + "_" + current.getId(),
//					current.getTen() + " " + current.getMa(), solrUrl);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}

	public void initFormulaWrap() {
		formulaMap.clear();
		formulaContainer.removeAllComponents();

		CssLayout formulaWrap = new CssLayout();
		formulaWrap.setWidth("100%");
		formulaWrap.addStyleName(ExplorerLayout.FORMULA_WRAP);

		Button btnAddFormula = new Button();
		btnAddFormula.addClickListener(this);
		btnAddFormula.setId(BTN_ADD_FORMULA);
		btnAddFormula.setDescription(messageSourceAccessor.getMessage(Messages.UI_BUTTON_ADDFORMUlA));
		btnAddFormula.setIcon(FontAwesome.PLUS);
		formulaWrap.addComponent(btnAddFormula);

		formulaContainer.addComponent(formulaWrap);

	}

	public CssLayout initFormulaItem(String key) {
		CssLayout item = new CssLayout();
		item.addStyleName(ExplorerLayout.FORMULA_ITEM);

		item.setWidth("100%");

		CssLayout fieldItemWrap = new CssLayout();
		fieldItemWrap.addStyleName(ExplorerLayout.FORMULA_ITEM_FIELD_WRAP);
		TextField txtResultField = new TextField(messageSourceAccessor.getMessage(Messages.UI_LABEL_FIELD));
		txtResultField.setWidth("100%");
		txtResultField.setNullRepresentation("");
		txtResultField.setNullSettingAllowed(true);
		fieldItemWrap.addComponent(txtResultField);
		item.addComponent(fieldItemWrap);

		fieldItemWrap = new CssLayout();
		fieldItemWrap.addStyleName(ExplorerLayout.FORMULA_ITEM_FIELD_WRAP);
		TextField txtFormula = new TextField(messageSourceAccessor.getMessage(Messages.UI_LABEL_FORMULA));
		txtFormula.setWidth("100%");
		txtFormula.setNullRepresentation("");
		txtFormula.setNullSettingAllowed(true);
		fieldItemWrap.addComponent(txtFormula);
		item.addComponent(fieldItemWrap);

		fieldItemWrap = new CssLayout();
		fieldItemWrap.addStyleName(ExplorerLayout.FORMULA_ITEM_FIELD_WRAP);
		TextField txtFormulaPath = new TextField("Path");
		txtFormulaPath.setWidth("100%");
		txtFormulaPath.setNullRepresentation("");
		txtFormulaPath.setNullSettingAllowed(true);
		fieldItemWrap.addComponent(txtFormulaPath);
		item.addComponent(fieldItemWrap);
		Map<String, String> mapTmp = formulaMap.get(key);
		String value = null;
		if (mapTmp != null) {
			value = mapTmp.get("resultField");
		}
		formulaItem.addItemProperty(key + "_resultField", new ObjectProperty<String>(value, String.class));
		formulaFeildGroup.bind(txtResultField, key + "_resultField");

		String formula = null;
		if (mapTmp != null) {
			formula = mapTmp.get("formula");
		}

		formulaItem.addItemProperty(key + "_formula", new ObjectProperty<String>(formula, String.class));
		formulaFeildGroup.bind(txtFormula, key + "_formula");

		String formulaPath = null;
		if (mapTmp != null) {
			formulaPath = mapTmp.get("formulaPath");
		}
		formulaItem.addItemProperty(key + "_formulaPath", new ObjectProperty<String>(formulaPath, String.class));
		formulaFeildGroup.bind(txtFormulaPath, key + "_formulaPath");

		CssLayout removeFormularItem = new CssLayout();
		removeFormularItem.addStyleName(ExplorerLayout.FORMULA_REMOVE);
		item.addComponent(removeFormularItem);
		Button btnRemoveFormula = new Button();
		btnRemoveFormula.addStyleName(ExplorerLayout.FORMULA_REMOVE_BTN);
		removeFormularItem.addComponent(btnRemoveFormula);
		btnRemoveFormula.setId(BUTTON_REMOVE_FORMULA_ID);
		btnRemoveFormula.setIcon(FontAwesome.REMOVE);

		item.setData(key);
		btnRemoveFormula.setData(item);
		btnRemoveFormula.addClickListener(this);

		return item;
	}

	public void removeFormulaItem(CssLayout item) {
		String key = (String) item.getData();
		formulaContainer.removeComponent(item);

		try {
			TextField txtResult = (TextField) formulaFeildGroup.getField(key + "_resultField");
			formulaFeildGroup.unbind(txtResult);
			TextField txtFormula = (TextField) formulaFeildGroup.getField(key + "_formula");
			formulaFeildGroup.unbind(txtFormula);
			TextField txtPath = (TextField) formulaFeildGroup.getField(key + "_formulaPath");
			formulaFeildGroup.unbind(txtPath);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (formulaMap != null) {
			formulaMap.keySet().remove(key);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if (BUTTON_CANCEL_ID.equals(button.getId())) {
				UI.getCurrent().getNavigator().navigateTo(BieuMauView.VIEW_NAME);
			} else if (BUTTON_OPEN_WINDOW.equals(button.getId())) {
				UI.getCurrent().addWindow(window);
			} else if (BUTTON_UNDO.equals(button.getId())) {
				if (hangCotArrHistory != null) {
					hangCotArr = hangCotArrHistory;
					soHang = soHangHistory;
					soCot = soCotHistory;
					selectedLapHang = selectedLapHangHistory;
					cellContentMap = cellContentMapHistory;
					refreshLuoi();
				}
			} else if (BUTTON_INIT_GRID.equals(button.getId())) {
				if (initGridGroup.isValid()) {
					initGridGroup.commit();
					soHang = (Integer) initGridGroup.getItemDataSource().getItemProperty("soHang").getValue();
					soCot = (Integer) initGridGroup.getItemDataSource().getItemProperty("soCot").getValue();
					window.close();
					taoLuoi();
				}
			} else if (BUTTON_GHEP_PHAI_ID.equals(button.getId())) {
				ghepSangPhai(button);
			} else if (BUTTON_TACH_CELL_ID.equals(button.getId())) {
				TachSangTrai(button);
			} else if (BUTTON_TACH_ROW_ID.equals(button.getId())) {
				TachHang(button);
			} else if (BUTTON_GHEP_XUONG_ID.equals(button.getId())) {
				ghepXuongDuoi(button);
			} else if (BUTTON_EDIT_CELL_ID.equals(button.getId())) {
				cellCurrent = (CellDetail) button.getData();
				ComboBox cbb = (ComboBox) editCellGroup.getField("loaiO");
				textArea.setValue("");
				txtMa.setValue("");
				if (cellContentMap != null
						&& cellContentMap.get(cellCurrent.getRowIndex() + "_" + cellCurrent.getColIndex()) != null) {
					Object obj = cellContentMap.get(cellCurrent.getRowIndex() + "_" + cellCurrent.getColIndex());
					if (obj instanceof String) {
						cbb.setValue(new Long(CommonUtils.ELoaiO.TEXT.getCode()));
						String str = (String) obj;
						textArea.setValue(str);
					} else if (obj instanceof BieuMau) {
						cbb.setValue(new Long(CommonUtils.ELoaiO.BIEU_MAU.getCode()));
					} else if (obj instanceof List) {
						cbb.setValue(new Long(CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()));
						// txtMa

						List<TruongThongTin> truongTTLst = (List<TruongThongTin>) obj;
						if (truongTTLst.size() > 0) {
							TruongThongTin truongTT = truongTTLst.get(0);
							if(truongTT!=null)
								txtMa.setValue(truongTT.getMa());
						}
						List<Long> trangThai = null;
						WorkflowManagement<TruongThongTin> truongThongTinWM = new WorkflowManagement<TruongThongTin>(
								EChucNang.E_FORM_TRUONG_THONG_TIN.getMa());
						if (truongThongTinWM.getTrangThaiSuDung() != null) {
							trangThai = new ArrayList();
							trangThai.add(new Long(truongThongTinWM.getTrangThaiSuDung().getId()));
						}
						paginationBar.search(txtMa.getValue(), null, trangThai, null, null);
					}

//					}
				} else {
					cbb.setValue(new Long(CommonUtils.ELoaiO.TEXT.getCode()));
				}
				
				tttReadOnlyList.clear();
				tttSelectionList.clear();
				constrantsMap.clear();
				tttFilterList.clear();
				maNhomCurrent = null;
				valueDisplayButton = 0L;
//				if(textArea != null && cellCurrent != null && cellCurrent.){
//					textArea.setValue(newFieldValue);
//				} 

				UI.getCurrent().addWindow(editCellWindow);
			} else if (BUTTON_ADD_ROW_ID.equals(button.getId())) {
				themHang(button);
			} else if (BUTTON_ADD_COLUMN_ID.equals(button.getId())) {
				themCot(button);
			} else if (BUTTON_REMOVE_ROW_ID.equals(button.getId())) {
				xoaHang(button);
			} else if (BTN_ADD_TRUONG_THONG_TIN.equals(button.getId())) {
				editCellWindow.close();
				UI.getCurrent().addWindow(addTruongThongTinWindow);
			} else if (BUTTON_ADD_TTT_POPUP_ID.equals(button.getId())) {
				TruongThongTin truongThongTin = truongThongTinDetailView.addNew();
				cellCurrent.setCellType(new Long(CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()));
				List<TruongThongTin> truongTTList = new ArrayList();
				truongTTList.add(truongThongTin);
				cellContentMap.put(cellCurrent.getRowIndex() + "_" + cellCurrent.getColIndex(), truongTTList);
				addTruongThongTinWindow.close();
				refreshLuoi();

				// refresh data truong thong tin
				List<Long> trangThaiTruongTT = null;
				WorkflowManagement<TruongThongTin> truongTTWM = new WorkflowManagement<TruongThongTin>(
						EChucNang.E_FORM_TRUONG_THONG_TIN.getMa());
				if (truongTTWM.getTrangThaiSuDung() != null) {
					trangThaiTruongTT = new ArrayList<Long>();
					trangThaiTruongTT.add(new Long(truongTTWM.getTrangThaiSuDung().getId()));
				}
			} else if (BUTTON_UPDATE_CELL_ID.equals(button.getId())) {
				if (cellCurrent != null && cellContentMap != null && editCellGroup.isValid()) {
					editCellGroup.commit();
					Object cellValue = null;
					Long loaiO = (Long) editCellGroup.getField("loaiO").getValue();
					Object oldValue = cellContentMap.get(cellCurrent.getRowIndex() + "_" + cellCurrent.getColIndex());
					if (oldValue instanceof List) {
						List<TruongThongTin> tttList = (List<TruongThongTin>) oldValue;
						truongThongTinAddedList.removeAll(tttList);
					}
					if (loaiO != null && CommonUtils.ELoaiO.BIEU_MAU.getCode() == loaiO.intValue()) {
						List<BieuMau> bmList = (List<BieuMau>) button.getData();
						if (bmList != null && !bmList.isEmpty()) {
							cellValue = bmList.get(0);
							cellCurrent.setGroupCode(maNhomCurrent);
							cellCurrent.setCellType(new Long(CommonUtils.ELoaiO.BIEU_MAU.getCode()));
							System.out.println("valueDisplayButton: "+valueDisplayButton);
							cellCurrent.setDisplayButton(valueDisplayButton);
						}
					} else if (loaiO != null && CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode() == loaiO.intValue()) {
						List<TruongThongTin> truongTTList = new ArrayList(tttSelectionList);
						cellValue = truongTTList;
						cellCurrent.setCellType(new Long(CommonUtils.ELoaiO.TRUONG_THONG_TIN.getCode()));
						cellCurrent.setReadOnly(new ArrayList(tttReadOnlyList));
						cellCurrent.setConstrantCode(new HashMap(constrantsMap));
						cellCurrent.setFilter(new ArrayList(tttFilterList));
						truongThongTinAddedList.addAll(truongTTList);
					} else if (loaiO != null && CommonUtils.ELoaiO.TEXT.getCode() == loaiO.intValue()) {
						String textVal = (String) editCellGroup.getField("textVal").getValue();
						cellValue = textVal;
						cellCurrent.setCellType(new Long(CommonUtils.ELoaiO.TEXT.getCode()));
					}
					cellContentMap.put(cellCurrent.getRowIndex() + "_" + cellCurrent.getColIndex(), cellValue);
				}
				editCellGroup.clear();
				editCellWindow.close();
				refreshLuoi();
			} else if (BUTTON_REMOVE_FORMULA_ID.equals(button.getId())) {
				if (button.getData() != null) {
					CssLayout item = (CssLayout) button.getData();
					removeFormulaItem(item);
				}
			} else if (BTN_ADD_FORMULA.equals(button.getId())) {
				if (formulaMap != null) {
					int key = formulaMap.size();
					Map<String, String> formulaMapTmp = new HashMap();
					formulaMap.put(key + "", formulaMapTmp);
					CssLayout item = initFormulaItem(key + "");
					if (item != null) {
						formulaContainer.addComponent(item);
					}
				}
			} else {
				if (Constants.ACTION_EDIT.equals(action)) {
					Notification notf = new Notification(messageSourceAccessor.getMessage(Messages.UI_MESSAGES_SYSTEM), messageSourceAccessor.getMessage(Messages.UI_MESSAGES_UPDATESUCCESSFUL));
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid() && cellIdFieldGroup.isValid() && formulaFeildGroup.isValid()) {
						fieldGroup.commit();
						cellIdFieldGroup.commit();
						formulaFeildGroup.commit();
						BieuMau current = ((BeanItem<BieuMau>) fieldGroup.getItemDataSource()).getBean();
						notf.show(Page.getCurrent());

						List<Formula> fList = new ArrayList();
						if (formulaMap != null) {
							for (String key : formulaMap.keySet()) {
								Formula formula = new Formula();

								String f = (String) formulaItem.getItemProperty(key + "_formula").getValue();
								String path = (String) formulaItem.getItemProperty(key + "_formulaPath").getValue();
								String resultField = (String) formulaItem.getItemProperty(key + "_resultField")
										.getValue();

								formula.setBieuMau(bieuMauCurrent);
								formula.setFormula(f);
								formula.setThuTu(new Long(key));
								formula.setFormulaPath(path);
								formula.setResultField(resultField);

								fList.add(formula);
							}
						}

						current.setSoCot(new Long(soCot));
						current.setSoHang(new Long(soHang));
						List<BmO> bmOList = new ArrayList<>();
						List<BmHang> bmHangList = new ArrayList<>();
						List<BmOTruongThongTin> bmOTruongTTList = new ArrayList<>();
						getBieuMauChild(bmOList, bmHangList, bmOTruongTTList, current);
						bieuMauService.updateBieuMau(current, bmOList, bmHangList, bmOTruongTTList, fList);
						UI.getCurrent().getNavigator().navigateTo(BieuMauView.VIEW_NAME);
						updateSolrBieuMau(current);
					}
				} else if (Constants.ACTION_ADD.equals(action)) {
					Notification notf = new Notification("Thông báo", "Thêm mới thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid() && cellIdFieldGroup.isValid() && formulaFeildGroup.isValid()) {
						formulaFeildGroup.commit();
						fieldGroup.commit();
						cellIdFieldGroup.commit();
						BieuMau current = ((BeanItem<BieuMau>) fieldGroup.getItemDataSource()).getBean();
						notf.show(Page.getCurrent());

						// Add new data
						current.setSoCot(new Long(soCot));
						current.setSoHang(new Long(soHang));
						// TODO set jasper
						List<BmO> bmOList = new ArrayList<>();
						List<BmHang> bmHangList = new ArrayList<>();
						List<BmOTruongThongTin> bmOTruongTTList = new ArrayList<>();
						getBieuMauChild(bmOList, bmHangList, bmOTruongTTList, current);

						List<Formula> fList = new ArrayList();
						if (formulaMap != null) {
							for (String key : formulaMap.keySet()) {
								Formula formula = new Formula();

								String f = (String) formulaItem.getItemProperty(key + "_formula").getValue();
								String path = (String) formulaItem.getItemProperty(key + "_formulaPath").getValue();
								String resultField = (String) formulaItem.getItemProperty(key + "_resultField")
										.getValue();

								formula.setBieuMau(bieuMauCurrent);
								formula.setFormula(f);
								formula.setFormulaPath(path);
								formula.setThuTu(new Long(key));
								formula.setResultField(resultField);

								fList.add(formula);
							}
						}

						bieuMauService.updateBieuMau(current, bmOList, bmHangList, bmOTruongTTList, fList);
						// End add new data

						UI.getCurrent().getNavigator().navigateTo(BieuMauView.VIEW_NAME);
						updateSolrBieuMau(current);
					}
				} else if (Constants.ACTION_DEL.equals(action)) {
					Notification notf = new Notification("Thông báo", "Xóa thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid()) {
						fieldGroup.commit();
						notf.show(Page.getCurrent());
						BieuMau bm = ((BeanItem<BieuMau>) fieldGroup.getItemDataSource()).getBean();
						String solrId = CommonUtils.ESolrSearchType.BIEU_MAU.getCode() + "_" + bm.getId();
						bieuMauService.deleteBieuMau(bm);
						UI.getCurrent().getNavigator().navigateTo(BieuMauView.VIEW_NAME);
						try {
							SolrUtils.deleteFromSolr(solrId, solrUrl);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} else if (Constants.ACTION_APPROVE.equals(action)) {
					Notification notf = new Notification("Thông báo", bieuMauWf.getPheDuyetTen() + " thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid() && cellIdFieldGroup.isValid() && formulaFeildGroup.isValid()) {
						fieldGroup.commit();
						cellIdFieldGroup.commit();
						formulaFeildGroup.commit();
						BieuMau current = bieuMauWf.pheDuyet();
						notf.show(Page.getCurrent());
						current.setSoCot(new Long(soCot));
						current.setSoHang(new Long(soHang));
						List<BmO> bmOList = new ArrayList<>();
						List<BmHang> bmHangList = new ArrayList<>();
						List<BmOTruongThongTin> bmOTruongTTList = new ArrayList<>();
						getBieuMauChild(bmOList, bmHangList, bmOTruongTTList, current);

						List<Formula> fList = new ArrayList();
						if (formulaMap != null) {
							for (String key : formulaMap.keySet()) {
								Formula formula = new Formula();

								String f = (String) formulaItem.getItemProperty(key + "_formula").getValue();
								String path = (String) formulaItem.getItemProperty(key + "_formulaPath").getValue();
								String resultField = (String) formulaItem.getItemProperty(key + "_resultField")
										.getValue();

								formula.setBieuMau(bieuMauCurrent);
								formula.setFormula(f);
								formula.setFormulaPath(path);
								formula.setThuTu(new Long(key));
								formula.setResultField(resultField);

								fList.add(formula);
							}
						}

						bieuMauService.updateBieuMau(current, bmOList, bmHangList, bmOTruongTTList, fList);
						UI.getCurrent().getNavigator().navigateTo(BieuMauView.VIEW_NAME);
						updateSolrBieuMau(current);
					}
				} else if (Constants.ACTION_REFUSE.equals(action)) {
					Notification notf = new Notification("Thông báo", bieuMauWf.getTuChoiTen() + " thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid() && cellIdFieldGroup.isValid() && formulaFeildGroup.isValid()) {
						fieldGroup.commit();
						cellIdFieldGroup.commit();
						formulaFeildGroup.commit();
						BieuMau current = bieuMauWf.tuChoi();
						notf.show(Page.getCurrent());

						current.setSoCot(new Long(soCot));
						current.setSoHang(new Long(soHang));
						List<BmO> bmOList = new ArrayList<>();
						List<BmHang> bmHangList = new ArrayList<>();
						List<BmOTruongThongTin> bmOTruongTTList = new ArrayList<>();
						getBieuMauChild(bmOList, bmHangList, bmOTruongTTList, current);

						List<Formula> fList = new ArrayList();
						if (formulaMap != null) {
							for (String key : formulaMap.keySet()) {
								Formula formula = new Formula();

								String f = (String) formulaItem.getItemProperty(key + "_formula").getValue();
								String path = (String) formulaItem.getItemProperty(key + "_formulaPath").getValue();
								String resultField = (String) formulaItem.getItemProperty(key + "_resultField")
										.getValue();

								formula.setBieuMau(bieuMauCurrent);
								formula.setThuTu(new Long(key));
								formula.setFormula(f);
								formula.setFormulaPath(path);
								formula.setResultField(resultField);

								fList.add(formula);
							}
						}

						bieuMauService.updateBieuMau(current, bmOList, bmHangList, bmOTruongTTList, fList);

						UI.getCurrent().getNavigator().navigateTo(BieuMauView.VIEW_NAME);

					}
				} else if (Constants.ACTION_COPY.equals(action)) {
					Notification notf = new Notification("Thông báo", "Thêm mới thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid() && cellIdFieldGroup.isValid()) {
						fieldGroup.commit();
						cellIdFieldGroup.commit();
						BieuMau currentTmp = ((BeanItem<BieuMau>) fieldGroup.getItemDataSource()).getBean();
						BieuMau current = (BieuMau) currentTmp.clone();
						current.setId(null);
						notf.show(Page.getCurrent());

						// Add new data
						current.setSoCot(new Long(soCot));
						current.setSoHang(new Long(soHang));
						// TODO set jasper
						List<BmO> bmOList = new ArrayList<>();
						List<BmHang> bmHangList = new ArrayList<>();
						List<BmOTruongThongTin> bmOTruongTTList = new ArrayList<>();
						getBieuMauChild(bmOList, bmHangList, bmOTruongTTList, current);

						List<Formula> fList = new ArrayList();
						if (formulaMap != null) {
							for (String key : formulaMap.keySet()) {
								Formula formula = new Formula();

								String f = (String) formulaItem.getItemProperty(key + "_formula").getValue();
								String path = (String) formulaItem.getItemProperty(key + "_formulaPath").getValue();
								String resultField = (String) formulaItem.getItemProperty(key + "_resultField")
										.getValue();

								formula.setBieuMau(current);
								formula.setFormula(f);
								formula.setFormulaPath(path);
								formula.setResultField(resultField);

								fList.add(formula);
							}
						}

						bieuMauService.updateBieuMau(current, bmOList, bmHangList, bmOTruongTTList, fList);
						// End add new data
						UI.getCurrent().getNavigator().navigateTo(BieuMauView.VIEW_NAME);
						updateSolrBieuMau(current);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra", Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
		}

	}

}
package com.eform.ui.view.process;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.utils.CommonUtils.EAction;
import com.eform.common.workflow.Workflow;
import com.eform.model.entities.BieuMau;
import com.eform.ui.custom.ButtonAction;
import com.eform.ui.custom.paging.ProcessDefinitionListPaginationBar;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.themes.Reindeer;

public class ProcessDefinitionContainer extends CssLayout{
	private CssLayout processDefinitionContainer;
	private ProcessDefinitionListPaginationBar paginationBar;
	private BeanItemContainer<ProcessDefinition> container;
	
	public ProcessDefinitionContainer(RepositoryService repositoryService, ClickListener clickListener, Set<String> depIds, Boolean isActive, Map<Long, Boolean> mapQuyen){
		addStyleName(ExplorerLayout.TASK_LIST_WRAP);
		setWidth("100%");
		processDefinitionContainer = new CssLayout();
		processDefinitionContainer.addStyleName(ExplorerLayout.DATA_WRAP);
		processDefinitionContainer.addStyleName("process-container");
		processDefinitionContainer.addStyleName("process-grid");
		processDefinitionContainer.setWidth("100%");
		
		CssLayout tableWrap = new CssLayout();
		tableWrap.setVisible(false);
		CssLayout viewModeContainer = new CssLayout();
		viewModeContainer.addStyleName("process-view-mode");
		viewModeContainer.setWidth("100%");
		Button btnViewMode = new Button(FontAwesome.TH.getHtml());
		btnViewMode.setHtmlContentAllowed(true);
		btnViewMode.addStyleName(Reindeer.BUTTON_LINK);
		btnViewMode.addStyleName("btn-view-mode");
		btnViewMode.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				tableWrap.setVisible(!tableWrap.isVisible());
				processDefinitionContainer.setVisible(!processDefinitionContainer.isVisible());
			}
		});
		viewModeContainer.addComponent(btnViewMode);
		addComponent(viewModeContainer);
		
		addComponent(processDefinitionContainer);
		
		
		
		tableWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		tableWrap.addStyleName("process-container");
		tableWrap.addStyleName("process-th-list");
		tableWrap.setWidth("100%");
		Table table = new Table();
		container = new BeanItemContainer<ProcessDefinition>(ProcessDefinition.class, new ArrayList<ProcessDefinition>());
		table.setContainerDataSource(container);
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setSizeFull();
		table.setWidth(100, Unit.PERCENTAGE);
		table.setColumnHeader("name", "Tên quy trình");
		table.setColumnHeader("description", "Mô tả");

		table.addGeneratedColumn("deployDate", new ColumnGenerator() {
			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				ProcessDefinition proDef = (ProcessDefinition) itemId;
				if(proDef.getDeploymentId() != null){
					Deployment dep = repositoryService.createDeploymentQuery().deploymentId(proDef.getDeploymentId()).singleResult();
					if(dep != null && dep.getDeploymentTime() != null){
						SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
						return df.format(dep.getDeploymentTime());
					}
				}
				
				return null;
			} 
		});

		table.setColumnHeader("deployDate", "Ngày ban hành");
		
		table.addGeneratedColumn("action", new ColumnGenerator() { 
			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				ProcessDefinition process = (ProcessDefinition) itemId;
				
				HorizontalLayout actionWrap = new HorizontalLayout();
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
				if(process != null && !process.isSuspended()){
					if(mapQuyen != null && mapQuyen.get(new Long(EAction.START.getCode())) != null && mapQuyen.get(new Long(EAction.START.getCode()))){
				        Button btnStart = new Button();
				        btnStart.setId(ProcessDefinitionListView.BTN_START_ID);
				        btnStart.setDescription("Khởi tạo quy trình");
				        btnStart.setIcon(FontAwesome.PLAY);
				        btnStart.addStyleName(ExplorerLayout.BUTTON_ACTION);
				        btnStart.addClickListener(clickListener);
				        btnStart.setData(itemId);
				        btnStart.addStyleName(Reindeer.BUTTON_LINK);
				        actionWrap.addComponent(btnStart);
					}
					if(mapQuyen != null && mapQuyen.get(new Long(EAction.PHE_DUYET.getCode())) != null && mapQuyen.get(new Long(EAction.PHE_DUYET.getCode()))){
						Button btnSuspend = new Button();
						btnSuspend.setId(ProcessDefinitionListView.BTN_REFUSE_ID);
						btnSuspend.setDescription("Ngừng sử dụng");
						btnSuspend.setIcon(FontAwesome.LEVEL_DOWN);
						btnSuspend.addStyleName(ExplorerLayout.BUTTON_ACTION);
						btnSuspend.addClickListener(clickListener);
						btnSuspend.setData(itemId);
						btnSuspend.addStyleName(Reindeer.BUTTON_LINK);
				        actionWrap.addComponent(btnSuspend);
				        

				        Button btnEdit = new Button();
						btnEdit.setId(ProcessDefinitionListView.BTN_EDIT_ID);
						btnEdit.setDescription("Sửa thông tin");
						btnEdit.setIcon(FontAwesome.PENCIL);
						btnEdit.addStyleName(ExplorerLayout.BUTTON_ACTION);
						btnEdit.addClickListener(clickListener);
						btnEdit.setData(itemId);
						btnEdit.addStyleName(Reindeer.BUTTON_LINK);
				        actionWrap.addComponent(btnEdit);
		        	}
				}else{
					if(mapQuyen != null && mapQuyen.get(new Long(EAction.PHE_DUYET.getCode())) != null && mapQuyen.get(new Long(EAction.PHE_DUYET.getCode()))){
						Button btnActive = new Button();
						btnActive.setDescription("Sử dụng");
				        btnActive.setId(ProcessDefinitionListView.BTN_APPROVE_ID);
				        btnActive.setIcon(FontAwesome.CHECK);
				        btnActive.addStyleName(ExplorerLayout.BUTTON_ACTION);
				        btnActive.addClickListener(clickListener);
				        btnActive.setData(process);
				        btnActive.addStyleName(Reindeer.BUTTON_LINK);
				        actionWrap.addComponent(btnActive);
					}
					if(mapQuyen != null && mapQuyen.get(new Long(EAction.XOA.getCode())) != null && mapQuyen.get(new Long(EAction.XOA.getCode()))){
				        Button btnDelete = new Button();
				        btnDelete.setDescription("Xóa");
				        btnDelete.setId(ProcessDefinitionListView.BTN_DELETE_ID);
				        btnDelete.setIcon(FontAwesome.REMOVE);
				        btnDelete.addStyleName(ExplorerLayout.BUTTON_ACTION);
				        btnDelete.addClickListener(clickListener);
				        btnDelete.setData(process);
				        btnDelete.addStyleName(Reindeer.BUTTON_LINK);
				        actionWrap.addComponent(btnDelete);
					}
				}
	

		        
		        
		        return actionWrap;
		    }
		});
		table.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				ProcessDefinition process = (ProcessDefinition) itemId;
				if(process.isSuspended()){
					return "status-susspended";
				}
				return "status-using";
			}
			
		});
		
		table.setColumnWidth("action", 150);
		table.setColumnHeader("action", "Thao tác");
		table.setRowHeaderMode(RowHeaderMode.INDEX);
		table.setVisibleColumns("name", "description", "deployDate", "action");
		tableWrap.addComponent(table);
		addComponent(tableWrap);
		
		paginationBar = new ProcessDefinitionListPaginationBar(processDefinitionContainer, container, repositoryService, clickListener, depIds, isActive, mapQuyen);
		if(paginationBar.getRowCount() > 0){
			CssLayout pagingWrap = new CssLayout();
			pagingWrap.setWidth("100%");
			pagingWrap.addStyleName(ExplorerLayout.PAGING_WRAP);
			pagingWrap.addComponent(paginationBar);
			addComponent(pagingWrap);
		}
	}

	public	ProcessDefinitionListPaginationBar getPaginationBar() {
		return paginationBar;
	}

	public void setPaginationBar(ProcessDefinitionListPaginationBar paginationBar) {
		this.paginationBar = paginationBar;
	}
	
}

package com.eform.ui.view.form.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FormProperty;
import org.activiti.bpmn.model.SubProcess;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.ExportUtils;
import com.eform.common.utils.FormUtils;
import com.eform.common.utils.CommonUtils.ELoaiBieuMau;
import com.eform.common.workflow.ITrangThai;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.BieuMau;
import com.eform.service.BieuMauService;
import com.eform.ui.custom.ButtonAction;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.StatusCommentLayout;
import com.eform.ui.custom.paging.BieuMauPaginationBar;
import com.eform.ui.view.dashboard.DashboardView;
import com.eform.ui.view.process.ProcessDefinitionDetailView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = BieuMauView.VIEW_NAME)
public class BieuMauView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "bieu-mau";
	public static final String MAIN_TITLE = "Biểu mẫu";
	private static final String SMALL_TITLE = "Danh sách biểu mẫu và tìm kiếm";

	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	private static final String BTN_VIEW_ID = "xem";
	private static final String BTN_PREVIEW_ID = "xemTruoc";
	private static final String BTN_ADDNEW_ID = "themMoi";
	private static final String BTN_COPPY_ID = "copy";
	private static final String BTN_EXPORT_ID = "export";
	private static final String BTN_PROCESS_ID = "btnProcess";
	private final BieuMauView currentView = this;
	
	@Autowired
	BieuMauService bieuMauService;
	private BeanItemContainer<BieuMau> container;
	private Table table = new Table();
	private WorkflowManagement<BieuMau> bieuMauWM;
	
	private final PropertysetItem searchItem = new PropertysetItem();
	private final FieldGroup searchFieldGroup = new FieldGroup();
	private Button btnAddNew;
	
	private BieuMauPaginationBar paginationBar;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RepositoryService repositoryService;
	private Map<String, List<String>> proDefMaBmMap;
	
	List<ProcessDefinition> processDefinitionList;
	
	@PostConstruct
    void init() {
		processDefinitionList = repositoryService.createProcessDefinitionQuery().list();
		bieuMauWM = new WorkflowManagement<BieuMau>(EChucNang.E_FORM_BIEU_MAU.getMa());
		proDefMaBmMap = new HashMap();
		container = new BeanItemContainer<BieuMau>(BieuMau.class, new ArrayList<BieuMau>());
		paginationBar = new BieuMauPaginationBar(container, bieuMauService);
    	
    	searchItem.addItemProperty("ma", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("ten", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("loaiBieuMau", new ObjectProperty<ComboboxItem>(new ComboboxItem()));
    	searchItem.addItemProperty("trangThai", new ObjectProperty<ComboboxItem>(new ComboboxItem()));
    	searchItem.addItemProperty("quyTrinh", new ObjectProperty<ProcessDefinition>(null, ProcessDefinition.class));
    	searchFieldGroup.setItemDataSource(searchItem);
    	
		initMainTitle();
		initSearchForm();
		initDataContent();
		
		btnAddNew = new Button();
    	btnAddNew.setIcon(FontAwesome.PLUS);
    	btnAddNew.addClickListener(this);
    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
    	btnAddNew.setDescription("Tạo mới biểu mẫu");
    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
    	btnAddNew.setId(BTN_ADDNEW_ID);
		addComponent(btnAddNew);
		
//		Button btnImport = new Button();
//		btnImport.setIcon(FontAwesome.CLOUD_UPLOAD);
//		btnImport.addClickListener(this);
//		btnImport.addStyleName(Reindeer.BUTTON_LINK);
//		btnImport.setDescription("Import");
//		btnImport.addStyleName(ExplorerLayout.BUTTON_IMPORT);
//		btnImport.setId("BTN_IMPORT_ID");
//		addComponent(btnImport);
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	
	public void initMainTitle(){
		PageBreadcrumbInfo home = new PageBreadcrumbInfo();
    	home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
    	home.setViewName(DashboardView.VIEW_NAME);
    	
    	PageBreadcrumbInfo current = new PageBreadcrumbInfo();
    	current.setTitle(MAIN_TITLE);
    	current.setViewName(VIEW_NAME);		
    			
    	addComponent(new PageBreadcrumbLayout(home, current));
    	
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		
		CssFormLayout searchForm = new CssFormLayout(-1, 60);
		
		TextField txtMa = new TextField("Mã biểu mẫu");
		searchForm.addFeild(txtMa);
		
		TextField txtTen = new TextField(messageSource.getMessage(Messages.UI_VIEW_BIEUMAU_TEN, null, VaadinSession.getCurrent().getLocale()));
		searchForm.addFeild(txtTen);
		
		List<ComboboxItem> loaiBieuMauList = new ArrayList<>();
		for (CommonUtils.ELoaiBieuMau loaiBm : CommonUtils.ELoaiBieuMau.values()) {
			ComboboxItem item = new ComboboxItem(loaiBm.getName(), new Long(loaiBm.getCode()));
			loaiBieuMauList.add(item);
		}
		BeanItemContainer<ComboboxItem> cbbLoaiBmContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, loaiBieuMauList);
		ComboBox cbbLoaiBm = new ComboBox("Loại biểu mẫu", cbbLoaiBmContainer);
		cbbLoaiBm.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbLoaiBm.setItemCaptionPropertyId("text");
    	searchForm.addFeild(cbbLoaiBm);
    	
    	List<ITrangThai> trangThaiListTmp = bieuMauWM.getTrangThaiList();
    	List<ComboboxItem> trangThaiList = new ArrayList<>();
    	if(trangThaiListTmp != null){
    		for (ITrangThai trangThai : trangThaiListTmp) {
    			trangThaiList.add(new ComboboxItem(trangThai.getTen(), new Long(trangThai.getId())));
			}
    	}
    	
    	BeanItemContainer<ComboboxItem> cbbTrangThaiContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, trangThaiList);
		ComboBox cbbTrangThai = new ComboBox("Trạng thái", cbbTrangThaiContainer);
		cbbTrangThai.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbTrangThai.setItemCaptionPropertyId("text");
    	searchForm.addFeild(cbbTrangThai);
    	
    	
    	BeanItemContainer<ProcessDefinition> proDefContainer = new BeanItemContainer<ProcessDefinition>(ProcessDefinition.class, processDefinitionList);
		ComboBox cbbProDef = new ComboBox("Quy trình", proDefContainer);
		cbbProDef.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbProDef.setItemCaptionPropertyId("name");
    	searchForm.addFeild(cbbProDef);

		searchFieldGroup.bind(txtMa, "ma");
		searchFieldGroup.bind(txtTen, "ten");
		searchFieldGroup.bind(cbbLoaiBm, "loaiBieuMau");
		searchFieldGroup.bind(cbbTrangThai, "trangThai");
		searchFieldGroup.bind(cbbProDef, "quyTrinh");
		
		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(currentView);
		searchForm.addButton(btnSearch);
		paginationBar.fixEnterHotKey(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(currentView);
		searchForm.addButton(btnRefresh);
		
		addComponent(searchForm);
	}
	
	public void initDataContent(){
		List<ProcessDefinition> proDefList = repositoryService.createProcessDefinitionQuery().active().list();
		Map<String, List<ProcessDefinition>> maBieuMauProDefMap = new HashMap<String, List<ProcessDefinition>>();
		
		if(proDefList != null && !proDefList.isEmpty()){
			for(ProcessDefinition proDef : proDefList){
				BpmnModel bpmnModel = repositoryService.getBpmnModel(proDef.getId());
				List<String> maBmList = getBieuMau(bpmnModel);
				proDefMaBmMap.put(proDef.getId(), maBmList);
				for(String maBm : maBmList){
					List<ProcessDefinition> proDefListTmp  = maBieuMauProDefMap.get(maBm);
					if(proDefListTmp == null){
						proDefListTmp = new ArrayList<ProcessDefinition>();
						maBieuMauProDefMap.put(maBm, proDefListTmp);
					}
					proDefListTmp.add(proDef);
				}
			}
		}
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
		List<ITrangThai> trangThaiList = bieuMauWM.getTrangThaiList();
		if(trangThaiList != null && !trangThaiList.isEmpty()){
			dataWrap.addComponent(new StatusCommentLayout(trangThaiList));
		}
		
		table.setContainerDataSource(container);
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setSizeFull();
		table.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		table.setColumnHeader("ma", "Mã");
		table.setColumnHeader("ten", "Tên");
//		table.setColumnHeader("loaiBieuMau", "Biểu mẫu con");
		table.setColumnHeader("quyTrinh", "Quy trình");
		table.setColumnHeader("trangThai", "Trạng thái");
		
//		table.addGeneratedColumn("loaiBieuMau", new ColumnGenerator(){
//			private static final long serialVersionUID = 1L;
//
//			@Override
//			public Object generateCell(Table source, Object itemId, Object columnId) {
//				BieuMau current = (BieuMau) itemId;
//				CommonUtils.ELoaiBieuMau loaiBm = CommonUtils.ELoaiBieuMau.getLoaiBieuMau(current.getLoaiBieuMau());
//				if(ELoaiBieuMau.BM_CON.equals(loaiBm)){
//					return FontAwesome.CHECK.getHtml();
//				}
//				return null;
//			}
//		});
		
		table.addGeneratedColumn("quyTrinh", new ColumnGenerator() {
			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				BieuMau current = (BieuMau) itemId;
				
				List<ProcessDefinition> proDefList = maBieuMauProDefMap.get(current.getMa());
				
				CssLayout proDefWrap = new CssLayout();
				proDefWrap.addStyleName(ExplorerLayout.BTN_PROCESS_DETAIL_WRAP);
				proDefWrap.setWidth("100%");
				
				if(proDefList != null){
					for(ProcessDefinition proDef : proDefList){
						String name = "#Không tên";
						if(proDef.getName() != null && !proDef.getName().isEmpty()){
							name = proDef.getName();
						}
						Button button = new Button(name);
						button.setId(BTN_PROCESS_ID);
						button.addClickListener(currentView);
						button.setData(proDef);
						button.addStyleName(ExplorerLayout.BTN_PROCESS_DETAIL);
						button.addStyleName(Reindeer.BUTTON_LINK);
						proDefWrap.addComponent(button);
					}
				}
				return proDefWrap;
			} 
			
		});
		
		table.addGeneratedColumn("action", new ColumnGenerator() { 
			private static final long serialVersionUID = 1L;

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				BieuMau current = (BieuMau) itemId;
				Workflow<BieuMau> wf = bieuMauWM.getWorkflow(current);
				
				HorizontalLayout actionWrap = new HorizontalLayout();
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
				
		        Button btnView = new Button();
		        btnView.setId(BTN_VIEW_ID);
		        btnView.setDescription("Xem cấu hình");
		        btnView.setIcon(FontAwesome.CUBE);
		        btnView.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnView.addClickListener(currentView);
		        btnView.setData(itemId);
		        btnView.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnView);

		        Button btnPreview = new Button();
		        btnPreview.setId(BTN_PREVIEW_ID);
		        btnPreview.setDescription("Xem trước");
		        btnPreview.setIcon(FontAwesome.EYE);
		        btnPreview.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnPreview.addClickListener(currentView);
		        btnPreview.setData(itemId);
		        btnPreview.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnPreview);

		        Button btnCopy = new Button();
        		btnCopy.setId(BTN_COPPY_ID);
        		btnCopy.setDescription("Sao chép");
        		btnCopy.setIcon(FontAwesome.COPY);
        		btnCopy.addStyleName(ExplorerLayout.BUTTON_ACTION);
        		btnCopy.addClickListener(currentView);
        		btnCopy.setData(itemId);
        		btnCopy.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnCopy);
		        
//		        Button btnExport = new Button();
//		        btnExport.setId(BTN_EXPORT_ID);
//		        btnExport.setDescription("Export");
//		        btnExport.setIcon(FontAwesome.COPY);
//		        btnExport.addStyleName(ExplorerLayout.BUTTON_ACTION);
//		        btnExport.addClickListener(currentView);
//		        btnExport.setData(itemId);
//        		btnExport.addStyleName(Reindeer.BUTTON_LINK);
//		        actionWrap.addComponent(btnExport);
		        
		        new ButtonAction<BieuMau>().getButtonAction(actionWrap, wf, currentView);
		        
		        return actionWrap;
		    }
		});
		table.setColumnWidth("action", 315);
		table.setColumnHeader("action", "Thao tác");
		table.setRowHeaderMode(RowHeaderMode.INDEX);

		table.setVisibleColumns("ma", "ten", "quyTrinh", "action");
		
		table.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if("loaiBieuMau".equals(propertyId)){
                    return "center-aligned";
                }
				BieuMau current = (BieuMau) itemId;
				Workflow<BieuMau> wf = bieuMauWM.getWorkflow(current);
                return wf.getStyleName();
			}
			
		});

		dataWrap.addComponent(table);
		dataWrap.addComponent(paginationBar);
	}

	
	public List<String> getBieuMau(BpmnModel bpmnModel){
		List<String> maBieuMauList = new ArrayList<String>();
		if(bpmnModel == null){
			return maBieuMauList;
		}
		List<org.activiti.bpmn.model.Process> listProcess = bpmnModel.getProcesses();
		for (org.activiti.bpmn.model.Process process : listProcess) {
			System.out.println(process);
			Collection<FlowElement> elements = process.getFlowElements();
			List<FlowElement> listElements = new ArrayList<FlowElement>(elements);
			while(!listElements.isEmpty()){
				FlowElement elementObj = listElements.remove(0);
				if (elementObj instanceof UserTask) {
					UserTask element = (UserTask) elementObj;
					String formKey = element.getFormKey();
					if (formKey != null) {
						Pattern p = Pattern.compile("([0-9a-zA-Z_-]+)#([0-9a-zA-Z_-]+)");
						Matcher m = p.matcher(formKey);
						if(m.matches()){
							formKey = m.group(1);
						}
						if(!maBieuMauList.contains(formKey)){
							maBieuMauList.add(formKey);
						}
					}
					
				}else if (elementObj instanceof org.activiti.bpmn.model.StartEvent) {
					org.activiti.bpmn.model.StartEvent element = (org.activiti.bpmn.model.StartEvent) elementObj;
					String formKey = element.getFormKey();
					if (formKey != null) {
						Pattern p = Pattern.compile("([0-9a-zA-Z_-]+)#([0-9a-zA-Z_-]+)");
						Matcher m = p.matcher(formKey);
						if(m.matches()){
							formKey = m.group(1);
						}
						if(!maBieuMauList.contains(formKey)){
							maBieuMauList.add(formKey);
						}
					}
				}else if(elementObj instanceof SubProcess){
					SubProcess element = (SubProcess) elementObj;
					Collection<FlowElement> subProcessElements = element.getFlowElements();
					List<FlowElement> subProcessListElement = new ArrayList<FlowElement>(subProcessElements);
					listElements.addAll(subProcessListElement);
				}
			}
		}
		return maBieuMauList;
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		if(BTN_SEARCH_ID.equals(event.getButton().getId())){
			try {
				if(searchFieldGroup.isValid()){
					searchFieldGroup.commit();
					String ma = (String) searchItem.getItemProperty("ma").getValue();
					ma = ma != null ? "%"+ma.trim()+"%" : null;
					String ten = (String) searchItem.getItemProperty("ten").getValue();
					ten = ten != null ? "%"+ten.trim()+"%" : null;
					ComboboxItem trangThai = (ComboboxItem) searchItem.getItemProperty("trangThai").getValue();
					List<Long> trangThaiList = null;
					if(trangThai != null && trangThai.getValue() != null){
						trangThaiList = new ArrayList<Long>();
						trangThaiList.add((Long) trangThai.getValue());
					}
					ComboboxItem loaiBm = (ComboboxItem) searchItem.getItemProperty("loaiBieuMau").getValue();
					Long loaiBmSearch = null;
					if(loaiBm != null && loaiBm.getValue() != null){
						loaiBmSearch = (Long) loaiBm.getValue();
					}
					ProcessDefinition processDefition = (ProcessDefinition) searchItem.getItemProperty("quyTrinh").getValue();
					List<String> maBmList = null;
					
					if(processDefition != null){
						maBmList = proDefMaBmMap.get(processDefition.getId());
						if(maBmList == null || maBmList.isEmpty()){
							paginationBar.clearData();
							return;
						}
					}
					paginationBar.search(ma, maBmList, ten, loaiBmSearch, trangThaiList);
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
	    	searchFieldGroup.clear();
	    	paginationBar.search(null, null, null, null, null);
		}else if(BTN_VIEW_ID.equals(event.getButton().getId())){
			BieuMau currentRow = (BieuMau) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(BieuMauDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getMa().toLowerCase());
		}else if(BTN_COPPY_ID.equals(event.getButton().getId())){
			BieuMau currentRow = (BieuMau) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(BieuMauDetailView.VIEW_NAME+"/"+Constants.ACTION_COPY+"/"+currentRow.getMa().toLowerCase());
		}else if(BTN_PREVIEW_ID.equals(event.getButton().getId())){
			BieuMau currentRow = (BieuMau) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(BieuMauPreview.VIEW_NAME+"/"+Constants.ACTION_PREVIEW+"/"+currentRow.getMa().toLowerCase());
		}else if(ButtonAction.BTN_EDIT_ID.equals(event.getButton().getId())){
			BieuMau currentRow = (BieuMau) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(BieuMauDetailView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getMa().toLowerCase());
		}else if(ButtonAction.BTN_DEL_ID.equals(event.getButton().getId())){
			BieuMau currentRow = (BieuMau) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(BieuMauDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getMa().toLowerCase());
		}else if(ButtonAction.BTN_APPROVE_ID.equals(event.getButton().getId())){
			BieuMau currentRow = (BieuMau) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(BieuMauDetailView.VIEW_NAME+"/"+Constants.ACTION_APPROVE+"/"+currentRow.getMa().toLowerCase());
		}else if(ButtonAction.BTN_REFUSE_ID.equals(event.getButton().getId())){
			BieuMau currentRow = (BieuMau) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(BieuMauDetailView.VIEW_NAME+"/"+Constants.ACTION_REFUSE+"/"+currentRow.getMa().toLowerCase());
		}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
	        UI.getCurrent().getNavigator().navigateTo(BieuMauDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD);
		}else if(BTN_PROCESS_ID.equals(event.getButton().getId())){
	        ProcessDefinition proDef = (ProcessDefinition)event.getButton().getData();
	        if(proDef != null){
	        	UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+proDef.getId());
	        }
		}else if(BTN_EXPORT_ID.equals(event.getButton().getId())){
			BieuMau currentRow = (BieuMau) event.getButton().getData();
			if(currentRow != null){
				List<BieuMau> bieuMauList = new ArrayList();
				bieuMauList.add(currentRow);
				//String xml = ExportUtils.exportBieuMauAll(bieuMauList, null);
				//System.out.println(xml);
			}
		}else if("BTN_IMPORT_ID".equals(event.getButton().getId())){
			ExportUtils.importTest();
		}
	}

}

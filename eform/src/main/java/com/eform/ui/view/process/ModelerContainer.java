package com.eform.ui.view.process;

import java.util.Map;

import org.activiti.engine.RepositoryService;

import com.vaadin.ui.Button.ClickListener;
import com.eform.common.ExplorerLayout;
import com.eform.service.HtThamSoService;
import com.eform.ui.custom.paging.ModelerListPaginationBar;
import com.vaadin.ui.CssLayout;

public class ModelerContainer extends CssLayout{
	private CssLayout modelerContainer;
	private ModelerListPaginationBar paginationBar;
	
	public ModelerContainer(RepositoryService repositoryService, ClickListener clickListener, Map<Long, Boolean> mapQuyen){
		addStyleName(ExplorerLayout.TASK_LIST_WRAP);
		setWidth("100%");
		modelerContainer = new CssLayout();
		modelerContainer.addStyleName(ExplorerLayout.DATA_WRAP);
		modelerContainer.setWidth("100%");
		addComponent(modelerContainer);
		
		paginationBar = new ModelerListPaginationBar(modelerContainer, repositoryService, clickListener, mapQuyen);
		if(paginationBar.getRowCount() > 0){
			CssLayout pagingWrap = new CssLayout();
			pagingWrap.setWidth("100%");
			pagingWrap.addStyleName(ExplorerLayout.PAGING_WRAP);
			pagingWrap.addComponent(paginationBar);
			addComponent(pagingWrap);
		}
	}

	public ModelerListPaginationBar getPaginationBar() {
		return paginationBar;
	}

	public void setPaginationBar(ModelerListPaginationBar paginationBar) {
		this.paginationBar = paginationBar;
	}
	
	
}

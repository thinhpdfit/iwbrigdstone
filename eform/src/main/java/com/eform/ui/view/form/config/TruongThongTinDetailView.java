package com.eform.ui.view.form.config;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.CommonUtils.ETimeDetail;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.DanhMucService;
import com.eform.service.LoaiDanhMucService;
import com.eform.service.TruongThongTinService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.LongRangeValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SpringView(name = TruongThongTinDetailView.VIEW_NAME)
public class TruongThongTinDetailView extends VerticalLayout implements View, ClickListener {

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "chi-tiet-truong-thong-tin";
	private static final String MAIN_TITLE = "Chi tiết trường thông tin";
	private static final String SMALL_TITLE = "";

	private final int WIDTH_FULL = 100;
	private static final String BUTTON_APPROVE_ID = "btn-approve";
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_ADD_ID = "btn-edit";
	private static final String BUTTON_DEL_ID = "btn-del";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_REFUSE_ID = "btn-refuse";
	private static final String BUTTON_ADD_LOAI_DM_ID = "btn-add-loai-dm";
	private static final String BUTTON_UPDATE_LOAI_DM_ID = "btn-update-loai-dm";

	private TruongThongTin truongThongTinCurrent;
	private WorkflowManagement<TruongThongTin> truongThongTinWM;
	Workflow<TruongThongTin> truongThongTinWf;;
	private String action;

	private List<LoaiDanhMuc> loaiDanhMucList;
	private final FieldGroup fieldGroup = new FieldGroup();
	private BeanItem<TruongThongTin> item;
	private CssLayout formContainer;
	private Button btnUpdate;
	private Button btnBack;
	
	private Window addDanhMucWindow;
	private final FieldGroup dmFieldGroup = new FieldGroup();
	private final PropertysetItem dmItem = new PropertysetItem();
	
	
	@Autowired
	private TruongThongTinService truongThongTinService;
	@Autowired
	private LoaiDanhMucService loaiDanhMucService;
	@Autowired
	private DanhMucService danhMucService;
	
	private boolean isWindow = false;
	@Autowired
	private MessageSource messageSource;

	public void initAddWindow(LoaiDanhMucService loaiDanhMucService, TruongThongTinService truongThongTinService, DanhMucService danhMucService){
		this.loaiDanhMucService = loaiDanhMucService;
		this.truongThongTinService = truongThongTinService;
		this.danhMucService = danhMucService;

		initAddDanhMucWindow();
		truongThongTinWM = new WorkflowManagement<TruongThongTin>(EChucNang.E_FORM_TRUONG_THONG_TIN.getMa());
		loaiDanhMucList = Lists.newArrayList(loaiDanhMucService.findAll());
		
		isWindow = true;
		this.action = Constants.ACTION_ADD;
		truongThongTinDetail(truongThongTinCurrent);
		refreshAdd();

		initDetailForm();
	}
	
	public void refreshAdd(){
		truongThongTinCurrent = new TruongThongTin();
		// setDefault
		truongThongTinCurrent.setLoaiHienThi(new Long(CommonUtils.ELoaiHienThi.INPUT.getCode()));
		truongThongTinCurrent.setRequired(new Long(CommonUtils.EBatBuoc.BAT_BUOC.getCode()));

		truongThongTinWf = truongThongTinWM.getWorkflow(truongThongTinCurrent);
		item = new BeanItem<TruongThongTin>(truongThongTinCurrent);
		item.addItemProperty("viTriHienThiTmp", new ObjectProperty<LinkedHashSet>(null, LinkedHashSet.class));

		item.addItemProperty("minDurationText", new ObjectProperty<String>(null, String.class));
		item.addItemProperty("minDurationDate", new ObjectProperty<Date>(null, Date.class));

		item.addItemProperty("maxDurationText", new ObjectProperty<String>(null, String.class));
		item.addItemProperty("maxDurationDate", new ObjectProperty<Date>(null, Date.class));

		item.addItemProperty("defaultValueDate", new ObjectProperty<Date>(null, Date.class));
		item.addItemProperty("defaultValueDateText", new ObjectProperty<String>(null, String.class));
		item.addItemProperty("timeDetailBoolean", new ObjectProperty<Boolean>(null, Boolean.class));
		item.addItemProperty("helpText", new ObjectProperty<String>(null, String.class));
		
		
		fieldGroup.setItemDataSource(item);
		//truongThongTinDetail(truongThongTinCurrent);
	}
	
	@PostConstruct
	void init() {
		truongThongTinWM = new WorkflowManagement<TruongThongTin>(EChucNang.E_FORM_TRUONG_THONG_TIN.getMa());
		loaiDanhMucList = Lists.newArrayList(loaiDanhMucService.findAll());
		initMainTitle();
		initAddDanhMucWindow();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if (params != null) {
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if (m.matches()) {
				action = m.group(1);
				if (Constants.ACTION_ADD.equals(action)) {
					truongThongTinCurrent = new TruongThongTin();
					// setDefault
					truongThongTinCurrent.setLoaiHienThi(new Long(CommonUtils.ELoaiHienThi.INPUT.getCode()));
					truongThongTinCurrent.setRequired(new Long(CommonUtils.EBatBuoc.BAT_BUOC.getCode()));

					truongThongTinWf = truongThongTinWM.getWorkflow(truongThongTinCurrent);
					truongThongTinDetail(truongThongTinCurrent);

					initDetailForm();
				} else {
					String ma = m.group(3);
					List<TruongThongTin> list = truongThongTinService.findByCode(ma);
					if (list != null && !list.isEmpty()) {
						truongThongTinCurrent = list.get(0);
						truongThongTinWf = truongThongTinWM.getWorkflow(truongThongTinCurrent);
						truongThongTinDetail(truongThongTinCurrent);

						initDetailForm();
					}

				}

			}
		}
	}
	
	public void initAddDanhMucWindow(){
		dmFieldGroup.setItemDataSource(dmItem);
		
		FormLayout form = new FormLayout();
		form.setMargin(true);
		
		TextField txtLoaiDanhMuc = new TextField("Loại danh mục");
		txtLoaiDanhMuc.setRequired(true);
		txtLoaiDanhMuc.setRequiredError("Không được để trống");
		txtLoaiDanhMuc.setNullRepresentation("");
		txtLoaiDanhMuc.setWidth("100%");
		txtLoaiDanhMuc.addValidator(new RegexpValidator("([a-zA-Z0-9_-]+)#(.+)", "Loại danh mục không đúng định dạng"));
		form.addComponent(txtLoaiDanhMuc);
		dmItem.addItemProperty("loaiDanhMuc", new ObjectProperty<String>(null, String.class));
		dmFieldGroup.bind(txtLoaiDanhMuc, "loaiDanhMuc");
		
		TextArea txtDanhMuc = new TextArea("Danh sách danh mục");
		txtDanhMuc.setRequired(true);
		txtDanhMuc.setRequiredError("Không được để trống");
		txtDanhMuc.setNullRepresentation("");
		txtDanhMuc.setWidth("100%");
		txtDanhMuc.setRows(5);
		txtDanhMuc.addValidator(new RegexpValidator("(([a-zA-Z0-9_-]+)#(.+))(\n(([a-zA-Z0-9_-]+)#(.+)))*", "Danh sách danh mục không đúng định dạng"));
		form.addComponent(txtDanhMuc);
		dmItem.addItemProperty("danhMuc", new ObjectProperty<String>(null, String.class));
		dmFieldGroup.bind(txtDanhMuc, "danhMuc");
		
		
		Button btnAdd = new Button("Thêm mới");
		btnAdd.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnAdd.addClickListener(this);
		btnAdd.setId(BUTTON_UPDATE_LOAI_DM_ID);
		form.addComponent(btnAdd);
		
		addDanhMucWindow = new Window("Thông tin loại danh mục");
		addDanhMucWindow.setWidth(600, Unit.PIXELS);
		addDanhMucWindow.setHeight(300, Unit.PIXELS);
		addDanhMucWindow.center();
		addDanhMucWindow.setModal(true);
		addDanhMucWindow.setContent(form);
	}

	private void truongThongTinDetail(TruongThongTin truongThongTin) {
		if (truongThongTin == null) {
			truongThongTin = new TruongThongTin();
		}
		item = new BeanItem<TruongThongTin>(truongThongTin);
		fieldGroup.setItemDataSource(item);
	}

	public void initMainTitle() {
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(TruongThongTinView.MAIN_TITLE);
		bcItem.setViewName(TruongThongTinView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}

	public void initDetailForm() {
		formContainer = new CssLayout();
		formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		formContainer.addStyleName(ExplorerLayout.DETAIL_WRAP);
		formContainer.addStyleName(ExplorerLayout.TTT_FORM_WRAP);
		Button btnAddLoaiDm = new Button();
		btnAddLoaiDm.setVisible(false);

		TextField txtMa = new TextField("Mã trường thông tin");
		txtMa.focus();
		txtMa.setRequired(true);
		txtMa.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void textChange(TextChangeEvent event) {
				TextField txt = (TextField) fieldGroup.getField("ma");
				System.out.println(event.getText());
				if (!checkTrungMa(event.getText(), action)) {
					txt.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
				} else {
					txt.setComponentError(null);
				}

			}

		});

		txtMa.setTextChangeEventMode(TextChangeEventMode.LAZY);

		txtMa.setSizeFull();
		txtMa.setNullRepresentation("");
		txtMa.setRequiredError("Không được để trống");
		txtMa.addValidator(new StringLengthValidator("Số ký tự tối đa là 20", 1, 20, false));
		txtMa.addValidator(new RegexpValidator("[0-9a-zA-Z_]+", "Chỉ chấp nhận các ký tự 0-9 a-z A-Z _"));

		TextField txtTen = new TextField("Tên trường thông tin");
		txtTen.setRequired(true);
		txtTen.setSizeFull();
		txtTen.setNullRepresentation("");
		txtTen.setRequiredError("Không được để trống");
		txtTen.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", 1, 2000, false));

		OptionGroup optLoaiHienThi = new OptionGroup("Loại hiển thị");
		for (CommonUtils.ELoaiHienThi item : CommonUtils.ELoaiHienThi.values()) {
			optLoaiHienThi.addItem(new Long(item.getCode()));
			optLoaiHienThi.setItemCaption(new Long(item.getCode()), item.getName());
		}
		optLoaiHienThi.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		optLoaiHienThi.setRequired(true);
		optLoaiHienThi.setRequiredError("Không được để trống");

		OptionGroup optRequired = new OptionGroup("Bắt buộc");
		for (CommonUtils.EBatBuoc item : CommonUtils.EBatBuoc.values()) {
			optRequired.addItem(new Long(item.getCode()));
			optRequired.setItemCaption(new Long(item.getCode()), item.getName());
		}
		optRequired.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		optRequired.setRequired(true);
		optRequired.setRequiredError("Không được để trống");

		BeanItemContainer<LoaiDanhMuc> container = new BeanItemContainer<LoaiDanhMuc>(LoaiDanhMuc.class,
				loaiDanhMucList);
		ComboBox cbbLoaiDanhMuc = new ComboBox("Loại danh mục", container);
		cbbLoaiDanhMuc.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		cbbLoaiDanhMuc.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbLoaiDanhMuc.setStyleName(ExplorerLayout.CBB_LOAI_DM);
		cbbLoaiDanhMuc.setItemCaptionPropertyId("ten");
		cbbLoaiDanhMuc.setVisible(false);
		
		TextArea txtDanhMuc = new TextArea("Danh sách danh mục");
		txtDanhMuc.setVisible(false);
		txtDanhMuc.setRows(5);
		txtDanhMuc.setSizeFull();
		txtDanhMuc.setNullRepresentation("");
		txtDanhMuc.setReadOnly(true);
		cbbLoaiDanhMuc.setData(txtDanhMuc);
		cbbLoaiDanhMuc.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				LoaiDanhMuc loaiDm = (LoaiDanhMuc) event.getProperty().getValue();
				if(loaiDm != null){
					List<DanhMuc> dmList= danhMucService.getDanhMucFind(null, null, null, loaiDm, null, -1, -1);
					if(dmList != null && !dmList.isEmpty()){
						String dmStr = "";
						for(DanhMuc dm : dmList){
							dmStr+=dm.getMa()+"#"+dm.getTen()+"\n";
						}
						txtDanhMuc.setReadOnly(false);
						txtDanhMuc.setValue(dmStr);
						txtDanhMuc.setReadOnly(true);
					}
				}
			}
		});

//		TextArea txtInlineStyle = new TextArea("Định kiểu css");
//		txtInlineStyle.setSizeFull();
//		txtInlineStyle.setNullRepresentation("");
		LinkedHashSet<String> viTriStr = null;
		if(truongThongTinCurrent != null){
			List<String> viTriHienThiList = new ArrayList();
			if(truongThongTinCurrent.getTableDisplay() != null 
					&& truongThongTinCurrent.getTableDisplay().intValue() == 1){
				viTriHienThiList.add(CommonUtils.EViTriHienThi.TABLE.getCode()+"");
			}
			if(truongThongTinCurrent.getSearchFormDisplay() != null 
					&& truongThongTinCurrent.getSearchFormDisplay().intValue() == 1){
				viTriHienThiList.add(CommonUtils.EViTriHienThi.SEARCH_FORM.getCode()+"");
			}
			viTriStr = new LinkedHashSet(viTriHienThiList);
		}
		item.addItemProperty("viTriHienThiTmp", new ObjectProperty<LinkedHashSet>(viTriStr, LinkedHashSet.class));
		OptionGroup viTriHienThiCheck = new OptionGroup("Vị trí hiển thị");
		viTriHienThiCheck.setMultiSelect(true);
		for (CommonUtils.EViTriHienThi item : CommonUtils.EViTriHienThi.values()) {
			viTriHienThiCheck.addItem(item.getCode()+"");
			viTriHienThiCheck.setItemCaption(item.getCode()+"", item.getName());
		}
		viTriHienThiCheck.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		
		TextField txtTableDisplayOrder = new TextField("Thứ tự hiển thị trên Table");
		txtTableDisplayOrder.setSizeFull();
		txtTableDisplayOrder.setNullRepresentation("");
		txtTableDisplayOrder.setConverter(Long.class);
		
		TextField txtSearchFormDisplayOrder = new TextField("Thứ tự hiển thị trên Search Form");
		txtSearchFormDisplayOrder.setSizeFull();
		txtSearchFormDisplayOrder.setNullRepresentation("");
		txtSearchFormDisplayOrder.setConverter(Long.class);

		TextField txtExpression = new TextField("Biểu thức kiểm tra dữ liệu");
		txtExpression.setSizeFull();
		txtExpression.setNullRepresentation("");
		txtExpression.setVisible(false);

		TextField txtExpressionMessage = new TextField("Thông báo khi sai biểu thức");
		txtExpressionMessage.setSizeFull();
		txtExpressionMessage.setNullRepresentation("");
		txtExpressionMessage.setVisible(false);

		TextField txtMinLength = new TextField("Độ dài tối thiểu");
		txtMinLength.setSizeFull();
		txtMinLength.setConverter(Long.class);
		txtMinLength
				.addValidator(new LongRangeValidator("Vui lòng nhập số nguyên không âm", new Long(0), Long.MAX_VALUE));
		txtMinLength.setNullRepresentation("");
		txtMinLength.setVisible(false);

		TextField txtMaxLength = new TextField("Độ dài tối đa");
		txtMaxLength.setSizeFull();
		txtMaxLength.setConverter(Long.class);
		txtMaxLength
				.addValidator(new LongRangeValidator("Vui lòng nhập số nguyên không âm", new Long(0), Long.MAX_VALUE));
		txtMaxLength.setNullRepresentation("");
		txtMaxLength.setVisible(false);

		TextField txtMinValue = new TextField("Giá trị nhỏ nhất");
		txtMinValue.setSizeFull();
		txtMinValue.setConverter(BigDecimal.class);
		txtMinValue.setNullRepresentation("");
		txtMinValue.setVisible(false);

		TextField txtMaxValue = new TextField("Giá trị lớn nhất");
		txtMaxValue.setSizeFull();
		txtMaxValue.setConverter(BigDecimal.class);
		txtMaxValue.setNullRepresentation("");
		txtMaxValue.setVisible(false);
		
		TextField txtFileSize = new TextField("Dung lượng file tối đa");
		txtFileSize.setSizeFull();
		txtFileSize.setConverter(Long.class);
		txtFileSize
				.addValidator(new LongRangeValidator("Vui lòng nhập số nguyên không âm", new Long(0), Long.MAX_VALUE));
		txtFileSize.setNullRepresentation("");
		txtFileSize.setVisible(false);
		
		TextField txtFileAlow = new TextField("Định dạng file cho phép");
		txtFileAlow.setSizeFull();
		txtFileAlow.setInputPrompt("Ví dụ: doc;docx;pdf");
		txtFileAlow.setNullRepresentation("");
		txtFileAlow.setVisible(false);
		
		
		
		
		//Date
		
		
		
		
		OptionGroup optMinDurationType = new OptionGroup("Thời gian nhỏ nhất");
		for (CommonUtils.ELoaiThoiGian item : CommonUtils.ELoaiThoiGian.values()) {
			optMinDurationType.addItem(new Long(item.getCode()));
			optMinDurationType.setItemCaption(new Long(item.getCode()), item.getName());
		}
		optMinDurationType.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		optMinDurationType.setVisible(false);
		
		String minDurationText = null;
		Date minDurationDate = null;
		if(truongThongTinCurrent != null && truongThongTinCurrent.getMinDurationType() != null){
			if(truongThongTinCurrent.getMinDurationType().intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()){
				if(truongThongTinCurrent.getMinDuration() != null){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
					try {
						minDurationDate = df.parse(truongThongTinCurrent.getMinDuration());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else if(truongThongTinCurrent.getMinDurationType().intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()){
				minDurationText = truongThongTinCurrent.getMinDuration();
			}
		}
		
		DateField minDate = new DateField();
		minDate.setSizeFull();
		minDate.setVisible(false);
		
		TextField txtMinduration = new TextField();
		txtMinduration.setSizeFull();
		txtMinduration.setInputPrompt("Ví dụ: P7D");
		txtMinduration.setNullRepresentation("");
		txtMinduration.setVisible(false);
		
		
		txtMinduration.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				txtMinduration.setComponentError(null);
				if(value != null && value instanceof String){
					String durationText = (String) value;
					if(!durationText.isEmpty()){
						try {
							Period.parse(durationText.trim());
						} catch (Exception e) {
							txtMinduration.setComponentError(new UserError("Dữ liệu không chính xác!"));
							System.err.println("duration parse error: "+durationText);
						}
					}
					
				}
			}
		});
		
		item.addItemProperty("minDurationText", new ObjectProperty<String>(minDurationText, String.class));
		item.addItemProperty("minDurationDate", new ObjectProperty<Date>(minDurationDate, Date.class));
		
		OptionGroup optMaxDurationType = new OptionGroup("Thời gian lớn nhất");
		for (CommonUtils.ELoaiThoiGian item : CommonUtils.ELoaiThoiGian.values()) {
			optMaxDurationType.addItem(new Long(item.getCode()));
			optMaxDurationType.setItemCaption(new Long(item.getCode()), item.getName());
		}
		optMaxDurationType.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		optMaxDurationType.setVisible(false);
		
		DateField maxDate = new DateField();
		maxDate.setSizeFull();
		maxDate.setVisible(false);
		
		TextField txtMaxduration = new TextField();
		txtMaxduration.setSizeFull();
		txtMaxduration.setInputPrompt("Ví dụ: P7D");
		txtMaxduration.setNullRepresentation("");
		txtMaxduration.setVisible(false);
		
		txtMaxduration.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				txtMaxduration.setComponentError(null);
				if(value != null && value instanceof String){
					String durationText = (String) value;
					if(!durationText.isEmpty()){
						try {
							Period.parse(durationText.trim());
						} catch (Exception e) {
							txtMaxduration.setComponentError(new UserError("Dữ liệu không chính xác!"));
							System.err.println("duration parse error: "+durationText);
						}
					}
					
				}
			}
		});
		
		
		
		OptionGroup optDateDefaultType = new OptionGroup("Giá trị mặc định");
		for (CommonUtils.ELoaiThoiGian item : CommonUtils.ELoaiThoiGian.values()) {
			optDateDefaultType.addItem(new Long(item.getCode()));
			optDateDefaultType.setItemCaption(new Long(item.getCode()), item.getName());
		}
		optDateDefaultType.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		optDateDefaultType.setVisible(false);
		
		DateField defaultValue = new DateField();
		defaultValue.setSizeFull();
		defaultValue.setVisible(false);
		
		TextField txtDefaultduration = new TextField();
		txtDefaultduration.setSizeFull();
		txtDefaultduration.setInputPrompt("Ví dụ: P7D");
		txtDefaultduration.setNullRepresentation("");
		txtDefaultduration.setVisible(false);
		
		txtDefaultduration.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				txtDefaultduration.setComponentError(null);
				if(value != null && value instanceof String){
					String durationText = (String) value;
					if(!durationText.isEmpty()){
						try {
							java.time.Duration.parse(durationText.trim());
						} catch (Exception e) {
							txtDefaultduration.setComponentError(new UserError("Dữ liệu không chính xác!"));
							System.err.println("duration parse error: "+durationText);
						}
					}
					
				}
			}
		});
		
		
		String maxDurationText = null;
		Date maxDurationDate = null;
		if(truongThongTinCurrent != null && truongThongTinCurrent.getMaxDurationType() != null){
			if(truongThongTinCurrent.getMaxDurationType().intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()){
				if(truongThongTinCurrent.getMaxDuration() != null){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
					try {
						maxDurationDate = df.parse(truongThongTinCurrent.getMaxDuration());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else if(truongThongTinCurrent.getMaxDurationType().intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()){
				maxDurationText = truongThongTinCurrent.getMaxDuration();
			}
		}
		item.addItemProperty("maxDurationText", new ObjectProperty<String>(maxDurationText, String.class));
		item.addItemProperty("maxDurationDate", new ObjectProperty<Date>(maxDurationDate, Date.class));
		
		Date defaultDate = null;
		String defaultduration = null;
		Boolean timeDetail = null;
		if(truongThongTinCurrent != null && truongThongTinCurrent.getKieuDuLieu() != null && truongThongTinCurrent.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
			if(truongThongTinCurrent.getDateDefaultType() != null){
				if(truongThongTinCurrent.getDateDefaultType().intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()){
					if(truongThongTinCurrent.getDefaultValue() != null){
						SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
						try {
							defaultDate = df.parse(truongThongTinCurrent.getDefaultValue());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}else if(truongThongTinCurrent.getDateDefaultType().intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()){
					defaultduration = truongThongTinCurrent.getDefaultValue();
				}
			}
			if(truongThongTinCurrent.getTimeDetail() != null && ETimeDetail.DATE_AND_TIME.getCode() == truongThongTinCurrent.getTimeDetail().intValue()){
				timeDetail = true;
			}else{
				timeDetail = false;
			}
		}
		
		item.addItemProperty("defaultValueDate", new ObjectProperty<Date>(defaultDate, Date.class));
		item.addItemProperty("defaultValueDateText", new ObjectProperty<String>(defaultduration, String.class));

		
		
		optMinDurationType.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				if(value != null && value instanceof Long){
					Long type = (Long) value;
					if(type.intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()){
						minDate.setVisible(false);
						txtMinduration.setVisible(true);
					}else if(type.intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()){
						minDate.setVisible(true);
						txtMinduration.setVisible(false);
					}
				}
			}
		});
		
		
		
		optMaxDurationType.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				if(value != null && value instanceof Long){
					Long type = (Long) value;
					if(type.intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()){
						maxDate.setVisible(false);
						txtMaxduration.setVisible(true);
					}else if(type.intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()){
						maxDate.setVisible(true);
						txtMaxduration.setVisible(false);
					}
				}
			}
		});	
		
		
		
		optDateDefaultType.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				if(value != null && value instanceof Long){
					Long type = (Long) value;
					if(type.intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()){
						defaultValue.setVisible(false);
						txtDefaultduration.setVisible(true);
					}else if(type.intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()){
						defaultValue.setVisible(true);
						txtDefaultduration.setVisible(false);
					}
				}
			}
		});	
		
		item.addItemProperty("timeDetailBoolean", new ObjectProperty<Boolean>(timeDetail, Boolean.class));
		
		CheckBox timeDetailChk = new CheckBox("Bao gồm giờ, phút");
		timeDetailChk.setVisible(false);
		
		
		
		ComboBox optKieuDuLieu = new ComboBox("Kiểu dữ liệu");
		for (CommonUtils.EKieuDuLieu item : CommonUtils.EKieuDuLieu.values()) {
			optKieuDuLieu.addItem(new Long(item.getMa()));
			optKieuDuLieu.setItemCaption(new Long(item.getMa()), item.getTen());
		}
		
		RichTextArea helpText = new RichTextArea("Thông tin trợ giúp");
		helpText.setWidth("100%");
		helpText.setNullRepresentation("");
		helpText.addStyleName(ExplorerLayout.FORM_CONTROL);
		helpText.addStyleName(ExplorerLayout.FORM_EDITOR);
		helpText.setVisible(false);
		
		String helpTextValue = null;
		if(truongThongTinCurrent != null && truongThongTinCurrent.getKieuDuLieu() != null && truongThongTinCurrent.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.HELP_TEXT.getMa()){
			helpTextValue = truongThongTinCurrent.getDefaultValue();
		}
		
		item.addItemProperty("helpText", new ObjectProperty<String>(helpTextValue, String.class));
		
		optKieuDuLieu.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		optKieuDuLieu.setRequired(true);
		optKieuDuLieu.setRequiredError("Không được để trống");
		optKieuDuLieu.addValueChangeListener(new com.vaadin.data.Property.ValueChangeListener() {
			private static final long serialVersionUID = -7983563785080148670L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				ComboBox cbb = (ComboBox) fieldGroup.getField("kieuDuLieu");
				ComboBox cbbLoaiDM = (ComboBox) fieldGroup.getField("loaiDanhMuc");
				TextField txtMinvalue = (TextField) fieldGroup.getField("minvalue");
				TextField txtMaxvalue = (TextField) fieldGroup.getField("maxvalue");
				TextField txtMinlength = (TextField) fieldGroup.getField("minlength");
				TextField txtMaxlength = (TextField) fieldGroup.getField("maxlength");
				TextField txtExpressionMessage = (TextField) fieldGroup.getField("expressionMessage");
				TextField txtExpressions = (TextField) fieldGroup.getField("expressions");
				TextField txtFileSize = (TextField) fieldGroup.getField("fileSize");
				Long value = (Long) cbb.getValue();
				if (value != null && CommonUtils.EKieuDuLieu.isCoDanhMuc(value.intValue())) {
					cbbLoaiDM.setVisible(true);
					TextArea txt = (TextArea) cbbLoaiDM.getData();
					txt.setVisible(true);
					cbbLoaiDanhMuc.setRequired(true);
					btnAddLoaiDm.setVisible(true);
					cbbLoaiDanhMuc.setRequiredError("Không được để trống");
				} else {
					cbbLoaiDM.setVisible(false);
					btnAddLoaiDm.setVisible(false);
					cbbLoaiDanhMuc.setRequired(false);
					TextArea txt = (TextArea) cbbLoaiDM.getData();
					txt.setVisible(false);
				}
				
				if (value != null && (CommonUtils.EKieuDuLieu.NUMBER.getMa() == value.intValue()
						|| CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa() == value.intValue())) {
					txtMinvalue.setVisible(true);
					txtMaxvalue.setVisible(true);
				} else {
					txtMinvalue.setVisible(false);
					txtMaxvalue.setVisible(false);
				}
				if (value != null && (CommonUtils.EKieuDuLieu.TEXT.getMa() == value.intValue()
						|| CommonUtils.EKieuDuLieu.TEXT_AREA.getMa() == value.intValue()
						|| CommonUtils.EKieuDuLieu.EDITOR.getMa() == value.intValue())) {
					txtMinlength.setVisible(true);
					txtMaxlength.setVisible(true);
					txtExpressionMessage.setVisible(true);
					txtExpressions.setVisible(true);
				} else {
					txtMinlength.setVisible(false);
					txtMaxlength.setVisible(false);
					txtExpressionMessage.setVisible(false);
					txtExpressions.setVisible(false);
				}
		
				
				if(value != null && (CommonUtils.EKieuDuLieu.FILE.getMa() == value.intValue())){
					txtFileAlow.setVisible(true);
					txtFileSize.setVisible(true);
					txtFileSize.setRequired(true);
					txtFileSize.setRequiredError("Không được để trống");
				}else if(value != null && (CommonUtils.EKieuDuLieu.XAC_THUC_FILE.getMa() == value.intValue())){
					txtFileAlow.setVisible(true);
					txtFileSize.setVisible(true);
					txtFileAlow.setValue("doc;docx;pdf");
					txtFileAlow.setReadOnly(true);
					txtFileSize.setRequired(true);
					txtFileSize.setRequiredError("Không được để trống");
				}else{
					txtFileAlow.setVisible(false);
					txtFileAlow.setReadOnly(false);
					txtFileAlow.setValue("");
					txtFileSize.setVisible(false);
					txtFileSize.setRequired(false);
				}
				
				if(value != null && (CommonUtils.EKieuDuLieu.DATE.getMa() == value.intValue())){
					optMaxDurationType.setVisible(true);
					optMinDurationType.setVisible(true);
					optDateDefaultType.setVisible(true);
					timeDetailChk.setVisible(true);
					
				}else{
					optDateDefaultType.setVisible(false);
					optMaxDurationType.setVisible(false);
					optMinDurationType.setVisible(false);
					defaultValue.setVisible(false);
					txtMinduration.setVisible(false);
					txtMaxduration.setVisible(false);
					minDate.setVisible(false);
					maxDate.setVisible(false);
					txtDefaultduration.setVisible(false);
					defaultValue.setVisible(false);
					timeDetailChk.setVisible(false);
				}
				if(value != null && (CommonUtils.EKieuDuLieu.HELP_TEXT.getMa() == value.intValue())){
					helpText.setVisible(true);
				}else{
					helpText.setVisible(false);
				}
				
			}
		});

		fieldGroup.bind(txtMa, "ma");
		fieldGroup.bind(txtTen, "ten");
		fieldGroup.bind(optLoaiHienThi, "loaiHienThi");
		fieldGroup.bind(optRequired, "required");
		fieldGroup.bind(cbbLoaiDanhMuc, "loaiDanhMuc");
		fieldGroup.bind(txtMinLength, "minlength");
		fieldGroup.bind(txtMaxLength, "maxlength");
		fieldGroup.bind(txtMinValue, "minvalue");
		fieldGroup.bind(txtMaxValue, "maxvalue");
//		fieldGroup.bind(txtInlineStyle, "inlineStyle");
		fieldGroup.bind(viTriHienThiCheck, "viTriHienThiTmp");
		fieldGroup.bind(txtExpression, "expressions");
		fieldGroup.bind(txtExpressionMessage, "expressionMessage");
		fieldGroup.bind(txtFileAlow, "fileExtensionAlow");
		fieldGroup.bind(txtFileSize, "fileSize");
		fieldGroup.bind(optKieuDuLieu, "kieuDuLieu");
		
		fieldGroup.bind(optMinDurationType, "minDurationType");
		fieldGroup.bind(optMaxDurationType, "maxDurationType");
		
		fieldGroup.bind(optDateDefaultType, "dateDefaultType");
		
		fieldGroup.bind(txtMinduration, "minDurationText");
		fieldGroup.bind(txtMaxduration, "maxDurationText");
		fieldGroup.bind(minDate, "minDurationDate");
		fieldGroup.bind(maxDate, "maxDurationDate");
		fieldGroup.bind(defaultValue, "defaultValueDate");
		fieldGroup.bind(txtDefaultduration, "defaultValueDateText");
		fieldGroup.bind(helpText, "helpText");
		fieldGroup.bind(timeDetailChk, "timeDetailBoolean");
		fieldGroup.bind(txtTableDisplayOrder, "tableDisplayOrder");
		fieldGroup.bind(txtSearchFormDisplayOrder, "searchFormDisplayOrder");
		
		
		
		
		txtMa.setReadOnly("xem".equals(action));
		txtTen.setReadOnly("xem".equals(action));
		optKieuDuLieu.setReadOnly("xem".equals(action));
		optLoaiHienThi.setReadOnly("xem".equals(action));
		cbbLoaiDanhMuc.setReadOnly("xem".equals(action));
		optRequired.setReadOnly("xem".equals(action));
		viTriHienThiCheck.setReadOnly("xem".equals(action));
//		txtInlineStyle.setReadOnly("xem".equals(action));
		txtExpression.setReadOnly("xem".equals(action));
		txtExpressionMessage.setReadOnly("xem".equals(action));
		txtMinLength.setReadOnly("xem".equals(action));
		txtMaxLength.setReadOnly("xem".equals(action));
		txtMinValue.setReadOnly("xem".equals(action));
		txtMaxValue.setReadOnly("xem".equals(action));
		txtFileAlow.setReadOnly("xem".equals(action));
		txtFileSize.setReadOnly("xem".equals(action));
		
		optMinDurationType.setReadOnly("xem".equals(action));
		optMaxDurationType.setReadOnly("xem".equals(action));
		defaultValue.setReadOnly("xem".equals(action));
		txtMinduration.setReadOnly("xem".equals(action));
		txtMaxduration.setReadOnly("xem".equals(action));
		minDate.setReadOnly("xem".equals(action));
		maxDate.setReadOnly("xem".equals(action));
		

		optDateDefaultType.setReadOnly("xem".equals(action));
		txtDefaultduration.setReadOnly("xem".equals(action));
		helpText.setReadOnly("xem".equals(action));
		timeDetailChk.setReadOnly("xem".equals(action));
		txtTableDisplayOrder.setReadOnly("xem".equals(action));
		txtSearchFormDisplayOrder.setReadOnly("xem".equals(action));
		
		
		CssLayout formRowGeneral = new CssLayout();
		formRowGeneral.setWidth("100%");
		formRowGeneral.addStyleName(ExplorerLayout.FORM_ROW);
		
		Label generalTtl = new Label("Thông tin chung");
		generalTtl.addStyleName(ExplorerLayout.FORM_ROW_TITLE);
		formRowGeneral.addComponent(generalTtl);
		
		Button showGeneralForm = new Button();
		showGeneralForm.setStyleName(ExplorerLayout.BTN_SHOW_GENERAL_FORM);
		showGeneralForm.setIcon(FontAwesome.ANGLE_DOWN);
		formRowGeneral.addComponent(showGeneralForm);
		
		formContainer.addComponent(formRowGeneral);
		
		FormLayout formGeneral = new FormLayout();
		formGeneral.addStyleName(ExplorerLayout.FORM_TTT_GENERAL);
		
		formGeneral.setMargin(true);
		formGeneral.addComponent(txtMa);
		formGeneral.addComponent(txtTen);
		formGeneral.addComponent(optLoaiHienThi);
		formGeneral.addComponent(optRequired);
		formGeneral.addComponent(optKieuDuLieu);
		
		formContainer.addComponent(formGeneral);
		
		CssLayout formRowSetting = new CssLayout();
		formRowSetting.setWidth("100%");
		formRowSetting.addStyleName(ExplorerLayout.FORM_ROW);
		
		Label settinglTtl = new Label("Cấu hình chi tiết");
		settinglTtl.addStyleName(ExplorerLayout.FORM_ROW_TITLE);
		formRowSetting.addComponent(settinglTtl);
		
		Button showSettingForm = new Button();
		showSettingForm.setStyleName(ExplorerLayout.BTN_SHOW_SETTING_FORM);
		showSettingForm.setIcon(FontAwesome.ANGLE_DOWN);
		formRowSetting.addComponent(showSettingForm);
		
		formContainer.addComponent(formRowSetting);
		
		FormLayout formSetting = new FormLayout();
		formSetting.addStyleName(ExplorerLayout.FORM_TTT_SETTING);
		formSetting.setMargin(true);
		
		
		btnAddLoaiDm.setIcon(FontAwesome.PLUS);
		btnAddLoaiDm.addStyleName(ExplorerLayout.BTN_ADD_LOAI_DM);
		btnAddLoaiDm.setId(BUTTON_ADD_LOAI_DM_ID);
		btnAddLoaiDm.addClickListener(this);
		

		formSetting.addComponent(btnAddLoaiDm);

		formSetting.addComponent(cbbLoaiDanhMuc);
		formSetting.addComponent(txtDanhMuc);
		formSetting.addComponent(txtMinLength);
		formSetting.addComponent(txtMaxLength);
		formSetting.addComponent(txtMinValue);
		formSetting.addComponent(txtMaxValue);
		formSetting.addComponent(txtExpression);
		formSetting.addComponent(txtExpressionMessage);
		formSetting.addComponent(txtFileSize);
		formSetting.addComponent(txtFileAlow);
		
		

		formSetting.addComponent(optMinDurationType);
		formSetting.addComponent(txtMinduration);
		formSetting.addComponent(minDate);
		formSetting.addComponent(optMaxDurationType);
		formSetting.addComponent(txtMaxduration);
		formSetting.addComponent(maxDate);
		formSetting.addComponent(optDateDefaultType);
		
		formSetting.addComponent(defaultValue);
		formSetting.addComponent(txtDefaultduration);
		formSetting.addComponent(timeDetailChk);
		formSetting.addComponent(helpText);
		formSetting.addComponent(viTriHienThiCheck);
		formSetting.addComponent(txtTableDisplayOrder);
		formSetting.addComponent(txtSearchFormDisplayOrder);
//		formSetting.addComponent(txtInlineStyle);
		
		formContainer.addComponent(formSetting);
		
		
		if(!isWindow){
			HorizontalLayout buttons = new HorizontalLayout();
			buttons.setSpacing(true);
			buttons.addStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
			buttons.addStyleName(ExplorerLayout.TTT_FORM_BUTTONS);
			if (!Constants.ACTION_VIEW.equals(action)) {
				btnUpdate = new Button();
				btnUpdate.addClickListener(this);
				buttons.addComponent(btnUpdate);
				if (Constants.ACTION_EDIT.equals(action)) {
					btnUpdate.setCaption("Cập nhật");
					btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
					btnUpdate.setId(BUTTON_EDIT_ID);
				} else if (Constants.ACTION_ADD.equals(action)) {
					btnUpdate.setId(BUTTON_ADD_ID);
					btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
					btnUpdate.setCaption("Thêm mới");
				} else if (Constants.ACTION_DEL.equals(action)) {
					btnUpdate.setId(BUTTON_DEL_ID);
					btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
					btnUpdate.setCaption("Xóa");
				} else if (Constants.ACTION_APPROVE.equals(action)) {
					btnUpdate.setId(BUTTON_APPROVE_ID);
					btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
					btnUpdate.setCaption(truongThongTinWf.getPheDuyetTen());
				} else if (Constants.ACTION_REFUSE.equals(action)) {
					btnUpdate.setCaption(truongThongTinWf.getTuChoiTen());
					btnUpdate.setId(BUTTON_REFUSE_ID);
					btnUpdate.addStyleName(ExplorerLayout.BUTTON_WARNING);
				}
			}
			btnBack = new Button("Quay lại");
			btnBack.setId(BUTTON_CANCEL_ID);
			btnBack.addClickListener(this);
			buttons.addComponent(btnBack);
			formContainer.addComponent(buttons);

		}
		addComponent(formContainer);
	}

	public boolean checkTrungMa(String ma, String act) {
		try {
			if (ma == null || ma.isEmpty()) {
				return false;
			}
			if (Constants.ACTION_ADD.equals(act)) {
				List<TruongThongTin> list = truongThongTinService.findByCode(ma);
				if (list == null || list.isEmpty()) {
					return true;
				}
			} else if (!Constants.ACTION_DEL.equals(act)) {
				List<TruongThongTin> list = truongThongTinService.findByCode(ma);
				if (list == null || list.size() <= 1) {
					return true;
				}
			} else if (Constants.ACTION_DEL.equals(act)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public TruongThongTin addNew(){
		try {
			if (fieldGroup.isValid()) {
				fieldGroup.commit();
				TruongThongTin current = ((BeanItem<TruongThongTin>) fieldGroup.getItemDataSource()).getBean();
				Long trangThaiSD = null;
				if(truongThongTinWf.getTrangThaiSuDung() != null){
					trangThaiSD = new Long(truongThongTinWf.getTrangThaiSuDung().getId());
				}
				current.setTrangThai(trangThaiSD);
				
				setAdvanceInfo(current);
				
				return truongThongTinService.save(current);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				refreshAdd();
				fieldGroup.clear();
				OptionGroup loaiHT = (OptionGroup) fieldGroup.getField("loaiHienThi");
				loaiHT.select(new Long(CommonUtils.ELoaiHienThi.INPUT.getCode()));
				OptionGroup required = (OptionGroup) fieldGroup.getField("required");
				required.select(new Long(CommonUtils.EBatBuoc.BAT_BUOC.getCode()));
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if (BUTTON_CANCEL_ID.equals(button.getId())) {
				UI.getCurrent().getNavigator().navigateTo(TruongThongTinView.VIEW_NAME);
			}else if (BUTTON_ADD_LOAI_DM_ID.equals(button.getId())) {
				UI.getCurrent().addWindow(addDanhMucWindow);
			}else if (BUTTON_UPDATE_LOAI_DM_ID.equals(button.getId())) {
				if(dmFieldGroup.isValid()){
					dmFieldGroup.commit();
					String loaiDanhMuc = (String) dmItem.getItemProperty("loaiDanhMuc").getValue();
					String danhMuc = (String) dmItem.getItemProperty("danhMuc").getValue();
					
					Pattern p = Pattern.compile("([a-zA-Z0-9_-]+)#(.+)");
					Matcher m = p.matcher(loaiDanhMuc);
					
					LoaiDanhMuc loaiDanhMucObj = null;
					if(m.matches()){
						String maLoaiDm = m.group(1);
						String tenLoaiDm = m.group(2);
						loaiDanhMucObj = new LoaiDanhMuc();
						loaiDanhMucObj.setMa(maLoaiDm);
						loaiDanhMucObj.setTen(tenLoaiDm);
						loaiDanhMucObj.setTrangThai(1L);
					}
					List<DanhMuc> danhMucList = new ArrayList<DanhMuc>();
					if(danhMuc != null && !danhMuc.isEmpty()){
						String [] dmArr = danhMuc.split("\\n");
						if(dmArr != null && dmArr.length > 0){
							for(String dm : dmArr){
								Matcher m1 = p.matcher(dm);
								if(m1.matches()){
									String maDm = m1.group(1);
									String tenDm = m1.group(2);
									DanhMuc dmObj = new DanhMuc();
									dmObj.setMa(maDm);
									dmObj.setTen(tenDm);
									dmObj.setTrangThai(1L);
									dmObj.setLoaiDanhMuc(loaiDanhMucObj);
									danhMucList.add(dmObj);
								}
							}
						}
					}
					
					if(loaiDanhMucObj != null && danhMucList != null && !danhMucList.isEmpty()){
						loaiDanhMucObj = loaiDanhMucService.save(loaiDanhMucObj, danhMucList);
					}
					
					ComboBox cbbLoaiDM = (ComboBox) fieldGroup.getField("loaiDanhMuc");
					loaiDanhMucList.add(loaiDanhMucObj);
					BeanItemContainer<LoaiDanhMuc> container = (BeanItemContainer<LoaiDanhMuc>) cbbLoaiDM.getContainerDataSource();
					if(cbbLoaiDM != null && cbbLoaiDM.getData() != null){
						TextArea txt = (TextArea) cbbLoaiDM.getData();
						txt.setReadOnly(false);
						txt.setValue(danhMuc);
						txt.setReadOnly(true);
					}
					container.removeAllItems();
					container.addAll(loaiDanhMucList);
					cbbLoaiDM.select(loaiDanhMucObj);
					dmFieldGroup.clear();
					addDanhMucWindow.close();
				}
			} else {
				if (Constants.ACTION_EDIT.equals(action)) {
					Notification notf = new Notification("Thông báo", "Cập nhật thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid()) {
						fieldGroup.commit();
						TruongThongTin current = ((BeanItem<TruongThongTin>) fieldGroup.getItemDataSource()).getBean();
						
						setAdvanceInfo(current);
						
						notf.show(Page.getCurrent());
						truongThongTinService.save(current);
						UI.getCurrent().getNavigator().navigateTo(TruongThongTinView.VIEW_NAME);

					}
				} else if (Constants.ACTION_ADD.equals(action)) {
					Notification notf = new Notification("Thông báo", "Thêm mới thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid()) {
						fieldGroup.commit();
						
						
						
						
						TruongThongTin current = ((BeanItem<TruongThongTin>) fieldGroup.getItemDataSource()).getBean();
						
						setAdvanceInfo(current);
						
						notf.show(Page.getCurrent());
						truongThongTinService.save(current);
						UI.getCurrent().getNavigator().navigateTo(TruongThongTinView.VIEW_NAME);
					}
				} else if (Constants.ACTION_DEL.equals(action)) {
					Notification notf = new Notification("Thông báo", "Xóa thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid()) {
						fieldGroup.commit();
						notf.show(Page.getCurrent());
						truongThongTinService
								.delete(((BeanItem<TruongThongTin>) fieldGroup.getItemDataSource()).getBean());
						UI.getCurrent().getNavigator().navigateTo(TruongThongTinView.VIEW_NAME);
					}
				} else if (Constants.ACTION_APPROVE.equals(action)) {
					Notification notf = new Notification("Thông báo",
							truongThongTinWf.getPheDuyetTen() + " thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid()) {
						fieldGroup.commit();
						TruongThongTin current = truongThongTinWf.pheDuyet();
						
						setAdvanceInfo(current);
						
						notf.show(Page.getCurrent());
						truongThongTinService.save(current);
						UI.getCurrent().getNavigator().navigateTo(TruongThongTinView.VIEW_NAME);
					}
				} else if (Constants.ACTION_REFUSE.equals(action)) {
					Notification notf = new Notification("Thông báo", truongThongTinWf.getTuChoiTen() + " thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if (fieldGroup.isValid()) {
						fieldGroup.commit();
						TruongThongTin current = truongThongTinWf.tuChoi();
						
						setAdvanceInfo(current);
						
						notf.show(Page.getCurrent());
						truongThongTinService.save(current);
						UI.getCurrent().getNavigator().navigateTo(TruongThongTinView.VIEW_NAME);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra", Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
		}

	}

	private void setAdvanceInfo(TruongThongTin current) {
		if(current != null){
			try {
				Object viTriHienThiObj =  item.getItemProperty("viTriHienThiTmp").getValue();
				if(viTriHienThiObj instanceof LinkedHashSet){
					LinkedHashSet<String> value  = (LinkedHashSet<String>) viTriHienThiObj;
					current.setSearchFormDisplay(0L);
					current.setTableDisplay(0L);
					for(String str : value){
						if((CommonUtils.EViTriHienThi.SEARCH_FORM.getCode()+"").equals(str)){
							current.setSearchFormDisplay(1L);
						}
						if((CommonUtils.EViTriHienThi.TABLE.getCode()+"").equals(str)){
							current.setTableDisplay(1L);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(current != null
				&& current.getKieuDuLieu() != null 
				&& current.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.HELP_TEXT.getMa()){
			Object helpTextObj =  item.getItemProperty("helpText").getValue();
			if(helpTextObj != null && helpTextObj instanceof String){
				String helpText = (String) helpTextObj;
				current.setDefaultValue(helpText);
			}
		}
		if(current != null 
				&& current.getKieuDuLieu() != null 
				&& current.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){

			Object timeDetailBooleanObj =  item.getItemProperty("timeDetailBoolean").getValue();
			if(timeDetailBooleanObj != null && timeDetailBooleanObj instanceof Boolean){
				Boolean timeDetailBoolean = (Boolean) timeDetailBooleanObj;
				if(timeDetailBoolean != null && timeDetailBoolean){
					current.setTimeDetail(new Long(CommonUtils.ETimeDetail.DATE_AND_TIME.getCode()));
				}else{
					current.setTimeDetail(new Long(CommonUtils.ETimeDetail.DATE.getCode()));
				}
			}
			
			SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
			if(current.getMinDurationType() != null ){
				if(current.getMinDurationType().intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()){
					Object minDateObj = item.getItemProperty("minDurationDate").getValue();
					if(minDateObj != null && minDateObj instanceof Date){
						Date minDate = (Date) minDateObj;
						current.setMinDuration(df.format(minDate));
					}
				}else if(current.getMinDurationType().intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()){
					Object minDurationObj =  item.getItemProperty("minDurationText").getValue();
					if(minDurationObj != null && minDurationObj instanceof String){
						String minDuration = (String) minDurationObj;
						current.setMinDuration(minDuration.trim());
					}
				}
			}
			
			if(current.getMaxDurationType() != null ){
				if(current.getMaxDurationType().intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()){
					Object maxDateObj = item.getItemProperty("maxDurationDate").getValue();
					if(maxDateObj != null && maxDateObj instanceof Date){
						Date maxDate = (Date) maxDateObj;
						current.setMaxDuration(df.format(maxDate));
					}
				}else if(current.getMaxDurationType().intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()){
					Object maxDurationObj =  item.getItemProperty("maxDurationText").getValue();
					if(maxDurationObj != null && maxDurationObj instanceof String){
						String maxDuration = (String) maxDurationObj;
						current.setMaxDuration(maxDuration.trim());
					}
				}
			}
			
			if(current.getDateDefaultType() != null ){
				if(current.getDateDefaultType().intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()){
					Object defaultDateObj = item.getItemProperty("defaultValueDate").getValue();
					if(defaultDateObj != null && defaultDateObj instanceof Date){
						Date date = (Date) defaultDateObj;
						current.setDefaultValue(df.format(date));
					}
				}else if(current.getDateDefaultType().intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()){
					Object defaultDateObj =  item.getItemProperty("defaultValueDateText").getValue();
					if(defaultDateObj != null && defaultDateObj instanceof String){
						String defaultDuration = (String) defaultDateObj;
						current.setDefaultValue(defaultDuration.trim());
					}
				}
			}
			
			
		}
	}
}
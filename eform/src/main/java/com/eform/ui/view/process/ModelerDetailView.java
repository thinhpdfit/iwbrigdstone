package com.eform.ui.view.process;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.CustomProperty;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.SubProcess;
import org.activiti.bpmn.model.UserTask;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;

import com.eform.activiti.custom.BpmnJsonConverterCustom;
import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.ThamSo;
import com.eform.common.type.ModelInfo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.SolrUtils;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.NhomQuyTrinh;
import com.eform.model.entities.ProcessTaskProperty;
import com.eform.service.DeploymentHtDonViService;
import com.eform.service.HtDonViServices;
import com.eform.service.HtThamSoService;
import com.eform.service.NhomQuyTrinhService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.colorpicker.Color;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ColorPicker;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.colorpicker.ColorChangeEvent;
import com.vaadin.ui.components.colorpicker.ColorChangeListener;

@SpringView(name = ModelerDetailView.VIEW_NAME)
public class ModelerDetailView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "modeler-detail";
	private static  String MAIN_TITLE = "";
	private static  String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_ADD_ID = "btn-add";
	private static final String BUTTON_DEPLOY_ID = "btn-deploy";
	private static final String BUTTON_DEL_ID = "btn-del";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";

	private ModelInfo modelInfoCurrent;
	private Model modelCurrent;
	private String action;
	
	private FieldGroup fieldGroup;
	private CssLayout formContainer;
	private Button btnUpdate;
	private Button btnBack;
	private TreeTable donViTreeTable;
	private Map<Long,Boolean> donViSelected;
	private Map<Long,CheckBox> donViCheckboxMap;
	
	private WorkflowManagement<HtDonVi> htDonViWM;
	

	@Autowired
	protected RepositoryService repositoryService;
	@Autowired
	private HtDonViServices htDonViServices;
	@Autowired
	private DeploymentHtDonViService deploymentHtDonViService;
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;
	private Color appColor;
	private List<NhomQuyTrinh> nhomQuyTrinhList;
	@Autowired
	private NhomQuyTrinhService nhomQuyTrinhService;	
	@Autowired
	private HtThamSoService htThamSoService;
	
	private String solrUrl;
	@Autowired
	private ObjectMapper objectMapper;

    @PostConstruct
    void init() {
		messageSourceAccessor= new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());

    	MAIN_TITLE = messageSourceAccessor.getMessage(Messages.UI_VIEW_MODELER_DETAIL_TITLE);
		SMALL_TITLE = messageSourceAccessor.getMessage(Messages.UI_VIEW_MODELER_DETAIL_TITLE_SMALL);
    	initMainTitle();
    	donViSelected = new HashMap();
    	donViCheckboxMap = new HashMap();
    	
    	HtThamSo solrUrlThamSo = htThamSoService.getHtThamSoFind(ThamSo.SOLRSEARCH_URL);
		if(solrUrlThamSo != null && solrUrlThamSo.getGiaTri()!= null){
			solrUrl = solrUrlThamSo.getGiaTri();
		}
    	
    	List<Long> trangThai = null;
    	WorkflowManagement<NhomQuyTrinh> wf = new WorkflowManagement<NhomQuyTrinh>(EChucNang.E_FORM_NHOM_QUY_TRINH.getMa());
    	if(wf != null && wf.getTrangThaiSuDung() != null){
    		trangThai = new ArrayList();
    		trangThai.add(new Long(wf.getTrangThaiSuDung().getId()));
    	}
    	
    	
    	nhomQuyTrinhList = nhomQuyTrinhService.getNhomQuyTrinhFind(null, null, trangThai, null, -1, -1);
    	htDonViWM = new WorkflowManagement<HtDonVi>(EChucNang.HT_DON_VI.getMa());
    }

    @Override
    public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		fieldGroup = new FieldGroup();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				if(Constants.ACTION_ADD.equals(action)){
					modelInfoCurrent = new ModelInfo();
					modelerDetail(modelInfoCurrent);
					initDetailForm();
				}else{
					String id = m.group(3);
					List<Model> modelList = repositoryService.createModelQuery().modelId(id).list();
					if(modelList != null && !modelList.isEmpty()){
						modelCurrent = modelList.get(0);
						modelInfoCurrent = new ModelInfo();
						modelInfoCurrent.setId(id);
						ObjectMapper objectMapper = new ObjectMapper();
				        
						try {
							JsonNode modelObjectNode = objectMapper.readTree(modelCurrent.getMetaInfo());
							if(modelObjectNode != null){
								if(modelObjectNode.get("description") != null){
									String des = modelObjectNode.get("description").textValue();
									modelInfoCurrent.setMoTa(des);
								}
								if(modelObjectNode.get("rules") != null){
									String rules = modelObjectNode.get("rules").textValue();
									modelInfoCurrent.setRules(rules);
								}
								
								if(modelObjectNode.get("category") != null){
									String categoryStr = modelObjectNode.get("category").textValue();
									List<NhomQuyTrinh> nhomQTList = nhomQuyTrinhService.findByCode(categoryStr);
									if(nhomQTList != null && !nhomQTList.isEmpty()){
										modelInfoCurrent.setNhom(nhomQTList.get(0));
									}
								}

								if(modelObjectNode.get("color") != null){
									int rgb = modelObjectNode.get("color").intValue();
									appColor = new Color(rgb);
									modelInfoCurrent.setColor(rgb);
								}else{
									appColor = new Color(255,255,255);
								}
								if(modelObjectNode.get("icon") != null){
									int icon = modelObjectNode.get("icon").intValue();
									modelInfoCurrent.setIcon(icon);
								}
								
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}

						modelInfoCurrent.setTen(modelCurrent.getName());
						
						modelerDetail(modelInfoCurrent);
						initDetailForm();
					}
				}
				
			}
		}

    }
    
    public void initDetailForm(){
    	
    	formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	FormLayout form = new FormLayout();
    	form.setMargin(true);
    	
    	TextField txtTen = new TextField("Tên quy trình");
    	txtTen.focus();
    	txtTen.setRequired(true);
    	txtTen.setSizeFull();
    	txtTen.setNullRepresentation("");
    	txtTen.setRequiredError("Không được để trống");
    	txtTen.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", 1, 2000, false));
    	
    	
    	TextArea txtDesc = new TextArea("Mô tả");
    	txtDesc.setSizeFull();
    	
    	txtDesc.setNullRepresentation("");
    	
    	BeanItemContainer<NhomQuyTrinh> container = new BeanItemContainer<NhomQuyTrinh>(NhomQuyTrinh.class, nhomQuyTrinhList);
    	ComboBox cbbCategory = new ComboBox("Ứng dụng", container);
    	cbbCategory.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbCategory.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbCategory.setItemCaptionPropertyId("ten");
    	
    	
    	
    	ComboBox cbbIcon = new ComboBox("Icon");
    	
    	for (FontAwesome item : FontAwesome.values()) {
			Integer id = new Integer(item.getCodepoint());
			cbbIcon.addItem(id);
			cbbIcon.setItemCaption(id, item.name());
			cbbIcon.setItemIcon(id, item);
		}
    	cbbIcon.setWidth(WIDTH_FULL, Unit.PERCENTAGE);	
    	
    	ColorPicker colorPicker = new ColorPicker("Màu");
    	colorPicker.setSwatchesVisibility(false);
    	colorPicker.setHistoryVisibility(false);
    	colorPicker.setTextfieldVisibility(false);
    	colorPicker.setHSVVisibility(false);
    	if(appColor != null){
        	colorPicker.setColor(appColor);
    	}
    	colorPicker.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action) || Constants.ACTION_DEPLOY.equals(action));
    	
    	colorPicker.addColorChangeListener(new ColorChangeListener() {
            @Override
            public void colorChanged(final ColorChangeEvent event) {
            	appColor = event.getColor();
            }
        });
    	
    	TextArea txtRules = new TextArea("Business rules");
    	txtRules.setSizeFull();
    	
    	txtRules.setNullRepresentation("");
    	
    	fieldGroup.bind(txtTen, "ten");
    	
    	fieldGroup.bind(txtDesc, "moTa");
    	fieldGroup.bind(cbbCategory, "nhom");
    	fieldGroup.bind(cbbIcon, "icon");
    	fieldGroup.bind(txtRules, "rules");
    	cbbIcon.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action) || Constants.ACTION_DEPLOY.equals(action));
    	txtTen.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action) || Constants.ACTION_DEPLOY.equals(action));
    	txtDesc.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action) || Constants.ACTION_DEPLOY.equals(action));
    	txtRules.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action) || Constants.ACTION_DEPLOY.equals(action));
    	cbbCategory.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action) || Constants.ACTION_DEPLOY.equals(action));
    	
    	form.addComponent(txtTen);
    	form.addComponent(txtDesc);
    	form.addComponent(cbbCategory);
    	form.addComponent(cbbIcon);
    	form.addComponent(colorPicker);
    	form.addComponent(txtRules);
    	
    	if(Constants.ACTION_DEPLOY.equals(action)){
    		initDonViTreeTable();
        	form.addComponent(donViTreeTable);
    	}
    	
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	if(!Constants.ACTION_VIEW.equals(action)){
    		btnUpdate = new Button();
    		btnUpdate.addClickListener(this);
    		buttons.addComponent(btnUpdate);
    		if(Constants.ACTION_EDIT.equals(action)){
        		btnUpdate.setCaption("Cập nhật");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_EDIT_ID);
        	}else if(Constants.ACTION_ADD.equals(action)){
        		btnUpdate.setId(BUTTON_ADD_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption("Thêm mới");
        	}else if(Constants.ACTION_DEPLOY.equals(action)){
        		btnUpdate.setId(BUTTON_DEPLOY_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption("Deploy");
        	}else if(Constants.ACTION_DEL.equals(action)){
        		btnUpdate.setId(BUTTON_DEL_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
        		btnUpdate.setCaption("Xóa");
        	}
    	}
    	
    	btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
    	form.addComponent(buttons);

    	formContainer.addComponent(form);
    	addComponent(formContainer);
    }
    
    public void initDonViTreeTable(){
    	donViTreeTable = new TreeTable("Đơn vị được áp dụng");
    	donViTreeTable.setWidth("100%");
    	donViTreeTable.addContainerProperty("ma", CheckBox.class, null);
    	donViTreeTable.addContainerProperty("ten", String.class, null);
    	donViTreeTable.setColumnHeader("ma", "Mã");
    	donViTreeTable.setColumnHeader("ten", "Tên");
		
    	donViTreeTable.setVisibleColumns("ma", "ten");
    	
    	List<Long> trangThai=new ArrayList();
		trangThai.add(new Long(htDonViWM.getTrangThaiSuDung().getId()));
		List<HtDonVi> htDonViList = htDonViServices.getHtDonViFind(null,null, null, trangThai, -1, -1);
		for(HtDonVi donVi:htDonViList){
			Boolean check=donViSelected.get(donVi.getId());
			CheckBox checkBox =new CheckBox();
			checkBox.setCaption(donVi.getMa());
			checkBox.setValue(check!=null&&check);
			
			checkBox.addValueChangeListener(new ValueChangeListener(){
				@Override
				public void valueChange(ValueChangeEvent event) {
					Boolean check=(Boolean) event.getProperty().getValue();
					if(check==null){
						check = false;
					}
					donViSelected.put(donVi.getId(),check);
					Collection<HtDonVi> childList = (Collection<HtDonVi>) donViTreeTable.getChildren(donVi);
					if(childList != null){
						for (HtDonVi htDonVi : childList) {
							if(htDonVi != null){
								CheckBox chk = donViCheckboxMap.get(htDonVi.getId());
								if(chk != null){
									chk.setValue(check);
								}
							}
							
						}
					}
				}
			});
			donViTreeTable.addItem(new Object[]{checkBox, donVi.getTen()},donVi);
			donViTreeTable.setChildrenAllowed(donVi, false);
			donViCheckboxMap.put(donVi.getId(), checkBox);
			
		}
		for(HtDonVi donVi:htDonViList){
			if(donVi.getHtDonVi()!=null){
				donViTreeTable.setChildrenAllowed(donVi.getHtDonVi(), true);
				donViTreeTable.setParent(donVi, donVi.getHtDonVi());
			}
		}
    }
    
   private void modelerDetail(ModelInfo modelInfo) {
        if (modelInfo == null) {
        	modelInfo = new ModelInfo();
        }
        BeanItem<ModelInfo> item = new BeanItem<ModelInfo>(modelInfo);
        fieldGroup.setItemDataSource(item);
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSourceAccessor.getMessage(Messages.UI_VIEW_HOME));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(ModelerListView.MAIN_TITLE);
		bcItem.setViewName(ModelerListView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BUTTON_CANCEL_ID.equals(button.getId())){
				UI.getCurrent().getNavigator().navigateTo(ModelerListView.VIEW_NAME);
			}else{
				if(Constants.ACTION_ADD.equals(action)){
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						
						ModelInfo modelInfo = ((BeanItem<ModelInfo>) fieldGroup.getItemDataSource()).getBean();

				        NhomQuyTrinh category =modelInfo.getNhom();
						
						ObjectMapper objectMapper = new ObjectMapper();
				        ObjectNode editorNode = objectMapper.createObjectNode();
				        editorNode.put("id", "canvas");
				        editorNode.put("resourceId", "canvas");
				        ObjectNode stencilSetNode = objectMapper.createObjectNode();
				        stencilSetNode.put("namespace", "http://b3mn.org/stencilset/bpmn2.0#");
				        editorNode.put("stencilset", stencilSetNode);
				        Model modelData = repositoryService.newModel();
				        
				        ObjectNode modelObjectNode = objectMapper.createObjectNode();
				        modelObjectNode.put("name", modelInfo.getTen());
				        modelObjectNode.put("version", 1);
				        if(modelInfo.getMoTa()!=null && !modelInfo.getMoTa().equals("")) {
				        	modelObjectNode.put("description", modelInfo.getMoTa());
				        }else {
				        	modelObjectNode.put("description", modelInfo.getTen());
				        }
				        
				        modelObjectNode.put("rules", modelInfo.getRules());
				        if(appColor != null){
				        	 modelObjectNode.put("color", appColor.getRGB());
				        }
				        modelObjectNode.put("icon", modelInfo.getIcon());
				        if(category != null){
					        modelObjectNode.put("category", category.getMa());
				        }
				        modelData.setMetaInfo(modelObjectNode.toString());
				        modelData.setName(modelInfo.getTen());
				        
				        if(category != null){
					        modelData.setCategory(category.getMa());
				        }
				        
				        repositoryService.saveModel(modelData);
				        
						repositoryService.addModelEditorSource(modelData.getId(), editorNode.toString().getBytes("utf-8"));
						
						String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
						UI.getCurrent().getPage().setLocation(contextPath+"/modeler.html?modelId="+modelData.getId());
					
						
					}
				}else if(Constants.ACTION_DEL.equals(action)){
					if(modelInfoCurrent != null){
						repositoryService.deleteModel(modelInfoCurrent.getId());
						Notification notf = new Notification("Thông báo", "Xóa thành công.");
						notf.setDelayMsec(3000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
						UI.getCurrent().getNavigator().navigateTo(ModelerListView.VIEW_NAME);
					}
				}else if(Constants.ACTION_EDIT.equals(action)){
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						
						ModelInfo modelInfo = ((BeanItem<ModelInfo>) fieldGroup.getItemDataSource()).getBean();

				        String modelName=modelInfo.getTen();
				        String description = "";
				        if(modelInfo.getMoTa()!=null && !modelInfo.getMoTa().equals("")) {
				        	description = modelInfo.getMoTa();
				        }else {
				        	description = modelInfo.getTen();
				        }
				        
				        String rules =modelInfo.getRules();
				        NhomQuyTrinh category =modelInfo.getNhom();
						
						ObjectMapper objectMapper = new ObjectMapper();
				        ObjectNode editorNode = objectMapper.createObjectNode();
				        editorNode.put("id", "canvas");
				        editorNode.put("resourceId", "canvas");
				        ObjectNode stencilSetNode = objectMapper.createObjectNode();
				        stencilSetNode.put("namespace", "http://b3mn.org/stencilset/bpmn2.0#");
				        editorNode.put("stencilset", stencilSetNode);
				        
				        ObjectNode modelObjectNode = objectMapper.createObjectNode();
				        modelObjectNode.put("name", (String) modelName);
				        modelObjectNode.put("version", 1);
				        modelObjectNode.put("description", description);
				        modelObjectNode.put("rules", rules);
				        if(appColor != null){
				        	 modelObjectNode.put("color", appColor.getRGB());
				        }
				       
				        modelObjectNode.put("icon", modelInfo.getIcon());
				        if(category != null){
					        modelObjectNode.put("category", category.getMa());
				        }

				        modelCurrent.setMetaInfo(modelObjectNode.toString());
				        modelCurrent.setName(modelName);
				        if(category != null){
				        	modelCurrent.setCategory(category.getMa());
				        }
				        
				        repositoryService.saveModel(modelCurrent);
				        
						Notification notf = new Notification("Thông báo", "Cập nhật thành công");
						notf.setDelayMsec(3000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
						UI.getCurrent().getNavigator().navigateTo(ModelerListView.VIEW_NAME);
					}
				}else if(Constants.ACTION_DEPLOY.equals(action)){
					if(modelInfoCurrent != null){
						try {
							final ObjectNode modelNode = (ObjectNode) new ObjectMapper().readTree(repositoryService.getModelEditorSource(modelCurrent.getId()));
							
							Deployment deployment = deployModelerModel(modelNode, modelCurrent.getCategory(), modelInfoCurrent.getRules());
							

								List<DeploymentHtDonVi> deleteList=new ArrayList();
								
								List<DeploymentHtDonVi> updateList=new ArrayList<>();

								
								if(donViSelected != null && !donViSelected.isEmpty()){
									for(Map.Entry<Long, Boolean> entry : donViSelected.entrySet()){
										Boolean b = entry.getValue();
										Long id = entry.getKey();
										if(id != null && b != null && b){
											DeploymentHtDonVi modelerHtDonVi=new DeploymentHtDonVi();
											HtDonVi donVi = htDonViServices.findOne(id);
											modelerHtDonVi.setHtDonVi(donVi);
											modelerHtDonVi.setDeploymentId(deployment.getId());
											updateList.add(modelerHtDonVi);
										}
									}
								}
								BpmnModel model = new BpmnJsonConverterCustom().convertToBpmnModel(modelNode);
								List<ProcessTaskProperty> taskProperties = new ArrayList();
								if(model != null && model.getProcesses() != null){
									for (org.activiti.bpmn.model.Process process : model.getProcesses()) {
										if(process.getFlowElements() != null){
											Collection<FlowElement> elements = process.getFlowElements();
											List<FlowElement> listElements = new ArrayList(elements);
											while(!listElements.isEmpty()){
												FlowElement flowElement = listElements.remove(0);
												if(flowElement instanceof UserTask){
													UserTask userTask = (UserTask) flowElement;
													if(userTask.getCustomProperties() != null 
															&& !userTask.getCustomProperties().isEmpty()){
														for(CustomProperty property : userTask.getCustomProperties()){
															ProcessTaskProperty p = new ProcessTaskProperty();
															p.setDeploymentId(deployment.getId());
															p.setPropertyKey(property.getId());
															p.setTaskDefinitionId(userTask.getId());
															p.setPropertyValue(property.getSimpleValue());
															taskProperties.add(p);
														}
													}
												}else if(flowElement instanceof SubProcess){
													SubProcess element = (SubProcess) flowElement;
													Collection<FlowElement> subProcessElements = element.getFlowElements();
													List<FlowElement> subProcessListElement = new ArrayList(subProcessElements);
													listElements.addAll(subProcessListElement);
												}
											}
										}
									}
								}
								
								deploymentHtDonViService.update(updateList, deleteList, taskProperties);
								
//								try {
									ProcessDefinition proDef = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).singleResult();
									
//									if(proDef != null){
//										SolrUtils.updateSolr(CommonUtils.ESolrSearchType.PROCESS.getCode()+"_"+proDef.getId(), proDef.getName() +" "+proDef.getDescription(), solrUrl);
//									}
//								} catch (Exception e) {
//									e.printStackTrace();
//								}
								
							Notification notf = new Notification("Thông báo", "Deploy thành công.");
							notf.setDelayMsec(3000);
							notf.setPosition(Position.TOP_CENTER);
							notf.show(Page.getCurrent());
								
							UI.getCurrent().getNavigator().navigateTo(ModelerListView.VIEW_NAME);
							
						} catch (Exception e) {
							Notification notf = new Notification("Thông báo", e.getMessage(), Notification.Type.WARNING_MESSAGE);
							notf.setDelayMsec(20000);
							notf.setPosition(Position.TOP_CENTER);
							notf.show(Page.getCurrent());
							UI.getCurrent().getNavigator().navigateTo(ModelerListView.VIEW_NAME);
							e.printStackTrace();
						}
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra!", Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
			UI.getCurrent().getNavigator().navigateTo(ModelerListView.VIEW_NAME);
			
		}
        
	}
	
	protected Deployment deployModelerModel(final ObjectNode modelNode, String category, String rules) throws Exception{
		BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
		byte[] bpmnBytes = new BpmnXMLConverter().convertToXML(model);
		    ObjectNode objNode = new BpmnJsonConverterCustom().convertToJson(model);
		    
		    String processName = modelCurrent.getName() + ".bpmn20.xml";
		    DeploymentBuilder deploymentBuilder = repositoryService.createDeployment()
		            .name(modelCurrent.getName()).category(category)
		            .addString(processName, new String(bpmnBytes));

		    if(rules != null && !rules.isEmpty()){
			    String rulesName = modelCurrent.getName() + ".drl";
			    InputStream stream = new ByteArrayInputStream(rules.getBytes(StandardCharsets.UTF_8));
		    	deploymentBuilder.addInputStream(rulesName, stream);
		    }
		            
		    Deployment deployment =deploymentBuilder.deploy();
		    return deployment;
		
	  }
	
}
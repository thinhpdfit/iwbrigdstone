package com.eform.ui.view.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricIdentityLink;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Attachment;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.IdentityLinkType;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskInfo;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Broadcaster;
import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.ThamSo;
import com.eform.common.type.BroadcastType;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.type.EFormTruongThongTin;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.FileUtils;
import com.eform.common.utils.FormUtils;
import com.eform.common.utils.ProcessInstanceUtils;
import com.eform.common.utils.SolrUtils;
import com.eform.common.workflow.ITrangThai;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.HoSo;
import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.ProcessTaskProperty;
import com.eform.service.DanhMucService;
import com.eform.service.HoSoService;
import com.eform.service.HtThamSoService;
import com.eform.service.LoaiDanhMucService;
import com.eform.service.ProcessTaskPropertyService;
import com.eform.ui.custom.ButtonAction;
import com.eform.ui.custom.ConfirmDialog;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.UploadAttachmentInfo;
import com.eform.ui.custom.UploadReceive;
import com.eform.ui.form.FormGenerate;
import com.eform.ui.view.dashboard.DashboardView;
import com.eform.ui.view.process.MyProcessInstanceDetailView;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = TaskDetailView.VIEW_NAME)
public class TaskDetailView extends VerticalLayout implements View, ClickListener {

	public static final String VIEW_NAME = "task-detail";
	private static String MAIN_TITLE = "Task Detail";
	private static String SMALL_TITLE = "";

	private static final String BTN_HISTORY_ID = "btnHistory";
	private static final String BTN_TASK_ENVENT_ID = "btnTaskEvent";
	private static final String BTN_REMOVE_RELATED_ID = "btnRemoveUser";
	private static final String BUTTON_COMPLETE_ID = "btnComplete";
	private static final String BUTTON_SAVE_ID = "btnSave";
	private static final String BTN_ADD_RELATED_ID = "btnAddRelated";
	private static final String BTN_DEL_COMMENT_ID = "btnDelComment";
	private static final String BTN_CREATE_RELATED_ID = "btnCreateRelated";
	private static final String BTN_ADD_RELATED_USER_ID = "btnAddRelatedUser";
	private static final String BTN_REASSIGN_ID = "btnReAssign";
	private static final String BTN_TRANFER_ID = "btnTranfer";
	private static final String BUTTON_UPDATE_INVOL_USER_ID = "btnUpdateInvol";
	private static final String BUTTON_UPDATE_USER_ID = "btnUpdateMainUser";
	private static final String BUTTON_ADD_NEWTASK_ID = "btnAddNewTask";
	private static final String BTN_CLAIM_ID = "btnClaim";
	private static final String BTN_ADDNEW_ID = "BTN_ADDNEW_ID";

	@Autowired
	protected LoaiDanhMucService loaiDanhMucService;
	@Autowired
	protected DanhMucService danhMucService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private HtThamSoService htThamSoService;
	@Autowired
	private HoSoService hoSoService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private IdentityService identityService;
	@Autowired
	protected transient FormService formService;
	@Autowired
	protected HistoryService historyService;
	@Autowired
	protected RepositoryService repositoryService;
	@Autowired
	protected ProcessTaskPropertyService processTaskPropertyService;

	private String uploadPath;
	private TaskDetailView currentView;
	private TaskInfo taskCurrent;
	private String action;
	private FormGenerate formInfo;
	private FormGenerate formGenerate;
	private HoSo hoSoCurrent;
	private CssLayout commentContainer;
	private CssLayout attContainer;

	private Window addRelatedWindow;
	private final PropertysetItem relatedItem = new PropertysetItem();
	private final FieldGroup relatedFeildGroup = new FieldGroup();

	private ConfirmDialog removeUserDialog;
	private ConfirmDialog removeCommentDialog;
	
	private CssLayout relatedUserLayout;
	private Label lblRelatedUser;
	private Label seeMoreUser;

	private User removeUser;
	private String removeType;

	private Window taskEventWindow;
	private boolean taskEventLoaded;
	private boolean relatedLoaded;
	private boolean relatedUserLoaded;
	private boolean selectUserLoaded;

	private Grid gridUser;
	private Grid gridContributorUser;
	private Grid gridImplementerUser;
	private Grid gridManagerUser;
	private Grid gridSponsorUser;

	private Window selectUserWindow;
	private Window involUserWindow;

	private Label lblAssignee;
	private Label lblOwner;
	private boolean isHistoric = false;
	private Label commentCount;
	private int commentCountNum;

	private String solrUrl;
	private Label attCount;
	private int attCountNum;
	private CssLayout claimWrap;
	private Button btnClaim;
	private String supserParentCurrentId;

	private CssLayout mainContainer;
	private BeanItemContainer<HistoricTaskInstance> subtaskContainer;
	private String currentPath;
	private Integer currentThuTu;
	private Integer parentLoopCounter;
	private String dataLoopPath;
	private PageBreadcrumbLayout pageBreadcrumbLayout;
	
	private String commentId;
	private Component componentScrollToView;
	private String processInsId;
	

	private Window newTaskWindow;
	private PropertysetItem newTaskItem;
	private FieldGroup newTaskFeildGroup;

	@PostConstruct
	void init() {
		MAIN_TITLE = messageSource.getMessage(Messages.UI_VIEW_TASK_DETAIL_TITLE, null,
				VaadinSession.getCurrent().getLocale());
		SMALL_TITLE = messageSource.getMessage(Messages.UI_VIEW_TASK_DETAIL_TITLE_SMALL, null,
				VaadinSession.getCurrent().getLocale());

		uploadPath = "";
		HtThamSo uploadPathParam = htThamSoService.getHtThamSoFind(ThamSo.FOLDER_UPLOAD_PATH);
		if (uploadPathParam != null) {
			uploadPath = uploadPathParam.getGiaTri();
		}
		removeUserDialog = new ConfirmDialog("Xóa người dùng", "Bạn chắc chắn muốn xóa?", this, "BTN_DELELE_USER_OK", "BTN_DELELE_USER_CANCEL");
		removeCommentDialog = new ConfirmDialog("Xóa bình luận", "Bạn chắc chắn muốn xóa?", this, "BTN_DELELE_CMT_OK", "BTN_DELELE_CMT_CANCEL");
		relatedFeildGroup.setItemDataSource(relatedItem);

		initTaskEventWindow();
		
		taskEventLoaded = false;
		relatedLoaded = false;
		relatedUserLoaded = false;
		currentView = this;

		HtThamSo solrUrlThamSo = htThamSoService.getHtThamSoFind(ThamSo.SOLRSEARCH_URL);
		if (solrUrlThamSo != null && solrUrlThamSo.getGiaTri() != null) {
			solrUrl = solrUrlThamSo.getGiaTri();
		}
		initBreakCrumb();
		mainContainer = new CssLayout();
		mainContainer.setWidth("100%");
		mainContainer.addStyleName(ExplorerLayout.TASK_DETAIL_MAIN_CONTAINER);
		
		addComponent(mainContainer);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		mainContainer.removeAllComponents();
		pageBreadcrumbLayout.clear();
		
		PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		pageBreadcrumbLayout.addBreadcrumbItem(home);
		
		if (params != null) {
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)/([a-zA-z0-9-_:]+)(/cmt-([0-9]+))*");
			Matcher m = p.matcher(params.toString());
			if (m.matches()) {
				action = m.group(1);
				commentId = m.group(4);
				
				PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
				if(Constants.ACTION_VIEW_HISTORY.equals(action)){
					bcItem.setTitle(ArchivedView.MAIN_TITLE);
					bcItem.setViewName(ArchivedView.VIEW_NAME);
				}else{
					bcItem.setTitle(InboxView.MAIN_TITLE);
					bcItem.setViewName(InboxView.VIEW_NAME);
				}
				pageBreadcrumbLayout.addBreadcrumbItem(bcItem);

				PageBreadcrumbInfo current = new PageBreadcrumbInfo();
				current.setTitle(MAIN_TITLE);
				current.setViewName(VIEW_NAME);
				pageBreadcrumbLayout.addBreadcrumbItem(current);
				
				String id = m.group(2);
				if (Constants.ACTION_VIEW_HISTORY.equals(action)) {
					HistoricTaskInstance historicTask = historyService.createHistoricTaskInstanceQuery().taskId(id)
							.singleResult();
					taskCurrent = historicTask;
					isHistoric = true;
				} else {
					Task task = taskService.createTaskQuery().taskId(id).singleResult();
					taskCurrent = task;
				}

				if (taskCurrent != null) {
					List<HoSo> hoSoList = null;
					processInsId = taskCurrent.getProcessInstanceId();
					if(processInsId == null && taskCurrent.getParentTaskId() != null){
						TaskInfo superParent = ProcessInstanceUtils.getParentTaskIdInProcess(taskCurrent.getParentTaskId());
						if(superParent != null){
							processInsId = superParent.getProcessInstanceId();
						}
					}
					if(processInsId != null){
						String processParentId = ProcessInstanceUtils.getParentInstance(processInsId, historyService);
						if (processParentId != null) {
							supserParentCurrentId = processParentId;
							hoSoList = hoSoService.getHoSoFind(null, null, null, processParentId, 0, 1);
						} else {
							supserParentCurrentId = taskCurrent.getProcessInstanceId();
							hoSoList = hoSoService.getHoSoFind(null, null, null, processInsId, 0, 1);
						}

						if (hoSoList != null && !hoSoList.isEmpty()) {
							hoSoCurrent = hoSoList.get(0);
						}
					}
					
					try {
						//Map map = runtimeService.getVariables(taskCurrent.getExecutionId());
						if(!Constants.ACTION_VIEW_HISTORY.equals(action)){
							String executionId = taskCurrent.getExecutionId();
							if(executionId != null){
								Object elementVarObj = runtimeService.getVariable(executionId, "element_var");
								if(elementVarObj != null && elementVarObj instanceof String){
									String pathTmp = (String) elementVarObj;
									Pattern pa = Pattern.compile("(.*)#([0-9]+)");
									Matcher ma = pa.matcher(pathTmp);
							    	if(ma.matches()){
							    		currentPath = ma.group(1);
										currentThuTu = new Integer(ma.group(2));
							    	}
								}
								
								Object parentLoopCounterObj =  runtimeService.getVariable(executionId, "parentLoopCounter");
								if(parentLoopCounterObj != null && parentLoopCounterObj instanceof Integer){
									parentLoopCounter = (Integer) parentLoopCounterObj;
								}
								
								Object dataLoopPathObj =  runtimeService.getVariable(executionId, "dataLoopPath");
								if(dataLoopPathObj != null && dataLoopPathObj instanceof String){
									dataLoopPath = (String) dataLoopPathObj;
								}
							}
							
							initNewTaskWindow();
							Button btnCreateSubtask = new Button();
							btnCreateSubtask = new Button();
							btnCreateSubtask.setIcon(FontAwesome.PLUS);
							btnCreateSubtask.addClickListener(this);
							btnCreateSubtask.addStyleName(Reindeer.BUTTON_LINK);
							btnCreateSubtask.setDescription("Tạo subtask");
							btnCreateSubtask.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
							btnCreateSubtask.addStyleName(ExplorerLayout.BUTTON_ADDNEW_SUBTASK);
							btnCreateSubtask.setId(BTN_ADDNEW_ID);
							addComponent(btnCreateSubtask);
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					initClaimWrap();
					initMainTitle();
					initUserWrap();
					initFormWrap();
					initSubtaskContainer();
					initInteractiveWrap();
					initNavigationWrap();
					if(componentScrollToView != null){
						UI.getCurrent().scrollIntoView(componentScrollToView);
						com.vaadin.ui.JavaScript.getCurrent().execute("highligh('cmt-"+commentId+"')");
					}
				}

			}
		}
	}

	public void initTaskEventWindow() {
		taskEventWindow = new Window("Lịch sử");
		taskEventWindow.setWidth(700, Unit.PIXELS);
		taskEventWindow.setHeight(600, Unit.PIXELS);
		taskEventWindow.center();
		taskEventWindow.setModal(true);
	}

	public void initTaskEventContent() {
		CssLayout content = new CssLayout();
		content.setWidth("100%");
		content.addStyleName(ExplorerLayout.TASK_EVENT_POPUP);

		List<org.activiti.engine.task.Event> taskEvents = taskService.getTaskEvents(taskCurrent.getId());
		if (taskEvents != null && !taskEvents.isEmpty()) {
			for (org.activiti.engine.task.Event event : taskEvents) {
				CssLayout item = initTaskEventItem(event);
				if (item != null) {
					content.addComponent(item);
				}
			}
		}
		taskEventWindow.setContent(content);
	}

	public CssLayout initTaskEventItem(org.activiti.engine.task.Event event) {
		if (event.getUserId() != null && event.getProcessInstanceId() == null) {
			CssLayout item = new CssLayout();
			item.setWidth("100%");
			item.addStyleName(ExplorerLayout.TASK_EVENT_POPUP_ITEM);
			User user = identityService.createUserQuery().userId(event.getUserId()).singleResult();
			SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);

			String username = user.getFirstName() + " " + user.getLastName();

			boolean add = true;

			String content = "<span class='task-event-u-name'>" + username + "</span> ";
			if (org.activiti.engine.task.Event.ACTION_ADD_USER_LINK.equals(event.getAction())) {
				User involvedUser = identityService.createUserQuery().userId(event.getMessageParts().get(0))
						.singleResult();
				content += "Involved " + "<span class='task-event-highlight-u'>" + involvedUser.getFirstName() + " "
						+ involvedUser.getLastName() + "</span> as <span class='task-event-highlight'>"
						+ event.getMessageParts().get(1) + "</span>";
			} else if (org.activiti.engine.task.Event.ACTION_DELETE_USER_LINK.equals(event.getAction())) {
				User involvedUser = identityService.createUserQuery().userId(event.getMessageParts().get(0))
						.singleResult();
				content += "Removed " + "<span class='task-event-highlight-u'>" + involvedUser.getFirstName() + " "
						+ involvedUser.getLastName() + "</span>" + " as " + "<span class='task-event-highlight'>"
						+ event.getMessageParts().get(1) + "</span>";
				add = false;
			} else if (org.activiti.engine.task.Event.ACTION_ADD_GROUP_LINK.equals(event.getAction())) {
				content += "Involved group " + "<span class='task-event-highlight-g'>" + event.getMessageParts().get(0)
						+ "</span>" + " as " + "<span class='task-event-highlight'>" + event.getMessageParts().get(1)
						+ "</span>";
			} else if (org.activiti.engine.task.Event.ACTION_DELETE_GROUP_LINK.equals(event.getAction())) {
				content += "Removed group " + "<span class='task-event-highlight-g'>" + event.getMessageParts().get(0)
						+ "</span>" + " as " + "<span>" + event.getMessageParts().get(1) + "</span>";
				add = false;
			}

			Label icon = new Label();
			icon.addStyleName(ExplorerLayout.TASK_EVENT_POPUP_ITEM_ICON);
			if (add) {
				icon.addStyleName(ExplorerLayout.TASK_EVENT_POPUP_ITEM_ICON_ADD);
			} else {
				icon.addStyleName(ExplorerLayout.TASK_EVENT_POPUP_ITEM_ICON_REMOVE);
			}

			item.addComponent(icon);

			Label lblContent = new Label(content, ContentMode.HTML);
			lblContent.addStyleName(ExplorerLayout.TASK_EVENT_POPUP_ITEM_CONTENT);
			item.addComponent(lblContent);

			if (event.getTime() != null) {
				Label time = new Label(df.format(event.getTime()));
				time.addStyleName(ExplorerLayout.TASK_EVENT_POPUP_ITEM_TIME);
				item.addComponent(time);
			}

			return item;
		}

		return null;
	}
	
	public void initNewTaskWindow() {

		newTaskItem = new PropertysetItem();
		newTaskFeildGroup = new FieldGroup();
		newTaskFeildGroup.setItemDataSource(newTaskItem);
		
		
		VerticalLayout windowContent = new VerticalLayout();
		windowContent.setMargin(true);
		windowContent.setSpacing(true);
		windowContent.setWidth("100%");
		
		FormLayout form = new FormLayout();
		form.setWidth("100%");
		
		TextField name = new TextField("Tên công việc");
		name.addStyleName(ExplorerLayout.FORM_CONTROL);
		name.setWidth("100%");
		form.addComponent(name);
		
		TextArea desc = new TextArea("Mô tả");
		desc.addStyleName(ExplorerLayout.FORM_CONTROL);
		desc.setWidth("100%");
		form.addComponent(desc);
		
		ComboBox assign = new ComboBox("Người thực hiện");
		assign.addStyleName(ExplorerLayout.FORM_CONTROL);
		assign.setWidth("100%");
		form.addComponent(assign);
		List<User> userList = identityService.createUserQuery().list();
		for (User u : userList) {
			assign.addItem(u.getId());
			assign.setItemCaption(u.getId(), u.getFirstName() + " " + u.getLastName() + " ("+u.getId()+")");
		}
		
		windowContent.addComponent(form);
		
		newTaskItem.addItemProperty("name", new ObjectProperty<String>("", String.class));
		newTaskItem.addItemProperty("description", new ObjectProperty<String>("", String.class));
		newTaskItem.addItemProperty("assign", new ObjectProperty<String>("", String.class));
		
		newTaskFeildGroup.bind(name, "name");
		newTaskFeildGroup.bind(desc, "description");
		newTaskFeildGroup.bind(assign, "assign");
		
		Button btnUpdateCell = new Button("Thêm task");
		btnUpdateCell.addClickListener(this);
		btnUpdateCell.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnUpdateCell.setId(BUTTON_ADD_NEWTASK_ID);
		
		CssLayout btnWrap = new CssLayout();
		btnWrap.setWidth("100%");
		btnWrap.addStyleName(ExplorerLayout.BTN_POPUP_GROUP_CENTER);
		btnWrap.addComponent(btnUpdateCell);
		windowContent.addComponent(btnWrap);
		
		newTaskWindow = new Window("Subtask");
		newTaskWindow.setWidth(700, Unit.PIXELS);
		newTaskWindow.center();
		newTaskWindow.setModal(true);
		newTaskWindow.setContent(windowContent);
	}
	public void initSelectUserWindow() {
		VerticalLayout windowContent = new VerticalLayout();
		windowContent.setMargin(true);
		windowContent.setSpacing(true);

		gridUser = new Grid();

		Button btnUpdateCell = new Button("Cập nhật");
		btnUpdateCell.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnUpdateCell.setId(BUTTON_UPDATE_USER_ID);

		List<User> userList = identityService.createUserQuery().list();
		BeanItemContainer<User> userContainer = new BeanItemContainer<User>(User.class, userList);

		gridUser.setWidth("100%");
		gridUser.setContainerDataSource(userContainer);
		gridUser.setSelectionMode(SelectionMode.SINGLE);
		gridUser.setColumnOrder("id", "firstName", "lastName");
		gridUser.setColumns("id", "firstName", "lastName");
		gridUser.getColumn("id").setHeaderCaption("Tên đăng nhập");
		gridUser.getColumn("firstName").setHeaderCaption("Họ đệm");
		gridUser.getColumn("lastName").setHeaderCaption("Tên");

		gridUser.addSelectionListener(selectionEvent -> {
			Object selected = gridUser.getSelectionModel().getSelectedRows();
			if (selected != null) {
				btnUpdateCell.setData(selected);
			}
		});
		
		HeaderRow filteringHeader = gridUser.appendHeaderRow();
		TextField filterId = getColumnFilter(gridUser, "id");
		TextField filterFn = getColumnFilter(gridUser, "firstName");
		TextField filterLn = getColumnFilter(gridUser, "lastName");
		filteringHeader.getCell("id").setComponent(filterId);
		filteringHeader.getCell("firstName").setComponent(filterFn);
		filteringHeader.getCell("lastName").setComponent(filterLn);

		btnUpdateCell.addClickListener(this);
		CssLayout btnWrap = new CssLayout();
		btnWrap.setWidth("100%");
		btnWrap.addStyleName(ExplorerLayout.BTN_POPUP_GROUP_CENTER);
		btnWrap.addComponent(btnUpdateCell);

		windowContent.addComponent(gridUser);
		windowContent.addComponent(btnWrap);

		selectUserWindow = new Window("Người dùng");
		selectUserWindow.setWidth(700, Unit.PIXELS);
		// selectUserWindow.setHeight(600, Unit.PIXELS);
		selectUserWindow.center();
		selectUserWindow.setModal(true);
		selectUserWindow.setContent(windowContent);
	}
	
	private TextField getColumnFilter(final Grid sample, final Object columnId) {
        TextField filter = new TextField();
        filter.setWidth("100%");
        filter.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = -458756607677822455L;
			SimpleStringFilter filter = null;

            @Override
            public void textChange(TextChangeEvent event) {
                Filterable f = (Filterable) sample.getContainerDataSource();
                if (filter != null) {
                    f.removeContainerFilter(filter);
                }
                filter = new SimpleStringFilter(columnId, event.getText(), true, true);
                f.addContainerFilter(filter);

                sample.cancelEditor();
            }
        });
        return filter;
    }

	public void initRelatedUserWindow() {
		List<User> userList = identityService.createUserQuery().list();

		VerticalLayout windowInvolContent = new VerticalLayout();
		windowInvolContent.setMargin(true);
		windowInvolContent.setSpacing(true);

		TabSheet tabSheetInvol = new TabSheet();
		tabSheetInvol.addStyleName(ExplorerLayout.TABSHEET);

		CssLayout tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);

		gridContributorUser = new Grid();

		tabContent.addComponent(gridContributorUser);
		tabSheetInvol.addTab(tabContent, "Contributor");

		Button btnUpdateUser = new Button("Cập nhật");
		Map<String, Object> map = new HashMap();
		btnUpdateUser.setData(map);
		btnUpdateUser.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnUpdateUser.setId(BUTTON_UPDATE_INVOL_USER_ID);

		gridContributorUser.setWidth("100%");
		gridContributorUser.setContainerDataSource(new BeanItemContainer<User>(User.class, userList));
		gridContributorUser.setSelectionMode(SelectionMode.MULTI);
		gridContributorUser.setColumnOrder("id", "firstName", "lastName");
		gridContributorUser.setColumns("id", "firstName", "lastName");
		gridContributorUser.getColumn("id").setHeaderCaption("Tên đăng nhập");
		gridContributorUser.getColumn("firstName").setHeaderCaption("Họ đệm");
		gridContributorUser.getColumn("lastName").setHeaderCaption("Tên");

		gridContributorUser.addSelectionListener(selectionEvent -> {
			Object selected = gridContributorUser.getSelectionModel().getSelectedRows();
			if (selected != null) {
				map.put("contributor", selected);
			}
		});
		
		HeaderRow filteringHeader = gridContributorUser.appendHeaderRow();
		TextField filterId = getColumnFilter(gridContributorUser, "id");
		TextField filterFn = getColumnFilter(gridContributorUser, "firstName");
		TextField filterLn = getColumnFilter(gridContributorUser, "lastName");
		filteringHeader.getCell("id").setComponent(filterId);
		filteringHeader.getCell("firstName").setComponent(filterFn);
		filteringHeader.getCell("lastName").setComponent(filterLn);

		tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);

		gridImplementerUser = new Grid();

		tabContent.addComponent(gridImplementerUser);
		tabSheetInvol.addTab(tabContent, "Implementer");

		gridImplementerUser.setWidth("100%");
		gridImplementerUser.setContainerDataSource(new BeanItemContainer<User>(User.class, userList));
		gridImplementerUser.setSelectionMode(SelectionMode.MULTI);
		gridImplementerUser.setColumnOrder("id", "firstName", "lastName");
		gridImplementerUser.setColumns("id", "firstName", "lastName");
		gridImplementerUser.getColumn("id").setHeaderCaption("Tên đăng nhập");
		gridImplementerUser.getColumn("firstName").setHeaderCaption("Họ đệm");
		gridImplementerUser.getColumn("lastName").setHeaderCaption("Tên");
		

		gridImplementerUser.addSelectionListener(selectionEvent -> {
			Object selected = gridImplementerUser.getSelectionModel().getSelectedRows();
			if (selected != null) {
				map.put("implementer", selected);
			}
		});
		HeaderRow filteringHeaderImpl = gridImplementerUser.appendHeaderRow();
		TextField filterIdImpl = getColumnFilter(gridImplementerUser, "id");
		TextField filterFnImpl = getColumnFilter(gridImplementerUser, "firstName");
		TextField filterLnImpl = getColumnFilter(gridImplementerUser, "lastName");
		filteringHeaderImpl.getCell("id").setComponent(filterIdImpl);
		filteringHeaderImpl.getCell("firstName").setComponent(filterFnImpl);
		filteringHeaderImpl.getCell("lastName").setComponent(filterLnImpl);

		tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);

		gridManagerUser = new Grid();

		tabContent.addComponent(gridManagerUser);
		tabSheetInvol.addTab(tabContent, "Manager");

		gridManagerUser.setWidth("100%");
		gridManagerUser.setContainerDataSource(new BeanItemContainer<User>(User.class, userList));
		gridManagerUser.setSelectionMode(SelectionMode.MULTI);
		gridManagerUser.setColumnOrder("id", "firstName", "lastName");
		gridManagerUser.setColumns("id", "firstName", "lastName");
		gridManagerUser.getColumn("id").setHeaderCaption("Tên đăng nhập");
		gridManagerUser.getColumn("firstName").setHeaderCaption("Họ đệm");
		gridManagerUser.getColumn("lastName").setHeaderCaption("Tên");

		gridManagerUser.addSelectionListener(selectionEvent -> {
			Object selected = gridManagerUser.getSelectionModel().getSelectedRows();
			if (selected != null) {
				map.put("manager", selected);
			}
		});
		HeaderRow filteringHeaderMn = gridManagerUser.appendHeaderRow();
		TextField filterIdMn = getColumnFilter(gridManagerUser, "id");
		TextField filterFnMn = getColumnFilter(gridManagerUser, "firstName");
		TextField filterLnMn = getColumnFilter(gridManagerUser, "lastName");
		filteringHeaderMn.getCell("id").setComponent(filterIdMn);
		filteringHeaderMn.getCell("firstName").setComponent(filterFnMn);
		filteringHeaderMn.getCell("lastName").setComponent(filterLnMn);

		tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);

		gridSponsorUser = new Grid();

		tabContent.addComponent(gridSponsorUser);
		tabSheetInvol.addTab(tabContent, "Sponsor");

		gridSponsorUser.setWidth("100%");
		gridSponsorUser.setContainerDataSource(new BeanItemContainer<User>(User.class, userList));
		gridSponsorUser.setSelectionMode(SelectionMode.MULTI);
		gridSponsorUser.setColumnOrder("id", "firstName", "lastName");
		gridSponsorUser.setColumns("id", "firstName", "lastName");
		gridSponsorUser.getColumn("id").setHeaderCaption("Tên đăng nhập");
		gridSponsorUser.getColumn("firstName").setHeaderCaption("Họ đệm");
		gridSponsorUser.getColumn("lastName").setHeaderCaption("Tên");

		gridSponsorUser.addSelectionListener(selectionEvent -> {
			Object selected = gridSponsorUser.getSelectionModel().getSelectedRows();
			if (selected != null) {
				map.put("sponsor", selected);
			}
		});
		
		HeaderRow filteringHeaderSs = gridSponsorUser.appendHeaderRow();
		TextField filterIdSs = getColumnFilter(gridSponsorUser, "id");
		TextField filterFnSs = getColumnFilter(gridSponsorUser, "firstName");
		TextField filterLnSs = getColumnFilter(gridSponsorUser, "lastName");
		filteringHeaderSs.getCell("id").setComponent(filterIdSs);
		filteringHeaderSs.getCell("firstName").setComponent(filterFnSs);
		filteringHeaderSs.getCell("lastName").setComponent(filterLnSs);

		btnUpdateUser.addClickListener(this);
		CssLayout btnUpdateWrap = new CssLayout();
		btnUpdateWrap.setWidth("100%");
		btnUpdateWrap.addStyleName(ExplorerLayout.BTN_POPUP_GROUP_CENTER);
		btnUpdateWrap.addComponent(btnUpdateUser);

		windowInvolContent.addComponent(tabSheetInvol);
		windowInvolContent.addComponent(btnUpdateWrap);

		involUserWindow = new Window("Người dùng");
		involUserWindow.setWidth(700, Unit.PIXELS);
		involUserWindow.setHeight(600, Unit.PIXELS);
		involUserWindow.center();
		involUserWindow.setModal(true);
		involUserWindow.setContent(windowInvolContent);
	}

	

	public void initBreakCrumb() {
		
		pageBreadcrumbLayout = new PageBreadcrumbLayout();
		
		addComponent(pageBreadcrumbLayout);
	}
	
	public void initClaimWrap(){
		claimWrap = new CssLayout();
		claimWrap.setWidth("100%");
		claimWrap.addStyleName(ExplorerLayout.TASK_CLAIM_WRAP);
		claimWrap.setVisible(false);
		mainContainer.addComponent(claimWrap);
		
		btnClaim = new Button("Nhận công việc này");
		btnClaim.setId(BTN_CLAIM_ID);
		btnClaim.addClickListener(this);
		btnClaim.setStyleName(ExplorerLayout.TASK_BTN_CLAIM);
		claimWrap.addComponent(btnClaim);
		
	}

	public void initMainTitle() {
		CssLayout wrap = new CssLayout();
		wrap.setWidth("100%");
		wrap.addStyleName(ExplorerLayout.TASK_DETAIL_HEADER_WRAP);
		mainContainer.addComponent(wrap);
		
		
		CssLayout priorityWrap = new CssLayout();
		priorityWrap.addStyleName(ExplorerLayout.TASK_DETAIL_PRIORITY_WRAP);
		wrap.addComponent(priorityWrap);

		CommonUtils.EPriority pri = CommonUtils.EPriority.getPriority(taskCurrent.getPriority());

		Label priority = new Label();
		priority.addStyleName(ExplorerLayout.TASK_DETAIL_PRIORITY_STAR);
		priority.setDescription("Mức độ ưu tiên " + pri.getName().toLowerCase());
		priority.addStyleName(pri.getStyleName());
		if (pri.equals(CommonUtils.EPriority.LOW)) {
			priority.setValue(FontAwesome.STAR_O.getHtml());
		} else {
			priority.setValue(FontAwesome.STAR.getHtml());
		}
		priority.setContentMode(ContentMode.HTML);
		priorityWrap.addComponent(priority);

		CssLayout taskNameWrap = new CssLayout();
		taskNameWrap.addStyleName(ExplorerLayout.TASK_DETAIL_NAME_WRAP);
		wrap.addComponent(taskNameWrap);

		String taskName = taskCurrent.getName();
		if(taskCurrent.getParentTaskId() != null){
			taskName += " (Subtask)";
		}
		Label lblTitle = new Label(taskName);
		lblTitle.setContentMode(ContentMode.HTML);
		lblTitle.addStyleName(ExplorerLayout.TASK_DETAIL_NAME);
		taskNameWrap.addComponent(lblTitle);

		CssLayout createTimeWrap = new CssLayout();
		createTimeWrap.addStyleName(ExplorerLayout.TASK_DETAIL_CREATE_TIME_WRAP);
		wrap.addComponent(createTimeWrap);

		SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
		Label createTime = new Label(df.format(taskCurrent.getCreateTime()));
		createTime.setContentMode(ContentMode.HTML);
		createTime.addStyleName(ExplorerLayout.TASK_DETAIL_CREATE_TIME);
		createTimeWrap.addComponent(createTime);
		createTime.setDescription("Ngày tạo");
		
		if(taskCurrent.getDueDate() != null){
			Label dueDateLbl = new Label(df.format(taskCurrent.getDueDate()));
			dueDateLbl.setContentMode(ContentMode.HTML);
			dueDateLbl.addStyleName(ExplorerLayout.TASK_DETAIL_DUEDATE);
			dueDateLbl.setDescription("Hạn thực hiện");
			createTimeWrap.addComponent(dueDateLbl);
		}

		Button btnHistory = new Button();
		btnHistory.setId(BTN_HISTORY_ID);
		btnHistory.setIcon(FontAwesome.HISTORY);
		btnHistory.setDescription("Lịch sử công việc");
		btnHistory.addStyleName(Reindeer.BUTTON_LINK);
		btnHistory.addClickListener(this);
		btnHistory.addStyleName(ExplorerLayout.TASK_DETAIL_HISTORY_BTN);
		createTimeWrap.addComponent(btnHistory);

		
	}

	public void initUserWrap() {
		try {
			CssLayout wrap = new CssLayout();
			wrap.setWidth("100%");
			wrap.addStyleName(ExplorerLayout.TASK_DETAIL_USER_WRAP);
			if (taskCurrent != null) {
				CssLayout mainUserWrap = new CssLayout();
				mainUserWrap.setWidth("100%");
				mainUserWrap.addStyleName(ExplorerLayout.TASK_DETAIL_USER_MAIN);
				wrap.addComponent(mainUserWrap);

				Label lblttl = new Label("Thực hiện");
				lblttl.addStyleName(ExplorerLayout.TASK_DETAIL_USER_ASSIGNEE_LBL);
				mainUserWrap.addComponent(lblttl);

				CssLayout assigneeWrap = new CssLayout();
				assigneeWrap.addStyleName(ExplorerLayout.TASK_DETAIL_USER_ASSIGNEE_WRAP);
				mainUserWrap.addComponent(assigneeWrap);
				lblAssignee = new Label();
				if (taskCurrent.getAssignee() != null) {
					User u = identityService.createUserQuery().userId(taskCurrent.getAssignee()).singleResult();
					lblAssignee = new Label(u.getFirstName() + " " + u.getLastName());
				}

				lblAssignee.addStyleName(ExplorerLayout.TASK_DETAIL_USER_ASSIGNEE);
				assigneeWrap.addComponent(lblAssignee);

				if(!Constants.ACTION_VIEW_HISTORY.equals(action)){
					Button reAssign = new Button();
					reAssign.setDescription("Assign");
					reAssign.addClickListener(this);
					reAssign.setIcon(FontAwesome.PENCIL);
					reAssign.setId(BTN_REASSIGN_ID);
					reAssign.addStyleName(ExplorerLayout.TASK_DETAIL_USER_ASSIGNEE_BTN);
					assigneeWrap.addComponent(reAssign);
				}
				
				Label lblOwnerttl = new Label("Owner");
				lblOwnerttl.addStyleName(ExplorerLayout.TASK_DETAIL_USER_OWNER_LBL);
				mainUserWrap.addComponent(lblOwnerttl);

				CssLayout ownerWrap = new CssLayout();
				ownerWrap.addStyleName(ExplorerLayout.TASK_DETAIL_USER_OWNER_WRAP);
				mainUserWrap.addComponent(ownerWrap);

				lblOwner = new Label();
				lblOwner.addStyleName(ExplorerLayout.TASK_DETAIL_USER_OWNER);
				if (taskCurrent.getOwner() != null) {
					User u = identityService.createUserQuery().userId(taskCurrent.getOwner()).singleResult();
					lblOwner.setValue(u.getFirstName() + " " + u.getLastName());
				}

				ownerWrap.addComponent(lblOwner);

				if(!Constants.ACTION_VIEW_HISTORY.equals(action)){
					Button tranfer = new Button();
					tranfer.setDescription("Tranfer");
					tranfer.addClickListener(this);
					tranfer.setIcon(FontAwesome.PENCIL);
					tranfer.setId(BTN_TRANFER_ID);
					tranfer.addStyleName(ExplorerLayout.TASK_DETAIL_USER_OWNER_BTN);
					ownerWrap.addComponent(tranfer);
				}

				CssLayout relatedLblWrap = new CssLayout();
				relatedLblWrap.setWidth("100%");
				relatedLblWrap.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_LBL_WRAP);
				wrap.addComponent(relatedLblWrap);

				lblRelatedUser = new Label();
				lblRelatedUser.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_LBL);
				relatedLblWrap.addComponent(lblRelatedUser);

				seeMoreUser = new Label();
				seeMoreUser.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_SEEMORE);
				relatedLblWrap.addComponent(seeMoreUser);
				if(!Constants.ACTION_VIEW_HISTORY.equals(action)){
					Button btnAddRelatedUser = new Button();
					btnAddRelatedUser.setIcon(FontAwesome.USER_PLUS);
					btnAddRelatedUser.setDescription("Thêm người dùng liên quan");
					btnAddRelatedUser.addClickListener(this);
					btnAddRelatedUser.setId(BTN_ADD_RELATED_USER_ID);
					btnAddRelatedUser.addStyleName(Reindeer.BUTTON_LINK);
					btnAddRelatedUser.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_BTN_ADD);
					relatedLblWrap.addComponent(btnAddRelatedUser);

					Button btnTaskEvent = new Button();
					btnTaskEvent.setIcon(FontAwesome.HISTORY);
					btnTaskEvent.setDescription("Lịch sử");
					btnTaskEvent.addClickListener(this);
					btnTaskEvent.setId(BTN_TASK_ENVENT_ID);
					btnTaskEvent.addStyleName(Reindeer.BUTTON_LINK);
					btnTaskEvent.addStyleName(ExplorerLayout.TASK_DETAIL_TASK_EVENT_BTN);
					relatedLblWrap.addComponent(btnTaskEvent);
				}
				

				relatedUserLayout = new CssLayout();
				relatedUserLayout.setWidth("100%");
				relatedUserLayout.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_WRAP);
				wrap.addComponent(relatedUserLayout);

				if(Constants.ACTION_VIEW_HISTORY.equals(action)){
					historicRelatedUser(relatedUserLayout, lblRelatedUser, seeMoreUser);
				}else{
					refreshRelatedUser(relatedUserLayout, lblRelatedUser, seeMoreUser);
				}
				
			}
			mainContainer.addComponent(wrap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void refreshRelatedUser(CssLayout wrap, Label lblRelatedUser, Label seeMoreUser) {
		wrap.removeAllComponents();
		List<IdentityLink> identityLinks = taskService.getIdentityLinksForTask(taskCurrent.getId());
		if (identityLinks != null && !identityLinks.isEmpty()) {
			int d = 0;
			Map<String, List<IdentityLink>> identityLinkTypeMap = new HashMap();
			for (IdentityLink il : identityLinks) {
				if (!IdentityLinkType.ASSIGNEE.equals(il.getType()) && !IdentityLinkType.OWNER.equals(il.getType())) {
					d++;
					List<IdentityLink> list = identityLinkTypeMap.get(il.getType());
					if (list == null) {
						list = new ArrayList<IdentityLink>();
						identityLinkTypeMap.put(il.getType(), list);
					}
					list.add(il);
				}
			}

			if (!identityLinkTypeMap.isEmpty()) {

				seeMoreUser.setVisible(true);
				lblRelatedUser.setValue("Cùng với " + d + " người khác");
				for (Map.Entry<String, List<IdentityLink>> entry : identityLinkTypeMap.entrySet()) {
					if (entry.getValue() != null && entry.getKey() != null) {
						CssLayout item = initRelatedUserGroup(entry.getKey(), entry.getValue());
						if (item != null) {
							relatedUserLayout.addComponent(item);
						}
					}
				}
			} else {
				seeMoreUser.setVisible(false);
				lblRelatedUser.setValue("Không có ai liên quan khác");
			}
		}
	}
	
	public void historicRelatedUser(CssLayout wrap, Label lblRelatedUser, Label seeMoreUser) {
		wrap.removeAllComponents();
		
		List<HistoricIdentityLink> identityLinks = historyService.getHistoricIdentityLinksForTask(taskCurrent.getId());
		if (identityLinks != null && !identityLinks.isEmpty()) {
			int d = 0;
			Map<String, List<HistoricIdentityLink>> identityLinkTypeMap = new HashMap();
			for (HistoricIdentityLink il : identityLinks) {
				if (!IdentityLinkType.ASSIGNEE.equals(il.getType()) && !IdentityLinkType.OWNER.equals(il.getType())) {
					d++;
					List<HistoricIdentityLink> list = identityLinkTypeMap.get(il.getType());
					if (list == null) {
						list = new ArrayList<HistoricIdentityLink>();
						identityLinkTypeMap.put(il.getType(), list);
					}
					list.add(il);
				}
			}

			if (!identityLinkTypeMap.isEmpty()) {

				seeMoreUser.setVisible(true);
				lblRelatedUser.setValue("Cùng với " + d + " người khác");
				for (Map.Entry<String, List<HistoricIdentityLink>> entry : identityLinkTypeMap.entrySet()) {
					if (entry.getValue() != null && entry.getKey() != null) {
						CssLayout item = initHistoricRelatedUserGroup(entry.getKey(), entry.getValue());
						if (item != null) {
							relatedUserLayout.addComponent(item);
						}
					}
				}
			} else {
				seeMoreUser.setVisible(false);
				lblRelatedUser.setValue("Không có ai liên quan khác");
			}

		}
	}

	public CssLayout initRelatedUserGroup(String type, List<IdentityLink> ilList) {
		if (type != null && ilList != null && !ilList.isEmpty()) {
			CssLayout wrap = new CssLayout();
			wrap.setWidth("100%");
			wrap.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_GROUP);

			Label lblttl = new Label(type);
			lblttl.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_GROUP_LBL);
			wrap.addComponent(lblttl);

			CssLayout relatedUserWrap = new CssLayout();
			relatedUserWrap.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_CONTAINER);
			wrap.addComponent(relatedUserWrap);

			for (IdentityLink il : ilList) {
				if (il.getUserId() != null) {
					User u = identityService.createUserQuery().userId(il.getUserId()).singleResult();

					if (u != null) {
						CssLayout relatedUser = new CssLayout();
						relatedUser.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_ITEM);
						relatedUserWrap.addComponent(relatedUser);

						Label lbl = new Label(u.getFirstName() + " " + u.getLastName());
						lbl.setIcon(FontAwesome.USER);
						lbl.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_ITEM_NAME);
						relatedUser.addComponent(lbl);

						Button remove = new Button();
						remove.setIcon(FontAwesome.REMOVE);
						remove.setId(BTN_REMOVE_RELATED_ID);
						Map<String, Object> data = new HashMap();
						data.put("USER", u);
						data.put("TYPE", il.getType());
						remove.setData(data);
						remove.addClickListener(this);
						remove.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_ITEM_REMOVE);
						remove.setDescription("Xóa");
						relatedUser.addComponent(remove);
					}
				}else if(il.getGroupId() != null){
					Group g = identityService.createGroupQuery().groupId(il.getGroupId()).singleResult();
					
					if (g != null) {
						CssLayout relatedGroup = new CssLayout();
						relatedGroup.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_ITEM);
						relatedGroup.addStyleName(ExplorerLayout.TASK_DETAIL_GROUP_RELATED_ITEM);
						relatedUserWrap.addComponent(relatedGroup);

						Label lbl = new Label(g.getName());
						lbl.setIcon(FontAwesome.USERS);
						lbl.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_ITEM_NAME);
						relatedGroup.addComponent(lbl);

						Button remove = new Button();
						remove.setIcon(FontAwesome.REMOVE);
						remove.setId(BTN_REMOVE_RELATED_ID);
						Map<String, Object> data = new HashMap();
						data.put("GROUP", g);
						data.put("TYPE", il.getType());
						remove.setData(data);
						remove.addClickListener(this);
						remove.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_ITEM_REMOVE);
						remove.setDescription("Xóa");
						relatedGroup.addComponent(remove);
					}
				}
			}
			return wrap;
		}
		return null;

	}
	
	public CssLayout initHistoricRelatedUserGroup(String type, List<HistoricIdentityLink> ilList) {
		if (type != null && ilList != null && !ilList.isEmpty()) {
			CssLayout wrap = new CssLayout();
			wrap.setWidth("100%");
			wrap.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_GROUP);

			Label lblttl = new Label(type);
			lblttl.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_GROUP_LBL);
			wrap.addComponent(lblttl);

			CssLayout relatedUserWrap = new CssLayout();
			relatedUserWrap.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_CONTAINER);
			wrap.addComponent(relatedUserWrap);

			for (HistoricIdentityLink il : ilList) {
				if (il.getUserId() != null) {
					User u = identityService.createUserQuery().userId(il.getUserId()).singleResult();

					if (u != null) {
						CssLayout relatedUser = new CssLayout();
						relatedUser.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_ITEM);
						relatedUser.addStyleName(ExplorerLayout.TASK_DETAIL_HISTORY_RELATED_ITEM);
						relatedUserWrap.addComponent(relatedUser);

						Label lbl = new Label(u.getFirstName() + " " + u.getLastName());
						lbl.setIcon(FontAwesome.USER);
						lbl.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_ITEM_NAME);
						relatedUser.addComponent(lbl);
					}
				}else if(il.getGroupId() != null){
					Group g = identityService.createGroupQuery().groupId(il.getGroupId()).singleResult();
					
					if (g != null) {
						CssLayout relatedGroup = new CssLayout();
						relatedGroup.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_ITEM);
						relatedGroup.addStyleName(ExplorerLayout.TASK_DETAIL_GROUP_RELATED_ITEM);
						relatedGroup.addStyleName(ExplorerLayout.TASK_DETAIL_HISTORY_RELATED_ITEM);
						relatedUserWrap.addComponent(relatedGroup);

						Label lbl = new Label(g.getName());
						lbl.setIcon(FontAwesome.USERS);
						lbl.addStyleName(ExplorerLayout.TASK_DETAIL_USER_RELATED_ITEM_NAME);
						relatedGroup.addComponent(lbl);

					}
				}
			}
			return wrap;
		}
		return null;

	}

	public void initFormWrap() {

		try {
			TaskInfo superParent = null;
			
			if(taskCurrent.getParentTaskId() != null){
				superParent = ProcessInstanceUtils.getParentTaskIdInProcess(taskCurrent.getId());
			}
			
			CssLayout wrap = new CssLayout();
			wrap.setWidth("100%");
			wrap.addStyleName(ExplorerLayout.TASK_DETAIL_FORM_WRAP);

			if (!isCurrentUserAssignee(taskCurrent) && canUserClaimTask(taskCurrent)) {
				claimWrap.setVisible(true);
			}

			if (superParent != null || taskCurrent != null) {
				String proDefId = null;
				if(superParent != null){
					proDefId = superParent.getProcessDefinitionId();
				}else{
					proDefId = taskCurrent.getProcessDefinitionId();
				}
				if(proDefId != null){
					ProcessDefinition proDef = repositoryService.createProcessDefinitionQuery().processDefinitionId(proDefId).singleResult();
					EFormHoSo hoSoFormInfo = null;
					String formInfoKey = null;
					if(proDef != null){
						
						String tskDefId = taskCurrent.getTaskDefinitionKey();
						List<ProcessTaskProperty> properties = processTaskPropertyService.findByDeploymentIdAndTaskDefinitionId(proDef.getDeploymentId(), tskDefId);
						
						if(properties != null && !properties.isEmpty()){
							for (ProcessTaskProperty processTaskProperty : properties) {
								if("forminfokeydefinition".equals(processTaskProperty.getPropertyKey())){
									formInfoKey = processTaskProperty.getPropertyValue();
								}
							}
						}
						if(formInfoKey != null){
							if (hoSoCurrent != null) {
								String boHoSoXml = hoSoCurrent.getHoSoGiaTri();
								EFormBoHoSo boHS = CommonUtils.getEFormBoHoSo(boHoSoXml);
								
								if(boHS != null && boHS.getHoSo() != null){
									hoSoFormInfo = boHS.getHoSo();
								}
							}
						}
					}
					
					String hoSoXml = null;
					if (hoSoCurrent != null) {
						String boHoSoXml = hoSoCurrent.getHoSoGiaTri();
						EFormBoHoSo boHS = CommonUtils.getEFormBoHoSo(boHoSoXml);
						
						EFormHoSo hs = null;
						if(Constants.ACTION_VIEW_HISTORY.equals(action)){
							if(boHS != null && boHS.getLichSuList() != null){
								for(EFormHoSo history : boHS.getLichSuList()){
									if(taskCurrent != null && taskCurrent.getId().equals(history.getTaskId())){
										hs = history;
										break;
									}
								}
								if(hs != null){
									currentPath = hs.getLoopItemPath();
									currentThuTu = hs.getLoopItemIndex();
									dataLoopPath = hs.getDataLoopPath();
								}
								
							}
						}else{
							if (boHS != null && boHS.getHoSo() != null) {
								hs = boHS.getHoSo();
							}
						}
						
						if(hs != null){
							List<String> pathList = null;
							if(currentPath != null){
								String[] pathArr = currentPath.split("#");
					    		pathList = java.util.Arrays.asList(pathArr);
							}
							
							if(dataLoopPath != null){
								String[] pathArr = dataLoopPath.split("#");
								List<String> pathListTmp = java.util.Arrays.asList(pathArr);

								List<EFormTruongThongTin> truongThongTinLoop = new ArrayList();
								hs = CommonUtils.getEFormHoSoByLoopPath(hs, pathListTmp, truongThongTinLoop);
								if(hoSoFormInfo != null){
									hoSoFormInfo.getTruongThongTinList().addAll(truongThongTinLoop);
								}
							}

							hoSoXml = CommonUtils.getEFormHoSoString(hs, pathList, currentThuTu);
							
						}
						
						
					}
					
					String formKey = taskCurrent.getFormKey();
					String buttonKey = null;
					
					if(formKey == null && superParent != null){
						formKey = superParent.getFormKey();
					}
					
					if(formInfoKey != null){
						String hoSoInfoXml = CommonUtils.getEFormHoSoString(hoSoFormInfo);
						formInfo = new FormGenerate(formInfoKey, hoSoInfoXml, true);
						formInfo.setWidth("100%");
						wrap.addComponent(formInfo);
					}
					
					if (formKey != null) {
						Pattern p = Pattern.compile("(.+)#(.+)");
						Matcher m = p.matcher(formKey);
						if (m.matches()) {
							formKey = m.group(1);
							buttonKey = m.group(2);
						}
						
						boolean readOnly = isHistoric || !isCurrentUserAssignee(taskCurrent) || superParent != null;
						formGenerate = new FormGenerate(formKey, hoSoXml, readOnly, taskCurrent.getAssignee());
						formGenerate.setWidth("100%");
						wrap.addComponent(formGenerate);
					} else {
						Label label = new Label("Không có biểu mẫu!");
						label.setStyleName(ExplorerLayout.LBL_EMPTY_TAB);
						wrap.addComponent(label);
					}
					
					
					
	
					if (!isHistoric && isCurrentUserAssignee(taskCurrent)) {
						CssLayout buttons = initButtons(buttonKey, superParent != null);
						if (buttons != null) {
							wrap.addComponent(buttons);
						}
					}
				}
			}
			mainContainer.addComponent(wrap);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public CssLayout initButtons(String buttonKey, boolean subtask) {
		// tuanna
		CssLayout buttons = new CssLayout();
		buttons.setWidth("100%");
		
		buttons.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);

		if(!subtask){
			Button btnSave = new Button("Save");
			btnSave.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
			btnSave.setId(BUTTON_SAVE_ID);
			btnSave.addClickListener(this);
			buttons.addComponent(btnSave);

		}
		
		List<DanhMuc> danhMucList = null;
		boolean hasButton = false;
		if (buttonKey != null && !subtask) {
			
			Pattern p = Pattern.compile("(.+):\\[(.+)\\]");
	    	Matcher m = p.matcher(buttonKey);
	    	if(m.matches()){
	    		//Lay button theo string
	    		String btns = m.group(2);
        		String btnArr[] = btns.split(";");
        		String btnGroup = m.group(1).trim();
        		if(btnArr != null && btnArr.length >0){
        			hasButton = true;
        			Pattern p1 = Pattern.compile("(.+):(.+)");
        	    	for(int i =0; i< btnArr.length; i++){
        	    		Matcher m1 = p1.matcher(btnArr[i]);
        	    		if(m1.matches()){
        	    			System.out.println(m1.group(1).trim());
        	    			System.out.println(m1.group(2).trim());
        	    			
        	    			Button btn = new Button(m1.group(2).trim());
							btn.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
							btn.addClickListener(this);
							Map<String, String> mapData = new HashMap<String, String>();
							mapData.put("nhom", btnGroup);
							mapData.put("ma", m1.group(1).trim());
							btn.setData(mapData);
							btn.setId(BUTTON_COMPLETE_ID);
							buttons.addComponent(btn);
        	    		}
        	    	}
        		}
	    	}else{
	    		//Lay button theo danh muc
	    		WorkflowManagement<LoaiDanhMuc> ldmWM = new WorkflowManagement<LoaiDanhMuc>(
						EChucNang.E_FORM_LOAI_DANH_MUC.getMa());
				ITrangThai ldmTrangThaiSD = ldmWM.getTrangThaiSuDung();
				List<Long> ldmTrangThaiList = null;
				if (ldmTrangThaiSD != null) {
					ldmTrangThaiList = new ArrayList<Long>();
					ldmTrangThaiList.add(new Long(ldmTrangThaiSD.getId()));
				}
				List<LoaiDanhMuc> loaiDanhMucList = loaiDanhMucService.getLoaiDanhMucFind(buttonKey, null, ldmTrangThaiList,
						null, 0, 1);

				if (loaiDanhMucList != null && !loaiDanhMucList.isEmpty()) {
					LoaiDanhMuc loaiDanhMuc = loaiDanhMucList.get(0);

					WorkflowManagement<DanhMuc> dmWM = new WorkflowManagement<DanhMuc>(EChucNang.E_FORM_DANH_MUC.getMa());
					ITrangThai dmTrangThaiSD = dmWM.getTrangThaiSuDung();
					List<Long> dmTrangThaiList = null;
					if (ldmTrangThaiSD != null) {
						dmTrangThaiList = new ArrayList<Long>();
						dmTrangThaiList.add(new Long(dmTrangThaiSD.getId()));
					}
					danhMucList = danhMucService.getDanhMucFind(null, null, dmTrangThaiList, loaiDanhMuc, null, -1, -1);
					if (danhMucList != null && !danhMucList.isEmpty()) {
						for (DanhMuc dm : danhMucList) {
							Button btn = new Button(dm.getTen());
							btn.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
							btn.addClickListener(this);
							Map<String, String> mapData = new HashMap<String, String>();
							mapData.put("nhom", loaiDanhMuc.getMa());
							mapData.put("ma", dm.getMa());
							btn.setData(mapData);
							btn.setId(BUTTON_COMPLETE_ID);
							buttons.addComponent(btn);
						}
						hasButton = true;
					}
				}
	    	}
		}
		if (!hasButton) {
			Button btnComplete = new Button("Complete");
			btnComplete.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
			btnComplete.setId(BUTTON_COMPLETE_ID);
			btnComplete.addClickListener(this);
			buttons.addComponent(btnComplete);
		}
		return buttons;
	}

	public boolean canUserClaimTask(TaskInfo task) {
		return taskService.createTaskQuery().taskCandidateUser(CommonUtils.getUserLogedIn()).taskId(task.getId())
				.count() == 1;
	}

	public boolean isCurrentUserAssignee(TaskInfo task) {
		String currentUser = CommonUtils.getUserLogedIn();
		return currentUser.equals(task.getAssignee());
	}

	public boolean isCurrentUserOwner(TaskInfo task) {
		String currentUser = CommonUtils.getUserLogedIn();
		return currentUser.equals(task.getOwner());
	}

	public void initInteractiveWrap() {
		try {
			CssLayout wrap = new CssLayout();
			wrap.setWidth("100%");
			wrap.addStyleName(ExplorerLayout.TASK_DETAIL_INTERACTIVE_WRAP);
			mainContainer.addComponent(wrap);

			if (taskCurrent != null) {
				List<Attachment> attachments = taskService
						.getProcessInstanceAttachments(processInsId);
				// tất  cả  các  tệp đính kèm theo  processinstance
				List<Comment> comments = taskService.getProcessInstanceComments(processInsId);
				// tất theo processinstance
				List<org.activiti.engine.task.Event> taskEvents = taskService.getTaskEvents(taskCurrent.getId());
				// Event va cả comment comment cua task
				wrap.addComponent(initCommentWrap(comments));

				wrap.addComponent(initAttachmentWrap(attachments));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	
	public void refreshCommentWrap(String userLogedinId){
		List<Comment> comments = taskService.getProcessInstanceComments(processInsId);
		if(comments != null){
			Collections.sort(comments, new Comparator<Comment>() {
			    @Override
			    public int compare(Comment o1, Comment o2) {
			    	if(o1.getTime() == null){
			    		return -1;
			    	}
			        return o1.getTime().compareTo(o2.getTime());
			    }
			});
			commentContainer.removeAllComponents();
			for(Comment comment : comments){
				CssLayout cmtItem = initCommentItem(comment, userLogedinId);
				if (cmtItem != null && commentContainer != null) {
					commentContainer.addComponent(cmtItem, 0);
				}
			}
			if(commentCount != null){
				commentCount.setValue(comments.size() + " Bình luận");
			}
		}
	}

	public CssLayout initCommentWrap(List<Comment> comments) {
		CssLayout cmtWrap = new CssLayout();
		cmtWrap.setWidth("100%");
		cmtWrap.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_WRAP);

		if (!isHistoric) {
			commentCountNum = comments.size();
			CssLayout cmtBox = initCommentBox(commentCountNum);
			if (cmtBox != null) {
				cmtWrap.addComponent(cmtBox);
			}
		} else {
			CssLayout header = new CssLayout();
			header.setWidth("100%");
			header.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_HEADER);
			cmtWrap.addComponent(header);

			Label count = new Label(comments.size() + " Bình luận");
			count.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_COUNT);
			header.addComponent(count);
		}

		commentContainer = new CssLayout();
		commentContainer.setWidth("100%");
		commentContainer.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_CONTAINER);
		cmtWrap.addComponent(commentContainer);

		if (comments != null && !comments.isEmpty()) {

			for (org.activiti.engine.task.Comment comment : comments) {
				if (comment != null && "comment".equals(comment.getType())) {
					CssLayout cmtItem = initCommentItem(comment, CommonUtils.getUserLogedIn());
					
					if(comment.getId().equals(commentId)){
						componentScrollToView = cmtItem;
					}
					
					if (cmtItem != null) {
						commentContainer.addComponent(cmtItem);
					}
				}
			}
		}
		return cmtWrap;
	}
	
	public void refreshSubTaskContainer(){
		if(subtaskContainer == null){
			initSubtaskContainer();
		}else{
			subtaskContainer.removeAllItems();
			List<HistoricTaskInstance> subTaskList = historyService.createHistoricTaskInstanceQuery().taskParentTaskId(taskCurrent.getId()).orderByTaskCreateTime().desc().list();
			subtaskContainer.addAll(subTaskList);
		}
	}
	
	public void initSubtaskContainer(){
		List<HistoricTaskInstance> subTaskList = historyService.createHistoricTaskInstanceQuery().taskParentTaskId(taskCurrent.getId()).orderByTaskCreateTime().desc().list();
		
		if(subTaskList == null || subTaskList.isEmpty()){
			return;
		}
		
		CssLayout subtaskWrap = new CssLayout();
		subtaskWrap.setWidth("100%");
		subtaskWrap.addStyleName(ExplorerLayout.TASK_DETAIL_SUBTASK_WRAP); 
		Label subtaskTitle = new Label("Danh sách Subtask");
		subtaskWrap.addComponent(subtaskTitle);
		
		CssLayout subtaskContent = new CssLayout();
		subtaskContent.setWidth("100%");
		subtaskContent.addStyleName(ExplorerLayout.TASK_DETAIL_SUBTASK_CONTENT);
		
		Table table = new Table();
		subtaskContainer = new BeanItemContainer<HistoricTaskInstance>(HistoricTaskInstance.class, subTaskList);
		table.setContainerDataSource(subtaskContainer);
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setResponsive(true);
		table.setWidth(100, Unit.PERCENTAGE);
		
		table.addGeneratedColumn("createTimeText", new ColumnGenerator() { 
			private static final long serialVersionUID = 1L;
			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				HistoricTaskInstance current = (HistoricTaskInstance) itemId;
				if(current != null && current.getCreateTime() != null){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
					return df.format(current.getCreateTime());
				}
		        return "";
		    }
		});
		table.addGeneratedColumn("endTimeText", new ColumnGenerator() { 
			private static final long serialVersionUID = 1L;
			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				HistoricTaskInstance current = (HistoricTaskInstance) itemId;
				if(current != null && current.getEndTime() != null){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
					return df.format(current.getEndTime());
				}
		        return "Chưa hoàn thành";
		    }
		});
		
		table.setColumnHeader("name", "Tên công việc");
		table.setColumnHeader("description", "Mô tả");
		table.setColumnHeader("assignee", "Người thực hiện");
		table.setColumnHeader("createTimeText", "Ngày tạo");
		table.setColumnHeader("endTimeText", "Ngày hoàn thành");
		table.setVisibleColumns("name", "description", "assignee", "createTimeText", "endTimeText");
		
		subtaskContent.addComponent(table);
		
		
		subtaskWrap.addComponent(subtaskContent);
		
		mainContainer.addComponent(subtaskWrap);
	}

	public void initNavigationWrap() {
		CssLayout navWrap = new CssLayout();
		navWrap.addStyleName(ExplorerLayout.TASK_DETAIL_NAV_WRAP);
		mainContainer.addComponent(navWrap);

		Label cmtBtn = new Label();
		cmtBtn.setDescription("Bình luận");
		cmtBtn.addStyleName(ExplorerLayout.TASK_DETAIL_NAV_COMMENT);
		navWrap.addComponent(cmtBtn);

		Label attBtn = new Label();
		attBtn.setDescription("Đính kèm");
		attBtn.addStyleName(ExplorerLayout.TASK_DETAIL_NAV_ATT);
		navWrap.addComponent(attBtn);

		Label buttonOpen = new Label();
		buttonOpen.addStyleName(ExplorerLayout.TASK_DETAIL_NAV_OPEN_BTN);
		navWrap.addComponent(buttonOpen);
		String humburger = "<span class='hamburger hamburger-1'></span> <span class='hamburger hamburger-2'></span> <span class='hamburger hamburger-3'></span>";
		buttonOpen.setValue(humburger);
		buttonOpen.setContentMode(ContentMode.HTML);
		
		
	}

	public CssLayout initCommentBox(int count) {
		CssLayout commentBox = new CssLayout();
		commentBox.setWidth("100%");
		commentBox.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_BOX);

		CssLayout commentCoutWrap = new CssLayout();
		commentCoutWrap.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_CMT_COUNT);
		commentBox.addComponent(commentCoutWrap);
		if (count > 0) {
			commentCoutWrap.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_CMT);
		}
		commentCount = new Label(count + " Bình luận");
		commentCoutWrap.addComponent(commentCount);

		CssLayout txtCommentWrap = new CssLayout();
		txtCommentWrap.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_TXT_WRAP);
		commentBox.addComponent(txtCommentWrap);

		TextField txtComment = new TextField();
		txtComment.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Authentication.setAuthenticatedUserId(CommonUtils.getUserLogedIn());
				String value = (String) event.getProperty().getValue();
				if (value != null && !value.trim().isEmpty()) {
					try {
						org.activiti.engine.task.Comment comment = taskService.addComment(taskCurrent.getId(),
								processInsId, value);
						refreshCommentWrap(CommonUtils.getUserLogedIn());
						Broadcaster.broadcast(BroadcastType.TASK_COMMENT, value, CommonUtils.getUserLogedIn(), comment.getId());
						txtComment.clear();

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
		txtComment.setInputPrompt("Bình luận của bạn...");
		txtComment.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_TXT);
		txtCommentWrap.addComponent(txtComment);

		return commentBox;
	}

	public CssLayout initCommentItem(org.activiti.engine.task.Comment comment, String userLogedinId) {
		CssLayout commentItem = new CssLayout();
		commentItem.setId("cmt-"+comment.getId());
		commentItem.setWidth("100%");
		commentItem.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_ITEM);

		// Left
		CssLayout commentDateWrap = new CssLayout();
		commentDateWrap.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_DATE_WRAP);
		commentItem.addComponent(commentDateWrap);

		String time = "#";
		if (comment.getTime() != null) {
			SimpleDateFormat dayMonthDf = new SimpleDateFormat(Constants.DATE_FORMAT_DAY_MONTH_TEXT);
			SimpleDateFormat timeDf = new SimpleDateFormat(Constants.DATE_FORMAT_TIME);
			time = "<span class='cmt-date-day'>" + dayMonthDf.format(comment.getTime()) + "</span>";
			time += "<span class='cmt-date-time'>" + timeDf.format(comment.getTime()) + "</span>";
		}
		Label commentDate = new Label(time);
		commentDate.setContentMode(ContentMode.HTML);
		commentDate.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_DATE);
		commentDateWrap.addComponent(commentDate);

		// Right

		CssLayout commentInfo = new CssLayout();
		commentInfo.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_INFO);
		commentItem.addComponent(commentInfo);
		User user = null;
		if (comment.getUserId() != null) {
			user = identityService.createUserQuery().userId(comment.getUserId()).singleResult();
		}

		String userStr = "#Không xác định";
		if (user != null) {
			userStr = user.getFirstName() + " " + user.getLastName();
		}

		CssLayout userWrap = new CssLayout();
		userWrap.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_USER_WRAP);
		commentInfo.addComponent(userWrap);

		Label userLbl = new Label(userStr);
		userLbl.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_USER);
		userWrap.addComponent(userLbl);

		CssLayout contentWrap = new CssLayout();
		contentWrap.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_CONTENT_WRAP);
		commentInfo.addComponent(contentWrap);

		Label contentLbl = new Label(comment.getFullMessage());
		contentLbl.addStyleName(ExplorerLayout.TASK_DETAIL_COMMENT_CONTENT);
		contentWrap.addComponent(contentLbl);
		
		if(comment.getUserId() != null && comment.getUserId().equals(userLogedinId)){
			Button btnRemoveComment = new Button("Xóa");
			btnRemoveComment.addStyleName(ExplorerLayout.TASK_DETAIL_BTN_DELETE_COMMENT);
			btnRemoveComment.setId(BTN_DEL_COMMENT_ID);
			btnRemoveComment.addStyleName(Reindeer.BUTTON_LINK);
			btnRemoveComment.addClickListener(this);
			btnRemoveComment.setData(comment);
			commentInfo.addComponent(btnRemoveComment);
		}

		return commentItem;
	}

	public CssLayout initAttachmentWrap(List<Attachment> attachments) {

		if (attachments != null) {
			CssLayout wrap = new CssLayout();
			wrap.setWidth("100%");
			wrap.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_WRAP);

			CssLayout header = new CssLayout();
			header.setWidth("100%");
			header.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_HEADER);
			wrap.addComponent(header);

			attCount = new Label(attachments.size() + " Tệp đính kèm");
			attCount.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_COUNT);
			header.addComponent(attCount);

			attContainer = new CssLayout();
			attContainer.setWidth("100%");
			attContainer.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_CONTAINER);
			wrap.addComponent(attContainer);

			if (!isHistoric) {
				CssLayout addItem = new CssLayout();
				addItem.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM);
				addItem.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_ADD);
				attContainer.addComponent(addItem);

				Button btnAddAtt = new Button();
				btnAddAtt.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_ADD_BTN);
				btnAddAtt.setDescription("Thêm tệp đính kèm");
				btnAddAtt.setIcon(FontAwesome.PAPERCLIP);
				btnAddAtt.setId(BTN_ADD_RELATED_ID);
				btnAddAtt.addClickListener(this);
				addItem.addComponent(btnAddAtt);

			}

			if (!attachments.isEmpty()) {
				for (Attachment att : attachments) {
					CssLayout attItem = initAttachmentItem(att);
					if (attItem != null) {
						attContainer.addComponent(attItem);
					}
				}
			}

			return wrap;
		}

		return null;

	}

	public void initAddRelatedWindow() {
		FormLayout form = new FormLayout();
		form.setMargin(true);

		TextField txtTen = new TextField("Tên");
		txtTen.setRequired(true);
		txtTen.setWidth("100%");
		txtTen.setRequiredError("Không được để trống");
		txtTen.setNullRepresentation("");
		form.addComponent(txtTen);

		TextArea textArea = new TextArea("Mô tả");
		textArea.setRequired(true);
		textArea.setWidth("100%");
		textArea.setRequiredError("Không được để trống");
		textArea.setNullRepresentation("");
		form.addComponent(textArea);

		Long fileSize = 10000000L;
		List<String> fileExtList = new ArrayList<String>();

		UploadReceive receive = new UploadReceive(uploadPath);
		Upload upload = new Upload(null, receive);
		upload.setImmediate(true);
		upload.setStyleName(ExplorerLayout.BTN_UPLOAD);

		Label lbl = new Label();
		lbl.setVisible(false);
		long size = fileSize != null ? fileSize.longValue() : 0;
		new UploadAttachmentInfo(upload, lbl, receive, size, fileExtList);

		CssLayout uploadWrap = new CssLayout();

		TextField hidden = new TextField();
		hidden.setVisible(false);
		upload.setData(hidden);

		uploadWrap.addComponent(upload);
		uploadWrap.addComponent(lbl);
		uploadWrap.addComponent(hidden);

		form.addComponent(uploadWrap);

		relatedItem.addItemProperty("ten", new ObjectProperty<String>("", String.class));
		relatedItem.addItemProperty("mota", new ObjectProperty<String>("", String.class));
		relatedItem.addItemProperty("file", new ObjectProperty<String>("", String.class));
		relatedFeildGroup.bind(txtTen, "ten");
		relatedFeildGroup.bind(textArea, "mota");
		relatedFeildGroup.bind(hidden, "file");

		Button create = new Button("Thêm mới");
		create.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
		create.addClickListener(this);
		create.setId(BTN_CREATE_RELATED_ID);
		form.addComponent(create);

		addRelatedWindow = new Window("Nội dung liên quan");
		addRelatedWindow.setWidth(600, Unit.PIXELS);
		addRelatedWindow.setResizable(false);
		addRelatedWindow.center();
		addRelatedWindow.setModal(true);
		addRelatedWindow.setContent(form);
		Map<String, Object> data = new HashMap();
		data.put("upload", upload);
		data.put("lbl", lbl);
		addRelatedWindow.setData(data);
	}

	public CssLayout initAttachmentItem(Attachment att) {
		CssLayout item = new CssLayout();
		item.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM);

		CssLayout infoWrap = new CssLayout();
		infoWrap.setWidth("100%");
		infoWrap.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_INFO_WRAP);
		item.addComponent(infoWrap);

		Label type = new Label();
		if (att.getType() != null) {
			type.setValue(att.getType());
		} else {
			type.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_TYPE_EMPTY);
		}
		type.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_TYPE);
		infoWrap.addComponent(type);

		String userStr = "#Không xác định";

		if (att.getUserId() != null) {
			User user = identityService.createUserQuery().userId(att.getUserId()).singleResult();
			if (user != null) {
				userStr = user.getFirstName() + " " + user.getLastName();
			}
		}
		Label user = new Label(userStr);
		user.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_USER);
		infoWrap.addComponent(user);

		if (att.getTime() != null) {
			SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
			Label lblTime = new Label(df.format(att.getTime()));
			lblTime.setStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_TIME);
			infoWrap.addComponent(lblTime);
		}

		CssLayout nameWrap = new CssLayout();
		nameWrap.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_NAME_WRAP);
		nameWrap.setWidth("100%");
		item.addComponent(nameWrap);

		Label name = new Label(att.getName());
		name.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_NAME);
		nameWrap.addComponent(name);

		CssLayout downloadWrap = new CssLayout();
		downloadWrap.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_DONWLOAD_WRAP);
		downloadWrap.setWidth("100%");
		nameWrap.addComponent(downloadWrap);

		Button btnDownload = new Button();
		btnDownload.setIcon(FontAwesome.DOWNLOAD);
		btnDownload.setDescription("Tải xuống");
		btnDownload.addStyleName(ExplorerLayout.TASK_DETAIL_ATT_ITEM_DONWLOAD_BTN);
		downloadWrap.addComponent(btnDownload);
		InputStream input = taskService.getAttachmentContent(att.getId());
		FileUtils.downloadWithButton(btnDownload, input, att.getName() + "." + att.getType());

		return item;
	}



	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Authentication.setAuthenticatedUserId(CommonUtils.getUserLogedIn());
			Button button = event.getButton();
			if (BUTTON_SAVE_ID.equals(button.getId())) {
				if (formGenerate.validateForm()) {
					String hoSoGiaTri = formGenerate.getFormData();
					System.out.println(hoSoGiaTri);
					EFormBoHoSo eFormBoHS = null;

					Date date = new Date();
					if (hoSoCurrent != null) {
						hoSoCurrent.setNgayCapNhat(new Timestamp(date.getTime()));
						String boHS = hoSoCurrent.getHoSoGiaTri();
						EFormHoSo hs = CommonUtils.getEFormHoSo(hoSoGiaTri);
						EFormBoHoSo eBoHS = CommonUtils.getEFormBoHoSo(boHS);
						List<String> pathList = null;
						EFormHoSo eHs = hs;
						if(currentPath != null && currentThuTu != null){
							String[] pathArr = currentPath.split("#");
				    		pathList = java.util.Arrays.asList(pathArr);
						}
						//FormUtils.updateHoSo(eBoHS.getHoSo(), hs, pathList, currentThuTu);
						
//							hs = eBoHS.getHoSo();
						if(eBoHS != null){
							FormUtils.updateEFormHoSo(eBoHS.getHoSo(), hs, pathList, dataLoopPath, currentThuTu);
							eHs = eBoHS.getHoSo();
			    		}
						
						eFormBoHS = FormUtils.getBoHs(eBoHS, eHs, false, taskCurrent.getId());
						if (eFormBoHS != null) {
							boHS = CommonUtils.getEFormBoHoSoString(eFormBoHS);
						}
						hoSoCurrent.setHoSoGiaTri(boHS);
						hoSoCurrent.setNguoiCapNhat(CommonUtils.getUserLogedIn());
						//hoSoCurrent.setProcessInstanceId(taskCurrent.getProcessInstanceId());
					} else {
						hoSoCurrent = new HoSo();
						hoSoCurrent.setTrangThai(new Long(CommonUtils.ETrangThaiHoSo.NEW.getCode()));
						hoSoCurrent.setNgayTao(new Timestamp(date.getTime()));
						String boHS = hoSoCurrent.getHoSoGiaTri();

						EFormBoHoSo eBoHS = CommonUtils.getEFormBoHoSo(boHS);
						EFormHoSo hs = CommonUtils.getEFormHoSo(hoSoGiaTri);
						List<String> pathList = null;
						EFormHoSo eHs = hs;
						if(currentPath != null && currentThuTu != null){
							String[] pathArr = currentPath.split("#");
				    		pathList = java.util.Arrays.asList(pathArr);
						}
			    		
			    		if(eBoHS != null){
			    			FormUtils.updateEFormHoSo(eBoHS.getHoSo(), hs, pathList, dataLoopPath, currentThuTu);
							eHs = eBoHS.getHoSo();
			    		}
						
						
						eFormBoHS = FormUtils.getBoHs(eBoHS, eHs, false, taskCurrent.getId());
						if (eFormBoHS != null) {
							boHS = CommonUtils.getEFormBoHoSoString(eFormBoHS);
						}
						hoSoCurrent.setHoSoGiaTri(boHS);
						hoSoCurrent.setNguoiTao(CommonUtils.getUserLogedIn());
						hoSoCurrent.setProcessInstanceId(supserParentCurrentId);
					}

					if (eFormBoHS != null) {
						JSONObject json = FormUtils.getHoSoJson(eFormBoHS.getHoSo());
						hoSoCurrent.setHoSoJson(json);
					}

					if (hoSoCurrent != null) {
						hoSoService.save(hoSoCurrent);
					}

					Notification notf = new Notification("Thông báo", "Lưu thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					UI.getCurrent().getNavigator().navigateTo(InboxView.VIEW_NAME);
				}
			} else if (BUTTON_COMPLETE_ID.equals(button.getId())) {
				try {
					if (formGenerate == null) {
						taskService.complete(taskCurrent.getId());
						Notification notf = new Notification("Thông báo", "Complete thành công");
						notf.setDelayMsec(3000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
						try{
							List<Task> taskList = taskService.createTaskQuery().taskAssignee(CommonUtils.getUserLogedIn()).processInstanceId(processInsId).active().list();
							if(taskList != null && taskList.size() == 1){
								Task currentRow = taskList.get(0);
								UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
								
							}else{
								UI.getCurrent().getNavigator().navigateTo(InboxView.VIEW_NAME);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						
					} else if (formGenerate.validateForm()) {
						String hoSoGiaTri = formGenerate.getFormData();
						Date date = new Date();
						EFormBoHoSo eFormBoHS = null;
						Map<String, String> data = null;
						if (button.getData() != null && button.getData() instanceof Map) {
							data = (Map<String, String>) button.getData();
						}

						EFormHoSo hs = CommonUtils.getEFormHoSo(hoSoGiaTri);
						if (hoSoCurrent != null) {
							hoSoCurrent.setNgayCapNhat(new Timestamp(date.getTime()));
							String boHS = hoSoCurrent.getHoSoGiaTri();
							
							EFormBoHoSo eBoHS = CommonUtils.getEFormBoHoSo(boHS);
							List<String> pathList = null;
							EFormHoSo eHs = hs;
							if(currentPath != null && currentThuTu != null){
								String[] pathArr = currentPath.split("#");
					    		pathList = java.util.Arrays.asList(pathArr);
							}
				    		
				    		if(eBoHS != null){
				    			FormUtils.updateEFormHoSo(eBoHS.getHoSo(), hs, pathList, dataLoopPath, currentThuTu);
								eHs = eBoHS.getHoSo();
				    		}
							
							
							
							eFormBoHS = FormUtils.getBoHs(eBoHS, eHs, true, taskCurrent.getId(), currentPath, currentThuTu, dataLoopPath);
							if (eFormBoHS != null) {
								boHS = CommonUtils.getEFormBoHoSoString(eFormBoHS);
							}
							hoSoCurrent.setHoSoGiaTri(boHS);
							hoSoCurrent.setNguoiCapNhat(CommonUtils.getUserLogedIn());
							hoSoCurrent.setTaskId(taskCurrent.getId());
						} else {
							hoSoCurrent = new HoSo();
							hoSoCurrent.setTrangThai(new Long(CommonUtils.ETrangThaiHoSo.NEW.getCode()));
							hoSoCurrent.setNgayTao(new Timestamp(date.getTime()));
							String boHS = hoSoCurrent.getHoSoGiaTri();
							EFormBoHoSo eBoHS = CommonUtils.getEFormBoHoSo(boHS);
							List<String> pathList = null;
							EFormHoSo eHs = hs;
							if(currentPath != null && currentThuTu != null){
								String[] pathArr = currentPath.split("#");
					    		pathList = java.util.Arrays.asList(pathArr);
							}
				    		
				    		if(eBoHS != null){
				    			FormUtils.updateEFormHoSo(eBoHS.getHoSo(), hs, pathList, dataLoopPath, currentThuTu);
								eHs = eBoHS.getHoSo();
							}
							
							
							eFormBoHS = FormUtils.getBoHs(eBoHS, eHs, true, taskCurrent.getId(), currentPath, currentThuTu, dataLoopPath);
							if (eFormBoHS != null) {
								boHS = CommonUtils.getEFormBoHoSoString(eFormBoHS);
							}
							hoSoCurrent.setHoSoGiaTri(boHS);
							hoSoCurrent.setNguoiTao(CommonUtils.getUserLogedIn());
							hoSoCurrent.setTaskId(taskCurrent.getId());
							hoSoCurrent.setProcessInstanceId(supserParentCurrentId);
						}

						if (eFormBoHS != null) {
							JSONObject json = FormUtils.getHoSoJson(eFormBoHS.getHoSo());
							hoSoCurrent.setHoSoJson(json);

							if(hs != null){
								Map<String, List<String>> loopMap = new HashMap();
								FormUtils.getDataLoop(hs, loopMap, null);
								if(!loopMap.isEmpty()){
									for(Map.Entry<String, List<String>> entry : loopMap.entrySet()){
										runtimeService.setVariable(processInsId, entry.getKey(), entry.getValue());
									}
								}
							}
						}
						
						
						if (hoSoCurrent != null) {
							hoSoService.save(hoSoCurrent);
						}
						try {
							runtimeService.setVariable(processInsId, "CORE#DATA", hoSoGiaTri);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						Map<String, Object> variables = formGenerate.getFormVariableMap();
						if (data != null) {
							variables.put(data.get("nhom"), data.get("ma"));
						}
						taskService.complete(taskCurrent.getId(), variables);
						
						/*
						String formStringContent = formGenerate.getFormString();
						if (formStringContent != null && hoSoCurrent.getId() != null) {
							try {
								ProcessDefinition proDef = repositoryService.createProcessDefinitionQuery().processDefinitionId(taskCurrent.getProcessDefinitionId()).singleResult();
								String name = "";
								if (proDef != null) {
									name = proDef.getName() + " " + proDef.getCategory();
								}
								SolrUtils.updateSolr(
										CommonUtils.ESolrSearchType.HO_SO.getCode() + "_" + hoSoCurrent.getId(),
										formStringContent+" "+name, solrUrl);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						*/

						Notification notf = new Notification("Thông báo", "Complete thành công");
						notf.setDelayMsec(3000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
						try{
							List<Task> taskList = taskService.createTaskQuery().taskAssignee(CommonUtils.getUserLogedIn()).processInstanceId(processInsId).active().list();
							if(taskList != null && taskList.size() == 1){
								Task currentRow = taskList.get(0);
								UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
								
							}else{
								UI.getCurrent().getNavigator().navigateTo(InboxView.VIEW_NAME);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					
					HistoricProcessInstance proIns = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInsId).singleResult();
					if(proIns != null && hoSoCurrent != null){
						List<HoSo> hoSoCurrentListTmp = hoSoService.getHoSoFind(null, null, null, hoSoCurrent.getProcessInstanceId(), 0, 1);
						if(hoSoCurrentListTmp != null && !hoSoCurrentListTmp.isEmpty()){
							HoSo hoSoCurrentTmp = hoSoCurrentListTmp.get(0);
							
							if(proIns.getEndTime() != null){
								hoSoCurrentTmp.setTrangThai(new Long(CommonUtils.ETrangThaiHoSo.ENDED.getCode()));
								hoSoService.save(hoSoCurrentTmp);
							}else{
								hoSoCurrentTmp.setTrangThai(new Long(CommonUtils.ETrangThaiHoSo.IN_PROCESS.getCode()));
								hoSoService.save(hoSoCurrentTmp);
							}
						}
					}
				} catch (Exception e) {
					String message = e.getMessage();
					if(message == null || message.isEmpty()){
						message = "Đã có lỗi xảy ra!";
					}
					Notification notf = new Notification("Thông báo", message, Notification.Type.WARNING_MESSAGE);
					notf.setDelayMsec(20000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					e.printStackTrace();
				}
			} else if (BTN_CREATE_RELATED_ID.equals(button.getId())) {
				try {
					if (relatedFeildGroup.isValid()) {
						relatedFeildGroup.commit();
						addRelatedWindow.close();
						String ten = (String) relatedItem.getItemProperty("ten").getValue();
						String moTa = (String) relatedItem.getItemProperty("mota").getValue();
						String file = (String) relatedItem.getItemProperty("file").getValue();
						InputStream input = new FileInputStream(new File(uploadPath + file));
						String type = FileUtils.getFileExtension(file);

						Attachment att = taskService.createAttachment(type, taskCurrent.getId(),
								processInsId, ten, moTa, input);
						relatedFeildGroup.clear();
						Map<String, Object> data = (Map<String, Object>) addRelatedWindow.getData();
						Upload upload = (Upload) data.get("upload");
						upload.setVisible(true);
						Label lbl = (Label) data.get("lbl");
						lbl.setVisible(false);
						if (att != null) {
							CssLayout item = initAttachmentItem(att);
							if (item != null && attContainer != null) {
								attContainer.addComponent(item, 1);
								attCountNum++;
								attCount.setValue(attCountNum + " Tệp đính kèm");
							}
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
					Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra", Type.ERROR_MESSAGE);
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
				}
			} else if (BTN_ADD_RELATED_ID.equals(button.getId())) {
				if (!relatedLoaded) {
					initAddRelatedWindow();
					relatedLoaded = true;
				}
				UI.getCurrent().addWindow(addRelatedWindow);
			} else if (BTN_REMOVE_RELATED_ID.equals(button.getId())) {
				UI.getCurrent().addWindow(removeUserDialog);
				Map<String, Object> map = (Map<String, Object>) button.getData();
				removeUser = (User) map.get("USER");
				removeType = (String) map.get("TYPE");
			} else if (removeUserDialog.getBtnOkId().equals(button.getId())) {
				if (removeUser != null && removeType != null) {
					if (removeUser != null && removeType != null) {
						taskService.deleteUserIdentityLink(taskCurrent.getId(), removeUser.getId(), removeType);
						refreshRelatedUser(relatedUserLayout, lblRelatedUser, seeMoreUser);
						taskEventLoaded = false;
					}

				}
			} else if (BTN_HISTORY_ID.equals(button.getId())) {
				if (taskCurrent != null && processInsId != null) {
					String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
					UI.getCurrent().getPage().open(contextPath + "#!" + MyProcessInstanceDetailView.VIEW_NAME + "/"
							+ Constants.ACTION_VIEW + "/" + processInsId, "_blank");
				}
			} else if (BTN_TASK_ENVENT_ID.equals(button.getId())) {
				if (!taskEventLoaded) {
					initTaskEventContent();
					taskEventLoaded = true;
				}
				UI.getCurrent().addWindow(taskEventWindow);
			} else if (BTN_ADD_RELATED_USER_ID.equals(button.getId())) {
				if (!relatedUserLoaded) {
					initRelatedUserWindow();
					relatedUserLoaded = true;
				}
				UI.getCurrent().addWindow(involUserWindow);
			} else if (BTN_REASSIGN_ID.equals(button.getId()) || BTN_TRANFER_ID.equals(button.getId())) {
				if (!selectUserLoaded) {
					initSelectUserWindow();
					selectUserLoaded = true;
				}
				UI.getCurrent().addWindow(selectUserWindow);
				action = button.getId();
			} else if (BTN_DEL_COMMENT_ID.equals(button.getId())) {
				UI.getCurrent().addWindow(removeCommentDialog);
				removeCommentDialog.setCurrentData(button.getData());
				
			} else if (removeCommentDialog.getBtnOkId().equals(button.getId())) {
				if(removeCommentDialog.getCurrentData() != null && removeCommentDialog.getCurrentData() instanceof org.activiti.engine.task.Comment){
					org.activiti.engine.task.Comment comment = (Comment) removeCommentDialog.getCurrentData();
					taskService.deleteComment(comment.getId());
					refreshCommentWrap(CommonUtils.getUserLogedIn());
					removeCommentDialog.setCurrentData(null);
				}
			} else if (BUTTON_UPDATE_USER_ID.equals(button.getId())) {

				selectUserWindow.close();

				if (BTN_REASSIGN_ID.equals(action)) {
					List<User> uList = (List<User>) button.getData();
					if (uList != null && !uList.isEmpty()) {
						User u = uList.get(0);
						taskService.setAssignee(taskCurrent.getId(), u.getId());
						
						Notification notf = new Notification("Thông báo", "Assign thành công");
						notf.setDelayMsec(3000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
						
						if (u.getId().equals(CommonUtils.getUserLogedIn())) {
							UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+taskCurrent.getId());
						} else {
							UI.getCurrent().getNavigator().navigateTo(InboxView.VIEW_NAME);
						}
					}
				} else if (BTN_TRANFER_ID.equals(action)) {
					List<User> uList = (List<User>) button.getData();
					if (uList != null && !uList.isEmpty()) {
						User u = uList.get(0);
						taskService.setOwner(taskCurrent.getId(), u.getId());
						lblOwner.setValue(u.getFirstName() + " " + u.getLastName());
					}
				}

				refreshRelatedUser(relatedUserLayout, lblRelatedUser, seeMoreUser);
				taskEventLoaded = false;

			} else if (BUTTON_UPDATE_INVOL_USER_ID.equals(button.getId())) {
				involUserWindow.close();
				Map<String, Object> uMap = (Map<String, Object>) button.getData();
				if (uMap != null && !uMap.isEmpty()) {
					for (Map.Entry<String, Object> entry : uMap.entrySet()) {
						List<User> uList = (List<User>) entry.getValue();
						String key = entry.getKey();
						if (uList != null && !uList.isEmpty()) {
							if ("contributor".equals(key)) {
								for (User u : uList) {
									taskService.addUserIdentityLink(taskCurrent.getId(), u.getId(), "Contributor");
								}
							} else if ("manager".equals(key)) {
								for (User u : uList) {

									taskService.addUserIdentityLink(taskCurrent.getId(), u.getId(), "Manager");
								}
							} else if ("implementer".equals(key)) {
								for (User u : uList) {
									taskService.addUserIdentityLink(taskCurrent.getId(), u.getId(), "Implementer");
								}
							} else if ("sponsor".equals(key)) {
								for (User u : uList) {
									taskService.addUserIdentityLink(taskCurrent.getId(), u.getId(), "Sponsor");
								}
							}
						}
					}
					refreshRelatedUser(relatedUserLayout, lblRelatedUser, seeMoreUser);
					taskEventLoaded = false;

				}
			}else if (BTN_CLAIM_ID.equals(button.getId())) {
				try {
					taskService.claim(taskCurrent.getId(), CommonUtils.getUserLogedIn());

					button.setVisible(false);
					taskCurrent = taskService.createTaskQuery().taskId(taskCurrent.getId()).singleResult();
					
					Notification notf = new Notification("Thông báo", "Claim thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					
					UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+taskCurrent.getId());
				} catch (Exception e) {
					e.printStackTrace();
					Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra!", Notification.Type.ERROR_MESSAGE);
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
				}
			}else if (BTN_ADDNEW_ID.equals(button.getId())) {
				UI.getCurrent().addWindow(newTaskWindow);
			}else if(BUTTON_ADD_NEWTASK_ID.equals(button.getId())){
				if(taskCurrent != null && newTaskFeildGroup.isValid()){
					newTaskFeildGroup.commit();
					String name = (String) newTaskItem.getItemProperty("name").getValue();
					String assign = (String) newTaskItem.getItemProperty("assign").getValue();
					String description = (String) newTaskItem.getItemProperty("description").getValue();
					
					Task newTask = taskService.newTask();
			        newTask.setParentTaskId(taskCurrent.getId());
			        newTask.setName(name);
			        newTask.setDescription(description);
			        newTask.setAssignee(assign);
			        
			        
			        TaskInfo superParent = ProcessInstanceUtils.getParentTaskIdInProcess(taskCurrent.getId());
			        newTask.setFormKey(superParent.getFormKey());
			        taskService.saveTask(newTask);
			       
			        
			        //add Candidate User, group
			        
			        newTaskWindow.close();
			        newTaskFeildGroup.clear();
			        Notification notf = new Notification("Thông báo", "Tạo subtask thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					refreshSubTaskContainer();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra!", Notification.Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
		}
	}

}

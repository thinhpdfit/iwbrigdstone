package com.eform.ui.view.process;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.IdentityLinkType;
import org.activiti.image.ProcessDiagramGenerator;
import org.activiti.image.impl.DefaultProcessDiagramGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.core.context.SecurityContextHolder;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.EFormBoHoSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.StringUtils;
import com.eform.model.entities.HoSo;
import com.eform.security.custom.ActivitiUserDetails;
import com.eform.service.BieuMauService;
import com.eform.service.HoSoService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.form.FormGenerate;
import com.eform.ui.view.dashboard.DashboardView;
import com.eform.ui.view.task.TaskListView;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = HistoricTaskDetailView.VIEW_NAME)
public class HistoricTaskDetailView extends VerticalLayout implements View, ClickListener {
	private static final long serialVersionUID = 1L;

	private final int WIDTH_FULL = 100;
	public static final String VIEW_NAME = "historic-task";
	private static final String MAIN_TITLE = "Archive detail";
	private static final String SMALL_TITLE = "";
	private static final String BTN_INVOL_ID = "btnInvol";
	private static final String BTN_ASSIGN_ID = "btnAssign";
	private static final String BTN_REMOVE_USER_ID = "btnRemoveUser";
	private static final String BTN_TRANSFER_ID = "btnTransfer";
	private static final String BUTTON_UPDATE_USER_ID = "btnUpdateUser";
	private static final String BUTTON_UPDATE_INVOL_USER_ID = "btnUpdateInvolUser";
	private static final String BUTTON_OK_ID = "btnOK";
	private static final String BUTTON_CANCEL_ID = "btnCancel";
	private static final String BUTTON_COMPLETE_ID = "btnComplete";
	private static final String BUTTON_SAVE_ID = "btnSave";
	private static final String BTN_CMT_ID = "btnCmt";
	private static final String BTN_PRO_CMT_ID = "btnProCmt";
	private static final String BTN_VIEW_FORM = "btnViewForm";
	private CssLayout mainTitleWrap;
	private CssLayout mainWrap;
	private CssLayout basicInfoWrap;

	private HistoricTaskInstance taskCurrent;
	private String action;
	private FormGenerate formGenerate;
	private Table userInvolTable;
	private Label lblAssignee;
	private Label lblOwner;
	private Grid gridUser;
	private Grid gridContributorUser;
	private Grid gridImplementerUser;
	private Grid gridManagerUser;
	private Grid gridSponsorUser;
	private CssLayout commentContainer;
	private CssLayout commentProContainer;

	private Window selectUserWindow;
	private Window involUserWindow;
	private Window removeUserConfirmWindow;

	private User removeUser;
	private String removeType;
	private HoSo hoSo;
	private Boolean isUpdate;
	private Boolean hasEForm;

	private CssLayout tabCommentContent;
	private CssLayout tabProcessComment;
	private TextField txtCommentBox;
	private TextField txtProCommentBox;

	private HistoricTaskDetailView currentView;
	private Window viewFormData;

	@Autowired
	private TaskService taskService;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	 protected transient FormService formService;

	@Autowired
	private BieuMauService bieuMauService;
	@Autowired
	private HoSoService hoSoService;
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		isUpdate = false;
		hasEForm = false;
		if (params != null) {
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_:]+))*");
			Matcher m = p.matcher(params.toString());
			if (m.matches()) {
				action = m.group(1);
				String id = m.group(3);
				taskCurrent = historyService.createHistoricTaskInstanceQuery().taskId(id).singleResult();

				if (taskCurrent != null) {
					List<HoSo> hoSoList = hoSoService.getHoSoFind(null, null, null, taskCurrent.getProcessInstanceId(),
							0, 1);
					if (hoSoList != null && !hoSoList.isEmpty()) {
						hoSo = hoSoList.get(0);
						isUpdate = true;
					}
				}
				if (hoSo == null) {
					hoSo = new HoSo();
				}
				initBasicInfo();
				initMainTab();
				initSelectUserWindow();
			}
		}
	}

	@PostConstruct
	void init() {
		messageSourceAccessor= new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());

		currentView = this;
		mainWrap = new CssLayout();
		mainWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		mainWrap.setStyleName(ExplorerLayout.MAIN_COTAINER);
		initMainTitle();
		initViewFormData();
		addComponent(mainWrap);
	}

	public void initMainTitle() {
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSourceAccessor.getMessage(Messages.UI_VIEW_HOME));
		home.setViewName(DashboardView.VIEW_NAME);
			
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
	
	public void initViewFormData(){
		HorizontalLayout wrap = new HorizontalLayout();
		wrap.setWidth("100%");
		wrap.setSpacing(true);
		
		viewFormData = new Window("Dữ liệu");
		viewFormData.setWidth(1000, Unit.PIXELS);
		viewFormData.setResizable(true);
		viewFormData.center();
		viewFormData.setHeight("500px");
		viewFormData.setModal(true);
		viewFormData.setContent(wrap);
	}

	public void initBasicInfo() {
		basicInfoWrap = new CssLayout();
		basicInfoWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		basicInfoWrap.setStyleName(ExplorerLayout.INFO_WRAP);
		mainWrap.addComponent(basicInfoWrap);

		CssLayout nameWrap = new CssLayout();
		nameWrap.setStyleName(ExplorerLayout.NAME_WRAP);
		
		Label lblTaskName = new Label(taskCurrent.getName());
		lblTaskName.setStyleName(ExplorerLayout.DETAIL_TITLE);
		nameWrap.addComponent(lblTaskName);
		basicInfoWrap.addComponent(nameWrap);

		CssLayout info = new CssLayout();
		info.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		info.setStyleName(ExplorerLayout.INFO_CONTAINER);
		basicInfoWrap.addComponent(info);

		Label lblDueDateTitle = new Label();
		lblDueDateTitle.setStyleName(ExplorerLayout.INFO_ITEM);
		lblDueDateTitle.setIcon(FontAwesome.BELL);
		SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
		if (taskCurrent.getDueDate() != null) {
			lblDueDateTitle.setCaption("Thời hạn: " + df.format(taskCurrent.getDueDate()));
		} else {
			lblDueDateTitle.setCaption("Không có thời hạn");
		}

		info.addComponent(lblDueDateTitle);

		Label lblPriorityTitle = new Label();
		lblPriorityTitle.setStyleName(ExplorerLayout.INFO_ITEM);
		lblPriorityTitle.setIcon(FontAwesome.SORT_ALPHA_ASC);
		lblPriorityTitle.setCaption("Mức độ ưu tiên: "+taskCurrent.getPriority());

		info.addComponent(lblPriorityTitle);

		Label lblCreateDate = new Label();
		lblCreateDate.setStyleName(ExplorerLayout.INFO_ITEM);
		lblCreateDate.setIcon(FontAwesome.CLOCK_O);
		lblCreateDate.setCaption("Thời gian tạo: " + df.format(taskCurrent.getCreateTime()));

		info.addComponent(lblCreateDate);
	}

	public void initMainTab() {
		TabSheet tabSheet = new TabSheet();
		tabSheet.addStyleName(ExplorerLayout.TABSHEET);

		CssLayout tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);
		initTabBieuMau(tabContent);
		tabSheet.addTab(tabContent, "Biểu mẫu");

		tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);
		initTabPeople(tabContent);
		tabSheet.addTab(tabContent, "Liên quan");

		tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);
		if(initTabDesc(tabContent)){
			tabSheet.addTab(tabContent, "Mô tả");
		}

		tabCommentContent = new CssLayout();
		tabCommentContent.addStyleName(ExplorerLayout.TAB_CONTENT);
		initTabComments(tabCommentContent);
		tabSheet.addTab(tabCommentContent, "Task Events");
		tabSheet.addComponent(tabCommentContent);
		
		tabProcessComment = new CssLayout();
		tabProcessComment.addStyleName(ExplorerLayout.TAB_CONTENT);
		initTabProcessComments(tabProcessComment);
		tabSheet.addTab(tabProcessComment, "Comments");
		tabSheet.addComponent(tabProcessComment);

		tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);
		initTabHistory(tabContent);
		tabSheet.addTab(tabContent, "Lịch sử");
		mainWrap.addComponent(tabSheet);
	}

	public void initSelectUserWindow() {
		removeUserConfirmWindow = new Window();
		removeUserConfirmWindow = new Window("Thông báo");
		removeUserConfirmWindow.setWidth(240, Unit.PIXELS);
		removeUserConfirmWindow.setHeight(170, Unit.PIXELS);
		removeUserConfirmWindow.center();
		removeUserConfirmWindow.setModal(true);

		VerticalLayout confirmContainer = new VerticalLayout();
		confirmContainer.setMargin(true);
		confirmContainer.setSpacing(true);

		Label lbl = new Label();
		lbl.setValue("Bạn chắc chắn?");
		lbl.setStyleName(ExplorerLayout.LBL_CONFIRM);
		confirmContainer.addComponent(lbl);

		CssLayout buttons = new CssLayout();
		buttons.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		buttons.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);
		Button btnOk = new Button("OK");
		btnOk.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnOk.setId(BUTTON_OK_ID);
		btnOk.addClickListener(this);
		buttons.addComponent(btnOk);

		Button btnCancel = new Button("Cancel");
		btnCancel.setId(BUTTON_CANCEL_ID);
		btnCancel.addClickListener(this);
		buttons.addComponent(btnCancel);

		confirmContainer.addComponent(buttons);
		removeUserConfirmWindow.setContent(confirmContainer);

		VerticalLayout windowContent = new VerticalLayout();
		windowContent.setMargin(true);
		windowContent.setSpacing(true);

		gridUser = new Grid();

		Button btnUpdateCell = new Button("Cập nhật");
		btnUpdateCell.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnUpdateCell.setId(BUTTON_UPDATE_USER_ID);

		List<User> userList = identityService.createUserQuery().list();
		BeanItemContainer<User> userContainer = new BeanItemContainer<User>(User.class, userList);

		gridUser.setWidth("100%");
		gridUser.setContainerDataSource(userContainer);
		gridUser.setSelectionMode(SelectionMode.SINGLE);
		gridUser.setColumnOrder("id", "firstName", "lastName");
		gridUser.setColumns("id", "firstName", "lastName");
		gridUser.getColumn("id").setHeaderCaption("Id");
		gridUser.getColumn("firstName").setHeaderCaption("Họ đệm");
		gridUser.getColumn("lastName").setHeaderCaption("Tên");

		gridUser.addSelectionListener(selectionEvent -> {
			Object selected = gridUser.getSelectionModel().getSelectedRows();
			if (selected != null) {
				btnUpdateCell.setData(selected);
			}
		});

		btnUpdateCell.addClickListener(this);
		CssLayout btnWrap = new CssLayout();
		btnWrap.setWidth("100%");
		btnWrap.addStyleName(ExplorerLayout.BTN_POPUP_GROUP_CENTER);
		btnWrap.addComponent(btnUpdateCell);

		windowContent.addComponent(gridUser);
		windowContent.addComponent(btnWrap);

		selectUserWindow = new Window("Người dùng");
		selectUserWindow.setWidth(700, Unit.PIXELS);
		selectUserWindow.setHeight(600, Unit.PIXELS);
		selectUserWindow.center();
		selectUserWindow.setModal(true);
		selectUserWindow.setContent(windowContent);

		VerticalLayout windowInvolContent = new VerticalLayout();
		windowInvolContent.setMargin(true);
		windowInvolContent.setSpacing(true);

		TabSheet tabSheetInvol = new TabSheet();
		tabSheetInvol.addStyleName(ExplorerLayout.TABSHEET);

		CssLayout tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);

		gridContributorUser = new Grid();

		tabContent.addComponent(gridContributorUser);
		tabSheetInvol.addTab(tabContent, "Contributor");

		Button btnUpdateUser = new Button("Cập nhật");
		Map<String, Object> map = new HashMap();
		btnUpdateUser.setData(map);
		btnUpdateUser.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnUpdateUser.setId(BUTTON_UPDATE_INVOL_USER_ID);

		gridContributorUser.setWidth("100%");
		gridContributorUser.setContainerDataSource(userContainer);
		gridContributorUser.setSelectionMode(SelectionMode.MULTI);
		gridContributorUser.setColumnOrder("id", "firstName", "lastName");
		gridContributorUser.setColumns("id", "firstName", "lastName");
		gridContributorUser.getColumn("id").setHeaderCaption("Id");
		gridContributorUser.getColumn("firstName").setHeaderCaption("Họ đệm");
		gridContributorUser.getColumn("lastName").setHeaderCaption("Tên");

		gridContributorUser.addSelectionListener(selectionEvent -> {
			Object selected = gridContributorUser.getSelectionModel().getSelectedRows();
			if (selected != null) {
				map.put("contributor", selected);
			}
		});

		tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);

		gridImplementerUser = new Grid();

		tabContent.addComponent(gridImplementerUser);
		tabSheetInvol.addTab(tabContent, "Implementer");

		gridImplementerUser.setWidth("100%");
		gridImplementerUser.setContainerDataSource(userContainer);
		gridImplementerUser.setSelectionMode(SelectionMode.MULTI);
		gridImplementerUser.setColumnOrder("id", "firstName", "lastName");
		gridImplementerUser.setColumns("id", "firstName", "lastName");
		gridImplementerUser.getColumn("id").setHeaderCaption("Id");
		gridImplementerUser.getColumn("firstName").setHeaderCaption("Họ đệm");
		gridImplementerUser.getColumn("lastName").setHeaderCaption("Tên");

		gridImplementerUser.addSelectionListener(selectionEvent -> {
			Object selected = gridImplementerUser.getSelectionModel().getSelectedRows();
			if (selected != null) {
				map.put("implementer", selected);
			}
		});

		tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);

		gridManagerUser = new Grid();

		tabContent.addComponent(gridManagerUser);
		tabSheetInvol.addTab(tabContent, "Manager");

		gridManagerUser.setWidth("100%");
		gridManagerUser.setContainerDataSource(userContainer);
		gridManagerUser.setSelectionMode(SelectionMode.MULTI);
		gridManagerUser.setColumnOrder("id", "firstName", "lastName");
		gridManagerUser.setColumns("id", "firstName", "lastName");
		gridManagerUser.getColumn("id").setHeaderCaption("Id");
		gridManagerUser.getColumn("firstName").setHeaderCaption("Họ đệm");
		gridManagerUser.getColumn("lastName").setHeaderCaption("Tên");

		gridManagerUser.addSelectionListener(selectionEvent -> {
			Object selected = gridManagerUser.getSelectionModel().getSelectedRows();
			if (selected != null) {
				map.put("manager", selected);
			}
		});

		tabContent = new CssLayout();
		tabContent.addStyleName(ExplorerLayout.TAB_CONTENT);

		gridSponsorUser = new Grid();

		tabContent.addComponent(gridSponsorUser);
		tabSheetInvol.addTab(tabContent, "Sponsor");

		gridSponsorUser.setWidth("100%");
		gridSponsorUser.setContainerDataSource(userContainer);
		gridSponsorUser.setSelectionMode(SelectionMode.MULTI);
		gridSponsorUser.setColumnOrder("id", "firstName", "lastName");
		gridSponsorUser.setColumns("id", "firstName", "lastName");
		gridSponsorUser.getColumn("id").setHeaderCaption("Id");
		gridSponsorUser.getColumn("firstName").setHeaderCaption("Họ đệm");
		gridSponsorUser.getColumn("lastName").setHeaderCaption("Tên");

		gridSponsorUser.addSelectionListener(selectionEvent -> {
			Object selected = gridSponsorUser.getSelectionModel().getSelectedRows();
			if (selected != null) {
				map.put("sponsor", selected);
			}
		});

		btnUpdateUser.addClickListener(this);
		CssLayout btnUpdateWrap = new CssLayout();
		btnUpdateWrap.setWidth("100%");
		btnUpdateWrap.addStyleName(ExplorerLayout.BTN_POPUP_GROUP_CENTER);
		btnUpdateWrap.addComponent(btnUpdateUser);

		windowInvolContent.addComponent(tabSheetInvol);
		windowInvolContent.addComponent(btnUpdateWrap);

		involUserWindow = new Window("Người dùng");
		involUserWindow.setWidth(700, Unit.PIXELS);
		involUserWindow.setHeight(600, Unit.PIXELS);
		involUserWindow.center();
		involUserWindow.setModal(true);
		involUserWindow.setContent(windowInvolContent);
	}

	public void initTabBieuMau(CssLayout tabContent) {
		try {
			if (taskCurrent != null) {
				String formKey = taskCurrent.getFormKey();

				if (formKey != null) {
					Pattern p = Pattern.compile("([0-9a-zA-Z_-]+)#([0-9a-zA-Z_-]+)");
					Matcher m = p.matcher(formKey);
					if(m.matches()){
						formKey = m.group(1);
					}
					
					String hoSoXml = null;
					if (hoSo != null) {
						String boHoSoXml = hoSo.getHoSoGiaTri();
						EFormBoHoSo boHS = CommonUtils.getEFormBoHoSo(boHoSoXml);
						//TODO view form data history
						if(boHS != null && boHS.getLichSuList() != null && !boHS.getLichSuList().isEmpty()){
							for(EFormHoSo hs : boHS.getLichSuList()){
								if(hs != null && taskCurrent.getId().equals(hs.getTaskId())){
									hoSoXml = CommonUtils.getEFormHoSoString(hs);
									break;
								}
							}
							
 						}
					}
					formGenerate = new FormGenerate(formKey, hoSoXml);
					formGenerate.setWidth("100%");
					tabContent.addComponent(formGenerate);
					hasEForm = true;

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!hasEForm) {
			Label label = new Label("Không có biểu mẫu!");
			label.setStyleName(ExplorerLayout.LBL_EMPTY_TAB);
			tabContent.addComponent(label);
		}
	}

	public void initTabPeople(CssLayout tabContent) {
		try {
			if (taskCurrent != null) {
				CssLayout wrap = new CssLayout();
				wrap.setStyleName(ExplorerLayout.PEOPLE_TAB_WRAP);

				CssLayout ownerWrap = new CssLayout();
				ownerWrap.setStyleName(ExplorerLayout.PEOPLE_TAB_OWNER);
				Label lblOwnerTtl = new Label("Owner");
				lblOwnerTtl.setIcon(FontAwesome.STREET_VIEW);
				lblOwnerTtl.setStyleName(ExplorerLayout.PEOPLE_OWNER_LBL);
				ownerWrap.addComponent(lblOwnerTtl);

				lblOwner = new Label();
				lblOwner.setStyleName(ExplorerLayout.PEOPLE_OWNER_NAME);
				if (taskCurrent.getOwner() != null) {
					User u = identityService.createUserQuery().userId(taskCurrent.getOwner()).singleResult();
					lblOwner.setCaption(u.getFirstName() + " " + u.getLastName());
				} else {
					lblOwner.setCaption("No owner");
				}
				ownerWrap.addComponent(lblOwner);

				wrap.addComponent(ownerWrap);

				CssLayout assigneeWrap = new CssLayout();
				assigneeWrap.setStyleName(ExplorerLayout.PEOPLE_TAB_ASSIGN);
				Label lblAssigneeTtl = new Label("Assignee");
				lblAssigneeTtl.setIcon(FontAwesome.HAND_PAPER_O);
				lblAssigneeTtl.setStyleName(ExplorerLayout.PEOPLE_ASSIGNEE_LBL);
				assigneeWrap.addComponent(lblAssigneeTtl);

				lblAssignee = new Label();
				lblAssignee.setStyleName(ExplorerLayout.PEOPLE_ASSIGNEE_NAME);
				if (taskCurrent.getAssignee() != null) {
					User u = identityService.createUserQuery().userId(taskCurrent.getAssignee()).singleResult();
					lblAssignee.setCaption(u.getFirstName() + " " + u.getLastName());
				} else {
					lblAssignee.setCaption("Chưa được assign");
				}
				assigneeWrap.addComponent(lblAssignee);
				wrap.addComponent(assigneeWrap);

				tabContent.addComponent(wrap);

				CssLayout involWrap = new CssLayout();
				involWrap.setStyleName(ExplorerLayout.PEOPLE_TAB_INVOL);
				Label lblInvol = new Label("Người dùng khác");
				lblInvol.setIcon(FontAwesome.USER);
				lblInvol.setStyleName(ExplorerLayout.PEOPLE_INVOL_LBL);
				involWrap.addComponent(lblInvol);
				wrap.addComponent(involWrap);

				List<IdentityLink> identityLinks = taskService.getIdentityLinksForTask(taskCurrent.getId());
				if (identityLinks != null && !identityLinks.isEmpty()) {
					userInvolTable = new Table();
					userInvolTable.addStyleName(ExplorerLayout.DATA_TABLE);
					userInvolTable.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
					userInvolTable.addContainerProperty("icon", Label.class, null);
					userInvolTable.addContainerProperty("user", String.class, null);
					userInvolTable.addContainerProperty("role", String.class, null);
					userInvolTable.addContainerProperty("action", Button.class, null);
					boolean b = false;
					for (final IdentityLink identityLink : identityLinks) {
						if (identityLink.getUserId() != null) {
							if (!IdentityLinkType.ASSIGNEE.equals(identityLink.getType())
									&& !IdentityLinkType.OWNER.equals(identityLink.getType())) {
								b = true;
								User u = identityService.createUserQuery().userId(identityLink.getUserId())
										.singleResult();
								Label lbl = new Label();
								lbl.setIcon(FontAwesome.USER);

								Button btn = new Button();
								btn.setId(BTN_REMOVE_USER_ID);
								btn.addStyleName(ExplorerLayout.BUTTON_ACTION);
								btn.addStyleName(Reindeer.BUTTON_LINK);
								btn.setIcon(FontAwesome.REMOVE);
								btn.addClickListener(this);
								Map<String, Object> data = new HashMap();
								data.put("USER", u);
								data.put("TYPE", identityLink.getType());
								btn.setData(data);
								userInvolTable.addItem(new Object[] { lbl, u.getFirstName() + " " + u.getLastName(),
										identityLink.getType(), btn }, u.getId());
							}
						}
					}
					userInvolTable.setVisible(b);
					tabContent.addComponent(userInvolTable);
					if (!b) {
						Label label = new Label("Không có người dùng liên quan khác");
						userInvolTable.setData(label);
						label.setStyleName(ExplorerLayout.LBL_EMPTY_TAB);
						tabContent.addComponent(label);
					}
				}
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Label label = new Label("Không có người dùng liên quan!");
		label.setStyleName(ExplorerLayout.LBL_EMPTY_TAB);
		tabContent.addComponent(label);
	}

	public boolean initTabDesc(CssLayout tabContent) {
		try {
			if (taskCurrent != null) {
				if (taskCurrent.getDescription() != null) {
					Label label = new Label(taskCurrent.getDescription());
					label.setStyleName(ExplorerLayout.TASK_DESC);
					tabContent.addComponent(label);
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Label label = new Label("Không có mô tả!");
		label.setStyleName(ExplorerLayout.LBL_EMPTY_TAB);
		tabContent.addComponent(label);
		return false;
	}

	public void initTabComments(CssLayout tabContent) {
		try {
			commentContainer = new CssLayout();
			commentContainer.setStyleName(ExplorerLayout.COMMENTS_CONTAINER);

			List<org.activiti.engine.task.Event> taskEvents = taskService.getTaskEvents(taskCurrent.getId());
			List<org.activiti.engine.task.Comment> taskComments = taskService.getTaskComments(taskCurrent.getId());
			List<org.activiti.engine.task.Comment> processInstanceComments = taskService.getProcessInstanceComments(taskCurrent.getProcessInstanceId());
			boolean eventExisted = false;
			for (final org.activiti.engine.task.Event event : taskEvents) {
				CssLayout item = createCommentItem(event);
				if(item != null){
					eventExisted = true;
					commentContainer.addComponent(item);
				}
				
			}
			if(eventExisted){
				tabContent.addComponent(commentContainer);
				return;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		Label label = new Label("No events");
		label.setStyleName(ExplorerLayout.LBL_EMPTY_TAB);
		tabContent.addComponent(label);

	}
	
	public void initTabProcessComments(CssLayout tabContent){
		try {
			commentProContainer = new CssLayout();
			commentProContainer.setStyleName(ExplorerLayout.COMMENTS_CONTAINER);

			List<Comment> processInstanceComments = taskService.getProcessInstanceComments(taskCurrent.getProcessInstanceId());
			boolean cmtExisted = false;
			if(processInstanceComments != null && !processInstanceComments.isEmpty()){
				cmtExisted = true;
			}
			for (final Comment cmt : processInstanceComments) {
				CssLayout item = createProCommentItem(cmt);
				if(item != null){
					commentProContainer.addComponent(item);
				}
				
			}
			if(cmtExisted){
				tabContent.addComponent(commentProContainer);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Label label = new Label("Không có comment!");
		label.setStyleName(ExplorerLayout.LBL_EMPTY_TAB);
		tabContent.addComponent(label);
	}

	public CssLayout createCommentItem(org.activiti.engine.task.Event event) {
		try {
			if (event.getUserId() != null && event.getProcessInstanceId() == null) {
				User user = identityService.createUserQuery().userId(event.getUserId()).singleResult();

				CssLayout commentItemWrap = new CssLayout();
				commentItemWrap.setStyleName(ExplorerLayout.CMT_ITEM);

				CssLayout cmtUser = new CssLayout();
				cmtUser.setStyleName(ExplorerLayout.USER_IMAGE_WRAP);
				commentItemWrap.addComponent(cmtUser);

				CssLayout userImg = new CssLayout();
				userImg.setStyleName(ExplorerLayout.USER_IMAGE);
				String sn = StringUtils.getShortName(user.getFirstName() + " " + user.getLastName());
				Label shortName = new Label(sn);
				shortName.setStyleName(ExplorerLayout.USER_SHORT_NAME);
				shortName.addStyleName(StringUtils.getBgClassName(sn));
				userImg.addComponent(shortName);
				cmtUser.addComponent(userImg);

				CssLayout commentInfoWrap = new CssLayout();
				commentInfoWrap.setStyleName(ExplorerLayout.COMMENTS_INFO_WRAP);
				commentItemWrap.addComponent(commentInfoWrap);

				CssLayout commentInfo = new CssLayout();
				commentInfo.setStyleName(ExplorerLayout.COMMENTS_INFO);
				commentInfoWrap.addComponent(commentInfo);

				CssLayout cmtTime = new CssLayout();
				cmtTime.setStyleName(ExplorerLayout.CMT_TIME);
				commentInfoWrap.addComponent(cmtTime);

				String lblUser = user.getFirstName() + " " + user.getLastName();

				SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
				Date date = event.getTime();
				Label lblTime = new Label(df.format(date));
				cmtTime.addComponent(lblTime);

				String content = "<span class='user-cmt-name'>" + lblUser + "</span> ";
				if (org.activiti.engine.task.Event.ACTION_ADD_USER_LINK.equals(event.getAction())) {
					User involvedUser = identityService.createUserQuery().userId(event.getMessageParts().get(0))
							.singleResult();
					content += "Involved <span class='cmt-highlight'>" + involvedUser.getFirstName() + " "
							+ involvedUser.getLastName() + "</span> as <span class='cmt-highlight'>"
							+ event.getMessageParts().get(1) + "</span>";
				} else if (org.activiti.engine.task.Event.ACTION_DELETE_USER_LINK.equals(event.getAction())) {
					User involvedUser = identityService.createUserQuery().userId(event.getMessageParts().get(0))
							.singleResult();
					content += "Removed <span class='cmt-highlight'>" + involvedUser.getFirstName() + " "
							+ involvedUser.getLastName() + "</span> as <span class='cmt-highlight'>"
							+ event.getMessageParts().get(1) + "</span>";
				} else if (org.activiti.engine.task.Event.ACTION_ADD_GROUP_LINK.equals(event.getAction())) {
					content += "Involved group <span class='cmt-highlight'>" + event.getMessageParts().get(0)
							+ "</span> as <span class='cmt-highlight'>" + event.getMessageParts().get(1) + "</span>";
				} else if (org.activiti.engine.task.Event.ACTION_DELETE_GROUP_LINK.equals(event.getAction())) {
					content += "Removed group <span class='cmt-highlight'>" + event.getMessageParts().get(0)
							+ "</span> as <span>" + event.getMessageParts().get(1) + "</span>";
				} else if (org.activiti.engine.task.Event.ACTION_ADD_ATTACHMENT.equals(event.getAction())) {
					content = "attached <span class='cmt-highlight'>" + event.getMessage() + "</span>";
				} else if (org.activiti.engine.task.Event.ACTION_DELETE_ATTACHMENT.equals(event.getAction())) {
					content += "Deleted attachment <span class='cmt-highlight'>" + event.getMessage() + "</span>";
				} else if (org.activiti.engine.task.Event.ACTION_ADD_COMMENT.equals(event.getAction())) {
					content += "<span class='cmt-highlight'>" + event.getMessage() + "</span>";
				} else {
					content += "<span class='cmt-highlight'>" + event.getMessage() + "</span>";
				}

				Label lblContent = new Label(content, ContentMode.HTML);
				commentInfo.addComponent(lblContent);

				return commentItemWrap;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public CssLayout createProCommentItem(org.activiti.engine.task.Comment comment) {
		try {
			if (comment.getUserId() != null) {
				User user = identityService.createUserQuery().userId(comment.getUserId()).singleResult();

				CssLayout commentItemWrap = new CssLayout();
				commentItemWrap.setStyleName(ExplorerLayout.CMT_ITEM);

				CssLayout cmtUser = new CssLayout();
				cmtUser.setStyleName(ExplorerLayout.USER_IMAGE_WRAP);
				commentItemWrap.addComponent(cmtUser);

				CssLayout userImg = new CssLayout();
				userImg.setStyleName(ExplorerLayout.USER_IMAGE);
				String sn = StringUtils.getShortName(user.getFirstName() + " " + user.getLastName());
				Label shortName = new Label(sn);
				shortName.setStyleName(ExplorerLayout.USER_SHORT_NAME);
				shortName.addStyleName(StringUtils.getBgClassName(sn));
				userImg.addComponent(shortName);
				cmtUser.addComponent(userImg);

				CssLayout commentInfoWrap = new CssLayout();
				commentInfoWrap.setStyleName(ExplorerLayout.COMMENTS_INFO_WRAP);
				commentItemWrap.addComponent(commentInfoWrap);

				CssLayout commentInfo = new CssLayout();
				commentInfo.setStyleName(ExplorerLayout.COMMENTS_INFO);
				commentInfoWrap.addComponent(commentInfo);

				CssLayout cmtTime = new CssLayout();
				cmtTime.setStyleName(ExplorerLayout.CMT_TIME);
				commentInfoWrap.addComponent(cmtTime);

				String lblUser = user.getFirstName() + " " + user.getLastName();

				SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
				Date date = comment.getTime();
				Label lblTime = new Label(df.format(date));
				cmtTime.addComponent(lblTime);

				String content = "<span class='user-cmt-name'>" + lblUser + "</span> ";
				
				content += "<span class='cmt-highlight'>" + comment.getFullMessage() + "</span>";
				

				Label lblContent = new Label(content, ContentMode.HTML);
				commentInfo.addComponent(lblContent);

				return commentItemWrap;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void initTabHistory(CssLayout tabContent) {
		try {
			if (taskCurrent != null && taskCurrent.getProcessInstanceId() != null) {
				try {
					ProcessInstance processInstanceCurrent = runtimeService.createProcessInstanceQuery()
							.processInstanceId(taskCurrent.getProcessInstanceId()).singleResult();

					CssLayout imageWrap = new CssLayout();
					imageWrap.setStyleName(ExplorerLayout.RPOC_IMAGE_WRAP);
					Image image = new Image();
					imageWrap.addComponent(image);

					BpmnModel bpmnModel = repositoryService
							.getBpmnModel(processInstanceCurrent.getProcessDefinitionId());

					ProcessDiagramGenerator generator = new DefaultProcessDiagramGenerator();
					InputStream is = generator.generateDiagram(bpmnModel, "png",
							runtimeService.getActiveActivityIds(processInstanceCurrent.getProcessInstanceId()));

					final StreamResource streamResource = new StreamResource(new StreamSource() {
						@Override
						public InputStream getStream() {
							return is;
						}
					}, processInstanceCurrent.getId());
					streamResource.setCacheTime(0);
					image.setSource(streamResource);

					tabContent.addComponent(imageWrap);
				} catch (Exception e) {
					System.err.println("Diagram resource not found!");
				}
				
				
				List<HistoricTaskInstance> tasks = historyService.createHistoricTaskInstanceQuery()
					      .processInstanceId(taskCurrent.getProcessInstanceId())
					      .orderByHistoricTaskInstanceEndTime().desc()
					      .orderByTaskCreateTime().desc()
					      .list();
				if (tasks != null && !tasks.isEmpty()) {
					Table table = new Table();
					BeanItemContainer<HistoricTaskInstance> container = new BeanItemContainer<HistoricTaskInstance>(HistoricTaskInstance.class, tasks);
					table.setContainerDataSource(container);
					table.addStyleName(ExplorerLayout.DATA_TABLE);
					table.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
					table.setColumnHeader("name", "Name");
					table.setColumnHeader("priority", "Priority");
					table.setColumnHeader("assignee", "Assignee");
					table.setColumnHeader("dueDate", "DueDate");
					table.setColumnHeader("startTime", "Created");
					table.setColumnHeader("endTime", "Completed");
					table.setColumnHeader("action", "Action");
					
					table.addGeneratedColumn("dueDate", new ColumnGenerator(){
						private static final long serialVersionUID = 1L;

						@Override
						public Object generateCell(Table source, Object itemId, Object columnId) {
							HistoricTaskInstance current = (HistoricTaskInstance) itemId;
							if(current != null && current.getDueDate() != null){
								SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
								return df.format(current.getDueDate());
							}
							return null;
						}
					});
					
					table.addGeneratedColumn("startTime", new ColumnGenerator(){
						private static final long serialVersionUID = 1L;

						@Override
						public Object generateCell(Table source, Object itemId, Object columnId) {
							HistoricTaskInstance current = (HistoricTaskInstance) itemId;
							if(current != null && current.getStartTime() != null){
								SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
								return df.format(current.getStartTime());
							}
							return null;
						}
					});
					
					table.addGeneratedColumn("endTime", new ColumnGenerator(){
						private static final long serialVersionUID = 1L;

						@Override
						public Object generateCell(Table source, Object itemId, Object columnId) {
							HistoricTaskInstance current = (HistoricTaskInstance) itemId;
							if(current != null && current.getEndTime() != null){
								SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
								return df.format(current.getEndTime());
							}
							return null;
						}
					});
					
					table.addGeneratedColumn("action", new ColumnGenerator(){
						private static final long serialVersionUID = 1L;

						@Override
						public Object generateCell(Table source, Object itemId, Object columnId) {
							HistoricTaskInstance current = (HistoricTaskInstance) itemId;
							if(current != null && current.getEndTime() != null){
								Button btn = new Button();
								btn.setId(BTN_VIEW_FORM);
								btn.addStyleName(ExplorerLayout.BUTTON_ACTION);
								btn.addStyleName(Reindeer.BUTTON_LINK);
								btn.setIcon(FontAwesome.EYE);
								btn.addClickListener(currentView);
								btn.setData(current);
								return btn;
							}
							return null;
						}
					});
					
					table.setRowHeaderMode(RowHeaderMode.INDEX);

					table.setVisibleColumns("name", "priority", "assignee", "dueDate", "startTime", "endTime", "action");
					tabContent.addComponent(table);
					return;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Label label = new Label("Không có lịch sử!");
		label.setStyleName(ExplorerLayout.LBL_EMPTY_TAB);
		tabContent.addComponent(label);
	}

	public void refreshTableInvol() {
		try {
			List<IdentityLink> identityLinks = taskService.getIdentityLinksForTask(taskCurrent.getId());
			if (identityLinks != null && !identityLinks.isEmpty()) {
				userInvolTable.clear();
				userInvolTable.removeAllItems();
				boolean b = false;
				for (final IdentityLink identityLink : identityLinks) {
					if (identityLink.getUserId() != null) {
						if (!IdentityLinkType.ASSIGNEE.equals(identityLink.getType())
								&& !IdentityLinkType.OWNER.equals(identityLink.getType())) {
							b = true;
							User u = identityService.createUserQuery().userId(identityLink.getUserId()).singleResult();
							Label lbl = new Label();
							lbl.setIcon(FontAwesome.USER);

							Button btn = new Button();
							btn.setId(BTN_REMOVE_USER_ID);
							btn.addStyleName(ExplorerLayout.BUTTON_ACTION);
							btn.addStyleName(Reindeer.BUTTON_LINK);
							btn.setIcon(FontAwesome.REMOVE);
							btn.addClickListener(this);
							Map<String, Object> data = new HashMap();
							data.put("USER", u);
							data.put("TYPE", identityLink.getType());
							btn.setData(data);
							userInvolTable.addItem(new Object[] { lbl, u.getFirstName() + " " + u.getLastName(),
									identityLink.getType(), btn }, u.getId());
						}
					}
				}
				userInvolTable.setVisible(b);
				if (userInvolTable.getData() != null) {
					Label label = (Label) userInvolTable.getData();
					label.setVisible(!b);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {

			Authentication.setAuthenticatedUserId(getUserName());
			Button button = event.getButton();
			if (BTN_ASSIGN_ID.equals(button.getId())) {
				action = "assign";
				UI.getCurrent().addWindow(selectUserWindow);
			} else if (BTN_TRANSFER_ID.equals(button.getId())) {
				action = "transfer";
				UI.getCurrent().addWindow(selectUserWindow);
			} else if (BTN_INVOL_ID.equals(button.getId())) {
				action = "invol";
				UI.getCurrent().addWindow(involUserWindow);
			} else if (BTN_REMOVE_USER_ID.equals(button.getId())) {
				Map<String, Object> map = (Map<String, Object>) button.getData();
				removeUser = (User) map.get("USER");
				removeType = (String) map.get("TYPE");
				UI.getCurrent().addWindow(removeUserConfirmWindow);
			} else if (BUTTON_OK_ID.equals(button.getId())) {
				removeUserConfirmWindow.close();
				if (removeUser != null && removeType != null) {
					taskService.deleteUserIdentityLink(taskCurrent.getId(), removeUser.getId(), removeType);
					refreshTableInvol();
				}

			} else if (BUTTON_CANCEL_ID.equals(button.getId())) {
				removeUserConfirmWindow.close();
			}else if (BTN_VIEW_FORM.equals(button.getId())) {
				HorizontalLayout content = (HorizontalLayout) viewFormData.getContent();
				HistoricTaskInstance current = (HistoricTaskInstance) button.getData();
				
				List<HoSo> hoSoList = hoSoService.getHoSoFind(null, null, null, current.getProcessInstanceId(), 0, 1);
				if (hoSoList != null && !hoSoList.isEmpty()) {
					hoSo = hoSoList.get(0);
					String formKey = current.getFormKey();

					if (formKey != null) {
						Pattern p = Pattern.compile("([0-9a-zA-Z_-]+)#([0-9a-zA-Z_-]+)");
						Matcher m = p.matcher(formKey);
						if(m.matches()){
							formKey = m.group(1);
						}
						String hoSoXml = null;
						if (hoSo != null) {
							String boHoSoXml = hoSo.getHoSoGiaTri();
							EFormBoHoSo boHS = CommonUtils.getEFormBoHoSo(boHoSoXml);
							//TODO view form data history
							if(boHS != null && boHS.getLichSuList() != null && !boHS.getLichSuList().isEmpty()){
								for(EFormHoSo hs : boHS.getLichSuList()){
									if(hs != null && current.getId().equals(hs.getTaskId())){
										hoSoXml = CommonUtils.getEFormHoSoString(hs);
										break;
									}
								}
								FormGenerate form = new FormGenerate(formKey, hoSoXml);
								form.setWidth("100%");
								content.removeAllComponents();
								content.addComponent(form);
	 						}
						}
					}
					
				}
				
				
				UI.getCurrent().addWindow(viewFormData);
			}else if (BUTTON_UPDATE_USER_ID.equals(button.getId())) {
				selectUserWindow.close();
				
				if ("assign".equals(action)) {
					List<User> uList = (List<User>) button.getData();
					if (uList != null && !uList.isEmpty()) {
						User u = uList.get(0);
						taskService.setAssignee(taskCurrent.getId(), u.getId());
						if (u.getId().equals(getUserLogedIn())) {
							lblAssignee.setCaption(u.getFirstName() + " " + u.getLastName());
						} else {
							Notification notf = new Notification("Thông báo", "Assign thành công");
							notf.setDelayMsec(3000);
							notf.setPosition(Position.TOP_CENTER);
							notf.show(Page.getCurrent());
							UI.getCurrent().getNavigator().navigateTo(TaskListView.VIEW_NAME + "/" + TaskListView.INBOX_CATE);
						}
					}
				} else if ("transfer".equals(action)) {
					List<User> uList = (List<User>) button.getData();
					if (uList != null && !uList.isEmpty()) {
						User u = uList.get(0);
						taskService.setOwner(taskCurrent.getId(), u.getId());
						lblOwner.setCaption(u.getFirstName() + " " + u.getLastName());
					}
				}
				tabCommentContent.removeAllComponents();
				initTabComments(tabCommentContent);
			} else if (BUTTON_UPDATE_INVOL_USER_ID.equals(button.getId())) {
				involUserWindow.close();
				if ("invol".equals(action)) {
					Map<String, Object> uMap = (Map<String, Object>) button.getData();
					if (uMap != null && !uMap.isEmpty()) {
						for (Map.Entry<String, Object> entry : uMap.entrySet()) {
							List<User> uList = (List<User>) entry.getValue();
							String key = entry.getKey();
							if (uList != null && !uList.isEmpty()) {
								if ("contributor".equals(key)) {
									for (User u : uList) {
										taskService.addUserIdentityLink(taskCurrent.getId(), u.getId(), "Contributor");
									}
								} else if ("manager".equals(key)) {
									for (User u : uList) {

										taskService.addUserIdentityLink(taskCurrent.getId(), u.getId(), "Manager");
									}
								} else if ("implementer".equals(key)) {
									for (User u : uList) {
										taskService.addUserIdentityLink(taskCurrent.getId(), u.getId(), "Implementer");
									}
								} else if ("sponsor".equals(key)) {
									for (User u : uList) {
										taskService.addUserIdentityLink(taskCurrent.getId(), u.getId(), "Sponsor");
									}
								}
							}
						}

						refreshTableInvol();

						tabCommentContent.removeAllComponents();
						initTabComments(tabCommentContent);
					}

				}
			}else if (BTN_CMT_ID.equals(button.getId())) {
				try {
					if(txtCommentBox != null){
						String comment = txtCommentBox.getValue();
						if(comment != null && !comment.isEmpty()){
							taskService.addComment(taskCurrent.getId(), null, comment);
							tabCommentContent.removeAllComponents();
							initTabComments(tabCommentContent);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else if (BTN_PRO_CMT_ID.equals(button.getId())) {
				try {
					if(txtProCommentBox != null){
						String comment = txtProCommentBox.getValue();
						if(comment != null && !comment.isEmpty()){
							taskService.addComment(taskCurrent.getId(), taskCurrent.getProcessInstanceId(), comment);
							tabProcessComment.removeAllComponents();
							initTabProcessComments(tabProcessComment);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getUserLogedIn() {
		try {
			Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (obj != null) {
				ActivitiUserDetails user = (ActivitiUserDetails) obj;
				return user.getUsername();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public String getUserName(){
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(obj != null){
			ActivitiUserDetails user = (ActivitiUserDetails) obj;
			return user.getUsername();
		}
		return null;
	}

}

package com.eform.ui.view.system;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.ThamSo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.model.entities.HtThamSo;
import com.eform.service.HtThamSoService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = LogDetailView.VIEW_NAME)
public class LogDetailView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "server-log";
	private static final String MAIN_TITLE = "Server Log";
	private static final String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_REMOVE_ALL = "btn-remove-all";
	
	private CssLayout formContainer;
	private Button btnUpdate;
	
	private List<HtThamSo> htThamSoList;
	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();

	@Autowired
	private HtThamSoService htThamSoService;

	@Autowired
	private RepositoryService repositoryService;	
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private MessageSource messageSource;

    @PostConstruct
    void init() {
    	fieldGroup.setItemDataSource(itemValue);
    	initMainTitle();
    	List<Long> loai = new ArrayList<Long>();
    	loai.add(1L);
    	htThamSoList = htThamSoService.getHtThamSoFind(null, null, loai, -1, -1);
    }

    @Override
    public void enter(ViewChangeEvent event) {
    	UI.getCurrent().getPage().setTitle(MAIN_TITLE);
    	initDetailForm();
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
    
    public void initDetailForm(){
    	
    	formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	HtThamSo thamSo = htThamSoService.getHtThamSoFind(ThamSo.LOG_PATH);
    	Label lbl = new Label();
    	lbl.addStyleName("log-file");
    	lbl.setContentMode(ContentMode.HTML);
    	if(thamSo!=null && thamSo.getGiaTri() != null){
    		BufferedReader br = null;
    		FileReader fr = null;
    		try {
    			String sCurrentLine;
    			String log = "";
    			fr = new FileReader(thamSo.getGiaTri());
    			br = new BufferedReader(fr);
    			while ((sCurrentLine = br.readLine()) != null) {
    				log+=sCurrentLine+"<br/>";
    			}
    			lbl.setValue(log);
    		} catch (IOException e) {
    			e.printStackTrace();
    		} finally {
    			try {
    				if (br != null)br.close();
    				if (fr != null)fr.close();
    			} catch (IOException ex) {
    				ex.printStackTrace();
    			}
    		}
    	}
    	
    	formContainer.addComponent(lbl);
    	addComponent(formContainer);
    }

@SuppressWarnings("unchecked")
@Override
public void buttonClick(ClickEvent event) {
	
}
}
package com.eform.ui.view.report.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.eform.common.ThamSo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.SqlJsonBuilder;
import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BaoCaoThongKeService;
import com.eform.service.HoSoService;
import com.eform.service.HtThamSoService;
import com.eform.service.TruongThongTinService;
import com.eform.ui.view.report.type.FilterObject;

@Controller
@RequestMapping("/charts")
public class ChartsController {
	@Autowired
	private BaoCaoThongKeService baoCaoThongKeService;	
	@Autowired
	private HistoryService historyService;
	@Autowired
	private HoSoService hoSoService;
	@Autowired
	private TruongThongTinService truongThongTinService;
	@Autowired
	private HtThamSoService htThamSoService;
	
	@RequestMapping(value = "/review", method=RequestMethod.GET)
	public String test(HttpServletRequest request, Model model){
		Map<String, Object> chartDataMap = (Map<String, Object>) request.getSession().getAttribute("chartDataMap");
		if(chartDataMap != null && !chartDataMap.isEmpty()){
			Map<String, Object> params = (Map<String, Object>) chartDataMap.get("CHART_PARAMS");
			if(params != null && !params.isEmpty()){
				for(Map.Entry<String, Object> entry : params.entrySet()){
					model.addAttribute(entry.getKey(), entry.getValue());
				}
			}
			model.addAttribute("jsonData", chartDataMap.get("JSON"));
			model.addAttribute("chartType", chartDataMap.get("CHART_TYPE"));
			JSONArray jsonArr = new JSONArray();
			HtThamSo colorHtThamSo = htThamSoService.getHtThamSoFind(ThamSo.COLOR_LIST);
			if(colorHtThamSo != null && colorHtThamSo.getGiaTri() != null){
				String colorArr[] = colorHtThamSo.getGiaTri().split(",");
				if(colorArr != null  && colorArr.length >0){
					for(int i =0; i<colorArr.length ; i++){
						if(colorArr[i] != null){
							jsonArr.put(colorArr[i].trim());
						}
					}
				}

				model.addAttribute("colors", jsonArr.toString());
			}
			
		}
		
		return "chartdetail";
	}
	
	@RequestMapping(value = "/detail/{ma}", method=RequestMethod.GET)
	public String drawChart(@PathVariable String ma, HttpServletRequest request, Model model){
		if(ma != null){
			
			List<BaoCaoThongKe> bcList = baoCaoThongKeService.findByCode(ma);
			if(bcList != null && !bcList.isEmpty()){
				BaoCaoThongKe bc = bcList.get(0);
				JSONObject giaTri = bc.getGiaTri();
				JSONArray filterArr = giaTri.getJSONArray("filter");
				JSONArray groupArr = giaTri.getJSONArray("group");
				JSONArray fnArr = giaTri.getJSONArray("function");
					
				String chartType = bc.getKieuDoThi();
				Date fromDate = null;
				if(bc.getTuNgay() != null){
					fromDate = new Date(bc.getTuNgay().getTime());
				}
				Date toDate = null;
				if(bc.getDenNgay() != null){
					toDate = new Date(bc.getDenNgay().getTime());
				}
				String processDfId = bc.getProcessDefId();
				//process instance list get by process definition
				List<String> processInstanceIdList = new ArrayList<String>();
				if(processDfId != null){
					List<HistoricProcessInstance> historicProInsList = historyService.createHistoricProcessInstanceQuery().processDefinitionId(processDfId).list();
					if(historicProInsList != null && !historicProInsList.isEmpty()){
						for(HistoricProcessInstance hpi : historicProInsList){
							processInstanceIdList.add(hpi.getId());
						}
					}
				}
				
				List<String> chartDataKeyList = new ArrayList<String>();
				Map<String, String> aliasName = new HashMap<String, String>();
				List<FilterObject> filterList =  new ArrayList<FilterObject>();
				Map<String, TruongThongTin> truongThongTinKeyMap = new HashMap<String, TruongThongTin>(); 
				
				if(filterArr != null){
					for(int i =0; i< filterArr.length(); i++){
						JSONObject json = filterArr.getJSONObject(i);
						FilterObject obj = new FilterObject();
						obj.setKieuDuLieu(json.getLong("kieudulieu"));
						obj.setComparison(json.getLong("comparison"));
						obj.setName(json.getString("name"));
						obj.setPath(json.getString("path"));
						obj.setValue(json.get("value"));
					}
				}
				
				List<String> groupByPath = new ArrayList<String>();
				if(groupArr != null){
					for(int i =0; i< groupArr.length(); i++){
						JSONObject json = groupArr.getJSONObject(i);
						String key =json.getString("key");
						groupByPath.add(key);
						truongThongTinKeyMap.put(key, getTruongThongByPath(key));
						
						aliasName.put(key.toLowerCase(), json.getString("name"));
						chartDataKeyList.add(key.toLowerCase());
						
					}
				}
				
				List<String> functionPath = new ArrayList<String>();
				if(fnArr != null){
					for(int i =0; i< fnArr.length(); i++){
						JSONObject json = fnArr.getJSONObject(i);
						String key =json.getString("key");
						Long function = json.getLong("function");
						functionPath.add(key+"#"+(function != null ? function.intValue() : ""));
						truongThongTinKeyMap.put(key, getTruongThongByPath(key));
						if(function != null){
							CommonUtils.ESummaryFunction f = CommonUtils.ESummaryFunction.getESummaryFunction(function);
							if(f != null){
								chartDataKeyList.add(key.toLowerCase()+"_"+f.getShortName().toLowerCase());
								aliasName.put(key.toLowerCase()+"_"+f.getShortName().toLowerCase(), json.getString("name"));
							}
						}
					}
				}
				
				Map<String, Object> valueMap = SqlJsonBuilder.getReportSqlQuery(groupByPath, truongThongTinKeyMap, functionPath, processInstanceIdList, filterList, fromDate, toDate);
				if(valueMap != null){
					String sql = (String) valueMap.get("SQL");
					Object[] objParams = (Object[]) valueMap.get("PARAMS");
					int[] types = (int[]) valueMap.get("TYPES");
					
					List<Map<String, Object>> results = hoSoService.getReportResult(sql, objParams, types);
				
					String jsonStr = getGChartData(chartDataKeyList, results, aliasName);
					model.addAttribute("jsonData", jsonStr);
					model.addAttribute("chartType", chartType);
					JSONArray jsonArr = new JSONArray();
					HtThamSo colorHtThamSo = htThamSoService.getHtThamSoFind(ThamSo.COLOR_LIST);
					if(colorHtThamSo != null && colorHtThamSo.getGiaTri() != null){
						String colorArr[] = colorHtThamSo.getGiaTri().split(",");
						if(colorArr != null  && colorArr.length >0){
							for(int i =0; i<colorArr.length ; i++){
								if(colorArr[i] != null){
									jsonArr.put(colorArr[i].trim());
								}
							}
						}
					}
					model.addAttribute("colors", jsonArr.toString());
						
				}
			}
			
		}
		return "chartdetail";
		
	}
	public TruongThongTin getTruongThongByPath(String key){
		try {
			if(key.contains(",")){
				Pattern p = Pattern.compile("(([0-9a-zA-Z-_]+),)+([0-9a-zA-Z-_]+)");
				Matcher m = p.matcher(key);
				if(m.matches()){
					String maTtt = m.group(3);
					if(maTtt != null){
						List<TruongThongTin> tttList = truongThongTinService.findByCode(maTtt);
						if(tttList != null && !tttList.isEmpty()){
							TruongThongTin ttt = tttList.get(0);
							return ttt;
						}
					}
				}
				
			}else{
				List<TruongThongTin> tttList = truongThongTinService.findByCode(key);
				if(tttList != null && !tttList.isEmpty()){
					TruongThongTin ttt = tttList.get(0);
					return ttt;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public String getGChartData(List<String> keys, List<Map<String, Object>> results, Map<String, String> aliasName){
		if(results != null && !results.isEmpty() && keys != null && !keys.isEmpty()){
			JSONArray jsonArr = new JSONArray();
			JSONArray jsonHeaderArr = new JSONArray();
			for(String k : keys){
				jsonHeaderArr.put(aliasName.get(k));
			}
			jsonArr.put(jsonHeaderArr);
			for(Map<String, Object> row : results){
				JSONArray jsonDataArr = new JSONArray();
				for(String k : keys){
					Object value = row.get(k);
					if(value != null){
						jsonDataArr.put(value);
					}
				}
				jsonArr.put(jsonDataArr);
			}
			return jsonArr.toString();
		}
		return "";
	}
	
	@RequestMapping(value = "/test", method=RequestMethod.POST)
	public String testPost(@RequestParam("json") String jsonStr, @RequestParam("chartType") String chartType,  Model model){

		model.addAttribute("jsonData", jsonStr);
		model.addAttribute("chartType", chartType);
		return "test";
	}
}

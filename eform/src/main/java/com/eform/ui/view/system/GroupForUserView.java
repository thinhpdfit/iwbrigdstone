package com.eform.ui.view.system;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = GroupForUserView.VIEW_NAME)
public class GroupForUserView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "nguoi-dung-nhom-nguoi-dung";
	private static final String MAIN_TITLE = "Người dùng nhóm người dùng";
	private static final String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_ADDMEMBER_ID = "btn-addmember";
	private static final String BUTTON_UPDATE_MEMBER_ID = "btn-update-member";
	private static final String BTN_DEL_ID = "btn-del";
	private static final String BUTTON_UPDATE_ID = "btn-update";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private Group groupCurrent;
	private CssLayout mainTitleWrap;
	private String action;
	
	private final FieldGroup fieldGroup = new FieldGroup();
	private CssLayout formContainer;
	private Button btnUpdate;
	private Button btnBack;
	
	private Table table = new Table();
	private BeanItemContainer<User> containerMember;
	private BeanItemContainer<User> containerNotMember;
	private List<User> userMemberList;
	private List<User> userNotMemberList;
	private Window addUserWindow;

	private List<User> userAddNewList;
	private List<User> userRemovedList;
	private GroupForUserView currentView = this;
	private Grid grid;
	
	
	@Autowired
	protected IdentityService identityService;
	@Autowired
	private MessageSource messageSource;
	
    @PostConstruct
    void init() {
    	userAddNewList = new ArrayList<User>();
    	userRemovedList = new ArrayList<User>();
    	
    	initMainTitle();
    }

    @Override
    public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				
				String ma = m.group(3);
				groupCurrent = identityService.createGroupQuery().groupId(ma).singleResult();
				if(groupCurrent != null){
					groupDetail(groupCurrent);
					userMemberList = identityService.createUserQuery().memberOfGroup(ma).list();
					containerMember = new BeanItemContainer<User>(User.class, userMemberList);
					initDetailForm();
					initAddMemberWindow();
				}
			}
		}
    }
    
    private void initAddMemberWindow() {
    	VerticalLayout windowContent = new VerticalLayout();
    	windowContent.setSpacing(true);
    	windowContent.setMargin(true);
    	Button btnUpdateMember = new Button("Chọn");
    	btnUpdateMember.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
    	btnUpdateMember.setId(BUTTON_UPDATE_MEMBER_ID);
    	
    	grid = new Grid();
		
    	grid.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		
    	
		List<User> userListTmp = identityService.createUserQuery().list();
		List<User> userOfGroupList = identityService.createUserQuery().memberOfGroup(groupCurrent.getId()).list();

		userNotMemberList = new ArrayList<User>();
		if (userNotMemberList != null && userOfGroupList != null) {
			String str = "";
			for(User u : userOfGroupList){
				str+=u.getId()+":)";
			}
			while(!userListTmp.isEmpty()){
				User u = userListTmp.remove(0);
				if(!str.contains(u.getId())){
					userNotMemberList.add(u);
				}
			}
		}
		
		containerNotMember = new BeanItemContainer<User>(User.class, userNotMemberList);
		
		
		grid.setContainerDataSource(containerNotMember);
		grid.setSelectionMode(SelectionMode.MULTI);
		grid.setColumnOrder("id", "firstName", "lastName");
		grid.setColumns("id", "firstName", "lastName");
		grid.getColumn("id").setHeaderCaption("Tên đăng nhập");
		grid.getColumn("firstName").setHeaderCaption("Họ đệm");
		grid.getColumn("lastName").setHeaderCaption("Tên");
		
		HeaderRow filteringTruongTTHeader = grid.appendHeaderRow();
		TextField filterId = getColumnFilter(grid, "id");
		TextField filterFirstName = getColumnFilter(grid, "firstName");
		TextField filterLastName = getColumnFilter(grid, "lastName");
		filteringTruongTTHeader.getCell("id").setComponent(filterId);
		filteringTruongTTHeader.getCell("firstName").setComponent(filterFirstName);
		filteringTruongTTHeader.getCell("lastName").setComponent(filterLastName);
		
		grid.addSelectionListener(selectionEvent -> { 
		    Object selected = grid.getSelectionModel().getSelectedRows();
		    if (selected != null){
		    	btnUpdateMember.setData(selected);
		    }
		});
		
		btnUpdateMember.addClickListener(this);
		CssLayout btnWrap = new CssLayout();
		btnWrap.setWidth("100%");
		btnWrap.addStyleName(ExplorerLayout.BTN_POPUP_GROUP_CENTER);
		btnWrap.addComponent(btnUpdateMember);
    	

		windowContent.addComponent(grid);
		windowContent.addComponent(btnWrap);
    	
    	addUserWindow = new Window("Danh sách người dùng");
    	addUserWindow.setWidth(900, Unit.PIXELS);
    	addUserWindow.setHeight(520, Unit.PIXELS);
    	addUserWindow.center();
    	addUserWindow.setModal(true);
    	addUserWindow.setContent(windowContent);
	}
    
    private TextField getColumnFilter(final Grid sample, final Object columnId) {
        TextField filter = new TextField();
        filter.setWidth("100%");
        filter.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = -458756607677822455L;
			SimpleStringFilter filter = null;

            @Override
            public void textChange(TextChangeEvent event) {
                Filterable f = (Filterable) sample.getContainerDataSource();
                if (filter != null) {
                    f.removeContainerFilter(filter);
                }
                filter = new SimpleStringFilter(columnId, event.getText(), true, true);
                f.addContainerFilter(filter);

                sample.cancelEditor();
            }
        });
        return filter;
    }

	public void initDetailForm(){
    	
    	formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	FormLayout form = new FormLayout();
    	form.setMargin(true);
    	
    	TextField txtId = new TextField("Mã nhóm");
    	txtId.setRequired(true);
    	txtId.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void textChange(TextChangeEvent event) {

				TextField txt = (TextField) fieldGroup.getField("id");
				System.out.println(event.getText());
				if(!checkTrungMa(event.getText(), action)){
					txt.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
				}else{
					txt.setComponentError(null);
				}
			}
    	});

    	txtId.setTextChangeEventMode(TextChangeEventMode.LAZY);
    	
    	txtId.setSizeFull();
    	txtId.setNullRepresentation("");
    	txtId.setRequiredError("Không được để trống");
    	txtId.addValidator(new StringLengthValidator("Số ký tự tối đa là 20", 1, 20, false));   
    	txtId.addValidator(new RegexpValidator("[0-9a-zA-Z_]+", "Chỉ chấp nhận các ký tự 0-9 a-z A-Z _"));
    	
    	TextField txtName = new TextField("Tên nhóm");
    	txtName.setRequired(true);
    	txtName.setSizeFull();
    	txtName.setNullRepresentation("");
    	txtName.setRequiredError("Không được để trống");
    	txtName.addValidator(new StringLengthValidator("Số ký tự tối đa là 250", 1, 250, false)); 
    	
    	List<ComboboxItem> typeList = new ArrayList<ComboboxItem>();
		typeList.add(new ComboboxItem("security-role", "security-role"));
		typeList.add(new ComboboxItem("assignment", "assignment"));
		
		ComboBox cbbType = new ComboBox("Loại nhóm");
		for (CommonUtils.GroupType item : CommonUtils.GroupType.values()) {
			cbbType.addItem(item.getCode());
			cbbType.setItemCaption(item.getCode(), item.getName());
		}
		cbbType.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		cbbType.setRequired(true);
		cbbType.setRequiredError("Không được để trống");
    	
    	
    	fieldGroup.bind(txtId, "id");
    	fieldGroup.bind(txtName, "name");
    	fieldGroup.bind(cbbType, "type");
    	
    	txtId.setReadOnly(true);
    	txtName.setReadOnly(true);
    	cbbType.setReadOnly(true);
    	
    	form.addComponent(txtId);
    	form.addComponent(txtName);
    	form.addComponent(cbbType);

    	formContainer.addComponent(form);
    	
    	

    	addComponent(formContainer);
    	

    	initUserMemberList();

    	CssLayout buttons = new CssLayout();
    	buttons.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		buttons.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);
    	
    	if(Constants.ACTION_PHAN_NHOM.equals(action)){
    		btnUpdate = new Button();
    		btnUpdate.setId(BUTTON_UPDATE_ID);
    		btnUpdate.addClickListener(this);
    		buttons.addComponent(btnUpdate);
    		
    		btnUpdate.setCaption("Cập nhật");
    		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
    	}
        	
    	btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
		
		
		addComponent(buttons);

    }
    
    public void initUserMemberList(){
		
		if(Constants.ACTION_PHAN_NHOM.equals(action)){
			CssLayout buttons = new CssLayout();
	    	buttons.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
			buttons.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);
			addComponent(buttons);
			
			Button btnAddMember = new Button();
			btnAddMember.setIcon(FontAwesome.PLUS);
			btnAddMember.setId(BUTTON_ADDMEMBER_ID);
			btnAddMember.addClickListener(this);
			btnAddMember.addStyleName(ExplorerLayout.BUTTON_ADDMEMBER);
			buttons.addComponent(btnAddMember);
		}
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		table.setContainerDataSource(containerMember);
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setResponsive(true);
		table.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		table.setColumnHeader("id", "Tên đăng nhập");
		table.setColumnHeader("firstName", "Họ đệm");
		table.setColumnHeader("lastName", "Tên");
		table.setColumnHeader("email", "Email");
		table.setRowHeaderMode(RowHeaderMode.INDEX);
		if(Constants.ACTION_PHAN_NHOM.equals(action)){
			table.addGeneratedColumn("action", new ColumnGenerator() { 
				private static final long serialVersionUID = 1L;
	
				@Override
			    public Object generateCell(final Table source, final Object itemId, Object columnId) {
			        Button btnDel = new Button("Xóa");
			    	btnDel.setId(BTN_DEL_ID);
			    	btnDel.addStyleName(ExplorerLayout.BUTTON_ACTION);
			    	btnDel.addStyleName(Reindeer.BUTTON_LINK);
			    	btnDel.setData(itemId);
			    	btnDel.setIcon(FontAwesome.REMOVE);
			    	btnDel.addClickListener(currentView);
			        return btnDel;
			    }
			});
	
			table.setColumnHeader("action", "Thao tác");
	
			table.setVisibleColumns("id", "firstName", "lastName", "email", "action");
		}else{
			table.setVisibleColumns("id", "firstName", "lastName", "email");
		}
		dataWrap.addComponent(table);
    }
    
    private void groupDetail(Group group) {
        if (group == null) {
        	group = identityService.newGroup("");
        }
        BeanItem<Group> item = new BeanItem<Group>(group);
        fieldGroup.setItemDataSource(item);
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(GroupView.MAIN_TITLE);
		bcItem.setViewName(GroupView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BUTTON_CANCEL_ID.equals(button.getId())){
				UI.getCurrent().getNavigator().navigateTo(GroupView.VIEW_NAME);
			}else if(BUTTON_ADDMEMBER_ID.equals(button.getId())){
				String str = "";
				if(userAddNewList != null && !userAddNewList.isEmpty()){
					for (User u : userAddNewList) {
						str += u.getId() + ":)";
					}
				}
				List<User> uList = new ArrayList();
				for (User u : userNotMemberList) {
					if(str.contains(u.getId())){
						uList.add(u);
					}
				}
				userNotMemberList.removeAll(uList);
				userNotMemberList.addAll(userRemovedList);
				
				containerNotMember.removeAllItems();
				containerNotMember.addAll(userNotMemberList);
				grid.deselectAll();
				UI.getCurrent().addWindow(addUserWindow);
			}else if(BTN_DEL_ID.equals(button.getId())){
				User u = (User) button.getData();
				userRemovedList.add(u);
				userMemberList.remove(u);
				containerMember.removeAllItems();
				containerMember.addAll(userMemberList);
			}else if(BUTTON_UPDATE_MEMBER_ID.equals(button.getId())){
				addUserWindow.close();
				List<User> uList = (List<User>) button.getData();
				if (uList != null && !uList.isEmpty()) {
					userMemberList.addAll(uList);
					userAddNewList.addAll(uList);
					userRemovedList.removeAll(uList);
				}
				
				containerMember.removeAllItems();
				containerMember.addAll(userMemberList);
				
			}else if(BUTTON_UPDATE_ID.equals(button.getId())){
					Notification notf = new Notification("Thông báo", "Cập nhật thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					
					if(userRemovedList != null && !userRemovedList.isEmpty()){
						for (User u : userRemovedList) {
							identityService.deleteMembership(u.getId(), groupCurrent.getId());
						}
					}
					
					List<User> uMember = identityService.createUserQuery().memberOfGroup(groupCurrent.getId()).list();
					String str = "";
					if(uMember != null){
						for (User u : uMember) {
							str += u.getId()+":)";
						}
					}
					
					
					if(userAddNewList != null && !userAddNewList.isEmpty()){
						for (User u : userAddNewList) {
							if(!str.contains(u.getId())){
								identityService.createMembership(u.getId(), groupCurrent.getId());
							}
						}
					}
					
					notf.show(Page.getCurrent());
					UI.getCurrent().getNavigator().navigateTo(GroupView.VIEW_NAME);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        
	}
	
	public boolean checkTrungMa(String ma, String act){
		try {
			if(ma == null || ma.isEmpty()){
				return false;
			}
			if(Constants.ACTION_ADD.equals(act)){
				List<Group> list = identityService.createGroupQuery().groupId(ma).list();
				if(list == null || list.isEmpty()){
					return true;
				}
			}else if(!Constants.ACTION_DEL.equals(act)){
				List<Group> list = identityService.createGroupQuery().groupId(ma).list();
				if(list == null || list.size() <= 1){
					return true;
				}
			}else if(Constants.ACTION_DEL.equals(act)){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
package com.eform.ui.view.task;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.TaskService;

import com.eform.common.ExplorerLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;

public class TaskCategoryMenuBar extends CssLayout implements ClickListener{

	private static final String BTN_INBOX_ID = "btnInbox";
	private static final String BTN_MY_TASK_ID = "btnMyTask";
	private static final String BTN_QUEUED_ID = "btnQueued";
	private static final String BTN_Involed_ID = "btnInvoled";
	private static final String BTN_ARCHIVED_ID = "btnArchived";
	private static final String BTN_ASSIGNED_ID = "btnAssigned";
	
	private Button btnInbox;
	private Button btnMyTask;
	private Button btnQueued;
	private Button btnInvoled;
	private Button btnArchived;
	private Button btnAssigned;
	
	private long inboxCount = 0L;
	private long mytaskCount = 0L;
	private long queueCount = 0L;
	private long involesCount = 0L;
	private long archivedCount = 0L;
	private long assignedCount = 0L;
	
	private String category;

	private HistoryService historyService;
	private TaskService taskService;
	private String userId;
	private List<String> groupIds;
	private final List<Button> buttons = new ArrayList();
	
	public TaskCategoryMenuBar(HistoryService historyService, TaskService taskService, String userId, List<String> groupIds){
		addStyleName(ExplorerLayout.TASK_LIST_BUTTON_WRAP);
		this.historyService = historyService;
		this.taskService = taskService;
		this.userId = userId;
		this.groupIds = groupIds;
		init();
		refreshCount();
	}
	
	public void init(){
		btnInbox = new Button("Inbox ("+inboxCount+")");
		btnInbox.setStyleName(ExplorerLayout.BTN_TASK_LIST_ACTIVE);
		
		btnInbox.addClickListener(this);
		btnInbox.setId(BTN_INBOX_ID);
		btnInbox.setData(TaskListView.INBOX_CATE);
		addComponent(btnInbox);
		buttons.add(btnInbox);
		
		btnMyTask = new Button("My tasks ("+mytaskCount+")");
		btnMyTask.setStyleName(ExplorerLayout.BTN_TASK_LIST);
		btnMyTask.addClickListener(this);
		btnMyTask.setId(BTN_MY_TASK_ID);
		if(TaskListView.MY_TASK_CATE.equals(category)){
			btnMyTask.setStyleName(ExplorerLayout.BTN_TASK_LIST_ACTIVE);
		}
		btnMyTask.setData(TaskListView.MY_TASK_CATE);
		
		addComponent(btnMyTask);
		buttons.add(btnMyTask);
		
		btnQueued = new Button("Queued ("+queueCount+")");
		btnQueued.setStyleName(ExplorerLayout.BTN_TASK_LIST);
		btnQueued.addClickListener(this);
		btnQueued.setId(BTN_QUEUED_ID);
		if(TaskListView.QUEUED_CATE.equals(category)){
			btnQueued.setStyleName(ExplorerLayout.BTN_TASK_LIST_ACTIVE);
		}
		btnQueued.setData(TaskListView.QUEUED_CATE);
		
		addComponent(btnQueued);
		buttons.add(btnQueued);
		
		btnInvoled = new Button("Involed ("+involesCount+")");
		btnInvoled.setStyleName(ExplorerLayout.BTN_TASK_LIST);
		btnInvoled.addClickListener(this);
		btnInvoled.setId(BTN_Involed_ID);
		if(TaskListView.INVOLED_CATE.equals(category)){
			btnInvoled.setStyleName(ExplorerLayout.BTN_TASK_LIST_ACTIVE);
		}
		btnInvoled.setData(TaskListView.INVOLED_CATE);
		
		addComponent(btnInvoled);
		buttons.add(btnInvoled);
		
		btnArchived = new Button("Archived ("+archivedCount+")");
		btnArchived.setStyleName(ExplorerLayout.BTN_TASK_LIST);
		btnArchived.addClickListener(this);
		btnArchived.setId(BTN_ARCHIVED_ID);
		if(TaskListView.ARCHIVED_CATE.equals(category)){
			btnArchived.setStyleName(ExplorerLayout.BTN_TASK_LIST_ACTIVE);
		}
		btnArchived.setData(TaskListView.ARCHIVED_CATE);
		
		addComponent(btnArchived);
		buttons.add(btnArchived);
		
		btnAssigned = new Button("Finished ("+assignedCount+")");
		btnAssigned.setStyleName(ExplorerLayout.BTN_TASK_LIST);
		btnAssigned.addClickListener(this);
		btnAssigned.setId(BTN_ASSIGNED_ID);
		if(TaskListView.ASSIGNED_CATE.equals(category)){
			btnAssigned.setStyleName(ExplorerLayout.BTN_TASK_LIST_ACTIVE);
		}
		btnAssigned.setData(TaskListView.ASSIGNED_CATE);
		addComponent(btnAssigned);
		buttons.add(btnAssigned);
	}
	
	public void refreshCount(){
		archivedCount = historyService.createHistoricTaskInstanceQuery().taskOwner(userId).finished().count();
		assignedCount = historyService.createHistoricTaskInstanceQuery().taskAssignee(userId).finished().count();
		inboxCount = taskService.createTaskQuery().taskAssignee(userId).count();
		queueCount = taskService.createTaskQuery().taskCandidateGroupIn(groupIds).count();
		mytaskCount = taskService.createTaskQuery().taskOwner(userId).count();
		involesCount = taskService.createTaskQuery().taskInvolvedUser(userId).count();
		
		btnArchived.setCaption("Archived ("+archivedCount+")");
		btnInbox.setCaption("Inbox ("+inboxCount+")");
		btnMyTask.setCaption("My tasks ("+mytaskCount+")");
		btnInvoled.setCaption("Involed ("+involesCount+")");
		btnQueued.setCaption("Queued ("+queueCount+")");
		btnAssigned.setCaption("Finished ("+assignedCount+")");
	}
	
	public void setActiveButton(Button button){
		for(Button btn : buttons){
			btn.setStyleName(ExplorerLayout.BTN_TASK_LIST);
		}
		button.setStyleName(ExplorerLayout.BTN_TASK_LIST_ACTIVE);
	}
	
	public void setActiveButton(String category){
		for(Button btn : buttons){
			btn.setStyleName(ExplorerLayout.BTN_TASK_LIST);
			String data = (String) btn.getData();
			if(data.equals(category)){
				btn.setStyleName(ExplorerLayout.BTN_TASK_LIST_ACTIVE);
			}
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_INBOX_ID.equals(event.getButton().getId())){
			setActiveButton(button);
			String category = (String) button.getData();
			UI.getCurrent().getNavigator().navigateTo(TaskListView.VIEW_NAME+"/"+category);
		}else if(BTN_MY_TASK_ID.equals(event.getButton().getId())){
			setActiveButton(button);
			String category = (String) button.getData();
			UI.getCurrent().getNavigator().navigateTo(TaskListView.VIEW_NAME+"/"+category);
		}else if(BTN_QUEUED_ID.equals(event.getButton().getId())){
			setActiveButton(button);
			String category = (String) button.getData();
			UI.getCurrent().getNavigator().navigateTo(TaskListView.VIEW_NAME+"/"+category);
		}else if(BTN_Involed_ID.equals(event.getButton().getId())){
			setActiveButton(button);
			String category = (String) button.getData();
			UI.getCurrent().getNavigator().navigateTo(TaskListView.VIEW_NAME+"/"+category);
		}else if(BTN_ARCHIVED_ID.equals(event.getButton().getId())){
			setActiveButton(button);
			String category = (String) button.getData();
			UI.getCurrent().getNavigator().navigateTo(TaskListView.VIEW_NAME+"/"+category);
		}else if(BTN_ASSIGNED_ID.equals(event.getButton().getId())){
			setActiveButton(button);
			String category = (String) button.getData();
			UI.getCurrent().getNavigator().navigateTo(TaskListView.VIEW_NAME+"/"+category);
		}
	}
}

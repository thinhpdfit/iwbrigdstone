package com.eform.ui.view.system;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.workflow.ITrangThai;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.HtQuyenDao;
import com.eform.model.entities.HtQuyen;
import com.eform.ui.custom.ButtonAction;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.StatusCommentLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = HtQuyenView.VIEW_NAME)
public class HtQuyenView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "ht-quyen";
	static final String MAIN_TITLE = "Hệ thống quyền";
	private static final String SMALL_TITLE = "";

	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	private static final String BTN_ADDNEW_ID = "themMoi";
	private static final String BTN_ADD_CHILD_ID = "btnAddChild";
	private final HtQuyenView currentView = this;
	
	@Autowired
	HtQuyenDao htQuyenDao;
	private List<HtQuyen> htQuyenList;
	private CssLayout mainTitleWrap;
	private TreeTable treeTable;
	private WorkflowManagement<HtQuyen> htQuyenWM;
	
	private final PropertysetItem searchItem = new PropertysetItem();
	private final FieldGroup searchFieldGroup = new FieldGroup();
	private Button btnAddNew;
	@Autowired
	private MessageSource messageSource;
	
	@PostConstruct
    void init() {
		htQuyenWM = new WorkflowManagement<HtQuyen>(null);
		htQuyenList = Lists.newArrayList(htQuyenDao.getHtQuyenFind(null,null, null, null, null, -1, -1));
    	searchItem.addItemProperty("ma", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("ten", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("trangThai", new ObjectProperty<ComboboxItem>(new ComboboxItem()));
    	searchFieldGroup.setItemDataSource(searchItem);
    	
		initMainTitle();
		initSearchForm();
		initDataContent();
        
		btnAddNew = new Button();
    	btnAddNew.setIcon(FontAwesome.PLUS);
    	btnAddNew.addClickListener(this);
    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
    	btnAddNew.setDescription("Tạo mới quyền");
    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
    	btnAddNew.setId(BTN_ADDNEW_ID);
		addComponent(btnAddNew);
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		setWidth("100%");
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		
		CssFormLayout searchForm = new CssFormLayout(-1, 60);
		
		TextField txtMa = new TextField("Mã quyền");
		searchForm.addFeild(txtMa);
		
		TextField txtTen = new TextField("Tên quyền");
		searchForm.addFeild(txtTen);
    	
    	List<ITrangThai> trangThaiListTmp = htQuyenWM.getTrangThaiList();
    	List<ComboboxItem> trangThaiLoaiDanhMucList = new ArrayList<>();
    	if(trangThaiListTmp != null){
    		for (ITrangThai trangThai : trangThaiListTmp) {
    			trangThaiLoaiDanhMucList.add(new ComboboxItem(trangThai.getTen(), new Long(trangThai.getId())));
			}
    	}
    	
    	BeanItemContainer<ComboboxItem> cbbTrangThaiContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, trangThaiLoaiDanhMucList);
		ComboBox cbbTrangThai = new ComboBox("Trạng thái", cbbTrangThaiContainer);
		cbbTrangThai.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbTrangThai.setItemCaptionPropertyId("text");
    	searchForm.addFeild(cbbTrangThai);

		searchFieldGroup.bind(txtMa, "ma");
		searchFieldGroup.bind(txtTen, "ten");
		searchFieldGroup.bind(cbbTrangThai, "trangThai");
		
		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(currentView);
		searchForm.addButton(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(currentView);
		searchForm.addButton(btnRefresh);
		
		addComponent(searchForm);
	}
	
	public void initDataContent(){
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
		List<ITrangThai> trangThaiList = htQuyenWM.getTrangThaiList();
		if(trangThaiList != null && !trangThaiList.isEmpty()){
			dataWrap.addComponent(new StatusCommentLayout(trangThaiList));
		}
		
		treeTable = new TreeTable();
		treeTable.addStyleName(ExplorerLayout.DATA_TABLE);
		treeTable.setSizeFull();
		treeTable.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		
		treeTable.addContainerProperty("icon", String.class, "");
		treeTable.addContainerProperty("ma", String.class, "");
		treeTable.addContainerProperty("ten", String.class, "");
		treeTable.addContainerProperty("viewName", String.class, "");
		treeTable.addContainerProperty("menu", Label.class, null);
		treeTable.addContainerProperty("action", HorizontalLayout.class, null);
		treeTable.setColumnWidth("action", 130);
		treeTable.setColumnHeader("ma", "Mã");
		treeTable.setColumnHeader("ten", "Tên");
		treeTable.setColumnHeader("icon", "Icon");
		treeTable.setColumnHeader("viewName", "URL");
		treeTable.setColumnHeader("menu", "Là menu");
		treeTable.setColumnHeader("action", "Thao tác");
		treeTable.setColumnWidth("action", 220);
		treeTable.setRowHeaderMode(RowHeaderMode.INDEX);
		
		treeTable.setCellStyleGenerator(new TreeTable.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if("action".equals(propertyId) ){
                    return "left-aligned";
                }else if("trangThai".equals(propertyId)){
                    return "center-aligned ";
                }else if("menu".equals(propertyId)){
                    return "center-aligned ";
                }

            	HtQuyen current = (HtQuyen) itemId;
				Workflow<HtQuyen> wf = htQuyenWM.getWorkflow(current);
				return wf.getStyleName();
			}
			
		});
		treeTable.removeAllItems();
		setContentTreeTable();
		dataWrap.addComponent(treeTable);
	}
	
	public void setContentTreeTable(){
		if(htQuyenList == null){
			return;
		}
		for(HtQuyen q : htQuyenList){
			addItemTreeTable(q);
		}
		for(HtQuyen q : htQuyenList){
			treeTable.setParent(q, q.getHtQuyen());
		}
		
		for (Object itemId: treeTable.getContainerDataSource().getItemIds()) {
			treeTable.setCollapsed(itemId, false);
			
		    if (!treeTable.hasChildren(itemId)){
		    	treeTable.setChildrenAllowed(itemId, false);
		    }
		}
	}
	
	public void addItemTreeTable(HtQuyen q){
		if(q != null){
			Workflow<HtQuyen> wf = htQuyenWM.getWorkflow(q);
			
			HorizontalLayout actionWrap = new HorizontalLayout();
			actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
	        
			new ButtonAction<HtQuyen>().getButtonAction(actionWrap, wf, currentView);
	        
	        
        	Button btnAddChild = new Button();
        	btnAddChild.setId(BTN_ADD_CHILD_ID);
        	btnAddChild.setData(q);
        	btnAddChild.setDescription("Thêm quyền con");
        	btnAddChild.setIcon(FontAwesome.PLUS);
        	btnAddChild.addStyleName(ExplorerLayout.BUTTON_ACTION);
        	btnAddChild.addStyleName(Reindeer.BUTTON_LINK);
        	btnAddChild.addClickListener(currentView);
	        actionWrap.addComponent(btnAddChild);
	        
	       Label lblMenu = new Label();
	       if(q.getMenu() != null && q.getMenu().intValue() == CommonUtils.EMenu.MENU.getCode()){
	    	   lblMenu.setValue(FontAwesome.CHECK.getHtml());
	    	   lblMenu.setContentMode(ContentMode.HTML);
	        }
	        
	        if(q.getIcon() != null){
	        	treeTable.setItemIcon(q, FontAwesome.fromCodepoint(q.getIcon().intValue()));
	        }
	        
			treeTable.addItem(new Object[]{"", q.getMa(), q.getTen(), q.getViewName(),lblMenu, actionWrap}, q);
			
			
		}
		
	}
	
	public void reloadData(){
		treeTable.removeAllItems();
		setContentTreeTable();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			if(BTN_SEARCH_ID.equals(event.getButton().getId())){
				try {
					if(searchFieldGroup.isValid()){
						searchFieldGroup.commit();
						String ma = (String) searchItem.getItemProperty("ma").getValue();
						ma = ma != null ? "%"+ma.trim()+"%" : null;
						String ten = (String) searchItem.getItemProperty("ten").getValue();
						ten = ten != null ? "%"+ten.trim()+"%" : null;
						ComboboxItem trangThai = (ComboboxItem) searchItem.getItemProperty("trangThai").getValue();
						List<Long> trangThaiList = null;
						if(trangThai != null && trangThai.getValue() != null){
							trangThaiList = new ArrayList<Long>();
							trangThaiList.add((Long) trangThai.getValue());
						}
						htQuyenList = htQuyenDao.getHtQuyenFind(ma,null, ten, trangThaiList,null,  -1, -1);
						treeTable.removeAllItems();
						setContentTreeTable();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
		    	searchFieldGroup.clear();
		    	htQuyenList = htQuyenDao.getHtQuyenFind(null,null, null, null,null,  -1, -1);
		    	treeTable.removeAllItems();
				setContentTreeTable();
			}else if(ButtonAction.BTN_EDIT_ID.equals(event.getButton().getId())){
				HtQuyen currentRow = (HtQuyen) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(HtQuyenDetailView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getMa().toLowerCase());
			}else if(ButtonAction.BTN_DEL_ID.equals(event.getButton().getId())){
				HtQuyen currentRow = (HtQuyen) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(HtQuyenDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getMa().toLowerCase());
			}else if(ButtonAction.BTN_APPROVE_ID.equals(event.getButton().getId())){
				HtQuyen currentRow = (HtQuyen) event.getButton().getData();
		        //UI.getCurrent().getNavigator().navigateTo(HtQuyenDetailView.VIEW_NAME+"/"+Constants.ACTION_APPROVE+"/"+currentRow.getMa().toLowerCase());
				Workflow<HtQuyen> htQuyenWf = htQuyenWM.getWorkflow(currentRow);
				HtQuyen current = htQuyenWf.pheDuyet();
				htQuyenDao.save(current);
				Notification notf = new Notification("Thông báo", htQuyenWf.getPheDuyetTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(Page.getCurrent());
				reloadData();
			}else if(ButtonAction.BTN_REFUSE_ID.equals(event.getButton().getId())){
				HtQuyen currentRow = (HtQuyen) event.getButton().getData();
		        //UI.getCurrent().getNavigator().navigateTo(HtQuyenDetailView.VIEW_NAME+"/"+Constants.ACTION_REFUSE+"/"+currentRow.getMa().toLowerCase());
				Workflow<HtQuyen> htQuyenWf = htQuyenWM.getWorkflow(currentRow);
				HtQuyen current = htQuyenWf.tuChoi();
				htQuyenDao.save(current);
				Notification notf = new Notification("Thông báo", htQuyenWf.getTuChoiTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(Page.getCurrent());
				reloadData();
			}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
				 UI.getCurrent().getNavigator().navigateTo(HtQuyenDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD);
			}else if(BTN_ADD_CHILD_ID.equals(event.getButton().getId())){
				HtQuyen currentRow = (HtQuyen) event.getButton().getData();
				 UI.getCurrent().getNavigator().navigateTo(HtQuyenDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD+"/"+currentRow.getMa().toLowerCase());
			}
		}catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra", Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
		}
	}

}

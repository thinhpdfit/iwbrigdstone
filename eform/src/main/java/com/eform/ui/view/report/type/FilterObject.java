package com.eform.ui.view.report.type;

import java.io.Serializable;

public class FilterObject implements Serializable{
	private String name;
	private String path;
	private Long comparison;
	private Object value;
	private Long kieuDuLieu;
	
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Long getComparison() {
		return comparison;
	}
	public void setComparison(Long comparison) {
		this.comparison = comparison;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public Long getKieuDuLieu() {
		return kieuDuLieu;
	}
	public void setKieuDuLieu(Long kieuDuLieu) {
		this.kieuDuLieu = kieuDuLieu;
	}
	
	
}

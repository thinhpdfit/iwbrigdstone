
package com.eform.ui.view.dashboard;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.ThamSo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.HtThamSo;
import com.eform.service.DeploymentHtDonViService;
import com.eform.service.HtDonViServices;
import com.eform.service.HtThamSoService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.process.ProcessDefinitionDetailView;
import com.eform.ui.view.process.ProcessDefinitionListView;
import com.eform.ui.view.task.ArchivedView;
import com.eform.ui.view.task.FinishedView;
import com.eform.ui.view.task.InboxView;
import com.eform.ui.view.task.QueueView;
import com.eform.ui.view.task.TaskDetailView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = DashboardView.VIEW_NAME)
public class DashboardView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "";
	private static String MAIN_TITLE = "";
	private static String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private final String BTN_TASK_ASSIGN_ID = "btnTaskAssign";
	private final String BTN_PROCESS_DETAIL_ID = "btnProcessDetail";
	private final String BTN_TASK_QUEUE_ID = "btnQueue";
	private final String BTN_OWNER_ID = "btnOwner";
	private final String BTN_FINISHED_ID = "btnFinished";
	private final String BTN_CURRENT_TASK_ID = "btnCurrentTask";
	private final String BTN_TASK_LIST_VIEW_ID = "btnInboxView";
	private final String BTN_PROCESS_LIST_VIEW_ID = "btnProcessListView";
	
	@Autowired
	private TaskService taskService;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	protected transient FormService formService;
	@Autowired
	private HtThamSoService htThamSoService;
	@Autowired
	private HtDonViServices htDonViServices;
	@Autowired
	private DeploymentHtDonViService deploymentHtDonViService;
	@Autowired
	private MessageSource messageSource;
	
	private CssLayout dashboardContainer;
	private boolean visibleProcessReport = false;
	private boolean visibleGeneralReport = false;
	

    @PostConstruct
    void init() {
    	MAIN_TITLE = messageSource.getMessage(Messages.UI_VIEW_DASHBOARD_TITLE, null, VaadinSession.getCurrent().getLocale());
		SMALL_TITLE = messageSource.getMessage(Messages.UI_VIEW_DASHBOARD_TITLE_SMALL, null, VaadinSession.getCurrent().getLocale());
    	
    	HtThamSo htThamSo= htThamSoService.getHtThamSoFind(ThamSo.PROCESS_REPORT);
		if(htThamSo != null && htThamSo.getGiaTri() != null && !htThamSo.getGiaTri().trim().isEmpty()){
			if(htThamSo.getGiaTri().equals("1")){
				visibleProcessReport = true;
			}
		}
		
		htThamSo= htThamSoService.getHtThamSoFind(ThamSo.GENERAL_REPORT);
		if(htThamSo != null && htThamSo.getGiaTri() != null && !htThamSo.getGiaTri().trim().isEmpty()){
			if(htThamSo.getGiaTri().equals("1")){
				visibleGeneralReport = true;
			}
		}
    	
    	initMainTitle();
    	
    	dashboardContainer = new CssLayout();
    	dashboardContainer.addStyleName(ExplorerLayout.DASHBOARD_CONTAINER);
    	dashboardContainer.setWidth("100%");
		addComponent(dashboardContainer);
		
    	initThongKe();
    	initNewTaskWrap();
    	initProcessList();
    	if(visibleProcessReport){
    		initProcessReport();
    	}
    	if(visibleGeneralReport){
    		initGeneralReport();
    	}
    	initChart();
    }

    @Override
    public void enter(ViewChangeEvent event) {
    	UI.getCurrent().getPage().setTitle(MAIN_TITLE);
    }
    
    public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE));
	}
    
    public void initThongKe(){
    	CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_ROW);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		dashboardContainer.addComponent(dataWrap);
		
		long ownerTaskFinished = historyService.createHistoricTaskInstanceQuery().taskOwner(CommonUtils.getUserLogedIn()).finished().count();
		dataWrap.addComponent(initItem(messageSource.getMessage(Messages.UI_VIEW_DASHBOARD_TASKOWNER_TITLE, null, VaadinSession.getCurrent().getLocale()), ownerTaskFinished, FontAwesome.STREET_VIEW, BTN_OWNER_ID, ExplorerLayout.TK_OWNER));
		
		long assigneeTaskFinished = historyService.createHistoricTaskInstanceQuery().taskAssignee(CommonUtils.getUserLogedIn()).finished().count();
		dataWrap.addComponent(initItem(messageSource.getMessage(Messages.UI_VIEW_DASHBOARD_TASKFINISHED_TITLE, null, VaadinSession.getCurrent().getLocale()), assigneeTaskFinished, FontAwesome.USER, BTN_FINISHED_ID, ExplorerLayout.TK_FINISHED));
		
		long assigneeTask = taskService.createTaskQuery().taskAssignee(CommonUtils.getUserLogedIn()).count();
		dataWrap.addComponent(initItem(messageSource.getMessage(Messages.UI_VIEW_DASHBOARD_TASKASSIGN_TITLE, null, VaadinSession.getCurrent().getLocale()), assigneeTask, FontAwesome.STAR_HALF_EMPTY, BTN_CURRENT_TASK_ID, ExplorerLayout.TK_INBOX));
		long queueTask = 0L;
		List<String> groupIds = CommonUtils.getGroupId(identityService);
		if(groupIds != null && !groupIds.isEmpty()){
			queueTask = taskService.createTaskQuery().taskCandidateGroupIn(groupIds).count();
		}
		
		dataWrap.addComponent(initItem(messageSource.getMessage(Messages.UI_VIEW_DASHBOARD_TASKQUEUE_TITLE, null, VaadinSession.getCurrent().getLocale()), queueTask, FontAwesome.USERS, BTN_TASK_QUEUE_ID, ExplorerLayout.TK_QUEUE));
		
    }
    
    public CssLayout initItem(String label, Long number, FontAwesome icon, String btnId, String styleName){
    	if(label != null && number != null){
    		CssLayout item = new CssLayout();
    		item.addStyleName(ExplorerLayout.TK_ITEM);
    		if(styleName != null){
        		item.addStyleName(styleName);
    		}
    		Label lblIcon = new Label(icon.getHtml());
    		lblIcon.setCaptionAsHtml(true);
    		lblIcon.setContentMode(ContentMode.HTML);
    		lblIcon.addStyleName(ExplorerLayout.TK_ITEM_ICON);
    		item.addComponent(lblIcon);
    		
    		Label lblSoLieu = new Label(number + "");
    		Label lblTitle = new Label(label);
    		lblSoLieu.addStyleName(ExplorerLayout.TK_ITEM_SL);
    		lblTitle.addStyleName(ExplorerLayout.TK_ITEM_TT);
    		item.addComponent(lblSoLieu);
    		item.addComponent(lblTitle);

    		Button btnDetail = new Button();
    		btnDetail.addClickListener(this);
    		btnDetail.setId(btnId);
    		btnDetail.addStyleName(ExplorerLayout.BTN_TK_DETAIL);
    		item.addComponent(btnDetail);
    		
    		return item;
    	}
    	return null;
    }

	private void initNewTaskWrap() {
		CssLayout dashboardBlock = new  CssLayout();
		dashboardBlock.addStyleName(ExplorerLayout.DASHBOARD_BLOCK);
		dashboardBlock.addStyleName(ExplorerLayout.NEW_TASK_LIST);
		
		dashboardBlock.setWidth(100, Unit.PERCENTAGE);
		
		CssLayout dashboardBlockTitle = new  CssLayout();
		dashboardBlockTitle.setWidth(100, Unit.PERCENTAGE);
		dashboardBlockTitle.addStyleName(ExplorerLayout.DASHBOARD_BLOCK_TITLE_WRAP);
		dashboardBlockTitle.addStyleName(ExplorerLayout.NEW_TASK_LIST_TITLE_WRAP);
		
		Button newTaskTtl = new Button(messageSource.getMessage(Messages.UI_VIEW_DASHBOARD_TASKLIST_TITLE, null, VaadinSession.getCurrent().getLocale()));
		newTaskTtl.addClickListener(this);
		newTaskTtl.setId(BTN_TASK_LIST_VIEW_ID);
		newTaskTtl.addStyleName(Reindeer.BUTTON_LINK);
		newTaskTtl.addStyleName(ExplorerLayout.DASHBOARD_BLOCK_TITLE);
		newTaskTtl.addStyleName(ExplorerLayout.NEW_TASK_TITLE);
		dashboardBlockTitle.addComponent(newTaskTtl);
		dashboardBlock.addComponent(dashboardBlockTitle);
		
		CssLayout dashboardBlockContent = new  CssLayout();
		dashboardBlockContent.setWidth(100, Unit.PERCENTAGE);
		dashboardBlockContent.addStyleName(ExplorerLayout.DASHBOARD_BLOCK_CONTAINER);
		dashboardBlockContent.addStyleName(ExplorerLayout.NEW_TASK_CONTAINER);
		dashboardBlock.addComponent(dashboardBlockContent);
		
		List<Task> newTasks = taskService.createTaskQuery().taskAssignee(CommonUtils.getUserLogedIn()).active().orderByTaskCreateTime().desc().listPage(0, 10);
		if(newTasks != null && !newTasks.isEmpty()){
			int i = 0;
			for(Task t : newTasks){
				CssLayout item = initTaskNewItem(t, ++i);
				if(item != null){
					dashboardBlockContent.addComponent(item);
				}
			}
		}else{
			Label emptyLbl = new Label(messageSource.getMessage(Messages.UI_VIEW_DASHBOARD_TASKLIST_EMPTY, null, VaadinSession.getCurrent().getLocale()));
			emptyLbl.setStyleName(ExplorerLayout.NEW_TASK_EMPTY);
			dashboardBlockContent.addComponent(emptyLbl);
		}
		dashboardContainer.addComponent(dashboardBlock);
	}
	
	public CssLayout initTaskNewItem(Task t, int i){
		if(t != null){
			CssLayout taskItem = new  CssLayout();
			taskItem.setStyleName(ExplorerLayout.NEW_TASK_ITEM);
			
			CssLayout taskNumberWrap = new CssLayout();
			taskNumberWrap.setStyleName(ExplorerLayout.NEW_TASK_ITEM_NUMBER_WRAP);
			taskItem.addComponent(taskNumberWrap);
			
			Label number = new Label(i+"");
			number.setStyleName(ExplorerLayout.NEW_TASK_ITEM_NUMBER);
			taskNumberWrap.addComponent(number);
			
			CssLayout taskInfoWrap = new CssLayout();
			taskInfoWrap.setStyleName(ExplorerLayout.NEW_TASK_ITEM_INFO_WRAP);
			taskItem.addComponent(taskInfoWrap);
			
			
			String nameStr = "#Không tên";
			if(t.getName() != null && !t.getName().isEmpty()){
				nameStr = t.getName();
			}
			Button name = new Button(nameStr);
			name.addStyleName(Reindeer.BUTTON_LINK);
			name.addStyleName(ExplorerLayout.NEW_TASK_ITEM_NAME);
			name.setId(BTN_TASK_ASSIGN_ID);
			name.addClickListener(this);
			name.setData(t);
			taskInfoWrap.addComponent(name);
			
			SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
			if(t.getCreateTime() != null){
				Label createTime = new Label(df.format(t.getCreateTime()));
				createTime.setStyleName(ExplorerLayout.NEW_TASK_CREATE_TIME);
				taskInfoWrap.addComponent(createTime);
			}
			
			
			
			
			return taskItem;
		}
		return null;
	}

    public void initChart(){
		HtThamSo htThamSo= htThamSoService.getHtThamSoFind(ThamSo.DASHBOAD_CHARTS);
		if(htThamSo != null && htThamSo.getGiaTri() != null && !htThamSo.getGiaTri().trim().isEmpty()){
			
			String giaTri = htThamSo.getGiaTri();
			String[] strArr = giaTri.split(",");
			
			if(strArr != null && strArr.length > 0){
				CssLayout dataWrap = new CssLayout();
				dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
				dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
				dashboardContainer.addComponent(dataWrap);
				String path = CommonUtils.getRootPath()+"/charts/detail/";
				for(String str : strArr){
					BrowserFrame browser = new BrowserFrame(null, new ExternalResource(path+str.trim()));
			    	browser.setWidth("100%");
			    	browser.setHeight("520px");
					dataWrap.addComponent(browser);
				}
			}
			
		}
    	
    }
    
    public void initProcessReport(){
    	CssLayout dashboardBlock = new  CssLayout();
		dashboardBlock.addStyleName(ExplorerLayout.DASHBOARD_BLOCK);
		dashboardBlock.addStyleName(ExplorerLayout.NEW_TASK_LIST);
		dashboardBlock.setWidth(100, Unit.PERCENTAGE);
		
		CssLayout dashboardBlockTitle = new  CssLayout();
		dashboardBlockTitle.setWidth(100, Unit.PERCENTAGE);
		dashboardBlockTitle.addStyleName(ExplorerLayout.DASHBOARD_BLOCK_TITLE_WRAP);
		
		HtThamSo htThamSoTitle = htThamSoService.getHtThamSoFind(ThamSo.PROCESS_REPORT_TITLE);
		String titleStr = "Biểu đồ tổng hợp";
		if(htThamSoTitle != null && htThamSoTitle.getGiaTri() != null){
			titleStr = htThamSoTitle.getGiaTri();
		}
		
		Label title = new Label(titleStr);
		title.setStyleName(ExplorerLayout.DASHBOARD_BLOCK_TITLE);
		dashboardBlockTitle.addComponent(title);
		dashboardBlock.addComponent(dashboardBlockTitle);
		
		CssLayout dashboardBlockContent = new  CssLayout();
		dashboardBlockContent.setWidth(100, Unit.PERCENTAGE);
		dashboardBlockContent.addStyleName(ExplorerLayout.DASHBOARD_BLOCK_CONTAINER);
		dashboardBlockContent.addStyleName(ExplorerLayout.NEW_TASK_CONTAINER);
		String path = CommonUtils.getRootPath()+"/report/process";
		BrowserFrame browser = new BrowserFrame(null, new ExternalResource(path));
    	browser.setWidth("100%");
    	browser.setHeight("520px");
    	dashboardBlockContent.addComponent(browser);
		dashboardBlock.addComponent(dashboardBlockContent);
    	
    	dashboardContainer.addComponent(dashboardBlock);
    }
    
    public void initGeneralReport(){
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		dashboardContainer.addComponent(dataWrap);
		String path = CommonUtils.getRootPath()+"/report/general";
		BrowserFrame browser = new BrowserFrame(null, new ExternalResource(path));
    	browser.setWidth("100%");
    	browser.setHeight("590px");
		dataWrap.addComponent(browser);
    }
    
    public void initProcessList(){
    	
		
		String userId = CommonUtils.getUserLogedIn();
		String maDonVi = identityService.getUserInfo(userId, "HT_DON_VI");
		Set<String> depIds= new HashSet<String>();
		if(maDonVi != null){
			List<HtDonVi> dvList = htDonViServices.getHtDonViFind(maDonVi,null, null, null, 0, 1);
			if(!dvList.isEmpty()){
				List<DeploymentHtDonVi> deploymentHtDonViList = deploymentHtDonViService.getDeploymentHtDonViFind(null, dvList.get(0));
				if(deploymentHtDonViList != null && !deploymentHtDonViList.isEmpty()){
					for(DeploymentHtDonVi item : deploymentHtDonViList){
						depIds.add(item.getDeploymentId());
					}
				}
			}
		}
		
		CssLayout dashboardBlock = new  CssLayout();
		dashboardBlock.addStyleName(ExplorerLayout.DASHBOARD_BLOCK);
		dashboardBlock.addStyleName(ExplorerLayout.PROCESS_LIST);
		
		dashboardBlock.setWidth(100, Unit.PERCENTAGE);
		
		CssLayout dashboardBlockTitle = new  CssLayout();
		dashboardBlockTitle.setWidth(100, Unit.PERCENTAGE);
		dashboardBlockTitle.addStyleName(ExplorerLayout.DASHBOARD_BLOCK_TITLE_WRAP);
		dashboardBlockTitle.addStyleName(ExplorerLayout.PROCESS_LIST_TITLE_WRAP);
		
		Button processListTtl = new Button(messageSource.getMessage(Messages.UI_VIEW_DASHBOARD_PROCESSLIST_TITLE, null, VaadinSession.getCurrent().getLocale()));
		processListTtl.addClickListener(this);
		processListTtl.setId(BTN_PROCESS_LIST_VIEW_ID);
		processListTtl.addStyleName(Reindeer.BUTTON_LINK);
		processListTtl.addStyleName(ExplorerLayout.DASHBOARD_BLOCK_TITLE);
		processListTtl.addStyleName(ExplorerLayout.PROCESS_TITLE);
		dashboardBlockTitle.addComponent(processListTtl);
		dashboardBlock.addComponent(dashboardBlockTitle);
		
		CssLayout dashboardBlockContent = new  CssLayout();
		dashboardBlockContent.setWidth(100, Unit.PERCENTAGE);
		dashboardBlockContent.addStyleName(ExplorerLayout.DASHBOARD_BLOCK_CONTAINER);
		dashboardBlockContent.addStyleName(ExplorerLayout.PROCESS_CONTAINER);
		dashboardBlock.addComponent(dashboardBlockContent);
		
		dashboardContainer.addComponent(dashboardBlock);
		boolean isEmpty = false;
		if(depIds != null && !depIds.isEmpty()){
			
			List<ProcessDefinition> processDefinitionList = repositoryService.createProcessDefinitionQuery().deploymentIds(depIds).active().orderByDeploymentId().desc().listPage(0, 10);
			
			if(processDefinitionList != null && !processDefinitionList.isEmpty()){
				int i = 0;
				for(ProcessDefinition p : processDefinitionList){
					CssLayout item = initProcessItem(p, ++i);
					if(item != null){
						dashboardBlockContent.addComponent(item);
					}
					
				}
			}else{
				isEmpty  =true;
			}
			
		}else {
			isEmpty  =true;
		}
		
		if(isEmpty){
			Label emptyLbl = new Label("Không có quy trình nào!");
			emptyLbl.setStyleName(ExplorerLayout.PROCESS_EMPTY);
			dashboardBlockContent.addComponent(emptyLbl);
		}
    }
    
    public CssLayout initProcessItem(ProcessDefinition p, int i){
		if(p != null){
			CssLayout processItem = new  CssLayout();
			processItem.setStyleName(ExplorerLayout.PROCESS_ITEM);
			
			CssLayout taskNumberWrap = new CssLayout();
			taskNumberWrap.setStyleName(ExplorerLayout.PROCESS_ITEM_NUMBER_WRAP);
			processItem.addComponent(taskNumberWrap);
			
			Label number = new Label(i+"");
			number.setStyleName(ExplorerLayout.PROCESS_ITEM_NUMBER);
			taskNumberWrap.addComponent(number);
			
			CssLayout taskInfoWrap = new CssLayout();
			taskInfoWrap.setStyleName(ExplorerLayout.PROCESS_ITEM_INFO_WRAP);
			processItem.addComponent(taskInfoWrap);
			
			
			String nameStr = "#Không tên";
			if(p.getName() != null && !p.getName().isEmpty()){
				nameStr = p.getName();
			}
			Button name = new Button(nameStr);
			name.addStyleName(Reindeer.BUTTON_LINK);
			name.addStyleName(ExplorerLayout.PROCESS_ITEM_NAME);
			name.setId(BTN_PROCESS_DETAIL_ID);
			name.addClickListener(this);
			name.setData(p);
			taskInfoWrap.addComponent(name);
			
			if(p.getDeploymentId() != null){
				Deployment dep = repositoryService.createDeploymentQuery().deploymentId(p.getDeploymentId()).singleResult();
				if(dep != null && dep.getDeploymentTime() != null){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
					
					Label createTime = new Label(df.format(dep.getDeploymentTime()));
					createTime.setStyleName(ExplorerLayout.NEW_TASK_CREATE_TIME);
					taskInfoWrap.addComponent(createTime);
				}
			}
			
			return processItem;
		}
		return null;
	}
    
    
    
@Override
public void buttonClick(ClickEvent event) {
	try {
		Button button = event.getButton();
		if(BTN_TASK_ASSIGN_ID.equals(button.getId())){
			Task current = (Task) button.getData();
			if(current != null){
				UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+current.getId());
			}
		}else if(BTN_OWNER_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(ArchivedView.VIEW_NAME);
		}else if(BTN_FINISHED_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(FinishedView.VIEW_NAME);
		}else if(BTN_TASK_QUEUE_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(QueueView.VIEW_NAME);
		}else if(BTN_CURRENT_TASK_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(InboxView.VIEW_NAME);
		}else if(BTN_PROCESS_DETAIL_ID.equals(button.getId())){
			ProcessDefinition p = (ProcessDefinition) button.getData();
			if(p != null){
				UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+p.getId());
			}
		}else if(BTN_PROCESS_LIST_VIEW_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionListView.VIEW_NAME);
		}else if(BTN_TASK_LIST_VIEW_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(InboxView.VIEW_NAME);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
    
}
}
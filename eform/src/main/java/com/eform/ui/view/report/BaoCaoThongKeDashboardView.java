package com.eform.ui.view.report;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.activiti.ActivitiEngineConfiguration;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.service.BaoCaoThongKeService;
import com.eform.service.HtThamSoService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = BaoCaoThongKeDashboardView.VIEW_NAME)
public class BaoCaoThongKeDashboardView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "thong-ke-tong-hop";
	private static final String MAIN_TITLE = "Thống kê tổng hợp";
	private static final String SMALL_TITLE = "";
	private static final String BTN_APPLY_ID = "btn-apply";

	@Autowired
	private BaoCaoThongKeService baoCaoThongKeService;
	private CssLayout mainTitleWrap;
	@Autowired
	private HtThamSoService htThamSoService;
	
	@Autowired
	private RepositoryService repositoryService;	
	
	private CssLayout dataWrap;
	
	private final FieldGroup fieldGroup = new FieldGroup();
	private final PropertysetItem itemValue = new PropertysetItem();
	private final Logger log = LoggerFactory.getLogger(ActivitiEngineConfiguration.class);
	
	@Autowired
	private MessageSource messageSource;
	
	@PostConstruct
    void init() {
		fieldGroup.setItemDataSource(itemValue);
		initMainTitle();
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		initFilter();
		initProcessReport();
		
		
	}
	
	
	public void initMainTitle(){
		PageBreadcrumbInfo home = new PageBreadcrumbInfo();
    	home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
    	home.setViewName(DashboardView.VIEW_NAME);
    	
    	PageBreadcrumbInfo current = new PageBreadcrumbInfo();
    	current.setTitle(MAIN_TITLE);
    	current.setViewName(VIEW_NAME);		
    			
    	addComponent(new PageBreadcrumbLayout(home, current));
		
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
	
	public void initFilter(){
		CssLayout filterContainer = new CssLayout();
		filterContainer.setWidth("100%");
		filterContainer.setStyleName(ExplorerLayout.FILTER_WRAP);
		addComponent(filterContainer);
		
		CssLayout processFilter = new CssLayout();
		processFilter.setStyleName(ExplorerLayout.PROCESS_FILTER_WRAP);
		filterContainer.addComponent(processFilter);
		
		List<ProcessDefinition> processDefinitionList = repositoryService.createProcessDefinitionQuery().orderByProcessDefinitionName().asc().list();
		BeanItemContainer<ProcessDefinition> processContainer = new BeanItemContainer<ProcessDefinition>(ProcessDefinition.class, processDefinitionList);
		ComboBox cbbProcess = new ComboBox("Quy trình", processContainer);
		cbbProcess.setWidth("100%");
		cbbProcess.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbProcess.setItemCaptionPropertyId("name");
		processFilter.addComponent(cbbProcess);
		
		itemValue.addItemProperty("process", new ObjectProperty<ProcessDefinition>(null, ProcessDefinition.class));
    	fieldGroup.bind(cbbProcess, "process");
		
		CssLayout dinhKyFilter = new CssLayout();
		dinhKyFilter.setStyleName(ExplorerLayout.PERIODIC_FILTER_WRAP);
		filterContainer.addComponent(dinhKyFilter);
		ComboBox cbbDinhKy = new ComboBox("Định kỳ");
		cbbDinhKy.setWidth("100%");
		dinhKyFilter.addComponent(cbbDinhKy);
		for(CommonUtils.EDinhKyTongHop item : CommonUtils.EDinhKyTongHop.values()){
			cbbDinhKy.addItem(new Long(item.getCode()));
			cbbDinhKy.setItemCaption(new Long(item.getCode()), item.getName());
		}
		
		
		itemValue.addItemProperty("dinhKy", new ObjectProperty<Long>(new Long(CommonUtils.EDinhKyTongHop.NGAY.getCode()), Long.class));
    	fieldGroup.bind(cbbDinhKy, "dinhKy");
		
    	CssLayout dateFilter = new CssLayout();
		dateFilter.setStyleName(ExplorerLayout.DATE_FILTER_WRAP);
		filterContainer.addComponent(dateFilter);
		
		CssLayout fromDateFilter = new CssLayout();
		fromDateFilter.setStyleName(ExplorerLayout.FROM_DATE_FILTER_WRAP);
		dateFilter.addComponent(fromDateFilter);
    	
		DateField fromDate = new DateField("Từ ngày");
		fromDate.setWidth("100%");
		fromDate.setRangeEnd(new Date());
		fromDateFilter.addComponent(fromDate);
		itemValue.addItemProperty("from", new ObjectProperty<Date>(null, Date.class));
    	fieldGroup.bind(fromDate, "from");
		
		CssLayout toDateFilter = new CssLayout();
		toDateFilter.setStyleName(ExplorerLayout.TO_DATE_FILTER_WRAP);
		dateFilter.addComponent(toDateFilter);
		
		DateField toDate = new DateField("Đến ngày");
		toDate.setRangeEnd(new Date());
		toDate.setWidth("100%");
		toDateFilter.addComponent(toDate);
		itemValue.addItemProperty("to", new ObjectProperty<Date>(null, Date.class));
    	fieldGroup.bind(toDate, "to");
    	
    	cbbDinhKy.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Long value = (Long) event.getProperty().getValue();
				if(value != null){
					if(value.intValue() == CommonUtils.EDinhKyTongHop.NGAY.getCode()){
						toDate.setResolution(Resolution.DAY);
						toDate.setCaption("Đến ngày");
						fromDate.setResolution(Resolution.DAY);
						fromDate.setCaption("Từ ngày");
					}else if(value.intValue() == CommonUtils.EDinhKyTongHop.THANG.getCode()){
						toDate.setResolution(Resolution.MONTH);
						toDate.setCaption("Đến tháng");
						fromDate.setResolution(Resolution.MONTH);
						fromDate.setCaption("Từ tháng");
					}else if(value.intValue() == CommonUtils.EDinhKyTongHop.NAM.getCode()){
						toDate.setResolution(Resolution.YEAR);
						toDate.setCaption("Đến năm");
						fromDate.setResolution(Resolution.YEAR);
						fromDate.setCaption("Từ năm");
					}else{
						toDate.setResolution(Resolution.DAY);
						toDate.setCaption("Đến ngày");
						fromDate.setResolution(Resolution.DAY);
						fromDate.setCaption("Từ ngày");
					}
				}
			}
		});
		
		CssLayout submitFilter = new CssLayout();
		submitFilter.setStyleName(ExplorerLayout.SUBMIT_FILTER_WRAP);
		filterContainer.addComponent(submitFilter);
		
		Button button = new Button("Áp dụng");
		submitFilter.addComponent(button);
		button.setId(BTN_APPLY_ID);
		button.addClickListener(this); 
		
	}
	
	public void initProcessReport(){
		dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
		String path = CommonUtils.getRootPath()+"/report/process";
		
		System.out.println("-----------path: "+path);
		log.debug("-----------path--------: "+path);
		BrowserFrame browser = new BrowserFrame(null, new ExternalResource(path));
    	browser.setWidth("100%");
    	browser.setHeight("520px");
		dataWrap.addComponent(browser);
    }

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BTN_APPLY_ID.equals(button.getId())){
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					ProcessDefinition process = (ProcessDefinition) itemValue.getItemProperty("process").getValue();
					Date from = (Date) itemValue.getItemProperty("from").getValue();
					Date to = (Date) itemValue.getItemProperty("to").getValue();
					Long dinhKy = (Long) itemValue.getItemProperty("dinhKy").getValue();
					
					String param = "";
					if(process != null){
						param += "?p="+process.getId();
					}
					if(from != null){
						if(param.isEmpty()){
							param +="?";
						}else{
							param+="&";
						}
						param += "f="+from.getTime();
					}
					if(to != null){
						if(param.isEmpty()){
							param +="?";
						}else{
							param+="&";
						}
						param += "t="+to.getTime();
					}
					if(dinhKy != null){
						if(param.isEmpty()){
							param +="?";
						}else{
							param+="&";
						}
						param += "d="+dinhKy;
					}
					String path = CommonUtils.getRootPath()+"/report/process"+param;
					BrowserFrame browser = new BrowserFrame(null, new ExternalResource(path));
			    	browser.setWidth("100%");
			    	browser.setHeight("520px");
			    	dataWrap.removeAllComponents();
					dataWrap.addComponent(browser);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}

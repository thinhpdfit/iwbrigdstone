package com.eform.ui.view.system;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.ExportUtils;
import com.eform.common.utils.ModelerUtils;
import com.eform.model.entities.BieuMau;
import com.eform.service.BieuMauService;
import com.eform.service.HtThamSoService;
import com.eform.sync.model.entities.SyncBieuMau;
import com.eform.sync.model.entities.SyncLoaiDanhMuc;
import com.eform.sync.service.SynchronizationService;
import com.eform.sync.type.SyncModel;
import com.eform.sync.type.SyncResultModel;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;


@SpringView(name = SynchonizationView.VIEW_NAME)
public class SynchonizationView  extends VerticalLayout implements View, ClickListener{
	
	public static final String VIEW_NAME = "dong-bo";
	public static final String MAIN_TITLE = "Đồng bộ ứng dụng";
	private static final String SMALL_TITLE = "";
	private static final String BUTTON_IMPORT_ID = "btnImport";
	private static final String BUTTON_IMPORT_FORM_ID = "btnImportForm";

	@Autowired
	private HtThamSoService htThamSoService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private BieuMauService bieuMauService;
	@Autowired
	private SynchronizationService synchronizationService;
	
	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();

	private CssLayout formContainer;
	
	@PostConstruct
    void init() {
    	fieldGroup.setItemDataSource(itemValue);
    	initMainTitle();
    	
    }

    @Override
    public void enter(ViewChangeEvent event) {
    	UI.getCurrent().getPage().setTitle(MAIN_TITLE);
    	initDetailForm();
    }
	
	 public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
	 
	 public void initDetailForm(){
		formContainer = new CssLayout();
    	formContainer.setWidth(100, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	FormLayout form = new FormLayout();
    	form.setMargin(true);
    	ComboBox cbbSiteList = new ComboBox("Hệ thống");
    	cbbSiteList.setWidth("100%");
    	cbbSiteList.setRequired(true);
    	cbbSiteList.setRequiredError("Không được để trống!");
    	cbbSiteList.setNullSelectionAllowed(true);
    	cbbSiteList.addStyleName(ExplorerLayout.FORM_CONTROL);
    	String siteStr = htThamSoService.getHtThamSoValue("PRODUCT_SITE_LIST");
    	if(siteStr != null && !siteStr.isEmpty()){
    		String siteArr[] = siteStr.split(";");
    		if(siteStr != null && siteStr.length() > 0){
    			for (String str : siteArr) {
    				cbbSiteList.addItem(str);
    				cbbSiteList.setItemCaption(str, str);
				}
    		}
    	}
    	
    	itemValue.addItemProperty("site", new ObjectProperty<String>(null, String.class));
		fieldGroup.bind(cbbSiteList, "site");
		
		form.addComponent(cbbSiteList);
		
		List<Model> modelList = repositoryService.createModelQuery().orderByCreateTime().desc().list();
		ComboBox cbbModelList = new ComboBox("Quy trình");
		cbbModelList.setWidth("100%");
		cbbModelList.setNullSelectionAllowed(true);
		cbbModelList.addStyleName(ExplorerLayout.FORM_CONTROL);
		BeanItemContainer container = new BeanItemContainer<Model>(Model.class, modelList);
		cbbModelList.setContainerDataSource(container);
		cbbModelList.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbModelList.setItemCaptionPropertyId("name");
		
		itemValue.addItemProperty("model", new ObjectProperty<Model>(null, Model.class));
		fieldGroup.bind(cbbModelList, "model");
		
		form.addComponent(cbbModelList);
		
		HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	Button btnImport = new Button();
    	btnImport.addClickListener(this);
    	btnImport.setCaption("Đồng bộ quy trình");
    	btnImport.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
    	btnImport.setId(BUTTON_IMPORT_ID);
		buttons.addComponent(btnImport);
    	form.addComponent(buttons);
    	
    	
    	List<BieuMau> formList = bieuMauService.getBieuMauFind(null, null, null, null, -1, -1);
		ComboBox cbbForm = new ComboBox("Biểu mẫu");
		cbbForm.setWidth("100%");
		cbbForm.setNullSelectionAllowed(true);
		cbbForm.addStyleName(ExplorerLayout.FORM_CONTROL);
		BeanItemContainer fContainer = new BeanItemContainer<BieuMau>(BieuMau.class, formList);
		cbbForm.setContainerDataSource(fContainer);
		cbbForm.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbForm.setItemCaptionPropertyId("ten");
		
		itemValue.addItemProperty("bieuMau", new ObjectProperty<BieuMau>(null, BieuMau.class));
		fieldGroup.bind(cbbForm, "bieuMau");
		
		form.addComponent(cbbForm);
		
		
		HorizontalLayout buttons2 = new HorizontalLayout();
		buttons2.setSpacing(true);
		buttons2.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	Button btnImportForm = new Button();
    	btnImportForm.addClickListener(this);
    	btnImportForm.setCaption("Đồng bộ biểu mẫu");
    	btnImportForm.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
    	btnImportForm.setId(BUTTON_IMPORT_FORM_ID);
    	buttons2.addComponent(btnImportForm);
    	form.addComponent(buttons2);

    	formContainer.addComponent(form);
    	addComponent(formContainer);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BUTTON_IMPORT_ID.equals(button.getId())){
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					String site = (String) itemValue.getItemProperty("site").getValue();
					Model model = (Model) itemValue.getItemProperty("model").getValue();
					if(site != null && model != null){
						String bieuMauExport = ExportUtils.exportModeler(model.getId());
						JsonNode editorNode = ModelerUtils.getEditorJson(model.getId());
						String bpmnModelStr = editorNode.toString();
						
						SyncModel data = new SyncModel();
						data.setBieuMauStr(bieuMauExport);
						data.setModolerStr(bpmnModelStr);
						
						ObjectMapper mapper = new ObjectMapper();
						String requestBody = mapper.writeValueAsString(data);
						
						ResteasyClient client = new ResteasyClientBuilder().build();
			            ResteasyWebTarget target = client.target(site+"/sync/");
			            Response response = target.request("application/json").post(Entity.entity(requestBody, "application/json"));
			            
			            if(response.getStatus() != 200){
			            	Notification notf = new Notification("Thông báo", "HTTP error code: "+response.getStatus(), Notification.Type.WARNING_MESSAGE);
							notf.setDelayMsec(20000);
							notf.setPosition(Position.TOP_CENTER);
							notf.show(Page.getCurrent());
			            	return;
			            }
			            
			            String resultStr = response.readEntity(String.class);
			            
			            if(resultStr != null){
			            	SyncResultModel result = mapper.readValue(resultStr, SyncResultModel.class);
			            	if(result.getSuccess() != null && result.getSuccess()){
			            		Notification notf = new Notification("Thông báo", "Đồng bộ thành công!", Notification.Type.TRAY_NOTIFICATION);
								notf.setDelayMsec(3000);
								notf.setPosition(Position.TOP_CENTER);
								notf.show(Page.getCurrent());
			            	}else{
			            		System.out.println("error");
			            		Notification notf = new Notification("Thông báo", "Đồng bộ không thành công!\n"+result.getLog(), Notification.Type.WARNING_MESSAGE);
								notf.setDelayMsec(20000);
								notf.setPosition(Position.TOP_CENTER);
								notf.show(Page.getCurrent());
			            	}
			            }
					}
				}
			}else if(BUTTON_IMPORT_FORM_ID.equals(button.getId())){
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					String site = (String) itemValue.getItemProperty("site").getValue();
					BieuMau bieuMau = (BieuMau) itemValue.getItemProperty("bieuMau").getValue();
					if(site != null && bieuMau != null){
						List<SyncBieuMau> bieuMauList = synchronizationService.findBieuMauByCode(bieuMau.getMa());
						String bieuMauExport = ExportUtils.exportBieuMauAll(bieuMauList, null);
						
						SyncModel data = new SyncModel();
						data.setBieuMauStr(bieuMauExport);
						
						ObjectMapper mapper = new ObjectMapper();
						String requestBody = mapper.writeValueAsString(data);
						
						ResteasyClient client = new ResteasyClientBuilder().build();
			            ResteasyWebTarget target = client.target(site+"/sync/");
			            Response response = target.request("application/json").post(Entity.entity(requestBody, "application/json"));
			            
			            if(response.getStatus() != 200){
			            	Notification notf = new Notification("Thông báo", "HTTP error code: "+response.getStatus(), Notification.Type.WARNING_MESSAGE);
							notf.setDelayMsec(20000);
							notf.setPosition(Position.TOP_CENTER);
							notf.show(Page.getCurrent());
			            	return;
			            }
			            
			            String resultStr = response.readEntity(String.class);
			            
			            if(resultStr != null){
			            	SyncResultModel result = mapper.readValue(resultStr, SyncResultModel.class);
			            	if(result.getSuccess() != null && result.getSuccess()){
			            		Notification notf = new Notification("Thông báo", "Đồng bộ thành công!", Notification.Type.TRAY_NOTIFICATION);
								notf.setDelayMsec(3000);
								notf.setPosition(Position.TOP_CENTER);
								notf.show(Page.getCurrent());
			            	}else{
			            		System.out.println("error");
			            		Notification notf = new Notification("Thông báo", "Đồng bộ không thành công!\n"+result.getLog(), Notification.Type.WARNING_MESSAGE);
								notf.setDelayMsec(20000);
								notf.setPosition(Position.TOP_CENTER);
								notf.show(Page.getCurrent());
			            	}
			            }
					}
				}
			}
		} catch (Exception e) {
			Notification notf = new Notification("Thông báo", e.getMessage()+"\nCause by: "+e.getCause().getMessage(), Notification.Type.WARNING_MESSAGE);
			notf.setDelayMsec(20000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
			e.printStackTrace();
		}
	}

}

package com.eform.ui.view.report.type;

import java.io.Serializable;

import com.eform.model.entities.TruongThongTin;

public class GroupObject implements Serializable{
	private TruongThongTin truongThongTin;
	private String path;
	private String aliasName;
	
	public TruongThongTin getTruongThongTin() {
		return truongThongTin;
	}
	public void setTruongThongTin(TruongThongTin truongThongTin) {
		this.truongThongTin = truongThongTin;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getAliasName() {
		return aliasName;
	}
	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}
	
	
	
}

package com.eform.ui.view.report;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.TemporaryFileDownloadResource;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.ChartUtils;
import com.eform.common.utils.CommonUtils;
import com.eform.common.workflow.ITrangThai;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BaoCaoThongKeService;
import com.eform.service.BieuMauService;
import com.eform.service.HoSoService;
import com.eform.service.HtDonViServices;
import com.eform.service.TruongThongTinService;
import com.eform.ui.custom.ButtonAction;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.StatusCommentLayout;
import com.eform.ui.custom.paging.BaoCaoThongKePaginationBar;
import com.eform.ui.view.dashboard.DashboardView;
import com.eform.ui.view.report.type.FilterObject;
import com.eform.ui.view.report.type.v2.VisibleColumn;
import com.eform.ui.view.report.utils.ReportUtils;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = BaoCaoThongKeView.VIEW_NAME)
public class BaoCaoThongKeView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "thong-ke";
	private static final String MAIN_TITLE = "Thống kê";
	private static final String SMALL_TITLE = "";

	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	private static final String BTN_VIEW_ID = "xem";
	private static final String BTN_ADDNEW_ID = "themMoi";
	
	private static final String BUTTON_DOWNLOAD_EXCEL_ID = "download-excel";
	private final BaoCaoThongKeView currentView = this;
	
	@Autowired
	BaoCaoThongKeService baoCaoThongKeService;
	private BeanItemContainer<BaoCaoThongKe> container;
	private Table table = new Table();
	private WorkflowManagement<BaoCaoThongKe> baoCaoThongKeWM;
	
	private final PropertysetItem searchItem = new PropertysetItem();
	private final FieldGroup searchFieldGroup = new FieldGroup();
	private Button btnAddNew;
	
	private BaoCaoThongKePaginationBar paginationBar;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private TruongThongTinService truongThongTinService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private BieuMauService bieuMauService;
	@Autowired
	private HoSoService hoSoService;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private HtDonViServices htDonViServices;
	
	private Window htmlTableWindow;
	private Label contentHtml;

	private Map<String, Object> selectedRow;
	private HtDonVi htDonViCurrent;
	private List<HtDonVi> htDonViList;
	
	private Window tableResultWindow;
	private HorizontalLayout tableContainer;
	
	@PostConstruct
    void init() {
		String userId = CommonUtils.getUserLogedIn();
		String maDonVi = identityService.getUserInfo(userId, "HT_DON_VI");
		if(maDonVi != null){
			List<HtDonVi> dvList = htDonViServices.getHtDonViFind(maDonVi,null, null, null, 0, 1);
			if(!dvList.isEmpty()){
				htDonViCurrent = dvList.get(0);
			}
		}
		
		
		baoCaoThongKeWM = new WorkflowManagement<BaoCaoThongKe>(EChucNang.E_FORM_LOAI_DANH_MUC.getMa());
		
		container = new BeanItemContainer<BaoCaoThongKe>(BaoCaoThongKe.class, new ArrayList());
		htDonViList = new ArrayList();
		
		if(htDonViCurrent != null){
			//htDonViList.addAll(getHtDonViCon(htDonViCurrent));
			htDonViList.add(htDonViCurrent);
		}
		paginationBar = new BaoCaoThongKePaginationBar(container, baoCaoThongKeService, htDonViList);
    	
    	searchItem.addItemProperty("ma", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("ten", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("trangThai", new ObjectProperty<ComboboxItem>(new ComboboxItem()));
    	searchFieldGroup.setItemDataSource(searchItem);
    	selectedRow = new HashMap();
    	
		initMainTitle();
		initSearchForm();
		initDataContent();
		
		initTableResultViewer();
		
		btnAddNew = new Button();
    	btnAddNew.setIcon(FontAwesome.PLUS);
    	btnAddNew.addClickListener(this);
    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
    	btnAddNew.setDescription("Tạo mới báo cáo thống kê");
    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
    	btnAddNew.setId(BTN_ADDNEW_ID);
		addComponent(btnAddNew);
		
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	public void initTableResultViewer(){
		VerticalLayout content = new VerticalLayout();
		content.setWidth("100%");
    	content.setSpacing(true);
    	content.setMargin(true);
		
    	tableContainer = new HorizontalLayout();
    	tableContainer.setWidth("100%");
    	
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	buttons.addStyleName(ExplorerLayout.BUTTONS_CENTER);
    	
    	Button btnDownloadExcel = new Button();
		
		btnDownloadExcel.setCaption("Tải excel");
		btnDownloadExcel.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnDownloadExcel.setId(BUTTON_DOWNLOAD_EXCEL_ID);
		btnDownloadExcel.addClickListener(this);
		buttons.addComponent(btnDownloadExcel);
		
		content.addComponent(tableContainer);
		content.addComponent(buttons);
    	
		tableResultWindow = new Window("Báo cáo thống kê");
		tableResultWindow.setWidth(100, Unit.PERCENTAGE);
		tableResultWindow.setHeight(100, Unit.PERCENTAGE);
		tableResultWindow.setResizable(true);
		tableResultWindow.center();
		tableResultWindow.setModal(true);
		tableResultWindow.setContent(content);
    }
	
	public void initMainTitle(){
		PageBreadcrumbInfo home = new PageBreadcrumbInfo();
    	home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
    	home.setViewName(DashboardView.VIEW_NAME);
    	
    	PageBreadcrumbInfo current = new PageBreadcrumbInfo();
    	current.setTitle(MAIN_TITLE);
    	current.setViewName(VIEW_NAME);		
    			
    	addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		
		CssFormLayout searchForm = new CssFormLayout(-1, 60);
		
		TextField txtMaBaoCaoThongKe = new TextField("Mã báo cáo thống kê");
		searchForm.addFeild(txtMaBaoCaoThongKe);
		
		TextField txtTenBaoCaoThongKe = new TextField("Tên báo cáo thống kê");
		searchForm.addFeild(txtTenBaoCaoThongKe);
		
    	
    	List<ITrangThai> trangThaiListTmp = baoCaoThongKeWM.getTrangThaiList();
    	List<ComboboxItem> trangThaiBaoCaoThongKeList = new ArrayList<>();
    	if(trangThaiListTmp != null){
    		for (ITrangThai trangThai : trangThaiListTmp) {
    			trangThaiBaoCaoThongKeList.add(new ComboboxItem(trangThai.getTen(), new Long(trangThai.getId())));
			}
    	}
    	
    	BeanItemContainer<ComboboxItem> cbbTrangThaiContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, trangThaiBaoCaoThongKeList);
		ComboBox cbbTrangThai = new ComboBox("Trạng thái", cbbTrangThaiContainer);
		cbbTrangThai.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbTrangThai.setItemCaptionPropertyId("text");
    	searchForm.addFeild(cbbTrangThai);

		searchFieldGroup.bind(txtMaBaoCaoThongKe, "ma");
		searchFieldGroup.bind(txtTenBaoCaoThongKe, "ten");
		searchFieldGroup.bind(cbbTrangThai, "trangThai");
		
		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(currentView);
		searchForm.addButton(btnSearch);
		paginationBar.fixEnterHotKey(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(currentView);
		searchForm.addButton(btnRefresh);
		
		addComponent(searchForm);
	}
	
	public void initDataContent(){
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
		List<ITrangThai> trangThaiList = baoCaoThongKeWM.getTrangThaiList();
		if(trangThaiList != null && !trangThaiList.isEmpty()){
			dataWrap.addComponent(new StatusCommentLayout(trangThaiList));
		}
		
		
		table.setContainerDataSource(container);
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setResponsive(true);
		table.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		table.setColumnHeader("ma", "Mã");
		table.setColumnHeader("ten", "Tên");
		
		
		table.addGeneratedColumn("action", new ColumnGenerator() { 
			private static final long serialVersionUID = 1L;

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				BaoCaoThongKe current = (BaoCaoThongKe) itemId;
				Workflow<BaoCaoThongKe> wf = baoCaoThongKeWM.getWorkflow(current);
				
				HorizontalLayout actionWrap = new HorizontalLayout();
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
		        Button btnView = new Button();
		        btnView.setId(BTN_VIEW_ID);
		        btnView.setDescription("Xem");
		        btnView.setIcon(FontAwesome.EYE);
		        btnView.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnView.addClickListener(currentView);
		        btnView.setData(itemId);
		        btnView.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnView);

		        if(current != null && (htDonViCurrent == null || (current.getHtDonVi() != null && current.getHtDonVi().equals(htDonViCurrent)))){
		        	new ButtonAction<BaoCaoThongKe>().getButtonAction(actionWrap, wf, currentView);
		        }
		        
		        
		        return actionWrap;
		    }
		});
		table.setColumnWidth("action", 220);

		table.setColumnHeader("action", "Thao tác");
		table.setRowHeaderMode(RowHeaderMode.INDEX);

		table.setVisibleColumns("ma", "ten", "action");
		

		table.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if ("ma".equals(propertyId)) {
                    return "left-aligned";
                }else if("ten".equals(propertyId)){
                    return "left-aligned";
                }else if("baoCaoThongKe".equals(propertyId)){
                    return "left-aligned";
                }else if("action".equals(propertyId)){
                    return "left-aligned";
                }
				BaoCaoThongKe current = (BaoCaoThongKe) itemId;
				Workflow<BaoCaoThongKe> wf = baoCaoThongKeWM.getWorkflow(current);
                return wf.getStyleName();
			}
			
		});

		dataWrap.addComponent(table);
		dataWrap.addComponent(paginationBar);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			if(BTN_SEARCH_ID.equals(event.getButton().getId())){
				try {
					if(searchFieldGroup.isValid()){
						searchFieldGroup.commit();
						String ma = (String) searchItem.getItemProperty("ma").getValue();
						ma = ma != null ? "%"+ma.trim()+"%" : null;
						String ten = (String) searchItem.getItemProperty("ten").getValue();
						ten = ten != null ? "%"+ten.trim()+"%" : null;
						ComboboxItem trangThai = (ComboboxItem) searchItem.getItemProperty("trangThai").getValue();
						List<Long> trangThaiList = null;
						if(trangThai != null && trangThai.getValue() != null){
							trangThaiList = new ArrayList<Long>();
							trangThaiList.add((Long) trangThai.getValue());
						}
						
						paginationBar.search(ma, ten, htDonViList, trangThaiList);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
		    	searchFieldGroup.clear();
				paginationBar.search(null, null, htDonViList, null);
			}else if(BTN_VIEW_ID.equals(event.getButton().getId())){
				BaoCaoThongKe baoCaoThongKeCurrent = (BaoCaoThongKe) event.getButton().getData();
	    		List<VisibleColumn> visibleColumnList = new ArrayList<VisibleColumn>();
				List<FilterObject> filterObjectList = new ArrayList<FilterObject>();
				List<String> maBieuMauList = new ArrayList();
				
	    		JSONObject giaTri = baoCaoThongKeCurrent.getGiaTri();
	    		JSONArray groupArr = giaTri.getJSONArray("visible");
	    		if(groupArr != null){
	    			for(int i =0 ; i< groupArr.length(); i++){
	    				JSONObject json = groupArr.getJSONObject(i);
	    				VisibleColumn obj= new VisibleColumn();
	    				obj.setTenHienThi(json.getString("name"));
	    				String maTruongThongTin = json.getString("key");
	    				if(maTruongThongTin != null){
	    					List<TruongThongTin> list = truongThongTinService.findByCode(maTruongThongTin);
	    					if(list != null && !list.isEmpty()){
	    						obj.setTruongThongTin(list.get(0));
	    					}
	    				}
	    				visibleColumnList.add(obj);
	        		}
	    		}
	    		JSONArray filterArr = giaTri.getJSONArray("filter");
	    		if(filterArr != null){
	    			for(int i =0 ; i< filterArr.length(); i++){
	    				JSONObject json = filterArr.getJSONObject(i);
	    				FilterObject obj= new FilterObject();
	    				obj.setPath(json.getString("path"));
	    				obj.setComparison(json.getLong("comparison"));
	    				Long kieuDL = json.getLong("kieudulieu");
	    				obj.setKieuDuLieu(kieuDL);
	    				obj.setName(json.getString("name"));
	    				obj.setValue(json.get("value"));
	    				
	    				filterObjectList.add(obj);
	        		}
	    		}
	    		JSONArray formCodeArr = giaTri.getJSONArray("form_code");
	    		if(formCodeArr != null){
	    			for(int i =0 ; i< formCodeArr.length(); i++){
	    				maBieuMauList.add(formCodeArr.getString(i));
	    			}
	    		}
	    		List<String> processInstanceIdList = new ArrayList<String>();
				if (baoCaoThongKeCurrent.getProcessDefId() != null) {
					ProcessDefinition processDef =  repositoryService.createProcessDefinitionQuery()
							.processDefinitionId(baoCaoThongKeCurrent.getProcessDefId()).singleResult();
					List<HistoricProcessInstance> historicProInsList = historyService.createHistoricProcessInstanceQuery().processDefinitionId(processDef.getId()).list();
					if(historicProInsList != null && !historicProInsList.isEmpty()){
						for(HistoricProcessInstance h : historicProInsList){
							processInstanceIdList.add(h.getId());
						}
					}
				}
				
				
				Date fromDate = baoCaoThongKeCurrent.getTuNgay();
				Date toDate = baoCaoThongKeCurrent.getDenNgay();
				String tieuDe = baoCaoThongKeCurrent.getTieuDe();
				BieuMau bieuMau = baoCaoThongKeCurrent.getBieuMau();
				
				Map<String, Object> bieuMauTreeMap = ReportUtils.getBieuMauTreeMap(bieuMau, maBieuMauList);
				Map<String, Object> paramMap = new HashMap();
				String sql = ReportUtils.createSqlString(bieuMauTreeMap, processInstanceIdList, filterObjectList, visibleColumnList, fromDate, toDate, paramMap);
				List<Map<String, Object>> resultList = hoSoService.getReportResult(sql, paramMap);
				
				Table resultTable = ReportUtils.createTableViewer(resultList, visibleColumnList);
				tableContainer.removeAllComponents();
				tableContainer.addComponent(resultTable);
				tableResultWindow.setCaption(tieuDe);
				UI.getCurrent().addWindow(tableResultWindow);
				
				selectedRow.clear();
				selectedRow.put("results", resultList);
				selectedRow.put("tieuDe", tieuDe);
				selectedRow.put("visibleColumnList", visibleColumnList);
				
				
			}else if(BUTTON_DOWNLOAD_EXCEL_ID.equals(event.getButton().getId())){
				List<Map<String, Object>> results = (List<Map<String, Object>>) selectedRow.get("results");
				String tieuDe = (String) selectedRow.get("tieuDe");
				List<VisibleColumn> visibleColumnList = (List<VisibleColumn>) selectedRow.get("visibleColumnList");
				
				
				Workbook workbook = ReportUtils.exportExcel(tieuDe, results, visibleColumnList);
				File tempFile = File.createTempFile("tmp", ".xlsx");
				FileOutputStream fos = new FileOutputStream(tempFile);
				workbook.write(fos);
				fos.close();
				String fileName = "BCTK";
				if(tieuDe != null){
					fileName = tieuDe;
				}
			    TemporaryFileDownloadResource resourcetmp = new TemporaryFileDownloadResource(fileName+".xlsx", "application/vnd.ms-excel", tempFile);
			    UI.getCurrent().getPage().open(resourcetmp, "Báo cáo thống kê Excel", false);
			}else if(ButtonAction.BTN_EDIT_ID.equals(event.getButton().getId())){
				BaoCaoThongKe currentRow = (BaoCaoThongKe) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeDetailNewView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getMa().toLowerCase());
			}else if(ButtonAction.BTN_DEL_ID.equals(event.getButton().getId())){
				BaoCaoThongKe currentRow = (BaoCaoThongKe) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeDetailNewView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getMa().toLowerCase());
			}else if(ButtonAction.BTN_APPROVE_ID.equals(event.getButton().getId())){
				BaoCaoThongKe currentRow = (BaoCaoThongKe) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeDetailNewView.VIEW_NAME+"/"+Constants.ACTION_APPROVE+"/"+currentRow.getMa().toLowerCase());
			}else if(ButtonAction.BTN_REFUSE_ID.equals(event.getButton().getId())){
				BaoCaoThongKe currentRow = (BaoCaoThongKe) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeDetailNewView.VIEW_NAME+"/"+Constants.ACTION_REFUSE+"/"+currentRow.getMa().toLowerCase());
			}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
		        UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeDetailNewView.VIEW_NAME+"/"+Constants.ACTION_ADD);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

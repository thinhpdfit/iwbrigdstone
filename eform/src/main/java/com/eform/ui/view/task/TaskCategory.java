package com.eform.ui.view.task;

public interface TaskCategory {
	public static final String ARCHIVED_CATE = "archived";
	public static final String FINISHED_CATE = "finished";
	public static final String CANDIDATE_FINISHED_CATE = "candidate-finished";
}

package com.eform.ui.view.system;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.HtQuyenDao;
import com.eform.model.entities.HtQuyen;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;

@SpringView(name = HtQuyenDetailView.VIEW_NAME)
public class HtQuyenDetailView extends VerticalLayout implements View, ClickListener {

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "chi-tiet-quyen";
	private static final String MAIN_TITLE = "Chi tiết quyền";
	private static final String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_APPROVE_ID = "btn-approve";
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_ADD_ID = "btn-edit";
	private static final String BUTTON_DEL_ID = "btn-del";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_REFUSE_ID = "btn-refuse";
	
	private HtQuyen htQuyenCurrent;
	private WorkflowManagement<HtQuyen> htQuyenWM;
	Workflow<HtQuyen> htQuyenWf;;
	private String action;
	
	private final FieldGroup fieldGroup = new FieldGroup();
	private CssLayout formContainer;
	private Button btnUpdate;
	private Button btnBack;
	private HtQuyen parent;
	@Autowired
	private MessageSource messageSource;
	

	@Autowired
	private HtQuyenDao htQuyenDao;

    @PostConstruct
    void init() {
    	htQuyenWM = new WorkflowManagement<HtQuyen>(EChucNang.HT_QUYEN.getMa());
    	initMainTitle();
    }

    @Override
    public void enter(ViewChangeEvent event) {
    	UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				if(Constants.ACTION_ADD.equals(action)){
					htQuyenCurrent = new HtQuyen();
					htQuyenWf = htQuyenWM.getWorkflow(htQuyenCurrent);
					String ma = m.group(3);
					List<HtQuyen> list = htQuyenDao.findByCode(ma);
					if(list != null && !list.isEmpty()){
						parent = list.get(0);
					}
					htQuyenCurrent.setHtQuyen(parent);
					
					//Mac dinh khong phai menu
					htQuyenCurrent.setMenu(new Long(CommonUtils.EMenu.NO_MENU.getCode()));
					htQuyenDetail(htQuyenCurrent);
					
					initDetailForm();
				}else{
					String ma = m.group(3);
					List<HtQuyen> list = htQuyenDao.findByCode(ma);
					if(list != null && !list.isEmpty()){
						htQuyenCurrent = list.get(0);
						htQuyenWf = htQuyenWM.getWorkflow(htQuyenCurrent);
						htQuyenDetail(htQuyenCurrent);
						parent = htQuyenCurrent.getHtQuyen();
						initDetailForm();
					}
					
				}
				
			}
		}
    }
    
    private void htQuyenDetail(HtQuyen htQuyen) {
        if (htQuyen == null) {
        	htQuyen = new HtQuyen();
        }
        BeanItem<HtQuyen> item = new BeanItem<HtQuyen>(htQuyen);
        fieldGroup.setItemDataSource(item);
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(HtQuyenView.MAIN_TITLE);
		bcItem.setViewName(HtQuyenView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
    
    public void initDetailForm(){
    	
    	formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	FormLayout form = new FormLayout();
    	form.setMargin(true);

    	List<HtQuyen> htQuyenList = htQuyenDao.getHtQuyenFind(null,null, null, null, null, -1, -1);
    	List<HtQuyen> childList = new ArrayList<HtQuyen>();
    	if(htQuyenList != null && !htQuyenList.isEmpty()){
    		List<HtQuyen> list = new ArrayList<HtQuyen>();
        	list.add(htQuyenCurrent);
        	while(!list.isEmpty()){
        		HtQuyen current = list.remove(0);
        		for(HtQuyen q : htQuyenList){
        			if(q.getHtQuyen() != null && q.getHtQuyen().equals(current)){
        				list.add(q);
        				childList.add(q);
        			}
        		}
        	}
    	}
    	childList.add(htQuyenCurrent);
    	
    	htQuyenList.removeAll(childList);
    	System.out.println(childList);
    	
    	if(htQuyenCurrent.getHtQuyen() != null && htQuyenList.contains(htQuyenCurrent.getHtQuyen())){
    		htQuyenList.set(htQuyenList.indexOf(htQuyenCurrent.getHtQuyen()), htQuyenCurrent.getHtQuyen());
    	}
    	
    	BeanItemContainer<HtQuyen> quyenChaContainer = new BeanItemContainer<HtQuyen>(HtQuyen.class, htQuyenList);
    	ComboBox cbbParent = new ComboBox("Quyền cha", quyenChaContainer);
    	cbbParent.setNullSelectionAllowed(true);
    	cbbParent.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbParent.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbParent.setItemCaptionPropertyId("ten");
    	
    	TextField txtMa = new TextField("Mã quyền");
    	txtMa.focus();
    	txtMa.setRequired(true);
    	txtMa.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void textChange(TextChangeEvent event) {

				TextField txt = (TextField) fieldGroup.getField("ma");
				System.out.println(event.getText());
				if(!checkTrungMa(event.getText(), action)){
					txt.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
				}else{
					txt.setComponentError(null);
				}
			}
    	});

    	txtMa.setTextChangeEventMode(TextChangeEventMode.LAZY);
    	
    	txtMa.setSizeFull();
    	txtMa.setNullRepresentation("");
    	txtMa.setRequiredError("Không được để trống");
    	txtMa.addValidator(new StringLengthValidator("Số ký tự tối đa là 20", 1, 20, false));   
    	txtMa.addValidator(new RegexpValidator("[0-9a-zA-Z_]+", "Chỉ chấp nhận các ký tự 0-9 a-z A-Z _"));
    	
    	TextField txtTen = new TextField("Tên quyền");
    	txtTen.setRequired(true);
    	txtTen.setSizeFull();
    	txtTen.setNullRepresentation("");
    	txtTen.setRequiredError("Không được để trống");
    	txtTen.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", 1, 2000, false)); 
    	
    	TextField txtViewName = new TextField("URL");
    	txtViewName.setSizeFull();
    	txtViewName.setNullRepresentation("");
    	txtViewName.setNullSettingAllowed(true);
    	txtViewName.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", 1, 2000, false)); 
    	
    	TextField txtProdefId = new TextField("Mã quy trình");
    	txtProdefId.setSizeFull();
    	txtProdefId.setNullRepresentation("");
    	txtProdefId.setNullSettingAllowed(true);
    	txtProdefId.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", 0, 2000, true)); 
    	
    	TextField txtFormCode = new TextField("Mã biểu mẫu");
    	txtFormCode.setSizeFull();
    	txtFormCode.setNullRepresentation("");
    	txtFormCode.setNullSettingAllowed(true);
    	txtFormCode.addValidator(new StringLengthValidator("Số ký tự tối đa là 4000", 0, 2000, true)); 
    	
    	OptionGroup menuOption = new OptionGroup("Là menu hay không?");
    	for(CommonUtils.EMenu menu : CommonUtils.EMenu.values()){
    		menuOption.addItem(new Long(menu.getCode()));
        	menuOption.setItemCaption(new Long(menu.getCode()), menu.getName());
    	}
    	
    	ComboBox cbbIcon = new ComboBox("Icon");
    	
    	for (FontAwesome item : FontAwesome.values()) {
			Integer id = new Integer(item.getCodepoint());
			cbbIcon.addItem(id);
			cbbIcon.setItemCaption(id, item.name());
			cbbIcon.setItemIcon(id, item);
		}
    	cbbIcon.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	
    	OptionGroup chkActions = new OptionGroup("Quyền");

		for (CommonUtils.EAction item : CommonUtils.EAction.values()) {
			chkActions.addItem(new Long(item.getCode()));
			chkActions.setItemCaption(new Long(item.getCode()), item.getName());
		}
		chkActions.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		chkActions.addStyleName(ExplorerLayout.FORM_CONTROL);
		chkActions.setNullSelectionAllowed(true);
		chkActions.setMultiSelect(true);
		
		LinkedHashSet<Long> actions = null;
		if(!Constants.ACTION_ADD.equals(action)){
			if(htQuyenCurrent.getAction() != null){
				int action = htQuyenCurrent.getAction().intValue();
				actions = new LinkedHashSet<Long>();
    			for(CommonUtils.EAction a : CommonUtils.EAction.values()){
    				int actionValue = a.getCode();
    				if((actionValue&action) == actionValue){
    					actions.add(new Long(actionValue));
    				}
    			}
    		}
		}
		
    	fieldGroup.getItemDataSource().addItemProperty("actions", new ObjectProperty<LinkedHashSet>(actions, LinkedHashSet.class));
		
    	fieldGroup.bind(cbbParent, "htQuyen");
    	fieldGroup.bind(txtMa, "ma");
    	fieldGroup.bind(txtTen, "ten");
    	fieldGroup.bind(txtViewName, "viewName");
    	fieldGroup.bind(txtProdefId, "prodefId");
    	fieldGroup.bind(txtFormCode, "maBieuMau");
    	fieldGroup.bind(menuOption, "menu");
    	fieldGroup.bind(cbbIcon, "icon");
    	fieldGroup.bind(chkActions, "actions");
    	
    	
    	cbbParent.setReadOnly("xem".equals(action));
    	txtMa.setReadOnly("xem".equals(action));
    	txtTen.setReadOnly("xem".equals(action));
    	txtViewName.setReadOnly("xem".equals(action));
    	menuOption.setReadOnly("xem".equals(action));
    	txtProdefId.setReadOnly("xem".equals(action));
    	txtFormCode.setReadOnly("xem".equals(action));
    	cbbIcon.setReadOnly("xem".equals(action));
    	chkActions.setReadOnly("xem".equals(action));

    	form.addComponent(cbbParent);
    	form.addComponent(txtMa);
    	form.addComponent(txtTen);
    	form.addComponent(txtViewName);
    	form.addComponent(txtProdefId);
    	form.addComponent(txtFormCode);
    	form.addComponent(menuOption);
    	form.addComponent(cbbIcon);
    	form.addComponent(chkActions);
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	if(!Constants.ACTION_VIEW.equals(action)){
    		btnUpdate = new Button();
    		btnUpdate.addClickListener(this);
    		buttons.addComponent(btnUpdate);
    		if(Constants.ACTION_EDIT.equals(action)){
        		btnUpdate.setCaption("Cập nhật");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_EDIT_ID);
        	}else if(Constants.ACTION_ADD.equals(action)){
        		btnUpdate.setId(BUTTON_ADD_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption("Thêm mới");
        	}else if(Constants.ACTION_DEL.equals(action)){
        		btnUpdate.setId(BUTTON_DEL_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
        		btnUpdate.setCaption("Xóa");
        	}else if(Constants.ACTION_APPROVE.equals(action)){
        		btnUpdate.setId(BUTTON_APPROVE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption(htQuyenWf.getPheDuyetTen());
        	}else if(Constants.ACTION_REFUSE.equals(action)){
        		btnUpdate.setCaption(htQuyenWf.getTuChoiTen());
        		btnUpdate.setId(BUTTON_REFUSE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_WARNING);
        	}
    	}
    	
    	btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
    	form.addComponent(buttons);

    	formContainer.addComponent(form);
    	addComponent(formContainer);
    }

public boolean checkTrungMa(String ma, String act){
	try {
		if(ma == null || ma.isEmpty()){
			return false;
		}
		if(!Constants.ACTION_DEL.equals(act)){
			List<HtQuyen> list = htQuyenDao.findByCode(ma);
			list.remove(htQuyenCurrent);
			if(list == null || list.isEmpty()){
				return true;
			}
		}else if(Constants.ACTION_DEL.equals(act)){
			return true;
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return false;
}

@SuppressWarnings("unchecked")
@Override
public void buttonClick(ClickEvent event) {
	try {
		Button button = event.getButton();
		if(BUTTON_CANCEL_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(HtQuyenView.VIEW_NAME);
		}else{
			if(Constants.ACTION_EDIT.equals(action)){
				TextField txt = (TextField) fieldGroup.getField("ma");
				if(!checkTrungMa(txt.getValue(), action)){
					txt.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
					return;
				}else{
					txt.setComponentError(null);
				}
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					HtQuyen current = ((BeanItem<HtQuyen>) fieldGroup.getItemDataSource()).getBean();
					Object actionsObj = fieldGroup.getItemDataSource().getItemProperty("actions").getValue();
					if(actionsObj !=null && actionsObj instanceof LinkedHashSet){
						LinkedHashSet<Long> actions = (LinkedHashSet<Long>) actionsObj;
						int actionValue=0;
						for(Long act : actions){
							actionValue = actionValue|act.intValue();
						}
						current.setAction(new Long(actionValue));
					}
					htQuyenDao.save(current);
					UI.getCurrent().getNavigator().navigateTo(HtQuyenView.VIEW_NAME);
					Notification notf = new Notification("Thông báo", "Cập nhật thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
				}
			}else if(Constants.ACTION_ADD.equals(action)){
				Notification notf = new Notification("Thông báo", "Thêm mới thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					HtQuyen current = ((BeanItem<HtQuyen>) fieldGroup.getItemDataSource()).getBean();
					notf.show(Page.getCurrent());
					
					Object actionsObj = fieldGroup.getItemDataSource().getItemProperty("actions").getValue();
					if(actionsObj !=null && actionsObj instanceof LinkedHashSet){
						LinkedHashSet<Long> actions = (LinkedHashSet<Long>) actionsObj;
						int actionValue=0;
						for(Long act : actions){
							actionValue = actionValue|act.intValue();
						}
						current.setAction(new Long(actionValue));
					}
					
					htQuyenDao.save(current);
					UI.getCurrent().getNavigator().navigateTo(HtQuyenView.VIEW_NAME);
					
				}
			}else if(Constants.ACTION_DEL.equals(action)){
				Notification notf = new Notification("Thông báo", "Xóa thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					notf.show(Page.getCurrent());
					htQuyenDao.delete(((BeanItem<HtQuyen>) fieldGroup.getItemDataSource()).getBean());
					UI.getCurrent().getNavigator().navigateTo(HtQuyenView.VIEW_NAME);
				}
			}else if(Constants.ACTION_APPROVE.equals(action)){
				Notification notf = new Notification("Thông báo", htQuyenWf.getPheDuyetTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					HtQuyen current = htQuyenWf.pheDuyet();
					notf.show(Page.getCurrent());
					
					Object actionsObj = fieldGroup.getItemDataSource().getItemProperty("actions").getValue();
					if(actionsObj !=null && actionsObj instanceof LinkedHashSet){
						LinkedHashSet<Long> actions = (LinkedHashSet<Long>) actionsObj;
						int actionValue=0;
						for(Long act : actions){
							actionValue = actionValue|act.intValue();
						}
						current.setAction(new Long(actionValue));
					}
					
					htQuyenDao.save(current);
					UI.getCurrent().getNavigator().navigateTo(HtQuyenView.VIEW_NAME);
				}
			}else if(Constants.ACTION_REFUSE.equals(action)){
				Notification notf = new Notification("Thông báo", htQuyenWf.getTuChoiTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					HtQuyen current = htQuyenWf.tuChoi();
					notf.show(Page.getCurrent());
					
					Object actionsObj = fieldGroup.getItemDataSource().getItemProperty("actions").getValue();
					if(actionsObj !=null && actionsObj instanceof LinkedHashSet){
						LinkedHashSet<Long> actions = (LinkedHashSet<Long>) actionsObj;
						int actionValue=0;
						for(Long act : actions){
							actionValue = actionValue|act.intValue();
						}
						current.setAction(new Long(actionValue));
					}
					
					htQuyenDao.save(current);
					UI.getCurrent().getNavigator().navigateTo(HtQuyenView.VIEW_NAME);
				}
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
    
}
}
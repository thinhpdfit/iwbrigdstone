package com.eform.ui.view.eoffice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.TemporaryFileDownloadResource;
import com.eform.common.ThamSo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.CommonUtils.ETrangThaiHoSo;
import com.eform.common.utils.FileUtils;
import com.eform.common.utils.FormUtils;
import com.eform.common.utils.SqlJsonBuilder;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.HtQuyenServices;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;
import com.eform.model.entities.HtQuyen;
import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BieuMauService;
import com.eform.service.BmOTruongThongTinService;
import com.eform.service.FileManagementService;
import com.eform.service.HoSoService;
import com.eform.service.HtThamSoService;
import com.eform.service.TruongThongTinService;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.paging.DocumentPaginationBar;
import com.eform.ui.view.BaseView;
import com.eform.ui.view.dashboard.DashboardView;
import com.eform.ui.view.process.MyProcessInstanceDetailView;
import com.eform.ui.view.process.ProcessDefinitionDetailView;
import com.eform.ui.view.task.TaskDetailView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = DocumentManagementView.VIEW_NAME)
public class DocumentManagementView extends VerticalLayout implements View, ClickListener, BaseView{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "quan-ly-van-ban";
	private static final String MAIN_TITLE = "Quản lý văn bản";
	private static final String SMALL_TITLE = "";
	
	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	private static final String BTN_VIEW_ID = "xem";
	private static final String BTN_VIEW_HISTORY_ID = "viewHistory";
	private static final String BTN_CANCEL_PROCESS_ID = "cancelProcess";
	private static final String BTN_DOWNLOAD_ATT = "downloadAtt";
	private static final String BTN_ADDNEW_ID = "themMoi";
	private static final String BTN_DOWNLOAD_ID = "excel";
	private final DocumentManagementView currentView = this;

	private String prevViewName;
	private HtQuyen htQuyen;

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private BieuMauService bieuMauService;

	
	@Autowired
	private RepositoryService repositoryService;	
	
	@Autowired
	protected TaskService taskService;
	
	@Autowired
	protected RuntimeService runtimeService;
	
	private WorkflowManagement<TruongThongTin> truongThongTinWM;

	@Autowired
	private HoSoService hoSoService;
	@Autowired
	private HtThamSoService htThamSoService;
	@Autowired
	private FileManagementService fileManagementService;
	@Autowired
	private HistoryService historyService;
	
	private Table table = new Table();
	private BeanItemContainer<Map<String, Object>> container;
	private DocumentPaginationBar paginationBar;
	private String uploadPath;
	private Button btnAddNew;
	private Button btnDownloadExcel;
	private List<BieuMau> bieuMauList;
	

	@Autowired
	TruongThongTinService truongThongTinService;
	@Autowired
	BmOTruongThongTinService bmOTruongThongTinService;
	@Autowired
	private HtQuyenServices htQuyenServices;
	private List<TruongThongTin> truongThongTinSearchList;
	private List<TruongThongTin> truongThongTinTableList;
	private Map<String, Integer> jsonKeyMap;

	private PropertysetItem searchItem;
	private FieldGroup searchFieldGroup;
	private CssLayout wrapper;
	
	private List<HistoricProcessInstance> proInsList;
	private String url;
	
	@PostConstruct
    void init() {
		
		HtThamSo folderUpload = htThamSoService.getHtThamSoFind(ThamSo.FOLDER_UPLOAD_PATH);
		uploadPath="";
		if(folderUpload != null){
			uploadPath = folderUpload.getGiaTri();
		}
		
		initMainTitle();
		wrapper = new CssLayout();
		wrapper.setWidth("100%");
		addComponent(wrapper);
		
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		
		url = event.getParameters();
		if(url == null){
			url = "";
		}
		url = VIEW_NAME+"/"+url;
		List<HtQuyen> htQuyenList = htQuyenServices.getHtQuyenFind(null, url, null, null, null, -1, -1);
		
		if(htQuyenList != null && !htQuyenList.isEmpty()){
			htQuyen = htQuyenList.get(0);
		}
		
		if(htQuyen != null && htQuyen.getMaBieuMau() != null && htQuyen.getProdefId() != null){
			searchItem = new PropertysetItem();
			searchFieldGroup = new FieldGroup();
			searchFieldGroup.setItemDataSource(searchItem);
			wrapper.removeAllComponents();
			
			proInsList = historyService.createHistoricProcessInstanceQuery().processDefinitionId(htQuyen.getProdefId()).list();
			
			truongThongTinWM = new WorkflowManagement<TruongThongTin>(EChucNang.E_FORM_TRUONG_THONG_TIN.getMa());
			bieuMauList = new ArrayList();
			String[] maBmArr = htQuyen.getMaBieuMau().split(",");
			for (String str : maBmArr) {
				bieuMauList.addAll(bieuMauService.findByCode(str));
			}
			initSearchForm();
			initDataContent();
			
			btnAddNew = new Button();
	    	btnAddNew.setIcon(FontAwesome.PLUS);
	    	btnAddNew.addClickListener(this);
	    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
	    	btnAddNew.setDescription("Thêm văn bản");
	    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
	    	btnAddNew.setId(BTN_ADDNEW_ID);
	    	wrapper.addComponent(btnAddNew);
	    	
	    	btnDownloadExcel = new Button();
	    	btnDownloadExcel.setIcon(FontAwesome.DOWNLOAD);
	    	btnDownloadExcel.addClickListener(this);
	    	btnDownloadExcel.addStyleName(Reindeer.BUTTON_LINK);
	    	btnDownloadExcel.setDescription("Tải xuống");
	    	btnDownloadExcel.addStyleName(ExplorerLayout.BUTTON_DOWNLOAD);
	    	btnDownloadExcel.setId(BTN_DOWNLOAD_ID);
	    	wrapper.addComponent(btnDownloadExcel);
		}
	}
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		CssFormLayout searchForm = new CssFormLayout(20, 80);
		
		List<TruongThongTin> trgTTList = bmOTruongThongTinService.findTruongThongtinByBieuMau(bieuMauList, null, true);
		
		truongThongTinSearchList = new ArrayList();
		if(trgTTList != null){
			for(TruongThongTin ttt : trgTTList){
				if(!truongThongTinSearchList.contains(ttt)){
					truongThongTinSearchList.add(ttt);
				}
			}
		}
		if(truongThongTinSearchList != null){
			for(TruongThongTin truongThongTin : truongThongTinSearchList){
				if(truongThongTin.getKieuDuLieu() != null){
					int kieuDuLieu = truongThongTin.getKieuDuLieu().intValue();
					if(CommonUtils.EKieuDuLieu.TEXT.getMa() == kieuDuLieu
							|| CommonUtils.EKieuDuLieu.TEXT_AREA.getMa() == kieuDuLieu
							|| CommonUtils.EKieuDuLieu.EMAIL.getMa() == kieuDuLieu
							|| CommonUtils.EKieuDuLieu.EDITOR.getMa() == kieuDuLieu){
						TextField txt = new TextField(truongThongTin.getTen());
						txt.setNullRepresentation("");
						txt.setNullSettingAllowed(true);
						txt.addStyleName(ExplorerLayout.FORM_CONTROL);
						searchItem.addItemProperty(truongThongTin.getMa(), new ObjectProperty<String>(null, String.class));
						searchFieldGroup.bind(txt, truongThongTin.getMa());
						searchForm.addFeild(txt);
					}else if(CommonUtils.EKieuDuLieu.DANH_MUC.getMa() == kieuDuLieu){
						ComboBox comboBox = new ComboBox(truongThongTin.getTen());
						comboBox.setFilteringMode(FilteringMode.CONTAINS);
						comboBox.setNullSelectionAllowed(true);
						comboBox.addStyleName(ExplorerLayout.FORM_CONTROL);
						comboBox.addStyleName(ExplorerLayout.FORM_COMBOBOX);
						
						WorkflowManagement<DanhMuc> danhMucWM = new WorkflowManagement<DanhMuc>(EChucNang.E_FORM_DANH_MUC.getMa());
						List<Long> trangThai = null;
						if(danhMucWM != null && danhMucWM.getTrangThaiSuDung() != null){
							trangThai = new ArrayList<Long>();
							trangThai.add(new Long(danhMucWM.getTrangThaiSuDung().getId()));
						}
						LoaiDanhMuc loaiDanhMuc = truongThongTin.getLoaiDanhMuc();
						if(loaiDanhMuc != null){
							List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(null, null, trangThai, loaiDanhMuc, null, -1, -1);
							if(danhMucList != null && !danhMucList.isEmpty()){
								for (DanhMuc dm : danhMucList) {
									comboBox.addItem(dm.getMa());
									comboBox.setItemCaption(dm.getMa(), dm.getTen());
								}
							}
						}
						searchItem.addItemProperty(truongThongTin.getMa(), new ObjectProperty<String>(null, String.class));
						searchFieldGroup.bind(comboBox, truongThongTin.getMa());
						searchForm.addFeild(comboBox);
					}else if(CommonUtils.EKieuDuLieu.DATE.getMa() == kieuDuLieu){
						CssLayout formGroup = new CssLayout();
						formGroup.addStyleName("form-group");
						formGroup.addStyleName("form-group-2-comp");
						formGroup.setWidth("100%");
						
						PopupDateField dateField = new PopupDateField();
						dateField.addStyleName(ExplorerLayout.FORM_CONTROL);
						dateField.addStyleName(ExplorerLayout.FORM_DATE);
						dateField.setInputPrompt("Từ ngày...");
						searchItem.addItemProperty(truongThongTin.getMa()+"FROM", new ObjectProperty<Date>(null, Date.class));
						searchFieldGroup.bind(dateField, truongThongTin.getMa()+"FROM");
						formGroup.addComponent(dateField);
						
						PopupDateField dateFieldTo = new PopupDateField();
						dateFieldTo.addStyleName(ExplorerLayout.FORM_CONTROL);
						dateFieldTo.addStyleName(ExplorerLayout.FORM_DATE);
						dateFieldTo.setInputPrompt("Đến ngày...");
						searchItem.addItemProperty(truongThongTin.getMa()+"TO", new ObjectProperty<Date>(null, Date.class));
						searchFieldGroup.bind(dateFieldTo, truongThongTin.getMa()+"TO");
						formGroup.addComponent(dateFieldTo);
						
						searchForm.addFeild(formGroup, truongThongTin.getTen());
					}else if(CommonUtils.EKieuDuLieu.NUMBER.getMa() == kieuDuLieu
							|| CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa() == kieuDuLieu){
						CssLayout formGroup = new CssLayout();
						formGroup.addStyleName("form-group");
						formGroup.addStyleName("form-group-2-comp");
						formGroup.setWidth("100%");
						
						TextField inputText = new TextField();
						inputText.setNullRepresentation("");
						inputText.addStyleName(ExplorerLayout.FORM_CONTROL);
						inputText.addStyleName(ExplorerLayout.FORM_NUMBER);
						inputText.setInputPrompt("Từ...");
						searchItem.addItemProperty(truongThongTin.getMa()+"FROM", new ObjectProperty<Long>(null, Long.class));
						searchFieldGroup.bind(inputText, truongThongTin.getMa()+"FROM");
						formGroup.addComponent(inputText);
						
						TextField inputTextTo = new TextField();
						inputTextTo.setNullRepresentation("");
						inputTextTo.setInputPrompt("Đến...");
						inputTextTo.addStyleName(ExplorerLayout.FORM_CONTROL);
						inputTextTo.addStyleName(ExplorerLayout.FORM_NUMBER);
						searchItem.addItemProperty(truongThongTin.getMa()+"TO", new ObjectProperty<Long>(null, Long.class));
						searchFieldGroup.bind(inputTextTo, truongThongTin.getMa()+"TO");
						formGroup.addComponent(inputTextTo);
						
						searchForm.addFeild(formGroup, truongThongTin.getTen());
					}
				}
			}
		}
		
		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(currentView);
		searchForm.addButton(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(currentView);
		searchForm.addButton(btnRefresh);
		
		wrapper.addComponent(searchForm);
	}
	
	public void initDataContent(){
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(100, Unit.PERCENTAGE);
		wrapper.addComponent(dataWrap);
		
		List<TruongThongTin> trgTTList = bmOTruongThongTinService.findTruongThongtinByBieuMau(bieuMauList, true, null);
		
		truongThongTinTableList = new ArrayList();
		if(trgTTList != null){
			for(TruongThongTin ttt : trgTTList){
				if(!truongThongTinTableList.contains(ttt)){
					truongThongTinTableList.add(ttt);
				}
			}
		}
		
		
		jsonKeyMap = new HashMap();
		
		List<TruongThongTin> truongThongTinAll = new ArrayList();
		truongThongTinAll.addAll(truongThongTinSearchList);
		truongThongTinAll.removeAll(truongThongTinTableList);
		truongThongTinAll.addAll(truongThongTinTableList);
		
		for (TruongThongTin truongThongTin : truongThongTinAll) {
			if(truongThongTin.getKieuDuLieu() != null){
				int type = FormUtils.getSQLType(truongThongTin.getKieuDuLieu());
				jsonKeyMap.put(truongThongTin.getMa(), type);
			}
		}
		container = new BeanItemContainer<Map<String, Object>>(Map.class, new ArrayList());
		
		if(proInsList != null && !proInsList.isEmpty()){
			Map<String, Object> sqlQ = SqlJsonBuilder.getHoSoJsonSqlQuery(jsonKeyMap, null, proInsList, 0, 0);
			String sql = (String) sqlQ.get("SQL");
			Object [] objParams = (Object[]) sqlQ.get("PARAM");
			int[] types = (int[]) sqlQ.get("TYPE");
			paginationBar = new DocumentPaginationBar(sql, objParams, types, container);
		}
		
		//List<Map<String, Object>> data = hoSoService.getHoSoJsonByKey(sql, objParams, types);
		//if(data != null){
		table = new Table();
		
		table.setContainerDataSource(container);
		table.setSizeFull();
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setWidth(100, Unit.PERCENTAGE);
		
		
		
		if(truongThongTinTableList != null){
			String[] visibleColumn = new String[truongThongTinTableList.size()+2];
			int i =0;
			for (TruongThongTin truongThongTin : truongThongTinTableList) {
				String key = truongThongTin.getMa();
				int kieuDL = truongThongTin.getKieuDuLieu().intValue();
				table.setColumnHeader(key, truongThongTin.getTen());
				visibleColumn[i] = key;
				i++;
				table.addGeneratedColumn(key, new ColumnGenerator(){
					private static final long serialVersionUID = 1L;

					@Override
					public Object generateCell(Table source, Object itemId, Object columnId) {
						Map<String, Object> current = (Map) itemId;
						if(current != null && current.get(key) != null){
							Object valueObj = current.get(key);
							if(kieuDL == CommonUtils.EKieuDuLieu.DANH_MUC.getMa()){
								String value = (String) valueObj;
								List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(value, null, null, null, null, 0, 1);
								if(danhMucList != null && !danhMucList.isEmpty()){
									DanhMuc dm = danhMucList.get(0);
									return dm.getTen();
								}
							}else if(kieuDL == CommonUtils.EKieuDuLieu.EDITOR.getMa()){
								String value = (String) valueObj;
								Label lbl = new Label(value);
								lbl.setContentMode(ContentMode.HTML);
								lbl.addStyleName("table-content-lbl");
								return lbl;
							}else if(kieuDL == CommonUtils.EKieuDuLieu.XAC_THUC_FILE.getMa()){
								
								String value = (String) valueObj;
								HorizontalLayout actionWrap = new HorizontalLayout();
								actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
								if(value != null && !value.isEmpty()){
									String arr[] = value.split(",");
									if(arr != null && arr.length > 0){
										for(int i =0; i< arr.length; i++){
											if(arr[i] != null && arr[i].trim().matches("[0-9]+")){
												Long id = Long.parseLong(arr[i].trim());
												FileManagement file = fileManagementService.findOne(id); 
												if(file != null){
													Button btnCheckout = new Button();
													btnCheckout.setId(BTN_DOWNLOAD_ATT);
													btnCheckout.setIcon(FontAwesome.FILE_O);
													btnCheckout.addStyleName(ExplorerLayout.BUTTON_ACTION);
													btnCheckout.addClickListener(currentView);
													btnCheckout.setData(file);
													btnCheckout.setDescription(file.getTieuDe());
													btnCheckout.addStyleName(Reindeer.BUTTON_LINK);
											        actionWrap.addComponent(btnCheckout);
												}
											}
										}
									}
								}
								return actionWrap;
							}
							return valueObj;
						}
						return null;
					}
				});
			}
			
			table.setColumnHeader("trangThai", "Trạng thái");
			visibleColumn[truongThongTinTableList.size()] = "trangThai";
			
			table.addGeneratedColumn("trangThai", new ColumnGenerator() { 
				private static final long serialVersionUID = 1L;

				@Override
			    public Object generateCell(final Table source, final Object itemId, Object columnId) {
					Map<String, Object> current = (Map) itemId;
					if(current != null && current.get("trang_thai") != null){
						try {
							Long trangThai = ((BigDecimal) current.get("trang_thai")).longValue();
							ETrangThaiHoSo eTrangThai = ETrangThaiHoSo.getTrangThai(trangThai);
							return eTrangThai.getName();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					return null;
			    }
			});
			
			table.setColumnWidth("action", 220);
			table.setColumnHeader("action", "Thao tác");
			visibleColumn[truongThongTinTableList.size()+1] = "action";
			table.addGeneratedColumn("action", new ColumnGenerator() { 
				private static final long serialVersionUID = 1L;

				@Override
			    public Object generateCell(final Table source, final Object itemId, Object columnId) {
					Map<String, Object> current = (Map) itemId;
					if(current != null && current.get("id") != null){
						HorizontalLayout actionWrap = new HorizontalLayout();
						actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
				        
						Button btnView = new Button();
				        btnView.setDescription("Xem");
				        btnView.setId(BTN_VIEW_ID);
				        btnView.setIcon(FontAwesome.EYE);
				        btnView.addStyleName(ExplorerLayout.BUTTON_ACTION);
				        btnView.addClickListener(currentView);
				        btnView.setData(itemId);
				        btnView.addStyleName(Reindeer.BUTTON_LINK);
				        actionWrap.addComponent(btnView);
				        
				        Button btnViewHistory = new Button();
				        btnViewHistory.setDescription("Quy trình");
				        btnViewHistory.setId(BTN_VIEW_HISTORY_ID);
				        btnViewHistory.setIcon(FontAwesome.SITEMAP);
				        btnViewHistory.addStyleName(ExplorerLayout.BUTTON_ACTION);
				        btnViewHistory.addClickListener(currentView);
				        btnViewHistory.setData(itemId);
				        btnViewHistory.addStyleName(Reindeer.BUTTON_LINK);
				        actionWrap.addComponent(btnViewHistory);
				        
//				        Button btnCancel = new Button();
//				        btnCancel.setDescription("Cancel");
//				        btnCancel.setId(BTN_CANCEL_PROCESS_ID);
//				        btnCancel.setIcon(FontAwesome.STOP_CIRCLE_O);
//				        btnCancel.addStyleName(ExplorerLayout.BUTTON_ACTION);
//				        btnCancel.addClickListener(currentView);
//				        btnCancel.setData(itemId);
//				        btnCancel.addStyleName(Reindeer.BUTTON_LINK);
//				        actionWrap.addComponent(btnCancel);
				        
				        return actionWrap;
					}
					return null;
			    }
			});
			table.setRowHeaderMode(RowHeaderMode.INDEX);
			table.setVisibleColumns(visibleColumn);
		}
		dataWrap.addComponent(table);
		if(paginationBar != null){
			dataWrap.addComponent(paginationBar);
		}
		//}
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if(BTN_VIEW_ID.equals(event.getButton().getId())){
			try {
				Map currentRow = (Map) event.getButton().getData();
				
				String proInsId = (String) currentRow.get("process_instance_id");
				String taskId = (String) currentRow.get("task_id");	
				if(proInsId != null){
					List<Task> taskList = taskService.createTaskQuery().processInstanceId(proInsId.trim()).active().list();
					if(taskList != null 
							&& taskList.size() == 1 
							&& CommonUtils.getUserLogedIn() != null
							&& CommonUtils.getUserLogedIn().equals(taskList.get(0).getAssignee())){
						Task taskCurrent = taskList.get(0);
						UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+taskCurrent.getId());
					}else if(taskId != null){
						UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW_HISTORY+"/"+taskId.trim());
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_VIEW_HISTORY_ID.equals(event.getButton().getId())){
			try {
				Map currentRow = (Map) event.getButton().getData();
				String processInsid = (String) currentRow.get("process_instance_id");	
				if(processInsid != null){
					
					String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
					UI.getCurrent().getPage().open(contextPath + "#!" + MyProcessInstanceDetailView.VIEW_NAME + "/"
							+ Constants.ACTION_VIEW + "/" + processInsid.trim(), "_blank");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_CANCEL_PROCESS_ID.equals(event.getButton().getId())){
			try {
				Map currentRow = (Map) event.getButton().getData();
				String processInsid = (String) currentRow.get("process_instance_id");	
				if(processInsid != null){
					ProcessInstance proIns = runtimeService.createProcessInstanceQuery().processInstanceId(processInsid.trim()).singleResult();
					if(proIns != null && !proIns.isSuspended() && !proIns.isEnded()){
						runtimeService.deleteProcessInstance(processInsid.trim(), "Kết thúc quy trình");
						Notification notf = new Notification("Thông báo", "Quy trình đã cancel thành công!");
						notf.setDelayMsec(3000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra!", Type.ERROR_MESSAGE);
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(Page.getCurrent());
			}
		}else if(BTN_DOWNLOAD_ATT.equals(event.getButton().getId())){
			try {
				FileManagement currentRow = (FileManagement) event.getButton().getData();
				if(currentRow != null){
					FileManagement file = (FileManagement) event.getButton().getData();
					FileVersion fileVersion = fileManagementService.getMaxVersion(file);
					boolean hasFile = false;
					if(fileVersion != null && fileVersion.getTrangThai() != null && fileVersion.getTrangThai().intValue() == 1
						&& fileVersion.getSignedPath() != null){
							File tempFile = new File(uploadPath+fileVersion.getSignedPath());
							if(tempFile.exists()){
								FileInputStream input = new FileInputStream(tempFile);
								StreamResource resourcetmp = FileUtils.getStreamResource(input, file.getTieuDe()+".pdf");
							    UI.getCurrent().getPage().open(resourcetmp, "checkout", true);
							    hasFile = true;
							}
					} 
						
					if(fileVersion != null && fileVersion.getUploadPath() != null && !hasFile){
						File tempFile = new File(uploadPath+fileVersion.getUploadPath());
						FileInputStream input = new FileInputStream(tempFile);
						StreamResource resourcetmp = FileUtils.getStreamResource(input, file.getTieuDe()+"."+fileVersion.getDinhDang());
					    UI.getCurrent().getPage().open(resourcetmp, "checkout", true);
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_SEARCH_ID.equals(event.getButton().getId())){
			try {
				if(truongThongTinSearchList != null && jsonKeyMap!= null && searchFieldGroup.isValid()){
					searchFieldGroup.commit();
					
					Map<String, Object> paramMap = new HashMap();
					Map<String, String> paramStringMap = new HashMap();
					for (TruongThongTin truongTT : truongThongTinSearchList) {
						try {
							if(CommonUtils.EKieuDuLieu.NUMBER.getMa() == truongTT.getKieuDuLieu().intValue()
									|| CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa() == truongTT.getKieuDuLieu().intValue()
									|| CommonUtils.EKieuDuLieu.DATE.getMa() == truongTT.getKieuDuLieu().intValue()){
								Object objFrom = searchItem.getItemProperty(truongTT.getMa()+"FROM").getValue();
								Object objTo = searchItem.getItemProperty(truongTT.getMa()+"TO").getValue();
								Map<String, Object> valueMap = new HashMap();
								String valueString = "";
								if(objFrom != null){
									valueMap.put("FROM", objFrom);
									valueString += "Từ "+objFrom.toString();
								}
								if(objTo != null){
									valueMap.put("TO", objTo);
									valueString += " Đến "+objTo.toString();
								}
								if(objFrom != null || objTo != null){
									paramMap.put(truongTT.getMa(), valueMap);
									paramStringMap.put(truongTT.getTen(), valueString);
								}
								
							}else if(CommonUtils.EKieuDuLieu.DANH_MUC.getMa() == truongTT.getKieuDuLieu().intValue()){
								if(searchItem.getItemProperty(truongTT.getMa()) != null){
									Object obj = searchItem.getItemProperty(truongTT.getMa()).getValue();
									if(obj != null && obj instanceof String){
										String str = (String) obj;
										paramMap.put(truongTT.getMa(), str);
										List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(str, null, null, truongTT.getLoaiDanhMuc(), null, -1, -1);
										if(danhMucList != null && !danhMucList.isEmpty()){
											paramStringMap.put(truongTT.getTen(), danhMucList.get(0).getTen());
										}
									}
								}
							}else{
								if(searchItem.getItemProperty(truongTT.getMa()) != null){
									Object obj = searchItem.getItemProperty(truongTT.getMa()).getValue();
									if(obj != null){
										paramMap.put(truongTT.getMa(), obj);
										paramStringMap.put(truongTT.getTen(), obj.toString());
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					Map<String, Object> sqlQ = SqlJsonBuilder.getHoSoJsonSqlQuery(jsonKeyMap, paramMap, proInsList, 0, 0);
					String sql = (String) sqlQ.get("SQL");
					Object [] objParams = (Object[]) sqlQ.get("PARAM");
					int[] types = (int[]) sqlQ.get("TYPE");
					
					//List<Map<String, Object>> data = hoSoService.getHoSoJsonByKey(sql, objParams, types);
					paginationBar.search(sql, objParams, types, paramStringMap);
				 }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
			if(htQuyen != null){
				UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionDetailView.VIEW_NAME+"/"+Constants.ACTION_START+"/"+htQuyen.getProdefId());
			}
		}else if(BTN_DOWNLOAD_ID.equals(event.getButton().getId())){
			try {
				if(paginationBar != null){
					List<Map<String, Object>> dataList = paginationBar.getDataList();
					
					if(dataList != null && truongThongTinTableList != null){
						
						XSSFWorkbook workbook = new XSSFWorkbook();
						
						CellStyle titleStyle = workbook.createCellStyle();
						titleStyle.setAlignment(HorizontalAlignment.CENTER);
						XSSFFont font = workbook.createFont();
						font.setFontHeightInPoints((short) 13);
						font.setBold(true);
						titleStyle.setFont(font);
						
						CellStyle exportInfoStyle = workbook.createCellStyle();
						exportInfoStyle.setAlignment(HorizontalAlignment.CENTER);
						
						CellStyle headerStyle = workbook.createCellStyle();
						headerStyle.setAlignment(HorizontalAlignment.CENTER);
						XSSFFont headerFont = workbook.createFont();
						headerFont.setBold(true);
						headerStyle.setFont(headerFont);
						headerStyle.setBorderBottom(BorderStyle.THIN);
						headerStyle.setBorderTop(BorderStyle.THIN);
						headerStyle.setBorderRight(BorderStyle.THIN);
						headerStyle.setBorderLeft(BorderStyle.THIN);
						
						CellStyle contentStyle = workbook.createCellStyle();
						XSSFFont contentFont = workbook.createFont();
						contentStyle.setFont(contentFont);
						contentStyle.setBorderBottom(BorderStyle.THIN);
						contentStyle.setBorderTop(BorderStyle.THIN);
						contentStyle.setBorderRight(BorderStyle.THIN);
						contentStyle.setBorderLeft(BorderStyle.THIN);
						
			            XSSFSheet sheet=workbook.createSheet(htQuyen.getTen());
			            
			            XSSFRow titleRow =sheet.createRow(1);
			            XSSFCell titleCell =titleRow.createCell(0);
			            sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, truongThongTinTableList.size()));
			            titleCell.setCellValue(htQuyen.getTen());
			            titleCell.setCellStyle(titleStyle);
			            
			            XSSFRow exportInfoRow =sheet.createRow(2);
			            XSSFCell exportInfoCell =exportInfoRow.createCell(0);
			            sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, truongThongTinTableList.size()));
			            
			            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			            
			            exportInfoCell.setCellValue("Ngày dữ liệu: "+df.format(new Date()));
			            exportInfoCell.setCellStyle(exportInfoStyle);
			            
			            int rowIndex=5;
			            int cellIndex=0;
			            XSSFRow row=null;
			            
						for (Map<String, Object> map : dataList) {
							if(map != null){
								row=sheet.createRow(rowIndex);
								cellIndex = 0;
								
								XSSFCell indexNumberCell=row.createCell(cellIndex++);
								indexNumberCell.setCellStyle(contentStyle);
								indexNumberCell.setCellValue(rowIndex-4);
								
								for (TruongThongTin truongThongtin : truongThongTinTableList) {
						            XSSFCell cell=row.createCell(cellIndex++);
						            cell.setCellStyle(contentStyle);
						            Object valueObj = map.get(truongThongtin.getMa());
						            int kieuDL = truongThongtin.getKieuDuLieu().intValue();
						            
									if(kieuDL == CommonUtils.EKieuDuLieu.DANH_MUC.getMa()){
										if(valueObj != null){
											String v = (String) valueObj;
											List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(v, null, null, null, null, 0, 1);
											if(danhMucList != null && !danhMucList.isEmpty()){
												DanhMuc dm = danhMucList.get(0);
												String valueStr = dm.getTen();
												cell.setCellValue(valueStr);
											}
										}
									}else if(kieuDL == CommonUtils.EKieuDuLieu.TEXT.getMa()
											|| kieuDL == CommonUtils.EKieuDuLieu.TEXT_AREA.getMa()
											|| kieuDL == CommonUtils.EKieuDuLieu.EMAIL.getMa()){
										String value = (String) valueObj;
										if(value != null){
											cell.setCellValue(value);
										}
									}else if(kieuDL == CommonUtils.EKieuDuLieu.EDITOR.getMa()){
										String value = (String) valueObj;
										if(value != null){
											value = StringEscapeUtils.unescapeHtml4(value);
											cell.setCellValue(value);
										}
									}else if(kieuDL == CommonUtils.EKieuDuLieu.CHECKBOX.getMa() && valueObj instanceof Boolean){
										Boolean value = (Boolean) valueObj;
										if(value != null){
											cell.setCellValue(value);
										}
									}else if(kieuDL == CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa() && valueObj instanceof BigDecimal){
										BigDecimal value = (BigDecimal) valueObj;
										if(value != null){
											cell.setCellValue(value.intValue());
										}
									}else if(kieuDL == CommonUtils.EKieuDuLieu.NUMBER.getMa() && valueObj instanceof BigDecimal){
										BigDecimal value = (BigDecimal) valueObj;
										if(value != null){
											cell.setCellValue(value.floatValue());
										}
									}else if(kieuDL == CommonUtils.EKieuDuLieu.DATE.getMa() && valueObj instanceof Date){
										Date value = (Date) valueObj;
										if(value != null){
											cell.setCellValue(value);
										}
									}
								}
								rowIndex++;
							}
						}
						
						cellIndex = 0;
						row = sheet.createRow(4);
						
						XSSFCell cell=row.createCell(cellIndex);
			            cell.setCellValue("STT");
			            cell.setCellStyle(headerStyle);
			            sheet.autoSizeColumn(cellIndex);
			            cellIndex++;
						for (TruongThongTin truongThongtin : truongThongTinTableList) {
				            cell=row.createCell(cellIndex);
				            cell.setCellValue(truongThongtin.getTen());
				            cell.setCellStyle(headerStyle);
				            sheet.autoSizeColumn(cellIndex);
				            cellIndex++;
						}
						
						
						Map<String, String> paramStringMap = paginationBar.getParamStringMap();
						
						XSSFSheet querySheet=workbook.createSheet("Tìm kiếm");
						int queryRowIndex=1;
						
						XSSFRow queryRow=querySheet.createRow(queryRowIndex++);
						XSSFCell cellQuery=queryRow.createCell(0);
						cellQuery.setCellValue("Tiêu chí");
						cellQuery.setCellStyle(headerStyle);
						cellQuery=queryRow.createCell(1);
						cellQuery.setCellValue("Giá trị");
						cellQuery.setCellStyle(headerStyle);
						
						if(paramStringMap != null && !paramStringMap.isEmpty()){
							for (Map.Entry<String, String> entry : paramStringMap.entrySet()) {
								queryRow=querySheet.createRow(queryRowIndex++);
								cellQuery=queryRow.createCell(0);
								cellQuery.setCellStyle(contentStyle);
								cellQuery.setCellValue(entry.getKey());
								cellQuery=queryRow.createCell(1);
								cellQuery.setCellStyle(contentStyle);
								cellQuery.setCellValue(entry.getValue());
							}
						}
						querySheet.autoSizeColumn(0);
						querySheet.autoSizeColumn(1);
						
						File tempFile = File.createTempFile("export", ".xlsx");
						FileOutputStream output = new FileOutputStream(tempFile);
						workbook.write(output);
					    TemporaryFileDownloadResource resource = new TemporaryFileDownloadResource("export.xlsx",
					                "application/vnd.ms-excel", tempFile);
					    UI.getCurrent().getPage().open(resource, "Danh sách văn bản", true);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
			searchFieldGroup.clear();
			Map<String, Object> sqlQ = SqlJsonBuilder.getHoSoJsonSqlQuery(jsonKeyMap, null, proInsList, 0, 0);
			String sql = (String) sqlQ.get("SQL");
			Object [] objParams = (Object[]) sqlQ.get("PARAM");
			int[] types = (int[]) sqlQ.get("TYPE");
			
			//List<Map<String, Object>> data = hoSoService.getHoSoJsonByKey(sql, objParams, types);
			paginationBar.clearData();
			paginationBar.search(sql, objParams, types);
		}
	}

	@Override
	public String getViewName() {
		return url;
	}

	@Override
	public String getPrevViewName() {
		return prevViewName;
	}

	@Override
	public void setPrevViewName(String prevViewName) {
		this.prevViewName = prevViewName;
	}

}

package com.eform.ui.view.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.StringUtils;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.HtChucVuDao;
import com.eform.model.entities.HtChucVu;
import com.eform.model.entities.HtDonVi;
import com.eform.service.HtDonViServices;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = UserDetailView.VIEW_NAME)
public class UserDetailView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "thong-tin-nguoi-dung";
	private static final String MAIN_TITLE = "Thông tin người dùng";
	private static final String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_ADD_ID = "btn-add";
	private static final String BUTTON_DEL_ID = "btn-del";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_GEN_PASS_ID = "btn-gen-pass";
	private User userCurrent;
	private String action;
	
	private final FieldGroup fieldGroup = new FieldGroup();
	private CssLayout formContainer;
	private Button btnUpdate;
	private Button btnBack;
	
	private TreeTable groupTreeTable;
	private Map<String,Boolean> selectedGroup;
	
	@Autowired
	protected IdentityService identityService;

	@Autowired
	private HtDonViServices htDonViServices;
	@Autowired
	private HtChucVuDao htChucVuDao;
	@Autowired
	private MessageSource messageSource;
	

	private final FieldGroup infoFieldGroup = new FieldGroup();
	private final PropertysetItem infoItem = new PropertysetItem();
	
    @PostConstruct
    void init() {
    	infoFieldGroup.setItemDataSource(infoItem);
    	initMainTitle();
    }

    @Override
    public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_.@]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				if(Constants.ACTION_ADD.equals(action)){
					userCurrent = identityService.newUser("");
					userDetail(userCurrent);
					initDetailForm();
				}else{
					String ma = m.group(3);
					userCurrent = identityService.createUserQuery().userId(ma).singleResult();
					if(userCurrent != null){
						userDetail(userCurrent);
						initDetailForm();
					}
					
				}
				
			}
		}

    }
    
    public void initDetailForm(){
    	HtDonVi donVi = null;
    	HtChucVu chucVu = null;
    	Date expiryDateTmp = null;
    	try {
    		if(userCurrent.getId() != null){
        		String maDonVi = identityService.getUserInfo(userCurrent.getId(), "HT_DON_VI");
        		if(maDonVi != null){
        			List<HtDonVi> list = htDonViServices.findByCode(maDonVi);
        			if(list != null && !list.isEmpty()){
        				donVi = list.get(0);
        			}
        		}
        		String maChucVu = identityService.getUserInfo(userCurrent.getId(), "HT_CHUC_VU");
        		if(maChucVu != null){
        			List<HtChucVu> list = htChucVuDao.findByCode(maChucVu);
        			if(list != null && !list.isEmpty()){
        				chucVu = list.get(0);
        			}
        		}
        		String expDateStr = identityService.getUserInfo(userCurrent.getId(), "EXPIRY_DATE");
        		if(expDateStr != null && expDateStr.matches("[0-9]+")){
        			expiryDateTmp = new Date(new Long(expDateStr));
        		}
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	FormLayout form = new FormLayout();
    	form.setWidth("100%");
    	form.addStyleName(ExplorerLayout.USER_FORM);
    	form.setMargin(true);
    	
    	TextField txtId = new TextField("Tên đăng nhập");
    	txtId.focus();
    	txtId.setRequired(true);
    	txtId.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void textChange(TextChangeEvent event) {

				TextField txt = (TextField) fieldGroup.getField("id");
				System.out.println(event.getText());
				if(!checkTrungMa(event.getText(), action)){
					txt.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
				}else{
					txt.setComponentError(null);
				}
			}
    	});

    	txtId.setTextChangeEventMode(TextChangeEventMode.LAZY);
    	
    	txtId.setSizeFull();
    	txtId.setNullRepresentation("");
    	txtId.setRequiredError("Không được để trống");
    	txtId.addValidator(new StringLengthValidator("Số ký tự tối đa là 60", 1, 60, false));   
    	txtId.addValidator(new RegexpValidator("[0-9a-zA-Z_@.]+", "Chỉ chấp nhận các ký tự 0-9 a-z A-Z _ @ ."));
    	
    	TextField txtFirstName = new TextField("Họ đệm");
    	txtFirstName.setRequired(true);
    	txtFirstName.setSizeFull();
    	txtFirstName.setNullRepresentation("");
    	txtFirstName.setRequiredError("Không được để trống");
    	txtFirstName.addValidator(new StringLengthValidator("Số ký tự tối đa là 50", 1, 50, false)); 
    	
    	
    	TextField txtLastName = new TextField("Tên");
    	txtLastName.setRequired(true);
    	txtLastName.setSizeFull();
    	txtLastName.setNullRepresentation("");
    	txtLastName.setRequiredError("Không được để trống");
    	txtLastName.addValidator(new StringLengthValidator("Số ký tự tối đa là 50", 1, 50, false)); 
    	
    	
    	TextField txtEmail = new TextField("Email");
    	txtEmail.setRequired(true);
    	txtEmail.setSizeFull();
    	txtEmail.setNullRepresentation("");
    	txtEmail.setRequiredError("Không được để trống");
    	txtEmail.addValidator(new StringLengthValidator("Số ký tự tối đa là 250", 1, 250, false));
    	txtEmail.addValidator(new RegexpValidator("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", "Email không đúng."));
    	
    	
    	
    	List<Long> trangThaiDv = null;
    	WorkflowManagement<HtDonVi> dvWM = new WorkflowManagement<HtDonVi>(EChucNang.HT_DON_VI.getMa());
    	if(dvWM.getTrangThaiSuDung() != null){
    		trangThaiDv = new ArrayList();
    		trangThaiDv.add(new Long(dvWM.getTrangThaiSuDung().getId()));
    	}
    	List<HtDonVi> htDonViList = htDonViServices.getHtDonViFind(null,null, null, trangThaiDv, -1, -1);
    	BeanItemContainer<HtDonVi> containerDv = new BeanItemContainer<HtDonVi>(HtDonVi.class, htDonViList);
    	ComboBox cbbDonVi = new ComboBox("Đơn vị", containerDv);
    	cbbDonVi.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbDonVi.setRequired(true);
    	cbbDonVi.setRequiredError("Không được để trống");
    	cbbDonVi.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbDonVi.setItemCaptionPropertyId("ten");
    	
    	if(htDonViList.contains(donVi)){
    		donVi = htDonViList.get(htDonViList.indexOf(donVi));
    	}
    	
    	infoItem.addItemProperty("donVi", new ObjectProperty<HtDonVi>(donVi, HtDonVi.class));
    	infoFieldGroup.bind(cbbDonVi, "donVi");
    	
    	List<Long> trangThaiCv = null;
    	WorkflowManagement<HtChucVu> cvWM = new WorkflowManagement<HtChucVu>(EChucNang.HT_CHUC_VU.getMa());
    	if(cvWM.getTrangThaiSuDung() != null){
    		trangThaiCv = new ArrayList();
    		trangThaiCv.add(new Long(cvWM.getTrangThaiSuDung().getId()));
    	}
    	
    	List<HtChucVu> htCvList = htChucVuDao.getHtChucVuFind(null, null, trangThaiCv, -1, -1);
    	BeanItemContainer<HtChucVu> containerCv = new BeanItemContainer<HtChucVu>(HtChucVu.class, htCvList);
    	ComboBox cbbChucVu = new ComboBox("Chức vụ", containerCv);
    	cbbChucVu.setRequired(true);
    	cbbChucVu.setRequiredError("Không được để trống");
    	cbbChucVu.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbChucVu.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbChucVu.setItemCaptionPropertyId("ten");
    	
    	if(htCvList.contains(chucVu)){
    		chucVu = htCvList.get(htCvList.indexOf(chucVu));
    	}
    	
    	infoItem.addItemProperty("chucVu", new ObjectProperty<HtChucVu>(chucVu, HtChucVu.class));
    	infoFieldGroup.bind(cbbChucVu, "chucVu");
    	
    	DateField expiryDate = new DateField("Ngày hết hạn");
    	expiryDate.setWidth("100%");
    	expiryDate.setRequired(false);
    	
    	infoItem.addItemProperty("expiryDate", new ObjectProperty<Date>(expiryDateTmp, Date.class));
    	infoFieldGroup.bind(expiryDate, "expiryDate");
    	
    	TextField txtPass = null;
    	
    	if(Constants.ACTION_ADD.equals(action)){
    		txtPass = new TextField("Mật khẩu");
        	txtPass.setRequired(true);
        	txtPass.setSizeFull();
        	txtPass.addStyleName(ExplorerLayout.TXT_GEN_PASS);
        	txtPass.setNullRepresentation("");
        	txtPass.setRequiredError("Không được để trống");
        	txtPass.addValidator(new StringLengthValidator("Số ký tự tối đa là 50", 1, 50, false));
        	
    	}
    	

    	fieldGroup.bind(txtId, "id");
    	fieldGroup.bind(txtFirstName, "firstName");
    	fieldGroup.bind(txtLastName, "lastName");
    	fieldGroup.bind(txtEmail, "email");
    	
    	if(txtPass != null){
    		
    		fieldGroup.bind(txtPass, "password");
    	}
    	
    	txtId.setReadOnly(!Constants.ACTION_ADD.equals(action));
    	txtFirstName.setReadOnly("xem".equals(action));
    	txtLastName.setReadOnly("xem".equals(action));
    	txtEmail.setReadOnly("xem".equals(action));
    	cbbDonVi.setReadOnly("xem".equals(action));
    	cbbChucVu.setReadOnly("xem".equals(action));
    	expiryDate.setReadOnly("xem".equals(action));
    	
    	form.addComponent(txtId);
    	form.addComponent(txtFirstName);
    	form.addComponent(txtLastName);
    	form.addComponent(txtEmail);
    	if(txtPass != null){
    		Button btnGenPass = new Button();
    		btnGenPass.setIcon(FontAwesome.RANDOM);
    		btnGenPass.addStyleName(ExplorerLayout.BTN_GEN_PASS);
    		btnGenPass.addClickListener(this);
    		btnGenPass.setId(BUTTON_GEN_PASS_ID);
    		btnGenPass.setData(txtPass);
    		form.addComponent(btnGenPass);
    		form.addComponent(txtPass);
    	}
    	
    	form.addComponent(cbbDonVi);
    	form.addComponent(cbbChucVu);
    	form.addComponent(expiryDate);
    	
    	
    	selectedGroup = new HashMap();
    	groupTreeTable=new TreeTable("Nhóm người dùng");
    	groupTreeTable.setWidth("100%");
		groupTreeTable.addContainerProperty("ma", CheckBox.class, null);
		groupTreeTable.addContainerProperty("ten", String.class, null);

		groupTreeTable.setColumnHeader("ma", "Mã nhóm");
		groupTreeTable.setColumnHeader("ten", "Tên nhóm");
		
		groupTreeTable.setVisibleColumns("ma", "ten");

		if(Constants.ACTION_ADD.equals(action)==false){
			List<Group> groupList=identityService.createGroupQuery().groupMember(userCurrent.getId()).list();
			for(Group group:groupList){
				selectedGroup.put(group.getId(), true);
			}
		}
		List<Group> allGroup = identityService.createGroupQuery().list();
		for(Group g:allGroup){
			Boolean check=selectedGroup.get(g.getId());
			CheckBox checkBox =new CheckBox();
			checkBox.setCaption(g.getId());
			checkBox.setValue(check!=null&&check);
			checkBox.setReadOnly("xem".equals(action));
			checkBox.addValueChangeListener(new ValueChangeListener(){
				@Override
				public void valueChange(ValueChangeEvent event) {
					Boolean check=(Boolean) event.getProperty().getValue();
					if(check!=null&&check){
						selectedGroup.put(g.getId(),true);
					}else{
						selectedGroup.put(g.getId(),false);
					}
				}
			});
			groupTreeTable.addItem(new Object[]{checkBox,g.getName()},g);
			groupTreeTable.setChildrenAllowed(g, false);
		}
    	form.addComponent(groupTreeTable);
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	if(!Constants.ACTION_VIEW.equals(action)){
    		btnUpdate = new Button();
    		btnUpdate.addClickListener(this);
    		buttons.addComponent(btnUpdate);
    		if(Constants.ACTION_EDIT.equals(action)){
        		btnUpdate.setCaption("Cập nhật");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_EDIT_ID);
        	}else if(Constants.ACTION_ADD.equals(action)){
        		btnUpdate.setId(BUTTON_ADD_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption("Thêm mới");
        	}else if(Constants.ACTION_DEL.equals(action)){
        		btnUpdate.setId(BUTTON_DEL_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
        		btnUpdate.setCaption("Xóa");
        	}
    	}
    	
    	btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
    	form.addComponent(buttons);

    	formContainer.addComponent(form);
    	addComponent(formContainer);
    }
    
    private void userDetail(User user) {
        if (user == null) {
        	user = identityService.newUser("");
        }
        BeanItem<User> item = new BeanItem<User>(user);
        fieldGroup.setItemDataSource(item);
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(UserView.MAIN_TITLE);
		bcItem.setViewName(UserView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}

	
	public boolean checkTrungMa(String ma, String act){
		try {
			if(ma == null || ma.isEmpty()){
				return false;
			}
			if(Constants.ACTION_ADD.equals(act)){
				List<User> list = identityService.createUserQuery().userId(ma).list();
				if(list == null || list.isEmpty()){
					return true;
				}
			}else if(!Constants.ACTION_DEL.equals(act)){
				List<User> list = identityService.createUserQuery().userId(ma).list();
				if(list == null || list.size() <= 1){
					return true;
				}
			}else if(Constants.ACTION_DEL.equals(act)){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
    
	@SuppressWarnings("unchecked")
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BUTTON_CANCEL_ID.equals(button.getId())){
				UI.getCurrent().getNavigator().navigateTo(UserView.VIEW_NAME);
			} else if(BUTTON_GEN_PASS_ID.equals(button.getId())){
				if(button.getData() != null && button.getData() instanceof TextField){
					TextField txtPass = (TextField) button.getData();
					String pass = StringUtils.randomString(10);
					txtPass.setValue(pass);
				}
			} else{
				if(Constants.ACTION_EDIT.equals(action)){
					Notification notf = new Notification("Thông báo", "Cập nhật thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid() && infoFieldGroup.isValid()){
						fieldGroup.commit();
						infoFieldGroup.commit();
						
						
						HtDonVi donVi = (HtDonVi) infoItem.getItemProperty("donVi").getValue();
						HtChucVu chucVu = (HtChucVu) infoItem.getItemProperty("chucVu").getValue();
					
						Date expiryDate = (Date) infoItem.getItemProperty("expiryDate").getValue();
						
						identityService.saveUser(userCurrent);
						identityService.setUserInfo(userCurrent.getId(), "HT_DON_VI", donVi.getMa());
						identityService.setUserInfo(userCurrent.getId(), "HT_CHUC_VU", chucVu.getMa());
						if(expiryDate != null){
							identityService.setUserInfo(userCurrent.getId(), "EXPIRY_DATE", expiryDate.getTime()+"");
						}
						
						if(selectedGroup != null){
							for(String key : selectedGroup.keySet()){
								long count = identityService.createGroupQuery().groupMember(userCurrent.getId()).groupId(key).count();
								if(selectedGroup.get(key) != null && selectedGroup.get(key)){
									if(count == 0){
										identityService.createMembership(userCurrent.getId(), key);
									}
								}else{
									identityService.deleteMembership(userCurrent.getId(), key);
								}
							}
						}
						
						UI.getCurrent().getNavigator().navigateTo(UserView.VIEW_NAME);
						
						notf.show(Page.getCurrent());
					}
				}else if(Constants.ACTION_ADD.equals(action)){
					Notification notf = new Notification("Thông báo", "Thêm mới thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid() && infoFieldGroup.isValid()){
						fieldGroup.commit();
						infoFieldGroup.commit();
						notf.show(Page.getCurrent());

						HtDonVi donVi = (HtDonVi) infoItem.getItemProperty("donVi").getValue();
						HtChucVu chucVu = (HtChucVu) infoItem.getItemProperty("chucVu").getValue();
						
						
						if(userCurrent.getPassword() != null){
							userCurrent.setPassword(StringUtils.getSHA512(userCurrent.getPassword()));
						}
						
						identityService.saveUser(userCurrent);
						identityService.setUserInfo(userCurrent.getId(), "HT_DON_VI", donVi.getMa());
						identityService.setUserInfo(userCurrent.getId(), "HT_CHUC_VU", chucVu.getMa());
						
						if(selectedGroup != null){
							for(String key : selectedGroup.keySet()){
								long count = identityService.createGroupQuery().groupMember(userCurrent.getId()).groupId(key).count();
								if(selectedGroup.get(key) != null && selectedGroup.get(key)){
									if(count == 0){
										identityService.createMembership(userCurrent.getId(), key);
									}
								}else{
									identityService.deleteMembership(userCurrent.getId(), key);
								}
							}
						}
						
						
						
						UI.getCurrent().getNavigator().navigateTo(UserView.VIEW_NAME);
						
					}
				}else if(Constants.ACTION_DEL.equals(action)){
					Notification notf = new Notification("Thông báo", "Xóa thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						notf.show(Page.getCurrent());
						User current = ((BeanItem<User>) fieldGroup.getItemDataSource()).getBean();
						identityService.deleteUser(current.getId());
						UI.getCurrent().getNavigator().navigateTo(UserView.VIEW_NAME);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        
	}


}
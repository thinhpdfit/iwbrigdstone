package com.eform.ui.view.task;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;

import com.vaadin.ui.Button.ClickListener;
import com.eform.common.ExplorerLayout;
import com.eform.ui.custom.paging.HistoricTaskPaginationBar;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;

public class HistoricTaskListContainer extends CssLayout{
	
	private CssLayout taskContainer;
	private HistoricTaskPaginationBar paginationBar;
	
	public HistoricTaskListContainer(HistoryService historyService,RepositoryService repositoryService, String category, String userId,  ClickListener clickListener){
		
		addStyleName(ExplorerLayout.TASK_LIST_WRAP);
		setWidth("100%");
		taskContainer = new CssLayout();
		taskContainer.addStyleName(ExplorerLayout.DATA_WRAP);
		
		taskContainer.setWidth("100%");
		addComponent(taskContainer);
		
		paginationBar = new HistoricTaskPaginationBar(taskContainer, category, userId, clickListener);
		
		if(paginationBar.getRowCount() > 0){
			CssLayout pagingWrap = new CssLayout();
			pagingWrap.setWidth("100%");
			pagingWrap.addStyleName(ExplorerLayout.PAGING_WRAP);
			pagingWrap.addComponent(paginationBar);
			addComponent(pagingWrap);
		}else{
			Label lbl = new Label("Không tìm thấy công việc nào");
			lbl.addStyleName(ExplorerLayout.TASK_LIST_EMPTY);
			addComponent(lbl);
		}
	}

	public HistoricTaskPaginationBar getPaginationBar() {
		return paginationBar;
	}
	
}

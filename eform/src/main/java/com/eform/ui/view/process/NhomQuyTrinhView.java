package com.eform.ui.view.process;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.workflow.ITrangThai;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.NhomQuyTrinh;
import com.eform.service.NhomQuyTrinhService;
import com.eform.ui.custom.ButtonAction;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.StatusCommentLayout;
import com.eform.ui.custom.paging.NhomQuyTrinhPaginationBar;
import com.eform.ui.view.dashboard.DashboardView;
import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = NhomQuyTrinhView.VIEW_NAME)
public class NhomQuyTrinhView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "nhom-quy-trinh";
	static final String MAIN_TITLE = "Ứng dụng";
	private static final String SMALL_TITLE = "";

	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	private static final String BTN_VIEW_ID = "xem";
	private static final String BTN_ADDNEW_ID = "themMoi";
	private final NhomQuyTrinhView currentView = this;
	
	@Autowired
	NhomQuyTrinhService nhomQuyTrinhService;
	private List<NhomQuyTrinh> nhomQuyTrinhSearchList;
	private BeanItemContainer<NhomQuyTrinh> container;
	private Table table = new Table();
	private WorkflowManagement<NhomQuyTrinh> nhomQuyTrinhWM;
	
	private final PropertysetItem searchItem = new PropertysetItem();
	private final FieldGroup searchFieldGroup = new FieldGroup();
	private Button btnAddNew;
	
	private NhomQuyTrinhPaginationBar paginationBar;
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;

	@PostConstruct
    void init() {
		messageSourceAccessor= new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());
		nhomQuyTrinhWM = new WorkflowManagement<NhomQuyTrinh>(EChucNang.E_FORM_NHOM_QUY_TRINH.getMa());
		nhomQuyTrinhSearchList = Lists.newArrayList(nhomQuyTrinhService.findAll());
		
		container = new BeanItemContainer<NhomQuyTrinh>(NhomQuyTrinh.class, new ArrayList());
		paginationBar = new NhomQuyTrinhPaginationBar(container, nhomQuyTrinhService);
		
    	searchItem.addItemProperty("ma", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("ten", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("trangThai", new ObjectProperty<ComboboxItem>(new ComboboxItem()));
    	searchItem.addItemProperty("nhomQuyTrinh", new ObjectProperty<NhomQuyTrinh>(new NhomQuyTrinh()));
    	searchFieldGroup.setItemDataSource(searchItem);
    	
		initMainTitle();
		initSearchForm();
		initDataContent();
		
		btnAddNew = new Button();
    	btnAddNew.setIcon(FontAwesome.PLUS);
    	btnAddNew.addClickListener(this);
    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
    	btnAddNew.setDescription("Tạo mới ứng dụng");
    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
    	btnAddNew.setId(BTN_ADDNEW_ID);
		addComponent(btnAddNew);
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSourceAccessor.getMessage(Messages.UI_VIEW_HOME));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		
		CssFormLayout searchForm = new CssFormLayout(-1, 60);
		
		TextField txtMaNhomQuyTrinh = new TextField("Mã ứng dụng");
		searchForm.addFeild(txtMaNhomQuyTrinh);
		
		TextField txtTenNhomQuyTrinh = new TextField("Tên ứng dụng");
		searchForm.addFeild(txtTenNhomQuyTrinh);
		
//		BeanItemContainer<NhomQuyTrinh> cbbLoaiDMContainer = new BeanItemContainer<NhomQuyTrinh>(NhomQuyTrinh.class, nhomQuyTrinhSearchList);
//		ComboBox cbbNhomQuyTrinh = new ComboBox("Nhóm quy trình cha", cbbLoaiDMContainer);
//    	cbbNhomQuyTrinh.setItemCaptionMode(ItemCaptionMode.PROPERTY);
//    	cbbNhomQuyTrinh.setItemCaptionPropertyId("ten");
//    	searchForm.addFeild(cbbNhomQuyTrinh);
	
    	List<ITrangThai> trangThaiListTmp = nhomQuyTrinhWM.getTrangThaiList();
    	List<ComboboxItem> trangThaiNhomQuyTrinhList = new ArrayList<>();
    	if(trangThaiListTmp != null){
    		for (ITrangThai trangThai : trangThaiListTmp) {
    			trangThaiNhomQuyTrinhList.add(new ComboboxItem(trangThai.getTen(), new Long(trangThai.getId())));
			}
    	}
    	
    	BeanItemContainer<ComboboxItem> cbbTrangThaiContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, trangThaiNhomQuyTrinhList);
		ComboBox cbbTrangThai = new ComboBox("Trạng thái", cbbTrangThaiContainer);
		cbbTrangThai.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		cbbTrangThai.setItemCaptionPropertyId("text");
    	searchForm.addFeild(cbbTrangThai);

		searchFieldGroup.bind(txtMaNhomQuyTrinh, "ma");
		searchFieldGroup.bind(txtTenNhomQuyTrinh, "ten");
		searchFieldGroup.bind(cbbTrangThai, "trangThai");
		
		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(currentView);
		searchForm.addButton(btnSearch);
		paginationBar.fixEnterHotKey(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(currentView);
		searchForm.addButton(btnRefresh);
		
		addComponent(searchForm);
	}
	
	public void initDataContent(){
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
		List<ITrangThai> trangThaiList = nhomQuyTrinhWM.getTrangThaiList();
		if(trangThaiList != null && !trangThaiList.isEmpty()){
			dataWrap.addComponent(new StatusCommentLayout(trangThaiList));
		}
		
		table.setContainerDataSource(container);
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setResponsive(true);
		table.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		table.setColumnHeader("ma", "Mã");
		table.setColumnHeader("ten", "Tên");
		
		table.addGeneratedColumn("action", new ColumnGenerator() { 
			private static final long serialVersionUID = 1L;

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				NhomQuyTrinh current = (NhomQuyTrinh) itemId;
				Workflow<NhomQuyTrinh> wf = nhomQuyTrinhWM.getWorkflow(current);
				
				HorizontalLayout actionWrap = new HorizontalLayout();
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
		        Button btnView = new Button();
		        btnView.setId(BTN_VIEW_ID);
		        btnView.setDescription("Xem");
		        btnView.setIcon(FontAwesome.EYE);
		        btnView.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnView.addClickListener(currentView);
		        btnView.setData(itemId);
		        btnView.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnView);

		        
		        new ButtonAction<NhomQuyTrinh>().getButtonAction(actionWrap, wf, currentView);
		        return actionWrap;
		    }
		});

		table.setColumnHeader("action", "Thao tác");
		table.setRowHeaderMode(RowHeaderMode.INDEX);

		table.setVisibleColumns("ma", "ten", "action");
		

		table.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if ("ma".equals(propertyId)) {
                    return "center-aligned";
                }else if("ten".equals(propertyId)){
                    return "left-aligned";
                }else if("trangThai".equals(propertyId)){
                    return "left-aligned ";
                }else if("nhomQuyTrinh".equals(propertyId)){
                    return "left-aligned";
                }else if("action".equals(propertyId)){
                    return "left-aligned";
                }
            	NhomQuyTrinh current = (NhomQuyTrinh) itemId;
				Workflow<NhomQuyTrinh> wf = nhomQuyTrinhWM.getWorkflow(current);
				return wf.getStyleName();
			}
			
		});

		dataWrap.addComponent(table);
		dataWrap.addComponent(paginationBar);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			if(BTN_SEARCH_ID.equals(event.getButton().getId())){
				
					if(searchFieldGroup.isValid()){
						searchFieldGroup.commit();
						String ma = (String) searchItem.getItemProperty("ma").getValue();
						ma = ma != null ? "%"+ma.trim()+"%" : null;
						String ten = (String) searchItem.getItemProperty("ten").getValue();
						ten = ten != null ? "%"+ten.trim()+"%" : null;
						ComboboxItem trangThai = (ComboboxItem) searchItem.getItemProperty("trangThai").getValue();
						List<Long> trangThaiList = null;
						if(trangThai != null && trangThai.getValue() != null){
							trangThaiList = new ArrayList<Long>();
							trangThaiList.add((Long) trangThai.getValue());
						}
//						NhomQuyTrinh nhomQuyTrinh = (NhomQuyTrinh) searchItem.getItemProperty("nhomQuyTrinh").getValue();
//						if(nhomQuyTrinh != null && nhomQuyTrinh.getId() == null){
//							nhomQuyTrinh = null;
//						}
						paginationBar.search(ma, ten, trangThaiList, null);
					}
				
			}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
		    	searchFieldGroup.clear();
		    	paginationBar.search(null, null, null, null);
			}else if(BTN_VIEW_ID.equals(event.getButton().getId())){
				NhomQuyTrinh currentRow = (NhomQuyTrinh) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getMa().toLowerCase());
			}else if(ButtonAction.BTN_EDIT_ID.equals(event.getButton().getId())){
				NhomQuyTrinh currentRow = (NhomQuyTrinh) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhDetailView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getMa().toLowerCase());
			}else if(ButtonAction.BTN_DEL_ID.equals(event.getButton().getId())){
				NhomQuyTrinh currentRow = (NhomQuyTrinh) event.getButton().getData();
		        UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getMa().toLowerCase());
			}else if(ButtonAction.BTN_APPROVE_ID.equals(event.getButton().getId())){
				NhomQuyTrinh currentRow = (NhomQuyTrinh) event.getButton().getData();
		        //UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhDetailView.VIEW_NAME+"/"+Constants.ACTION_APPROVE+"/"+currentRow.getMa().toLowerCase());
				Workflow<NhomQuyTrinh> nhomQuyTrinhWf = nhomQuyTrinhWM.getWorkflow(currentRow);
				NhomQuyTrinh current = nhomQuyTrinhWf.pheDuyet();
				nhomQuyTrinhService.save(current);
				paginationBar.reloadData();
				Notification notf = new Notification("Thông báo", nhomQuyTrinhWf.getPheDuyetTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(Page.getCurrent());
			}else if(ButtonAction.BTN_REFUSE_ID.equals(event.getButton().getId())){
				NhomQuyTrinh currentRow = (NhomQuyTrinh) event.getButton().getData();
		        //UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhDetailView.VIEW_NAME+"/"+Constants.ACTION_REFUSE+"/"+currentRow.getMa().toLowerCase());
				Workflow<NhomQuyTrinh> nhomQuyTrinhWf = nhomQuyTrinhWM.getWorkflow(currentRow);
				NhomQuyTrinh current = nhomQuyTrinhWf.tuChoi();
				nhomQuyTrinhService.save(current);
				paginationBar.reloadData();
				Notification notf = new Notification("Thông báo", nhomQuyTrinhWf.getTuChoiTen()+" thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(Page.getCurrent());
			}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
		        UI.getCurrent().getNavigator().navigateTo(NhomQuyTrinhDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra", Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
		}
	}

}

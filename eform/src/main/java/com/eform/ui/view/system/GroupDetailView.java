package com.eform.ui.view.system;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.HtQuyenServices;
import com.eform.model.entities.GroupQuyen;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.HtQuyen;
import com.eform.service.GroupQuyenServices;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = GroupDetailView.VIEW_NAME)
public class GroupDetailView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "thong-tin-nhom-nguoi-dung";
	private static final String MAIN_TITLE = "Thông tin nhóm người dùng";
	private static final String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_ADD_ID = "btn-add";
	private static final String BUTTON_DEL_ID = "btn-del";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private Group groupCurrent;
	private CssLayout mainTitleWrap;
	private String action;

	private final FieldGroup fieldGroup = new FieldGroup();
	private CssLayout formContainer;
	private Button btnUpdate;
	private Button btnBack;

	private TreeTable quyenTreeTable;
	private WorkflowManagement<HtQuyen> HtQuyenWM;
	private Map<Long,Boolean> selectedQuyen;
	private Map<Long,CheckBox> quyenCheckboxMap;
	@Autowired
	HtQuyenServices htQuyenServices;
	@Autowired
	GroupQuyenServices groupQuyenServices;
	
	@Autowired
	protected IdentityService identityService;
	@Autowired
	private MessageSource messageSource;
	private Map<Long, LinkedHashSet<Long>> quyenMap;
	private List<HtQuyen> htQuyenList;
	
    @PostConstruct
    void init() {
    	initMainTitle();
    	HtQuyenWM = new WorkflowManagement<HtQuyen>(EChucNang.HT_QUYEN.getMa());
    	List<Long> trangThai=new ArrayList();
		trangThai.add(new Long(HtQuyenWM.getTrangThaiSuDung().getId()));
		htQuyenList = htQuyenServices.getHtQuyenFind(null, null, null, trangThai, null, -1, -1);
    }

    @Override
    public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				if(Constants.ACTION_ADD.equals(action)){
					groupCurrent = identityService.newGroup("");
					groupDetail(groupCurrent);
					initDetailForm();
				}else{
					String ma = m.group(3);
					groupCurrent = identityService.createGroupQuery().groupId(ma).singleResult();
					if(groupCurrent != null){
						groupDetail(groupCurrent);
						initDetailForm();
					}
					
				}
				
			}
		}

    }
    
    public void initDetailForm(){
    	formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	FormLayout form = new FormLayout();
    	form.setMargin(true);
    	
    	TextField txtId = new TextField("Mã nhóm");
    	txtId.focus();
    	txtId.setRequired(true);
    	txtId.addTextChangeListener(new TextChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void textChange(TextChangeEvent event) {

				TextField txt = (TextField) fieldGroup.getField("id");
				System.out.println(event.getText());
				if(!checkTrungMa(event.getText(), action)){
					txt.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
				}else{
					txt.setComponentError(null);
				}
			}
    	});

    	txtId.setTextChangeEventMode(TextChangeEventMode.LAZY);
    	
    	txtId.setSizeFull();
    	txtId.setNullRepresentation("");
    	txtId.setRequiredError("Không được để trống");
    	txtId.addValidator(new StringLengthValidator("Số ký tự tối đa là 20", 1, 20, false));   
    	txtId.addValidator(new RegexpValidator("[0-9a-zA-Z_]+", "Chỉ chấp nhận các ký tự 0-9 a-z A-Z _"));
    	
    	TextField txtName = new TextField("Tên nhóm");
    	txtName.setRequired(true);
    	txtName.setSizeFull();
    	txtName.setNullRepresentation("");
    	txtName.setRequiredError("Không được để trống");
    	txtName.addValidator(new StringLengthValidator("Số ký tự tối đa là 250", 1, 250, false)); 
    	
    	List<ComboboxItem> typeList = new ArrayList<ComboboxItem>();
		typeList.add(new ComboboxItem("security-role", "security-role"));
		typeList.add(new ComboboxItem("assignment", "assignment"));
		
		ComboBox cbbType = new ComboBox("Loại nhóm");
		for (CommonUtils.GroupType item : CommonUtils.GroupType.values()) {
			cbbType.addItem(item.getCode());
			cbbType.setItemCaption(item.getCode(), item.getName());
		}
		cbbType.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		cbbType.setRequired(true);
		cbbType.setRequiredError("Không được để trống");
    	
    	
    	fieldGroup.bind(txtId, "id");
    	fieldGroup.bind(txtName, "name");
    	fieldGroup.bind(cbbType, "type");
    	
    	txtId.setReadOnly(!Constants.ACTION_ADD.equals(action));
    	txtName.setReadOnly("xem".equals(action));
    	cbbType.setReadOnly("xem".equals(action));
    	
    	form.addComponent(txtId);
    	form.addComponent(txtName);
    	form.addComponent(cbbType);

    	
    	quyenTreeTable=new TreeTable("Danh sách quyền");
    	quyenTreeTable.setWidth("100%");
		quyenTreeTable.addContainerProperty("ma", CheckBox.class, null);
		quyenTreeTable.addContainerProperty("ten", String.class, null);

		quyenTreeTable.addContainerProperty("actions", OptionGroup.class, null);

		quyenTreeTable.setColumnHeader("ma", "Mã");
		quyenTreeTable.setColumnHeader("ten", "Tên");
		quyenTreeTable.setColumnHeader("actions", "Quyền");

		quyenTreeTable.setVisibleColumns("ma", "ten", "actions");

		if(Constants.ACTION_ADD.equals(action)==false){
			List<GroupQuyen> groupQuyenList=groupQuyenServices.getGroupQuyenFind(groupCurrent.getId(), -1, -1);
			for(GroupQuyen groupQuyen:groupQuyenList){
				selectedQuyen.put(groupQuyen.getHtQuyen().getId(), true);
				if(groupQuyen.getAction() != null){
					int action = groupQuyen.getAction().intValue();
					LinkedHashSet<Long> hs = new LinkedHashSet<Long>();
					for(CommonUtils.EAction a : CommonUtils.EAction.values()){
						if((a.getCode()&action) == a.getCode()){
							hs.add(new Long(a.getCode()));
						}
					}
					quyenMap.put(groupQuyen.getHtQuyen().getId(), hs);
				}
			}
		}
		
		// Create the tree nodes and set the hierarchy
		
		for(HtQuyen quyen:htQuyenList){
			Boolean check=selectedQuyen.get(quyen.getId());
			CheckBox checkBox =new CheckBox();
			checkBox.setCaption(quyen.getMa());
			checkBox.setValue(check!=null&&check);
			checkBox.setReadOnly("xem".equals(action));
			checkBox.addValueChangeListener(new ValueChangeListener(){
				@Override
				public void valueChange(ValueChangeEvent event) {
					Boolean check=(Boolean) event.getProperty().getValue();
					if(check==null){
						check = false;
					}
					selectedQuyen.put(quyen.getId(),check);
					Collection<HtQuyen> childList = (Collection<HtQuyen>) quyenTreeTable.getChildren(quyen);
					if(childList != null){
						for (HtQuyen q : childList) {
							if(q != null){
								CheckBox chk = quyenCheckboxMap.get(q.getId());
								if(chk != null){
									chk.setValue(check);
								}
							}
							
						}
					}
				}
			});
			
			
			OptionGroup chkActions = new OptionGroup("Quyền");
			chkActions.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
			chkActions.addStyleName(ExplorerLayout.FORM_CONTROL);
			chkActions.setNullSelectionAllowed(true);
			chkActions.setMultiSelect(true);
			
			if(quyenMap.get(quyen.getId()) != null){
				chkActions.setValue(quyenMap.get(quyen.getId()));
			}
			
			
			if(quyen.getAction() != null){
				int actionCurrent = quyen.getAction().intValue();
				for(CommonUtils.EAction a : CommonUtils.EAction.values()){
					int actionValue = a.getCode();
    				if((actionValue&actionCurrent) == actionValue){
    					chkActions.addItem(new Long(actionValue));
    					chkActions.setItemCaption(new Long(actionValue), a.getName());
    				}
				}
			}
			
			chkActions.addValueChangeListener(new ValueChangeListener(){

				@Override
				public void valueChange(ValueChangeEvent event) {
					try {
						Object value = event.getProperty().getValue();
						if(value instanceof Set){
							LinkedHashSet quyenLhs = new LinkedHashSet((Set)value);
							quyenMap.put(quyen.getId(), quyenLhs);
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				
			});
			
			quyenTreeTable.addItem(new Object[]{checkBox,quyen.getTen(), chkActions},quyen);
			quyenTreeTable.setChildrenAllowed(quyen, false);
			quyenCheckboxMap.put(quyen.getId(), checkBox);
		}
		for(HtQuyen quyen:htQuyenList){
			if(quyen.getHtQuyen()!=null){
				quyenTreeTable.setChildrenAllowed(quyen.getHtQuyen(), true);
				quyenTreeTable.setParent(quyen, quyen.getHtQuyen());
			}
		}
    	form.addComponent(quyenTreeTable);
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	if(!Constants.ACTION_VIEW.equals(action)){
    		btnUpdate = new Button();
    		btnUpdate.addClickListener(this);
    		buttons.addComponent(btnUpdate);
    		if(Constants.ACTION_EDIT.equals(action)){
        		btnUpdate.setCaption("Cập nhật");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_EDIT_ID);
        	}else if(Constants.ACTION_ADD.equals(action)){
        		btnUpdate.setId(BUTTON_ADD_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption("Thêm mới");
        	}else if(Constants.ACTION_DEL.equals(action)){
        		btnUpdate.setId(BUTTON_DEL_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
        		btnUpdate.setCaption("Xóa");
        	}
    	}
    	
    	btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
    	form.addComponent(buttons);

    	formContainer.addComponent(form);
    	addComponent(formContainer);
    	
    	
    	
    }
    
    private void groupDetail(Group group) {
        if (group == null) {
        	group = identityService.newGroup("");
        }
        BeanItem<Group> item = new BeanItem<Group>(group);
        fieldGroup.setItemDataSource(item);
    	
    	selectedQuyen=new HashMap();
    	quyenCheckboxMap = new HashMap();
    	quyenMap = new HashMap();
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(GroupView.MAIN_TITLE);
		bcItem.setViewName(GroupView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BUTTON_CANCEL_ID.equals(button.getId())){
				UI.getCurrent().getNavigator().navigateTo(GroupView.VIEW_NAME);
			}else{
				if(Constants.ACTION_EDIT.equals(action)){
					Notification notf = new Notification("Thông báo", "Cập nhật thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						notf.show(Page.getCurrent());
						identityService.saveGroup(groupCurrent);
						
						List<GroupQuyen> updateList=new ArrayList<>();
						for(HtQuyen quyen : htQuyenList){
							Boolean check=selectedQuyen.get(quyen.getId());
							if(check!=null&&check){
								GroupQuyen groupQuyen=new GroupQuyen();
								groupQuyen.setGroupId(groupCurrent.getId());
								groupQuyen.setHtQuyen(quyen);
								groupQuyen.setAction(getActionQuyen(quyenMap.get(quyen.getId())));
								updateList.add(groupQuyen);
							}
						}
						groupQuyenServices.update(groupCurrent, updateList);
						UI.getCurrent().getNavigator().navigateTo(GroupView.VIEW_NAME);
						
					}
				}else if(Constants.ACTION_ADD.equals(action)){
					Notification notf = new Notification("Thông báo", "Thêm mới thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						notf.show(Page.getCurrent());
						identityService.saveGroup(groupCurrent);

						List<GroupQuyen> updateList=new ArrayList<>();
						for(HtQuyen quyen : htQuyenList){
							Boolean check=selectedQuyen.get(quyen.getId());
							if(check!=null&&check){
								GroupQuyen groupQuyen=new GroupQuyen();
								groupQuyen.setGroupId(groupCurrent.getId());
								groupQuyen.setHtQuyen(quyen);
								groupQuyen.setAction(getActionQuyen(quyenMap.get(quyen.getId())));
								updateList.add(groupQuyen);
							}
						}
						groupQuyenServices.update(groupCurrent, updateList);
						UI.getCurrent().getNavigator().navigateTo(GroupView.VIEW_NAME);
						
					}
				}else if(Constants.ACTION_DEL.equals(action)){
					Notification notf = new Notification("Thông báo", "Xóa thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						notf.show(Page.getCurrent());
						Group current = ((BeanItem<Group>) fieldGroup.getItemDataSource()).getBean();
						identityService.deleteGroup(current.getId());
						UI.getCurrent().getNavigator().navigateTo(GroupView.VIEW_NAME);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        
	}
	
	
	public Long getActionQuyen(LinkedHashSet<Long> hs){
		if(hs != null){
			int action = 0;
			for(Long a : hs){
				action = action|a.intValue();
			}
			return new Long(action);
		}
		return 0L;
	}
	
	public boolean checkTrungMa(String ma, String act){
		try {
			if(ma == null || ma.isEmpty()){
				return false;
			}
			if(Constants.ACTION_ADD.equals(act)){
				List<Group> list = identityService.createGroupQuery().groupId(ma).list();
				if(list == null || list.isEmpty()){
					return true;
				}
			}else if(!Constants.ACTION_DEL.equals(act)){
				List<Group> list = identityService.createGroupQuery().groupId(ma).list();
				if(list == null || list.size() <= 1){
					return true;
				}
			}else if(Constants.ACTION_DEL.equals(act)){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
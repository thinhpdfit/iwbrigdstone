package com.eform.ui.view.task;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.utils.CommonUtils;
import com.eform.ui.view.process.HistoricTaskDetailView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = TaskListView.VIEW_NAME)
public class TaskListView extends VerticalLayout implements View, ClickListener{

private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;
	
	private CssLayout mainTitleWrap;

	public static final String VIEW_NAME = "task-list";
	static String MAIN_TITLE = "Task View";
	private static String SMALL_TITLE = "";
	
	private static final String BTN_ACTION_ID = "action";
	public static final String BTN_DETAIL_ID = "detail";
	
	public static final String INBOX_CATE = "inbox";
	public static final String MY_TASK_CATE = "my-tasks";
	public static final String QUEUED_CATE = "queued";
	public static final String INVOLED_CATE = "involed";
	public static final String ARCHIVED_CATE = "archived";
	public static final String ASSIGNED_CATE = "assigned";
	
	@Autowired
	private TaskService taskService;
	@Autowired
	protected IdentityService identityService;
	@Autowired
	protected HistoryService historyService;
	@Autowired
	private RepositoryService repositoryService;
	
	private String category;
	private CssLayout contaner = new CssLayout();
	
	@Autowired
	private MessageSource messageSource;
	private TaskCategoryMenuBar taskMenu;
	
	@PostConstruct
	public void init(){
		MAIN_TITLE = messageSource.getMessage(Messages.UI_VIEW_TASK_TITLE, null, VaadinSession.getCurrent().getLocale());
		SMALL_TITLE = messageSource.getMessage(Messages.UI_VIEW_TASK_TITLE_SMALL, null, VaadinSession.getCurrent().getLocale());
		initMainTitle();
		contaner = new CssLayout();
		contaner.setWidth("100%");
		addComponent(contaner);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_:]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				category = m.group(1);
				contaner.removeAllComponents();
				taskMenu.setActiveButton(category);
				if(ARCHIVED_CATE.equals(category) || ASSIGNED_CATE.equals(category)){
					HistoricTaskListContainer taskContainer = new HistoricTaskListContainer(historyService, repositoryService, category, CommonUtils.getUserLogedIn(), this);
					contaner.addComponent(taskContainer);
				}else{
					TaskListContainer taskContainer = new TaskListContainer(taskService, repositoryService, category, CommonUtils.getGroupId(identityService), CommonUtils.getUserLogedIn(), this);
					contaner.addComponent(taskContainer);
				}
				
			}else{
				UI.getCurrent().getNavigator().navigateTo(TaskListView.VIEW_NAME+"/"+TaskListView.INBOX_CATE);
			}
		}
	}
	
	public void initMainTitle(){
		
		
		mainTitleWrap = new CssLayout();
		mainTitleWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		mainTitleWrap.setStyleName(ExplorerLayout.MAIN_TITLE_WRAP);
		addComponent(mainTitleWrap);
		
		Label title = new Label(MAIN_TITLE);
		title.setIcon(FontAwesome.HASHTAG);
		title.setStyleName(ExplorerLayout.MAIN_TITLE);
		mainTitleWrap.addComponent(title);
		
		taskMenu = new TaskCategoryMenuBar(historyService, taskService, CommonUtils.getUserLogedIn(),  CommonUtils.getGroupId(identityService));
		mainTitleWrap.addComponent(taskMenu);
	}
	
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_ACTION_ID.equals(event.getButton().getId())){
			Task currentRow = (Task) button.getData();
			UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
		}else if(BTN_DETAIL_ID.equals(event.getButton().getId())){
			if(ARCHIVED_CATE.equals(category) || ASSIGNED_CATE.equals(category)){
				HistoricTaskInstance currentRow = (HistoricTaskInstance) button.getData();
				UI.getCurrent().getNavigator().navigateTo(HistoricTaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW_HISTORY+"/"+currentRow.getId());
			}else{
				Task currentRow = (Task) button.getData();
				UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
			}
		}
	}


}

package com.eform.ui.view.process;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.AuthenticationUtils;
import com.eform.common.utils.CommonUtils;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.HtQuyenServices;
import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.NhomQuyTrinh;
import com.eform.service.DeploymentHtDonViService;
import com.eform.service.GroupQuyenServices;
import com.eform.service.HtDonViServices;
import com.eform.service.NhomQuyTrinhService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = ProcessDefinitionListView.VIEW_NAME)
@PreserveOnRefresh
public class ProcessDefinitionListView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;

	public static final String VIEW_NAME = "deployed-process";
	public static String MAIN_TITLE = "Quy trình đã ban hành";
	private static final String SMALL_TITLE = "";
	
	public static final String BTN_SEARCH_ID = "timKiem";
	public static final String BTN_REFRESH_ID = "refresh";
	public static final String BTN_VIEW_ID = "xem";
	public static final String BTN_START_ID = "start";
	public static final String BTN_DELETE_ID = "delete";
	public static final String BTN_APPROVE_ID = "pheDuyet";
	public static final String BTN_REFUSE_ID = "tuChoi";
	public static final String BTN_EDIT_ID = "sua";
	public static final String BTN_TAB_ID = "btnTab";
	
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;

	private ProcessDefinitionContainer container;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private HtDonViServices htDonViServices;
	@Autowired
	private DeploymentHtDonViService deploymentHtDonViService;	

	private Set<String> departmentIds;
	private Set<String> departmentCurrentIds;
	private Boolean isActive;

	@Autowired
	private HtQuyenServices htQuyenService;
	@Autowired
	private GroupQuyenServices groupQuyenServices;
	
	Map<Long, Boolean> mapQuyen;
	@Autowired
	private NhomQuyTrinhService nhomQuyTrinhService;
	private List<Button> buttonTabList;
	
	@PostConstruct
    void init() {
		
		messageSourceAccessor= new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());
		MAIN_TITLE=messageSourceAccessor.getMessage(Messages.UI_VIEW_PROCESSDEFINITION);
		mapQuyen = AuthenticationUtils.getQuyen(null, VIEW_NAME, CommonUtils.getGroupId(identityService), htQuyenService, groupQuyenServices);
    	String userId = CommonUtils.getUserLogedIn();
		String maDonVi = identityService.getUserInfo(userId, "HT_DON_VI");
		departmentIds = new HashSet<String>();
		if(maDonVi != null){
			List<HtDonVi> dvList = htDonViServices.getHtDonViFind(maDonVi,null, null, null, 0, 1);
			if(!dvList.isEmpty()){
				List<DeploymentHtDonVi> deploymentHtDonViList = deploymentHtDonViService.getDeploymentHtDonViFind(null, dvList.get(0));
				if(deploymentHtDonViList != null && !deploymentHtDonViList.isEmpty()){
					
					for(DeploymentHtDonVi item : deploymentHtDonViList){
						departmentIds.add(item.getDeploymentId());
					}
					departmentCurrentIds = departmentIds;
				}
			}
		}

		if(!CommonUtils.isAdmin(identityService)){
			isActive = true;
		}
		buttonTabList = new ArrayList();
		initMainTitle();
		intCategoryTab();
		initContent();
		
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSourceAccessor.getMessage(Messages.UI_VIEW_HOME));
		
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		
		TextField txtSearchTen = new TextField();
		txtSearchTen.addStyleName(ExplorerLayout.PROCESS_DEFINITION_TXT_SEARCH);
		txtSearchTen.setInputPrompt("Tìm kiếm");
		txtSearchTen.addTextChangeListener(new TextChangeListener() {
			@Override
			public void textChange(TextChangeEvent event) {
				String searchStr = event.getText();
				search(searchStr);
			}

			
    	});
		
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false, txtSearchTen));
	}
	
	
	public void intCategoryTab(){
		CssLayout tabContainer = new CssLayout();
		tabContainer.setWidth("100%");
		tabContainer.addStyleName(ExplorerLayout.PRO_DEF_CATE_TAB);
		addComponent(tabContainer);
		List<Long> trangThai = null;
    	WorkflowManagement<NhomQuyTrinh> wf = new WorkflowManagement<NhomQuyTrinh>(EChucNang.E_FORM_NHOM_QUY_TRINH.getMa());
    	if(wf != null && wf.getTrangThaiSuDung() != null){
    		trangThai = new ArrayList();
    		trangThai.add(new Long(wf.getTrangThaiSuDung().getId()));
    	}
    	
    	Button buttonTabAll = new Button("Tất cả");
    	buttonTabAll.addStyleName(Reindeer.BUTTON_LINK);
    	buttonTabAll.addStyleName(ExplorerLayout.PRO_DEF_TAB_BTN);
    	buttonTabAll.addStyleName(ExplorerLayout.PRO_DEF_TAB_BTN_ACTIVE);
    	buttonTabAll.addClickListener(this);
    	buttonTabAll.setId(BTN_TAB_ID);
		tabContainer.addComponent(buttonTabAll);
		buttonTabList.add(buttonTabAll);
    	
		List<String> otherDepIds = new ArrayList(departmentIds);
		
    	List<NhomQuyTrinh> nhomQuyTrinhList = nhomQuyTrinhService.getNhomQuyTrinhFind(null, null, trangThai, null, -1, -1);
    	if(nhomQuyTrinhList != null && !nhomQuyTrinhList.isEmpty()){
    		for(NhomQuyTrinh nhomQT : nhomQuyTrinhList){
    			List<Deployment> depList = repositoryService.createDeploymentQuery().deploymentCategory(nhomQT.getMa()).list();
    			if(depList != null && !depList.isEmpty()){
    				List<String> depIds = new ArrayList();
    				for(Deployment dep : depList){
    					depIds.add(dep.getId());
    				}
    				if(depIds != null && !depIds.isEmpty()){
    					List<String> depIdsTmp = new ArrayList(depIds);
    					depIdsTmp.removeAll(departmentIds);
    					depIds.removeAll(depIdsTmp);
    					if(!depIds.isEmpty()){
    						ProcessDefinitionQuery q = repositoryService.createProcessDefinitionQuery();
        					q = q.deploymentIds(new HashSet(depIds));
        					
        					if (isActive != null) {
        						if (isActive) {
        							q = q.active();
        						} else {
        							q = q.suspended();
        						}
        					}
        					
        					long proDefCount = q.count();
        					if(proDefCount > 0L){
        						Button buttonTab = new Button(nhomQT.getTen());
        						buttonTab.addStyleName(Reindeer.BUTTON_LINK);
        						buttonTab.addStyleName(ExplorerLayout.PRO_DEF_TAB_BTN);
        						buttonTab.setData(depIds);
        						otherDepIds.removeAll(depIds);
        						tabContainer.addComponent(buttonTab);
        						buttonTab.addClickListener(this);
        						buttonTab.setId(BTN_TAB_ID);
        						buttonTabList.add(buttonTab);
        					}
    					}
    					 
    				}
    			}
    		}
    	}
    	
    	if(buttonTabList.size() > 1 && !otherDepIds.isEmpty()){
    		Button buttonTabOther = new Button("Khác");
    		buttonTabOther.addStyleName(Reindeer.BUTTON_LINK);
    		buttonTabOther.addStyleName(ExplorerLayout.PRO_DEF_TAB_BTN);
    		buttonTabOther.addClickListener(this);
    		buttonTabOther.setData(otherDepIds);
    		buttonTabOther.setId(BTN_TAB_ID);
    		tabContainer.addComponent(buttonTabOther);
    		buttonTabList.add(buttonTabOther);
    	}
	}
	
	public void initContent(){
		container = new ProcessDefinitionContainer(repositoryService, this, departmentIds, isActive, mapQuyen);
		addComponent(container);
	}
	
	
	public void search(String searchStr){
		searchStr = searchStr != null ? searchStr.trim() : "";
		container.getPaginationBar().search(searchStr, departmentCurrentIds, isActive);
	}
	
	public void setActiveTab(Button btn){
		for(Button b : buttonTabList){
			b.removeStyleName(ExplorerLayout.PRO_DEF_TAB_BTN_ACTIVE);
			if(b.equals(btn)){
				b.addStyleName(ExplorerLayout.PRO_DEF_TAB_BTN_ACTIVE);
			}
		}
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_START_ID.equals(event.getButton().getId())){
			ProcessDefinitionEntity currentRow = (ProcessDefinitionEntity) button.getData();
			UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionDetailView.VIEW_NAME+"/"+Constants.ACTION_START+"/"+currentRow.getId());
		}else if(BTN_VIEW_ID.equals(event.getButton().getId())){
			ProcessDefinitionEntity currentRow = (ProcessDefinitionEntity) button.getData();
			UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
		}else if(BTN_DELETE_ID.equals(event.getButton().getId())){
			ProcessDefinitionEntity currentRow = (ProcessDefinitionEntity) button.getData();
			UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getId());
		}else if(BTN_APPROVE_ID.equals(event.getButton().getId())){
			ProcessDefinitionEntity currentRow = (ProcessDefinitionEntity) button.getData();
			UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionDetailView.VIEW_NAME+"/"+Constants.ACTION_ACTIVE+"/"+currentRow.getId());
		}else if(BTN_REFUSE_ID.equals(event.getButton().getId())){
			ProcessDefinitionEntity currentRow = (ProcessDefinitionEntity) button.getData();
			UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionDetailView.VIEW_NAME+"/"+Constants.ACTION_SUSPEND+"/"+currentRow.getId());
		}else if(BTN_EDIT_ID.equals(event.getButton().getId())){
			ProcessDefinitionEntity currentRow = (ProcessDefinitionEntity) button.getData();
			UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionDetailView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getId());
		}else if(BTN_TAB_ID.equals(event.getButton().getId())){
			Object data = button.getData();
			if(data != null && data instanceof List){
				departmentCurrentIds = new HashSet((List<String>) data);
				container.getPaginationBar().tabFilter(departmentCurrentIds);
			}else{
				departmentCurrentIds = departmentIds;
				container.getPaginationBar().tabFilter(departmentIds);
			}
			setActiveTab(button);
		}
	}

}

package com.eform.ui.view.process;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.model.entities.NhomQuyTrinh;
import com.eform.service.NhomQuyTrinhService;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.paging.ModelerPaginationBar;
import com.eform.ui.view.dashboard.DashboardView;
import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = ModelerView.VIEW_NAME)
public class ModelerView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "modeler";
	static String MAIN_TITLE = "";
	private static String SMALL_TITLE = "";

	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	private static final String BTN_VIEW_ID = "xem";
	private static final String BTN_EDIT_ID = "sua";
	private static final String BTN_EDIT_MODEL_ID = "suaModel";
	private static final String BTN_DEL_ID = "xoa";
	private static final String BTN_DEPLOY_ID = "deploy";
	private static final String BTN_ADDNEW_ID = "themMoi";
	private final ModelerView currentView = this;
	
	private BeanItemContainer<Model> container;
	private List<Model> modelList;
	private Table table = new Table();
	
	private final PropertysetItem searchItem = new PropertysetItem();
	private final FieldGroup searchFieldGroup = new FieldGroup();
	private Button btnAddNew;

	@Autowired
	protected RepositoryService repositoryService;
	
	@Autowired
	private NhomQuyTrinhService nhomQuyTrinhService;
	
	private List<NhomQuyTrinh> nhomQuyTrinhList;	
	
	private ModelerPaginationBar paginationBar;
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;
	@PostConstruct
    void init() {
		messageSourceAccessor= new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());
    	MAIN_TITLE = messageSourceAccessor.getMessage(Messages.UI_VIEW_MODELER_TITLE);
		SMALL_TITLE = messageSourceAccessor.getMessage(Messages.UI_VIEW_MODELER_TITLE_SMALL);
    	nhomQuyTrinhList = Lists.newArrayList(nhomQuyTrinhService.findAll());
		
		container = new BeanItemContainer<Model>(Model.class, new ArrayList());
		paginationBar = new ModelerPaginationBar(container, repositoryService);
		
    	searchItem.addItemProperty("name", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("category", new ObjectProperty<NhomQuyTrinh>(new NhomQuyTrinh()));
    	searchFieldGroup.setItemDataSource(searchItem);
		
		initMainTitle();
		initSearchForm();
		initDataContent();
		
		btnAddNew = new Button();
    	btnAddNew.setIcon(FontAwesome.PLUS);
    	btnAddNew.addClickListener(this);
    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
    	btnAddNew.setDescription(messageSource.getMessage(Messages.UI_VIEW_MODELER_ADD, null, VaadinSession.getCurrent().getLocale()));
    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
    	btnAddNew.setId(BTN_ADDNEW_ID);
		addComponent(btnAddNew);
		
		
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		
		CssFormLayout searchForm = new CssFormLayout(-1, 60);

		TextField txtTen = new TextField("Tên ứng dụng");
		searchForm.addFeild(txtTen);

		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(currentView);
		searchForm.addButton(btnSearch);
		paginationBar.fixEnterHotKey(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(currentView);
		searchForm.addButton(btnRefresh);
		
		searchFieldGroup.bind(txtTen, "name");
		
		addComponent(searchForm);
	}
	
	public void initDataContent(){
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		table.setContainerDataSource(container);
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setResponsive(true);
		table.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		table.setColumnHeader("name", "Tên");
		table.setColumnHeader("createTime", "Thời gian tạo");

		
		table.addGeneratedColumn("createTime", new ColumnGenerator() { 
			private static final long serialVersionUID = 1L;

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				if(itemId != null && itemId instanceof Model){
					Model model = (Model) itemId;
					java.util.Date date = model.getCreateTime();
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
					return df.format(date);
				}
		        return "";
		    }
		});
		
		table.addGeneratedColumn("action", new ColumnGenerator() { 
			private static final long serialVersionUID = 1L;

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				HorizontalLayout actionWrap = new HorizontalLayout();
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
				
		        
				 Button btnEdit = new Button();
				 btnEdit.setDescription("Sửa thông tin chung");
				 btnEdit.setId(BTN_EDIT_ID);
				 btnEdit.setIcon(FontAwesome.PENCIL);
				 btnEdit.addStyleName(ExplorerLayout.BUTTON_ACTION);
				 btnEdit.addClickListener(currentView);
				 btnEdit.setData(itemId);
				 btnEdit.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnEdit);
		        
		        Button btnEditModel = new Button();
		        btnEditModel.setDescription("Sửa");
		        btnEditModel.setId(BTN_EDIT_MODEL_ID);
		        btnEditModel.setIcon(FontAwesome.PENCIL_SQUARE_O);
		        btnEditModel.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnEditModel.addClickListener(currentView);
		        btnEditModel.setData(itemId);
		        btnEditModel.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnEditModel);
		        
		        Button btnDel = new Button();
		        btnDel.setDescription("Xóa");
		    	btnDel.setId(BTN_DEL_ID);
		    	btnDel.addStyleName(ExplorerLayout.BUTTON_ACTION);
		    	btnDel.addStyleName(Reindeer.BUTTON_LINK);
		    	btnDel.setData(itemId);
		    	btnDel.setIcon(FontAwesome.REMOVE);
		    	btnDel.addClickListener(currentView);
		        actionWrap.addComponent(btnDel);
		        
		        Button btnDeploy = new Button();
		        btnDeploy.setDescription("Deploy");
		        btnDeploy.setId(BTN_DEPLOY_ID);
		        btnDeploy.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnDeploy.addStyleName(Reindeer.BUTTON_LINK);
		        btnDeploy.setData(itemId);
		        btnDeploy.setIcon(FontAwesome.CHECK);
		        btnDeploy.addClickListener(currentView);
		        actionWrap.addComponent(btnDeploy);

		       
		        return actionWrap;
		    }
		});

		table.setColumnHeader("action", "Thao tác");
		table.setRowHeaderMode(RowHeaderMode.INDEX);

		table.setVisibleColumns("name", "createTime", "action");
		

		table.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				return "left-aligned";
			}
			
		});
		
		dataWrap.addComponent(table);
		dataWrap.addComponent(paginationBar);
	}
	

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_SEARCH_ID.equals(event.getButton().getId())){
			try {
				if(searchFieldGroup.isValid()){
					searchFieldGroup.commit();
					String name = (String) searchItem.getItemProperty("name").getValue();
					name = name != null ? name.trim() : "";
					paginationBar.search(name, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
			modelList = repositoryService.createModelQuery().list();
			paginationBar.search(null, null);
		}else if(BTN_VIEW_ID.equals(event.getButton().getId())){
			Model currentRow = (Model) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(ModelerDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
		}else if(BTN_EDIT_ID.equals(event.getButton().getId())){
			Model currentRow = (Model) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(ModelerDetailView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getId());
		}else if(BTN_EDIT_MODEL_ID.equals(event.getButton().getId())){
			Model current = (Model) button.getData();
			String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
			UI.getCurrent().getPage().open(contextPath+"/modeler.html?modelId="+current.getId(), "_blank"); //setLocation(contextPath+"/modeler.html?modelId="+current.getId());
		}else if(BTN_DEL_ID.equals(event.getButton().getId())){
			Model currentRow = (Model) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(ModelerDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getId());
		}else if(BTN_DEPLOY_ID.equals(event.getButton().getId())){
			Model currentRow = (Model) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(ModelerDetailView.VIEW_NAME+"/"+Constants.ACTION_DEPLOY+"/"+currentRow.getId());
		}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
			UI.getCurrent().getNavigator().navigateTo(ModelerDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD);
		}
	}

}

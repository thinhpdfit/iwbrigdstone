package com.eform.ui.view.report.type.v2;

import com.eform.model.entities.TruongThongTin;

public class VisibleColumn {
	private TruongThongTin truongThongTin;
	private String tenHienThi;

	public VisibleColumn() {
		super();
	}

	public String getTenHienThi() {
		return tenHienThi;
	}

	public void setTenHienThi(String tenHienThi) {
		this.tenHienThi = tenHienThi;
	}

	public TruongThongTin getTruongThongTin() {
		return truongThongTin;
	}

	public void setTruongThongTin(TruongThongTin truongThongTin) {
		this.truongThongTin = truongThongTin;
	}

}

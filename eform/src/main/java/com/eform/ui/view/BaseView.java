package com.eform.ui.view;

import com.eform.model.entities.HtQuyen;

public interface BaseView {
	public String getViewName();
	public String getPrevViewName();
	public void setPrevViewName(String viewName);
}

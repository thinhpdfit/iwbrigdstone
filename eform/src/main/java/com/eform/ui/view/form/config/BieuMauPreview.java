package com.eform.ui.view.form.config;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.HoSo;
import com.eform.service.BieuMauService;
import com.eform.service.HoSoService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.form.FormGenerate;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SpringView(name = BieuMauPreview.VIEW_NAME)
public class BieuMauPreview extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "xem-truoc-bieu-mau";
	private static final String MAIN_TITLE = "Xem trước biểu mẫu";
	private static final String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	private static final String BUTTON_VIEW_HTML_TBL_ID = "btn-view-html-table";
	private BieuMau bieuMauCurrent;
	private WorkflowManagement<BieuMau> bieuMauWM;
	Workflow<BieuMau> bieuMauWf;
	private CssLayout formContainer;
	private FormGenerate formGenerate;
	private Button btnBack;
	private CssLayout buttonsContainer;
	
	private Window htmlTableWindow;
	private Label contentHtml;
	
	@Autowired
	private BieuMauService bieuMauService;
	@Autowired
	private MessageSource messageSource;
	@PostConstruct
    void init() {
    	addStyleName(ExplorerLayout.E_FORM_GENERATE_WRAP);
    	bieuMauWM = new WorkflowManagement<BieuMau>(EChucNang.E_FORM_DANH_MUC.getMa());
    	initMainTitle();
    	initHtmlViewer();
    }

    @Override
    public void enter(ViewChangeEvent event) {
    	UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				String ma = m.group(3);
				List<BieuMau> list = bieuMauService.findByCode(ma);
				if(list != null && !list.isEmpty()){
					bieuMauCurrent = list.get(0);
					bieuMauWf = bieuMauWM.getWorkflow(bieuMauCurrent);
					initContent(bieuMauCurrent.getMa());
					initButtons();
				}
			}
		}
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo bcItem = new PageBreadcrumbInfo();
		bcItem.setTitle(BieuMauView.MAIN_TITLE);
		bcItem.setViewName(BieuMauView.VIEW_NAME);	
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home,bcItem, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
    
    public void initHtmlViewer(){
    	HorizontalLayout content = new HorizontalLayout();
    	content.setWidth("100%");
    	content.setSpacing(true);
    	content.setMargin(true);
    	
    	contentHtml = new Label("");
    	contentHtml.setContentMode(ContentMode.HTML);
    	contentHtml.setWidth("100%");
    	contentHtml.setStyleName(ExplorerLayout.HTML_DATA_LBL);
    	content.addComponent(contentHtml);
    	
    	htmlTableWindow = new Window("Html");
    	htmlTableWindow.setWidth(900, Unit.PIXELS);
    	htmlTableWindow.setHeight(500, Unit.PIXELS);
    	htmlTableWindow.setResizable(false);
    	htmlTableWindow.center();
    	htmlTableWindow.setModal(true);
    	htmlTableWindow.setContent(content);
    }
    
    public void initContent(String maBm){
    	formContainer = new CssLayout();
    	addComponent(formContainer);
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.addStyleName(ExplorerLayout.DETAIL_WRAP);
    	formContainer.addStyleName(ExplorerLayout.DETAIL_FORM_GEN_WRAP);
    	
    	
    	formGenerate = new FormGenerate(maBm);
    	formGenerate.setWidth("100%");
    	formContainer.addComponent(formGenerate);
    	
    	
    }
    
    public void initButtons(){
    	buttonsContainer = new CssLayout();
    	buttonsContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		buttonsContainer.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);
		
		Button btnViewHtmlTable = new Button("Hiển thị dạng html");
		btnViewHtmlTable.addClickListener(this);
		btnViewHtmlTable.setId(BUTTON_VIEW_HTML_TBL_ID);
		buttonsContainer.addComponent(btnViewHtmlTable);
		
		btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttonsContainer.addComponent(btnBack);
		addComponent(buttonsContainer);
    }
    
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			
			if(BUTTON_CANCEL_ID.equals(button.getId())){
				UI.getCurrent().getNavigator().navigateTo(BieuMauView.VIEW_NAME);
			}else if(BUTTON_VIEW_HTML_TBL_ID.equals(button.getId())){
				if(formGenerate.validateForm()){
					String html = formGenerate.getHtmlData();
					contentHtml.setValue(html);
					UI.getCurrent().addWindow(htmlTableWindow);
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    
	}
	
}
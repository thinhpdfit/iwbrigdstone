package com.eform.ui.view.system;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.ThamSo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.SolrUtils;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.HtThamSo;
import com.eform.service.BieuMauService;
import com.eform.service.HtThamSoService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = HtThamSoDetailView.VIEW_NAME)
public class HtThamSoDetailView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "tham-so-he-thong";
	private static final String MAIN_TITLE = "Tham số hệ thống";
	private static final String SMALL_TITLE = "";
	
	private final int WIDTH_FULL = 100;
	private static final String BUTTON_EDIT_ID = "btn-edit";
	private static final String BUTTON_REMOVE_ALL = "btn-remove-all";
	private static final String BUTTON_UPDATE_SOLR = "btn-update-solr";
	
	private CssLayout formContainer;
	private Button btnUpdate;
	
	private List<HtThamSo> htThamSoList;
	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();

	@Autowired
	private HtThamSoService htThamSoService;

	@Autowired
	private RepositoryService repositoryService;	
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private BieuMauService bieuMauService;
	private String solrUrl;

    @PostConstruct
    void init() {
    	fieldGroup.setItemDataSource(itemValue);
    	initMainTitle();
    	List<Long> loai = new ArrayList<Long>();
    	loai.add(1L);
    	htThamSoList = htThamSoService.getHtThamSoFind(null, null, loai, -1, -1);
    	HtThamSo solrUrlThamSo = htThamSoService.getHtThamSoFind(ThamSo.SOLRSEARCH_URL);
		if(solrUrlThamSo != null && solrUrlThamSo.getGiaTri()!= null){
			solrUrl = solrUrlThamSo.getGiaTri();
		}
    }

    @Override
    public void enter(ViewChangeEvent event) {
    	UI.getCurrent().getPage().setTitle(MAIN_TITLE);
    	initDetailForm();
    }
    
    public void initMainTitle(){
    	PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
    	addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
    
    public void initDetailForm(){
    	
    	formContainer = new CssLayout();
    	formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	FormLayout form = new FormLayout();
    	form.setMargin(true);
    	
    	if(htThamSoList != null && !htThamSoList.isEmpty()){
    		boolean first = true;
    		for(HtThamSo htThamSo : htThamSoList){
    			if(htThamSo != null){
    				if(htThamSo.getLoaiHienThi() != null){
    					if(CommonUtils.ELoaiHienThiHtThamSo.TEXT_FIELD.getCode() == htThamSo.getLoaiHienThi().intValue()){
    						TextField txt = new TextField(htThamSo.getTen());
    						if(first){
    							txt.focus();
    							first = false;
    						}
    	    				txt.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	    				txt.setNullRepresentation("");
    	    				txt.setNullSettingAllowed(true);
    	    				txt.setInputPrompt(htThamSo.getMoTa() != null ? htThamSo.getMoTa() : "");
    	    				txt.setDescription(htThamSo.getMoTa() != null ? htThamSo.getMoTa() : "");
    	    				txt.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", null, 2000, true));
    	    				if(htThamSo.getBieuThucKiemTra() != null && !htThamSo.getBieuThucKiemTra().trim().isEmpty()){
    	    					String msg = "Dữ liệu không đúng";
    	    					if(htThamSo.getMoTa() != null && !htThamSo.getMoTa().trim().isEmpty()){
    	    						msg = htThamSo.getMoTa();
    	    					}
    	    					txt.addValidator(new RegexpValidator(htThamSo.getBieuThucKiemTra(), msg));
    	    				}
    	    				itemValue.addItemProperty(htThamSo.getMa(), new ObjectProperty<String>(htThamSo.getGiaTri(), String.class));
    	    		    	fieldGroup.bind(txt, htThamSo.getMa());
    	    		    	form.addComponent(txt);
        				}else if(CommonUtils.ELoaiHienThiHtThamSo.TEXT_AREA.getCode() == htThamSo.getLoaiHienThi().intValue()){
        					TextArea textArea = new TextArea(htThamSo.getTen());
        					if(first){
        						textArea.focus();
    							first = false;
    						}
        					textArea.setWidth("100%");
        					textArea.setNullRepresentation("");
        					textArea.setNullSettingAllowed(true);
        					textArea.addStyleName(ExplorerLayout.FORM_CONTROL);
        					textArea.setInputPrompt(htThamSo.getMoTa() != null ? htThamSo.getMoTa() : "");
        					textArea.setDescription(htThamSo.getMoTa() != null ? htThamSo.getMoTa() : "");
        					textArea.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", 0, 2000, true));
        					
        					if(htThamSo.getBieuThucKiemTra() != null && !htThamSo.getBieuThucKiemTra().trim().isEmpty()){
    	    					String msg = "Dữ liệu không đúng";
    	    					if(htThamSo.getMoTa() != null && !htThamSo.getMoTa().trim().isEmpty()){
    	    						msg = htThamSo.getMoTa();
    	    					}
    	    					textArea.addValidator(new RegexpValidator(htThamSo.getBieuThucKiemTra(), msg));
    	    				}
        					
        					itemValue.addItemProperty(htThamSo.getMa(), new ObjectProperty<String>(htThamSo.getGiaTri(), String.class));
    	    		    	fieldGroup.bind(textArea, htThamSo.getMa());
    	    		    	form.addComponent(textArea);
        				}else if(CommonUtils.ELoaiHienThiHtThamSo.COMBOBOX.getCode() == htThamSo.getLoaiHienThi().intValue()){
        					ComboBox comboBox = new ComboBox(htThamSo.getTen());
        					if(first){
        						comboBox.focus();
    							first = false;
    						}
        					comboBox.setWidth("100%");
        					comboBox.setNullSelectionAllowed(true);
        					comboBox.addStyleName(ExplorerLayout.FORM_CONTROL);
        					comboBox.setDescription(htThamSo.getMoTa() != null ? htThamSo.getMoTa() : "");
        					if(htThamSo.getDuLieu() != null){
        						JSONObject json = htThamSo.getDuLieu();
        						JSONArray array = json.getJSONArray("data");
        						if(array != null){
        							for(int i = 0; i< array.length(); i++){
        								JSONObject item = array.getJSONObject(i);
        								comboBox.addItem(item.get("value"));
        								comboBox.setItemCaption(item.get("value"), item.getString("name"));
        							}
        						}
        					}
        					
        					itemValue.addItemProperty(htThamSo.getMa(), new ObjectProperty<String>(htThamSo.getGiaTri(), String.class));
        					fieldGroup.bind(comboBox, htThamSo.getMa());
        					form.addComponent(comboBox);
        				}else if(CommonUtils.ELoaiHienThiHtThamSo.CHECKBOX.getCode() == htThamSo.getLoaiHienThi().intValue()){
        					CheckBox checkBox = new CheckBox(htThamSo.getTen());
        					if(first){
        						checkBox.focus();
    							first = false;
    						}
        					checkBox.addStyleName(ExplorerLayout.FORM_CONTROL);
        					checkBox.setDescription(htThamSo.getMoTa() != null ? htThamSo.getMoTa() : "");
        					Boolean value = false;
        					if(htThamSo.getGiaTri() != null && htThamSo.getGiaTri().trim().equals("1")){
        						value = true;
        					}
        					
        					itemValue.addItemProperty(htThamSo.getMa(), new ObjectProperty<Boolean>(value, Boolean.class));
    	    		    	fieldGroup.bind(checkBox, htThamSo.getMa());
    	    		    	form.addComponent(checkBox);
        				}
    				}else{
	    				TextField txt = new TextField(htThamSo.getTen());
	    				if(first){
	    					txt.focus();
							first = false;
						}
	    				txt.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
	    				txt.setNullRepresentation("");
	    				txt.setNullSettingAllowed(true);
	    				txt.setInputPrompt(htThamSo.getMoTa() != null ? htThamSo.getMoTa() : "");
	    				txt.addValidator(new StringLengthValidator("Số ký tự tối đa là 2000", null, 2000, true)); 
	    				txt.setDescription(htThamSo.getMoTa() != null ? htThamSo.getMoTa() : "");
	    				if(htThamSo.getBieuThucKiemTra() != null && !htThamSo.getBieuThucKiemTra().trim().isEmpty()){
	    					String msg = "Dữ liệu không đúng";
	    					if(htThamSo.getMoTa() != null && !htThamSo.getMoTa().trim().isEmpty()){
	    						msg = htThamSo.getMoTa();
	    					}
	    					txt.addValidator(new RegexpValidator(htThamSo.getBieuThucKiemTra(), msg));
	    				}
	    				
	    				itemValue.addItemProperty(htThamSo.getMa(), new ObjectProperty<String>(htThamSo.getGiaTri(), String.class));
	    		    	fieldGroup.bind(txt, htThamSo.getMa());
	    		    	form.addComponent(txt);
    				}
    			}
    		}
    	}
    	
    	
    	HorizontalLayout buttonAction = new HorizontalLayout();
		buttonAction.setSpacing(true);
		buttonAction.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
		
		HtThamSo thamSo = htThamSoService.getHtThamSoFind("REMOVE_ALL_DATA");
		if(thamSo != null && "@123".equals(thamSo.getGiaTri())){
			Button btnRemoveAll = new Button("Xóa dữ liệu");
			btnRemoveAll.addClickListener(this);
			btnRemoveAll.setCaption("Xóa dữ liệu");
			btnRemoveAll.addStyleName(ExplorerLayout.BUTTON_WARNING);
			btnRemoveAll.setId(BUTTON_REMOVE_ALL);
			buttonAction.addComponent(btnRemoveAll);
		}
		
		HtThamSo thamSoUpdateSolr = htThamSoService.getHtThamSoFind(ThamSo.UPDATE_SOLR);
		if(thamSoUpdateSolr != null && "@321".equals(thamSoUpdateSolr.getGiaTri())){
			Button btnUpdateSolr = new Button("Update solr");
			btnUpdateSolr.addClickListener(this);
			btnUpdateSolr.setCaption("Update solr");
			btnUpdateSolr.addStyleName(ExplorerLayout.BUTTON_WARNING);
			btnUpdateSolr.setId(BUTTON_UPDATE_SOLR);
			buttonAction.addComponent(btnUpdateSolr);
		}
		form.addComponent(buttonAction);
    	
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	btnUpdate = new Button();
		btnUpdate.addClickListener(this);
		btnUpdate.setCaption("Cập nhật");
		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnUpdate.setId(BUTTON_EDIT_ID);
		buttons.addComponent(btnUpdate);
    	form.addComponent(buttons);

    	formContainer.addComponent(form);
    	addComponent(formContainer);
    }

@SuppressWarnings("unchecked")
@Override
public void buttonClick(ClickEvent event) {
	try {
		Button button = event.getButton();
		if(BUTTON_EDIT_ID.equals(button.getId())){
			if(fieldGroup.isValid() && fieldGroup.isModified()){
				fieldGroup.commit();
				if(htThamSoList != null && !htThamSoList.isEmpty()){
					htThamSoList.forEach(htThamSo->{
						if(htThamSo.getLoaiHienThi() != null && htThamSo.getLoaiHienThi().intValue() == CommonUtils.ELoaiHienThiHtThamSo.CHECKBOX.getCode()){
							Boolean b = (Boolean) itemValue.getItemProperty(htThamSo.getMa()).getValue();
							if(b != null && b){
								htThamSo.setGiaTri("1");
							}else{
								htThamSo.setGiaTri("0");
							}
						}else{
							String v = (String) itemValue.getItemProperty(htThamSo.getMa()).getValue();
							htThamSo.setGiaTri(v);
						}
					});
					htThamSoService.updateList(htThamSoList);
				}
				Notification notf = new Notification("Thông báo", "Cập nhật thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(UI.getCurrent().getPage());
			}
		}else if(BUTTON_REMOVE_ALL.equals(button.getId())){
//			Xóa dữ liệu
			List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery().list();
			List<ProcessInstance> list1 = runtimeService.createProcessInstanceQuery().list();
			for(ProcessInstance p: list1){
				try {
					runtimeService.deleteProcessInstance(p.getId(), null);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			
			for(HistoricProcessInstance p: list){
				try {
					historyService.deleteHistoricProcessInstance(p.getId());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			
			
			
			List<ProcessDefinition> list2 = repositoryService.createProcessDefinitionQuery().list();
			for(ProcessDefinition p : list2){
				try {
					repositoryService.deleteDeployment(p.getDeploymentId());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			
			Notification notf = new Notification("Thông báo", "Xóa thành công");
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(UI.getCurrent().getPage());
		}else if(BUTTON_UPDATE_SOLR.equals(button.getId())){
			List<BieuMau> bieuMauList = bieuMauService.getBieuMauFind(null, null, null, null, -1, -1);
			if(bieuMauList != null){
				for(BieuMau current : bieuMauList){
					try {
						SolrUtils.updateSolr(CommonUtils.ESolrSearchType.BIEU_MAU.getCode()+"_"+current.getId(), current.getTen() +" "+current.getMa(), solrUrl);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			List<ProcessDefinition> proList = repositoryService.createProcessDefinitionQuery().list();
			if(proList != null){
				for(ProcessDefinition proDef: proList){
					try {
						SolrUtils.updateSolr(CommonUtils.ESolrSearchType.PROCESS.getCode()+"_"+proDef.getId(), proDef.getName() +" "+proDef.getDescription(), solrUrl);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			Notification notf = new Notification("Thông báo", "Cập nhật thành công");
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(UI.getCurrent().getPage());
		}
	} catch (Exception e) {
		e.printStackTrace();
		Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra!", Type.ERROR_MESSAGE);
		notf.setDelayMsec(3000);
		notf.setPosition(Position.TOP_CENTER);
		notf.show(UI.getCurrent().getPage());
	}
    
}
}
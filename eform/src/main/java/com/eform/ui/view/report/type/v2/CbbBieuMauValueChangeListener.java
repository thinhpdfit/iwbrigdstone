package com.eform.ui.view.report.type.v2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.eform.common.SpringApplicationContext;
import com.eform.common.type.ComboboxItem;
import com.eform.common.utils.ChartUtils;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BieuMauService;
import com.eform.ui.view.report.type.FilterObject;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;

public class CbbBieuMauValueChangeListener implements ValueChangeListener{
	private List<ComboboxItem> truongThongTinCbbList;
	private BeanItemContainer<ComboboxItem> truongThongTinContainer;
	private BeanItemContainer<FilterObject> filterContainer;
	private BeanItemContainer<VisibleColumn> visibleColumnContainer;
	private CssLayout wrapper;
	private Map<String, String> bieuMauSelectedMap;
	private String maBieuMauParent;
	private List<String> maBieuMauSelectedList;
	
	@Override
	public void valueChange(ValueChangeEvent event) {
		wrapper.removeAllComponents();
		BieuMau selected = (BieuMau) event.getProperty().getValue();
		BieuMauService bieuMauService = SpringApplicationContext.getApplicationContext().getBean(BieuMauService.class);
		truongThongTinCbbList.clear();
		if(selected != null){
			
			
			
			List<BieuMau> bmConList = ChartUtils.getBieuMauCon(selected, bieuMauService);
			resetChild(selected.getMa());
			bieuMauSelectedMap.put(selected.getMa(), maBieuMauParent);
			List<String> maBieuMauList = new ArrayList();
			for(Map.Entry<String, String> entry : bieuMauSelectedMap.entrySet()){
				if(!maBieuMauList.contains(entry.getKey())){
					maBieuMauList.add(entry.getKey());
				}
			}
			List<TruongThongTin> truongThongTinList = ChartUtils.getTruongThongTinByBieuMau(maBieuMauList, bieuMauService);
			if(truongThongTinList != null && !truongThongTinList.isEmpty()){
				for(TruongThongTin truongThongTin : truongThongTinList){
					ComboboxItem item = new ComboboxItem(truongThongTin.getTen(), truongThongTin);
					truongThongTinCbbList.add(item);
				}
			}
			if(bmConList != null && !bmConList.isEmpty()){
				BeanItemContainer<BieuMau> containerBm = new BeanItemContainer<BieuMau>(BieuMau.class, bmConList);
				ComboBox cbbBieuMau = new ComboBox();
				cbbBieuMau.setContainerDataSource(containerBm);
		    	cbbBieuMau.setWidth(100, Unit.PERCENTAGE);
		    	cbbBieuMau.setItemCaptionMode(ItemCaptionMode.PROPERTY);
		    	cbbBieuMau.setItemCaptionPropertyId("ten");
		    	for(BieuMau bm : bmConList){
		    		if(maBieuMauSelectedList.contains(bm.getMa())){
		    			cbbBieuMau.setValue(bm);
			    	}
		    	}

		    	CssLayout cbbBieuMauWrap = new CssLayout();
		    	cbbBieuMauWrap.setStyleName("cbb-inner");
		    	cbbBieuMauWrap.setWidth("100%");
		    	cbbBieuMau.setData(cbbBieuMauWrap);
		    	wrapper.addComponent(cbbBieuMau);
		    	wrapper.addComponent(cbbBieuMauWrap);
		    	
		    	List<ComboboxItem> truongThongTinCbbList = new ArrayList<ComboboxItem>();
				CbbBieuMauValueChangeListener cbbValueChangeListener = new CbbBieuMauValueChangeListener();
				cbbValueChangeListener.setFilterContainer(filterContainer);
				cbbValueChangeListener.setTruongThongTinCbbList(truongThongTinCbbList);
				cbbValueChangeListener.setTruongThongTinContainer(truongThongTinContainer);
				cbbValueChangeListener.setWrapper(cbbBieuMauWrap);
				cbbValueChangeListener.setVisibleColumnContainer(visibleColumnContainer);
				cbbValueChangeListener.setBieuMauSelectedMap(bieuMauSelectedMap);
				cbbValueChangeListener.setMaBieuMauParent(selected.getMa());
				cbbBieuMau.addValueChangeListener(cbbValueChangeListener);
			}
		}
		truongThongTinContainer.removeAllItems();
		truongThongTinContainer.addAll(truongThongTinCbbList);
		filterContainer.removeAllItems();
		visibleColumnContainer.removeAllItems();
	}
	
	public void resetChild(String code){
		List<String> list = new ArrayList();
		list.add(code);
		List<String> removeList = new ArrayList();
		while(!list.isEmpty()){
			String current = list.remove(0);
			for(Map.Entry<String, String> entry : bieuMauSelectedMap.entrySet()){
				String k = entry.getKey();
				String v = entry.getValue();
				if(current.equals(v)){
					removeList.add(k);
					list.add(k);
				}
			}
		}
		bieuMauSelectedMap.keySet().removeAll(removeList);
	}

	public List<ComboboxItem> getTruongThongTinCbbList() {
		return truongThongTinCbbList;
	}

	public void setTruongThongTinCbbList(List<ComboboxItem> truongThongTinCbbList) {
		this.truongThongTinCbbList = truongThongTinCbbList;
	}

	public BeanItemContainer<ComboboxItem> getTruongThongTinContainer() {
		return truongThongTinContainer;
	}

	public void setTruongThongTinContainer(BeanItemContainer<ComboboxItem> truongThongTinContainer) {
		this.truongThongTinContainer = truongThongTinContainer;
	}

	public BeanItemContainer<FilterObject> getFilterContainer() {
		return filterContainer;
	}

	public void setFilterContainer(BeanItemContainer<FilterObject> filterContainer) {
		this.filterContainer = filterContainer;
	}

	public BeanItemContainer<VisibleColumn> getVisibleColumnContainer() {
		return visibleColumnContainer;
	}

	public void setVisibleColumnContainer(BeanItemContainer<VisibleColumn> visibleColumnContainer) {
		this.visibleColumnContainer = visibleColumnContainer;
	}

	public CssLayout getWrapper() {
		return wrapper;
	}

	public void setWrapper(CssLayout wrapper) {
		this.wrapper = wrapper;
	}

	public Map<String, String> getBieuMauSelectedMap() {
		return bieuMauSelectedMap;
	}

	public void setBieuMauSelectedMap(Map<String, String> bieuMauSelectedMap) {
		this.bieuMauSelectedMap = bieuMauSelectedMap;
	}

	public String getMaBieuMauParent() {
		return maBieuMauParent;
	}

	public void setMaBieuMauParent(String maBieuMauParent) {
		this.maBieuMauParent = maBieuMauParent;
	}

	public List<String> getMaBieuMauSelectedList() {
		return maBieuMauSelectedList;
	}

	public void setMaBieuMauSelectedList(List<String> maBieuMauSelectedList) {
		this.maBieuMauSelectedList = maBieuMauSelectedList;
	}

	

}

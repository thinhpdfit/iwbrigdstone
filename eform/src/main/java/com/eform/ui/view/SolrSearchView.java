package com.eform.ui.view;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.ThamSo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.SolrUtils;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.HoSo;
import com.eform.model.entities.HtThamSo;
import com.eform.service.BieuMauService;
import com.eform.service.HoSoService;
import com.eform.service.HtThamSoService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.eform.ui.view.form.config.BieuMauDetailView;
import com.eform.ui.view.process.MyProcessInstanceDetailView;
import com.eform.ui.view.process.ProcessDefinitionDetailView;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = SolrSearchView.VIEW_NAME)
public class SolrSearchView extends VerticalLayout implements View, ClickListener{

private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;
	
	public static final String VIEW_NAME = "sorl-search";
	static String MAIN_TITLE = "Tìm kiếm";
	
	private List<Object> results;
	private String query;
	private CssLayout dataWrap;
	private TextField txtSearch;
	

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private HoSoService hoSoService;
	@Autowired
	private BieuMauService bieuMauService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private HtThamSoService htThamSoService;
	
	private String solrUrl;
	
	@PostConstruct
	public void init(){
		initMainTitle();
		initSearchBox();
		dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
		HtThamSo solrUrlThamSo = htThamSoService.getHtThamSoFind(ThamSo.SOLRSEARCH_URL);
		if(solrUrlThamSo != null && solrUrlThamSo.getGiaTri()!= null){
			solrUrl = solrUrlThamSo.getGiaTri();
		}
	}

	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			query = params.toString();
			try {
				results = SolrUtils.search(query, solrUrl);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
//			//TODO test
//			results = new ArrayList();
//			results.add("BM_198");
//			results.add("BM_199");
			
			txtSearch.setValue(query);
			txtSearch.focus();
			initDataContent();
		}
	}
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		
	}
	
	public void initSearchBox(){
		CssLayout searchBox = new CssLayout();
		searchBox.setWidth("100%");
		searchBox.addStyleName(ExplorerLayout.SEARCH_BOX_WRAP);
		addComponent(searchBox);
		
		txtSearch = new TextField();
		txtSearch.setWidth("100%");
		txtSearch.addStyleName(ExplorerLayout.SEARCH_BOX);
		txtSearch.setInputPrompt("Tìm kiếm...");
		searchBox.addComponent(txtSearch);
		
		txtSearch.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object valueObj = event.getProperty().getValue();
				if(valueObj != null && valueObj instanceof String){
					String value = (String) valueObj;
					if(value != null && !value.trim().isEmpty()){
						UI.getCurrent().getNavigator().navigateTo(SolrSearchView.VIEW_NAME+"/"+value);
					}
				}
			}
		});
	}
	
	public void initDataContent(){
		dataWrap.removeAllComponents();
		
		int d = 0;
		CssLayout searchResults = null;
		if(results != null && !results.isEmpty()){
			
			
			searchResults = new CssLayout();
			searchResults.addStyleName(ExplorerLayout.SEARCH_RESULT_WRAP);
			searchResults.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
			for(Object obj : results){
				if(obj != null && obj instanceof String){
					String idStr = (String) obj;
					Pattern p = Pattern.compile("([^_]*)_(.*)");
					Matcher m = p.matcher(idStr);
					if(m.matches()){
						String type = m.group(1);
						String id = m.group(2);
						if(CommonUtils.ESolrSearchType.HO_SO.getCode().equals(type)){
							if(id != null && id.matches("[0-9]+")){
								HoSo hoSo = hoSoService.findOne(Long.parseLong(id));
								if(hoSo != null && hoSo.getProcessInstanceId() != null){
									HistoricProcessInstance proIns = historyService.createHistoricProcessInstanceQuery().processInstanceId(hoSo.getProcessInstanceId().trim()).singleResult();
									if(proIns != null){
										
										
										if(proIns.getProcessDefinitionId() != null){
											ProcessDefinition pro = repositoryService.createProcessDefinitionQuery().processDefinitionId(proIns.getProcessDefinitionId()).singleResult();
											if(pro != null){
												String name = "#Không tên";
												if(pro.getName() != null && !pro.getName().isEmpty()){
													name = pro.getName();
												}
												CssLayout resultItem = initResultItem(type, name, proIns.getId(), proIns);
												searchResults.addComponent(resultItem);
												d++;
											}
										}
										
									}
									
								}
							}
						}else if(CommonUtils.ESolrSearchType.BIEU_MAU.getCode().equals(type)){
							if(id != null && id.matches("[0-9]+")){
								BieuMau bm = bieuMauService.findOne(Long.parseLong(id));
								if(bm != null){
									CssLayout resultItem = initResultItem(type, bm.getTen(), bm.getMa(), bm);
									searchResults.addComponent(resultItem);
									d++;
								}
							}
						}else if(CommonUtils.ESolrSearchType.PROCESS.getCode().equals(type)){
							if(id != null){
								ProcessDefinition pro = repositoryService.createProcessDefinitionQuery().processDefinitionId(id).singleResult();
								if(pro != null){
									CssLayout resultItem = initResultItem(type, pro.getName(), null, pro);
									searchResults.addComponent(resultItem);
									d++;
								}
							}
						}
						
					}
				}
			}
		}
		
		Label total = new Label();
		total.addStyleName(ExplorerLayout.SEARCH_TOTAL_RESULT_LBL);
		total.setContentMode(ContentMode.HTML);
		dataWrap.addComponent(total);
		
		if(d > 0 && searchResults != null){
			total.setValue("Tìm thấy "+d+" kết quả phù hợp với <span class='search-q'>"+query+"</span>");
			dataWrap.addComponent(searchResults);
		}else{
			total.setValue("Không tìm thấy kết quả nào phù hợp với <span class='search-q'>"+query+"</span>");
		}
	}
	
	public CssLayout initResultItem(String type, String ten, String ma, Object data){
		CssLayout item = new CssLayout();
		item.setWidth("100%");
		item.addStyleName(ExplorerLayout.SEARCH_ITEM);
		
		CommonUtils.ESolrSearchType solrType = CommonUtils.ESolrSearchType.getSolrSearchType(type);
		Label lbl = new Label();
		
		lbl.setStyleName(ExplorerLayout.SEARCH_ITEM_ICON);
		item.addComponent(lbl);
		
		if(solrType != null){
			item.addStyleName(ExplorerLayout.SEARCH_ITEM+"-"+solrType.getCode().toLowerCase());
			lbl.setDescription(solrType.getName());
		}
		
		String nameStr = "";
		if(ma != null){
			nameStr = ten +" ("+ma+")";
		}else{
			nameStr = ten;
		}
		
		Button name = new Button(nameStr);
		name.addClickListener(this);
		name.setId(type);
		name.setData(data);
		name.addStyleName(ExplorerLayout.SEARCH_ITEM_TITLE);
		name.addStyleName(Reindeer.BUTTON_LINK);
		
		item.addComponent(name);
		
		return item;
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(button != null){
			if(CommonUtils.ESolrSearchType.BIEU_MAU.getCode().equals(button.getId())){
				BieuMau data = (BieuMau) button.getData();
				UI.getCurrent().getNavigator().navigateTo(BieuMauDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+data.getMa().toLowerCase());
			}else if(CommonUtils.ESolrSearchType.HO_SO.getCode().equals(button.getId())){
				HistoricProcessInstance pro = (HistoricProcessInstance) button.getData();
				UI.getCurrent().getNavigator().navigateTo(MyProcessInstanceDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+pro.getId());
			}else if(CommonUtils.ESolrSearchType.PROCESS.getCode().equals(button.getId())){
				ProcessDefinition pro = (ProcessDefinition) button.getData();
				UI.getCurrent().getNavigator().navigateTo(ProcessDefinitionDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+pro.getId());
			}
		}
	}


}

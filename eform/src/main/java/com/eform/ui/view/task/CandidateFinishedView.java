package com.eform.ui.view.task;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.persistence.entity.HistoricTaskInstanceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = CandidateFinishedView.VIEW_NAME)
public class CandidateFinishedView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;

	public static final String VIEW_NAME = "candidate-finished";
	public static final String MAIN_TITLE = "Công việc liên quan đã hoàn thành";
	private static final String SMALL_TITLE = "Candidate Finished";
	public static final String BTN_DETAIL_ID = "detail";
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private MessageSource messageSource;
	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();
	private static final String BTN_SEARCH_ID = "timKiem";
	private HistoricTaskListContainer taskContainer;
	
	@PostConstruct
    void init() {
		fieldGroup.setItemDataSource(itemValue);
		initMainTitle();
		initContent();
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new HistoricTaskCategoryNavigatorBar(MAIN_TITLE, SMALL_TITLE, HistoricTaskCategoryNavigatorBar.BTN_CANDIDATE_FINISHED_ID));
	}
	
	public void initContent(){
		CssLayout searchForm = new CssLayout();
		searchForm.setWidth("100%");
		searchForm.addStyleName("search-task-form");
		
		TextField txtSearch = new TextField();
		txtSearch.setNullRepresentation("");
		txtSearch.setNullSettingAllowed(true);
		searchForm.addComponent(txtSearch);
		itemValue.addItemProperty("searchQuery", new ObjectProperty<String>(null, String.class));
		fieldGroup.bind(txtSearch, "searchQuery");
		
		Button btnSearch = new Button(FontAwesome.SEARCH.getHtml());
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.setHtmlContentAllowed(true);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(this);
		searchForm.addComponent(btnSearch); 
		addComponent(searchForm);
		
		taskContainer = new HistoricTaskListContainer(historyService, repositoryService, TaskCategory.CANDIDATE_FINISHED_CATE, CommonUtils.getUserLogedIn(), this);
		addComponent(taskContainer);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_DETAIL_ID.equals(event.getButton().getId())){
			HistoricTaskInstanceEntity currentRow = (HistoricTaskInstanceEntity) button.getData();
			UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW_HISTORY+"/"+currentRow.getId());
		}else if(BTN_SEARCH_ID.equals(event.getButton().getId())){
			try {
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					Object value = itemValue.getItemProperty("searchQuery").getValue();
					String searchQuery =  null;
					if(value != null){
						searchQuery = value.toString().trim();
					}
					
					taskContainer.getPaginationBar().search(searchQuery, TaskCategory.CANDIDATE_FINISHED_CATE, CommonUtils.getUserLogedIn());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}

}

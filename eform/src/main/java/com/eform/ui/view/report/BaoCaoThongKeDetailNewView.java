package com.eform.ui.view.report;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.task.TaskDefinition;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.TemporaryFileDownloadResource;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.ChartUtils;
import com.eform.common.utils.CommonUtils;
import com.eform.common.workflow.Workflow;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.BaoCaoThongKeDonViDao;
import com.eform.dao.HtQuyenServices;
import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.BaoCaoThongKeDonVi;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.DeploymentHtDonVi;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BaoCaoThongKeService;
import com.eform.service.BieuMauService;
import com.eform.service.DeploymentHtDonViService;
import com.eform.service.HoSoService;
import com.eform.service.HtDonViServices;
import com.eform.service.TruongThongTinService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.eform.ui.view.report.type.FilterObject;
import com.eform.ui.view.report.type.v2.CbbBieuMauValueChangeListener;
import com.eform.ui.view.report.type.v2.VisibleColumn;
import com.eform.ui.view.report.utils.ReportUtils;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.ColumnHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SpringView(name = BaoCaoThongKeDetailNewView.VIEW_NAME)
public class BaoCaoThongKeDetailNewView extends VerticalLayout implements View, ClickListener {

	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "dynamic-report-detail";
	private static final String MAIN_TITLE = "Thông tin báo cáo thống kê";
	private static final String SMALL_TITLE = "";
	private static final String BUTTON_CREATE_REPORT_ID = "btn-create-report";
	private static final String BUTTON_REMOVE_GROUP_ID = "btn-remove-group";
	private static final String BUTTON_ADD_GROUP_ID = "btn-add-group";
	private static final String BUTTON_SHOW_GROUP_POPUP_ID = "btn-show-group-popup";
	private static final String BUTTON_REMOVE_FILTER_ID = "btn-remove-filter";
	private static final String BUTTON_SHOW_FILTER_POPUP_ID = "btn-show-filter-popup";
	private static final String BUTTON_ADD_FILTER_ID = "btn-add-filter";
	private static final String BUTTON_DOWNLOAD_EXCEL_ID = "btn-download-excel";
	private static final String BUTTON_SAVE_ID = "btn-save";
	private static final String BUTTON_UPDATE_ID = "btn-update";
	private static final String BUTTON_UPDATE_POPUP_ID = "btn-update-popup";
	private static final String BUTTON_DEL_ID = "btn-del";
	private static final String BUTTON_CANCEL_ID = "btn-cancel";
	
	private final int WIDTH_FULL = 100;
	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();
	private Set<String> deploymentIds;
	private HtDonVi htDonViCurrent;
	private BaoCaoThongKeDetailNewView currentView = this;
	private BeanItemContainer<VisibleColumn> visibleColumnContainer;
	private BeanItemContainer<ComboboxItem> truongThongTinContainer;
	private BeanItemContainer<FilterObject> filterContainer;
	
	private WorkflowManagement<BaoCaoThongKe> baoCaoThongKeWM;
	private Workflow<BaoCaoThongKe> baoCaoThongKeWf;
	private BaoCaoThongKe baoCaoThongKeCurrent;
	private TreeTable donViTreeTable;
	

	private final PropertysetItem visibleColumnItemValue = new PropertysetItem();
	private final FieldGroup visibleColumnFieldGroup = new FieldGroup();
	private final PropertysetItem filterItemValue = new PropertysetItem();
	private final FieldGroup filterFieldGroup = new FieldGroup();
	private final PropertysetItem saveItemValue = new PropertysetItem();
	private final FieldGroup saveFieldGroup = new FieldGroup();

	private Window visibleColumnWindow;
	private Window filterWindow;
	private Window tableResultWindow;
	private Window saveWindow;
	private HorizontalLayout tableContainer;
	private String action;
	private Map<HtDonVi,Boolean> donViSelected;
	private ComboBox cbbBieuMau;
	private Map<String, String> bieuMauSelectedMap;
	
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private DeploymentHtDonViService deploymentHtDonViService;
	@Autowired
	private HtDonViServices htDonViServices;
	@Autowired
	private HtQuyenServices htQuyenServices;
	@Autowired
	private BaoCaoThongKeDonViDao baoCaoThongKeDonViDao;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private BieuMauService bieuMauService;
	@Autowired
	private HoSoService hoSoService;
	@Autowired
	private BaoCaoThongKeService baoCaoThongKeService;
	@Autowired
	private TruongThongTinService truongThongTinService;

	
	@PostConstruct
	void init() {
		initMainTitle();
		baoCaoThongKeWM = new WorkflowManagement<BaoCaoThongKe>(EChucNang.E_FORM_BAO_CAO_THONG_KE.getMa());
		fieldGroup.setItemDataSource(itemValue);
		filterFieldGroup.setItemDataSource(filterItemValue);
		visibleColumnFieldGroup.setItemDataSource(visibleColumnItemValue);
		saveFieldGroup.setItemDataSource(saveItemValue);
		String userId = CommonUtils.getUserLogedIn();
		String maDonVi = identityService.getUserInfo(userId, "HT_DON_VI");
    	donViSelected = new HashMap();
		deploymentIds = new HashSet<String>();
		bieuMauSelectedMap = new HashMap();
		if(maDonVi != null){
			List<HtDonVi> dvList = htDonViServices.getHtDonViFind(maDonVi,null, null, null, 0, 1);
			if(!dvList.isEmpty()){
				htDonViCurrent = dvList.get(0);
				List<DeploymentHtDonVi> deploymentHtDonViList = deploymentHtDonViService.getDeploymentHtDonViFind(null, dvList.get(0));
				if(deploymentHtDonViList != null && !deploymentHtDonViList.isEmpty()){
					for(DeploymentHtDonVi item : deploymentHtDonViList){
						deploymentIds.add(item.getDeploymentId());
					}
				}
			}
		}
	}

	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		Object params = event.getParameters();
		if(params != null){
			Pattern p = Pattern.compile("([a-zA-z0-9-_]+)(/([a-zA-z0-9-_]+))*");
			Matcher m = p.matcher(params.toString());
			if(m.matches()){
				action = m.group(1);
				if(Constants.ACTION_ADD.equals(action)){
					baoCaoThongKeCurrent = new BaoCaoThongKe();
					
				}else{
					String ma = m.group(3);
					List<BaoCaoThongKe> baoCaoThongKeList = baoCaoThongKeService.findByCode(ma);
					if(baoCaoThongKeList != null && !baoCaoThongKeList.isEmpty()){
						baoCaoThongKeCurrent = baoCaoThongKeList.get(0);
					}else{
						baoCaoThongKeCurrent = new BaoCaoThongKe();
					}
				}
				baoCaoThongKeWf = baoCaoThongKeWM.getWorkflow(baoCaoThongKeCurrent);

				initDetailForm();
				initVisibleCoumnWindow();
				initFilterWindow();
				initTableResultViewer();
				initSaveWindow();
			}
		}
	}
	

	public void initMainTitle(){
		PageBreadcrumbInfo home = new PageBreadcrumbInfo();
    	home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
    	home.setViewName(DashboardView.VIEW_NAME);
    	
    	PageBreadcrumbInfo current = new PageBreadcrumbInfo();
    	current.setTitle(MAIN_TITLE);
    	current.setViewName(VIEW_NAME);		
    			
    	addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
	
	public void initDetailForm(){
		truongThongTinContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, new ArrayList());
		visibleColumnContainer = new BeanItemContainer<VisibleColumn>(VisibleColumn.class, new ArrayList());
    	filterContainer = new BeanItemContainer<FilterObject>(FilterObject.class, new ArrayList());
    	
		CssLayout formContainer = new CssLayout();
		formContainer.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	formContainer.setStyleName(ExplorerLayout.DETAIL_WRAP);
    	
    	FormLayout form = new FormLayout();
    	form.setMargin(true);
    	
    	//Tieu de bao cao thong ke
    	TextField txtTieuDe = new TextField("Tiêu đề");
    	txtTieuDe.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	txtTieuDe.setNullRepresentation("");
    	txtTieuDe.setRequired(true);
    	txtTieuDe.setRequiredError("Không được để trống");
    	String tieuDeValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			tieuDeValue = baoCaoThongKeCurrent.getTieuDe();
		}
    	itemValue.addItemProperty("tieuDe", new ObjectProperty<String>(tieuDeValue, String.class));
    	fieldGroup.bind(txtTieuDe, "tieuDe");
    	txtTieuDe.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
    	
    	//Kieu do thi
    	ComboBox cbbChartTypes = new ComboBox("Kiểu đồ thị");
		for (CommonUtils.ELoaiDoThi item : CommonUtils.ELoaiDoThi.values()) {
			cbbChartTypes.addItem(item.getCode());
			cbbChartTypes.setItemCaption(item.getCode(), item.getName());
		}
		cbbChartTypes.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		cbbChartTypes.setVisible(false);
		String chartTypeValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			chartTypeValue = baoCaoThongKeCurrent.getKieuDoThi();
		}
		itemValue.addItemProperty("chartType", new ObjectProperty<String>(chartTypeValue, String.class));
		fieldGroup.bind(cbbChartTypes, "chartType");
		cbbChartTypes.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
		
		//Process definition
		ProcessDefinitionQuery processQuery = repositoryService.createProcessDefinitionQuery();
		if(deploymentIds!=null&& !deploymentIds.isEmpty()){
			processQuery = processQuery.deploymentIds(deploymentIds);
		}
		List<ProcessDefinition> processDefinitionList = processQuery.orderByProcessDefinitionName().asc().list();
		BeanItemContainer<ProcessDefinition> container = new BeanItemContainer<ProcessDefinition>(ProcessDefinition.class, processDefinitionList);
    	ComboBox cbbQuyTrinh = new ComboBox("Quy trình", container);
    	cbbQuyTrinh.setRequired(true);
    	cbbQuyTrinh.setRequiredError("Không được để trống");
    	cbbQuyTrinh.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbQuyTrinh.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbQuyTrinh.setItemCaptionPropertyId("name");
    	
    	
    	BeanItemContainer<BieuMau> containerBm = new BeanItemContainer<BieuMau>(BieuMau.class, new ArrayList());
    	cbbQuyTrinh.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -7320899139765211724L;
			@Override
			public void valueChange(ValueChangeEvent event) {
				ProcessDefinition selected = (ProcessDefinition) event.getProperty().getValue();
				List<BieuMau> bmList = new ArrayList<BieuMau>();
				if(selected != null){
					ProcessDefinitionEntity processDefEntity = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService).getDeployedProcessDefinition(selected.getId());
					List<ActivityImpl> activities = processDefEntity.getActivities();
					List<String> fomrKeyList = new ArrayList<String>();
					for(ActivityImpl act : activities){
						Object obj = act.getProperty("taskDefinition");
						if(obj != null && obj instanceof TaskDefinition){
							TaskDefinition taskDef = (TaskDefinition) obj;
							if(taskDef.getFormKeyExpression() != null){
								String formKey = taskDef.getFormKeyExpression().getExpressionText();
								if(formKey != null && !formKey.isEmpty()){
									Pattern p = Pattern.compile("(.+)#(.+)");
									Matcher m = p.matcher(formKey);
									if (m.matches()) {
										formKey = m.group(1);
									}
									fomrKeyList.add(formKey);
								}
							}
						}
					}
					bmList = bieuMauService.findByCode(fomrKeyList);
				}
				containerBm.removeAllItems();
				containerBm.addAll(bmList);
			}
		});
    	
    	ProcessDefinition processValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			String processIdTmp = baoCaoThongKeCurrent.getProcessDefId();
			if(processIdTmp != null){
				if(processDefinitionList != null){
					for(ProcessDefinition pd : processDefinitionList){
						if(pd != null && processIdTmp.equals(pd.getId())){
							processValue = pd;
							break;
						}
					}
				}
			}
		}
    	itemValue.addItemProperty("process", new ObjectProperty<ProcessDefinition>(processValue, ProcessDefinition.class));
		fieldGroup.bind(cbbQuyTrinh, "process");

    	//Bieu mau
		cbbBieuMau = new ComboBox("Biểu mẫu", containerBm);
		cbbBieuMau.setRequired(true);
    	cbbBieuMau.setRequiredError("Không được để trống");
    	cbbBieuMau.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	cbbBieuMau.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbBieuMau.setItemCaptionPropertyId("ten");
    	BieuMau bmValue = null;
    	if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
    		bmValue = baoCaoThongKeCurrent.getBieuMau();
    	}

    	CssLayout cbbBieuMauWrap = new CssLayout();
    	cbbBieuMauWrap.setWidth("100%");
    	cbbBieuMau.setData(cbbBieuMauWrap);
    	
    	List<String> maBieuMauList = new ArrayList();
    	if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
    		
    		JSONArray formCodeArr = baoCaoThongKeCurrent.getGiaTri().getJSONArray("form_code");
    		if(formCodeArr != null){
    			for(int i =0 ; i< formCodeArr.length(); i++){
    				maBieuMauList.add(formCodeArr.getString(i));
    			}
    		}
    	}
    	
    	
    	
		List<ComboboxItem> truongThongTinCbbList = new ArrayList<ComboboxItem>();
		CbbBieuMauValueChangeListener cbbValueChangeListener = new CbbBieuMauValueChangeListener();
		cbbValueChangeListener.setFilterContainer(filterContainer);
		cbbValueChangeListener.setTruongThongTinCbbList(truongThongTinCbbList);
		cbbValueChangeListener.setTruongThongTinContainer(truongThongTinContainer);
		cbbValueChangeListener.setVisibleColumnContainer(visibleColumnContainer);
		cbbValueChangeListener.setWrapper(cbbBieuMauWrap);
		cbbValueChangeListener.setBieuMauSelectedMap(bieuMauSelectedMap);
		cbbValueChangeListener.setMaBieuMauSelectedList(maBieuMauList);
		
		
		cbbBieuMau.addValueChangeListener(cbbValueChangeListener);
		
		itemValue.addItemProperty("bieuMau", new ObjectProperty<BieuMau>(bmValue, BieuMau.class));
		fieldGroup.bind(cbbBieuMau, "bieuMau");
		cbbBieuMau.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
		
		
		//Group
		Table tblColumnVisible = new Table("Hiển thị");
		tblColumnVisible.addStyleName(ExplorerLayout.DATA_TABLE);
		tblColumnVisible.setContainerDataSource(visibleColumnContainer);
		tblColumnVisible.setWidth("100%");
		tblColumnVisible.addGeneratedColumn("truongThongTin", new ColumnGenerator(){
			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				VisibleColumn current = (VisibleColumn) itemId;
				if(current != null && current.getTruongThongTin() != null){
					return current.getTruongThongTin().getTen();
				}
				return "";
			}
		});
		
		tblColumnVisible.addGeneratedColumn("tenHienThi", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				VisibleColumn current = (VisibleColumn) itemId;
				if(current != null && current.getTenHienThi() != null){
					return current.getTenHienThi();
				}
				return "";
			}
		});
		
		tblColumnVisible.setColumnWidth("action", 60);
		tblColumnVisible.addGeneratedColumn("action", new ColumnGenerator(){
			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				VisibleColumn current = (VisibleColumn) itemId;
				Button buttonRemoveGroup = new Button();
				buttonRemoveGroup.setId(BUTTON_REMOVE_GROUP_ID);
				buttonRemoveGroup.addClickListener(currentView);
				buttonRemoveGroup.setIcon(FontAwesome.REMOVE);
				buttonRemoveGroup.setStyleName(ExplorerLayout.BTN_REMOVE_TBL_RPT);
				buttonRemoveGroup.setData(current);
				buttonRemoveGroup.addStyleName(ExplorerLayout.REMOVE_BTN_ACT);
				return buttonRemoveGroup;
			}
		});
		
		tblColumnVisible.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;
			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if("action".equals(propertyId)){
                    return "center-aligned";
                }
				return "left-aligned";
			}
		});
		
		tblColumnVisible.setColumnHeader("truongThongTin", "Trường thông tin");
		tblColumnVisible.setColumnHeader("tenHienThi", "Tên hiển thị");
		tblColumnVisible.setColumnHeader("action", "");
		
		
		CssLayout btnAddFieldWrap = new CssLayout();
		btnAddFieldWrap.addStyleName("btn-center btn-add-field-wrap");
		
		Button btnAddField = new Button();
		btnAddField.setDescription("Thêm trường thông tin");
    	btnAddField.setIcon(FontAwesome.PLUS);
    	btnAddField.setStyleName(ExplorerLayout.BTN_ADD_FIELD);
    	btnAddField.setId(BUTTON_SHOW_GROUP_POPUP_ID);
    	btnAddField.addClickListener(this);
    	btnAddFieldWrap.addComponent(btnAddField);
    	
    	//Table filter
		Table filterTable = new Table("Lọc giá trị");
		filterTable.setContainerDataSource(filterContainer);
		filterTable.setColumnHeaderMode(ColumnHeaderMode.HIDDEN);
		filterTable.addStyleName(ExplorerLayout.DATA_TABLE);
		filterTable.setWidth("100%");
		filterTable.addGeneratedColumn("comparison", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				FilterObject current = (FilterObject) itemId;
				CommonUtils.EComparison toanTu = CommonUtils.EComparison.getComparison(current.getComparison());
				if(toanTu != null){
					return toanTu.getName();
				}
				return null;
			}
		});
		filterTable.addGeneratedColumn("value", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				FilterObject current = (FilterObject) itemId;
				
				if(current != null && current.getValue() != null && current.getValue() instanceof java.util.Date){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
					java.util.Date date = (Date) current.getValue();
					return df.format(date);
				}else if(current != null && current.getValue() != null){
					return current.getValue();
				}
				return null;
			}
		});
		filterTable.setColumnWidth("action", 60);
		filterTable.addGeneratedColumn("action", new ColumnGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, Object itemId, Object columnId) {
				FilterObject current = (FilterObject) itemId;
				Button buttonRemoveFilter = new Button();
				buttonRemoveFilter.setId(BUTTON_REMOVE_FILTER_ID);
				buttonRemoveFilter.addClickListener(currentView);
				buttonRemoveFilter.setIcon(FontAwesome.REMOVE);
				buttonRemoveFilter.setStyleName(ExplorerLayout.BTN_REMOVE_TBL_RPT);
				buttonRemoveFilter.setData(current);
				buttonRemoveFilter.addStyleName(ExplorerLayout.REMOVE_BTN_ACT);
				return buttonRemoveFilter;
			}
		});
		
		filterTable.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;
			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if("action".equals(propertyId)){
                    return "center-aligned";
                }
				return "left-aligned";
			}
		});
		filterTable.setVisibleColumns("name", "comparison", "value", "action");
		
		
		//Lay thong tin da luu: filter, group, function
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			try {
				List<VisibleColumn> visibleColumnList = new ArrayList<VisibleColumn>();
				List<FilterObject> filterObjectList = new ArrayList<FilterObject>();
				
	    		JSONObject giaTri = baoCaoThongKeCurrent.getGiaTri();
	    		JSONArray groupArr = giaTri.getJSONArray("visible");
	    		if(groupArr != null){
	    			for(int i =0 ; i< groupArr.length(); i++){
	    				JSONObject json = groupArr.getJSONObject(i);
	    				VisibleColumn obj= new VisibleColumn();
	    				obj.setTenHienThi(json.getString("name"));
	    				String maTruongThongTin = json.getString("key");
	    				if(maTruongThongTin != null){
	    					List<TruongThongTin> list = truongThongTinService.findByCode(maTruongThongTin);
	    					if(list != null && !list.isEmpty()){
	    						obj.setTruongThongTin(list.get(0));
	    					}
	    				}
	    				visibleColumnList.add(obj);
	        		}
	    		}
	    		JSONArray filterArr = giaTri.getJSONArray("filter");
	    		if(filterArr != null){
	    			for(int i =0 ; i< filterArr.length(); i++){
	    				JSONObject json = filterArr.getJSONObject(i);
	    				FilterObject obj= new FilterObject();
	    				obj.setPath(json.getString("path"));
	    				obj.setComparison(json.getLong("comparison"));
	    				Long kieuDL = json.getLong("kieudulieu");
	    				obj.setKieuDuLieu(kieuDL);
	    				obj.setName(json.getString("name"));
	    				obj.setValue(json.get("value"));
	    				
	    				filterObjectList.add(obj);
	        		}
	    		}
	    		
	    		visibleColumnContainer.addAll(visibleColumnList);
	    		filterContainer.addAll(filterObjectList);
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
		
		CssLayout btnAddFilterWrap = new CssLayout();
		btnAddFilterWrap.addStyleName("btn-center btn-add-field-wrap");
		
		Button btnAddFilter = new Button();
    	btnAddFilter.setDescription("Thêm điều kiện lọc");
    	btnAddFilter.setIcon(FontAwesome.PLUS);
    	btnAddFilter.setStyleName(ExplorerLayout.BTN_ADD_FIELD);
    	btnAddFilter.setId(BUTTON_SHOW_FILTER_POPUP_ID);
    	btnAddFilter.addClickListener(this);
    	btnAddFilterWrap.addComponent(btnAddFilter);
    	
    	
    	//Tu ngay
		PopupDateField fromDate = new PopupDateField("Từ ngày");
		fromDate.addStyleName(ExplorerLayout.FORM_CONTROL);
		fromDate.setWidth("100%");
		Date fromDateValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null && baoCaoThongKeCurrent.getTuNgay() != null){
			fromDateValue = new Date(baoCaoThongKeCurrent.getTuNgay().getTime());
    	}
		itemValue.addItemProperty("fromDate", new ObjectProperty<Date>(fromDateValue, Date.class));
		fieldGroup.bind(fromDate, "fromDate");
		fromDate.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
		//Den ngay
		PopupDateField toDate = new PopupDateField("Đến ngày");
		toDate.addStyleName(ExplorerLayout.FORM_CONTROL);
		toDate.setWidth("100%");Date toDateValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null && baoCaoThongKeCurrent.getDenNgay() != null){
			toDateValue = new Date(baoCaoThongKeCurrent.getDenNgay().getTime());
    	}
		itemValue.addItemProperty("toDate", new ObjectProperty<Date>(toDateValue, Date.class));
		fieldGroup.bind(toDate, "toDate");
		toDate.setReadOnly(Constants.ACTION_DEL.equals(action) || Constants.ACTION_VIEW.equals(action));
		
		
		
    	form.addComponent(txtTieuDe);
    	form.addComponent(cbbChartTypes);
    	form.addComponent(cbbQuyTrinh);

    	form.addComponent(cbbBieuMau);
    	form.addComponent(cbbBieuMauWrap);
    	form.addComponent(tblColumnVisible);
    	form.addComponent(btnAddFieldWrap);
    	form.addComponent(filterTable);
    	form.addComponent(btnAddFilterWrap);
    	form.addComponent(fromDate);
    	form.addComponent(toDate);
    	initDonViTreeTable();
    	form.addComponent(donViTreeTable);

    	formContainer.addComponent(form);
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	
    	Button btnCreateReport = new Button();
    	btnCreateReport.setCaption("Tạo báo cáo");
		btnCreateReport.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnCreateReport.setId(BUTTON_CREATE_REPORT_ID);
		btnCreateReport.addClickListener(this);
		buttons.addComponent(btnCreateReport);
		
		Button btnUpdate = new Button();
		if(!Constants.ACTION_VIEW.equals(action)){
    		btnUpdate = new Button();
    		btnUpdate.addClickListener(this);
    		buttons.addComponent(btnUpdate);
    		if(Constants.ACTION_EDIT.equals(action)){
    			btnUpdate.setCaption("Lưu");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_UPDATE_ID);
        	}else if(Constants.ACTION_ADD.equals(action) || Constants.ACTION_COPY.equals(action)){
        		btnUpdate.setCaption("Lưu");
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setId(BUTTON_UPDATE_ID);
        	}else if(Constants.ACTION_DEL.equals(action)){
        		btnUpdate.setId(BUTTON_DEL_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_DANGER);
        		btnUpdate.setCaption("Xóa");
        	}else if(Constants.ACTION_APPROVE.equals(action)){
        		btnUpdate.setId(BUTTON_UPDATE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
        		btnUpdate.setCaption(baoCaoThongKeWf.getPheDuyetTen());
        	}else if(Constants.ACTION_REFUSE.equals(action)){
        		btnUpdate.setCaption(baoCaoThongKeWf.getTuChoiTen());
        		btnUpdate.setId(BUTTON_UPDATE_ID);
        		btnUpdate.addStyleName(ExplorerLayout.BUTTON_WARNING);
        	}
    	}
		buttons.addComponent(btnUpdate);
    	
    	Button btnBack = new Button("Quay lại");
    	btnBack.addClickListener(this);
    	btnBack.setId(BUTTON_CANCEL_ID);
    	buttons.addComponent(btnBack);
		
    	form.addComponent(buttons);
    	
    	addComponent(formContainer);
	}
	
	public void initDonViTreeTable(){
		donViTreeTable = new TreeTable("Gửi tới đơn vị");
		
		donViTreeTable.setWidth("100%");
		
		donViTreeTable.addContainerProperty("ma", CheckBox.class, null);
    	donViTreeTable.addContainerProperty("ten", String.class, null);
    	donViTreeTable.setColumnHeader("ma", "Mã");
    	donViTreeTable.setColumnHeader("ten", "Tên");
		
    	donViTreeTable.setVisibleColumns("ma", "ten");
		
    	List<HtDonVi> htDonViList = getHtDonViCon(null);
		
    	if(baoCaoThongKeCurrent.getId() != null){
    		List<BaoCaoThongKeDonVi> baoCaoThongKeDonViList = baoCaoThongKeDonViDao.findByBaoCaoThongKe(baoCaoThongKeCurrent);
    		
    		if(baoCaoThongKeDonViList != null){
    			
    			for(BaoCaoThongKeDonVi bctkdv : baoCaoThongKeDonViList){
    				donViSelected.put(bctkdv.getHtDonVi(), true);
    			}
    		}
    	}
		
		for(HtDonVi donVi:htDonViList){
			Boolean check=donViSelected.get(donVi);
			CheckBox checkBox =new CheckBox();
			checkBox.setCaption(donVi.getMa());
			checkBox.setValue(check!=null&&check);
			
			checkBox.addValueChangeListener(new ValueChangeListener(){
				@Override
				public void valueChange(ValueChangeEvent event) {
					Boolean check=(Boolean) event.getProperty().getValue();
					if(check!=null&&check){
						donViSelected.put(donVi,true);
					}else{
						donViSelected.put(donVi,false);
					}
				}
			});
			donViTreeTable.addItem(new Object[]{checkBox, donVi.getTen()},donVi);
			donViTreeTable.setChildrenAllowed(donVi, false);
			
		}
		for(HtDonVi donVi:htDonViList){
			if(donVi.getHtDonVi()!=null){
				donViTreeTable.setChildrenAllowed(donVi.getHtDonVi(), true);
				donViTreeTable.setParent(donVi, donVi.getHtDonVi());
			}
		}
		
	}
	
	public List<HtDonVi> getHtDonViCon(HtDonVi htDonVi){
		List<HtDonVi> htDonViList = new ArrayList();
		if(htDonVi != null){
			List<HtDonVi> htDonViAll = htDonViServices.getHtDonViFind(null,null, null, null, -1, -1);
			if(htDonViAll != null && !htDonViAll.isEmpty()){
				List<HtDonVi> process = new ArrayList();
				process.add(htDonVi);
				while(!process.isEmpty()){
					HtDonVi current = process.remove(0);
					for(HtDonVi dv : htDonViAll){
						if(dv != null && dv.getHtDonVi() != null && dv.getHtDonVi().equals(current)){
							process.add(dv);
							htDonViList.add(dv);
						}
					}
				}
			}
			
		}else{
			htDonViList = htDonViServices.getHtDonViFind(null,null, null, null, -1, -1);
		}
		return htDonViList;
	}
	
	public void initSaveWindow(){
    	saveFieldGroup.setItemDataSource(saveItemValue);
    	FormLayout form = new FormLayout();
    	form.setWidth("100%");
    	form.setMargin(true);
    	form.setSpacing(true);
    	
    	TextField txtMa = new TextField("Mã báo cáo thống kê");
    	txtMa.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	txtMa.setNullRepresentation("");
    	form.addComponent(txtMa);
    	txtMa.setRequired(true);
    	txtMa.setRequiredError("Không được để trống");
    	txtMa.addValidator(new StringLengthValidator("Số ký tự tối đa là 20", 1, 20, false));   
    	txtMa.addValidator(new RegexpValidator("[0-9a-zA-Z_]+", "Chỉ chấp nhận các ký tự 0-9 a-z A-Z _"));
    	String maValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			maValue = baoCaoThongKeCurrent.getMa();
		}else{
			txtMa.addTextChangeListener(new TextChangeListener() {
				private static final long serialVersionUID = 1L;

				@Override
				public void textChange(TextChangeEvent event) {

					if(!checkTrungMa(event.getText(), action)){
						txtMa.setComponentError(new UserError("Mã bị trùng. Vui lòng nhập lại!"));
					}else{
						txtMa.setComponentError(null);
					}
					
				}

				
	    	});
		}
    	saveItemValue.addItemProperty("ma", new ObjectProperty<String>(maValue, String.class));
    	saveFieldGroup.bind(txtMa, "ma");
    	txtMa.setReadOnly(!Constants.ACTION_ADD.equals(action));
    	
    	
    	
    	TextField txtTen = new TextField("Tên báo cáo thống kê");
    	txtTen.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	txtTen.setNullRepresentation("");
    	form.addComponent(txtTen);
    	txtTen.setRequired(true);
    	txtTen.setRequiredError("Không được để trống");
    	String tenValue = null;
		if(!Constants.ACTION_ADD.equals(action) && baoCaoThongKeCurrent != null){
			tenValue = baoCaoThongKeCurrent.getTen();
		}
    	saveItemValue.addItemProperty("ten", new ObjectProperty<String>(tenValue, String.class));
    	saveFieldGroup.bind(txtTen, "ten");
    	
    	String lbl = "Lưu";
    	if(Constants.ACTION_APPROVE.equals(action)){
			lbl = baoCaoThongKeWf.getPheDuyetTen();
		}else if(Constants.ACTION_REFUSE.equals(action)){
			lbl = baoCaoThongKeWf.getTuChoiTen();
		}
    	
    	Button btnSave = new Button(lbl);
    	btnSave.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
    	btnSave.addClickListener(this);
    	btnSave.setId(BUTTON_UPDATE_POPUP_ID);
    	form.addComponent(btnSave);
    	
    	
    	saveWindow = new Window(lbl);
    	saveWindow.setWidth(500, Unit.PIXELS);
    	saveWindow.setHeight(210, Unit.PIXELS);
    	saveWindow.setResizable(true);
    	saveWindow.center();
    	saveWindow.setModal(true);
    	saveWindow.setContent(form);
    }

	public boolean checkTrungMa(String ma, String act) {
		try {
			if (ma == null || ma.isEmpty()) {
				return false;
			}
			if (Constants.ACTION_ADD.equals(act) || Constants.ACTION_COPY.equals(act)) {
				List<BaoCaoThongKe> list = baoCaoThongKeService.findByCode(ma);
				if (list == null || list.isEmpty()) {
					return true;
				}
			} else if (!Constants.ACTION_DEL.equals(act)) {
				List<BaoCaoThongKe> list = baoCaoThongKeService.findByCode(ma);
				if (list == null || list.size() <= 1) {
					return true;
				}
			} else if (Constants.ACTION_DEL.equals(act)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void initTableResultViewer(){
		VerticalLayout content = new VerticalLayout();
		content.setWidth("100%");
    	content.setSpacing(true);
    	content.setMargin(true);
		
    	tableContainer = new HorizontalLayout();
    	tableContainer.setWidth("100%");
    	
    	
    	HorizontalLayout buttons = new HorizontalLayout();
    	buttons.setSpacing(true);
    	buttons.setStyleName(ExplorerLayout.DETAIL_FORM_BUTTONS);
    	buttons.addStyleName(ExplorerLayout.BUTTONS_CENTER);
    	
    	Button btnDownloadExcel = new Button();
		
		btnDownloadExcel.setCaption("Tải excel");
		btnDownloadExcel.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnDownloadExcel.setId(BUTTON_DOWNLOAD_EXCEL_ID);
		btnDownloadExcel.addClickListener(this);
		buttons.addComponent(btnDownloadExcel);
		
		content.addComponent(tableContainer);
		content.addComponent(buttons);
    	
		tableResultWindow = new Window("Báo cáo thống kê");
		tableResultWindow.setWidth(100, Unit.PERCENTAGE);
		tableResultWindow.setHeight(100, Unit.PERCENTAGE);
		tableResultWindow.setResizable(true);
		tableResultWindow.center();
		tableResultWindow.setModal(true);
		tableResultWindow.setContent(content);
    }
	
	public void initFilterWindow(){
    	FormLayout form = new FormLayout();
    	form.setWidth("100%");
    	form.setMargin(true);
    	form.setSpacing(true);
    	
    	ComboBox truongThongTinCbb = new ComboBox("Trường thông tin", truongThongTinContainer);
    	truongThongTinCbb.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	truongThongTinCbb.setItemCaptionPropertyId("text");
    	truongThongTinCbb.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	truongThongTinCbb.setRequired(true);
    	truongThongTinCbb.setRequiredError("Không được để trống");
    	form.addComponent(truongThongTinCbb);
    	filterItemValue.addItemProperty("truongThongTin", new ObjectProperty<ComboboxItem>(null, ComboboxItem.class));
		filterFieldGroup.bind(truongThongTinCbb, "truongThongTin");
    	
    	ComboBox comparisonCbb = new ComboBox("Toán tử");
    	comparisonCbb.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	comparisonCbb.setRequired(true);
    	comparisonCbb.setRequiredError("Không được để trống");
    	form.addComponent(comparisonCbb);
    	filterItemValue.addItemProperty("comparison", new ObjectProperty<Long>(null, Long.class));
		filterFieldGroup.bind(comparisonCbb, "comparison");
    	
    	TextField valueTxt = new TextField("Giá trị");
    	valueTxt.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	valueTxt.setNullRepresentation("");
    	form.addComponent(valueTxt);
    	filterItemValue.addItemProperty("value", new ObjectProperty<Object>(null, Object.class));
		filterFieldGroup.bind(valueTxt, "value");
		valueTxt.setVisible(true);
		
		PopupDateField dateField = new PopupDateField("Giá trị");
		dateField.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		
    	form.addComponent(dateField);
    	filterItemValue.addItemProperty("date", new ObjectProperty<Date>(null, Date.class));
		filterFieldGroup.bind(dateField, "date");
		dateField.setVisible(false);
		
		truongThongTinCbb.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				ComboboxItem current = (ComboboxItem) event.getProperty().getValue(); 
				comparisonCbb.removeAllItems();
				if(current != null && current.getValue() instanceof TruongThongTin){
					TruongThongTin truongTT = (TruongThongTin) current.getValue();
					if(truongTT != null && truongTT.getKieuDuLieu() != null){
						if(truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
							valueTxt.setVisible(false);
							dateField.setVisible(true);
							dateField.setRequired(true);
							dateField.setRequiredError("Không được để trống");
					    	valueTxt.setRequired(false);
					    	for (CommonUtils.EComparison item : CommonUtils.EComparison.values()) {
								if((4&item.getType()) == 4){
									comparisonCbb.addItem(new Long(item.getCode()));
						    		comparisonCbb.setItemCaption(new Long(item.getCode()), item.getName());
								}
							}
						}else{
							dateField.setRequired(false);
							valueTxt.setVisible(true);
							dateField.setVisible(false);
					    	valueTxt.setRequired(true);
					    	valueTxt.setRequiredError("Không được để trống");
							if(truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa()){
								valueTxt.setConverter(Integer.class);
								for (CommonUtils.EComparison item : CommonUtils.EComparison.values()) {
									if((2&item.getType()) == 2){
										comparisonCbb.addItem(new Long(item.getCode()));
							    		comparisonCbb.setItemCaption(new Long(item.getCode()), item.getName());
									}
								}
							}else if(truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.NUMBER.getMa()){
								valueTxt.setConverter(BigDecimal.class);
								for (CommonUtils.EComparison item : CommonUtils.EComparison.values()) {
									if((2&item.getType()) == 2){
										comparisonCbb.addItem(new Long(item.getCode()));
							    		comparisonCbb.setItemCaption(new Long(item.getCode()), item.getName());
									}
								}
							}else{
								valueTxt.setConverter(String.class);
								for (CommonUtils.EComparison item : CommonUtils.EComparison.values()) {
									if((8&item.getType()) == 8){
										comparisonCbb.addItem(new Long(item.getCode()));
							    		comparisonCbb.setItemCaption(new Long(item.getCode()), item.getName());
									}
								}
							}
						}
					}
				}
			}
		});
		
		Button addFilter = new Button("Thêm");
		addFilter.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
		addFilter.addClickListener(this);
		addFilter.setId(BUTTON_ADD_FILTER_ID);
		form.addComponent(addFilter);
    	
    	filterWindow = new Window("Điều kiện lọc");
    	filterWindow.setWidth(600, Unit.PIXELS);
    	filterWindow.setHeight(300, Unit.PIXELS);
    	filterWindow.setResizable(true);
    	filterWindow.center();
    	filterWindow.setModal(true);
    	filterWindow.setContent(form);
    }
	
	public void initVisibleCoumnWindow(){
    	FormLayout form = new FormLayout();
    	form.setWidth("100%");
    	form.setMargin(true);
    	form.setSpacing(true);
    	
    	ComboBox truongThongTinCbb = new ComboBox("Trường thông tin", truongThongTinContainer);
    	truongThongTinCbb.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	truongThongTinCbb.setItemCaptionPropertyId("text");
    	truongThongTinCbb.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	truongThongTinCbb.setRequired(true);
    	truongThongTinCbb.setRequiredError("Không được để trống");
    	form.addComponent(truongThongTinCbb);
    	visibleColumnItemValue.addItemProperty("truongThongTin", new ObjectProperty<ComboboxItem>(null, ComboboxItem.class));
		visibleColumnFieldGroup.bind(truongThongTinCbb, "truongThongTin");
    	
    	TextField txtAliasName = new TextField("Tên hiển thị");
    	txtAliasName.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
    	txtAliasName.setNullRepresentation("");
    	txtAliasName.setRequired(true);
    	txtAliasName.setRequiredError("Không được để trống");
    	form.addComponent(txtAliasName);
    	visibleColumnItemValue.addItemProperty("alias", new ObjectProperty<String>(null, String.class));
    	visibleColumnFieldGroup.bind(txtAliasName, "alias");
    	
    	truongThongTinCbb.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				ComboboxItem obj = (ComboboxItem) event.getProperty().getValue();
				if(obj != null && obj.getText() != null){
					txtAliasName.setValue(obj.getText());
				}
			}
		});

		Button addGroup = new Button("Thêm");
		addGroup.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
		addGroup.addClickListener(this);
		addGroup.setId(BUTTON_ADD_GROUP_ID);
		form.addComponent(addGroup);
    	
    	visibleColumnWindow = new Window("Nhóm");
    	visibleColumnWindow.setWidth(600, Unit.PIXELS);
    	visibleColumnWindow.setHeight(200, Unit.PIXELS);
    	visibleColumnWindow.setResizable(true);
    	visibleColumnWindow.center();
    	visibleColumnWindow.setModal(true);
    	visibleColumnWindow.setContent(form);
    }
	
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BUTTON_CREATE_REPORT_ID.equals(button.getId()) || BUTTON_DOWNLOAD_EXCEL_ID.equals(button.getId())){
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					ProcessDefinition processDef = (ProcessDefinition) itemValue.getItemProperty("process").getValue();
					List<String> processInstanceIdList = new ArrayList<String>();
					List<HistoricProcessInstance> historicProInsList = historyService.createHistoricProcessInstanceQuery().processDefinitionId(processDef.getId()).list();
					if(historicProInsList != null && !historicProInsList.isEmpty()){
						for(HistoricProcessInstance h : historicProInsList){
							processInstanceIdList.add(h.getId());
						}
					}
					BieuMau bieuMau = (BieuMau) itemValue.getItemProperty("bieuMau").getValue();
					String tieuDe = (String) itemValue.getItemProperty("tieuDe").getValue();
					
					List<String> maBieuMauList = new ArrayList();
					for(Map.Entry<String, String> entry : bieuMauSelectedMap.entrySet()){
						if(!maBieuMauList.contains(entry.getKey())){
							maBieuMauList.add(entry.getKey());
						}
					}
					Map<String, Object> bieuMauTreeMap = ReportUtils.getBieuMauTreeMap(bieuMau, maBieuMauList);
					Date fromDate = (Date) itemValue.getItemProperty("fromDate").getValue();
					Date toDate = (Date) itemValue.getItemProperty("toDate").getValue();
					List<FilterObject> filterList =  filterContainer.getItemIds();
					List<VisibleColumn> visibleColumnList =  visibleColumnContainer.getItemIds();
					Map<String, Object> paramMap = new HashMap();
					String sql = ReportUtils.createSqlString(bieuMauTreeMap, processInstanceIdList, filterList, visibleColumnList, fromDate, toDate, paramMap);
					List<Map<String, Object>> resultList = hoSoService.getReportResult(sql, paramMap);
					
					Object test = cbbBieuMau.getData();
					if(test instanceof CssLayout){
						CssLayout w = (CssLayout) test;
						if(w.getComponentCount() > 0){
							Object c1 = w.getComponent(0);
							System.out.println(c1);
						}
					}
					
					if(BUTTON_DOWNLOAD_EXCEL_ID.equals(button.getId())){
						Workbook workbook = ReportUtils.exportExcel(tieuDe, resultList, visibleColumnList);
						File tempFile = File.createTempFile("tmp", ".xlsx");
						FileOutputStream fos = new FileOutputStream(tempFile);
						workbook.write(fos);
						fos.close();
						String fileName = "BCTK";
						if(tieuDe != null){
							fileName = tieuDe;
						}
					    TemporaryFileDownloadResource resourcetmp = new TemporaryFileDownloadResource(fileName+".xlsx", "application/vnd.ms-excel", tempFile);
					    UI.getCurrent().getPage().open(resourcetmp, "Báo cáo thống kê Excel", false);
					}
					if(BUTTON_CREATE_REPORT_ID.equals(button.getId())){
						Table resultTable = ReportUtils.createTableViewer(resultList, visibleColumnList);
						tableContainer.removeAllComponents();
						tableContainer.addComponent(resultTable);
						tableResultWindow.setCaption(tieuDe);
						UI.getCurrent().addWindow(tableResultWindow);
					}
					
				}
			}else if(BUTTON_CANCEL_ID.equals(button.getId())){
				UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeView.VIEW_NAME);
			}else if(BUTTON_SHOW_GROUP_POPUP_ID.equals(button.getId())){
				UI.getCurrent().addWindow(visibleColumnWindow);
			}else if(BUTTON_SHOW_FILTER_POPUP_ID.equals(button.getId())){
				UI.getCurrent().addWindow(filterWindow);
			}else if(BUTTON_REMOVE_GROUP_ID.equals(button.getId())){
				VisibleColumn current = (VisibleColumn) button.getData();
				visibleColumnContainer.removeItem(current);
			}else if(BUTTON_ADD_FILTER_ID.equals(button.getId())){
				if(filterFieldGroup.isValid()){
					filterFieldGroup.commit();
					filterWindow.close();
					Object value = filterItemValue.getItemProperty("value").getValue();
					Date valueDate = (Date) filterItemValue.getItemProperty("date").getValue();
					Long comparison = (Long) filterItemValue.getItemProperty("comparison").getValue();
					ComboboxItem truongTTCbb = (ComboboxItem) filterItemValue.getItemProperty("truongThongTin").getValue();

					FilterObject filterObject = new FilterObject();
					filterObject.setName(truongTTCbb.getText());
					filterObject.setComparison(comparison);
					filterObject.setValue(value);
					if(truongTTCbb != null && truongTTCbb.getValue() instanceof TruongThongTin){
						TruongThongTin  truongTT = (TruongThongTin) truongTTCbb.getValue();
						filterObject.setPath(truongTT.getMa());
						if(truongTT != null && truongTT.getKieuDuLieu() != null){
							filterObject.setKieuDuLieu(truongTT.getKieuDuLieu());
							if( truongTT.getKieuDuLieu().intValue() == CommonUtils.EKieuDuLieu.DATE.getMa()){
								SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
								if(valueDate != null){
									filterObject.setValue(df.format(valueDate));
								}
								
							}
						}
					}
					
					filterContainer.addBean(filterObject);
					filterFieldGroup.clear();
				}
			}else if(BUTTON_ADD_GROUP_ID.equals(button.getId())){
				if(visibleColumnFieldGroup.isValid()){
					visibleColumnFieldGroup.commit();
					visibleColumnWindow.close();
					String alias = (String) visibleColumnItemValue.getItemProperty("alias").getValue();
					ComboboxItem truongTTCbb = (ComboboxItem) visibleColumnItemValue.getItemProperty("truongThongTin").getValue();

					VisibleColumn visibleColumnObject = new VisibleColumn();
					visibleColumnObject.setTenHienThi(alias);
					if(truongTTCbb != null && truongTTCbb.getValue() instanceof TruongThongTin){
						TruongThongTin truongTT = (TruongThongTin) truongTTCbb.getValue();
						visibleColumnObject.setTruongThongTin(truongTT);
					}
					
					visibleColumnContainer.addBean(visibleColumnObject);
					visibleColumnFieldGroup.clear();
				}
			}else if(BUTTON_REMOVE_FILTER_ID.equals(button.getId())){
				FilterObject current = (FilterObject) button.getData();
				filterContainer.removeItem(current);
			}else if(BUTTON_UPDATE_ID.equals(button.getId())){
				if(fieldGroup.isValid()){
					UI.getCurrent().addWindow(saveWindow);
				}
			}else if(BUTTON_DEL_ID.equals(button.getId())){
				baoCaoThongKeService.delete(baoCaoThongKeCurrent);
				Notification notf = new Notification("Thông báo", "Xóa thành công");
				notf.setDelayMsec(3000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(UI.getCurrent().getPage());
				UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeView.VIEW_NAME);
			}if(BUTTON_UPDATE_POPUP_ID.equals(button.getId())){
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					if(saveFieldGroup.isValid()){
						saveWindow.close();
						saveFieldGroup.commit();
						ProcessDefinition processDef = (ProcessDefinition) itemValue.getItemProperty("process").getValue();
						BieuMau bieuMau = (BieuMau) itemValue.getItemProperty("bieuMau").getValue();
						Date fromDateTmp = (Date) itemValue.getItemProperty("fromDate").getValue();
						Date toDateTmp = (Date) itemValue.getItemProperty("toDate").getValue();
						String tieuDe = (String) itemValue.getItemProperty("tieuDe").getValue();
						List<FilterObject> filterList =  filterContainer.getItemIds();
						List<VisibleColumn> visibleColumnList =  visibleColumnContainer.getItemIds();
						

						String ten = (String) saveItemValue.getItemProperty("ten").getValue();
						String ma = (String) saveItemValue.getItemProperty("ma").getValue();
						
						baoCaoThongKeCurrent.setBieuMau(bieuMau);
						baoCaoThongKeCurrent.setProcessDefId(processDef.getId());
						baoCaoThongKeCurrent.setTieuDe(tieuDe);
						baoCaoThongKeCurrent.setMa(ma);
						baoCaoThongKeCurrent.setTen(ten);
						Timestamp fromDate = null;
						if(fromDateTmp != null){
							fromDate = new Timestamp(fromDateTmp.getTime());
						}
						baoCaoThongKeCurrent.setTuNgay(fromDate);
						
						Timestamp toDate = null;
						if(toDateTmp != null){
							toDate = new Timestamp(toDateTmp.getTime());
						}

						baoCaoThongKeCurrent.setDenNgay(toDate);
						Date now = new Date();
						if(Constants.ACTION_ADD.equals(action)){
							baoCaoThongKeCurrent.setNgayTao(new Timestamp(now.getTime()));
							baoCaoThongKeCurrent.setNguoiTao(CommonUtils.getUserLogedIn());
						}else{
							baoCaoThongKeCurrent.setNgayCapNhat(new Timestamp(now.getTime()));
							baoCaoThongKeCurrent.setNguoiCapNhat(CommonUtils.getUserLogedIn());
						}
						
						JSONObject json = new JSONObject();
						
						if(visibleColumnList != null){
							JSONArray arr = new JSONArray();
							for(VisibleColumn item : visibleColumnList){
								JSONObject fnJson = new JSONObject();
								fnJson.put("key", item.getTruongThongTin().getMa());
								fnJson.put("name", item.getTenHienThi());
								arr.put(fnJson);
							}
							json.put("visible", arr);
						}
						
						List<String> maBieuMauList = new ArrayList();
						for(Map.Entry<String, String> entry : bieuMauSelectedMap.entrySet()){
							if(!maBieuMauList.contains(entry.getKey())){
								maBieuMauList.add(entry.getKey());
							}
						}
						json.put("form_code", maBieuMauList);
						
						if(filterList != null){
							JSONArray arr = new JSONArray();
							for(FilterObject item : filterList){
								JSONObject filterJson = new JSONObject();
								filterJson.put("name", item.getName());
								filterJson.put("value", item.getValue());
								filterJson.put("comparison", item.getComparison());
								filterJson.put("path", item.getPath());
								filterJson.put("kieudulieu", item.getKieuDuLieu());
								arr.put(filterJson);
							}
							
							json.put("filter", arr);
						}
						
						baoCaoThongKeCurrent.setGiaTri(json);
						
						String mss = "Lưu thành công";
						List<BaoCaoThongKeDonVi> baoCaoThongKeDonViList = new ArrayList();
						if(donViSelected != null){
							for(Entry<HtDonVi, Boolean> entry : donViSelected.entrySet()){
								if(entry.getValue() != null && entry.getValue()){
									BaoCaoThongKeDonVi baoCaoThongKeDonVi = new BaoCaoThongKeDonVi();
									baoCaoThongKeDonVi.setBaoCaoThongKe(baoCaoThongKeCurrent);
									baoCaoThongKeDonVi.setHtDonVi(entry.getKey());
									baoCaoThongKeDonViList.add(baoCaoThongKeDonVi);
								}
							}
						}
						if(Constants.ACTION_APPROVE.equals(action)){
							BaoCaoThongKe current = baoCaoThongKeWf.pheDuyet();
							current.setHtDonVi(htDonViCurrent);
							
							baoCaoThongKeService.save(current, baoCaoThongKeDonViList);
							mss = baoCaoThongKeWf.getPheDuyetTen() + " thành công";
						}else if(Constants.ACTION_REFUSE.equals(action)){
							BaoCaoThongKe current = baoCaoThongKeWf.tuChoi();
							current.setHtDonVi(htDonViCurrent);
							
							baoCaoThongKeService.save(current, baoCaoThongKeDonViList);
							mss = baoCaoThongKeWf.getTuChoiTen() + " thành công";
						}else{
							baoCaoThongKeCurrent.setHtDonVi(htDonViCurrent);
							baoCaoThongKeService.save(baoCaoThongKeCurrent, baoCaoThongKeDonViList);
						}
						
						Notification notf = new Notification("Thông báo", mss);
						notf.setDelayMsec(3000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(UI.getCurrent().getPage());
						
						if(Constants.ACTION_APPROVE.equals(action) || Constants.ACTION_REFUSE.equals(action)){
							UI.getCurrent().getNavigator().navigateTo(BaoCaoThongKeView.VIEW_NAME);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}
	
	
	

}

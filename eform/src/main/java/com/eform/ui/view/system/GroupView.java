package com.eform.ui.view.system;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.ComboboxItem;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.ui.custom.CssFormLayout;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.paging.GroupPaginationBar;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SpringView(name = GroupView.VIEW_NAME)
public class GroupView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "group";
	static final String MAIN_TITLE = "Nhóm người dùng";
	private static final String SMALL_TITLE = "";

	private static final String BTN_SEARCH_ID = "timKiem";
	private static final String BTN_REFRESH_ID = "refresh";
	private static final String BTN_VIEW_ID = "xem";
	private static final String BTN_EDIT_ID = "sua";
	private static final String BTN_DEL_ID = "xoa";
	private static final String BTN_PHAN_NHOM_ID = "phanNhom";
	private static final String BTN_ADDNEW_ID = "themMoi";
	private static final String BTN_PHAN_QUYEN_ID = "btnPhanQuyen";
	private final GroupView currentView = this;
	
	private CssLayout mainTitleWrap;
	private BeanItemContainer<Group> container;
	private Table table = new Table();
	
	private final PropertysetItem searchItem = new PropertysetItem();
	private final FieldGroup searchFieldGroup = new FieldGroup();
	private Button btnAddNew;

	@Autowired
	protected IdentityService identityService;
	
	private GroupPaginationBar paginationBar;
	@Autowired
	private MessageSource messageSource;
	
	@PostConstruct
    void init() {

		container = new BeanItemContainer<Group>(Group.class, new ArrayList());
		
		paginationBar = new GroupPaginationBar(container, identityService);
		
    	searchItem.addItemProperty("id", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("name", new ObjectProperty<String>(""));
    	searchItem.addItemProperty("type", new ObjectProperty<ComboboxItem>(new ComboboxItem()));
    	searchFieldGroup.setItemDataSource(searchItem);
		
		initMainTitle();
		initSearchForm();
		initDataContent();
		
		btnAddNew = new Button();
    	btnAddNew.setIcon(FontAwesome.PLUS);
    	btnAddNew.addClickListener(this);
    	btnAddNew.addStyleName(Reindeer.BUTTON_LINK);
    	btnAddNew.setDescription("Tạo mới nhóm người dùng");
    	btnAddNew.addStyleName(ExplorerLayout.BUTTON_ADDNEW);
    	btnAddNew.setId(BTN_ADDNEW_ID);
		addComponent(btnAddNew);
		
		
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(this.MAIN_TITLE);
		current.setViewName(this.VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, true));
	}
	
	public void initSearchForm(){
		
		CssFormLayout searchForm = new CssFormLayout(-1, 60);

		TextField txtId = new TextField("Mã nhóm");
		searchForm.addFeild(txtId);
		TextField txtName = new TextField("Tên nhóm");
		searchForm.addFeild(txtName);
		
		List<ComboboxItem> typeList = new ArrayList<ComboboxItem>();
		typeList.add(new ComboboxItem("security-role", "security-role"));
		typeList.add(new ComboboxItem("assignment", "assignment"));
		
		BeanItemContainer<ComboboxItem> cbbTypeContainer = new BeanItemContainer<ComboboxItem>(ComboboxItem.class, typeList);
		ComboBox cbbType = new ComboBox("Loại nhóm", cbbTypeContainer);
    	cbbType.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    	cbbType.setItemCaptionPropertyId("text");
    	searchForm.addFeild(cbbType);
		
		

		Button btnSearch = new Button("Tìm kiếm");
		btnSearch.setId(BTN_SEARCH_ID);
		btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnSearch.setClickShortcut(KeyCode.ENTER);
		btnSearch.setDescription("Nhấn ENTER");
		btnSearch.addClickListener(currentView);
		searchForm.addButton(btnSearch);
		paginationBar.fixEnterHotKey(btnSearch);
		
		Button btnRefresh = new Button("Làm mới");
		btnRefresh.setId(BTN_REFRESH_ID);
		btnRefresh.setDescription("Nhấn ESC");
		btnRefresh.setClickShortcut(KeyCode.ESCAPE);
		btnRefresh.addClickListener(currentView);
		searchForm.addButton(btnRefresh);
		
		searchFieldGroup.bind(txtId, "id");
		searchFieldGroup.bind(txtName, "name");
		searchFieldGroup.bind(cbbType, "type");
		
		addComponent(searchForm);
	}
	
	public void initDataContent(){
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
		table.setContainerDataSource(container);
		table.addStyleName(ExplorerLayout.DATA_TABLE);
		table.setResponsive(true);
		table.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		table.setColumnHeader("id", "Mã nhóm");
		table.setColumnHeader("name", "Tên nhóm");
		table.setColumnHeader("type", "Loại nhóm");
		
		table.addGeneratedColumn("action", new ColumnGenerator() { 
			private static final long serialVersionUID = 1L;

			@Override
		    public Object generateCell(final Table source, final Object itemId, Object columnId) {
				HorizontalLayout actionWrap = new HorizontalLayout();
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
				
				
				Button btnView = new Button();
				btnView.setDescription("Xem");
		        btnView.setId(BTN_VIEW_ID);
		        btnView.setIcon(FontAwesome.EYE);
		        btnView.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnView.addClickListener(currentView);
		        btnView.setData(itemId);
		        btnView.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnView);
		        
		        
		        Button btnEdit = new Button();
		        btnEdit.setDescription("Sửa");
		        btnEdit.setId(BTN_EDIT_ID);
		        btnEdit.setIcon(FontAwesome.PENCIL_SQUARE_O);
		        btnEdit.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnEdit.addClickListener(currentView);
		        btnEdit.setData(itemId);
		        btnEdit.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnEdit);
		        
		        Button btnDel = new Button();
		        btnDel.setDescription("Xóa");
		    	btnDel.setId(BTN_DEL_ID);
		    	btnDel.addStyleName(ExplorerLayout.BUTTON_ACTION);
		    	btnDel.addStyleName(Reindeer.BUTTON_LINK);
		    	btnDel.setData(itemId);
		    	btnDel.setIcon(FontAwesome.REMOVE);
		    	btnDel.addClickListener(currentView);
		        actionWrap.addComponent(btnDel);
		        
		        
		        Button btnPhanNhom = new Button();
		        btnPhanNhom.setDescription("Phân nhóm");
		        btnPhanNhom.setId(BTN_PHAN_NHOM_ID);
		        btnPhanNhom.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnPhanNhom.addStyleName(Reindeer.BUTTON_LINK);
		        btnPhanNhom.setData(itemId);
		        btnPhanNhom.setIcon(FontAwesome.USERS);
		        btnPhanNhom.addClickListener(currentView);
		        actionWrap.addComponent(btnPhanNhom);
		       
		        return actionWrap;
		    }
		});

		table.setColumnHeader("action", "Thao tác");
		table.setRowHeaderMode(RowHeaderMode.INDEX);

		table.setVisibleColumns("id", "name", "type", "action");
		

		table.setCellStyleGenerator(new Table.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if ("ma".equals(propertyId)) {
                    return "left-aligned";
                }else if("ten".equals(propertyId)){
                    return "left-aligned";
                }else if("trangThai".equals(propertyId)){
                    return "center-aligned ";
                }else if("loaiDanhMuc".equals(propertyId)){
                    return "left-aligned";
                }else if("action".equals(propertyId)){
                    return "left-aligned";
                }
				return "left-aligned";
			}
			
		});

		dataWrap.addComponent(table);
		dataWrap.addComponent(paginationBar);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_SEARCH_ID.equals(event.getButton().getId())){
			try {
				if(searchFieldGroup.isValid()){
					searchFieldGroup.commit();
					String id = (String) searchItem.getItemProperty("id").getValue();
					id = id != null ? id.trim() : null;

					String name = (String) searchItem.getItemProperty("name").getValue();
					name = name != null ? name.trim() : "";
					
					ComboboxItem type = (ComboboxItem) searchItem.getItemProperty("type").getValue();
					String typeStr = null;
					if(type != null && type.getValue() != null){
						typeStr = type.getValue().toString();
					}
					
					paginationBar.search(id, name, typeStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(BTN_REFRESH_ID.equals(event.getButton().getId())){
			searchFieldGroup.clear();
			paginationBar.search(null, null, null);
		}else if(BTN_VIEW_ID.equals(event.getButton().getId())){
			Group currentRow = (Group) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(GroupForUserView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
		}else if(BTN_EDIT_ID.equals(event.getButton().getId())){
			Group currentRow = (Group) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(GroupDetailView.VIEW_NAME+"/"+Constants.ACTION_EDIT+"/"+currentRow.getId());
		}else if(BTN_DEL_ID.equals(event.getButton().getId())){
			Group currentRow = (Group) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(GroupDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getId());
		}else if(BTN_ADDNEW_ID.equals(event.getButton().getId())){
			UI.getCurrent().getNavigator().navigateTo(GroupDetailView.VIEW_NAME+"/"+Constants.ACTION_ADD);
		}else if(BTN_PHAN_NHOM_ID.equals(event.getButton().getId())){
			Group currentRow = (Group) event.getButton().getData();
	        UI.getCurrent().getNavigator().navigateTo(GroupForUserView.VIEW_NAME+"/"+Constants.ACTION_PHAN_NHOM+"/"+currentRow.getId());
		}
	}

}

package com.eform.ui.view.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.workflow.ITrangThai;
import com.eform.common.workflow.processinstance.ETrangThai;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.custom.PageTitleLayout;
import com.eform.ui.custom.StatusCommentLayout;
import com.eform.ui.custom.paging.InstanceTreePaginationBar;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = MyProcessInstancesTreeTableView.VIEW_NAME)
@PreserveOnRefresh
public class MyProcessInstancesTreeTableView extends VerticalLayout implements View, ClickListener{

	private static final long serialVersionUID = 1L;
	
	private final int WIDTH_FULL = 100;

	public static final String VIEW_NAME = "my-process-instances";
	static String MAIN_TITLE;
	private static String SMALL_TITLE;

	@Autowired
	private HistoryService historyService;
	@Autowired
	private RuntimeService runtimeService;
	
	private InstanceTreePaginationBar paginationBar;
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor messageSourceAccessor;
	
	private TreeTable treeTable = new TreeTable();
	
	@PostConstruct
    void init() {
		 VaadinSession.getCurrent().setLocale(new Locale("en", "US"));
		messageSourceAccessor = new MessageSourceAccessor(messageSource, VaadinSession.getCurrent().getLocale());
		MAIN_TITLE = messageSourceAccessor.getMessage(Messages.UI_VIEW_PROCESSINSTANCE_TITLE);
		SMALL_TITLE = messageSourceAccessor.getMessage(Messages.UI_VIEW_PROCESSINSTANCE_TITLE_SMALL);
		treeTable = new TreeTable();
		treeTable.addStyleName(ExplorerLayout.DATA_TABLE);
		treeTable.setSizeFull();
		treeTable.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		
//		treeTable.addContainerProperty("id", String.class, "");
		treeTable.addContainerProperty("code", String.class, "");
		treeTable.addContainerProperty("name", String.class, "");
		treeTable.addContainerProperty("desc", String.class, "");
		treeTable.addContainerProperty("startTime", String.class, "");
		treeTable.addContainerProperty("endTime", String.class, "");
		treeTable.addContainerProperty("action", HorizontalLayout.class, null);
//		treeTable.setColumnHeader("id", "Mã");
		treeTable.setColumnHeader("code", "Mã");
		treeTable.setColumnHeader("name", "Tên");
		treeTable.setColumnHeader("desc", "Ghi chú");
		treeTable.setColumnHeader("startTime", "Thời gian bắt đầu");
		treeTable.setColumnHeader("endTime", "Thời gian kết thúc");
		treeTable.setColumnHeader("action", "Thao tác");
		treeTable.setRowHeaderMode(RowHeaderMode.INDEX);
		
		treeTable.setCellStyleGenerator(new TreeTable.CellStyleGenerator(){
			private static final long serialVersionUID = 1L;

			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if(itemId != null && itemId instanceof String){
					if(itemId != null){
						ProcessInstance proInst = runtimeService.createProcessInstanceQuery().processInstanceId((String)itemId).singleResult();
						if(proInst != null && !proInst.isSuspended()){
							return ETrangThai.DANG_SU_DUNG.getStyleName();
						}
						
					}
				}
				return ETrangThai.NGUNG_SU_DUNG.getStyleName();
			}
			
		});
		
		paginationBar = new InstanceTreePaginationBar(treeTable, this);
		
		initMainTitle();
		initDataContent();
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSourceAccessor.getMessage(Messages.UI_VIEW_HOME));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new PageTitleLayout(MAIN_TITLE, SMALL_TITLE, false));
	}
	
	public void initDataContent(){
		
		CssLayout dataWrap = new CssLayout();
		dataWrap.addStyleName(ExplorerLayout.DATA_WRAP);
		dataWrap.setWidth(WIDTH_FULL, Unit.PERCENTAGE);
		addComponent(dataWrap);
		
		List<ITrangThai> trangThaiList = new ArrayList<ITrangThai>();
		trangThaiList.add(ETrangThai.DANG_SU_DUNG);
		trangThaiList.add(ETrangThai.NGUNG_SU_DUNG);
		if(trangThaiList != null && !trangThaiList.isEmpty()){
			dataWrap.addComponent(new StatusCommentLayout(trangThaiList));
		}
		
		dataWrap.addComponent(treeTable);
		dataWrap.addComponent(paginationBar);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(InstanceTreePaginationBar.BTN_VIEW_ID.equals(event.getButton().getId())){
			HistoricProcessInstance currentRow = (HistoricProcessInstance) button.getData();
			UI.getCurrent().getNavigator().navigateTo(MyProcessInstanceDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
		}else if(InstanceTreePaginationBar.BTN_DELETE_ID.equals(event.getButton().getId())){
			
			HistoricProcessInstance currentRow = (HistoricProcessInstance) button.getData();
			UI.getCurrent().getNavigator().navigateTo(MyProcessInstanceDetailView.VIEW_NAME+"/"+Constants.ACTION_DEL+"/"+currentRow.getId());
		}else if(InstanceTreePaginationBar.BTN_APPROVE_ID.equals(event.getButton().getId())){
			HistoricProcessInstance currentRow = (HistoricProcessInstance) button.getData();
			UI.getCurrent().getNavigator().navigateTo(MyProcessInstanceDetailView.VIEW_NAME+"/"+Constants.ACTION_APPROVE+"/"+currentRow.getId());
		}else if(InstanceTreePaginationBar.BTN_REFUSE_ID.equals(event.getButton().getId())){
			HistoricProcessInstance currentRow = (HistoricProcessInstance) button.getData();
			UI.getCurrent().getNavigator().navigateTo(MyProcessInstanceDetailView.VIEW_NAME+"/"+Constants.ACTION_REFUSE+"/"+currentRow.getId());
		}
	}
	
}

package com.eform.ui.view.report.type;

import java.io.Serializable;

import com.eform.model.entities.TruongThongTin;

public class FunctionObject implements Serializable{
	private TruongThongTin truongThongTin;
	private Long function;
	private String path;
	private String aliasName;
	
	public TruongThongTin getTruongThongTin() {
		return truongThongTin;
	}
	public void setTruongThongTin(TruongThongTin truongThongTin) {
		this.truongThongTin = truongThongTin;
	}
	public Long getFunction() {
		return function;
	}
	public void setFunction(Long function) {
		this.function = function;
	}
	public String getAliasName() {
		return aliasName;
	}
	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
}

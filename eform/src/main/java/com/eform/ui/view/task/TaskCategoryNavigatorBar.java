package com.eform.ui.view.task;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.task.IdentityLink;

import com.eform.common.ExplorerLayout;
import com.eform.common.utils.CommonUtils;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class TaskCategoryNavigatorBar extends CssLayout implements ClickListener{
	private static final long serialVersionUID = -2839634219714835627L;
	
	public static final String BTN_INBOX_ID = "btnInbox";
	public static final String BTN_MY_TASK_ID = "btnMyTask";
	public static final String BTN_QUEUED_ID = "btnQueued";
	public static final String BTN_INVOLED_ID = "btnInvoled";
	
	private TaskService taskService;
	protected IdentityService identityService;
	private String userId;
	private List<String> groupIds;
	private String btnActiveId;
	private String title;
	private String smallTitle;
	
	public TaskCategoryNavigatorBar(TaskService taskService, IdentityService identityService, String title, String smallTitle, String btnActiveId, String userId){
		init(taskService, identityService, title,smallTitle, btnActiveId, userId);
	}
	
	public void init(TaskService taskService, IdentityService identityService, String title,String smallTitle, String btnActiveId, String userId){
		addStyleName(ExplorerLayout.TASK_NAV_WRAP);
		setWidth("100%");
		this.smallTitle = smallTitle;
		this.taskService = taskService;
		this.userId = userId;
		this.identityService = identityService;
		this.btnActiveId = btnActiveId;
		this.title = title;
		
		List<Group> groups = identityService.createGroupQuery().groupMember(CommonUtils.getUserLogedIn()).list();
		if(groups != null && !groups.isEmpty()){
			groupIds = new ArrayList<String>();
			for(Group g: groups){
				groupIds.add(g.getId());
			}
		}
		initContent();
	}
	
	public void initContent(){
		//count
		long inboxCount = taskService.createTaskQuery().taskAssignee(userId).count();
		long queueCount = 0L;
		if(groupIds != null && !groupIds.isEmpty()){
			queueCount = taskService.createTaskQuery().taskCandidateGroupIn(groupIds).count();
		}
		long mytaskCount = taskService.createTaskQuery().taskOwner(userId).count();
		long involesCount = taskService.createTaskQuery().taskInvolvedUser(userId).count();

		if(title != null && smallTitle != null && !smallTitle.isEmpty()){
			title += "<span class='"+ExplorerLayout.TASK_NAV_SMALL_TITLE+"'>"+smallTitle+"</span>";
		}
		
		Label lblTitle = new Label(title);
		lblTitle.addStyleName(ExplorerLayout.TASK_NAV_TITLE);
		lblTitle.setContentMode(ContentMode.HTML);
		addComponent(lblTitle);
		
		CssLayout btnContainer = new CssLayout();
		btnContainer.addStyleName(ExplorerLayout.TASK_NAV_BTN_CONTAINER);
		addComponent(btnContainer);
		
		btnContainer.addComponent(createButton("Inbox", FontAwesome.ENVELOPE_O, inboxCount, BTN_INBOX_ID, BTN_INBOX_ID.equals(btnActiveId)));
		btnContainer.addComponent(createButton("My task", FontAwesome.USER, mytaskCount, BTN_MY_TASK_ID, BTN_MY_TASK_ID.equals(btnActiveId)));
		btnContainer.addComponent(createButton("Queue", FontAwesome.USERS, queueCount, BTN_QUEUED_ID, BTN_QUEUED_ID.equals(btnActiveId)));
		btnContainer.addComponent(createButton("Involed", FontAwesome.USER_PLUS, involesCount, BTN_INVOLED_ID, BTN_INVOLED_ID.equals(btnActiveId)));
	}
	
	public CssLayout createButton(String caption, FontAwesome icon, long count, String id, boolean active){
		CssLayout btnWrap = new CssLayout();
		btnWrap.addStyleName(ExplorerLayout.TASK_NAV_BTN_WRAP);
		
		Button btn = new Button();
		btn.addStyleName(ExplorerLayout.TASK_NAV_BTN);
		btn.setDescription(caption);
		btn.setIcon(icon);
		btn.setId(id);
		btn.addClickListener(this);
		
		if(active){
			btn.addStyleName(ExplorerLayout.TASK_NAV_BTN_ACTIVE);
		}
		
		btnWrap.addComponent(btn);
		Label lbl = new Label(count + "");
		lbl.addStyleName(ExplorerLayout.TASK_NAV_NUMBER);
		if(count == 0){
			lbl.addStyleName(ExplorerLayout.TASK_NAV_NUMBER_ZERO);
		}
		btnWrap.addComponent(lbl);
		
		return btnWrap;
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_INBOX_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(InboxView.VIEW_NAME);
		}else if(BTN_MY_TASK_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(MyTaskView.VIEW_NAME);
		}else if(BTN_QUEUED_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(QueueView.VIEW_NAME);
		}else if(BTN_INVOLED_ID.equals(button.getId())){
			UI.getCurrent().getNavigator().navigateTo(InvoledView.VIEW_NAME);
		}
	}
}

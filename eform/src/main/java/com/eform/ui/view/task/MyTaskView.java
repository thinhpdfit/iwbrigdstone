package com.eform.ui.view.task;

import java.util.List;

import javax.annotation.PostConstruct;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.Messages;
import com.eform.common.ThamSo;
import com.eform.common.type.PageBreadcrumbInfo;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.TaskUtils;
import com.eform.model.entities.HtThamSo;
import com.eform.service.HtThamSoService;
import com.eform.ui.custom.PageBreadcrumbLayout;
import com.eform.ui.view.dashboard.DashboardView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = MyTaskView.VIEW_NAME)
public class MyTaskView extends VerticalLayout implements View, ClickListener{
	
	private static final long serialVersionUID = 1L;

	public static final String VIEW_NAME = "my-task";
	public static final String MAIN_TITLE = "Công việc";
	private static final String SMALL_TITLE = "My task";
	public static final String BTN_DETAIL_ID = "detail";
	@Autowired
	private IdentityService identityService;
	
	@Autowired
	private TaskService taskService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private HtThamSoService htThamSoService;
	
	private String[] colors;
	@Autowired
	private MessageSource messageSource;
	private CssLayout taskListWrap = new CssLayout();
	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();
	private static final String BTN_SEARCH_ID = "timKiem";
	
	@PostConstruct
    void init() {
		HtThamSo color = htThamSoService.getHtThamSoFind(ThamSo.COLOR_LIST);
		if(color != null && color.getGiaTri() != null){
			colors = color.getGiaTri().split(",");
		}
		fieldGroup.setItemDataSource(itemValue);
		initMainTitle();
		initContent();
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
	}
	
	public void initMainTitle(){
	    PageBreadcrumbInfo home = new PageBreadcrumbInfo();
		home.setTitle(messageSource.getMessage(Messages.UI_VIEW_HOME, null, VaadinSession.getCurrent().getLocale()));
		home.setViewName(DashboardView.VIEW_NAME);
		
		PageBreadcrumbInfo current = new PageBreadcrumbInfo();
		current.setTitle(MAIN_TITLE);
		current.setViewName(VIEW_NAME);		
				
		addComponent(new PageBreadcrumbLayout(home, current));
		addComponent(new TaskCategoryNavigatorBar(taskService, identityService, MAIN_TITLE,SMALL_TITLE, TaskCategoryNavigatorBar.BTN_MY_TASK_ID, CommonUtils.getUserLogedIn()));
	}
	
	public void initContent(){
		List<Task> taskList = taskService.createTaskQuery().taskOwner(CommonUtils.getUserLogedIn()).active().orderByTaskPriority().desc().orderByTaskCreateTime().desc().list();
		CssLayout container = TaskUtils.buildTaskListByProcess(taskList, repositoryService, colors, this, BTN_DETAIL_ID);
		if(container != null){
			CssLayout searchForm = new CssLayout();
			searchForm.setWidth("100%");
			searchForm.addStyleName("search-task-form");
			
			TextField txtSearch = new TextField();
			txtSearch.setNullRepresentation("");
			txtSearch.setNullSettingAllowed(true);
			searchForm.addComponent(txtSearch);
			itemValue.addItemProperty("searchQuery", new ObjectProperty<String>(null, String.class));
			fieldGroup.bind(txtSearch, "searchQuery");
			
			Button btnSearch = new Button(FontAwesome.SEARCH.getHtml());
			btnSearch.setId(BTN_SEARCH_ID);
			btnSearch.setHtmlContentAllowed(true);
			btnSearch.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
			btnSearch.setClickShortcut(KeyCode.ENTER);
			btnSearch.setDescription("Nhấn ENTER");
			btnSearch.addClickListener(this);
			searchForm.addComponent(btnSearch); 
			addComponent(searchForm);
			
			taskListWrap.addComponent(container);
			addComponent(taskListWrap);
		}else{
			Label lbl = new Label("Không có công việc nào");
			lbl.addStyleName(ExplorerLayout.TASK_LIST_EMPTY);
			addComponent(lbl);
		}
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(BTN_DETAIL_ID.equals(event.getButton().getId())){
			Task currentRow = (Task) button.getData();
			UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+currentRow.getId());
		}else if(BTN_SEARCH_ID.equals(event.getButton().getId())){
			try {
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					Object value = itemValue.getItemProperty("searchQuery").getValue();
					String searchQuery =  null;
					if(value != null){
						searchQuery = value.toString().trim();
					}
					List<Task> taskList = null;
					if(searchQuery != null && !searchQuery.trim().isEmpty()){
						taskList = taskService.createTaskQuery().taskOwner(CommonUtils.getUserLogedIn()).active().taskNameLike("%"+searchQuery+"%").orderByTaskPriority().desc().orderByTaskCreateTime().desc().list();
					}else{
						taskList = taskService.createTaskQuery().taskOwner(CommonUtils.getUserLogedIn()).active().orderByTaskPriority().desc().orderByTaskCreateTime().desc().list();
					}
					taskListWrap.removeAllComponents();
					if(taskList != null && !taskList.isEmpty()){
						CssLayout container = TaskUtils.buildTaskListByProcess(taskList, repositoryService, colors, this, BTN_DETAIL_ID);
						taskListWrap.addComponent(container);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}

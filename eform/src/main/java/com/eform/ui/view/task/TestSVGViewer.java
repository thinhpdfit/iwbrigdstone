package com.eform.ui.view.task;

import java.io.InputStream;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.GraphicInfo;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;

import com.eform.common.utils.XmlUtil;
import com.eform.ui.custom.paging.BieuMauPaginationBar;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = TestSVGViewer.VIEW_NAME)
public class TestSVGViewer extends VerticalLayout implements View{
	
	private static final long serialVersionUID = 1L;
	

	public static final String VIEW_NAME = "test-svg";
	private static final String MAIN_TITLE = "Biểu mẫu";
	private static final String SMALL_TITLE = "";
	String processId;
	String instanceId;
	
	@Autowired
	private RepositoryService repositoryService;

	
	private BieuMauPaginationBar paginationBar;
	
	@PostConstruct
    void init() {
		
    }
	
	@Override
	public void enter(ViewChangeEvent event) {
		UI.getCurrent().getPage().setTitle(MAIN_TITLE);
		try {
			UI.getCurrent().getPage().setTitle(MAIN_TITLE);
			Object params = event.getParameters();
			if (params != null) {
				Pattern p = Pattern.compile("([a-zA-z0-9-_:]+)/([a-zA-z0-9-_]+)");
				Matcher m = p.matcher(params.toString());
				if (m.matches()) {
					processId = m.group(1);
					instanceId = m.group(2);

					ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
							.processDefinitionId(processId).singleResult();

					final InputStream definitionStream = repositoryService.getResourceAsStream(
							processDefinition.getDeploymentId(), processDefinition.getResourceName());
					XMLInputFactory xif = XmlUtil.createSafeXmlInputFactory();
					XMLStreamReader xtr = xif.createXMLStreamReader(definitionStream);
					BpmnModel bpmnModel = new BpmnXMLConverter().convertToBpmnModel(xtr);

					if (!bpmnModel.getFlowLocationMap().isEmpty()) {

						int maxY = 0;
						for (String key : bpmnModel.getLocationMap().keySet()) {
							GraphicInfo graphicInfo = bpmnModel.getGraphicInfo(key);
							double elementY = graphicInfo.getY() + graphicInfo.getHeight();
							if (maxY < elementY) {
								maxY = (int) elementY;
							}
						}

						CssLayout container = new CssLayout();
						container.setWidth("100%");
						addComponent(container);

						String host = UI.getCurrent().getPage().getLocation().getHost();
						String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
						int port = UI.getCurrent().getPage().getLocation().getPort();
						String scheme = UI.getCurrent().getPage().getLocation().getScheme();
						URL url = new URL(scheme, host, port,
								contextPath + "/diagram-viewer/index.html?processDefinitionId=" + processId
										+ "&processInstanceId=" + instanceId);
						Embedded browserPanel = new Embedded("", new ExternalResource(url));
						browserPanel.setType(Embedded.TYPE_BROWSER);
						browserPanel.setWidth("100%");
						browserPanel.setHeight(maxY + 300 + "px");
						container.addComponent(browserPanel);
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	public void initMainTitle(){
		
		
	}
	
	

}

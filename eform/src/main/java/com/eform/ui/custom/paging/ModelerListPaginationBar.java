package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;

import com.vaadin.ui.Button.ClickListener;
import com.eform.common.SpringApplicationContext;
import com.eform.common.ThamSo;
import com.eform.common.utils.ModelerUtils;
import com.eform.model.entities.HtThamSo;
import com.eform.service.HtThamSoService;
import com.eform.ui.custom.paging.base.ShowMorePaginationBar;
import com.vaadin.ui.Component;
import com.vaadin.ui.Layout;

public class ModelerListPaginationBar extends ShowMorePaginationBar<Model> {

	private RepositoryService repositoryService;
	private String name;
	private String category;
	private  ClickListener clickListener;
	private HtThamSoService htThamSoService;
	private String [] colors;
	Map<Long, Boolean> mapQuyen;
	
	public ModelerListPaginationBar(Layout wrapper, RepositoryService repositoryService,  ClickListener clickListener, Map<Long, Boolean> mapQuyen) {
		super(wrapper, ThamSo.APP_PAGE_SIZE);
		this.repositoryService = repositoryService;
		this.clickListener = clickListener;
		this.mapQuyen = mapQuyen;
		if(htThamSoService == null){
			htThamSoService = SpringApplicationContext.getApplicationContext().getBean(HtThamSoService.class);
		}
		HtThamSo htThamSo = htThamSoService.getHtThamSoFind(ThamSo.COLOR_LIST);
		if(htThamSo != null && htThamSo.getGiaTri() != null){
			colors = htThamSo.getGiaTri().split(",");
		}

		appendData();
	}

	@Override
	public long getRowCount() {
		Long count = 0L;
		if(repositoryService != null){
			if(name == null){
				name = "";
			}
			if(category != null){
				count = repositoryService.createModelQuery().modelNameLike("%"+name+"%").modelCategoryLike("%"+category+"%").count();
			}else{
				count = repositoryService.createModelQuery().modelNameLike("%"+name+"%").count();
			}
		}
		return count;
	}

	@Override
	public List<Model> getMoreDataList(int firstResult, int maxResults) {
		List<Model> modelList = new ArrayList<Model>();
		if(repositoryService != null){
			if(name == null){
				name = "";
			}
			if(category != null){
				modelList = repositoryService.createModelQuery().modelNameLike("%"+name+"%").modelCategoryLike("%"+category+"%").orderByCreateTime().desc().listPage(firstResult, maxResults);
			}else{
				modelList = repositoryService.createModelQuery().modelNameLike("%"+name+"%").orderByCreateTime().desc().listPage(firstResult, maxResults);
			}
		}
		return modelList;
	}
	
	public void search(String name, 
			String category){
		this.name = name;
		this.category = category;
		currentPage = 1;
		reloadData();
	}
	
	@Override
	public Component builItem(Model model, int i) {
		return ModelerUtils.buildItem(model, i, clickListener, repositoryService, colors, mapQuyen);
	}

}

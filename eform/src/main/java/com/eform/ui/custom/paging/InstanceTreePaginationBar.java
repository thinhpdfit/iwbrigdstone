package com.eform.ui.custom.paging;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.SpringApplicationContext;
import com.eform.common.utils.CommonUtils;
import com.eform.model.entities.ProcinstInfo;
import com.eform.service.ProcinstInfoService;
import com.eform.ui.custom.paging.base.PaginationBarList;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.themes.Reindeer;

public class InstanceTreePaginationBar extends PaginationBarList<HistoricProcessInstance>{

	private HistoryService historyService;
	private RuntimeService runtimeService;
	private RepositoryService repositoryService;
	private ProcinstInfoService procinstInfoService;
	private TreeTable treeTable;
	
	public static final String BTN_VIEW_ID = "xem";
	public static final String BTN_DELETE_ID = "delete";
	public static final String BTN_APPROVE_ID = "suDung";
	public static final String BTN_REFUSE_ID = "ngungSuDung";
	
	private ClickListener clickListener;
	
	public InstanceTreePaginationBar(TreeTable treeTable, ClickListener clickListener) {
		this.treeTable = treeTable;
		this.clickListener = clickListener;
		historyService = SpringApplicationContext.getApplicationContext().getBean(HistoryService.class);
		runtimeService = SpringApplicationContext.getApplicationContext().getBean(RuntimeService.class);
		repositoryService = SpringApplicationContext.getApplicationContext().getBean(RepositoryService.class);
		procinstInfoService = SpringApplicationContext.getApplicationContext().getBean(ProcinstInfoService.class);
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(historyService != null){
			return historyService
		      .createHistoricProcessInstanceQuery().excludeSubprocesses(true)
		      .startedBy(CommonUtils.getUserLogedIn()).count();
		}
		return 0L;
	}

	@Override
	public List<HistoricProcessInstance> getDataList(int firstResult, int maxResults) {
		if(historyService != null){
			return historyService
		      .createHistoricProcessInstanceQuery().excludeSubprocesses(true)
		      .startedBy(CommonUtils.getUserLogedIn()).orderByProcessInstanceStartTime().desc().listPage(firstResult, maxResults);
		}
		return new ArrayList<HistoricProcessInstance>();
	}
	
	public void search(){
		currentPage = 1;
		reloadData();
	}
	
	public List<HistoricProcessInstance> getAllSubProcess(List<HistoricProcessInstance> dataList){
		if(dataList != null){
			List<HistoricProcessInstance> dataListTmp = new ArrayList();
			dataListTmp.addAll(dataList);
			while(!dataList.isEmpty()){
				HistoricProcessInstance loop = dataList.remove(0);
				List<HistoricProcessInstance>  subList = historyService.createHistoricProcessInstanceQuery().superProcessInstanceId(loop.getId()).list();
				if(subList != null){
					dataListTmp.addAll(subList);
					dataList.addAll(subList);
				}
			}
			return dataListTmp;
		}
		return null;
	}

	@Override
	public void reloadComponent() {
		treeTable.removeAllItems();
		List<HistoricProcessInstance> dataListTmp = this.getCurrentDataList();
		
		if(dataListTmp != null){
			
			List<HistoricProcessInstance> dataList = getAllSubProcess(dataListTmp);
			
			for(HistoricProcessInstance itemId : dataList){
				HorizontalLayout actionWrap = new HorizontalLayout();
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
				
				Button btnView = new Button();
		        btnView.setId(BTN_VIEW_ID);
		        btnView.setDescription("Xem");
		        btnView.setIcon(FontAwesome.EYE);
		        btnView.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnView.addClickListener(clickListener);
		        btnView.setData(itemId);
		        btnView.addStyleName(Reindeer.BUTTON_LINK);
		        actionWrap.addComponent(btnView);
		        
		        if(itemId != null && itemId instanceof HistoricProcessInstance){
					HistoricProcessInstance current = (HistoricProcessInstance) itemId;
					if(current.getEndTime() == null){
				        Button btnDelete = new Button();
				        btnDelete.setDescription("Xóa");
				        btnDelete.setId(BTN_DELETE_ID);
				        btnDelete.setIcon(FontAwesome.REMOVE);
				        btnDelete.addStyleName(ExplorerLayout.BUTTON_ACTION);
				        btnDelete.addClickListener(clickListener);
				        btnDelete.setData(itemId);
				        btnDelete.addStyleName(Reindeer.BUTTON_LINK);
				        actionWrap.addComponent(btnDelete);
					}
					if(current.getId() != null){
						ProcessInstance proInst = runtimeService.createProcessInstanceQuery().processInstanceId(current.getId()).singleResult();
						if(proInst != null && !proInst.isSuspended()){
							Button btnSuspend = new Button();
							btnSuspend.setDescription("Ngừng sử dụng");
							btnSuspend.setId(BTN_REFUSE_ID);
							btnSuspend.setIcon(FontAwesome.LEVEL_DOWN);
							btnSuspend.addStyleName(ExplorerLayout.BUTTON_ACTION);
							btnSuspend.addClickListener(clickListener);
							btnSuspend.setData(itemId);
							btnSuspend.addStyleName(Reindeer.BUTTON_LINK);
					        actionWrap.addComponent(btnSuspend);
						}else if(proInst != null && proInst.isSuspended()){
							Button btnActive = new Button();
							btnActive.setDescription("Sử dụng");
							btnActive.setId(BTN_APPROVE_ID);
							btnActive.setIcon(FontAwesome.CHECK);
							btnActive.addStyleName(ExplorerLayout.BUTTON_ACTION);
							btnActive.addClickListener(clickListener);
							btnActive.setData(itemId);
							btnActive.addStyleName(Reindeer.BUTTON_LINK);
					        actionWrap.addComponent(btnActive);
						}
					}
		        }
		        
		        String proName = "#Không tên";
		        if(itemId.getProcessDefinitionId() != null){
					ProcessDefinition pro = repositoryService.createProcessDefinitionQuery().processDefinitionId(itemId.getProcessDefinitionId()).singleResult();
					if(pro != null && pro.getName() != null){
						proName = pro.getName();
					}
				}
		        
		        String startTime = "";
		        if(itemId.getStartTime() != null){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
					startTime = df.format(itemId.getStartTime());
				}
		        String endTime = "Đang thực hiện";
		        if(itemId.getEndTime() != null){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
					endTime = df.format(itemId.getEndTime());
				}
		        
		        ProcinstInfo procinstInfo = procinstInfoService.findByProcinstIdAndInfoKey(itemId.getId(), "DESC");
		        String note = "";
		        String code = "";
		        if(procinstInfo != null && procinstInfo.getInfo() != null){
		        	note = procinstInfo.getInfo();
		        }
		        if(procinstInfo != null && procinstInfo.getCode() != null){
		        	code = procinstInfo.getCode();
		        }else {
		        	code = itemId.getId();
		        }
//		        treeTable.addItem(new Object[]{itemId.getId(), code, proName, note, startTime, endTime, actionWrap}, itemId.getId());
		        treeTable.addItem(new Object[]{code, proName, note, startTime, endTime, actionWrap}, itemId.getId());
			}
			
			for(HistoricProcessInstance itemId : dataList){
				treeTable.setParent(itemId.getId(), itemId.getSuperProcessInstanceId());
			}
			
			for (Object itemId: treeTable.getContainerDataSource().getItemIds()) {
				treeTable.setCollapsed(itemId, false);
			    if (!treeTable.hasChildren(itemId)){
			    	treeTable.setChildrenAllowed(itemId, false);
			    }
			}
		}
	}
		
	

}

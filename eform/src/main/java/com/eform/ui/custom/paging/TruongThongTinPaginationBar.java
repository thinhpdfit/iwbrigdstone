package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.TruongThongTinService;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class TruongThongTinPaginationBar extends PaginationBar<TruongThongTin> {

	private TruongThongTinService truongThongTinService;
	private String ma;
	private String ten;
	private List<Long> trangThai;
	private LoaiDanhMuc loaiDanhMuc;
	private List<Long> kieuDuLieu;
	
	public TruongThongTinPaginationBar(BeanItemContainer<TruongThongTin> container, TruongThongTinService truongThongTinService) {
		super(container);
		this.truongThongTinService = truongThongTinService;
		reloadData();
	}
	
	public TruongThongTinPaginationBar(BeanItemContainer<TruongThongTin> container, TruongThongTinService truongThongTinService, String pageSizePar) {
		super(container, pageSizePar);
		this.truongThongTinService = truongThongTinService;
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(truongThongTinService != null){
			return truongThongTinService.getTruongThongTinFind(ma, ten, trangThai, loaiDanhMuc, kieuDuLieu, null);
		}
		return 0L;
	}

	@Override
	public List<TruongThongTin> getDataList(int firstResult, int maxResults) {
		if(truongThongTinService != null){
			return truongThongTinService.getTruongThongTinFind(ma, ten, trangThai, loaiDanhMuc, kieuDuLieu, null, firstResult, maxResults);
		}
		return new ArrayList<TruongThongTin>();
	}
	
	public void search(String ma, String ten, List<Long> trangThai,
			LoaiDanhMuc loaiDanhMuc, List<Long> kieuDuLieu){
		this.ma = ma;
		this.ten = ten;
		this.trangThai =trangThai;
		this.loaiDanhMuc = loaiDanhMuc;
		this.kieuDuLieu = kieuDuLieu;
		currentPage = 1;
		reloadData();
	}

}

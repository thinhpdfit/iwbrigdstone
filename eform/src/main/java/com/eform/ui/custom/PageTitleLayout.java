package com.eform.ui.custom;

import com.eform.common.ExplorerLayout;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;

public class PageTitleLayout extends CssLayout {
	
	private String title;
	private String smallTitle;
	private boolean hasSearchForm = false;
	private Component component[];
	
	public PageTitleLayout(String title, String smallTitle){
		init(title, smallTitle, false);
	}
	
	public PageTitleLayout(String title, String smallTitle, boolean hasSearchForm){
		init(title, smallTitle, hasSearchForm);
	}
	
	public PageTitleLayout(String title, String smallTitle, boolean hasSearchForm, Component... component){
		init(title, smallTitle, hasSearchForm, component);
	}
	
	public void init(String title, String smallTitle, boolean hasSearchForm, Component... component){
		setWidth("100%");
		setStyleName(ExplorerLayout.PAGE_TITLE_WRAP);
		this.title = title;
		this.smallTitle = smallTitle;
		this.hasSearchForm = hasSearchForm;
		this.component = component;
		initTitleContent();
	}
	
	public void init(String title, String smallTitle, boolean hasSearchForm){
		setWidth("100%");
		setStyleName(ExplorerLayout.PAGE_TITLE_WRAP);
		this.title = title;
		this.smallTitle = smallTitle;
		this.hasSearchForm = hasSearchForm;
		initTitleContent();
	}
	
	public void initTitleContent(){
		if(title != null && smallTitle != null && !smallTitle.isEmpty()){
			title += "<span class='"+ExplorerLayout.PAGE_SMALL_TITLE+"'>"+smallTitle+"</span>";
		}
		Label lblTitle = new Label(title);
		lblTitle.setContentMode(ContentMode.HTML);
		lblTitle.setStyleName(ExplorerLayout.PAGE_TITLE);
		addComponent(lblTitle);
		
		CssLayout btnActionWrap = new CssLayout();
		btnActionWrap.addStyleName(ExplorerLayout.PAGE_TITLE_ACTION_WRAP);
		addComponent(btnActionWrap);
		
		if(hasSearchForm){
			Button btnShowSearch = new Button("Tìm kiếm");
			btnShowSearch.setStyleName(ExplorerLayout.BTN_SHOW_SEARCH_FORM);
			btnShowSearch.setIcon(FontAwesome.SEARCH);
			btnActionWrap.addComponent(btnShowSearch);
		}
		if(component != null && component.length > 0){
			for(Component c :component){
				btnActionWrap.addComponent(c);
			}
		}
		
	}
	
}

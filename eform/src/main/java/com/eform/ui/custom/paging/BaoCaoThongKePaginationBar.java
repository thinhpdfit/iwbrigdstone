package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import com.eform.model.entities.BaoCaoThongKe;
import com.eform.model.entities.HtDonVi;
import com.eform.service.BaoCaoThongKeService;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class BaoCaoThongKePaginationBar extends PaginationBar<BaoCaoThongKe> {

	private BaoCaoThongKeService baoCaoThongKeService;
	private String ma;
	private String ten;
	private List<HtDonVi> htDonViList;
	private List<Long> trangThai;
	
	public BaoCaoThongKePaginationBar(BeanItemContainer<BaoCaoThongKe> container, BaoCaoThongKeService baoCaoThongKeService, List<HtDonVi> htDonViList) {
		super(container);
		this.baoCaoThongKeService = baoCaoThongKeService;
		this.htDonViList = htDonViList;
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(baoCaoThongKeService != null){
			return baoCaoThongKeService.getBaoCaoThongKeFind(ma, ten, htDonViList, trangThai, false);
		}
		return 0L;
	}

	@Override
	public List<BaoCaoThongKe> getDataList(int firstResult, int maxResults) {
		if(baoCaoThongKeService != null){
			return baoCaoThongKeService.getBaoCaoThongKeFind(ma, ten, htDonViList, trangThai, false, firstResult, maxResults);
		}
		return new ArrayList<BaoCaoThongKe>();
	}
	
	public void search(String ma, 
			String ten, 
			List<HtDonVi> htDonViList,
			List<Long> trangThai){
		this.ma = ma;
		this.ten = ten;
		this.trangThai =trangThai;
		this.htDonViList = htDonViList;
		currentPage = 1;
		reloadData();
	}

}

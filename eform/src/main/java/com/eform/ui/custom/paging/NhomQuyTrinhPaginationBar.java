package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.NhomQuyTrinh;
import com.eform.service.NhomQuyTrinhService;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class NhomQuyTrinhPaginationBar extends PaginationBar<NhomQuyTrinh> {
	
	private NhomQuyTrinhService nhomQuyTrinhService;
	private String ma;
	private String ten;
	private List<Long> trangThai;
	private NhomQuyTrinh nhomQuyTrinh;
	
	public NhomQuyTrinhPaginationBar(BeanItemContainer<NhomQuyTrinh> container, NhomQuyTrinhService nhomQuyTrinhService) {
		super(container);
		this.nhomQuyTrinhService = nhomQuyTrinhService;
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(nhomQuyTrinhService != null){
			return nhomQuyTrinhService.getNhomQuyTrinhFind(ma, ten, trangThai, nhomQuyTrinh);
		}
		return 0L;
	}

	@Override
	public List<NhomQuyTrinh> getDataList(int firstResult, int maxResults) {
		if(nhomQuyTrinhService != null){
			return nhomQuyTrinhService.getNhomQuyTrinhFind(ma, ten, trangThai, nhomQuyTrinh, firstResult, maxResults);
		}
		return new ArrayList<NhomQuyTrinh>();
	}
	
	public void search(String ma,
    		String ten, 
    		List<Long> trangThai, 
    		NhomQuyTrinh nhomQuyTrinh){
		this.ma = ma;
		this.ten = ten;
		this.trangThai =trangThai;
		this.nhomQuyTrinh = nhomQuyTrinh;
		currentPage = 1;
		reloadData();
	}

}

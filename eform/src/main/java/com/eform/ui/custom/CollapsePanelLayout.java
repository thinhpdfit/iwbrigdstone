package com.eform.ui.custom;

import com.eform.common.ExplorerLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;

public class CollapsePanelLayout extends CssLayout{
	
	private boolean showAll;
	
	public CollapsePanelLayout(){
		super();
		init(true);
	}
	
	public CollapsePanelLayout(boolean showAll){
		super();
		init(showAll);
	}
	
	public void init(boolean showAll){
		
		setWidth("100%");
		addStyleName(ExplorerLayout.COLLAPSE_PANEL_LAYOUT);
		this.showAll = showAll;
	}
	
	public void addCollapsePanelItem(String titleStr, Component content){
		CssLayout item = new CssLayout();
		item.setWidth("100%");
		item.addStyleName(ExplorerLayout.COLLAPSE_PANEL_ITEM);
		addComponent(item);
		
		CssLayout head = new CssLayout();
		head.setWidth("100%");
		head.addStyleName(ExplorerLayout.COLLAPSE_PANEL_ITEM_HEAD);
		item.addComponent(head);
		
		Label title = new Label(titleStr);
		title.addStyleName(ExplorerLayout.COLLAPSE_PANEL_ITEM_TITLE);
		head.addComponent(title);
		
		Label btnCollapse = new Label();
		btnCollapse.addStyleName(ExplorerLayout.COLLAPSE_PANEL_ITEM_BTN);
		head.addComponent(btnCollapse);
		
		CssLayout contentWrap = new CssLayout();
		contentWrap.setWidth("100%");
		contentWrap.addStyleName(ExplorerLayout.COLLAPSE_PANEL_ITEM_CONTENT);
		contentWrap.addComponent(content);
		item.addComponent(contentWrap);
		
	}
}

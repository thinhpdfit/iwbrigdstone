package com.eform.ui.custom;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.eform.common.Constants;
import com.eform.common.utils.FileUtils;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload.Receiver;

public class UploadReceive implements Receiver {
	private static final long serialVersionUID = -2029307352426460413L;
	private String fileName;
    private String mtype;
    private final String uploadPath;
    
    public UploadReceive(String uploadPath){
    	super();
    	this.uploadPath = uploadPath;
    }
    
    public OutputStream receiveUpload(String filename, String MIMEType) {
    	System.out.println("receiveUpload");
        fileName = filename;
        mtype = MIMEType;
        File file = null;
        FileOutputStream fos = null;
        
        try {
        	FileUtils.createIfNotExistFolder(uploadPath);
            file = new File(uploadPath + filename);
            fos = new FileOutputStream(file);
        } catch (final java.io.FileNotFoundException e) {
            new Notification("Thông báo",
                             "Không thể upload file này!",
                             Notification.Type.ERROR_MESSAGE)
                .show(Page.getCurrent());
            return null;
        }
        return fos;
    }

    public String getFileName() {
        return fileName;
    }

    public String getMimeType() {
        return mtype;
    }

}

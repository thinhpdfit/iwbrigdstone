package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import com.eform.model.entities.LoaiDanhMuc;
import com.eform.service.LoaiDanhMucService;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class LoaiDanhMucPaginationBar extends PaginationBar<LoaiDanhMuc> {

	private LoaiDanhMucService loaiDanhMucService;
	private String ma;
	private String ten;
	private List<Long> trangThai;
	private LoaiDanhMuc danhMuc;
	
	public LoaiDanhMucPaginationBar(BeanItemContainer<LoaiDanhMuc> container, LoaiDanhMucService loaiDanhMucService) {
		super(container);
		this.loaiDanhMucService = loaiDanhMucService;
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(loaiDanhMucService != null){
			return loaiDanhMucService.getLoaiDanhMucFind(ma, ten, trangThai, danhMuc);
		}
		return 0L;
	}

	@Override
	public List<LoaiDanhMuc> getDataList(int firstResult, int maxResults) {
		if(loaiDanhMucService != null){
			return loaiDanhMucService.getLoaiDanhMucFind(ma, ten, trangThai, danhMuc, firstResult, maxResults);
		}
		return new ArrayList();
	}
	
	public void search(String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc danhMuc){
		this.ma = ma;
		this.ten = ten;
		this.trangThai =trangThai;
		this.danhMuc = danhMuc;
		currentPage = 1;
		reloadData();
	}

}

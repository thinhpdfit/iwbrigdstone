package com.eform.ui.custom;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;

import com.aspose.words.Document;
import com.aspose.words.ImportFormatMode;
import com.eform.common.Constants;
import com.eform.common.CreateAsposeLic;
import com.eform.common.ExplorerLayout;
import com.eform.common.SpringApplicationContext;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.FileUtils;
import com.eform.common.utils.SignatureUtils;
import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;
import com.eform.model.entities.Signature;
import com.eform.service.FileManagementService;
import com.eform.service.FileVersionService;
import com.eform.service.SignatureService;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

public class UploadFileWindow extends Window implements ClickListener{

	public static final String BTN_UPLOAD_ID = "btn-upload";
	private static final String BUTTON_ADD_FILE_ID = "btn-add-file";
	private static final String BUTTON_EDIT_FILE_ID = "btn-edit-file";
	private static final String BTN_VIEW_ID = "btn-view";
	private static final String BTN_EDIT_ID = "btn-edit";
	private static final String BTN_EDIT_PROPERTIES_ID = "btn-edit-pop";
	private static final String BTN_CHECKOUT_ID = "btn-checkout";
	private static final String BTN_DELETE_ID = "btn-delete";
	private static final String BTN_SIGNATURE_ID = "btn-signature";
	private static final String BTN_RESTORE_ID = "btn-restore";
	private static final String BTN_DELETE_VERSION_ID = "btn-del-version";
	private static final String BTN_PREVIEW_ID = "btn-preview";
	
	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();
	private Long fileSize;
	private List<String> fileAllows;
	private CssLayout fileListWrap;
	private String filePath;
	private String fileExt;
	private Upload upload;
	private Label lblFileName;
	private TextField txtTitle;
	private TextArea txtMota;
	private Button editFile;
	private Button addFile;
	
	private String action;

	private FileManagement currentFileManagement;

	private SignatureService signatureService;
	private FileVersionService fileVersionService;
	private FileManagementService fileManagementService;
	
	private Window window;
	private Label fileTitle;
	private Label fileDesc;
	private Table tableVersion;
	
	private Table tableSignature;
	private Window signedWindow;
	
	private Map<String, Object> valueCurrent;
	protected IdentityService identityService;
	private final String uploadPath;
	
	private boolean readOnly;
	
	public UploadFileWindow(Long fileSize, List<String> fileAllows, CssLayout fileListWrap, String uploadPath, boolean readOnly){
		signatureService=SpringApplicationContext.getApplicationContext().getBean(SignatureService.class);
		fileVersionService=SpringApplicationContext.getApplicationContext().getBean(FileVersionService.class);
		fileManagementService=SpringApplicationContext.getApplicationContext().getBean(FileManagementService.class);
		identityService=SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
		this.uploadPath = uploadPath;
		setWidth(600, Unit.PIXELS);
		setHeight(320, Unit.PIXELS);
		setResizable(true);
		center();
		setCaption("Thông tin");
		setModal(true);
		
		this.readOnly = readOnly;
		this.fileSize = fileSize;
		this.fileAllows = fileAllows;
		this.fileListWrap = fileListWrap;
		
		fieldGroup.setItemDataSource(itemValue);
		init();
		initHisoryView();
		initSignatureView();
	}
	
	public void init(){
		FormLayout form = new FormLayout();
		form.setWidth("100%");
		form.setMargin(true);
		form.setSpacing(true);
		
		UploadSignatureInfo uploadInfo = new UploadSignatureInfo(fileSize, fileAllows, uploadPath);
		upload = new Upload("Chọn file", uploadInfo);
		upload.setButtonCaption("Chọn file");
		upload.setImmediate(true);
		upload.setStyleName(ExplorerLayout.BTN_UPLOAD);
		upload.addStartedListener(uploadInfo);
		upload.addFailedListener(uploadInfo);
		upload.addFinishedListener(uploadInfo);

		form.addComponent(upload);
		
		lblFileName = new Label();
		lblFileName.setVisible(false);
		form.addComponent(lblFileName);

		upload.addSucceededListener(new SucceededListener() {
			@Override
			public void uploadSucceeded(SucceededEvent event) {
				lblFileName.setVisible(true);
				upload.setVisible(false);
				lblFileName.setValue(event.getFilename());
				filePath = uploadInfo.getUploadedName();
				fileExt = FileUtils.getFileExtension(filePath);
			}
		});
		
		 txtTitle = new TextField("Tiêu đề");
		txtTitle.setRequired(true);
		txtTitle.setWidth("100%");
		txtTitle.setRequiredError("Không được để trống");
		txtTitle.setNullRepresentation("");
		form.addComponent(txtTitle);
		itemValue.addItemProperty("title", new ObjectProperty<String>(null, String.class));
		fieldGroup.bind(txtTitle, "title");
		
		txtMota = new TextArea("Mô tả");
		form.addComponent(txtMota);
		txtMota.setWidth("100%");
		txtMota.setNullRepresentation("");
		itemValue.addItemProperty("desc", new ObjectProperty<String>(null, String.class));
		fieldGroup.bind(txtMota, "desc");
		
		addFile = new Button("Thêm mới");
		addFile.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
		addFile.addClickListener(this);
		addFile.setId(BUTTON_ADD_FILE_ID);
		form.addComponent(addFile);
		
		editFile = new Button("Sửa");
		editFile.setStyleName(ExplorerLayout.BUTTON_SUCCESS);
		editFile.addClickListener(this);
		editFile.setId(BUTTON_EDIT_FILE_ID);
		editFile.setVisible(false);
		form.addComponent(editFile);
		
		setContent(form);
		
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			Button button = event.getButton();
			if(BTN_UPLOAD_ID.equals(button.getId())){
				fieldGroup.setReadOnly(false);
				addFile.setVisible(true);
				editFile.setVisible(false);
				upload.setVisible(true);
				lblFileName.setVisible(false);
				txtMota.setVisible(true);
				txtTitle.setVisible(true);
				UI.getCurrent().addWindow(this);
			}else if(BUTTON_ADD_FILE_ID.equals(button.getId())){
				if(fieldGroup.isValid()){
					fieldGroup.commit();
					Date date = new Date();
					
					String title = (String) itemValue.getItemProperty("title").getValue();
					String desc = (String) itemValue.getItemProperty("desc").getValue();

					FileManagement fileManager = new FileManagement();
					fileManager.setTieuDe(title);
					fileManager.setMoTa(desc);
					fileManager.setNguoiTao(CommonUtils.getUserLogedIn());
					fileManager.setNgayTao(new Timestamp(date.getTime()));
					
					FileVersion file = new FileVersion();
					
					file.setUploadPath(filePath);
					file.setDinhDang(fileExt);
					file.setNguoiCapNhat(CommonUtils.getUserLogedIn());
					file.setNgayCapNhat(new Timestamp(date.getTime()));
					file.setVersion(1L);
					file.setFileManagement(fileManager);
					
					fileVersionService.save(file, fileManager);
					
					TextField hiden = (TextField) fileListWrap.getData();
					List<FileManagement> fileList = (List<FileManagement>) hiden.getData();
					if(fileList == null){
						fileList = new ArrayList();
						
					}
					fileList.add(fileManager);
					hiden.setData(fileList);
					String value = "";
					for(FileManagement f: fileList){
						value+=f.getId()+",";
					}
					if(value != null && value.lastIndexOf(",") > 0){
						value = value.substring(0, value.lastIndexOf(","));
					}
					hiden.setValue(value);
					
					fileListWrap.addComponent(createFileItem(title, fileManager, this, false, readOnly));
					this.close();
					upload.setVisible(true);
					lblFileName.setVisible(false);
					fieldGroup.clear();
				}
			}else if(BTN_CHECKOUT_ID.equals(button.getId())){
				FileManagement file = (FileManagement) button.getData();
				FileVersion fileVersion = fileManagementService.getMaxVersion(file);
				boolean hasFile = false;
				if(fileVersion != null && fileVersion.getTrangThai() != null && fileVersion.getTrangThai().intValue() == 1
					&& fileVersion.getSignedPath() != null){
						File tempFile = new File(uploadPath+fileVersion.getSignedPath());
						if(tempFile.exists()){
							FileInputStream input = new FileInputStream(tempFile);
							StreamResource resourcetmp = FileUtils.getStreamResource(input, file.getTieuDe()+".pdf");
						    UI.getCurrent().getPage().open(resourcetmp, "checkout", true);
						    hasFile = true;
						}
						
					
				} 
					
				if(fileVersion != null && fileVersion.getUploadPath() != null && !hasFile){
					File tempFile = new File(uploadPath+fileVersion.getUploadPath());
					FileInputStream input = new FileInputStream(tempFile);
					StreamResource resourcetmp = FileUtils.getStreamResource(input, file.getTieuDe()+"."+fileVersion.getDinhDang());
				    UI.getCurrent().getPage().open(resourcetmp, "checkout", true);
				}
				
				
			}else if(BTN_EDIT_ID.equals(button.getId())){
				action = "EDIT";
				FileManagement file = (FileManagement) button.getData();
				
				VerticalLayout parent = (VerticalLayout) button.getParent();
				if(parent != null){
					valueCurrent = (Map<String, Object>) parent.getData();
				}
				
				currentFileManagement = file;
				fieldGroup.setReadOnly(false);
				addFile.setVisible(false);
				editFile.setVisible(true);
				txtMota.setVisible(true);
				txtTitle.setVisible(false);
			    upload.setVisible(true);
			    UI.getCurrent().addWindow(this);
			}else if(BTN_VIEW_ID.equals(button.getId())){
				FileManagement file = (FileManagement) button.getData();
				if(button.getParent() != null && button.getParent() instanceof VerticalLayout){
					VerticalLayout parent = (VerticalLayout) button.getParent();
					if(parent != null && parent.getData() != null && parent.getData() instanceof Map){
						Map<String, Object> data = (Map<String, Object>) parent.getData();
						if(data.get("POPUP") != null && data.get("POPUP") instanceof PopupView){
							PopupView popup = (PopupView) data.get("POPUP");
							popup.setPopupVisible(false);
						}
					}
				}
				updateHistoryView(file);
			    UI.getCurrent().addWindow(window);
			}else if(BTN_PREVIEW_ID.equals(button.getId())){
				try {
					FileManagement file = (FileManagement) button.getData();
					FileVersion fileVersion = fileManagementService.getMaxVersion(file);
					boolean hasFile = false;
					if(fileVersion != null && fileVersion.getTrangThai() != null && fileVersion.getTrangThai().intValue() == 1
						&& fileVersion.getSignedPath() != null){
							File tempFile = new File(uploadPath+fileVersion.getSignedPath());
							if(tempFile.exists()){
								FileInputStream input = new FileInputStream(tempFile);
								StreamResource resourcetmp = FileUtils.getStreamResource(input, file.getTieuDe()+".pdf");
								resourcetmp.setMIMEType("application/pdf");
							    UI.getCurrent().getPage().open(resourcetmp, "checkout", true);
							    hasFile = true;
							}
					} 
					if(fileVersion != null && fileVersion.getUploadPath() != null && !hasFile){
						File tempFile = new File(uploadPath+fileVersion.getUploadPath());
						FileInputStream input = new FileInputStream(tempFile);
						StreamResource resourcetmp = FileUtils.getStreamResource(input, file.getTieuDe()+"."+fileVersion.getDinhDang());
						resourcetmp.setMIMEType("application/pdf");
					    UI.getCurrent().getPage().open(resourcetmp, "checkout", true);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}else if(BTN_SIGNATURE_ID.equals(button.getId())){
				Date date = new Date();
				FileManagement file = (FileManagement) button.getData();
				FileVersion fileVersion = fileManagementService.getMaxVersion(file);
				if(signatureService.checkSigned(fileVersion, CommonUtils.getUserLogedIn())){
					Notification notf = new Notification("Thông báo", "Chữ ký của bạn đã tồn tại");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
				}else{
					SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
					String prefix = df.format(date)+"_";
					List<Signature> sigList = signatureService.getSignatureFind(fileVersion);
					int thuTuCurrent = 1;
					String pdfPath = "";
					
					if(sigList != null && !sigList.isEmpty()){
						thuTuCurrent = sigList.size()+1;
					}
					
					if(fileVersion != null && fileVersion.getTrangThai() != null
							&& fileVersion.getTrangThai().intValue() == 1
							&& fileVersion.getSignedPath() != null){
						pdfPath = fileVersion.getSignedPath();
					}else if(fileVersion != null ){
						
						if("pdf".equals(fileVersion.getDinhDang())){
							pdfPath = uploadPath + fileVersion.getUploadPath();
						}else{
							String docPath = uploadPath + fileVersion.getUploadPath();
							pdfPath = prefix + file.getTieuDe()+".pdf";
							FileUtils.createPdf(docPath, uploadPath + pdfPath);
						}
					}
					
					String signedPath = prefix + file.getTieuDe()+"signature"+thuTuCurrent+".pdf";
					
					User u = identityService.createUserQuery().userId(CommonUtils.getUserLogedIn()).singleResult();
					String userName = CommonUtils.getUserLogedIn();
					if(u!= null){
						userName = u.getFirstName()+" "+u.getLastName();
					}
					SignatureUtils.signPdf("/"+uploadPath + pdfPath, "/"+uploadPath + signedPath, userName, thuTuCurrent);
					FileUtils.removeFile(uploadPath + pdfPath);
					fileVersion.setTrangThai(1L);
					fileVersion.setSignedPath(signedPath);
					Signature signature = new Signature();
					signature.setFileVersion(fileVersion);
					signature.setGhiChu(null);
					signature.setNgayKy(new Timestamp(date.getTime()));
					signature.setNguoiKy(CommonUtils.getUserLogedIn());
					signature.setThuTu(new Long(thuTuCurrent));
					signatureService.save(fileVersion, signature);
					FileUtils.uploadToDocumentList(uploadPath+signedPath, signedPath, file.getTieuDe(), file.getTieuDe());
					Notification notf = new Notification("Thông báo", "Ký thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
					
					VerticalLayout parent = (VerticalLayout) button.getParent();
					if(parent != null){
						valueCurrent = (Map<String, Object>) parent.getData();
					}
					if(valueCurrent.get("ICON") != null){
						Label lbl = (Label) valueCurrent.get("ICON");
						lbl.setIcon(FontAwesome.CHECK_CIRCLE_O);
					}
				}
				
			}else if(BTN_DELETE_ID.equals(button.getId())){
				FileManagement file = (FileManagement) button.getData();
				if(file!=null
						&&file.getNguoiTao()!=null
						&&file.getNguoiTao().equals(CommonUtils.getUserLogedIn())){
					fileManagementService.deleteWithChild(file);
					VerticalLayout parent = (VerticalLayout) button.getParent();
					if(parent != null){
						valueCurrent = (Map<String, Object>) parent.getData();
						if(valueCurrent != null && valueCurrent.get("FILE_ITEM") != null && fileListWrap != null){
							CssLayout item = (CssLayout) valueCurrent.get("FILE_ITEM");
							fileListWrap.removeComponent(item);
						}
					}
					
					Notification notf = new Notification("Thông báo", "Xóa thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
				}else{
					Notification notf = new Notification("Thông báo", "Bạn không thể xóa tệp này", Type.WARNING_MESSAGE);
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
				}
			}else if(BTN_DELETE_VERSION_ID.equals(button.getId())){
				FileVersion file = (FileVersion) button.getData();
				if(file!=null
						&&file.getNguoiCapNhat()!=null
						&&file.getNguoiCapNhat().equals(CommonUtils.getUserLogedIn())){
					fileVersionService.deleteWithChild(file);
					updateHistoryView(file.getFileManagement());
					Notification notf = new Notification("Thông báo", "Xóa thành công");
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
				}else{
					Notification notf = new Notification("Thông báo", "Bạn không thể xóa version này", Type.WARNING_MESSAGE);
					notf.setDelayMsec(3000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
				}
			}else if(BTN_RESTORE_ID.equals(button.getId())){
				FileVersion file = (FileVersion) button.getData();
				if(file!=null && file.getFileManagement() != null){
					FileVersion maxVersion = fileManagementService.getMaxVersion(file.getFileManagement());
					if(maxVersion.getVersion() != null){
						file.setVersion(new Long(maxVersion.getVersion().intValue()+1));
						fileVersionService.save(file);
						updateHistoryView(file.getFileManagement());
						Notification notf = new Notification("Thông báo", "Khôi phục phiên bản thành công");
						notf.setDelayMsec(3000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
					}
					
				}
			}else if(BTN_EDIT_PROPERTIES_ID.equals(button.getId())){
				fieldGroup.setReadOnly(false);
				action = "EDIT_PROPERTIES";
				addFile.setVisible(false);
				editFile.setVisible(true);
				txtMota.setVisible(true);
				txtTitle.setVisible(true);
				VerticalLayout parent = (VerticalLayout) button.getParent();
				if(parent != null){
					valueCurrent = (Map<String, Object>) parent.getData();
				}
				FileManagement file = (FileManagement) button.getData();
				
				currentFileManagement = file;
				upload.setVisible(false);
				lblFileName.setVisible(false);
				txtMota.setValue(file.getMoTa());
				txtTitle.setValue(file.getTieuDe());
			    UI.getCurrent().addWindow(this);
			}else if(BUTTON_EDIT_FILE_ID.equals(button.getId())){
				Date date = new Date();
				if("EDIT".equals(action)){
					
					FileVersion file = new FileVersion();
					file.setDinhDang(fileExt);
					file.setUploadPath(filePath);
					file.setNguoiCapNhat(CommonUtils.getUserLogedIn());
					file.setNgayCapNhat(new Timestamp(date.getTime()));
					FileVersion fileVersion = fileManagementService.getMaxVersion(currentFileManagement);
					long version = 1L;
					if(fileVersion != null && fileVersion.getVersion() != null){
						version = fileVersion.getVersion();
						version++;
					}
					String moTa = txtMota.getValue();
					file.setGhiChu(moTa);
					file.setVersion(version);
					file.setFileManagement(currentFileManagement);
					fileVersionService.save(file);
					
					if(valueCurrent.get("ICON") != null){
						Label lbl = (Label) valueCurrent.get("ICON");
						lbl.setIcon(null);
					}
				}else if("EDIT_PROPERTIES".equals(action)){
					if(fieldGroup.isValid()){
						fieldGroup.commit();
						String title = (String) itemValue.getItemProperty("title").getValue();
						String desc = (String) itemValue.getItemProperty("desc").getValue();
						FileManagement manager = currentFileManagement;
						manager.setMoTa(desc);
						manager.setTieuDe(title);
						manager.setNguoiCapNhat(CommonUtils.getUserLogedIn());
						manager.setNgayCapNhat(new Timestamp(date.getTime()));
						fileManagementService.save(manager);
						if(valueCurrent.get("TITLE") != null){
							Label lbl = (Label) valueCurrent.get("TITLE");
							lbl.setValue(title);
						}
						
					}
				}
				this.close();
				upload.setVisible(true);
				lblFileName.setVisible(false);
				fieldGroup.setReadOnly(false);
				fieldGroup.clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Notification notf = new Notification("Thông báo", "Đã có lỗi xảy ra!", Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
		}
	}
	
	public static CssLayout createFileItem(String fileName, FileManagement file, ClickListener click, boolean signed, boolean readOnly){
		Map<String, Object> valueMap = new HashMap();
		
		
		
		CssLayout item = new CssLayout();

		valueMap.put("FILE_ITEM", item);
		item.setStyleName(ExplorerLayout.FILE_ITEM);
		item.setWidth("100%");
		Label lblIcon = new Label();
		if(signed){
			lblIcon.setIcon(FontAwesome.CHECK_CIRCLE_O);
		}
		lblIcon.addStyleName(ExplorerLayout.FILE_ITEM_TITLE);
		item.addComponent(lblIcon);
		
		valueMap.put("ICON", lblIcon);
		
		Label title = new Label(fileName);
		title.addStyleName(ExplorerLayout.FILE_ITEM_TITLE);
		item.addComponent(title);
		
		valueMap.put("TITLE", title);
		
		VerticalLayout actionWrap = new VerticalLayout();
		
		actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
		actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_LEFT);
		
		
		
		
		PopupView popup = new PopupView("<span class='v-icon FontAwesome'>&#xf0e7;</span>", actionWrap);
        popup.addStyleName(ExplorerLayout.ACTION_POPUP);
        popup.setHideOnMouseOut(false);
        popup.setCaptionAsHtml(true);
        
        valueMap.put("POPUP", popup);
		
        actionWrap.setData(valueMap);
		
		
		
        Button btnView = new Button("Xem lịch sử");
        btnView.setId(BTN_VIEW_ID);
        btnView.setIcon(FontAwesome.EYE);
        btnView.addStyleName(ExplorerLayout.BUTTON_ACTION);
        btnView.addClickListener(click);
        btnView.setData(file);
        btnView.addStyleName(Reindeer.BUTTON_LINK);
        actionWrap.addComponent(btnView);
        
       
        
        if(!readOnly){
        	Button btnEdit = new Button("Sửa tệp đính kèm");
     		btnEdit.setId(BTN_EDIT_ID);
     		btnEdit.setIcon(FontAwesome.PENCIL_SQUARE_O);
     		btnEdit.addStyleName(ExplorerLayout.BUTTON_ACTION);
     		btnEdit.addClickListener(click);
     		btnEdit.setData(file);
     		btnEdit.addStyleName(Reindeer.BUTTON_LINK);
            actionWrap.addComponent(btnEdit);
        	
        	 Button btnEditProperties = new Button("Sửa thông tin chung");
             btnEditProperties.setId(BTN_EDIT_PROPERTIES_ID);
             btnEditProperties.setIcon(FontAwesome.PENCIL_SQUARE_O);
             btnEditProperties.addStyleName(ExplorerLayout.BUTTON_ACTION);
             btnEditProperties.addClickListener(click);
             btnEditProperties.setData(file);
             btnEditProperties.addStyleName(Reindeer.BUTTON_LINK);
             actionWrap.addComponent(btnEditProperties);
             
        }
        
       
        Button btnCheckout = new Button("Checkout");
		btnCheckout.setId(BTN_CHECKOUT_ID);
		btnCheckout.setIcon(FontAwesome.ARROW_DOWN);
		btnCheckout.addStyleName(ExplorerLayout.BUTTON_ACTION);
		btnCheckout.addClickListener(click);
		btnCheckout.setData(file);
		btnCheckout.addStyleName(Reindeer.BUTTON_LINK);
        actionWrap.addComponent(btnCheckout);
        
        Button btnPreview = new Button("Xem trước");
        btnPreview.setId(BTN_PREVIEW_ID);
        btnPreview.setIcon(FontAwesome.EYE);
        btnPreview.addStyleName(ExplorerLayout.BUTTON_ACTION);
        btnPreview.addClickListener(click);
        btnPreview.setData(file);
        btnPreview.addStyleName(Reindeer.BUTTON_LINK);
        actionWrap.addComponent(btnPreview);
        
        if(!readOnly){
        	 Button btnSig = new Button("Ký số tệp");
     		btnSig.setId(BTN_SIGNATURE_ID);
     		btnSig.setIcon(FontAwesome.PAINT_BRUSH);
     		btnSig.addStyleName(ExplorerLayout.BUTTON_ACTION);
     		btnSig.addClickListener(click);
     		btnSig.setData(file);
     		btnSig.addStyleName(Reindeer.BUTTON_LINK);
             actionWrap.addComponent(btnSig);
             
             //TODO
             Button btnCopy = new Button("Xóa");
     		btnCopy.setId(BTN_DELETE_ID);
     		btnCopy.setIcon(FontAwesome.REMOVE);
     		btnCopy.addStyleName(ExplorerLayout.BUTTON_ACTION);
     		btnCopy.addClickListener(click);
     		btnCopy.setData(file);
     		btnCopy.addStyleName(Reindeer.BUTTON_LINK);
             actionWrap.addComponent(btnCopy);
        }
        
       
        
        
        
        item.addComponent(popup);
        
        
		return item;
	}
	
	
	public void initHisoryView(){
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("100%");
		layout.setSpacing(true);
		layout.setMargin(true);
		
		CssLayout titleWrap = new CssLayout();
		titleWrap.setWidth("100%");
		titleWrap.setStyleName(ExplorerLayout.FILE_INFO_TITLE_WRAP);

		Label titleLbl = new Label("Tiêu đề");
		titleLbl.setStyleName(ExplorerLayout.FILE_INFO_TITLE_LBL);
		titleWrap.addComponent(titleLbl);
		
		fileTitle = new Label();
		fileTitle.setStyleName(ExplorerLayout.FILE_INFO_TITLE);
		titleWrap.addComponent(fileTitle);
		layout.addComponent(titleWrap);
		
		CssLayout descWrap = new CssLayout();
		descWrap.setWidth("100%");
		descWrap.setStyleName(ExplorerLayout.FILE_INFO_DESC_WRAP);
		
		Label descLbl = new Label("Mô tả");
		descLbl.setStyleName(ExplorerLayout.FILE_INFO_DESC_LBL);
		descWrap.addComponent(descLbl);
		fileDesc = new Label();
		fileDesc.setStyleName(ExplorerLayout.FILE_INFO_DESC);
		descWrap.addComponent(fileDesc);
		layout.addComponent(descWrap);
		
		tableVersion = new Table();
		tableVersion.addStyleName(ExplorerLayout.DATA_TABLE);
		tableVersion.setWidth("100%");

		tableVersion.addContainerProperty("version", String.class, null);
		tableVersion.addContainerProperty("updateTime", String.class, null);
		tableVersion.addContainerProperty("user", String.class, null);
		tableVersion.addContainerProperty("comment", String.class, null);
		tableVersion.addContainerProperty("status", String.class, null);
		tableVersion.addContainerProperty("action", PopupView.class, null);
		
		
		tableVersion.setColumnHeader("version", "Phiên bản");
		tableVersion.setColumnHeader("updateTime", "Thời gian");
		tableVersion.setColumnHeader("user", "Người dùng");
		tableVersion.setColumnHeader("comment", "Ghi chú");
		tableVersion.setColumnHeader("status", "Trạng thái");
		tableVersion.setColumnHeader("action", "Thao tác");
		
		
		layout.addComponent(tableVersion);
		
		window = new Window("Chi tiết version");
		window.setWidth(900, Unit.PIXELS);
		//window.setHeight(500, Unit.PIXELS);
		window.setResizable(true);
		window.center();
		window.setCaption("Thông tin");
		window.setModal(true);
		window.setContent(layout);
	}
	
	public void initSignatureView(){
		VerticalLayout layout = new VerticalLayout();
		layout.setWidth("100%");
		layout.setSpacing(true);
		layout.setMargin(true);
		
		tableSignature = new Table();
		tableSignature.addStyleName(ExplorerLayout.DATA_TABLE);
		tableSignature.setWidth("100%");

		tableSignature.addContainerProperty("nguoiKy", String.class, null);
		tableSignature.addContainerProperty("ngayKy", String.class, null);
		tableSignature.addContainerProperty("ghiChu", String.class, null);
		tableSignature.setColumnHeader("nguoiKy", "Người ký");
		tableSignature.setColumnHeader("ngayKy", "Ngày ký");
		tableSignature.setColumnHeader("ghiChu", "Ghi chú");
		
		layout.addComponent(tableSignature);
		
		signedWindow = new Window("Chi tiết ký số");
		signedWindow.setWidth(900, Unit.PIXELS);
//		signedWindow.setHeight(500, Unit.PIXELS);
		signedWindow.setResizable(true);
		signedWindow.center();
		signedWindow.setModal(true);
		signedWindow.setContent(layout);
	}
	
	public void updateHistoryView(FileManagement file){
		fileTitle.setValue(file.getTieuDe());
		fileDesc.setValue(file.getMoTa());
		tableVersion.removeAllItems();
		List<FileVersion> fileVersionList = fileVersionService.getFileVersionFind(file);
		if(fileVersionList != null){
			boolean currentVersion = true;
			for(FileVersion v : fileVersionList){
				String version = v.getVersion() != null ? v.getVersion()+"" : "";
				String updateTime = "";
				if(v.getNgayCapNhat() != null){
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
					updateTime = df.format(v.getNgayCapNhat().getTime());
				}
				
				String status = "Chưa ký số";
				if(v != null && v.getTrangThai() != null && v.getTrangThai().intValue() == 1){
					status = "Đã ký số";
				}

				VerticalLayout actionWrap = new VerticalLayout();
				actionWrap.setData(v);
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_WRAP);
				actionWrap.addStyleName(ExplorerLayout.BUTTON_ACTION_LEFT);
				
				
				PopupView popup = new PopupView("<span class='v-icon FontAwesome'>&#xf0e7;</span>", actionWrap);
		        popup.addStyleName(ExplorerLayout.ACTION_POPUP);
		        popup.setHideOnMouseOut(false);
		        popup.setCaptionAsHtml(true);
				
				List<Signature> signatureList = signatureService.getSignatureFind(v);
				if(signatureList != null && !signatureList.isEmpty()){
					
					Button btnView = new Button("Lịch sử ký số");
			        btnView.setIcon(FontAwesome.EYE);
			        btnView.addStyleName(ExplorerLayout.BUTTON_ACTION);
			        btnView.setData(v);
			        btnView.addStyleName(Reindeer.BUTTON_LINK);
			        actionWrap.addComponent(btnView);
					
					
					btnView.addClickListener(new ClickListener() {
						@Override
						public void buttonClick(ClickEvent event) {
							tableSignature.removeAllItems();
							for(Signature s : signatureList){
								String ngayKy = "";
								
								if(s.getNgayKy() != null){
									SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
									ngayKy = df.format(s.getNgayKy().getTime());
								}
								tableSignature.addItem(new Object[]{s.getNguoiKy(), ngayKy, s.getGhiChu()}, s);
							}
							popup.setPopupVisible(false);
							UI.getCurrent().addWindow(signedWindow);
						}
					});
				}
				
			        
		        Button btnAttachment = new Button("Tải tệp đính kèm");
		        btnAttachment.setIcon(FontAwesome.ARROW_DOWN);
		        btnAttachment.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        btnAttachment.setData(v);
		        btnAttachment.addStyleName(Reindeer.BUTTON_LINK);
		        FileUtils.downloadWithButton(btnAttachment, uploadPath+v.getUploadPath());
		        actionWrap.addComponent(btnAttachment);
		        
		        if(v.getTrangThai() != null && v.getTrangThai().intValue() == 1
						&& v.getSignedPath() != null){
		        	Button btnGetSigned = new Button("Tải tệp đã ký số");
		        	btnGetSigned.setIcon(FontAwesome.ARROW_DOWN);
		        	btnGetSigned.addStyleName(ExplorerLayout.BUTTON_ACTION);
		        	btnGetSigned.setData(v);
		        	btnGetSigned.addStyleName(Reindeer.BUTTON_LINK);
		        	
		        	
		        	try {
						File f = new File(uploadPath+v.getSignedPath());
						if(f!= null && f.exists()){
							btnGetSigned.setVisible(true);
							FileUtils.downloadWithButton(btnGetSigned, uploadPath+v.getSignedPath());
						}else{
							btnGetSigned.setVisible(false);
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
		        	
		        	
			        actionWrap.addComponent(btnGetSigned);
		        }
		        
		        if(!readOnly){
		        	if(!currentVersion){
				    	   Button btnRestore = new Button("Khôi phục phiên bản");
					        btnRestore.setId(BTN_RESTORE_ID);
					        btnRestore.setIcon(FontAwesome.ROTATE_LEFT);
					        btnRestore.addStyleName(ExplorerLayout.BUTTON_ACTION);
					        btnRestore.addClickListener(this);
					        btnRestore.setData(v);
					        btnRestore.addStyleName(Reindeer.BUTTON_LINK);
					        actionWrap.addComponent(btnRestore);
				       }
				        
				        Button btnDel = new Button("Xóa");
						btnDel.setId(BTN_DELETE_VERSION_ID);
						btnDel.setIcon(FontAwesome.REMOVE);
						btnDel.addStyleName(ExplorerLayout.BUTTON_ACTION);
						btnDel.addClickListener(this);
						btnDel.setData(v);
						btnDel.addStyleName(Reindeer.BUTTON_LINK);
				        actionWrap.addComponent(btnDel);
		        }
		        
		        currentVersion = false;
				
		        tableVersion.setCellStyleGenerator(new Table.CellStyleGenerator(){
					private static final long serialVersionUID = 1L;

					@Override
					public String getStyle(Table source, Object itemId, Object propertyId) {
						if("action".equals(propertyId) || "version".equals(propertyId)){
		                    return "center-aligned";
		                }
						return "left-aligned";
					}
					
				});
				
				tableVersion.addItem(new Object[]{version, updateTime,v.getNguoiCapNhat(), v.getGhiChu(),status, popup}, v);
			}
		}
	}
}

package com.eform.ui.custom.paging.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.eform.common.ExplorerLayout;
import com.eform.common.SpringApplicationContext;
import com.eform.common.ThamSo;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.HtThamSo;
import com.eform.service.HtThamSoService;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Layout;

public abstract class ShowMorePaginationBar<T> extends CssLayout implements ClickListener, Serializable{

	private Layout wrapper;
	
	protected int pageSize = 5;
	protected int currentPage;
	protected long rowCount;
	protected int totalPage;
	protected List<T> dataList;
	
	private CssLayout btnShowMoreWrap;
	private Button btnShowMore;
	private HtThamSoService htThamSoService;
	private BeanItemContainer<T> container;
	
	
	//Phan trang kieu append
	public ShowMorePaginationBar(Layout wrapper, String pageSizeStr){
		super();
		init(wrapper,null, pageSizeStr);
	}
	public ShowMorePaginationBar(Layout wrapper, BeanItemContainer<T> container, String pageSizeStr){
		super();
		init(wrapper, container, pageSizeStr);
	}
	
	public void init(Layout wrapper, BeanItemContainer<T> container, String pageSizeStr){
		this.wrapper = wrapper;
		this.container = container;
		if (htThamSoService == null) {
			htThamSoService = SpringApplicationContext.getApplicationContext().getBean(HtThamSoService.class);
		}
		HtThamSo pageSizePar = htThamSoService.getHtThamSoFind(pageSizeStr);
		if(pageSizePar != null && pageSizePar.getGiaTri() != null && pageSizePar.getGiaTri().trim().matches("[0-9]+")){
			pageSize = Integer.parseInt(pageSizePar.getGiaTri().trim());
		}
		init();
		
	}
	
	public void init(){
		currentPage = 1;
		addStyleName(ExplorerLayout.SHOW_MORE_BAR);
		createPaginationBar();
	}
	
	public abstract long getRowCount();
	public abstract List<T> getMoreDataList(int firstResult, int maxResults);
	public abstract Component builItem(T t, int index);
	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	
	public void createPaginationBar(){
		btnShowMoreWrap = new CssLayout();
		btnShowMoreWrap.setWidth("100%");
		btnShowMoreWrap.addStyleName(ExplorerLayout.BTN_SHOW_MORE_WRAP);
		
		btnShowMore = new Button("Xem thêm");
		btnShowMore.addStyleName(ExplorerLayout.BTN_SHOW_MORE);
		btnShowMoreWrap.addComponent(btnShowMore);
		btnShowMore.addClickListener(this);
		
		addComponent(btnShowMoreWrap);
	}
	
	public void initData(boolean removeAll){
		try {
			rowCount = getRowCount();
			totalPage = (int) Math.ceil(1.0F*rowCount/pageSize);
			
			if(removeAll || rowCount == 0){
				if(dataList != null){
		        	dataList.clear();	
				}
        		this.wrapper.removeAllComponents();
        		if(container != null){
        			container.removeAllItems();
        		}
        	}
			
	        if(currentPage > 0 && currentPage <= totalPage){
	        	int from = (currentPage-1)*pageSize;
	        	this.dataList = getMoreDataList(from, pageSize);
	        }
	        
	        
        	refresh();
	        if(currentPage < totalPage){
				btnShowMoreWrap.setVisible(true);
			}else{
				btnShowMoreWrap.setVisible(false);
			}
		} catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public void appendData() {
		initData(false);
	}
	
	public void reloadData() {
		initData(true);
	}
	
	public void refresh(){
		if(dataList != null && !dataList.isEmpty()){
			int index = 0;
			for(T t : dataList){
				index++;
				Component item = builItem(t, index);
				if(item != null){
					wrapper.addComponent(item);
				}
			}
			if(container != null){
				container.addAll(dataList);
			}
		}
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		if(currentPage < (int) Math.ceil(1.0F*rowCount/pageSize)){
			currentPage++;
			appendData();
		}
		if(currentPage == totalPage){
			btnShowMoreWrap.setVisible(false);
		}
	}

	public List<T> getDataList() {
		return dataList;
	}

	public void setDataList(List<T> dataList) {
		this.dataList = dataList;
	}
	
	

}

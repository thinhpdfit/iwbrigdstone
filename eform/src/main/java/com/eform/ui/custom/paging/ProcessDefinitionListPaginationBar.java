package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;

import com.vaadin.ui.Button.ClickListener;
import com.eform.common.SpringApplicationContext;
import com.eform.common.ThamSo;
import com.eform.common.utils.ProcessDefinitionUtils;
import com.eform.model.entities.HtThamSo;
import com.eform.service.HtThamSoService;
import com.eform.ui.custom.paging.base.ShowMorePaginationBar;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Component;
import com.vaadin.ui.Layout;

public class ProcessDefinitionListPaginationBar extends ShowMorePaginationBar<ProcessDefinition> {

	private RepositoryService repositoryService;
	private String name;
	private Set<String> depIds;
	private Boolean isActive;
	private ClickListener clickListener;
	private HtThamSoService htThamSoService;
	private String[] colors;
	private Map<Long, Boolean> mapQuyen;

	public ProcessDefinitionListPaginationBar(Layout wrapper, BeanItemContainer<ProcessDefinition> container,  RepositoryService repositoryService,
			ClickListener clickListener, Set<String> depIds, Boolean isActive, Map<Long, Boolean> mapQuyen) {
		super(wrapper, container, ThamSo.PRO_DEF_PAGE_SIZE);
		this.repositoryService = repositoryService;
		this.clickListener = clickListener;
		this.mapQuyen = mapQuyen;
		if (htThamSoService == null) {
			htThamSoService = SpringApplicationContext.getApplicationContext().getBean(HtThamSoService.class);
		}
		this.depIds = depIds;
		this.isActive = isActive;
		HtThamSo htThamSo = htThamSoService.getHtThamSoFind(ThamSo.COLOR_LIST);
		if (htThamSo != null && htThamSo.getGiaTri() != null) {
			colors = htThamSo.getGiaTri().split(",");
		}
		
		appendData();
	}

	@Override
	public long getRowCount() {
		Long count = 0L;
		if (repositoryService != null) {
			ProcessDefinitionQuery q = repositoryService.createProcessDefinitionQuery();
			
			if (name != null && !name.trim().isEmpty()) {
				q = q.processDefinitionNameLike("%" + name + "%");
			}

			if (depIds != null && !depIds.isEmpty()) {
				q = q.deploymentIds(depIds);
			}

			if (isActive != null) {
				if (isActive) {
					q = q.active();
				} else {
					q = q.suspended();
				}
			}

			count = q.count();

		}
		return count;
	}

	@Override
	public List<ProcessDefinition> getMoreDataList(int firstResult, int maxResults) {
		List<ProcessDefinition> proDefList = new ArrayList<ProcessDefinition>();
		if (repositoryService != null) {
			
			ProcessDefinitionQuery q = repositoryService.createProcessDefinitionQuery();
			
			if (name != null && !name.trim().isEmpty()) {
				q = q.processDefinitionNameLike("%" + name + "%");
			}

			if (depIds != null && !depIds.isEmpty()) {
				q = q.deploymentIds(depIds);
			}

			if (isActive != null) {
				if (isActive) {
					q = q.active();
				} else {
					q = q.suspended();
				}
			}
			proDefList = q.orderByDeploymentId().desc().listPage(firstResult, maxResults);
		}
		return proDefList;
	}

	public void search(String name, Set<String> depIds, Boolean isActive) {
		this.name = name;
		this.depIds = depIds;
		this.isActive = isActive;
		currentPage = 1;
		reloadData();
	}
	
	public void tabFilter(Set<String> depIds){
		this.depIds = depIds;
		currentPage = 1;
		reloadData();
	}
	

	@Override
	public Component builItem(ProcessDefinition proDef, int i) {
		return ProcessDefinitionUtils.buildItem(proDef, i, clickListener, colors, repositoryService, mapQuyen);
	}

}

package com.eform.ui.custom;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.utils.FileUtils;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Window;

public class UploadAttachmentInfo implements Upload.StartedListener, Upload.ProgressListener,
		Upload.FailedListener, Upload.SucceededListener, Upload.FinishedListener{
	
    private long maxSize;

	private final UploadReceive receive;
	private Upload upload;
	private Label lbl;
	
	private final List<String> fileAlows;
	
	private String fileNameStr;

	public UploadAttachmentInfo(final Upload upload, Label lbl, final UploadReceive receive, long maxSize, List<String> fileAlows) {
		this.fileAlows = fileAlows;
		this.receive = receive;
		this.maxSize = maxSize;
		this.upload = upload;
		this.lbl = lbl;

		upload.addStartedListener(this);
		upload.addProgressListener(this);
		upload.addFailedListener(this);
		upload.addSucceededListener(this);
		upload.addFinishedListener(this);

	}

	@Override
	public void uploadFinished(final FinishedEvent event) {
		System.out.println("uploadFinished");
		
	}

	@Override
	public void uploadStarted(final StartedEvent event) {
		System.out.println("uploadStarted");
		fileNameStr = event.getFilename();
		String ext = FileUtils.getFileExtension(fileNameStr);
		if(fileAlows != null && !fileAlows.isEmpty() && !(ext != null && fileAlows.contains(ext))){
			upload.interruptUpload();
            String fileAlowStr = "";
            int i = 0;
            for(String str : fileAlows){
            	i++;
            	if(i == fileAlows.size()){
            		fileAlowStr += str;
            	}else{
            		fileAlowStr += str+"; ";
            	}
            }
            Notification notf = new Notification("Thông báo", "Định dạng file cho phép là "+fileAlowStr, Notification.Type.WARNING_MESSAGE);
			notf.setDelayMsec(5000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
            return;
		}

	}

	@Override
	public void updateProgress(final long readBytes, final long contentLength) {
		System.out.println("updateProgress");
		if (maxSize < contentLength) {
            upload.interruptUpload();
            Notification notf = new Notification("Thông báo", "File vượt quá dung lượng cho phép "+maxSize+" bytes", Notification.Type.WARNING_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
            return;
        }
	}

	@Override
	public void uploadSucceeded(final SucceededEvent event) {
		System.out.println("uploadSucceeded");
		try {
			TextField data = (TextField) upload.getData();
			data.setValue(fileNameStr);
	        if(lbl != null){
	        	lbl.setValue("Upload thành công: "+fileNameStr);
	        	lbl.setVisible(true);
			}
	        upload.setVisible(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void uploadFailed(final FailedEvent event) {
		System.out.println("uploadFailed");
	}
	
}
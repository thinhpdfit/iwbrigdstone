package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.activiti.engine.identity.Group;

import com.vaadin.ui.Button.ClickListener;
import com.eform.common.SpringApplicationContext;
import com.eform.common.utils.TaskUtils;
import com.eform.ui.custom.paging.base.PaginationBarForListComponent;
import com.eform.ui.view.task.TaskCategory;
import com.vaadin.ui.Component;
import com.vaadin.ui.Layout;

public class HistoricTaskPaginationBar extends PaginationBarForListComponent<HistoricTaskInstance> {

	protected HistoryService historyService;
	private RepositoryService repositoryService;
	private IdentityService identityService;
	private String name;
	private String category;
	private String userId;
	private List<String> groupIds;
	private ClickListener clickListener;

	
	public HistoricTaskPaginationBar(Layout wrapper, String category, String userId, ClickListener clickListener) {
		super(wrapper);
		this.historyService = SpringApplicationContext.getApplicationContext().getBean(HistoryService.class);
		this.repositoryService = SpringApplicationContext.getApplicationContext().getBean(RepositoryService.class);
		this.identityService = SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
		this.category = category;
		this.userId = userId;
		this.clickListener = clickListener;
		List<Group> groups = identityService.createGroupQuery().groupMember(userId).list();
		if(groups != null){
			groupIds = new ArrayList();
			for(Group g : groups){
				groupIds.add(g.getId());
			}
		}
		reloadData();
	}

	@Override
	public long getRowCount() {
		Long count = 0L;
		if(historyService != null){
			HistoricTaskInstanceQuery q = historyService.createHistoricTaskInstanceQuery();

			if(name != null && !name.trim().isEmpty()){
				q.taskNameLike("%"+name+"%");
			}
			if(TaskCategory.ARCHIVED_CATE.equals(category)){
				q.taskOwner(userId);
			}else if(TaskCategory.FINISHED_CATE.equals(category)){
				q.taskAssignee(userId);
			}else if(TaskCategory.CANDIDATE_FINISHED_CATE.equals(category)){
				q.or().taskInvolvedUser(userId).taskCandidateUser(userId);
				if(groupIds != null && !groupIds.isEmpty()){
					q.taskCandidateGroupIn(groupIds);
				}
			}
			count = q.finished().count();
		}
		return count;
	}

	@Override
	public List<HistoricTaskInstance> getDataList(int firstResult, int maxResults) {
		List<HistoricTaskInstance> taskList = new ArrayList();
		if(historyService != null){
			HistoricTaskInstanceQuery q = historyService.createHistoricTaskInstanceQuery();
			if(name != null && !name.trim().isEmpty()){
				q.taskNameLike("%"+name+"%");
			}
			if(TaskCategory.ARCHIVED_CATE.equals(category)){
				q.taskOwner(userId);
			}else if(TaskCategory.FINISHED_CATE.equals(category)){
				q.taskAssignee(userId);
			}else if(TaskCategory.CANDIDATE_FINISHED_CATE.equals(category)){
				q.or().taskInvolvedUser(userId).taskCandidateUser(userId);
				if(groupIds != null && !groupIds.isEmpty()){
					q.taskCandidateGroupIn(groupIds);
				}
			}
			
			taskList = q.finished().orderByTaskCreateTime().desc().listPage(firstResult, maxResults);
		}
		return taskList;
	}
	
	public void search(String name, 
			String category,
			String userId){
		this.category = category;
		this.userId = userId;
		this.name = name;
		currentPage = 1;
		List<Group> groups = identityService.createGroupQuery().groupMember(userId).list();
		if(groups != null){
			groupIds.clear();
			for(Group g : groups){
				groupIds.add(g.getId());
			}
		}
		reloadData();
	}

	@Override
	public Component builItem(HistoricTaskInstance task, int i) {
		return TaskUtils.buildItem(task, i, clickListener, repositoryService, identityService);
	}

}

package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import com.eform.dao.HtQuyenDao;
import com.eform.model.entities.HtQuyen;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class HtQuyenPaginationBar extends PaginationBar<HtQuyen> {

	private HtQuyenDao htQuyenDao;
	private String ma;
	private String ten;
	private List<Long> trangThai;
	
	public HtQuyenPaginationBar(BeanItemContainer<HtQuyen> container, HtQuyenDao htQuyenDao) {
		super(container);
		this.htQuyenDao = htQuyenDao;
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(htQuyenDao != null){
			return htQuyenDao.getHtQuyenFind(ma,null, ten, trangThai, null);
		}
		return 0L;
	}

	@Override
	public List<HtQuyen> getDataList(int firstResult, int maxResults) {
		if(htQuyenDao != null){
			return htQuyenDao.getHtQuyenFind(ma,null, ten, trangThai, null, firstResult, maxResults);
		}
		return new ArrayList<HtQuyen>();
	}
	
	public void search(String ma, 
			String ten, 
			List<Long> trangThai){
		this.ma = ma;
		this.ten = ten;
		this.trangThai =trangThai;
		currentPage = 1;
		reloadData();
	}

}

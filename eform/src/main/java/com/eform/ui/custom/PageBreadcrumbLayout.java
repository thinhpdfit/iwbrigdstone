package com.eform.ui.custom;

import java.util.ArrayList;
import java.util.List;

import com.eform.common.ExplorerLayout;
import com.eform.common.type.PageBreadcrumbInfo;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class PageBreadcrumbLayout extends CssLayout implements ClickListener{
	private List<PageBreadcrumbInfo> pageBreadcrumbInfoList;
	
	
	public PageBreadcrumbLayout(PageBreadcrumbInfo... viewInfos){
		setWidth("100%");
		setStyleName(ExplorerLayout.PAGE_BREADCRUMB_WRAP);
		pageBreadcrumbInfoList = new ArrayList();
		if(viewInfos != null && viewInfos.length >0){
			
			boolean isFirst = true;
			for(PageBreadcrumbInfo v : viewInfos){
				initItem(isFirst, v);
				isFirst = false;
			}
		}
	}
	
	public void addBreadcrumbItem(PageBreadcrumbInfo v){
		if(v != null && pageBreadcrumbInfoList !=null){
			initItem(pageBreadcrumbInfoList.isEmpty(), v);
		}
	}
	
	public void initItem(boolean isFirst, PageBreadcrumbInfo v){
		if(!isFirst){
			Label lbl = new Label();
			lbl.setValue(FontAwesome.ANGLE_RIGHT.getHtml());
			lbl.setContentMode(ContentMode.HTML);
			lbl.setStyleName(ExplorerLayout.PAGE_BREADCRUMB_SPACE);
			addComponent(lbl);
		}
		Button button = new Button(v.getTitle());
		button.setStyleName(ExplorerLayout.PAGE_BREADCRUMB_ITEM);
		button.addClickListener(this);
		button.setData(v);
		addComponent(button);
		pageBreadcrumbInfoList.add(v);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(button != null && button.getData() != null){
			PageBreadcrumbInfo v = (PageBreadcrumbInfo) button.getData();
			UI.getCurrent().getNavigator().navigateTo(v.getViewName());
		}
	}
	
	public void clear(){
		removeAllComponents();
		pageBreadcrumbInfoList.clear();
	}

}

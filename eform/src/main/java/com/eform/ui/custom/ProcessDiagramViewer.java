package com.eform.ui.custom;

import java.io.InputStream;
import java.net.URL;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.GraphicInfo;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;

import com.eform.common.SpringApplicationContext;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.XmlUtil;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.UI;

public class ProcessDiagramViewer extends CssLayout{
	
	private RepositoryService repositoryService;
	private String processDefId;
	private String processInsId;
	private int maxY = 0; //default
	
	public ProcessDiagramViewer(String processDefId, String processInsId){
		repositoryService = SpringApplicationContext.getApplicationContext().getBean(RepositoryService.class);
		setWidth("100%");
		this.processDefId = processDefId;
		this.processInsId = processInsId;
		initMaxY();
		initLayout();
	}
	
	public void initMaxY(){
		try {
			ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefId).singleResult();
			if(processDefinition != null){
				final InputStream definitionStream = repositoryService.getResourceAsStream(
						processDefinition.getDeploymentId(), processDefinition.getResourceName());
				XMLInputFactory xif = XmlUtil.createSafeXmlInputFactory();
				XMLStreamReader xtr = xif.createXMLStreamReader(definitionStream);
				BpmnModel bpmnModel = new BpmnXMLConverter().convertToBpmnModel(xtr);

				if (!bpmnModel.getFlowLocationMap().isEmpty()) {

					for (String key : bpmnModel.getLocationMap().keySet()) {
						GraphicInfo graphicInfo = bpmnModel.getGraphicInfo(key);
						double elementY = graphicInfo.getY() + graphicInfo.getHeight();
						if (maxY < elementY) {
							maxY = (int) elementY;
						}
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void initLayout(){
		try {
			String host = UI.getCurrent().getPage().getLocation().getHost();
			String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
			int port = UI.getCurrent().getPage().getLocation().getPort();
			String scheme = UI.getCurrent().getPage().getLocation().getScheme();
		
			if(processDefId != null && processInsId != null){
				URL url = new URL(scheme, host, port, contextPath + "/diagram-viewer/index.html?processDefinitionId=" + processDefId
						+ "&processInstanceId=" + processInsId);
		
				Embedded browserPanel = new Embedded("", new ExternalResource(url));
				browserPanel.setType(Embedded.TYPE_BROWSER);
				browserPanel.setWidth("100%");
				browserPanel.setHeight((maxY + 300) + "px");
				addComponent(browserPanel);
			}else if(processDefId != null){
				URL url = new URL(scheme, host, port, contextPath + "/diagram-viewer/index.html?processDefinitionId=" + processDefId);
		
				Embedded browserPanel = new Embedded("", new ExternalResource(url));
				browserPanel.setType(Embedded.TYPE_BROWSER);
				browserPanel.setWidth("100%");
				browserPanel.setHeight((maxY + 300) + "px");
				addComponent(browserPanel);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}

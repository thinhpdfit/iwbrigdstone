package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;

import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class ModelerPaginationBar extends PaginationBar<Model> {

	private RepositoryService repositoryService;
	private String name;
	private String category;
	
	public ModelerPaginationBar(BeanItemContainer<Model> container, RepositoryService repositoryService) {
		super(container);
		this.repositoryService = repositoryService;
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(repositoryService != null){
			if(name == null){
				name = "";
			}
			if(category != null){
				return repositoryService.createModelQuery().modelNameLike("%"+name+"%").modelCategoryLike("%"+category+"%").count();
			}else{
				return repositoryService.createModelQuery().modelNameLike("%"+name+"%").count();
			}
		}
		return 0L;
	}

	@Override
	public List<Model> getDataList(int firstResult, int maxResults) {
		if(repositoryService != null){
			if(name == null){
				name = "";
			}
			if(category != null){
				return repositoryService.createModelQuery().modelNameLike("%"+name+"%").modelCategoryLike("%"+category+"%").orderByCreateTime().desc().listPage(firstResult, maxResults);
			}else{
				return repositoryService.createModelQuery().modelNameLike("%"+name+"%").orderByCreateTime().desc().listPage(firstResult, maxResults);
			}
		}
		return new ArrayList<Model>();
	}
	
	public void search(String name, 
			String category){
		this.name = name;
		this.category = category;
		currentPage = 1;
		reloadData();
	}

}

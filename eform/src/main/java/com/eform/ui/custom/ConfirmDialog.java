package com.eform.ui.custom;

import com.eform.common.ExplorerLayout;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ConfirmDialog extends Window implements ClickListener{
	
	private String clicked;

	public String btnOkId = "btn-ok";
	public String btnCancelId = "btn-cancel";
	private String title;
	private String message;
	private ClickListener clickListener;
	private Object currentData;
	
	public ConfirmDialog(String title, String message, ClickListener clickListener){
		//Mac dinh
		center();
		setModal(true);
		setResizable(false);
		this.title = title;
		this.message = message;
		this.clickListener = clickListener;
		init();
	}
	
	public ConfirmDialog(String title, String message, ClickListener clickListener, String btnOkId, String btnCancelId){
		//Mac dinh
		center();
		setModal(true);
		setResizable(false);
		this.title = title;
		this.message = message;
		this.clickListener = clickListener;
		this.btnCancelId = btnCancelId;
		this.btnOkId = btnOkId;
		init();
	}
	
	
	public void init(){
		setCaption(title);
		
		VerticalLayout confirmContainer = new VerticalLayout();
		confirmContainer.setMargin(true);
		confirmContainer.setSpacing(true);

		Label lbl = new Label(message);
		lbl.setStyleName(ExplorerLayout.LBL_CONFIRM);
		confirmContainer.addComponent(lbl);

		CssLayout buttons = new CssLayout();
		buttons.setWidth("100%");
		buttons.setStyleName(ExplorerLayout.BTN_GROUP_CENTER);
		Button btnOk = new Button("OK");
		btnOk.addStyleName(ExplorerLayout.BUTTON_SUCCESS);
		btnOk.addStyleName(ExplorerLayout.BUTTON_CONFIRM_OK);
		
		btnOk.setId(btnOkId);
		btnOk.addClickListener(this);
		btnOk.addClickListener(clickListener);
		buttons.addComponent(btnOk);

		Button btnCancel = new Button("Cancel");
		btnCancel.setId(btnCancelId);
		btnCancel.addStyleName(ExplorerLayout.BUTTON_CONFIRM_CANCEL);
		btnCancel.addClickListener(this);
		btnCancel.addClickListener(clickListener);
		buttons.addComponent(btnCancel);

		confirmContainer.addComponent(buttons);
		setContent(confirmContainer);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(button != null){
			clicked = button.getId();
			this.close();
		}
	}

	public String getClicked() {
		return clicked;
	}

	public void setClicked(String clicked) {
		this.clicked = clicked;
	}

	public String getBtnOkId() {
		return btnOkId;
	}

	public void setBtnOkId(String btnOkId) {
		this.btnOkId = btnOkId;
	}

	public String getBtnCancelId() {
		return btnCancelId;
	}

	public void setBtnCancelId(String btnCancelId) {
		this.btnCancelId = btnCancelId;
	}

	public Object getCurrentData() {
		return currentData;
	}

	public void setCurrentData(Object currentData) {
		this.currentData = currentData;
	}
	
	
	
}

package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;

import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class UserPaginationBar extends PaginationBar<User> {

	protected IdentityService identityService;
	private String id;
	private String firstName;
	private String lastName;
	private String email;
	private String maHtDonVi;
	private String groupId;
	
	public UserPaginationBar(BeanItemContainer<User> container, IdentityService identityService) {
		super(container);
		this.identityService = identityService;
		reloadData();
	}

	@Override
	public long getRowCount() {
		Long count = 0L;
		if(identityService != null){
			
			UserQuery q = identityService.createUserQuery();
			if(id != null && !id.isEmpty()){
				q =  q.userId(id);
			}
			
			if(maHtDonVi != null){
				//q = q.
				
			}
			
			if(groupId != null){
				q = q.memberOfGroup(groupId);
			}
			
			if(firstName != null){
				q.userFirstNameLike("%"+firstName+"%");
			}
			if(lastName != null){
				q.userLastNameLike("%"+lastName+"%");
			}
			if(email != null){
				q.userEmailLike("%"+email+"%");
			}
			
			count = q.count();
		}
		return count;
	}

	@Override
	public List<User> getDataList(int firstResult, int maxResults) {
		List<User> userList = new ArrayList<User>();
		if(identityService != null){
			
			UserQuery q = identityService.createUserQuery();
			if(id != null && !id.isEmpty()){
				q =  q.userId(id);
			}
			
			if(maHtDonVi != null){
				//q = q.
				
			}
			
			if(groupId != null){
				q = q.memberOfGroup(groupId);
			}
			
			if(firstName != null){
				q.userFirstNameLike("%"+firstName+"%");
			}
			if(lastName != null){
				q.userLastNameLike("%"+lastName+"%");
			}
			if(email != null){
				q.userEmailLike("%"+email+"%");
			}
			userList = q.listPage(firstResult, maxResults);
			
		}
		return userList;
	}
	
	public void search(String id, 
			String firstName,
			String lastName,
			String email,
			String htDonVi,
			String groupId){
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.email = email;
		this.maHtDonVi = htDonVi;
		this.groupId = groupId;
		currentPage = 1;
		reloadData();
	}

}

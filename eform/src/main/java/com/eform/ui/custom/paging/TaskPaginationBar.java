package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;

import com.eform.common.utils.TaskUtils;
import com.eform.ui.custom.paging.base.PaginationBarForListComponent;
import com.eform.ui.view.task.TaskListView;
import com.vaadin.ui.Component;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Button.ClickListener;

public class TaskPaginationBar extends PaginationBarForListComponent<Task> {

	private TaskService taskService;
	private RepositoryService repositoryService;
	private String category;
	private List<String> groupIds;
	private String userId;
	private  ClickListener clickListener;
	
	public TaskPaginationBar(Layout wrapper, TaskService taskService,RepositoryService repositoryService, String category, List<String> groupIds, String userId,  ClickListener clickListener) {
		super(wrapper);
		this.taskService = taskService;
		this.repositoryService = repositoryService;
		this.category = category;
		this.userId = userId;
		this.groupIds = groupIds;
		this.clickListener = clickListener;
		reloadData();
	}

	@Override
	public long getRowCount() {
		Long count = 0L;
		if(taskService != null){
			if(TaskListView.INBOX_CATE.equals(category)){
				count = taskService.createTaskQuery().taskAssignee(userId).active().count();
			}else if(TaskListView.MY_TASK_CATE.equals(category)){
				count = taskService.createTaskQuery().taskOwner(userId).active().count();
			}else if(TaskListView.INVOLED_CATE.equals(category)){
				count = taskService.createTaskQuery().taskInvolvedUser(userId).active().count();
			}else if(TaskListView.QUEUED_CATE.equals(category)){
				count = taskService.createTaskQuery().taskCandidateGroupIn(groupIds).active().count();
			}
		}
		return count;
	}

	@Override
	public List<Task> getDataList(int firstResult, int maxResults) {
		List<Task> taskList = new ArrayList();
		if(taskService != null){
			if(TaskListView.INBOX_CATE.equals(category)){
				taskList = taskService.createTaskQuery().taskAssignee(userId).active().orderByTaskPriority().desc().orderByTaskCreateTime().desc().listPage(firstResult, maxResults);
			}else if(TaskListView.MY_TASK_CATE.equals(category)){
				taskList = taskService.createTaskQuery().taskOwner(userId).active().orderByTaskPriority().desc().orderByTaskCreateTime().desc().listPage(firstResult, maxResults);
			}else if(TaskListView.INVOLED_CATE.equals(category)){
				taskList = taskService.createTaskQuery().taskInvolvedUser(userId).active().orderByTaskPriority().orderByTaskCreateTime().desc().listPage(firstResult, maxResults);
			}else if(TaskListView.QUEUED_CATE.equals(category)){
				taskList = taskService.createTaskQuery().taskCandidateGroupIn(groupIds).active().orderByTaskPriority().desc().orderByTaskCreateTime().desc().listPage(firstResult, maxResults);
			}
		}
		return taskList;
	}
	
	public void search(String name, 
			String category,
			String userId,
			List<String> groupIds){
		this.category = category;
		this.userId = userId;
		this.groupIds = groupIds;
		currentPage = 1;
		reloadData();
	}

	@Override
	public Component builItem(Task task, int i) {
		return TaskUtils.buildItem(task, i, clickListener, repositoryService);
	}

}

package com.eform.ui.custom;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.log4j.Logger;

import com.eform.common.Constants;
import com.eform.common.utils.FileUtils;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;

public class UploadFileInfo implements Receiver, Upload.StartedListener, Upload.ProgressListener,
Upload.FailedListener, Upload.SucceededListener, Upload.FinishedListener{
	
	private Long maxSize;
	private List<String> fileAlows;
	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();
	private final String uploadPath;
	static Logger log = Logger.getLogger(UploadFileInfo.class.getName());
	
	public UploadFileInfo(Long maxSize, List<String> fileAllows, String uploadPath){
		super();
		this.fileAlows = fileAllows;
		this.maxSize = maxSize;
		this.uploadPath = uploadPath;
		fieldGroup.setItemDataSource(itemValue);
	}

	@Override
	public void uploadStarted(StartedEvent event) {
		System.out.println("upload started");
		//Kiem tra file upload len
		String fileName = event.getFilename();
		//Kiem tra dinh dang file
		String ext = FileUtils.getFileExtension(fileName);
		if(fileAlows != null && ext != null && !fileAlows.contains(ext)){
			event.getUpload().interruptUpload();
            fileAlows.toString();
            Notification notf = new Notification("Thông báo", "Định dạng file cho phép là "+fileAlows.toString(), Notification.Type.WARNING_MESSAGE);
			notf.setDelayMsec(5000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
			return;
		}
		//Dung luong file upload
		long contentLength = event.getContentLength();
		//Kiem tra dung luong file
		if (maxSize != null && contentLength > maxSize) {
			event.getUpload().interruptUpload();
            Notification notf = new Notification("Thông báo", "File vượt quá dung lượng cho phép "+maxSize+" bytes", Notification.Type.WARNING_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
            return;
        }
	}
	
	@Override
	public void updateProgress(long readBytes, long contentLength) {
		System.out.println("upload updateProcess");
	}
	
	@Override
	public void uploadFinished(FinishedEvent event) {
		System.out.println("upload finished");
	}

	@Override
	public void uploadSucceeded(SucceededEvent event) {
		System.out.println("upload successed");
		String fileNameStr = event.getFilename();
		if(fileNameStr != null){
        	String title = fileNameStr.substring(0, fileNameStr.lastIndexOf("."));
	        FileUtils.uploadToDocumentList(uploadPath+fileNameStr, fileNameStr, title, title);
        }
	}

	@Override
	public void uploadFailed(FailedEvent event) {
		System.out.println("upload failed");
	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		System.out.println("receiveUpload");
        File file = null;
        FileOutputStream fos = null;
        String folder = uploadPath;
        try {
        	
        	FileUtils.createIfNotExistFolder(folder);
            file = new File(folder + filename);
            fos = new FileOutputStream(file);
        } catch (final java.io.FileNotFoundException e) {
        	log.error("Error description",e);
            Notification notf = new Notification("Thông báo", "Không thể upload file này!", Type.ERROR_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
            return null;
        }
        return fos;
	}

}

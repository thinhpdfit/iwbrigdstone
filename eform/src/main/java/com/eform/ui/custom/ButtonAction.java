package com.eform.ui.custom;

import com.eform.common.ExplorerLayout;
import com.eform.common.workflow.Workflow;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Layout;
import com.vaadin.ui.themes.Reindeer;

public class ButtonAction<E> {
	
	public static final String BTN_EDIT_ID = "btn-edit-id";
	public static final String BTN_DEL_ID = "btn-del-id";
	public static final String BTN_APPROVE_ID = "btn-approve-id";
	public static final String BTN_REFUSE_ID = "btn-refuse-id";
	
	public void getButtonAction(Layout actionWrap, Workflow<E> wf, ClickListener listener){
		
		if(wf.isSua()){
        	Button btnEdit = new Button();
        	btnEdit.setId(BTN_EDIT_ID);
        	btnEdit.setDescription("Sửa");
        	btnEdit.setData(wf.getObject());
        	btnEdit.addStyleName(ExplorerLayout.BUTTON_ACTION);
        	btnEdit.addStyleName(Reindeer.BUTTON_LINK);
        	btnEdit.setIcon(FontAwesome.PENCIL_SQUARE_O);
        	btnEdit.addClickListener(listener);
	        actionWrap.addComponent(btnEdit);
        }
        
        if(wf.isXoa()){
        	Button btnDel = new Button();
        	btnDel.setId(BTN_DEL_ID);
        	btnDel.setDescription("Xóa");
        	btnDel.addStyleName(ExplorerLayout.BUTTON_ACTION);
        	btnDel.addStyleName(Reindeer.BUTTON_LINK);
        	btnDel.setData(wf.getObject());
        	btnDel.setIcon(FontAwesome.REMOVE);
        	btnDel.addClickListener(listener);
	        actionWrap.addComponent(btnDel);
        }
        
        if(wf.isPheDuyet()){
        	Button btnApprove = new Button();
        	btnApprove.setId(BTN_APPROVE_ID);
        	btnApprove.setData(wf.getObject());
        	btnApprove.setDescription(wf.getPheDuyetTen());
        	btnApprove.setIcon(FontAwesome.CHECK);
        	btnApprove.addStyleName(ExplorerLayout.BUTTON_ACTION);
        	btnApprove.addStyleName(Reindeer.BUTTON_LINK);
        	btnApprove.addClickListener(listener);
	        actionWrap.addComponent(btnApprove);
        }
        
        if(wf.isTuChoi()){
        	Button btn = new Button();
        	btn.setId(BTN_REFUSE_ID);
        	btn.setDescription(wf.getTuChoiTen());
        	btn.setData(wf.getObject());
        	btn.setIcon(FontAwesome.LEVEL_DOWN);
        	btn.addStyleName(ExplorerLayout.BUTTON_ACTION);
        	btn.addStyleName(Reindeer.BUTTON_LINK);
        	btn.addClickListener(listener);
	        actionWrap.addComponent(btn);
        }
        
	}
}

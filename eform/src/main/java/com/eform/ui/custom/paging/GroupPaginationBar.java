package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;

import com.eform.dao.HtChucVuDao;
import com.eform.model.entities.HtChucVu;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class GroupPaginationBar extends PaginationBar<Group> {

	protected IdentityService identityService;
	private String id;
	private String name;
	private String type;
	
	public GroupPaginationBar(BeanItemContainer<Group> container, IdentityService identityService) {
		super(container);
		this.identityService = identityService;
		reloadData();
	}

	@Override
	public long getRowCount() {
		Long count = 0L;
		if(identityService != null){
			if(name == null){
				name = "";
			}
			if(id != null && !id.isEmpty()){
				if(type != null && !type.isEmpty()){
					count = identityService.createGroupQuery().groupId(id).groupNameLike("%"+name+"%").groupType(type).count();
				}else{
					count = identityService.createGroupQuery().groupId(id).groupNameLike("%"+name+"%").count();
				}
				
			}else{
				if(type != null && !type.isEmpty()){
					count = identityService.createGroupQuery().groupNameLike("%"+name+"%").groupType(type).count();
				}else{
					count = identityService.createGroupQuery().groupNameLike("%"+name+"%").count();
				}
			}
		}
		return count;
	}

	@Override
	public List<Group> getDataList(int firstResult, int maxResults) {
		List<Group> groupList = new ArrayList<Group>();
		if(identityService != null){
			if(name == null){
				name = "";
			}
			if(id != null && !id.isEmpty()){
				if(type != null && !type.isEmpty()){
					groupList = identityService.createGroupQuery().groupId(id).groupNameLike("%"+name+"%").groupType(type).listPage(firstResult, maxResults);
				}else{
					groupList = identityService.createGroupQuery().groupId(id).groupNameLike("%"+name+"%").listPage(firstResult, maxResults);
				}
				
			}else{
				if(type != null && !type.isEmpty()){
					groupList = identityService.createGroupQuery().groupNameLike("%"+name+"%").groupType(type).listPage(firstResult, maxResults);
				}else{
					groupList = identityService.createGroupQuery().groupNameLike("%"+name+"%").listPage(firstResult, maxResults);
				}
			}
		}
		return groupList;
	}
	
	public void search(String id, 
			String name,
			String type){
		this.id = id;
		this.name = name;
		this.type = type;
		currentPage = 1;
		reloadData();
	}

}

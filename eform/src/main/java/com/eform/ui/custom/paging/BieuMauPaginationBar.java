package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import com.eform.model.entities.BieuMau;
import com.eform.service.BieuMauService;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class BieuMauPaginationBar extends PaginationBar<BieuMau> {

	private BieuMauService bieuMauService;
	private String ma;
	private List<String> maList;
	private String ten;
	private List<Long> trangThai;
	private Long loaiBieuMau;
	
	public BieuMauPaginationBar(BeanItemContainer<BieuMau> container, BieuMauService bieuMauService) {
		super(container);
		this.bieuMauService = bieuMauService;
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(bieuMauService != null){
			return bieuMauService.getBieuMauFind(ma,maList, ten, loaiBieuMau, trangThai);
		}
		return 0L;
	}

	@Override
	public List<BieuMau> getDataList(int firstResult, int maxResults) {
		if(bieuMauService != null){
			return bieuMauService.getBieuMauFind(ma,maList, ten, loaiBieuMau, trangThai, firstResult, maxResults);
		}
		return new ArrayList<BieuMau>();
	}
	
	public void search(String ma, List<String> maList, 
			String ten, 
			Long loaiBieuMau, 
			List<Long> trangThai){
		this.ma = ma;
		this.maList = maList;
		this.ten = ten;
		this.trangThai =trangThai;
		this.loaiBieuMau = loaiBieuMau;
		currentPage = 1;
		reloadData();
	}

}

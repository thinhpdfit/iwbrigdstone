package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.service.DanhMucService;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class DanhMucPaginationBar extends PaginationBar<DanhMuc> {

	private DanhMucService danhMucService;
	private String ma;
	private String ten;
	private List<Long> trangThai;
	private LoaiDanhMuc loaiDanhMuc;
	private DanhMuc danhMuc;
	
	public DanhMucPaginationBar(BeanItemContainer<DanhMuc> container, DanhMucService danhMucService) {
		super(container);
		this.danhMucService = danhMucService;
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(danhMucService != null){
			return danhMucService.getDanhMucFind(ma, ten, trangThai, loaiDanhMuc, danhMuc);
		}
		return 0L;
	}

	@Override
	public List<DanhMuc> getDataList(int firstResult, int maxResults) {
		if(danhMucService != null){
			return danhMucService.getDanhMucFind(ma, ten, trangThai, loaiDanhMuc, danhMuc, firstResult, maxResults);
		}
		return new ArrayList();
	}
	
	public void search(String ma, 
			String ten, 
			List<Long> trangThai, 
			LoaiDanhMuc loaiDanhMuc,
			DanhMuc danhMuc){
		this.ma = ma;
		this.ten = ten;
		this.trangThai =trangThai;
		this.loaiDanhMuc = loaiDanhMuc;
		this.danhMuc = danhMuc;
		currentPage = 1;
		reloadData();
	}

}

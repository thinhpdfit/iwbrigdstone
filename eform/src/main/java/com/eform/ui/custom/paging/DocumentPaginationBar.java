package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.eform.common.SpringApplicationContext;
import com.eform.service.HoSoService;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class DocumentPaginationBar extends PaginationBar<Map<String, Object>> {

	private HoSoService hoSoService;
	private String sql;
	private Object [] objParams;
	private int[] types;
	private Map<String, String> paramStringMap;
	
	public DocumentPaginationBar(String sql, Object [] objParams, int[] types,BeanItemContainer<Map<String, Object>> container) {
		super(container);
		this.sql = sql;
		this.objParams = objParams;
		this.types = types;
		hoSoService = SpringApplicationContext.getApplicationContext().getBean(HoSoService.class);
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(hoSoService != null){
			return hoSoService.getHoSoJsonByKey(sql, objParams, types).size();
		}
		return 0L;
	}

	@Override
	public List<Map<String, Object>> getDataList(int firstResult, int maxResults) {
		if(hoSoService != null){
			String sqlTmp = sql + " limit "+maxResults+" offset "+firstResult;
			return hoSoService.getHoSoJsonByKey(sqlTmp, objParams, types);
		}
		return new ArrayList<Map<String, Object>>();
	}
	
	public List<Map<String, Object>> getDataList() {
		if(hoSoService != null){
			return hoSoService.getHoSoJsonByKey(sql, objParams, types);
		}
		return new ArrayList<Map<String, Object>>();
	}
	
	public void search(String sql, Object[] objParams, int[] types){
		this.sql = sql;
		this.objParams = objParams;
		this.types = types;
		currentPage = 1;
		reloadData();
	}
	public void search(String sql, Object[] objParams, int[] types, Map<String, String> paramStringMap){
		this.sql = sql;
		this.objParams = objParams;
		this.types = types;
		this.paramStringMap = paramStringMap;
		currentPage = 1;
		reloadData();
	}

	public Map<String, String> getParamStringMap() {
		return paramStringMap;
	}


}

package com.eform.ui.custom;

import java.util.List;

import com.eform.common.ExplorerLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;

public class CssFormLayout extends CssLayout{

	private static final long serialVersionUID = 1L;
	private CssLayout buttonRow;
	private CssLayout buttonWrap;
	private int labelWrapWith;
	private int feildContentWith;

	public CssFormLayout(){
		this(-1, -1);
	}
	
	public CssFormLayout(int labelWrapWith, int feildContentWith){
		this.labelWrapWith = labelWrapWith;
		this.feildContentWith = feildContentWith;
		addStyleName(ExplorerLayout.SEARCH_FORM);
		initButtonRow();
	}
	
	public void initButtonRow(){
		buttonRow = new CssLayout();
		buttonRow.addStyleName(ExplorerLayout.SEARCH_FORM_ROW);
		buttonRow.addStyleName(ExplorerLayout.SEARCH_FORM_BUTTON_ROW);
		
		CssLayout lableWrap = new CssLayout();
		lableWrap.setStyleName(ExplorerLayout.SEARCH_FORM_LABEL_WRAP);
		buttonRow.addComponent(lableWrap);
		
		buttonWrap = new CssLayout();
		buttonWrap.addStyleName(ExplorerLayout.SEARCH_FORM_TEXTFIELD_WRAP);
		buttonWrap.addStyleName(ExplorerLayout.SEARCH_FORM_BUTTON_WRAP);
		buttonRow.addComponent(buttonWrap);
	}
	
	public void addButton(Button button){
		button.addStyleName(ExplorerLayout.BUTTON_SEARCH);
		buttonWrap.addComponent(button);
		if(this.getComponentIndex(buttonRow) < 0){
			addComponent(buttonRow);
		}
	}
	
	public void addFeild(Component component){
		CssFormRowLayout row = new CssFormRowLayout(component.getCaption(), component, labelWrapWith, feildContentWith);
		component.setCaption(null);
		addRow(row);
	}
	public void addFeild(Component component, String caption){
		CssFormRowLayout row = new CssFormRowLayout(caption, component, labelWrapWith, feildContentWith);
		component.setCaption(null);
		addRow(row);
	}
	
	public void addRow(CssFormRowLayout row){
		addComponent(row);
	}
	
	public void addRows(List<CssFormRowLayout> rows){
		if (rows != null && !rows.isEmpty()) {
			for (CssFormRowLayout row : rows) {
				addComponent(row);
			}
		}
	}

}

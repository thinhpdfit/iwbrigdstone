package com.eform.ui.custom;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.apache.commons.lang3.StringUtils;

import com.eform.common.SpringApplicationContext;
import com.eform.common.utils.XmlUtil;
import com.eform.ui.view.process.ModelerContainer;
import com.eform.ui.view.process.ModelerListView;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.FinishedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.StartedListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.Window;

public class UploadBpmnInfo implements Receiver, FinishedListener, StartedListener, SucceededListener{

	// Will be assigned during upload
	protected ByteArrayOutputStream outputStream;
	protected String fileName;
	// Will be assigned after deployment
	protected boolean validFile = false;
	private RepositoryService repositoryService;
	private Window uploadBpmnWindow;
	private ModelerContainer container;
	
	
	public UploadBpmnInfo(Window uploadBpmnWindow, ModelerContainer container){
		repositoryService = SpringApplicationContext.getApplicationContext().getBean(RepositoryService.class);
		this.uploadBpmnWindow = uploadBpmnWindow;
		this.container = container;
	}
	
	@Override
	public void uploadStarted(StartedEvent event) {
		System.out.println("upload started");
		String fileName = event.getFilename();
		if(!(fileName.endsWith(".bpmn20.xml") || fileName.endsWith(".bpmn"))){
			event.getUpload().interruptUpload();
            Notification notf = new Notification("Thông báo", "Định dạng file cho phép là .bpmn20.xml hoặc .bpmn", Notification.Type.WARNING_MESSAGE);
			notf.setDelayMsec(5000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
			return;
		}
	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		this.fileName = filename;
	    this.outputStream = new ByteArrayOutputStream();
	    return outputStream;
	}

	@Override
	public void uploadFinished(FinishedEvent event) {
		System.out.println("FinishedEvent");
		
	}
	
	protected boolean deployUploadedFile(ByteArrayOutputStream outputStream) {
		try {
			try {
				XMLInputFactory xif = XmlUtil.createSafeXmlInputFactory();
				InputStreamReader in = new InputStreamReader(new ByteArrayInputStream(outputStream.toByteArray()),
						"UTF-8");
				XMLStreamReader xtr = xif.createXMLStreamReader(in);
				BpmnModel bpmnModel = new BpmnXMLConverter().convertToBpmnModel(xtr);

				if (bpmnModel.getMainProcess() == null || bpmnModel.getMainProcess().getId() == null) {
					// MODEL_IMPORT_INVALID_BPMN_EXPLANATION
					Notification notf = new Notification("Thông báo", "File tải lên không đúng cấu trúc", Notification.Type.WARNING_MESSAGE);
					notf.setDelayMsec(5000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
				} else {

					if (bpmnModel.getLocationMap().isEmpty()) {
						// MODEL_IMPORT_INVALID_BPMNDI_EXPLANATION
						Notification notf = new Notification("Thông báo", "File tải lên không đúng cấu trúc", Notification.Type.WARNING_MESSAGE);
						notf.setDelayMsec(5000);
						notf.setPosition(Position.TOP_CENTER);
						notf.show(Page.getCurrent());
					} else {

						String processName = null;
						if (StringUtils.isNotEmpty(bpmnModel.getMainProcess().getName())) {
							processName = bpmnModel.getMainProcess().getName();
						} else {
							processName = bpmnModel.getMainProcess().getId();
						}

						Model modelData = repositoryService.newModel();
						ObjectNode modelObjectNode = new ObjectMapper().createObjectNode();
						modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME, processName);
						modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, 1);
						modelData.setMetaInfo(modelObjectNode.toString());
						modelData.setName(processName);

						repositoryService.saveModel(modelData);

						BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
						ObjectNode editorNode = jsonConverter.convertToJson(bpmnModel);

						repositoryService.addModelEditorSource(modelData.getId(),
								editorNode.toString().getBytes("utf-8"));
						return true;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				String errorMsg = e.getMessage().replace(System.getProperty("line.separator"), "<br/>");
				Notification notf = new Notification("Thông báo", errorMsg, Notification.Type.WARNING_MESSAGE);
				notf.setDelayMsec(5000);
				notf.setPosition(Position.TOP_CENTER);
				notf.show(Page.getCurrent());
			}
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
					// "Server-side error", e.getMessage();
					Notification notf = new Notification("Thông báo", e.getMessage(), Notification.Type.WARNING_MESSAGE);
					notf.setDelayMsec(5000);
					notf.setPosition(Position.TOP_CENTER);
					notf.show(Page.getCurrent());
				}
			}
		}
		return false;
	}

	@Override
	public void uploadSucceeded(SucceededEvent event) {
		System.out.println("SucceededEvent");
		boolean result = deployUploadedFile(outputStream);
		if(result){
			uploadBpmnWindow.close();
			Notification notf = new Notification("Thông báo", "Import thành công!");
			notf.setDelayMsec(5000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
			container.getPaginationBar().reloadData();
		}
	}

}

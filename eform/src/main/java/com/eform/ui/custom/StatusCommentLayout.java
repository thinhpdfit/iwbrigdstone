package com.eform.ui.custom;

import java.util.List;

import com.eform.common.ExplorerLayout;
import com.eform.common.type.StatusModel;
import com.eform.common.workflow.ITrangThai;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;

public class StatusCommentLayout extends CssLayout{
	
	public StatusCommentLayout(List trangThaiList){
		super();
		this.setWidth("100%");
		addStyleName(ExplorerLayout.STATUS_COMMENT_WRAP);
		
		for(Object trangThaiObj : trangThaiList){
			String ten = "";
			String styleName = "";
			if(trangThaiObj instanceof ITrangThai){
				ITrangThai trangThai = (ITrangThai) trangThaiObj;
				ten = trangThai.getTen();
				styleName = trangThai.getStyleName();
			}
			if(trangThaiObj instanceof StatusModel){
				StatusModel trangThai = (StatusModel) trangThaiObj;
				ten = trangThai.getName();
				styleName = trangThai.getStyleName();
			}
			Label lblStatus = new Label(ten);
			lblStatus.setCaption("");
			lblStatus.addStyleName(ExplorerLayout.STATUS_COMMENT);
			lblStatus.addStyleName("cmt-"+styleName);
			addComponent(lblStatus);
		}
	}
	
}

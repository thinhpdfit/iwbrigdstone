package com.eform.ui.custom;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.utils.FileUtils;
import com.eform.sharepoint.service.SharepointServices;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Window;

public class UploadInfoWindow extends Window implements Upload.StartedListener, Upload.ProgressListener,
		Upload.FailedListener, Upload.SucceededListener, Upload.FinishedListener{
	/**
		 * 
		 */
	private static final long serialVersionUID = 4196846812116977656L;
	private final Label state = new Label();
	private final Label fileName = new Label();
	private final Label textualProgress = new Label();
	private String fileNameStr;

    private long maxSize;
	private final String uploadPath;

	private final ProgressBar progressBar = new ProgressBar();
	private final Button cancelButton;
	private final UploadReceive receive;
	private Upload upload;
	private CssLayout fileListLayout;

	private final String BTN_REMOVE_ID = "btnRemove";
	private final String BTN_DOWNLOAD_ID = "btnDownload";
	
	private final ClickListener listener;
	private final List<String> fileList;
	private final List<String> fileAlows;

	public UploadInfoWindow(final Upload upload, CssLayout fileListLayout, ClickListener listener, final UploadReceive receive, final List<String> fileList, long maxSize, List<String> fileAlows, String uploadPath) {
		super("Upload File");
		this.fileAlows = fileAlows;
		this.fileList = fileList;
		this.uploadPath = uploadPath;
		this.receive = receive;
		this.listener = listener;
		this.maxSize = maxSize;
		this.upload = upload;
		this.fileListLayout = fileListLayout;

		setWidth(350, Unit.PIXELS);

		addStyleName("upload-info");

		setResizable(false);
		setDraggable(false);

		final FormLayout l = new FormLayout();
		setContent(l);
		l.setMargin(true);

		final HorizontalLayout stateLayout = new HorizontalLayout();
		stateLayout.setSpacing(true);
		stateLayout.addComponent(state);

		cancelButton = new Button("Cancel");
		cancelButton.addClickListener(new Button.ClickListener() {
			private static final long serialVersionUID = -4322009183207539358L;

			@Override
			public void buttonClick(final ClickEvent event) {
				upload.interruptUpload();
			}
		});
		cancelButton.setVisible(false);
		cancelButton.setStyleName("small");
		stateLayout.addComponent(cancelButton);

		stateLayout.setCaption("Trạng thái");
		state.setValue("Dừng");
		l.addComponent(stateLayout);

		fileName.setCaption("Tên file");
		l.addComponent(fileName);

		progressBar.setCaption("Tiến trình");
		progressBar.setVisible(false);
		l.addComponent(progressBar);

		textualProgress.setVisible(false);
		l.addComponent(textualProgress);

		upload.addStartedListener(this);
		upload.addProgressListener(this);
		upload.addFailedListener(this);
		upload.addSucceededListener(this);
		upload.addFinishedListener(this);

	}

	@Override
	public void uploadFinished(final FinishedEvent event) {
		System.out.println("uploadFinished");
		progressBar.setVisible(false);
		textualProgress.setVisible(false);
		cancelButton.setVisible(false);
		
	}

	@Override
	public void uploadStarted(final StartedEvent event) {
		System.out.println("uploadStarted");
		// this method gets called immediately after upload is started
		progressBar.setValue(0f);
		progressBar.setVisible(true);
		UI.getCurrent().setPollInterval(500);
		textualProgress.setVisible(true);
		// updates to client
		state.setValue("Đang tải lên...");
		fileName.setValue(event.getFilename());
		fileNameStr = event.getFilename();
		String ext = FileUtils.getFileExtension(fileNameStr);
		if(fileAlows != null && !(ext != null && fileAlows.contains(ext))){
			upload.interruptUpload();
            state.setValue("Tải lên không thành công");
            String fileAlowStr = "";
            int i = 0;
            for(String str : fileAlows){
            	i++;
            	if(i == fileAlows.size()){
            		fileAlowStr += str;
            	}else{
            		fileAlowStr += str+"; ";
            	}
            }
            Notification notf = new Notification("Thông báo", "Định dạng file cho phép là "+fileAlowStr, Notification.Type.WARNING_MESSAGE);
			notf.setDelayMsec(5000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
            return;
		}

		cancelButton.setVisible(true);
	}

	@Override
	public void updateProgress(final long readBytes, final long contentLength) {
		System.out.println("updateProgress");
		if (maxSize < contentLength) {
            upload.interruptUpload();
            state.setValue("Tải lên không thành công");
            Notification notf = new Notification("Thông báo", "File vượt quá dung lượng cho phép "+maxSize+" bytes", Notification.Type.WARNING_MESSAGE);
			notf.setDelayMsec(3000);
			notf.setPosition(Position.TOP_CENTER);
			notf.show(Page.getCurrent());
            return;
        }
		
		// this method gets called several times during the update
		progressBar.setValue(new Float(readBytes / (float) contentLength));
		textualProgress.setValue("" + readBytes + " bytes/ " + contentLength);
	}

	@Override
	public void uploadSucceeded(final SucceededEvent event) {
		System.out.println("uploadSucceeded");
		try {
			TextField data = (TextField) upload.getData();
			String dataValue = data.getValue();
			if(dataValue != null && !dataValue.isEmpty()){
				dataValue += ":"+fileNameStr;
			}else{
				dataValue = fileNameStr;
			}
			data.setValue(dataValue);
			
	        state.setValue("Tải lên thành công");
	        fileList.add(fileNameStr);
	        if(fileListLayout != null){
				fileListLayout.addComponent(createFileItem(fileNameStr, fileNameStr, fileList, data));
			}
	        
	        if(fileNameStr != null){
	        	String title = fileNameStr.substring(0, fileNameStr.lastIndexOf("."));
		        FileUtils.uploadToDocumentList(uploadPath+fileNameStr, fileNameStr, title, title);
	        }
	        
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void uploadFailed(final FailedEvent event) {
		System.out.println("uploadFailed"+Math.round(100 * progressBar.getValue()));
	}
	
	public CssLayout createFileItem(String fileName, String fileNameStr, List<String> fileList, TextField hiddenTxt){
		Map<String, Object> dataMap = new HashMap();
		dataMap.put("FILE_NAME", fileNameStr);
		dataMap.put("FILE_LIST", fileList);
		dataMap.put("HIDDEN_TXT", hiddenTxt);
		
		CssLayout fileItem = new CssLayout();
		fileItem.setStyleName(ExplorerLayout.FILE_ITEM);
		
		Label lbl = new Label(fileName);
		lbl.setStyleName(ExplorerLayout.FILE_ITEM_LBL);
		
		fileItem.addComponent(lbl);
		
		CssLayout fileButtonWrap = new CssLayout();
		fileButtonWrap.setStyleName(ExplorerLayout.FILE_BTN_WRAP);

		Button btnDownload = new Button();
		btnDownload.setIcon(FontAwesome.ARROW_DOWN);
		btnDownload.setData(dataMap);
		btnDownload.addClickListener(listener);
		FileUtils.downloadWithButton(btnDownload, uploadPath+fileNameStr);
		btnDownload.setId(BTN_DOWNLOAD_ID);
		btnDownload.setStyleName(ExplorerLayout.FILE_ITEM_BTN_DOWNLOAD);
		fileButtonWrap.addComponent(btnDownload);
		
		Button btnDel = new Button();
		btnDel.setIcon(FontAwesome.REMOVE);
		btnDel.setData(dataMap);
		btnDel.addClickListener(listener);
		btnDel.setId(BTN_REMOVE_ID);
		btnDel.setStyleName(ExplorerLayout.FILE_ITEM_BTN_REMOVE);
		fileButtonWrap.addComponent(btnDel);
		
		fileItem.addComponent(fileButtonWrap);
		
		return fileItem;
	}
	
	
	

	
}
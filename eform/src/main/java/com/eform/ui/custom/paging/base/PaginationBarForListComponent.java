package com.eform.ui.custom.paging.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.eform.common.ExplorerLayout;
import com.eform.common.SpringApplicationContext;
import com.eform.common.ThamSo;
import com.eform.model.entities.HtThamSo;
import com.eform.service.HtThamSoService;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;

public abstract class PaginationBarForListComponent<T> extends CssLayout implements ClickListener, Serializable{
	private static final long serialVersionUID = 6669010373742153163L;
	
	private Layout wrapper;
	
	protected int pageSize = 5;
	protected int currentPage;
	protected long rowCount;
	protected int totalPage;
	protected List<T> dataList;

	private Button btnLast;
	private Button btnNext;
	private Button btnPrev;
	private Button btnFirst;
	private Label lblPage;
	private Label lblCount;
	
	private TextField txtPageIndex;
	private NativeSelect cbbPageSizeCustom;

	private final PropertysetItem itemValue = new PropertysetItem();
	private final FieldGroup fieldGroup = new FieldGroup();
	
	@Autowired
	private HtThamSoService htThamSoService;
	
	private List<Integer> pageSizeList;
	
	//Phan trang cho list component item trong wrapper
	public PaginationBarForListComponent(Layout wrapper){
		super();
		init();
		this.wrapper = wrapper;
	}
	
	public void init(){
		pageSizeList = new ArrayList<Integer>(); //Mac dinh
		pageSizeList.add(5);
		pageSizeList.add(10);
		pageSizeList.add(25);
		pageSizeList.add(50);
		
		if(htThamSoService == null){
			htThamSoService = SpringApplicationContext.getApplicationContext().getBean(HtThamSoService.class);
		}
		HtThamSo htThamSo = htThamSoService.getHtThamSoFind(ThamSo.PAGE_SIZE);
		if(htThamSo != null && htThamSo.getGiaTri() != null && htThamSo.getGiaTri().trim().matches("[0-9]+")){
			pageSize = Integer.parseInt(htThamSo.getGiaTri().trim());
		}
		htThamSo = htThamSoService.getHtThamSoFind(ThamSo.PAGE_SIZE_CUSTOM);
		if(htThamSo != null && htThamSo.getGiaTri() != null && !htThamSo.getGiaTri().trim().isEmpty()){
			String pageSizeCustomStr = htThamSo.getGiaTri().trim();
			String arr [] = pageSizeCustomStr.split(",");
			if(arr != null && arr.length > 0){
				List<Integer> pageSizeListTmp = new ArrayList<Integer>(); 
				for(String str : arr){
					if(str != null && str.trim().matches("[0-9]+")){
						Integer size = Integer.parseInt(str.trim());
						pageSizeListTmp.add(size);
					}
				}
				if(!pageSizeListTmp.isEmpty()){
					pageSizeList.clear();
					pageSizeList.addAll(pageSizeListTmp);
				}
			}
		}
		
		
		if(pageSizeList != null && !pageSizeList.contains(pageSize)){
			pageSizeList.add(0, pageSize);
		}
		
		currentPage = 1;
		setStyleName(ExplorerLayout.PAGINATION_BAR);
		createPaginationBar();
		fieldGroup.setItemDataSource(itemValue);
	}
	
	public abstract long getRowCount();
	public abstract List<T> getDataList(int firstResult, int maxResults);
	public abstract Component builItem(T t, int index);
	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void createPaginationBar(){
		CssLayout left = new CssLayout();
		left.setStyleName(ExplorerLayout.PAGINATION_BAR_LEFT);
		
		cbbPageSizeCustom = new NativeSelect();
		cbbPageSizeCustom.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Integer value = (Integer) event.getProperty().getValue();
				if(value != null){
					pageSize = value;
					reloadData();
				}
			}
		});
		
		cbbPageSizeCustom.setStyleName(ExplorerLayout.PAGINATION_CUSTOM_PAGE_SIZE);
		cbbPageSizeCustom.setId("CBB_CUSTOM_PAGE_SIZE");
		if(pageSizeList != null && !pageSizeList.isEmpty()){
			for(int size : pageSizeList){
				cbbPageSizeCustom.addItem(size);
			}
		}
		itemValue.addItemProperty("pageSize", new ObjectProperty<Integer>(pageSize, Integer.class));
		fieldGroup.bind(cbbPageSizeCustom, "pageSize");
		left.addComponent(cbbPageSizeCustom);
		
		lblCount = new Label();
		lblCount.setStyleName(ExplorerLayout.LBL_COUNT);
		left.addComponent(lblCount);
		
		CssLayout right = new CssLayout();
		right.setStyleName(ExplorerLayout.PAGINATION_BAR_RIGHT);
		
		btnFirst = new Button();
		btnFirst.setIcon(FontAwesome.STEP_BACKWARD);
		btnFirst.addClickListener(this);
		btnFirst.setStyleName(ExplorerLayout.PAGINATION_BUTTON_FIRST);
		right.addComponent(btnFirst);
		
		btnPrev = new Button();
		btnPrev.setIcon(FontAwesome.BACKWARD);
		btnPrev.addClickListener(this);
		btnPrev.setStyleName(ExplorerLayout.PAGINATION_BUTTON_PREV);
		right.addComponent(btnPrev);
		
		txtPageIndex = new TextField();
		txtPageIndex.setConverter(Integer.class);
		txtPageIndex.setRequired(true);
		txtPageIndex.setRequiredError("Không được để trống");
		txtPageIndex.addValidator(new IntegerRangeValidator("Chỉ chấp nhận số nguyên dương", 1, null));
		txtPageIndex.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				String value = (String) event.getProperty().getValue();
				if(value != null && value.matches("[0-9]+")){
					int index = Integer.parseInt(value);
					if(index <= totalPage){
						currentPage = index;
						reloadData();
					}
				}
			}
		});
		
		
		txtPageIndex.setNullRepresentation("");
		txtPageIndex.setStyleName(ExplorerLayout.PAGINATION_PAGE_INDEX);
		txtPageIndex.setId("TXT_PAGE_INDEX");
		itemValue.addItemProperty("pageIndex", new ObjectProperty<Integer>(1, Integer.class));
		fieldGroup.bind(txtPageIndex, "pageIndex");
		right.addComponent(txtPageIndex);
		
		lblPage = new Label();
		lblPage.setStyleName(ExplorerLayout.LBL_PAGE);
		right.addComponent(lblPage);
		
		btnNext = new Button();
		btnNext.setIcon(FontAwesome.FORWARD);
		btnNext.addClickListener(this);
		btnNext.setStyleName(ExplorerLayout.PAGINATION_BUTTON_NEXT);
		right.addComponent(btnNext);
		
		btnLast = new Button();
		btnLast.setIcon(FontAwesome.STEP_FORWARD);
		btnLast.addClickListener(this);
		btnLast.setStyleName(ExplorerLayout.PAGINATION_BUTTON_LAST);
		right.addComponent(btnLast);

		addComponent(left);
		addComponent(right);
	}
	
	public void reloadData() {
		try {
			rowCount = getRowCount();
			lblCount.setValue(" /"+rowCount+" bản ghi");
			totalPage = (int) Math.ceil(1.0F*rowCount/pageSize);
			lblPage.setValue(" /"+totalPage);
            if(currentPage > 0 && currentPage <= totalPage){
            	int from = (currentPage-1)*pageSize;
            	this.dataList = getDataList(from, pageSize);
            	refresh();
            }
            if(rowCount == 0 && wrapper != null){
            	this.wrapper.removeAllComponents();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	
	public void refresh(){
		this.wrapper.removeAllComponents();
		if(dataList != null && !dataList.isEmpty()){
			int index = 0;
			for(T t : dataList){
				index++;
				wrapper.addComponent(builItem(t, index));
			}
		}
	}
	


	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if(btnFirst.equals(button)){
			currentPage = 1;
			reloadData();
		}else if(btnPrev.equals(button)){
			if(currentPage > 1){
				currentPage--;
				reloadData();
			}
		}else if(btnNext.equals(button)){
			if(currentPage < (int) Math.ceil(1.0F*rowCount/pageSize)){
				currentPage++;
				reloadData();
			}
		}else if(btnLast.equals(button)){
			currentPage = (int) Math.ceil(1.0F*rowCount/pageSize);
			reloadData();
		}
		txtPageIndex.setValue(currentPage+"");
	}
	
	public void fixEnterHotKey(Button btn) {
		txtPageIndex.addFocusListener(new FocusListener() {
			@Override
			public void focus(FocusEvent event) {
				btn.removeClickShortcut();
			}
		});
		txtPageIndex.addBlurListener(new BlurListener() {
			@Override
			public void blur(BlurEvent event) {
				btn.setClickShortcut(KeyCode.ENTER);
			}
		});
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public Label getLblCount() {
		return lblCount;
	}

	public void setLblCount(Label lblCount) {
		this.lblCount = lblCount;
	}
	
	

}

package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import com.eform.dao.HtChucVuDao;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.HtChucVu;
import com.eform.service.BieuMauService;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class HtChucVuPaginationBar extends PaginationBar<HtChucVu> {

	private HtChucVuDao htChucVuDao;
	private String ma;
	private String ten;
	private List<Long> trangThai;
	
	public HtChucVuPaginationBar(BeanItemContainer<HtChucVu> container, HtChucVuDao htChucVuDao) {
		super(container);
		this.htChucVuDao = htChucVuDao;
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(htChucVuDao != null){
			return htChucVuDao.getHtChucVuFind(ma, ten, trangThai);
		}
		return 0L;
	}

	@Override
	public List<HtChucVu> getDataList(int firstResult, int maxResults) {
		if(htChucVuDao != null){
			return htChucVuDao.getHtChucVuFind(ma, ten, trangThai, firstResult, maxResults);
		}
		return new ArrayList<HtChucVu>();
	}
	
	public void search(String ma, 
			String ten, 
			List<Long> trangThai){
		this.ma = ma;
		this.ten = ten;
		this.trangThai =trangThai;
		currentPage = 1;
		reloadData();
	}

}

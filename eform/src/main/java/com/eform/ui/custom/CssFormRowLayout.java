package com.eform.ui.custom;

import com.eform.common.ExplorerLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;

public class CssFormRowLayout extends CssLayout{
	private static final long serialVersionUID = 1L;
	
	private int columns;
	private CssLayout lableWrap;
	private CssLayout inputWrap;
	private Label label;
	private Component feild;
	private int labelWrapWith;
	private int feildContentWith;
	
	public CssFormRowLayout(String caption, Component feild){
		labelWrapWith = 30;
		feildContentWith = 100;
		this.label = new Label(caption);
		this.feild = feild;
		addStyleName(ExplorerLayout.SEARCH_FORM_ROW);
		initLayout();
	}
	
	public CssFormRowLayout(String caption, Component feild, int labelWrapWith, int feildContentWith){
		if(labelWrapWith > 100 || labelWrapWith < 0){
			labelWrapWith = 30;
		}
		this.labelWrapWith = labelWrapWith;
		if(feildContentWith > 100 || feildContentWith < 0){
			feildContentWith = 100;
		}
		this.feildContentWith = feildContentWith;
		this.label = new Label(caption);
		this.feild = feild;
		addStyleName(ExplorerLayout.SEARCH_FORM_ROW);
		initLayout();
	}

	public void initLayout(){
		lableWrap = new CssLayout();
		lableWrap.setWidth(labelWrapWith, Unit.PERCENTAGE);
		lableWrap.setStyleName(ExplorerLayout.SEARCH_FORM_LABEL_WRAP);
		if (label != null) {
			lableWrap.addComponent(label);
			label.addStyleName(ExplorerLayout.SEARCH_FORM_LABEL);
		}
		addComponent(lableWrap);
		inputWrap = new CssLayout();
		inputWrap.setWidth(100 - labelWrapWith, Unit.PERCENTAGE);
		inputWrap.setStyleName(ExplorerLayout.SEARCH_FORM_TEXTFIELD_WRAP);
		if (feild != null) {
			feild.setWidth(feildContentWith, Unit.PERCENTAGE);
			inputWrap.addComponent(feild);
			feild.addStyleName(ExplorerLayout.SEARCH_FORM_TEXTFIELD);
		}
		addComponent(inputWrap);
	}
	
	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}
	
}

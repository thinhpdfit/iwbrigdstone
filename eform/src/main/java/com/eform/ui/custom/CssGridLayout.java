package com.eform.ui.custom;


import com.eform.common.ExplorerLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;

public class CssGridLayout extends CssLayout{
	private int columns;
	private CssLayout currentRow;
	private int currentCell;
	
	public CssGridLayout(){
		setWidth("100%");
		addStyleName(ExplorerLayout.GRID_FORM_LAYOUT);
	}
	
	public void addComponent(Component component){
		if(currentCell == (columns - 1) || currentRow != null){
			currentRow = new CssLayout();
			currentRow.setWidth("100%");
			currentRow.addStyleName(ExplorerLayout.GRID_ROW_FORM_LAYOUT);
		}
		
		CssLayout cell = new CssLayout();
		cell.addStyleName(ExplorerLayout.GRID_CELL_FORM_LAYOUT);
		cell.addComponent(component);
		currentRow.addComponent(cell);
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}
		
}

package com.eform.ui.custom.paging;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.Model;

import com.eform.common.utils.CommonUtils;
import com.eform.ui.custom.paging.base.PaginationBar;
import com.vaadin.data.util.BeanItemContainer;

public class InstancePaginationBar extends PaginationBar<HistoricProcessInstance> {

	private HistoryService historyService;
	
	public InstancePaginationBar(BeanItemContainer<HistoricProcessInstance> container, HistoryService historyService) {
		super(container);
		this.historyService = historyService;
		reloadData();
	}

	@Override
	public long getRowCount() {
		if(historyService != null){
			return historyService
		      .createHistoricProcessInstanceQuery()
		      .startedBy(CommonUtils.getUserLogedIn()).count();
		}
		return 0L;
	}

	@Override
	public List<HistoricProcessInstance> getDataList(int firstResult, int maxResults) {
		if(historyService != null){
			return historyService
		      .createHistoricProcessInstanceQuery()
		      .startedBy(CommonUtils.getUserLogedIn()).orderByProcessInstanceStartTime().desc().listPage(firstResult, maxResults);
		}
		return new ArrayList<HistoricProcessInstance>();
	}
	
	public void search(){
		currentPage = 1;
		reloadData();
	}

}

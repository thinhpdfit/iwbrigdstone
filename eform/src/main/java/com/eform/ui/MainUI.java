package com.eform.ui;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.task.IdentityLink;
import org.springframework.beans.factory.annotation.Autowired;

import com.eform.common.Broadcaster;
import com.eform.common.Broadcaster.BroadcastListener;
import com.eform.common.Constants;
import com.eform.common.ExplorerLayout;
import com.eform.common.type.BroadcastType;
import com.eform.common.utils.CommonUtils;
import com.eform.dao.GroupQuyenDao;
import com.eform.model.entities.HtQuyen;
import com.eform.model.entities.Notification;
import com.eform.service.NotificationService;
import com.eform.ui.template.Header;
import com.eform.ui.template.MainMenuBar;
import com.eform.ui.view.BaseView;
import com.eform.ui.view.eoffice.DocumentManagementView;
import com.eform.ui.view.process.ProcessDefinitionDetailView;
import com.eform.ui.view.task.TaskDetailView;
import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Tree;
import com.vaadin.ui.UI;

@Push(value = PushMode.AUTOMATIC, transport = Transport.LONG_POLLING)
@Theme(ExplorerLayout.THEME)
@SpringUI(path="/")
@JavaScript({"vaadin://js/jquery-3.1.0.min.js", "vaadin://js/iworkflow.js", "https://apis.google.com/js/platform.js?onload=onLoad", "vaadin://js/googleauth.js"})
public class MainUI extends UI implements BroadcastListener{

	private static final long serialVersionUID = 1L;
	@Autowired
    private SpringViewProvider viewProvider;

	@Autowired
	private GroupQuyenDao groupQuyenDao;
	@Autowired
	protected IdentityService identityService;
	@Autowired
	protected HistoryService historyService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private NotificationService notificationService;

	private CssLayout notificationBlock;
	private Header header;
	private Map<String, View> viewNotifMap;
	
	private String currentUserId;
    
    @Override
    protected void init(VaadinRequest request) {
    	Broadcaster.register(this);
    	viewNotifMap = new HashMap();
    	currentUserId = CommonUtils.getUserLogedIn();
    	Locale locale = new Locale("vi", "vn");
    	setLocale(locale);
    	getSession().setLocale(locale);
		setTheme(ExplorerLayout.THEME);
		MainLayout mainLayout = new MainLayout(groupQuyenDao, getGroups());
		setContent(mainLayout);
		Navigator navigator = new Navigator(this, mainLayout.getViewContainer());
        navigator.addProvider(viewProvider);
        header = mainLayout.getHeader();
        Tree menuTree = mainLayout.getMenuTree();
        notificationBlock = mainLayout.getNotificationBlock();
        MainMenuBar mainMenuBar = mainLayout.getMainMenuBar();
        
        String userAgent = request.getHeader("User-Agent").toLowerCase();
        System.out.println(userAgent);
        navigator.addViewChangeListener(new ViewChangeListener() {
			
			@Override
			public boolean beforeViewChange(ViewChangeEvent event) {
				System.out.println("beforeViewChange");
				String viewName = event.getViewName();
				
				View newView = event.getNewView();
				View oldView = event.getOldView();
				if(newView instanceof BaseView){
					BaseView newViewTmp = (BaseView) newView;
					if(oldView instanceof BaseView){
						BaseView oldViewTmp = (BaseView) oldView;
						newViewTmp.setPrevViewName(oldViewTmp.getViewName());
					}
				}
				
				if(menuTree != null && menuTree.getData() instanceof Map){
					Map<String, HtQuyen> viewNameQuyenMap = (Map<String, HtQuyen>) menuTree.getData();
		        	if(viewNameQuyenMap.get(viewName) != null){
						return true;
					}
		        }
				
//				Notification notf = new Notification("Thông báo", "Bạn không có quyền thực hiện thao tác này!", Type.WARNING_MESSAGE);
//				notf.setDelayMsec(3000);
//				notf.setPosition(Position.TOP_CENTER);
//				notf.show(UI.getCurrent().getPage());
				return true;
			}
			
			@Override
			public void afterViewChange(ViewChangeEvent event) {
				System.out.println("afterViewChange");
				String viewName = event.getViewName();
				
				if(mainMenuBar != null){
					mainMenuBar.setCurrentView(viewName);
				}
				View newView = event.getNewView();
				String param = event.getParameters();
				String url = viewName;
				if(param != null && !param.isEmpty()){
					url += "/"+param;
				}
				if(menuTree != null && menuTree.getData() instanceof Map){
					Map<String, HtQuyen> viewNameQuyenMap = (Map<String, HtQuyen>) menuTree.getData();
		        	if(viewNameQuyenMap.get(url) != null){
						HtQuyen htQuyen = viewNameQuyenMap.get(url);
						menuTree.setValue(htQuyen);
					}
		        }
				if(newView != null && newView instanceof TaskDetailView){
					viewNotifMap.put(BroadcastType.TASK_COMMENT, newView);
				}
			}
		});
        
    }
    
    public List<String> getGroups(){
    	List<String> groups = new ArrayList();
    	String user = CommonUtils.getUserLogedIn();
    	List<Group> groupLst = identityService.createGroupQuery().groupMember(user).list();
    	if(groupLst != null && !groupLst.isEmpty()){
    		for(Group g : groupLst){
    			groups.add(g.getId());
    		}
    	}
    	return groups;
    }

    @Override
	public void detach() {
		Broadcaster.unregister(this);
		super.detach();
	}

	@Override
	public void receiveBroadcast(String type, String message, String userId, String refKey) {
		
		access(new Runnable() {
			@Override
			public void run() {
				if(refKey != null){
					org.activiti.engine.task.Comment commentObj = taskService.getComment(refKey);
					if(commentObj != null){
						List<IdentityLink> identityLinks = taskService.getIdentityLinksForTask(commentObj.getTaskId());
						if(identityLinks != null){
							boolean candidate = false;
							for(IdentityLink link : identityLinks){
								if(link != null && link.getUserId() != null && link.getUserId().equals(currentUserId)){
									candidate = true;
								}
							}
							if(!candidate){
								return;//nguoi dung khong lien quan
							}
						}
						if(userId != null && userId.equals(currentUserId)){
							return;//nguoi dung la nguoi thuc hien thao tac
						}
						if(BroadcastType.TASK_COMMENT.equals(type)){
							View view = viewNotifMap.get(type);
							if(view != null && view instanceof TaskDetailView){
								TaskDetailView taskDetailView = (TaskDetailView) view;
								taskDetailView.refreshCommentWrap(currentUserId);
							}
							
							
							Notification notif = new Notification();
							notif.setFromUserId(userId);
							notif.setUserId(currentUserId);
							notif.setType(new Long(CommonUtils.ENotificationType.COMMENT_TASK.getCode()));
							notif.setRefKey(refKey);
							notif.setTrangThai(0L);
							notif.setNotificationTime(new Timestamp(new Date().getTime()));
							notificationService.save(notif);

							if(header != null){
								header.addNotification(notif);
							}
							
							showCommentNotification(message, userId, refKey, notif);
							
						}
						
					}
				}
				
			}
		});
	}
	
	public void showCommentNotification(String comment, String userId, String refKey, Notification notifObj){
		try {
			if(notificationBlock != null && refKey != null){
				CssLayout notif = new CssLayout();
				notif.addStyleName(ExplorerLayout.NOTIFICATION_ITEM);
				SimpleDateFormat df = new SimpleDateFormat("ddMMyyyyHHmmssSSS");
				String notifId = "notif-"+df.format(new Date());
				notif.setId(notifId);
				
				String message = "";
				
				User user = identityService.createUserQuery().userId(userId).singleResult();
				if(user != null){
					String userName = user.getFirstName()+" "+user.getLastName()+"";
					message += "<span class='notif-u-name'>"+userName+"</span>";
				}
				
				message+=" bình luận về công việc ";
				
				String taskName= "#Không tên";
				if(refKey != null){
					org.activiti.engine.task.Comment commentObj = taskService.getComment(refKey);
					if(commentObj != null){
						HistoricTaskInstance task = historyService.createHistoricTaskInstanceQuery().taskId(commentObj.getTaskId()).singleResult();
						if(task!=null&&task.getName() != null && !task.getName().isEmpty()){
							taskName=task.getName();
						}
					}
				}
				message+="<span class='notif-task-name'>"+taskName+"</span>";
				
				if(comment != null && comment.length() > 70){
					comment = comment.substring(0, 70) + "...";
				}
				
				message+=" <span class='notif-content'>"+comment+"</span>";
				message+="<i>vài giây trước</i>";
				
				Label lbl = new Label(message);
				lbl.setContentMode(ContentMode.HTML);
				lbl.addStyleName(ExplorerLayout.NOTIFICATION_ITEM_LBL);
				notif.addComponent(lbl);
				
				Button btnTaskDetail = new Button();
				btnTaskDetail.addStyleName(ExplorerLayout.NOTIFICATION_ITEM_BTN);
				
				btnTaskDetail.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						notifObj.setTrangThai(new Long(CommonUtils.ENotificationStatus.CLICKED.getCode()));
						notificationService.save(notifObj);
						com.vaadin.ui.JavaScript.getCurrent().execute("maskAsClicked('notif-i-"+notifObj.getId()+"')");
						org.activiti.engine.task.Comment commentObj = taskService.getComment(refKey);
						UI.getCurrent().getNavigator().navigateTo(TaskDetailView.VIEW_NAME+"/"+Constants.ACTION_VIEW+"/"+commentObj.getTaskId()+"/cmt-"+commentObj.getId());
					}
				});
				
				notif.addComponent(btnTaskDetail);
				
				notificationBlock.addComponent(notif);
				com.vaadin.ui.JavaScript.getCurrent().execute("setTimeoutNotification('"+notifId+"')");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
}

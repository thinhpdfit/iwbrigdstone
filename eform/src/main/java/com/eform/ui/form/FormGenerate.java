package com.eform.ui.form;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.hadoop.mapred.gethistory_jsp;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.eform.common.Constants;
import com.eform.common.EChucNang;
import com.eform.common.ExplorerLayout;
import com.eform.common.SpringApplicationContext;
import com.eform.common.ThamSo;
import com.eform.common.type.EFormHoSo;
import com.eform.common.type.EFormTreeItem;
import com.eform.common.type.EFormTruongThongTin;
import com.eform.common.type.RelatedField;
import com.eform.common.type.UserRelatedField;
import com.eform.common.utils.CommonUtils;
import com.eform.common.utils.CommonUtils.ELoaiO;
import com.eform.common.utils.FileUtils;
import com.eform.common.utils.FormUtils;
import com.eform.common.utils.SolrUtils;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.HtChucVuDao;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.BmHang;
import com.eform.model.entities.BmO;
import com.eform.model.entities.BmOTruongThongTin;
import com.eform.model.entities.DanhMuc;
import com.eform.model.entities.FileManagement;
import com.eform.model.entities.FileVersion;
import com.eform.model.entities.Formula;
import com.eform.model.entities.HtChucVu;
import com.eform.model.entities.HtDonVi;
import com.eform.model.entities.HtThamSo;
import com.eform.model.entities.LoaiDanhMuc;
import com.eform.model.entities.Signature;
import com.eform.model.entities.TruongThongTin;
import com.eform.service.BieuMauService;
import com.eform.service.BmHangService;
import com.eform.service.BmOService;
import com.eform.service.BmOTruongThongTinService;
import com.eform.service.DanhMucService;
import com.eform.service.FileManagementService;
import com.eform.service.FormulaService;
import com.eform.service.HoSoService;
import com.eform.service.HtDonViServices;
import com.eform.service.HtThamSoService;
import com.eform.service.SignatureService;
import com.eform.service.TruongThongTinService;
import com.eform.ui.custom.UploadFileWindow;
import com.eform.ui.custom.UploadInfoWindow;
import com.eform.ui.custom.UploadReceive;
import com.eform.ui.form.formular.JSONFormularParser;
import com.eform.ui.view.report.type.v2.VisibleColumn;
import com.eform.ui.view.report.utils.ReportUtils;
import com.google.api.client.json.Json;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.DateRangeValidator;
import com.vaadin.data.validator.DoubleRangeValidator;
import com.vaadin.data.validator.LongRangeValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.StartedListener;
import com.vaadin.ui.themes.Reindeer;

public class FormGenerate extends GridLayout implements ClickListener {

	private BieuMauService bieuMauService;
	private BmHangService bmHangService;
	private BmOService bmOService;
	private BmOTruongThongTinService bmOTruongThongTinService;
	private IdentityService identityService;
	private HtThamSoService htThamSoService;
	private FileManagementService fileManagementService;
	private SignatureService signatureService;
	private DanhMucService danhMucService;
	private HtDonViServices htDonViServices;
	private HtChucVuDao htChucVuDao;

	private PropertysetItem itemValue;
	private FieldGroup fieldGroup;
	private GridLayout bieuMauGrid;
	private Map<String, Object> valueMap;
	private Map<String, Object> fieldsMap;
	private List<String> truongTTFileList;

	private EFormTreeItem<BieuMau> bieuMauTree;
	private BieuMau bieuMauCurrent;
	private String hoSoXml;
	private boolean readOnly;
	private boolean htmlDisplay;
	private boolean genForm;

	private String uploadPath;
	private String emailPattern;
	private Map<String, BieuMau> nhomBieuMauMap;

	private final String BTN_CALC_ID = "btnCalc";
	private final String BTN_ADD_ROW_ID = "btnAddRow";
	private final String BTN_REMOVE_ROW_ID = "btnRemoveRow";
	private final String BTN_REMOVE_ID = "btnRemove";
	private final String BTN_DOWNLOAD_ID = "btnDownload";

	private CssLayout btnCalcWrap;

	private List<Formula> formulaList;
	@Autowired
	private FormulaService formulaService;
	private String assigneeUserId;
	private List<String> removedKey;
	private Map<String, List<RelatedField>> relatedFieldMap;
	private Map<String, List<UserRelatedField>> userRelatedFieldMap;

	private RepositoryService repositoryService;
	private HistoryService historyService;
	private HoSoService hosoService;
	private TruongThongTinService truongThongTinService;

	public FormGenerate(String maBieuMau) {
		init(maBieuMau, null, false, true, false, null);
	}

	public FormGenerate(String maBieuMau, String hoSoXml) {
		init(maBieuMau, hoSoXml, false, true, false, null);
	}

	public FormGenerate(String maBieuMau, String hoSoXml, boolean readOnly) {
		init(maBieuMau, hoSoXml, readOnly, true, false, null);
	}

	public FormGenerate(String maBieuMau, String hoSoXml, boolean readOnly, String assigneeUserId) {
		init(maBieuMau, hoSoXml, readOnly, true, false, assigneeUserId);
	}

	public FormGenerate(String maBieuMau, boolean htmlDisplay, String hoSoXml) {
		init(maBieuMau, hoSoXml, false, true, htmlDisplay, null);
	}

	public FormGenerate(boolean genForm, String maBieuMau, String hoSoXml) {
		init(maBieuMau, hoSoXml, true, genForm, false, null);
	}

	public void init(String maBieuMau, String hoSoXml, boolean readOnly, boolean genForm, Boolean htmlDisplay,
			String assigneeUserId) {
		this.htmlDisplay = htmlDisplay;
		bieuMauService = SpringApplicationContext.getApplicationContext().getBean(BieuMauService.class);
		bmHangService = SpringApplicationContext.getApplicationContext().getBean(BmHangService.class);
		bmOService = SpringApplicationContext.getApplicationContext().getBean(BmOService.class);
		bmOTruongThongTinService = SpringApplicationContext.getApplicationContext()
				.getBean(BmOTruongThongTinService.class);
		identityService = SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
		htThamSoService = SpringApplicationContext.getApplicationContext().getBean(HtThamSoService.class);
		fileManagementService = SpringApplicationContext.getApplicationContext().getBean(FileManagementService.class);
		signatureService = SpringApplicationContext.getApplicationContext().getBean(SignatureService.class);
		formulaService = SpringApplicationContext.getApplicationContext().getBean(FormulaService.class);
		danhMucService = SpringApplicationContext.getApplicationContext().getBean(DanhMucService.class);
		htDonViServices = SpringApplicationContext.getApplicationContext().getBean(HtDonViServices.class);
		htChucVuDao = SpringApplicationContext.getApplicationContext().getBean(HtChucVuDao.class);
		repositoryService = SpringApplicationContext.getApplicationContext().getBean(RepositoryService.class);
		historyService = SpringApplicationContext.getApplicationContext().getBean(HistoryService.class);
		hosoService = SpringApplicationContext.getApplicationContext().getBean(HoSoService.class);
		truongThongTinService = SpringApplicationContext.getApplicationContext().getBean(TruongThongTinService.class);
		this.hoSoXml = hoSoXml;
		bieuMauGrid = new GridLayout();
		this.readOnly = readOnly;
		this.genForm = genForm;
		this.assigneeUserId = assigneeUserId;
		removedKey = new ArrayList();

		relatedFieldMap = new HashMap();
		userRelatedFieldMap = new HashMap();
		nhomBieuMauMap = new HashMap();
		valueMap = new HashMap<String, Object>();
		fieldsMap = new HashMap<String, Object>();
		truongTTFileList = new ArrayList();
		uploadPath = "";
		HtThamSo folderUpload = htThamSoService.getHtThamSoFind(ThamSo.FOLDER_UPLOAD_PATH);

		if (folderUpload != null) {
			uploadPath = folderUpload.getGiaTri();
		}
		HtThamSo emailPatternTs = htThamSoService.getHtThamSoFind(ThamSo.EMAIL_PATTERN);

		if (emailPatternTs != null) {
			emailPattern = emailPatternTs.getGiaTri();
		}

		addComponent(bieuMauGrid);

		btnCalcWrap = new CssLayout();
		btnCalcWrap.addStyleName(ExplorerLayout.CALC_BTN_WRAP);
		Button btnCalc = new Button();
		btnCalc.addStyleName(ExplorerLayout.CALC_BTN);
		btnCalc.addClickListener(this);
		btnCalc.setId(BTN_CALC_ID);
		btnCalc.setDescription("Xử lý tính toán");
		btnCalc.setIcon(FontAwesome.CALCULATOR);
		btnCalcWrap.addComponent(btnCalc);
		addComponent(btnCalcWrap);

		if (maBieuMau != null) {
			List<BieuMau> bieuMauList = bieuMauService.getBieuMauFind(maBieuMau, null, null, null, 0, 1);
			if (bieuMauList != null && !bieuMauList.isEmpty()) {
				bieuMauCurrent = bieuMauList.get(0);
				formulaList = formulaService.getFormulaFind(bieuMauCurrent);
				bieuMauTree = initTree(bieuMauCurrent);
				if (hoSoXml != null) {
					parseHoSoGiaTri();
				}
				if (genForm) {

					initService(bieuMauCurrent);
					initFormServiceConfig(bieuMauCurrent);
					refresh();
					ProcessService(null);
				}

			}
		}
	}

	public EFormTreeItem<BieuMau> initTree(BieuMau bm) {
		EFormTreeItem<BieuMau> treeItem = new EFormTreeItem<BieuMau>();
		treeItem.setE(bm);
		List<BmHang> hangList = bmHangService.getBmHangFind(null, null, null, bm, -1, -1);

		if (hangList != null) {
			List<EFormTreeItem> childList = treeItem.getChildList();
			if (childList == null) {
				childList = new ArrayList<EFormTreeItem>();
				treeItem.setChildList(childList);
			}
			for (BmHang bmHang : hangList) {
				EFormTreeItem<BmHang> child = initTree(bmHang);
				if (child != null) {
					if (bmHang.getLoaiHang() != null
							&& bmHang.getLoaiHang().intValue() == CommonUtils.ELoaiHang.LAP.getCode()) {
						EFormTreeItem<BmHang> childMaster = new EFormTreeItem<BmHang>();
						childMaster.setChildList(new ArrayList<EFormTreeItem>());
						childList.add(childMaster);
						child.setParent(childMaster);
						childMaster.getChildList().add(child);
						childMaster.setParent(treeItem);
					} else {
						child.setParent(treeItem);
						childList.add(child);
					}
				}
			}
		}

		return treeItem;
	}

	public EFormTreeItem<BmHang> initTree(BmHang bmHang) {
		EFormTreeItem<BmHang> treeItem = new EFormTreeItem<BmHang>();
		treeItem.setE(bmHang);

		List<BmO> bmOList = bmOService.getBmOFind(null, null, null, null, bmHang, null, -1, -1);
		if (bmOList != null) {
			List<EFormTreeItem> childList = treeItem.getChildList();
			if (childList == null) {
				childList = new ArrayList<EFormTreeItem>();
				treeItem.setChildList(childList);
			}

			for (BmO bmO : bmOList) {
				EFormTreeItem<BmO> child = initTree(bmO);
				if (child != null) {
					child.setParent(treeItem);
					childList.add(child);
				}
			}
		}

		return treeItem;
	}

	public EFormTreeItem<BmO> initTree(BmO o) {
		EFormTreeItem<BmO> treeItem = new EFormTreeItem<BmO>();
		treeItem.setE(o);
		if (o.getBieuMauCon() != null) {
			List<EFormTreeItem> childList = treeItem.getChildList();
			if (childList == null) {
				childList = new ArrayList();
				treeItem.setChildList(childList);
			}
			EFormTreeItem<BieuMau> child = initTree(o.getBieuMauCon());
			if (child != null) {
				child.setParent(treeItem);
				childList.add(child);
			}
		} else {
			List<BmOTruongThongTin> oTruongThongTinList = bmOTruongThongTinService.getBmOTruongThongTinFind(null, null,
					o, null, -1, -1);
			if (oTruongThongTinList != null) {
				List<EFormTreeItem> childList = treeItem.getChildList();
				if (childList == null) {
					childList = new ArrayList();
					treeItem.setChildList(childList);
				}
				for (BmOTruongThongTin oTruongThongTin : oTruongThongTinList) {
					EFormTreeItem<BmOTruongThongTin> child = initTree(oTruongThongTin);
					if (child != null) {
						child.setParent(treeItem);
						childList.add(child);
					}
				}
			}
		}
		return treeItem;
	}

	public EFormTreeItem<BmOTruongThongTin> initTree(BmOTruongThongTin bmOTruongThongTin) {
		EFormTreeItem<BmOTruongThongTin> treeItem = new EFormTreeItem<BmOTruongThongTin>();
		treeItem.setE(bmOTruongThongTin);
		return treeItem;
	}

	public void refresh() {
		relatedFieldMap.clear();
		userRelatedFieldMap.clear();
		itemValue = new PropertysetItem();
		fieldGroup = new FieldGroup();
		fieldGroup.setItemDataSource(itemValue);

		initBieuMauGrid(bieuMauGrid, bieuMauCurrent);
		createBieuMauUI(bieuMauTree, bieuMauGrid, null, null);
		String with = "100%";
		if (bieuMauCurrent.getWidth() != null) {
			with = bieuMauCurrent.getWidth();
		}
		bieuMauGrid.setWidth(with);
		setRelatedField();
		setUserRelatedField();
		if (formulaList != null && !formulaList.isEmpty()) {
			btnCalcWrap.setVisible(true);
		} else {
			btnCalcWrap.setVisible(false);
		}
	}

	private JSONObject serviceConfig;
	private JSONObject FormServiceConfig;
	private JSONObject serviceResponse;

	public void initService(BieuMau bm) {
		serviceConfig = bm.getServiceConfig();
	}

	public void ProcessService(Map<String, String> params) {
		if (serviceConfig != null) {
			String url = serviceConfig.getString("url");
			if (url != null && !url.trim().equalsIgnoreCase("")) {

				HttpHeaders headers = new HttpHeaders();
				RestTemplate restTemplate = new RestTemplate();
				headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

				UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
				boolean flagParam = false;
				if (serviceConfig.has("onchange")) {
					Object onchange = serviceConfig.get("onchange");
					JSONObject onchangeObj = null;
					if (onchange != null && onchange instanceof JSONObject) {
						onchangeObj = (JSONObject) onchange;
						for (Iterator iterator = onchangeObj.keySet().iterator(); iterator.hasNext();) {
							String key = (String) iterator.next();
							if (params != null) {
//							for (Map.Entry<String, String> entry : params.entrySet()) {
//								if(key.trim().equalsIgnoreCase(entry.getKey())) {
//									builder.queryParam(entry.getKey(), entry.getValue());
//								}
//							}
								if (params.containsKey(key)) {
									builder.queryParam(onchangeObj.get(key).toString(), params.get(key));
									flagParam = true;
								} else {
									builder.queryParam(onchangeObj.get(key).toString(), null);
								}
							} else {
								if (fieldsMap != null && fieldsMap.containsKey(key.toLowerCase())) {
									String id = fieldsMap.get(key.toLowerCase()).toString();
									if (valueMap.get(id) != null) {
										builder.queryParam(onchangeObj.get(key).toString(), valueMap.get(id));
									} else {
										builder.queryParam(onchangeObj.get(key).toString(), null);
									}
								} else {
									builder.queryParam(onchangeObj.get(key).toString(), null);
								}
							}
						}

					}

					if (params == null || flagParam == true) {
						HttpEntity<?> entity = new HttpEntity<>(headers);
						try {
							HttpEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET,
									entity, String.class);
							if (response.getBody() != null) {
								serviceResponse = new JSONObject(response.getBody());
								if (serviceConfig != null && serviceConfig.has("parammap")) {
									JSONObject paramMap = serviceConfig.getJSONObject("parammap");
									for (Iterator iterator = paramMap.keySet().iterator(); iterator.hasNext();) {
										String key = (String) iterator.next();
										// String id = "BM" + bieuMauCurrent.getId() + "O" + key;
										if ((onchangeObj == null || !onchangeObj.has(key)) && fieldsMap != null
												&& fieldsMap.containsKey(key.toLowerCase())) {
											String id = fieldsMap.get(key.toLowerCase()).toString();
											String fiedName = paramMap.getString(key);
											if (fiedName != null && !fiedName.equalsIgnoreCase("")) {
												Object fieldVal = serviceResponse.get(fiedName);
												if (fieldVal != null && !(fieldVal instanceof JSONArray)
														&& !(fieldVal instanceof JSONObject)) {

													if (fieldGroup != null) {
														Field<?> field = fieldGroup.getField(id);
														if (field instanceof TextField) {
															if (((TextField) field).isReadOnly()) {
																((TextField) field).setReadOnly(false);
																((TextField) field).setValue(fieldVal.toString());
																((TextField) field).setReadOnly(true);
															} else {
																((TextField) field).setValue(fieldVal.toString());
															}
														} else if (field instanceof PopupDateField) {
															if (((PopupDateField) field).isReadOnly()) {
																((PopupDateField) field).setReadOnly(false);
																((PopupDateField) field).setValue(new Timestamp(
																		Long.parseLong(fieldVal.toString())));
																((PopupDateField) field).setReadOnly(true);
															} else {
																((PopupDateField) field).setValue(new Timestamp(
																		Long.parseLong(fieldVal.toString())));
															}
														} else if (field instanceof TextArea) {
															if (((TextArea) field).isReadOnly()) {
																((TextArea) field).setReadOnly(false);
																((TextArea) field).setValue(fieldVal.toString());
																((TextArea) field).setReadOnly(true);
															} else {
																((TextArea) field).setValue(fieldVal.toString());
															}
														} else if (field instanceof ComboBox) {
															if (((ComboBox) field).isReadOnly()) {
																((ComboBox) field).setReadOnly(false);
																((ComboBox) field).setValue(fieldVal.toString());
																((ComboBox) field).setReadOnly(true);
															} else {
																((ComboBox) field).setValue(fieldVal.toString());
															}
														}
													}
												}
											}
										}
									}
								}
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				}
			}
		}
	}

	public void initFormServiceConfig(BieuMau bm) {
		FormServiceConfig = bm.getFormServicceConfig();
	}

	public List<Map<String, Object>> getFieldFormServiceConfig(String maTruongThongTin) {

		if (FormServiceConfig != null && FormServiceConfig.has("processdefid") && FormServiceConfig.has("bieumau")) {
			String processDefId = FormServiceConfig.getString("processdefid");
			String maBieuMau = FormServiceConfig.getString("bieumau");
			if (processDefId != null && !processDefId.trim().equalsIgnoreCase("")) {

				List<String> processInstanceIdList = new ArrayList<String>();
				if (processDefId != null) {
					ProcessDefinition processDef = repositoryService.createProcessDefinitionQuery()
							.processDefinitionId(processDefId).singleResult();
					List<HistoricProcessInstance> historicProInsList = historyService
							.createHistoricProcessInstanceQuery().processDefinitionId(processDef.getId()).list();
					if (historicProInsList != null && !historicProInsList.isEmpty()) {
						for (HistoricProcessInstance h : historicProInsList) {
							processInstanceIdList.add(h.getId());
						}
					}
				}
				List<VisibleColumn> visibleColumnList = new ArrayList<VisibleColumn>();

				VisibleColumn obj = new VisibleColumn();
				if (maTruongThongTin != null) {
					List<TruongThongTin> list = truongThongTinService.findByCode(maTruongThongTin);
					if (list != null && !list.isEmpty()) {
						obj.setTruongThongTin(list.get(0));
					}
				}
				visibleColumnList.add(obj);

				Date fromDate = null;
				Date toDate = null;
				// String tieuDe = baoCaoThongKeCurrent.getTieuDe();
//				BieuMau bieuMau = bieuMauService.findOne(Long.valueOf(bieuMauId));

				List<BieuMau> list = bieuMauService.findByCode(maBieuMau);
				if (list == null || list.isEmpty()) {
					return null;
				} else {
					BieuMau bieuMau = list.get(0);
					List<String> maBieuMauList = new ArrayList();
					maBieuMauList.add(bieuMau.getMa());
					Map<String, Object> bieuMauTreeMap = ReportUtils.getBieuMauTreeMap(bieuMau, maBieuMauList);
					Map<String, Object> paramMap = new HashMap();
					String sql = ReportUtils.createSqlString(bieuMauTreeMap, processInstanceIdList, null,
							visibleColumnList, fromDate, toDate, paramMap);
					return hosoService.getReportResult(sql, paramMap);
				}
			}
		}
		return null;

	}

	public void ProcessFormService(String fieldId) {
		if (FormServiceConfig != null && FormServiceConfig.has("processdefid") && FormServiceConfig.has("bieumau")
				&& FormServiceConfig.has("fieldmap")) {
			String processDefId = FormServiceConfig.getString("processdefid");
			String maBieuMau = FormServiceConfig.getString("bieumau");
			if (processDefId != null && !processDefId.trim().equalsIgnoreCase("")) {

				List<String> processInstanceIdList = new ArrayList<String>();
				if (processDefId != null) {
					ProcessDefinition processDef = repositoryService.createProcessDefinitionQuery()
							.processDefinitionId(processDefId).singleResult();
					List<HistoricProcessInstance> historicProInsList = historyService
							.createHistoricProcessInstanceQuery().processDefinitionId(processDef.getId()).list();
					if (historicProInsList != null && !historicProInsList.isEmpty()) {
						for (HistoricProcessInstance h : historicProInsList) {
							processInstanceIdList.add(h.getId());
						}
					}
				}
				List<VisibleColumn> visibleColumnList = new ArrayList<VisibleColumn>();

				JSONObject groupArr = FormServiceConfig.getJSONObject("fieldmap");
				if (groupArr != null) {
					for (Iterator iterator = groupArr.keySet().iterator(); iterator.hasNext();) {
						String maTruongThongTin = (String) iterator.next();
						VisibleColumn obj = new VisibleColumn();
						if (maTruongThongTin != null) {
							List<TruongThongTin> list = truongThongTinService.findByCode(maTruongThongTin);
							if (list != null && !list.isEmpty()) {
								obj.setTruongThongTin(list.get(0));
							}
						}
						visibleColumnList.add(obj);
					}
				}

				Date fromDate = null;
				Date toDate = null;
				// String tieuDe = baoCaoThongKeCurrent.getTieuDe();
				BieuMau bieuMau = null;
				List<BieuMau> list = bieuMauService.findByCode(maBieuMau);
				if (list == null || list.isEmpty()) {
					return;
				} else {
					bieuMau = list.get(0);

					List<String> maBieuMauList = new ArrayList();
					maBieuMauList.add(bieuMau.getMa());
					Map<String, Object> bieuMauTreeMap = ReportUtils.getBieuMauTreeMap(bieuMau, maBieuMauList);
					Map<String, Object> paramMap = new HashMap();
					String sql = ReportUtils.createSqlStringById(fieldId, bieuMauTreeMap, processInstanceIdList, null,
							visibleColumnList, fromDate, toDate, paramMap);
					List<Map<String, Object>> resultList = hosoService.getReportResult(sql, paramMap);
					for (Map<String, Object> obj : resultList) {
						for (String key : obj.keySet()) {
							if (groupArr.has(key) && fieldsMap != null
									&& fieldsMap.containsKey(groupArr.getString(key))) {

								String fiedName = groupArr.getString(key);
								String id = fieldsMap.get(fiedName).toString();
								if (fiedName != null && !fiedName.equalsIgnoreCase("")) {
									Object fieldVal = obj.get(key);
									if (fieldVal != null && !(fieldVal instanceof JSONArray)
											&& !(fieldVal instanceof JSONObject)) {
										if (fieldGroup != null) {
											Field<?> field = fieldGroup.getField(id);
											if (field instanceof TextField) {
												if (((TextField) field).isReadOnly()) {
													((TextField) field).setReadOnly(false);
													((TextField) field).setValue(fieldVal.toString());
													((TextField) field).setReadOnly(true);
												} else {
													((TextField) field).setValue(fieldVal.toString());
												}
											} else if (field instanceof PopupDateField) {
												if (((PopupDateField) field).isReadOnly()) {
													((PopupDateField) field).setReadOnly(false);
													((PopupDateField) field).setValue(
															new Timestamp(Long.parseLong(fieldVal.toString())));
													((PopupDateField) field).setReadOnly(true);
												} else {
													((PopupDateField) field).setValue(
															new Timestamp(Long.parseLong(fieldVal.toString())));
												}
											} else if (field instanceof TextArea) {
												if (((TextArea) field).isReadOnly()) {
													((TextArea) field).setReadOnly(false);
													((TextArea) field).setValue(fieldVal.toString());
													((TextArea) field).setReadOnly(true);
												} else {
													((TextArea) field).setValue(fieldVal.toString());
												}
											} else if (field instanceof ComboBox) {
												if (((ComboBox) field).isReadOnly()) {
													((ComboBox) field).setReadOnly(false);
													((ComboBox) field).setValue(fieldVal.toString());
													((ComboBox) field).setReadOnly(true);
												} else {
													((ComboBox) field).setValue(fieldVal.toString());
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}

	public void setRelatedField() {
		if (relatedFieldMap != null && !relatedFieldMap.isEmpty()) {

			for (Entry<String, List<RelatedField>> entry : relatedFieldMap.entrySet()) {
				List<RelatedField> relatedList = entry.getValue();
				if (relatedList != null && !relatedList.isEmpty()) {

					for (RelatedField rl : relatedList) {
						if (rl != null && rl.getComboBox() != null && rl.getLoaiDanhMuc() != null) {

							List<RelatedField> rlChilds = new ArrayList();
							for (RelatedField rl1 : relatedList) {
								if (rl1 != null && rl1.getLoaiDanhMuc() != null
										&& rl1.getLoaiDanhMuc().getLoaiDanhMuc() != null
										&& rl1.getLoaiDanhMuc().getLoaiDanhMuc().equals(rl.getLoaiDanhMuc())) {
									rlChilds.add(rl1);
								}
							}

							ComboBox cbb = rl.getComboBox();
							Object objValue = cbb.getValue();
							if (objValue != null && objValue instanceof String) {
								setRelateField(rlChilds, rl, (String) objValue, false);
							}

							cbb.addValueChangeListener(new ValueChangeListener() {
								@Override
								public void valueChange(ValueChangeEvent event) {
									Object value = event.getProperty().getValue();
									if (value != null && value instanceof String) {
										String dmCurrentStr = (String) value;
										setRelateField(rlChilds, rl, dmCurrentStr, true);
									}
								}
							});
						}
					}
				}
			}
		}
	}

	public void setRelateField(List<RelatedField> rlChilds, RelatedField rl, String dmCurrentStr, boolean setNull) {

		WorkflowManagement<DanhMuc> dmWM = new WorkflowManagement<>(EChucNang.E_FORM_DANH_MUC.getMa());
		List<Long> trangThai = null;
		if (dmWM.getTrangThaiSuDung() != null) {
			trangThai = new ArrayList();
			trangThai.add(new Long(dmWM.getTrangThaiSuDung().getId()));
		}

		// xoa du lieu combobox con
		if (rlChilds != null && !rlChilds.isEmpty()) {
			for (RelatedField rlChild : rlChilds) {
				if (rlChild != null && rlChild.getComboBox() != null) {
					rlChild.getComboBox().getContainerDataSource().removeAllItems();
					if (setNull) {
						rlChild.getComboBox().setValue(null);
					}
				}
			}
		}
		if (dmCurrentStr != null) {
			DanhMuc dmCurrent = null;
			List<DanhMuc> dmCurrentList = danhMucService.getDanhMucFind(dmCurrentStr, null, trangThai,
					rl.getLoaiDanhMuc(), null, -1, -1);
			if (dmCurrentList != null && !dmCurrentList.isEmpty()) {
				dmCurrent = dmCurrentList.get(0);
			}
			// lay danh sach danh muc theo danh muc cha
			if (dmCurrent != null && rlChilds != null && !rlChilds.isEmpty()) {
				for (RelatedField rlChild : rlChilds) {
					if (rlChild != null && rlChild.getComboBox() != null && rlChild.getLoaiDanhMuc() != null) {
						List<DanhMuc> dmChildList = danhMucService.getDanhMucFind(null, null, trangThai,
								rlChild.getLoaiDanhMuc(), dmCurrent, -1, -1);
						if (dmChildList != null && !dmChildList.isEmpty()) {
							for (DanhMuc dm : dmChildList) {
								rlChild.getComboBox().addItem(dm.getMa());
								rlChild.getComboBox().setItemCaption(dm.getMa(), dm.getTen());
							}

						}
					}
				}
			}
		}

	}

	public void setUserRelatedField() {
		if (userRelatedFieldMap != null && !userRelatedFieldMap.isEmpty()) {
			for (Entry<String, List<UserRelatedField>> entry : userRelatedFieldMap.entrySet()) {
				List<UserRelatedField> relatedList = entry.getValue();
				if (relatedList != null && !relatedList.isEmpty()) {
					for (UserRelatedField rl : relatedList) {
						if (rl != null && rl.getTruongThongTin() != null && rl.getComponent() instanceof ComboBox) {
							List<UserRelatedField> rlChilds = new ArrayList();
							for (UserRelatedField rl1 : relatedList) {
								if (rl1 != null && rl1.getTruongThongTin() != null && rl1.getConstrantCode() != null
										&& rl1.getConstrantCode().equals(rl.getTruongThongTin().getMa())) {
									rlChilds.add(rl1);
								}
							}

							ComboBox cbb = (ComboBox) rl.getComponent();
							Object objValue = cbb.getValue();
							if (objValue != null) {
								setUserRelateField(rlChilds, rl, false);
							}

							cbb.addValueChangeListener(new ValueChangeListener() {
								@Override
								public void valueChange(ValueChangeEvent event) {
									Object value = event.getProperty().getValue();
									if (value != null) {
										setUserRelateField(rlChilds, rl, true);
									}
								}
							});
						}
					}
				}
			}
		}
	}

	public void setUserRelateField(List<UserRelatedField> rlChilds, UserRelatedField rl, boolean setNull) {

		// xoa du lieu combobox con
		if (rlChilds != null && !rlChilds.isEmpty()) {
			for (UserRelatedField rlChild : rlChilds) {
				if (rlChild != null && rlChild.getComponent() != null) {
					Component comp = rlChild.getComponent();
					if (comp instanceof ComboBox) {
						ComboBox cbb = (ComboBox) comp;
						cbb.getContainerDataSource().removeAllItems();
						if (setNull) {
							cbb.setValue(null);
						}
					}

				}
			}
		}

		if (rl != null && rl.getTruongThongTin() != null && rl.getTruongThongTin().getKieuDuLieu() != null) {

			int kieuDL = rl.getTruongThongTin().getKieuDuLieu().intValue();
			if (kieuDL == CommonUtils.EKieuDuLieu.USER.getMa()) {
				Component comp = rl.getComponent();
				if (!(comp instanceof ComboBox)) {
					return;
				}
				ComboBox currentCombo = (ComboBox) comp;
				Object currentVal = currentCombo.getValue();
				String userIdSelected = (String) currentVal;
				for (UserRelatedField rlChild : rlChilds) {
					if (rlChild != null && rlChild.getComponent() != null && rlChild.getTruongThongTin() != null
							&& rlChild.getTruongThongTin().getKieuDuLieu() != null) {
						int kieuDLCon = rlChild.getTruongThongTin().getKieuDuLieu().intValue();
						if (kieuDLCon == CommonUtils.EKieuDuLieu.GROUP.getMa()
								&& rlChild.getComponent() instanceof ComboBox) {
							ComboBox cbb = (ComboBox) rlChild.getComponent();
							List<Group> groups = identityService.createGroupQuery().groupMember(userIdSelected).list();
							if (groups != null && !groups.isEmpty()) {
								for (Group o : groups) {
									cbb.addItem(o.getId());
									cbb.setItemCaption(o.getId(), o.getName());
								}
							}
						} else if (kieuDLCon == CommonUtils.EKieuDuLieu.CHUC_VU.getMa()
								&& rlChild.getComponent() instanceof ComboBox) {
							ComboBox cbb = (ComboBox) rlChild.getComponent();
							String cv = identityService.getUserInfo(userIdSelected, "HT_CHUC_VU");
							if (cv != null) {
								List<HtChucVu> cvList = htChucVuDao.getHtChucVuFind(cv, null, null, 0, 1);
								if (cvList != null && !cvList.isEmpty()) {
									for (HtChucVu o : cvList) {
										cbb.addItem(o.getMa());
										cbb.setItemCaption(o.getMa(), o.getTen());
									}
								}
							}
						} else if (kieuDLCon == CommonUtils.EKieuDuLieu.DON_VI.getMa()
								&& rlChild.getComponent() instanceof ComboBox) {
							ComboBox cbb = (ComboBox) rlChild.getComponent();
							String dv = identityService.getUserInfo(userIdSelected, "HT_DON_VI");
							if (dv != null) {
								List<HtDonVi> dvList = htDonViServices.getHtDonViFind(dv, null, null, null, 0, 1);
								if (dvList != null && !dvList.isEmpty()) {
									for (HtDonVi o : dvList) {
										cbb.addItem(o.getMa());
										cbb.setItemCaption(o.getMa(), o.getTen());
									}
								}
							}
						}
					}
				}

			} else if (kieuDL == CommonUtils.EKieuDuLieu.GROUP.getMa()) {
				Component comp = rl.getComponent();
				if (!(comp instanceof ComboBox)) {
					return;
				}
				ComboBox currentCombo = (ComboBox) comp;
				Object currentVal = currentCombo.getValue();
				String groupIdSelected = (String) currentVal;
				for (UserRelatedField rlChild : rlChilds) {
					if (rlChild != null && rlChild.getComponent() != null && rlChild.getTruongThongTin() != null
							&& rlChild.getTruongThongTin().getKieuDuLieu() != null) {
						int kieuDLCon = rlChild.getTruongThongTin().getKieuDuLieu().intValue();
						if (kieuDLCon == CommonUtils.EKieuDuLieu.USER.getMa()
								&& rlChild.getComponent() instanceof ComboBox) {
							ComboBox cbb = (ComboBox) rlChild.getComponent();
							List<User> users = identityService.createUserQuery().memberOfGroup(groupIdSelected).list();
							if (users != null && !users.isEmpty()) {
								for (User o : users) {
									cbb.addItem(o.getId());
									cbb.setItemCaption(o.getId(), o.getFirstName() + " " + o.getLastName());
								}
							}
						}
					}
				}
			} else if (kieuDL == CommonUtils.EKieuDuLieu.CHUC_VU.getMa()) {
				Component comp = rl.getComponent();
				if (!(comp instanceof ComboBox)) {
					return;
				}
				ComboBox currentCombo = (ComboBox) comp;
				Object currentVal = currentCombo.getValue();
				String maChucVuSelected = (String) currentVal;
				for (UserRelatedField rlChild : rlChilds) {
					if (rlChild != null && rlChild.getComponent() != null && rlChild.getTruongThongTin() != null
							&& rlChild.getTruongThongTin().getKieuDuLieu() != null) {
						int kieuDLCon = rlChild.getTruongThongTin().getKieuDuLieu().intValue();
						if (kieuDLCon == CommonUtils.EKieuDuLieu.USER.getMa()
								&& rlChild.getComponent() instanceof ComboBox) {
							ComboBox cbb = (ComboBox) rlChild.getComponent();
							List<User> users = identityService.createUserQuery().list();
							List<User> userChucVu = new ArrayList();
							if (users != null && !users.isEmpty()) {
								for (User u : users) {
									String maCvTmp = identityService.getUserInfo(u.getId(), "HT_CHUC_VU");
									if (maCvTmp != null && maCvTmp.equals(maChucVuSelected)) {
										userChucVu.add(u);
									}
								}
							}

							if (userChucVu != null && !userChucVu.isEmpty()) {
								for (User o : userChucVu) {
									cbb.addItem(o.getId());
									cbb.setItemCaption(o.getId(), o.getFirstName() + " " + o.getLastName());
								}
							}
						}
					}
				}
			} else if (kieuDL == CommonUtils.EKieuDuLieu.DON_VI.getMa()) {
				Component comp = rl.getComponent();
				if (!(comp instanceof ComboBox)) {
					return;
				}
				ComboBox currentCombo = (ComboBox) comp;
				Object currentVal = currentCombo.getValue();
				String maDonViSelected = (String) currentVal;
				for (UserRelatedField rlChild : rlChilds) {
					if (rlChild != null && rlChild.getComponent() != null && rlChild.getTruongThongTin() != null
							&& rlChild.getTruongThongTin().getKieuDuLieu() != null) {
						int kieuDLCon = rlChild.getTruongThongTin().getKieuDuLieu().intValue();
						if (kieuDLCon == CommonUtils.EKieuDuLieu.USER.getMa()
								&& rlChild.getComponent() instanceof ComboBox) {
							ComboBox cbb = (ComboBox) rlChild.getComponent();
							List<User> users = identityService.createUserQuery().list();
							List<User> userDonVi = new ArrayList();
							if (users != null && !users.isEmpty()) {
								for (User u : users) {
									String maDvTmp = identityService.getUserInfo(u.getId(), "HT_DON_VI");
									if (maDvTmp != null && maDvTmp.equals(maDonViSelected)) {
										userDonVi.add(u);
									}
								}
							}

							if (userDonVi != null && !userDonVi.isEmpty()) {
								for (User o : userDonVi) {
									cbb.addItem(o.getId());
									cbb.setItemCaption(o.getId(), o.getFirstName() + " " + o.getLastName());
								}
							}

						} else if (kieuDLCon == CommonUtils.EKieuDuLieu.DON_VI.getMa()
								&& rlChild.getComponent() instanceof ComboBox) {
							ComboBox cbb = (ComboBox) rlChild.getComponent();
							if (maDonViSelected != null) {
								List<HtDonVi> donViList = htDonViServices.getHtDonViFind(maDonViSelected, null, null,
										null, 0, 1);
								if (donViList != null && !donViList.isEmpty()) {
									HtDonVi dvSelected = donViList.get(0);
									WorkflowManagement<HtDonVi> htDonViWM = new WorkflowManagement<HtDonVi>(null);
									List<Long> trangThai = null;
									if (htDonViWM != null && htDonViWM.getTrangThaiSuDung() != null) {
										trangThai = new ArrayList();
										trangThai.add(new Long(htDonViWM.getTrangThaiSuDung().getId()));
									}
									List<HtDonVi> dvList = htDonViServices.getHtDonViFind(null, null, dvSelected,
											trangThai, 0, 1);
									if (dvList != null && !dvList.isEmpty()) {
										for (HtDonVi o : dvList) {
											cbb.addItem(o.getMa());
											cbb.setItemCaption(o.getMa(), o.getTen());
										}
									}
								}
							}
						}
					}
				}
			} else if (kieuDL == CommonUtils.EKieuDuLieu.COMPLETED_USER.getMa()) {
				Component comp = rl.getComponent();
				if (!(comp instanceof ComboBox)) {
					return;
				}
				ComboBox currentCombo = (ComboBox) comp;
				Object currentVal = currentCombo.getValue();
				String userId = (String) currentVal;
				if (userId == null || userId.isEmpty()) {
					return;
				}
				User userCurrent = identityService.createUserQuery().userId(userId).singleResult();
				if (userCurrent == null) {
					return;
				}
				String maHtDonVi = identityService.getUserInfo(userId, "HT_DON_VI");
				String maChucVu = identityService.getUserInfo(userId, "HT_CHUC_VU");
				for (UserRelatedField rlChild : rlChilds) {
					if (rlChild != null && rlChild.getComponent() != null && rlChild.getTruongThongTin() != null
							&& rlChild.getTruongThongTin().getKieuDuLieu() != null) {
						int kieuDLCon = rlChild.getTruongThongTin().getKieuDuLieu().intValue();
						if (maHtDonVi != null && !maHtDonVi.isEmpty()
								&& kieuDLCon == CommonUtils.EKieuDuLieu.DON_VI.getMa()
								&& rlChild.getComponent() instanceof ComboBox) {
							ComboBox cbb = (ComboBox) rlChild.getComponent();

							List<HtDonVi> donViList = htDonViServices.getHtDonViFind(maHtDonVi, null, null, null, 0, 1);
							if (donViList != null && !donViList.isEmpty()) {
								HtDonVi dvSelected = donViList.get(0);
								WorkflowManagement<HtDonVi> htDonViWM = new WorkflowManagement<HtDonVi>(null);
								List<Long> trangThai = null;
								if (htDonViWM != null && htDonViWM.getTrangThaiSuDung() != null) {
									trangThai = new ArrayList();
									trangThai.add(new Long(htDonViWM.getTrangThaiSuDung().getId()));
								}
								List<HtDonVi> dvList = htDonViServices.getHtDonViFind(null, null, dvSelected, trangThai,
										0, 1);
								if (dvList != null && !dvList.isEmpty()) {
									for (HtDonVi o : dvList) {
										cbb.addItem(o.getMa());
										cbb.setItemCaption(o.getMa(), o.getTen());
									}
								}
							}
						} else if (maChucVu != null && !maChucVu.isEmpty()
								&& kieuDLCon == CommonUtils.EKieuDuLieu.CHUC_VU.getMa()
								&& rlChild.getComponent() instanceof ComboBox) {
							ComboBox cbb = (ComboBox) rlChild.getComponent();
							List<HtChucVu> cvList = htChucVuDao.getHtChucVuFind(maChucVu, null, null, 0, 1);
							if (cvList != null && !cvList.isEmpty()) {
								for (HtChucVu o : cvList) {
									cbb.addItem(o.getMa());
									cbb.setItemCaption(o.getMa(), o.getTen());
								}
							}
						}
					}
				}

			}
		}

	}

	public void initBieuMauGrid(GridLayout grid, BieuMau bm) {
		grid.removeAllComponents();
		grid.setStyleName(ExplorerLayout.E_FORM_GENERATE);
		grid.setWidth("100%");
		grid.setColumns(bm.getSoCot().intValue() + 1);
		grid.setRows(bm.getSoHang().intValue() + 1);
		Long soHangLap = bieuMauService.getBmHangFind(null, null, new Long(CommonUtils.ELoaiHang.LAP.getCode()), bm);
		if (soHangLap > 0 && !readOnly && !htmlDisplay) {
			grid.setColumnExpandRatio(bm.getSoCot().intValue(), 0.25f);
		} else {
			grid.setColumnExpandRatio(bm.getSoCot().intValue(), 0f);
		}
		for (int i = 0; i < bm.getSoCot().intValue(); i++) {
			grid.setColumnExpandRatio(i, 1f);
		}

	}

	public void createBieuMauUI(EFormTreeItem treeItem, GridLayout layoutMaster, String parentId, String parentKey) {
		Object obj = treeItem.getE();

		if (obj instanceof BieuMau) {
			BieuMau bieuMau = (BieuMau) obj;

			String keyTmp = bieuMau.getMa();
			if (bieuMauCurrent.equals(bieuMau)) {
				keyTmp = "ROOT";
			} else if (treeItem.getParent() != null && treeItem.getParent().getE() != null
					&& treeItem.getParent().getE() instanceof BmO) {
				BmO bmO = (BmO) treeItem.getParent().getE();
				if (bmO != null && bmO.getTen() != null && !bmO.getTen().trim().isEmpty()) {
					keyTmp = bmO.getTen().trim();
				}
			}

			List<EFormTreeItem> childList = treeItem.getChildList();
			int dem = 0;
			for (EFormTreeItem child : childList) {
				if (child != null) {
					if (child.getE() == null) {
						// Hang lap
						boolean addButton = true;
						int i = 1;
						// Danh sach hang con
						List<EFormTreeItem> childSubList = child.getChildList();
						if (childSubList != null) {
							for (EFormTreeItem childSub : childSubList) {
								if (childSub.getE() instanceof BmHang) {
									createBmHangUI(childSub, layoutMaster,
											(parentId == null ? "BM" + bieuMau.getId()
													: parentId + "BM" + bieuMau.getId()) + "STT" + i,
											(parentKey == null ? "BM" + keyTmp : parentKey + "BM" + keyTmp) + "#STT"
													+ i,
											dem);
									Long displayButton = 0L;
									List<EFormTreeItem> childSubbmOList = childSub.getChildList();
									for (EFormTreeItem childSubbmO : childSubbmOList) {
										if (childSubbmO.getE() instanceof BmO) {
											BmO bmO = (BmO) childSubbmO.getE();
											//
											System.out.println("getDisplayButton: "+bmO.getDisplayButton());
											displayButton = bmO.getDisplayButton();
											break;
										}
										
									}
									BmHang hangCurrent = (BmHang) childSub.getE();
									CssLayout lastCell = new CssLayout();
									lastCell.addStyleName(ExplorerLayout.LAST_CELL_WRAP);

									int fromColumn = bieuMau.getSoCot().intValue();
									int fromRow = hangCurrent.getThuTu().intValue() + dem;
									int toColumn = bieuMau.getSoCot().intValue();
									int toRow = hangCurrent.getThuTu().intValue() + dem;

									layoutMaster.addComponent(lastCell, fromColumn, fromRow, toColumn, toRow);
									if(displayButton!=1L) {
									if (!readOnly && !htmlDisplay) {
										if (addButton) {
											addButton = false;
											Button btnAddRow = new Button();
											btnAddRow.setIcon(FontAwesome.PLUS);
											btnAddRow.addStyleName(ExplorerLayout.BTN_ADD_ROW);
											btnAddRow.setData(childSub);
											btnAddRow.setId(BTN_ADD_ROW_ID);
											btnAddRow.addClickListener(this);
											lastCell.addComponent(btnAddRow);											
										} else if (!addButton) {
											Button btnRemoveRow = new Button();
											btnRemoveRow.setIcon(FontAwesome.REMOVE);
											btnRemoveRow.setId(BTN_REMOVE_ROW_ID);
											btnRemoveRow.addClickListener(this);
											btnRemoveRow.addStyleName(ExplorerLayout.BTN_REMOVE_ROW);

											Map<String, Object> removeData = new HashMap();
											removeData.put("removeId", (parentId == null ? "BM" + bieuMau.getId()
													: parentId + "BM" + bieuMau.getId()) + "STT" + i);
											removeData.put("removeKey",
													(parentKey == null ? "BM" + keyTmp : parentKey + "BM" + keyTmp)
															+ "#STT" + i);
											removeData.put("removeItem", childSub);

											btnRemoveRow.setData(removeData);
											lastCell.addComponent(btnRemoveRow);
										}
									}
									}

								}
								i++;
								dem++;
								layoutMaster.setRows(layoutMaster.getRows() + 1);
							}
						}
					} else if (child.getE() instanceof BmHang) {
						// Hang khong lap
						String key = parentId == null ? "BM" + bieuMau.getId() : parentId + "BM" + bieuMau.getId();
						createBmHangUI(child, layoutMaster, key, parentKey, dem);
						BmHang hangCurrent = (BmHang) child.getE();
						CssLayout lastCell = new CssLayout();
						lastCell.addStyleName(ExplorerLayout.LAST_CELL_WRAP);
						layoutMaster.addComponent(lastCell, bieuMau.getSoCot().intValue(),
								hangCurrent.getThuTu().intValue() + dem, bieuMau.getSoCot().intValue(),
								hangCurrent.getThuTu().intValue() + dem);

					}
				}
			}
		}
	}

	public void createBmHangUI(EFormTreeItem treeItem, GridLayout layoutMaster, String parentId, String parentKey,
			int rowNumInc) {
		Object obj = treeItem.getE();
		if (obj == null || obj instanceof BmHang) {
			// Danh sach o
			List<EFormTreeItem> childList = treeItem.getChildList();
			if (childList != null) {
				for (EFormTreeItem child : childList) {
					if (child != null) {
						if (child.getE() instanceof BmO) {
							BmO o = (BmO) child.getE();
							CssLayout cellWrap = new CssLayout();
							cellWrap.setWidth("100%");
							cellWrap.addStyleName(ExplorerLayout.E_FORM_CELL_WRAP);

							int colSpan = o.getGhepCot() != null ? o.getGhepCot().intValue() : 1;
							int rowSpan = o.getGhepHang() != null ? o.getGhepHang().intValue() : 1;

							int fromColumn = o.getViTri().intValue();
							int fromRow = o.getBmHang().getThuTu().intValue() + rowNumInc;
							int toColumn = o.getViTri().intValue() + colSpan - 1;
							int toRow = o.getBmHang().getThuTu().intValue() + rowSpan - 1 + rowNumInc;

							layoutMaster.addComponent(cellWrap, fromColumn, fromRow, toColumn, toRow);
							createCellUI(child, cellWrap, parentId, parentKey);
						}
					}
				}
			}
		}
	}

	public void createCellUI(EFormTreeItem treeItem, CssLayout groupMaster, String parentId, String parentKey) {
		Object obj = treeItem.getE();
		if (obj instanceof BmO) {
			BmO bmO = (BmO) obj;
			List<EFormTreeItem> childList = treeItem.getChildList();

			for (EFormTreeItem child : childList) {
				if (child != null) {
					if (child.getE() instanceof BmOTruongThongTin) {
						BmOTruongThongTin oTruongThongTin = (BmOTruongThongTin) child.getE();
						createInputUI(oTruongThongTin, groupMaster, parentId);
					} else if (child.getE() instanceof BieuMau) {
						BieuMau bieuMau = (BieuMau) child.getE();
						GridLayout layoutSub = new GridLayout();
						initBieuMauGrid(layoutSub, bieuMau);
						createBieuMauUI(child, layoutSub, parentId, parentKey);
						groupMaster.addComponent(layoutSub);
					}
				}
			}
			if (bmO.getLoaiO() != null && bmO.getLoaiO().intValue() == ELoaiO.TEXT.getCode()) {
				Label text = new Label(bmO.getTen());
				text.setContentMode(ContentMode.HTML);
				text.addStyleName(ExplorerLayout.TEXT_CELL_CONTENT);
				groupMaster.addComponent(text);
			}
		}
	}

	public void setRequired(BmOTruongThongTin oTruongThongTin, Field f) {
		// Required
		if (oTruongThongTin != null && oTruongThongTin.getTruongThongTin() != null
				&& oTruongThongTin.getTruongThongTin().getRequired() != null && CommonUtils.EBatBuoc.BAT_BUOC
						.getCode() == oTruongThongTin.getTruongThongTin().getRequired().intValue()) {
			f.setRequired(true);
			f.setRequiredError("Không được để trống");
		}
	}

	public void setCaption(BmOTruongThongTin oTruongThongTin, Field f) {
		if (oTruongThongTin != null && oTruongThongTin.getTruongThongTin() != null
				&& oTruongThongTin.getTruongThongTin().getLoaiHienThi() != null && oTruongThongTin.getTruongThongTin()
						.getLoaiHienThi().intValue() == CommonUtils.ELoaiHienThi.INPUT_WITH_TEXT.getCode()) {
			f.setCaption(oTruongThongTin.getTruongThongTin().getTen());
		}
	}

	public void setExpression(BmOTruongThongTin oTruongThongTin, Field f) {
		// Expression
		if (oTruongThongTin != null && oTruongThongTin.getTruongThongTin() != null
				&& oTruongThongTin.getTruongThongTin().getExpressions() != null
				&& !oTruongThongTin.getTruongThongTin().getExpressions().isEmpty()) {
			String msg = oTruongThongTin.getTruongThongTin().getExpressionMessage();
			if (msg == null || msg.isEmpty()) {
				msg = "Dữ liệu không đúng";
			}
			f.addValidator(new RegexpValidator(oTruongThongTin.getTruongThongTin().getExpressions(), msg));
		}
	}

	public void setLimitValue(BmOTruongThongTin oTruongThongTin, Field f) {
		// Minimum length maximum length
		if (oTruongThongTin != null && oTruongThongTin.getTruongThongTin() != null) {
			if (oTruongThongTin.getTruongThongTin().getMinlength() != null
					&& oTruongThongTin.getTruongThongTin().getMinlength().intValue() > 0
					&& oTruongThongTin.getTruongThongTin().getMaxlength() != null
					&& oTruongThongTin.getTruongThongTin().getMaxlength().intValue() > oTruongThongTin
							.getTruongThongTin().getMinlength().intValue()) {
				Integer minLength = oTruongThongTin.getTruongThongTin().getMinlength().intValue();
				Integer maxLength = oTruongThongTin.getTruongThongTin().getMaxlength().intValue();
				f.addValidator(new StringLengthValidator(
						"Số ký tự cho phép từ " + minLength.intValue() + " - " + maxLength.intValue(), minLength,
						maxLength, true));
			} else if (oTruongThongTin.getTruongThongTin().getMinlength() != null
					&& oTruongThongTin.getTruongThongTin().getMinlength().intValue() > 0) {
				Integer minLength = oTruongThongTin.getTruongThongTin().getMinlength().intValue();
				f.addValidator(new StringLengthValidator("Số ký tự tối thiểu là " + minLength.intValue(), minLength,
						null, true));
			} else if (oTruongThongTin.getTruongThongTin().getMaxlength() != null
					&& oTruongThongTin.getTruongThongTin().getMaxlength().intValue() > 0) {
				Integer maxLength = oTruongThongTin.getTruongThongTin().getMaxlength().intValue();
				f.addValidator(
						new StringLengthValidator("Số ký tự tối đa là " + maxLength.intValue(), null, maxLength, true));
			}
		}
	}

	public void setLimitNumberValue(BmOTruongThongTin oTruongThongTin, Field f, boolean soNguyen) {
		if (oTruongThongTin != null && oTruongThongTin.getTruongThongTin() != null) {
			if (!soNguyen) {
				if (oTruongThongTin.getTruongThongTin().getMinvalue() != null
						&& oTruongThongTin.getTruongThongTin().getMaxvalue() != null
						&& oTruongThongTin.getTruongThongTin().getMaxvalue().intValue() > oTruongThongTin
								.getTruongThongTin().getMinvalue().intValue()) {
					Double minValue = oTruongThongTin.getTruongThongTin().getMinvalue().doubleValue();
					Double maxValue = oTruongThongTin.getTruongThongTin().getMaxvalue().doubleValue();
					f.addValidator(new DoubleRangeValidator("Chỉ cho phép nhập số từ" + minValue + " - " + maxValue,
							minValue, maxValue));
				} else if (oTruongThongTin.getTruongThongTin().getMinvalue() != null) {
					Double minValue = oTruongThongTin.getTruongThongTin().getMinvalue().doubleValue();
					f.addValidator(new DoubleRangeValidator("Chỉ cho phép nhập số lớn hơn" + minValue, minValue, null));
				} else if (oTruongThongTin.getTruongThongTin().getMaxvalue() != null) {
					Double maxValue = oTruongThongTin.getTruongThongTin().getMaxvalue().doubleValue();
					f.addValidator(
							new DoubleRangeValidator("Chỉ cho phép nhập số nhỏ hơn " + maxValue, null, maxValue));
				} else {
					f.addValidator(new DoubleRangeValidator("Chỉ cho phép nhập số", null, null));
				}
			} else {
				if (oTruongThongTin.getTruongThongTin().getMinvalue() != null
						&& oTruongThongTin.getTruongThongTin().getMaxvalue() != null
						&& oTruongThongTin.getTruongThongTin().getMaxvalue().intValue() > oTruongThongTin
								.getTruongThongTin().getMinvalue().intValue()) {
					Long minValue = oTruongThongTin.getTruongThongTin().getMinvalue().longValue();
					Long maxValue = oTruongThongTin.getTruongThongTin().getMaxvalue().longValue();
					f.addValidator(new LongRangeValidator(
							"Chỉ cho phép nhập số nguyên từ " + minValue + " - " + maxValue, minValue, maxValue));
				} else if (oTruongThongTin.getTruongThongTin().getMinvalue() != null) {
					Long minValue = oTruongThongTin.getTruongThongTin().getMinvalue().longValue();
					f.addValidator(
							new LongRangeValidator("Chỉ cho phép nhập số nguyên từ " + minValue, minValue, null));
				} else if (oTruongThongTin.getTruongThongTin().getMaxvalue() != null) {
					Long maxValue = oTruongThongTin.getTruongThongTin().getMaxvalue().longValue();
					f.addValidator(
							new LongRangeValidator("Chỉ cho phép nhập số nguyên nhỏ hơn " + maxValue, null, maxValue));
				} else {
					f.addValidator(new LongRangeValidator("Chỉ cho phép nhập số nguyên", null, null));
				}
			}
		}
	}

	private Object getValFormService(String key) {
		if (serviceConfig != null) {
			JSONObject paramMap = serviceConfig.getJSONObject("parammap");
			key = key.replace("BM" + bieuMauCurrent.getId() + "O", "");
			if (paramMap.has(key)) {
				String fiedName = paramMap.getString(key);
				if (fiedName != null && !fiedName.equalsIgnoreCase("")) {
					Object fieldVal = serviceResponse.get(fiedName);
					return fieldVal;
//				if(fieldVal!=null)
//				if (fieldVal instanceof JSONArray) {
//					System.out.println("");
//				} else if (fieldVal instanceof JSONObject) {
//					System.out.println("");
//				}else if (fieldVal instanceof String) {
//					input.setValue(fieldVal.toString());
//				}
				}
			}
		}
		return null;
	}

	public void createInputUI(BmOTruongThongTin oTruongThongTin, CssLayout groupMaster, String parentId) {
		if (oTruongThongTin.getTruongThongTin() != null
				&& oTruongThongTin.getTruongThongTin().getKieuDuLieu() != null) {
			int kieuDuLieu = oTruongThongTin.getTruongThongTin().getKieuDuLieu().intValue();
			// String valueKey = parentId + "O" + oTruongThongTin.getId();
			String valueKey = parentId + "O" + oTruongThongTin.getId();
			fieldsMap.put(oTruongThongTin.getTruongThongTin().getMa().toLowerCase(), valueKey);
			if (!valueMap.containsKey(valueKey)) {
				valueMap.put(valueKey, null);
			}

			boolean ro = false;
			if (oTruongThongTin.getReadOnly() != null
					&& oTruongThongTin.getReadOnly().intValue() == CommonUtils.EReadOnly.TRUE.getCode()) {
				ro = true;
			}

			if (CommonUtils.EKieuDuLieu.TEXT.getMa() == kieuDuLieu) {
				if (htmlDisplay) {

					String value = (String) valueMap.get(valueKey);
					String str = "";
					if (value != null) {
						str = value;
					}
					Label lbl = new Label(str);
					lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
					lbl.setContentMode(ContentMode.HTML);
					groupMaster.addComponent(lbl);
				} else {
					TextField input = new TextField();
					input.setNullRepresentation("");
					input.setWidth("100%");
					input.addStyleName(ExplorerLayout.FORM_CONTROL);
					input.addStyleName(ExplorerLayout.FORM_INPUT_TEXT);

					setRequired(oTruongThongTin, input);
					setCaption(oTruongThongTin, input);
					setExpression(oTruongThongTin, input);
					setLimitValue(oTruongThongTin, input);

					String value = valueMap.get(valueKey) != null ? String.valueOf(valueMap.get(valueKey)) : null;
					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(input, valueKey);
					}

					input.setReadOnly(readOnly || ro);
					input.setDescription(value);
					if (serviceConfig != null && serviceConfig.has("onchange")) {
						Object onchange = serviceConfig.get("onchange");
						String ma = oTruongThongTin.getTruongThongTin().getMa();
						if (onchange != null && onchange instanceof JSONObject && ((JSONObject) onchange).has(ma)) {
							input.addValueChangeListener(new ValueChangeListener() {
								@Override
								public void valueChange(ValueChangeEvent event) {
									Object value = event.getProperty().getValue();
									if (value != null && value instanceof String) {
										valueMap.put(valueKey, value);

										Map<String, String> params = new HashMap<>();
										params.put(ma, value.toString());
										ProcessService(params);
									}

								}
							});
						}
					}

					groupMaster.addComponent(input);
				}
			} else if (CommonUtils.EKieuDuLieu.EMAIL.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					String value = (String) valueMap.get(valueKey);

					String str = "";
					if (value != null) {
						str = value;
					}
					Label lbl = new Label(str);

					lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);

					lbl.setContentMode(ContentMode.HTML);
					groupMaster.addComponent(lbl);
				} else {
					TextField input = new TextField();
					input.setNullRepresentation("");
					input.setWidth("100%");
					input.addStyleName(ExplorerLayout.FORM_CONTROL);
					input.addStyleName(ExplorerLayout.FORM_EMAIL);

					setRequired(oTruongThongTin, input);
					setCaption(oTruongThongTin, input);
					setLimitValue(oTruongThongTin, input);
					if (emailPattern != null) {
						input.addValidator(new RegexpValidator(emailPattern, "Email không chính xác!"));
					}

					String value = (String) valueMap.get(valueKey);
					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(input, valueKey);
					}

					input.setDescription(value);

					input.setReadOnly(readOnly || ro);
					groupMaster.addComponent(input);
				}
			} else if (CommonUtils.EKieuDuLieu.TEXT_AREA.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					String value = (String) valueMap.get(valueKey);
					String str = "";
					if (value != null) {
						str = value;
					}
					Label lbl = new Label(str);
					lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);

					lbl.setContentMode(ContentMode.HTML);
					groupMaster.addComponent(lbl);
				} else {
					TextArea input = new TextArea();
					input.setNullRepresentation("");
					input.setWidth("100%");
					input.addStyleName(ExplorerLayout.FORM_CONTROL);
					input.addStyleName(ExplorerLayout.FORM_TEXT_AREA);

					setRequired(oTruongThongTin, input);
					setCaption(oTruongThongTin, input);
					setExpression(oTruongThongTin, input);
					setLimitValue(oTruongThongTin, input);

					String value = (String) valueMap.get(valueKey);
					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(input, valueKey);
					}
					input.setDescription(value);
					input.setReadOnly(readOnly || ro);
					groupMaster.addComponent(input);
				}
			} else if (CommonUtils.EKieuDuLieu.NUMBER.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					Double value = (Double) valueMap.get(valueKey);
					String str = "";
					if (value != null) {
						str = value + "";
					}
					Label lbl = new Label(str);
					lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);

					lbl.setContentMode(ContentMode.HTML);
					groupMaster.addComponent(lbl);
				} else {
					TextField inputText = new TextField();
					inputText.setWidth("100%");
					inputText.setNullRepresentation("");
					inputText.addStyleName(ExplorerLayout.FORM_CONTROL);
					inputText.addStyleName(ExplorerLayout.FORM_NUMBER);

					setRequired(oTruongThongTin, inputText);
					setCaption(oTruongThongTin, inputText);
					setExpression(oTruongThongTin, inputText);
					setLimitNumberValue(oTruongThongTin, inputText, false);

					Double value = (Double) valueMap.get(valueKey);

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<Double>(value, Double.class));
						fieldGroup.bind(inputText, valueKey);
					}

					inputText.setReadOnly(readOnly || ro);
					groupMaster.addComponent(inputText);
				}
			} else if (CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					Long value = (Long) valueMap.get(valueKey);
					String str = "";
					if (value != null) {
						str = value + "";
					}
					Label lbl = new Label(str);
					lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);

					lbl.setContentMode(ContentMode.HTML);
					groupMaster.addComponent(lbl);
				} else {
					TextField inputText = new TextField();
					inputText.setWidth("100%");
					inputText.setNullRepresentation("");
					inputText.setNullSettingAllowed(true);
					inputText.addStyleName(ExplorerLayout.FORM_CONTROL);
					inputText.addStyleName(ExplorerLayout.FORM_NUMBER);

					setRequired(oTruongThongTin, inputText);
					setCaption(oTruongThongTin, inputText);
					setExpression(oTruongThongTin, inputText);
					setLimitNumberValue(oTruongThongTin, inputText, true);

					Object valueTmp = valueMap.get(valueKey);

					Long value = null;
					if (valueTmp instanceof Long) {
						value = (Long) valueTmp;
					} else if (valueTmp instanceof Double) {
						value = ((Double) valueTmp).longValue();
					}

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<Long>(value, Long.class));
						fieldGroup.bind(inputText, valueKey);
					}
					inputText.setReadOnly(readOnly || ro);

					if (value != null) {
						inputText.setDescription(value.toString());
					}
					groupMaster.addComponent(inputText);
				}
			} else if (CommonUtils.EKieuDuLieu.DATE.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					Date d = (Date) valueMap.get(valueKey);
					if (d != null) {
						SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
						Label lbl = new Label(df.format(d));
						lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
						groupMaster.addComponent(lbl);
					}

				} else {
					PopupDateField dateField = new PopupDateField();
					dateField.addStyleName(ExplorerLayout.FORM_CONTROL);
					dateField.addStyleName(ExplorerLayout.FORM_DATE);
					dateField.setWidth("100%");

					if (oTruongThongTin.getTruongThongTin() != null
							&& oTruongThongTin.getTruongThongTin().getTimeDetail() != null
							&& oTruongThongTin.getTruongThongTin().getTimeDetail()
									.intValue() == CommonUtils.ETimeDetail.DATE_AND_TIME.getCode()) {
						dateField.setResolution(Resolution.MINUTE);
					}

					setRequired(oTruongThongTin, dateField);
					setCaption(oTruongThongTin, dateField);

					Date minValueDate = null;
					Date maxValueDate = null;

					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
					if (oTruongThongTin.getTruongThongTin() != null
							&& oTruongThongTin.getTruongThongTin().getMinDurationType() != null
							&& oTruongThongTin.getTruongThongTin().getMinDuration() != null) {
						if (oTruongThongTin.getTruongThongTin().getMinDurationType()
								.intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()) {
							String duration = oTruongThongTin.getTruongThongTin().getMinDuration();
							if (!duration.isEmpty()) {
								try {
									minValueDate = df.parse(duration.trim());
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						} else if (oTruongThongTin.getTruongThongTin().getMinDurationType()
								.intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()) {
							String duration = oTruongThongTin.getTruongThongTin().getMinDuration();
							if (!duration.isEmpty()) {
								Period period = Period.parse(duration);
								LocalDate date = LocalDate.now();
								minValueDate = java.sql.Date.valueOf(date.plusYears(period.getYears())
										.plusMonths(period.getMonths()).plusDays(period.getDays()));
							}
						}
					}

					if (oTruongThongTin.getTruongThongTin() != null
							&& oTruongThongTin.getTruongThongTin().getMaxDurationType() != null
							&& oTruongThongTin.getTruongThongTin().getMaxDuration() != null) {
						if (oTruongThongTin.getTruongThongTin().getMaxDurationType()
								.intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()) {
							String duration = oTruongThongTin.getTruongThongTin().getMaxDuration();
							if (!duration.isEmpty()) {
								try {
									maxValueDate = df.parse(duration.trim());
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						} else if (oTruongThongTin.getTruongThongTin().getMaxDurationType()
								.intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()) {
							String duration = oTruongThongTin.getTruongThongTin().getMaxDuration();
							if (!duration.isEmpty()) {
								Period period = Period.parse(duration);
								LocalDate date = LocalDate.now();
								maxValueDate = java.sql.Date.valueOf(date.plusYears(period.getYears())
										.plusMonths(period.getMonths()).plusDays(period.getDays()));
							}
						}
					}

					if (minValueDate != null || maxValueDate != null) {
						String message = "";
						SimpleDateFormat df1 = new SimpleDateFormat(Constants.DATE_FORMAT_SHORT);
						if (minValueDate != null) {
							message += " lớn hơn ngày " + df1.format(minValueDate);
						}
						if (maxValueDate != null) {
							if (!message.isEmpty()) {
								message += " và";
							}
							message += " nhỏ hơn ngày " + df1.format(maxValueDate);
						}
						message = "Giá trị bắt buộc" + message;
						DateRangeValidator range = new DateRangeValidator(message, minValueDate, maxValueDate, null);
						dateField.addValidator(range);
					}

					Date d = (Date) valueMap.get(valueKey);

					if (d == null && oTruongThongTin.getTruongThongTin() != null
							&& oTruongThongTin.getTruongThongTin().getDateDefaultType() != null
							&& oTruongThongTin.getTruongThongTin().getDefaultValue() != null) {
						if (oTruongThongTin.getTruongThongTin().getDateDefaultType()
								.intValue() == CommonUtils.ELoaiThoiGian.CO_DINH.getCode()) {
							String duration = oTruongThongTin.getTruongThongTin().getDefaultValue();
							if (!duration.isEmpty()) {
								try {
									d = df.parse(duration.trim());
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						} else if (oTruongThongTin.getTruongThongTin().getDateDefaultType()
								.intValue() == CommonUtils.ELoaiThoiGian.TUONG_DOI.getCode()) {
							String duration = oTruongThongTin.getTruongThongTin().getDefaultValue();
							if (!duration.isEmpty()) {
								Period period = Period.parse(duration);
								LocalDate date = LocalDate.now();
								d = java.sql.Date.valueOf(date.plusYears(period.getYears())
										.plusMonths(period.getMonths()).plusDays(period.getDays()));
							}
						}
					}

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<Date>(d, Date.class));
						fieldGroup.bind(dateField, valueKey);
					}
					dateField.setReadOnly(readOnly || ro);

					groupMaster.addComponent(dateField);
				}
			} else if (CommonUtils.EKieuDuLieu.CHECKBOX.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					Boolean value = (Boolean) valueMap.get(valueKey);
					String content = "&#9744;";
					if (value != null && value) {
						content = "&#9745;";
					}

					if (oTruongThongTin != null && oTruongThongTin.getTruongThongTin() != null
							&& oTruongThongTin.getTruongThongTin().getLoaiHienThi() != null
							&& oTruongThongTin.getTruongThongTin().getLoaiHienThi()
									.intValue() == CommonUtils.ELoaiHienThi.INPUT_WITH_TEXT.getCode()) {
						content += " " + oTruongThongTin.getTruongThongTin().getTen();
					}

					Label lbl = new Label(content);
					lbl.setContentMode(ContentMode.HTML);
					lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
					groupMaster.addComponent(lbl);
				} else {
					CheckBox input = new CheckBox();
					input.addStyleName(ExplorerLayout.FORM_CONTROL);
					input.addStyleName(ExplorerLayout.FORM_CHECKBOX);

					setCaption(oTruongThongTin, input);
					setRequired(oTruongThongTin, input);
					Boolean value = null;
					Object obj = valueMap.get(valueKey);
					if (obj != null) {
						if (obj instanceof Boolean) {
							value = (Boolean) obj;
						} else if (obj instanceof String) {
							String strValue = (String) obj;
							if ("true".equals(strValue)) {

							}
						}
					}

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<Boolean>(value, Boolean.class));
						fieldGroup.bind(input, valueKey);
					}
					input.setReadOnly(readOnly || ro);
					groupMaster.addComponent(input);
				}
			} else if (CommonUtils.EKieuDuLieu.DANH_MUC.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					String value = (String) valueMap.get(valueKey);
					if (value != null) {
						List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(value, null, null, null, null, 0, 1);
						if (danhMucList != null && !danhMucList.isEmpty()) {
							DanhMuc dm = danhMucList.get(0);
							Label lbl = new Label(dm.getTen());
							lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
							groupMaster.addComponent(lbl);
						}
					}

				} else {
					ComboBox comboBox = new ComboBox();
					comboBox.setFilteringMode(FilteringMode.CONTAINS);
					comboBox.setNullSelectionAllowed(true);
					comboBox.addStyleName(ExplorerLayout.FORM_CONTROL);
					comboBox.addStyleName(ExplorerLayout.FORM_COMBOBOX);

					setRequired(oTruongThongTin, comboBox);
					setCaption(oTruongThongTin, comboBox);
					RelatedField related = new RelatedField();
					String ma = oTruongThongTin.getTruongThongTin().getMa();

					Object fieldVal = getValFormService(ma);
					if (fieldVal != null && fieldVal instanceof JSONArray) {
						JSONArray arr = (JSONArray) fieldVal;
						for (Object item : arr) {
							JSONObject ob = (JSONObject) item;
							comboBox.addItem(ob.keys().next());
							comboBox.setItemCaption(ob.keys().next(), ob.getString(ob.keys().next()));
						}
						String value = (String) valueMap.get(valueKey);

						if (fieldGroup.getField(valueKey) == null) {
							itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
							fieldGroup.bind(comboBox, valueKey);
						}
						Object onchange = serviceConfig.get("onchange");
						if (onchange != null && onchange instanceof JSONObject && ((JSONObject) onchange).has(ma)) {
							comboBox.addListener(new ValueChangeListener() {
								@Override
								public void valueChange(ValueChangeEvent event) {
									// comboBox.setValue("ma03");
									valueMap.put(valueKey, event.getProperty().getValue());

									Map<String, String> params = new HashMap<>();
									params.put(ma, event.getProperty().getValue().toString());
									ProcessService(params);
									// refresh();
									// initService(bieuMauCurrent,params );

								}
							});
						}
						comboBox.setReadOnly(readOnly || ro);
					} else if (FormServiceConfig != null && FormServiceConfig.has("comboboxfield")) {
						JSONObject groupArr = FormServiceConfig.getJSONObject("comboboxfield");
						if (groupArr != null) {
							for (Iterator iterator = groupArr.keySet().iterator(); iterator.hasNext();) {
								String maTruongThongTin = (String) iterator.next();
								String maTruongThongTinInForm = groupArr.getString(maTruongThongTin);
								if (maTruongThongTinInForm.trim().equalsIgnoreCase(ma)) {
									List<Map<String, Object>> values = getFieldFormServiceConfig(maTruongThongTin);

									for (Map<String, Object> item : values) {
										comboBox.addItem(item.get("id").toString());
										comboBox.setItemCaption(item.get("id").toString(), item.get(maTruongThongTin).toString());
									}
									String value = (String) valueMap.get(valueKey);

									if (fieldGroup.getField(valueKey) == null) {
										itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
										fieldGroup.bind(comboBox, valueKey);
									}

									comboBox.addListener(new ValueChangeListener() {
										@Override
										public void valueChange(ValueChangeEvent event) {
											ProcessFormService(event.getProperty().getValue().toString());
											// comboBox.setValue("ma03");
//												valueMap.put(valueKey, event.getProperty().getValue());
		//
//												Map<String, String> params = new HashMap<>();
//												params.put(ma, event.getProperty().getValue().toString());
//												ProcessService(bieuMauCurrent, params);
											// refresh();
											// initService(bieuMauCurrent,params );

										}
									});	
								}else {

									WorkflowManagement<DanhMuc> danhMucWM = new WorkflowManagement<DanhMuc>(
											EChucNang.E_FORM_DANH_MUC.getMa());
									List<Long> trangThai = null;
									if (danhMucWM != null && danhMucWM.getTrangThaiSuDung() != null) {
										trangThai = new ArrayList<Long>();
										trangThai.add(new Long(danhMucWM.getTrangThaiSuDung().getId()));
									}

									LoaiDanhMuc loaiDanhMuc = oTruongThongTin.getTruongThongTin().getLoaiDanhMuc();
									if (loaiDanhMuc != null) {
										List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(null, null, trangThai,
												loaiDanhMuc, null, -1, -1);
										if (danhMucList != null && !danhMucList.isEmpty()) {
											for (DanhMuc dm : danhMucList) {
												comboBox.addItem(dm.getMa());
												comboBox.setItemCaption(dm.getMa(), dm.getTen());
											}
										}
									}
									String value = (String) valueMap.get(valueKey);

									if (fieldGroup.getField(valueKey) == null) {
										itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
										fieldGroup.bind(comboBox, valueKey);
									}
									comboBox.setReadOnly(readOnly || ro);

									List<RelatedField> relatedList = relatedFieldMap.get(parentId);
									if (relatedList == null) {
										relatedList = new ArrayList<RelatedField>();
										relatedFieldMap.put(parentId, relatedList);
									}

									related.setComboBox(comboBox);
									related.setLoaiDanhMuc(loaiDanhMuc);
									relatedList.add(related);
									if (value != null) {
										List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(value, null, trangThai,
												loaiDanhMuc, null, 0, 1);
										if (danhMucList != null && !danhMucList.isEmpty()) {
											comboBox.setDescription(danhMucList.get(0).getTen());
										}

									}
								}

							}
							
							comboBox.setReadOnly(readOnly || ro);
						}

					} else {

						WorkflowManagement<DanhMuc> danhMucWM = new WorkflowManagement<DanhMuc>(
								EChucNang.E_FORM_DANH_MUC.getMa());
						List<Long> trangThai = null;
						if (danhMucWM != null && danhMucWM.getTrangThaiSuDung() != null) {
							trangThai = new ArrayList<Long>();
							trangThai.add(new Long(danhMucWM.getTrangThaiSuDung().getId()));
						}

						LoaiDanhMuc loaiDanhMuc = oTruongThongTin.getTruongThongTin().getLoaiDanhMuc();
						if (loaiDanhMuc != null) {
							List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(null, null, trangThai,
									loaiDanhMuc, null, -1, -1);
							if (danhMucList != null && !danhMucList.isEmpty()) {
								for (DanhMuc dm : danhMucList) {
									comboBox.addItem(dm.getMa());
									comboBox.setItemCaption(dm.getMa(), dm.getTen());
								}
							}
						}
						String value = (String) valueMap.get(valueKey);

						if (fieldGroup.getField(valueKey) == null) {
							itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
							fieldGroup.bind(comboBox, valueKey);
						}
						comboBox.setReadOnly(readOnly || ro);

						List<RelatedField> relatedList = relatedFieldMap.get(parentId);
						if (relatedList == null) {
							relatedList = new ArrayList<RelatedField>();
							relatedFieldMap.put(parentId, relatedList);
						}

						related.setComboBox(comboBox);
						related.setLoaiDanhMuc(loaiDanhMuc);
						relatedList.add(related);
						if (value != null) {
							List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(value, null, trangThai,
									loaiDanhMuc, null, 0, 1);
							if (danhMucList != null && !danhMucList.isEmpty()) {
								comboBox.setDescription(danhMucList.get(0).getTen());
							}

						}
					}

					groupMaster.addComponent(comboBox);
				}
			} else if (CommonUtils.EKieuDuLieu.RADIO_BUTTON.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					String value = (String) valueMap.get(valueKey);
					if (value != null) {
						List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(value, null, null, null, null, 0, 1);
						if (danhMucList != null && !danhMucList.isEmpty()) {
							DanhMuc dm = danhMucList.get(0);
							Label lbl = new Label(dm.getTen());
							lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
							groupMaster.addComponent(lbl);
						}

					}

				} else {
					OptionGroup optionGroup = new OptionGroup();

					optionGroup.addStyleName(ExplorerLayout.FORM_CONTROL);
					optionGroup.addStyleName(ExplorerLayout.FORM_RADIO);
					optionGroup.setNullSelectionAllowed(true);

					setRequired(oTruongThongTin, optionGroup);
					setCaption(oTruongThongTin, optionGroup);

					WorkflowManagement<DanhMuc> danhMucWM = new WorkflowManagement<DanhMuc>(
							EChucNang.E_FORM_DANH_MUC.getMa());
					List<Long> trangThai = null;

					if (danhMucWM != null && danhMucWM.getTrangThaiSuDung() != null) {
						trangThai = new ArrayList<Long>();
						trangThai.add(new Long(danhMucWM.getTrangThaiSuDung().getId()));
					}
					LoaiDanhMuc loaiDanhMuc = oTruongThongTin.getTruongThongTin().getLoaiDanhMuc();
					if (loaiDanhMuc != null) {
						List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(null, null, trangThai, loaiDanhMuc,
								null, -1, -1);
						if (danhMucList != null && !danhMucList.isEmpty()) {
							for (DanhMuc dm : danhMucList) {
								optionGroup.addItem(dm.getMa());
								optionGroup.setItemCaption(dm.getMa(), dm.getTen());
							}
						}
					}
					String value = (String) valueMap.get(valueKey);

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(optionGroup, valueKey);
					}
					optionGroup.setReadOnly(readOnly || ro);

					groupMaster.addComponent(optionGroup);
				}
			} else if (CommonUtils.EKieuDuLieu.LUA_CHON_NHIEU.getMa() == kieuDuLieu) {
				if (htmlDisplay) {

					String valueTmp = (String) valueMap.get(valueKey);
					if (valueTmp != null && !valueTmp.isEmpty() && valueTmp.trim().length() > 2) {
						valueTmp = valueTmp.trim().substring(1, valueTmp.length() - 1);
						if (!valueTmp.isEmpty()) {
							String[] arr = valueTmp.split(",");
							if (arr != null && arr.length > 0) {
								LinkedHashSet<String> hs = new LinkedHashSet<String>();
								String str = "";
								for (String value : arr) {
									if (value != null) {
										value = value.trim();
										List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(value, null, null,
												null, null, 0, 1);
										if (danhMucList != null && !danhMucList.isEmpty()) {
											DanhMuc dm = danhMucList.get(0);
											str += dm.getTen() + "; ";
										}
									}

								}

								Label lbl = new Label(str);
								lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
								groupMaster.addComponent(lbl);
							}
						}
					}

				} else {
					OptionGroup optionGroup = new OptionGroup();

					optionGroup.addStyleName(ExplorerLayout.FORM_CONTROL);
					optionGroup.setNullSelectionAllowed(true);
					optionGroup.setMultiSelect(true);

					setRequired(oTruongThongTin, optionGroup);
					setCaption(oTruongThongTin, optionGroup);

					WorkflowManagement<DanhMuc> danhMucWM = new WorkflowManagement<DanhMuc>(
							EChucNang.E_FORM_DANH_MUC.getMa());
					List<Long> trangThai = null;

					if (danhMucWM != null && danhMucWM.getTrangThaiSuDung() != null) {
						trangThai = new ArrayList<Long>();
						trangThai.add(new Long(danhMucWM.getTrangThaiSuDung().getId()));
					}
					LoaiDanhMuc loaiDanhMuc = oTruongThongTin.getTruongThongTin().getLoaiDanhMuc();
					if (loaiDanhMuc != null) {
						List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(null, null, trangThai, loaiDanhMuc,
								null, -1, -1);
						if (danhMucList != null && !danhMucList.isEmpty()) {
							for (DanhMuc dm : danhMucList) {
								optionGroup.addItem(dm.getMa());
								optionGroup.setItemCaption(dm.getMa(), dm.getTen());
							}
						}
					}
					LinkedHashSet<String> value = (LinkedHashSet) valueMap.get(valueKey);

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey,
								new ObjectProperty<LinkedHashSet>(value, LinkedHashSet.class));
						fieldGroup.bind(optionGroup, valueKey);
					}
					optionGroup.setReadOnly(readOnly || ro);

					groupMaster.addComponent(optionGroup);
				}
			} else if (CommonUtils.EKieuDuLieu.USER.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					String userId = (String) valueMap.get(valueKey);
					if (userId != null) {
						User u = identityService.createUserQuery().userId(userId).singleResult();
						if (u != null) {
							Label lbl = new Label(u.getFirstName() + " " + u.getLastName());
							lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
							groupMaster.addComponent(lbl);
						}

					}

				} else {
					ComboBox comboBox = new ComboBox();
					comboBox.setWidth("100%");
					comboBox.setNullSelectionAllowed(true);
					comboBox.setFilteringMode(FilteringMode.CONTAINS);
					comboBox.addStyleName(ExplorerLayout.FORM_CONTROL);

					setRequired(oTruongThongTin, comboBox);
					setCaption(oTruongThongTin, comboBox);

					String userLogedIn = CommonUtils.getUserLogedIn();
					String heThongDangNhap = identityService.getUserInfo(userLogedIn, "HT_DON_VI");

					List<User> userList = identityService.createUserQuery().list();
					if (userList != null && !userList.isEmpty()) {
						for (User u : userList) {
							boolean isUse = true;
							if (CommonUtils.EFilterType.BY_DON_VI_DANG_NHAP.getCode()
									.equals(oTruongThongTin.getFilterType())) {
								String htTmp = identityService.getUserInfo(u.getId(), "HT_DON_VI");
								if (htTmp == null || !htTmp.equals(heThongDangNhap)) {
									isUse = false;
								}
							}
							if (isUse) {
								comboBox.addItem(u.getId());
								comboBox.setItemCaption(u.getId(), u.getFirstName() + " " + u.getLastName());
							}
						}
					}

					String value = (String) valueMap.get(valueKey);

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(comboBox, valueKey);
					}
					comboBox.setReadOnly(readOnly || ro);

					List<UserRelatedField> relatedList = userRelatedFieldMap.get(parentId);
					if (relatedList == null) {
						relatedList = new ArrayList<UserRelatedField>();
						userRelatedFieldMap.put(parentId, relatedList);
					}
					UserRelatedField related = new UserRelatedField();
					related.setComponent(comboBox);
					related.setTruongThongTin(oTruongThongTin.getTruongThongTin());
					related.setConstrantCode(oTruongThongTin.getConstrant());
					relatedList.add(related);

					groupMaster.addComponent(comboBox);
				}
			} else if (CommonUtils.EKieuDuLieu.GROUP.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					String groupId = (String) valueMap.get(valueKey);
					if (groupId != null) {
						Group g = identityService.createGroupQuery().groupId(groupId).singleResult();
						if (g != null) {
							Label lbl = new Label(g.getName());
							lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
							groupMaster.addComponent(lbl);
						}

					}

				} else {
					ComboBox comboBox = new ComboBox();
					comboBox.setWidth("100%");
					comboBox.setNullSelectionAllowed(true);
					comboBox.setFilteringMode(FilteringMode.CONTAINS);
					comboBox.addStyleName(ExplorerLayout.FORM_CONTROL);

					setRequired(oTruongThongTin, comboBox);
					setCaption(oTruongThongTin, comboBox);

					List<Group> groupList = identityService.createGroupQuery().list();
					if (groupList != null && !groupList.isEmpty()) {
						for (Group g : groupList) {
							comboBox.addItem(g.getId());
							comboBox.setItemCaption(g.getId(), g.getName());
						}
					}

					String value = (String) valueMap.get(valueKey);

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(comboBox, valueKey);
					}
					comboBox.setReadOnly(readOnly || ro);

					List<UserRelatedField> relatedList = userRelatedFieldMap.get(parentId);
					if (relatedList == null) {
						relatedList = new ArrayList<UserRelatedField>();
						userRelatedFieldMap.put(parentId, relatedList);
					}
					UserRelatedField related = new UserRelatedField();
					related.setComponent(comboBox);
					related.setTruongThongTin(oTruongThongTin.getTruongThongTin());
					related.setConstrantCode(oTruongThongTin.getConstrant());
					relatedList.add(related);

					groupMaster.addComponent(comboBox);
				}
			} else if (CommonUtils.EKieuDuLieu.XAC_THUC_FILE.getMa() == kieuDuLieu) {
				if (htmlDisplay) {
					CssLayout fileListWrap = new CssLayout();
					fileListWrap.addStyleName(ExplorerLayout.FILE_LIST_HTML);
					List<FileManagement> fileList = new ArrayList<FileManagement>();
					String fileListStr = null;
					if (valueMap.get(valueKey) != null && valueMap.get(valueKey) instanceof String) {
						fileListStr = (String) valueMap.get(valueKey);
					}
					if (fileListStr != null && !fileListStr.isEmpty()) {
						String arr[] = fileListStr.split(",");
						if (arr != null && arr.length > 0) {
							for (int i = 0; i < arr.length; i++) {
								if (arr[i] != null && arr[i].trim().matches("[0-9]+")) {
									Long id = Long.parseLong(arr[i].trim());
									FileManagement f = fileManagementService.findOne(id);
									if (f != null) {
										fileList.add(f);
									}
								}

							}
						}

					}
					for (FileManagement f : fileList) {
						FileVersion fileVersion = fileManagementService.getMaxVersion(f);

						Button fileBtn = new Button(fileVersion.getFileManagement().getTieuDe());
						fileBtn.addStyleName(Reindeer.BUTTON_LINK);
						fileBtn.addStyleName(ExplorerLayout.FILE_LIST_HTML_LINK);
						String filePath = null;
						if (fileVersion.getSignedPath() != null) {
							filePath = uploadPath + fileVersion.getSignedPath();
						} else if (fileVersion.getUploadPath() != null) {
							filePath = uploadPath + fileVersion.getUploadPath();
						}
						if (filePath != null) {
							FileUtils.downloadWithButton(fileBtn, filePath);
						}
						fileListWrap.addComponent(fileBtn);
					}
					groupMaster.addComponent(fileListWrap);
				} else {

					Button btnUpload = new Button("Thêm tệp");
					btnUpload.setId(UploadFileWindow.BTN_UPLOAD_ID);
					btnUpload.setVisible(!readOnly);

					Long fileSize = oTruongThongTin.getTruongThongTin().getFileSize();
					List<String> fileAllows = FileUtils
							.getTypeAlow(oTruongThongTin.getTruongThongTin().getFileExtensionAlow());

					CssLayout fileListWrap = new CssLayout();
					fileListWrap.addStyleName(ExplorerLayout.FILE_LIST);
					TextField hidden = new TextField();
					fileListWrap.setData(hidden);
					UploadFileWindow window = new UploadFileWindow(fileSize, fileAllows, fileListWrap, uploadPath,
							(readOnly || ro));

					btnUpload.addClickListener(window);

					hidden.setVisible(false);

					if (oTruongThongTin.getTruongThongTin().getLoaiHienThi() != null
							&& oTruongThongTin.getTruongThongTin().getLoaiHienThi()
									.intValue() == CommonUtils.ELoaiHienThi.INPUT_WITH_TEXT.getCode()) {
						btnUpload.setCaption(oTruongThongTin.getTruongThongTin().getTen());
					}

					String fileListStr = null;
					if (valueMap.get(valueKey) != null && valueMap.get(valueKey) instanceof String) {
						fileListStr = (String) valueMap.get(valueKey);
					}
					if (!truongTTFileList.contains(valueKey)) {
						truongTTFileList.add(valueKey);
					}

					List<FileManagement> fileList = new ArrayList<FileManagement>();

					if (fileListStr != null && !fileListStr.isEmpty()) {
						String arr[] = fileListStr.split(",");
						if (arr != null && arr.length > 0) {
							for (int i = 0; i < arr.length; i++) {
								if (arr[i] != null && arr[i].trim().matches("[0-9]+")) {
									Long id = Long.parseLong(arr[i].trim());
									FileManagement f = fileManagementService.findOne(id);
									if (f != null) {
										fileList.add(f);
									}
								}

							}
						}

					}
					hidden.setData(fileList);
					for (FileManagement f : fileList) {
						boolean signed = false;
						FileVersion fileVersion = fileManagementService.getMaxVersion(f);
						List<Signature> sigList = signatureService.getSignatureFind(fileVersion);
						if (sigList != null && !sigList.isEmpty()) {
							signed = true;
						}
						fileListWrap.addComponent(
								UploadFileWindow.createFileItem(f.getTieuDe(), f, window, signed, (readOnly || ro)));
					}
					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(fileListStr, String.class));
						fieldGroup.bind(hidden, valueKey);
					}
					btnUpload.setReadOnly(readOnly || ro);

					CssLayout fileUploadWrap = new CssLayout();
					fileUploadWrap.setStyleName(ExplorerLayout.UPLOAD_WRAP);
					fileUploadWrap.addComponent(btnUpload);
					fileUploadWrap.addComponent(fileListWrap);
					fileUploadWrap.addComponent(hidden);

					groupMaster.addComponent(fileUploadWrap);
				}
			} else if (CommonUtils.EKieuDuLieu.FILE.getMa() == kieuDuLieu) {
				if (htmlDisplay) {

					if (valueMap.get(valueKey) != null && valueMap.get(valueKey) instanceof String) {
						CssLayout fileListWrap = new CssLayout();
						fileListWrap.addStyleName(ExplorerLayout.FILE_LIST_HTML);

						String fileListStr = (String) valueMap.get(valueKey);

						if (fileListStr != null && !fileListStr.isEmpty()) {
							String arr[] = fileListStr.split(":");
							if (arr != null && arr.length > 0) {
								for (int i = 0; i < arr.length; i++) {
									Button fileBtn = new Button(arr[i]);
									fileBtn.addStyleName(Reindeer.BUTTON_LINK);
									fileBtn.addStyleName(ExplorerLayout.FILE_LIST_HTML_LINK);
									FileUtils.downloadWithButton(fileBtn, uploadPath + arr[i]);
									fileListWrap.addComponent(fileBtn);
								}
							}
						}
						groupMaster.addComponent(fileListWrap);
					}
				} else {

					Long fileSize = oTruongThongTin.getTruongThongTin().getFileSize();
					String fileExt = oTruongThongTin.getTruongThongTin().getFileExtensionAlow();
					List<String> fileExtList = new ArrayList<String>();
					if (fileExt != null && !fileExt.isEmpty()) {
						String[] arr = fileExt.split(";");
						fileExtList = Arrays.asList(arr);
					}

					UploadReceive receive = new UploadReceive(uploadPath);
					Upload upload = new Upload(null, receive);
					upload.setImmediate(true);
					upload.setVisible(!readOnly);
					upload.setStyleName(ExplorerLayout.BTN_UPLOAD);
					CssLayout fileListWrap = new CssLayout();
					fileListWrap.addStyleName(ExplorerLayout.FILE_LIST);

					TextField hidden = new TextField();
					hidden.setVisible(false);
					upload.setData(hidden);
					String fileListStr = null;
					if (valueMap.get(valueKey) != null && valueMap.get(valueKey) instanceof String) {
						fileListStr = (String) valueMap.get(valueKey);
					}
					if (!truongTTFileList.contains(valueKey)) {
						truongTTFileList.add(valueKey);
					}

					List<String> fileList = new ArrayList<String>();

					if (fileListStr != null && !fileListStr.isEmpty()) {
						String arr[] = fileListStr.split(":");
						if (arr != null && arr.length > 0) {
							for (int i = 0; i < arr.length; i++) {
								fileListWrap.addComponent(
										createFileItem(arr[i], arr[i], fileList, hidden, (readOnly || ro)));
								fileList.add(arr[i]);
							}
						}

					}
					long size = fileSize != null ? fileSize.longValue() : 0;
					UploadInfoWindow uploadInfoWindow = new UploadInfoWindow(upload, fileListWrap, this, receive,
							fileList, size, fileExtList, uploadPath);
					upload.addStartedListener(new StartedListener() {

						@Override
						public void uploadStarted(final StartedEvent event) {
							if (uploadInfoWindow.getParent() == null) {
								UI.getCurrent().addWindow(uploadInfoWindow);
							}
							uploadInfoWindow.setClosable(false);
						}
					});
					if (oTruongThongTin.getTruongThongTin().getLoaiHienThi() != null
							&& oTruongThongTin.getTruongThongTin().getLoaiHienThi()
									.intValue() == CommonUtils.ELoaiHienThi.INPUT_WITH_TEXT.getCode()) {
						upload.setCaption(oTruongThongTin.getTruongThongTin().getTen());
					}
					upload.addFinishedListener(new Upload.FinishedListener() {
						@Override
						public void uploadFinished(final FinishedEvent event) {
							uploadInfoWindow.setClosable(true);
						}
					});
					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(fileListStr, String.class));
						fieldGroup.bind(hidden, valueKey);
					}
					upload.setReadOnly(readOnly || ro);

					CssLayout fileUploadWrap = new CssLayout();
					fileUploadWrap.setStyleName(ExplorerLayout.UPLOAD_WRAP);
					fileUploadWrap.addComponent(upload);
					fileUploadWrap.addComponent(fileListWrap);
					fileUploadWrap.addComponent(hidden);

					groupMaster.addComponent(fileUploadWrap);
				}
			} else if (CommonUtils.EKieuDuLieu.EDITOR.getMa() == kieuDuLieu) {
				if (htmlDisplay) {

					String value = (String) valueMap.get(valueKey);
					Label lbl = new Label(value);
					lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
					lbl.setContentMode(ContentMode.HTML);
					groupMaster.addComponent(lbl);
				} else {
					RichTextArea editor = new RichTextArea();
					editor.setWidth("100%");
					editor.setNullRepresentation("");
					editor.addStyleName(ExplorerLayout.FORM_CONTROL);
					editor.addStyleName(ExplorerLayout.FORM_EDITOR);

					setRequired(oTruongThongTin, editor);
					setCaption(oTruongThongTin, editor);
					setLimitValue(oTruongThongTin, editor);
					setExpression(oTruongThongTin, editor);

					String value = (String) valueMap.get(valueKey);
					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(editor, valueKey);
					}
					editor.setReadOnly(readOnly || ro);
					groupMaster.addComponent(editor);
				}
			} else if (CommonUtils.EKieuDuLieu.DON_VI.getMa() == kieuDuLieu) {
				if (htmlDisplay) {

					String maDV = (String) valueMap.get(valueKey);
					if (maDV != null) {

						List<Long> trangThaiDv = null;
						WorkflowManagement<HtDonVi> dvWM = new WorkflowManagement<HtDonVi>(EChucNang.HT_DON_VI.getMa());
						if (dvWM.getTrangThaiSuDung() != null) {
							trangThaiDv = new ArrayList();
							trangThaiDv.add(new Long(dvWM.getTrangThaiSuDung().getId()));
						}
						List<HtDonVi> htDonViList = htDonViServices.getHtDonViFind(maDV, null, null, trangThaiDv, -1,
								-1);
						if (htDonViList != null && !htDonViList.isEmpty()) {
							HtDonVi dv = htDonViList.get(0);
							Label lbl = new Label(dv.getTen());
							lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
							groupMaster.addComponent(lbl);
						}
					}

				} else {
					ComboBox comboBox = new ComboBox();
					comboBox.setWidth("100%");
					comboBox.setNullSelectionAllowed(true);
					comboBox.setFilteringMode(FilteringMode.CONTAINS);
					comboBox.addStyleName(ExplorerLayout.FORM_CONTROL);

					setRequired(oTruongThongTin, comboBox);
					setCaption(oTruongThongTin, comboBox);

					List<Long> trangThaiDv = null;
					WorkflowManagement<HtDonVi> dvWM = new WorkflowManagement<HtDonVi>(EChucNang.HT_DON_VI.getMa());
					if (dvWM.getTrangThaiSuDung() != null) {
						trangThaiDv = new ArrayList();
						trangThaiDv.add(new Long(dvWM.getTrangThaiSuDung().getId()));
					}
					List<HtDonVi> htDonViList = htDonViServices.getHtDonViFind(null, null, null, trangThaiDv, -1, -1);

					if (htDonViList != null && !htDonViList.isEmpty()) {
						for (HtDonVi dv : htDonViList) {
							comboBox.addItem(dv.getMa());
							comboBox.setItemCaption(dv.getMa(), dv.getTen());
						}
					}

					String value = (String) valueMap.get(valueKey);

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(comboBox, valueKey);
					}
					comboBox.setReadOnly(readOnly || ro);

					List<UserRelatedField> relatedList = userRelatedFieldMap.get(parentId);
					if (relatedList == null) {
						relatedList = new ArrayList<UserRelatedField>();
						userRelatedFieldMap.put(parentId, relatedList);
					}
					UserRelatedField related = new UserRelatedField();
					related.setComponent(comboBox);
					related.setTruongThongTin(oTruongThongTin.getTruongThongTin());
					related.setConstrantCode(oTruongThongTin.getConstrant());
					relatedList.add(related);

					groupMaster.addComponent(comboBox);
				}
			} else if (CommonUtils.EKieuDuLieu.DON_VI_USER.getMa() == kieuDuLieu) {
				if (htmlDisplay) {

					String maDV = (String) valueMap.get(valueKey);
					if (maDV != null) {

						List<Long> trangThaiDv = null;
						WorkflowManagement<HtDonVi> dvWM = new WorkflowManagement<HtDonVi>(EChucNang.HT_DON_VI.getMa());
						if (dvWM.getTrangThaiSuDung() != null) {
							trangThaiDv = new ArrayList();
							trangThaiDv.add(new Long(dvWM.getTrangThaiSuDung().getId()));
						}
						List<HtDonVi> htDonViList = htDonViServices.getHtDonViFind(maDV, null, null, trangThaiDv, -1,
								-1);
						if (htDonViList != null && !htDonViList.isEmpty()) {
							HtDonVi dv = htDonViList.get(0);
							Label lbl = new Label(dv.getTen());
							lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
							groupMaster.addComponent(lbl);
						}
					}

				} else {
					ComboBox comboBox = new ComboBox();
					comboBox.setWidth("100%");
					comboBox.setNullSelectionAllowed(true);
					comboBox.setFilteringMode(FilteringMode.CONTAINS);
					comboBox.addStyleName(ExplorerLayout.FORM_CONTROL);

					setRequired(oTruongThongTin, comboBox);
					setCaption(oTruongThongTin, comboBox);

					List<Long> trangThaiDv = null;
					WorkflowManagement<HtDonVi> dvWM = new WorkflowManagement<HtDonVi>(EChucNang.HT_DON_VI.getMa());
					if (dvWM.getTrangThaiSuDung() != null) {
						trangThaiDv = new ArrayList();
						trangThaiDv.add(new Long(dvWM.getTrangThaiSuDung().getId()));
					}

					String maDonViUser = identityService.getUserInfo(assigneeUserId, "HT_DON_VI");
					if (maDonViUser != null) {
						List<HtDonVi> htDonViList = htDonViServices.getHtDonViFind(maDonViUser, null, null, trangThaiDv,
								-1, -1);

						if (htDonViList != null && !htDonViList.isEmpty()) {
							for (HtDonVi dv : htDonViList) {
								comboBox.addItem(dv.getMa());
								comboBox.setItemCaption(dv.getMa(), dv.getTen());
							}
						}
					}

					String value = (String) valueMap.get(valueKey);

					if (value == null) {
						value = maDonViUser;
					}

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(comboBox, valueKey);
					}
					comboBox.setReadOnly(true);

					List<UserRelatedField> relatedList = userRelatedFieldMap.get(parentId);
					if (relatedList == null) {
						relatedList = new ArrayList<UserRelatedField>();
						userRelatedFieldMap.put(parentId, relatedList);
					}
					UserRelatedField related = new UserRelatedField();
					related.setComponent(comboBox);
					related.setTruongThongTin(oTruongThongTin.getTruongThongTin());
					related.setConstrantCode(oTruongThongTin.getConstrant());
					relatedList.add(related);

					groupMaster.addComponent(comboBox);
				}
			} else if (CommonUtils.EKieuDuLieu.CHUC_VU.getMa() == kieuDuLieu) {
				if (htmlDisplay) {

					String maCV = (String) valueMap.get(valueKey);
					if (maCV != null) {

						List<Long> trangThaiCv = null;
						WorkflowManagement<HtChucVu> cvWM = new WorkflowManagement<HtChucVu>(
								EChucNang.HT_CHUC_VU.getMa());
						if (cvWM.getTrangThaiSuDung() != null) {
							trangThaiCv = new ArrayList();
							trangThaiCv.add(new Long(cvWM.getTrangThaiSuDung().getId()));
						}

						List<HtChucVu> htCvList = htChucVuDao.getHtChucVuFind(maCV, null, trangThaiCv, 0, 1);
						if (htCvList != null && !htCvList.isEmpty()) {
							HtChucVu cv = htCvList.get(0);
							Label lbl = new Label(cv.getTen());
							lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
							groupMaster.addComponent(lbl);
						}
					}

				} else {
					ComboBox comboBox = new ComboBox();
					comboBox.setWidth("100%");
					comboBox.setNullSelectionAllowed(true);
					comboBox.setFilteringMode(FilteringMode.CONTAINS);
					comboBox.addStyleName(ExplorerLayout.FORM_CONTROL);

					setRequired(oTruongThongTin, comboBox);
					setCaption(oTruongThongTin, comboBox);

					List<Long> trangThaiCv = null;
					WorkflowManagement<HtChucVu> cvWM = new WorkflowManagement<HtChucVu>(EChucNang.HT_CHUC_VU.getMa());
					if (cvWM.getTrangThaiSuDung() != null) {
						trangThaiCv = new ArrayList();
						trangThaiCv.add(new Long(cvWM.getTrangThaiSuDung().getId()));
					}

					List<HtChucVu> htCvList = htChucVuDao.getHtChucVuFind(null, null, trangThaiCv, -1, -1);

					if (htCvList != null && !htCvList.isEmpty()) {
						for (HtChucVu cv : htCvList) {
							comboBox.addItem(cv.getMa());
							comboBox.setItemCaption(cv.getMa(), cv.getTen());
						}
					}

					String value = (String) valueMap.get(valueKey);

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(comboBox, valueKey);
					}
					comboBox.setReadOnly(readOnly || ro);

					List<UserRelatedField> relatedList = userRelatedFieldMap.get(parentId);
					if (relatedList == null) {
						relatedList = new ArrayList<UserRelatedField>();
						userRelatedFieldMap.put(parentId, relatedList);
					}
					UserRelatedField related = new UserRelatedField();
					related.setComponent(comboBox);
					related.setTruongThongTin(oTruongThongTin.getTruongThongTin());
					related.setConstrantCode(oTruongThongTin.getConstrant());
					relatedList.add(related);

					groupMaster.addComponent(comboBox);
				}
			} else if (CommonUtils.EKieuDuLieu.COMPLETED_USER.getMa() == kieuDuLieu) {
				if (htmlDisplay) {

					String userId = (String) valueMap.get(valueKey);
					if (userId != null) {
						User u = identityService.createUserQuery().userId(userId).singleResult();
						if (u != null) {
							Label lbl = new Label(u.getFirstName() + " " + u.getLastName());
							lbl.addStyleName(ExplorerLayout.FORM_GEN_LBL);
							groupMaster.addComponent(lbl);
						}

					}

				} else {
					ComboBox comboBox = new ComboBox();
					comboBox.setWidth("100%");
					comboBox.setNullSelectionAllowed(true);
					comboBox.setFilteringMode(FilteringMode.CONTAINS);
					comboBox.addStyleName(ExplorerLayout.FORM_CONTROL);
					comboBox.addStyleName(ExplorerLayout.FORM_COMPLETED_USER_CONTROL);

					setCaption(oTruongThongTin, comboBox);
					String value = (String) valueMap.get(valueKey);
					if (value == null) {
						value = assigneeUserId;
					}
					if (value != null) {
						User user = identityService.createUserQuery().userId(value).singleResult();
						if (user != null) {
							comboBox.addItem(user.getId());
							comboBox.setItemCaption(user.getId(), user.getFirstName() + " " + user.getLastName());
						}
					}

					if (fieldGroup.getField(valueKey) == null) {
						itemValue.addItemProperty(valueKey, new ObjectProperty<String>(value, String.class));
						fieldGroup.bind(comboBox, valueKey);
					}
					comboBox.setReadOnly(true);

					List<UserRelatedField> relatedList = userRelatedFieldMap.get(parentId);
					if (relatedList == null) {
						relatedList = new ArrayList<UserRelatedField>();
						userRelatedFieldMap.put(parentId, relatedList);
					}
					UserRelatedField related = new UserRelatedField();
					related.setComponent(comboBox);
					related.setTruongThongTin(oTruongThongTin.getTruongThongTin());
					related.setConstrantCode(oTruongThongTin.getConstrant());
					relatedList.add(related);

					groupMaster.addComponent(comboBox);
				}
			} else if (CommonUtils.EKieuDuLieu.HELP_TEXT.getMa() == kieuDuLieu) {

				String lblContentStr = "";
				if (oTruongThongTin.getTruongThongTin() != null
						&& oTruongThongTin.getTruongThongTin().getDefaultValue() != null) {
					lblContentStr = oTruongThongTin.getTruongThongTin().getDefaultValue();
				}
				Label lblContent = new Label(lblContentStr);
				lblContent.setContentMode(ContentMode.HTML);
				lblContent.addStyleName(ExplorerLayout.HELP_TEXT_CONTENT);

				String lbl = FontAwesome.INFO.getHtml();
				PopupView popup = new PopupView(lbl, lblContent);
				popup.addStyleName(ExplorerLayout.HELP_TEXT_POPUP);
				popup.setHideOnMouseOut(false);
				popup.setCaptionAsHtml(true);
				groupMaster.addComponent(popup);
			}
		}
	}

	public boolean validateForm() {
		try {
			if (fieldGroup.isValid()) {
				fieldGroup.commit();
				submitDataToMap();
				return true;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return false;
	}

	public void submitDataToMap() {
		Iterator<String> iterator = valueMap.keySet().iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
//			if (itemValue.getItemProperty(key) != null && itemValue.getItemProperty(key).getValue() != null) {
//				Object obj = itemValue.getItemProperty(key).getValue();
//				valueMap.put(key, obj);
//			}
			if (itemValue.getItemProperty(key) != null) {
				Object obj = itemValue.getItemProperty(key).getValue();
				valueMap.put(key, obj);
			}
		}
	}

	public String getFormData() {
		String hoSoGiaTri = null;
		try {
			if (validateForm()) {

				hoSoGiaTri = getHoSoGiaTri();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return hoSoGiaTri;
	}

	public String getFormString() {
		try {
			if (validateForm()) {
				if (valueMap != null) {
					StringBuilder content = new StringBuilder();
					for (Map.Entry<String, Object> entry : valueMap.entrySet()) {

						if (entry.getValue() != null) {
							if (truongTTFileList != null && truongTTFileList.contains(entry.getKey())
									&& entry.getValue() instanceof String) {
								String fileListStr = (String) entry.getValue();
								if (fileListStr != null && !fileListStr.isEmpty()) {
									String arr[] = fileListStr.split(",");
									if (arr != null && arr.length > 0) {
										for (int i = 0; i < arr.length; i++) {
											if (arr[i] != null && arr[i].trim().matches("[0-9]+")) {
												Long id = Long.parseLong(arr[i].trim());
												FileManagement f = fileManagementService.findOne(id);
												if (f != null) {
													FileVersion fileVersion = fileManagementService.getMaxVersion(f);
													FileInputStream input = null;
													String ext = null;
													if (fileVersion != null && fileVersion.getTrangThai() != null
															&& fileVersion.getTrangThai().intValue() == 1
															&& fileVersion.getSignedPath() != null) {
														ext = FileUtils.getFileExtension(fileVersion.getSignedPath());
														File tempFile = new File(
																uploadPath + fileVersion.getSignedPath());
														input = new FileInputStream(tempFile);

													} else if (fileVersion != null
															&& fileVersion.getUploadPath() != null) {
														File tempFile = new File(
																uploadPath + fileVersion.getUploadPath());
														input = new FileInputStream(tempFile);
														ext = FileUtils.getFileExtension(fileVersion.getUploadPath());
													}
													if (input != null && ext != null) {
														String contentFile = SolrUtils.getFileContent(input, ext);
														content.append(contentFile);
													}
												}
											}
										}
									}
								}
							} else {
								content.append(entry.getValue());
							}
						}
					}
					return content.toString();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void themHang(EFormTreeItem treeItem) {
		if (treeItem.getParent() != null) {
			EFormTreeItem childNew = treeItem.clone();
			childNew.setParent(treeItem.getParent());
			List<EFormTreeItem> process = new ArrayList();
			process.add(childNew);
			while (process.size() > 0) {
				EFormTreeItem loop = process.remove(0);
				if (loop.getChildList() != null) {
					if (loop.getE() == null) {
						loop.setChildList(loop.getChildList().subList(0, 1));
					}
					process.addAll(loop.getChildList());
				}
			}
			treeItem.getParent().getChildList().add(childNew);
		}
	}

	public void xoaHang(String removeId, EFormTreeItem treeItem, String removeKey) {

		try {
			if (treeItem.getParent() != null && treeItem.getParent().getChildList() != null) {
				treeItem.getParent().getChildList().remove(treeItem);
			}

			removedKey.add(removeKey);

			Pattern p = Pattern.compile("((.*)STT)([0-9]+)");
			Matcher m = p.matcher(removeId);

			if (removeId != null && m.matches()) {
				String keyStart = m.group(2);
				Integer stt = new Integer(m.group(3));
				if (valueMap != null) {
					Pattern p1 = Pattern.compile("STT([0-9]+)(.*)");
					Map<String, Object> changeMap = new HashMap<String, Object>();
					Map<String, Object> noChangeMap = new HashMap<String, Object>();
					for (Map.Entry<String, Object> entry : valueMap.entrySet()) {
						String key = entry.getKey();
						if (key != null && key.startsWith(keyStart + "STT")) {
							String keyTmp = key.replace(keyStart, "");
							Matcher m1 = p1.matcher(keyTmp);
							if (m1.matches()) {
								Integer sttCurrent = new Integer(m1.group(1));

								if (sttCurrent < stt) {
									changeMap.put(key, entry.getValue());
								} else if (sttCurrent > stt) {
									String newKey = key.replaceAll(keyStart + "STT" + sttCurrent,
											keyStart + "STT" + (sttCurrent - 1) + "");
									changeMap.put(newKey, entry.getValue());
								}
							}
						} else {
							noChangeMap.put(key, entry.getValue());
						}
					}
					valueMap.clear();
					valueMap.putAll(noChangeMap);
					valueMap.putAll(changeMap);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String getHoSoGiaTri() {
		Map<String, EFormHoSo> hoSoMap = new HashMap();
		Map<String, BieuMau> bieuMauMap = new HashMap();
		Map<String, String> maNhomMap = new HashMap();
		Map<String, BmOTruongThongTin> oTruongThongTinMap = new HashMap();

		if (bieuMauTree != null) {
			List<EFormTreeItem> process = new ArrayList();
			process.add(bieuMauTree.clone());
			while (process.size() > 0) {
				EFormTreeItem loop = process.remove(0);
				if (loop.getE() != null) {
					if (loop.getE() instanceof BieuMau) {
						BieuMau bieuMauLoop = (BieuMau) loop.getE();

						String key = bieuMauLoop.getMa();
						if (loop.getParent() != null && loop.getParent().getE() != null
								&& loop.getParent().getE() instanceof BmO) {
							BmO bmO = (BmO) loop.getParent().getE();
							if (bmO != null && bmO.getTen() != null && !bmO.getTen().trim().isEmpty()) {
								key = bmO.getTen().trim();
							}
						}

						maNhomMap.put("BM" + bieuMauLoop.getId(), key);
						bieuMauMap.put("BM" + bieuMauLoop.getId(), bieuMauLoop);
					}
					if (loop.getE() instanceof BmOTruongThongTin) {
						BmOTruongThongTin oTruongThongTinLoop = (BmOTruongThongTin) loop.getE();
						oTruongThongTinMap.put("O" + oTruongThongTinLoop.getId(), oTruongThongTinLoop);
					}
				}
				if (loop.getChildList() != null) {
					if (loop.getE() == null) {
						loop.setChildList(loop.getChildList().subList(0, 1));
					}
					process.addAll(loop.getChildList());
				}
			}
		}

		EFormHoSo rootHoSo = new EFormHoSo();
		rootHoSo.setMaBieuMau(bieuMauCurrent.getMa());
		hoSoMap.put("BM" + bieuMauCurrent.getId(), rootHoSo);
		Pattern p1 = Pattern.compile("^((BM|STT|O)([0-9]+))((BM|STT|O)[0-9]+)+");
		Pattern p2 = Pattern.compile("((BM|STT|O)[0-9]+)+(O([0-9]+))");
		Iterator<String> keyIterator = valueMap.keySet().iterator();

		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			Object valueObject = valueMap.get(key);

			if (valueObject != null) {
				EFormHoSo currentHoSo = null;
				String currentSTT = null;
				String maxThuTuPath = "";
				if (currentHoSo == null) {
					String currentPath = "";

					String process = key;
					Matcher m = p1.matcher(process);
					while (m.matches()) {
						currentPath = currentPath + process.substring(0, m.group(1).length());
						System.err.println("currentPath: " + currentPath);
						maxThuTuPath = currentPath;
						// maxThuTuPath

						Pattern pm = Pattern.compile("(STT([0-9]+))");
						Matcher mm = pm.matcher(maxThuTuPath);
						while (mm.find()) {
							String token = mm.group(1);
							maxThuTuPath = maxThuTuPath.replaceAll(token, "");
						}
						// end maxThuTuPath

						String loai = m.group(2);
						String id = m.group(3);

						if ("BM".equalsIgnoreCase(loai)) {
							BieuMau bieuMau = bieuMauMap.get("BM" + id);
							String maNhom = maNhomMap.get("BM" + id);
							if (maNhom == null || maNhom.isEmpty()) {
								maNhom = bieuMau.getMa();
							}
							if (bieuMau != null) {
								EFormHoSo parentHoSo = currentHoSo;
								currentHoSo = hoSoMap.get(currentPath);
								if (currentHoSo == null && bieuMau != null) {
									currentHoSo = new EFormHoSo();
									if (currentSTT != null) {
										int thuTu = Integer.parseInt(currentSTT);
										currentHoSo.setThuTu(thuTu);
									}

									currentHoSo.setMaBieuMau(maNhom);
									if (parentHoSo == null) {
										parentHoSo = rootHoSo;
									}
									if (parentHoSo.getHoSoList() == null) {
										parentHoSo.setHoSoList(new ArrayList());
									}
									parentHoSo.getHoSoList().add(currentHoSo);
									hoSoMap.put(currentPath, currentHoSo);
								}
							} else {
								currentHoSo = null;
								break;
							}
							currentSTT = null;
						} else if ("STT".equalsIgnoreCase(loai)) {
							currentSTT = id;
						}
						process = process.substring(m.group(1).length());
						m = p1.matcher(process);
					}
				}

				if (currentHoSo != null) {
					Matcher m = p2.matcher(key);
					if (m.matches()) {
						String id = m.group(4);
						BmOTruongThongTin oTruongThongTin = oTruongThongTinMap.get("O" + id);
						if (oTruongThongTin != null && oTruongThongTin.getTruongThongTin() != null) {

							EFormTruongThongTin truongThongTin = new EFormTruongThongTin();
							if (currentSTT != null) {
								int thuTu = Integer.parseInt(currentSTT);
								truongThongTin.setThuTu(thuTu);
							}
							truongThongTin.setMaTruongThongTin(oTruongThongTin.getTruongThongTin().getMa());
							if (valueObject instanceof String) {
								String value = (String) valueObject;
								truongThongTin.setGiaTriChu(value);
							} else if (valueObject instanceof BigInteger) {
								BigInteger value = (BigInteger) valueObject;
								truongThongTin.setGiaTriSo(value.doubleValue());
							} else if (valueObject instanceof BigDecimal) {
								BigDecimal value = (BigDecimal) valueObject;
								truongThongTin.setGiaTriSo(value.doubleValue());
							} else if (valueObject instanceof java.util.Date) {
								java.util.Date value = (java.util.Date) valueObject;
								truongThongTin.setGiaTriNgay(value);
							} else if (valueObject instanceof Long) {
								Long value = (Long) valueObject;
								truongThongTin.setGiaTriSo(value.doubleValue());
							} else if (valueObject instanceof Integer) {
								Integer value = (Integer) valueObject;
								truongThongTin.setGiaTriSo(value.doubleValue());
							} else if (valueObject instanceof Double) {
								Double value = (Double) valueObject;
								truongThongTin.setGiaTriSo(value);
							} else if (valueObject instanceof Boolean) {
								Boolean bool = (Boolean) valueObject;
								truongThongTin.setGiaTriChu(bool ? "true" : "false");
							} else if (valueObject instanceof LinkedHashSet) {
								LinkedHashSet<String> obj = (LinkedHashSet) valueObject;
								truongThongTin.setGiaTriChu(obj.toString());
							}
							if (currentHoSo.getTruongThongTinList() == null) {
								currentHoSo.setTruongThongTinList(new ArrayList());
							}
							currentHoSo.getTruongThongTinList().add(truongThongTin);
						}
					}
				}
			} else {
				System.out.println("value is null");
			}
		}
		try {
			rootHoSo.sort();

			if (hoSoXml != null && !hoSoXml.isEmpty()) {
				EFormHoSo hoSoOld = CommonUtils.getEFormHoSo(hoSoXml);
//        		if(removedKey != null && !removedKey.isEmpty()){
//        			rootHoSo = FormUtils.updateHoSoGiaTri(hoSoOld, rootHoSo, removedKey);
//        		}
				rootHoSo = FormUtils.updateHoSoGiaTri(hoSoOld, rootHoSo, removedKey);
			}

			rootHoSo.setMaBieuMau(null);
			JAXBContext jaxbContext = JAXBContext.newInstance(EFormHoSo.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			jaxbMarshaller.marshal(rootHoSo, outputStream);
			return new String(outputStream.toByteArray(), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void parseHoSoGiaTri() {
		Map<String, BieuMau> bieuMauMaMap = new HashMap();
		Map<String, BmOTruongThongTin> oTruongThongTinMaMap = new HashMap();
		if (valueMap == null) {
			valueMap = new HashMap();
		} else {
			valueMap.clear();
		}
		if (hoSoXml != null) {
			if (bieuMauTree != null) {
				List<EFormTreeItem> process = new ArrayList();
				process.add(bieuMauTree.clone());
				while (process.size() > 0) {
					EFormTreeItem loop = process.remove(0);
					if (loop.getE() != null) {
						if (loop.getE() instanceof BieuMau) {
							// put ma nhom hoac ma bm
							BieuMau bieuMauLoop = (BieuMau) loop.getE();
							String key = bieuMauLoop.getMa();
							if (loop.getParent() != null && loop.getParent().getE() != null
									&& loop.getParent().getE() instanceof BmO) {
								BmO bmO = (BmO) loop.getParent().getE();
								if (bmO != null && bmO.getTen() != null && !bmO.getTen().trim().isEmpty()) {
									key = bmO.getTen().trim();
								}
							}

							bieuMauMaMap.put(key, bieuMauLoop);
							nhomBieuMauMap.put(key, bieuMauLoop);
						}
						if (loop.getE() instanceof BmOTruongThongTin) {
							BmOTruongThongTin oTruongThongTinLoop = (BmOTruongThongTin) loop.getE();

							String key = oTruongThongTinLoop.getBieuMau().getMa();

							if (loop.getParent() != null) {
								EFormTreeItem parent = loop.getParent();
								while (parent != null && !(parent.getE() instanceof BieuMau)) {
									parent = parent.getParent();
								}
								if (parent != null && parent.getParent() != null
										&& parent.getParent().getE() instanceof BmO) {
									BmO bmO = (BmO) parent.getParent().getE();
									if (bmO != null && bmO.getTen() != null && !bmO.getTen().trim().isEmpty()) {
										key = bmO.getTen().trim();
									}
								}
							}

							oTruongThongTinMaMap.put(key + "_" + oTruongThongTinLoop.getTruongThongTin().getMa(),
									oTruongThongTinLoop);
						}
					}
					if (loop.getChildList() != null) {
						if (loop.getE() == null) {
							loop.setChildList(loop.getChildList().subList(0, 1));
						}
						process.addAll(loop.getChildList());
					}
				}
			}
			try {
				JAXBContext jaxbContext = JAXBContext.newInstance(EFormHoSo.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				ByteArrayInputStream inputStream = new ByteArrayInputStream(hoSoXml.getBytes("UTF-8"));
				EFormHoSo rootHoSo = (EFormHoSo) jaxbUnmarshaller.unmarshal(inputStream);
				List<EFormHoSo> process = new ArrayList();

				if (rootHoSo != null) {
					rootHoSo.setMaTemp("BM" + bieuMauCurrent.getId());
					rootHoSo.setMaBieuMau(bieuMauCurrent.getMa());
					process.add(rootHoSo);
				}
				while (process.size() > 0) {
					EFormHoSo hoSoCurrent = process.remove(0);
					if (hoSoCurrent.getHoSoList() != null) {
						for (EFormHoSo child : hoSoCurrent.getHoSoList()) {

							BieuMau childBieuMau = bieuMauMaMap.get(child.getMaBieuMau());
							if (childBieuMau == null) {
								List<BieuMau> bmList = bieuMauService.findByCode(child.getMaBieuMau());
								if (bmList != null && !bmList.isEmpty()) {
									childBieuMau = bmList.get(0);
								}
							}
							if (childBieuMau != null) {
								child.setMaTemp(hoSoCurrent.getMaTemp()
										+ (child.getThuTu() != null ? "STT" + child.getThuTu() : "") + "BM"
										+ childBieuMau.getId());
								process.add(child);
							}
						}
					}
					if (hoSoCurrent.getTruongThongTinList() != null) {
						for (EFormTruongThongTin child : hoSoCurrent.getTruongThongTinList()) {
							BmOTruongThongTin childOTruongThongTin = oTruongThongTinMaMap
									.get(hoSoCurrent.getMaBieuMau() + "_" + child.getMaTruongThongTin());
							if (childOTruongThongTin != null && childOTruongThongTin.getTruongThongTin() != null
									&& childOTruongThongTin.getTruongThongTin().getKieuDuLieu() != null) {
								String maTemp = hoSoCurrent.getMaTemp()
										+ (child.getThuTu() != null ? "STT" + child.getThuTu() : "") + "O"
										+ childOTruongThongTin.getId();
								List<EFormTreeItem> treeItemProcess = new ArrayList();
								treeItemProcess.add(bieuMauTree);
								String currentPath = "";

								while (treeItemProcess.size() > 0 && currentPath.equals(maTemp) == false) {
									EFormTreeItem currentTreeItem = treeItemProcess.remove(0);
									if (currentTreeItem.getE() != null) {
										if (currentTreeItem.getE() instanceof BieuMau) {
											BieuMau bieuMau = (BieuMau) currentTreeItem.getE();
											if (maTemp.startsWith(currentPath + "BM" + bieuMau.getId())) {
												treeItemProcess.clear();
												if (currentTreeItem.getChildList() != null) {
													treeItemProcess.addAll(currentTreeItem.getChildList());
												}
												currentPath = currentPath + "BM" + bieuMau.getId();
											}
										} else if (currentTreeItem.getE() instanceof BmOTruongThongTin) {
											BmOTruongThongTin oTruongThongTin = (BmOTruongThongTin) currentTreeItem
													.getE();
											if (maTemp.startsWith(currentPath + "O" + oTruongThongTin.getId())) {
												currentPath = currentPath + "O" + oTruongThongTin.getId();
											}
										} else {
											if (currentTreeItem.getChildList() != null) {
												treeItemProcess.addAll(currentTreeItem.getChildList());
											}
										}
									} else if (currentTreeItem.getChildList() != null
											&& currentTreeItem.getChildList().size() > 0) {
										String stt = maTemp.substring(currentPath.length());
										Pattern p = Pattern.compile("^(STT([0-9]+))((BM|STT|O)[0-9]+)+");
										Matcher m = p.matcher(stt);
										if (m.matches()) {
											Integer count = Integer.parseInt(m.group(2));
											String processNext = stt.substring(3 + m.group(2).length());
											List<EFormTreeItem> list = currentTreeItem.getChildList();
											if (list != null) {
												EFormTreeItem addItem = list.get(0);
												if (addItem.getE() != null && (addItem.getE() instanceof BieuMau
														|| addItem.getE() instanceof BmOTruongThongTin
														|| addItem.getE() instanceof BmHang)) {
													String nextId = null;
													if (addItem.getE() instanceof BieuMau) {
														nextId = "BM" + ((BieuMau) addItem.getE()).getId();
													} else if (addItem.getE() instanceof BmOTruongThongTin) {
														nextId = "O" + ((BmOTruongThongTin) addItem.getE()).getId();
													} else if (addItem.getE() instanceof BmHang) {
														List<EFormTreeItem> oList = addItem.getChildList();
														for (EFormTreeItem o : oList) {
															if (o.getE() instanceof BmO) {
																List<EFormTreeItem> nextList = o.getChildList();
																for (EFormTreeItem next : nextList) {
																	if (next.getE() instanceof BieuMau) {
																		if (processNext.startsWith("BM"
																				+ ((BieuMau) next.getE()).getId())) {
																			nextId = "BM"
																					+ ((BieuMau) next.getE()).getId();
																		}
																	} else if (next
																			.getE() instanceof BmOTruongThongTin) {
																		if (processNext.startsWith(
																				"O" + ((BmOTruongThongTin) next.getE())
																						.getId())) {
																			nextId = "O"
																					+ ((BmOTruongThongTin) next.getE())
																							.getId();
																		}
																	}
																	if (nextId != null) {
																		break;
																	}
																}
															}
															if (nextId != null) {
																break;
															}
														}
													}
													if (nextId != null && processNext.startsWith(nextId)) {
														while (currentTreeItem.getChildList().size() < count) {
															themHang(addItem);
														}
														int i = 1;
														for (EFormTreeItem next : list) {
															if (maTemp.startsWith(currentPath + "STT" + i)) {
																treeItemProcess.clear();
																if (next.getChildList() != null) {
																	treeItemProcess.addAll(next.getChildList());
																}
																currentPath = currentPath + "STT" + i;
																break;
															}
															i++;
														}
													}
												}
											}
										}
									}
								}

								int kieuDuLieu = childOTruongThongTin.getTruongThongTin().getKieuDuLieu().intValue();

								if (CommonUtils.EKieuDuLieu.TEXT.getMa() == kieuDuLieu) {
//									Object filedVal = getValFormService(maTemp);
//									if (filedVal != null && !(filedVal instanceof JSONArray) && !(filedVal instanceof JSONObject)
//											&& !filedVal.toString().trim().equalsIgnoreCase("")) {
//										valueMap.put(maTemp, filedVal);
//									} else {
//										valueMap.put(maTemp, child.getGiaTriChu());
//									}
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.EMAIL.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.TEXT_AREA.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.NUMBER.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriSo());
								} else if (CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa() == kieuDuLieu) {
									Double d = child.getGiaTriSo();
									if (d != null) {
										valueMap.put(maTemp, d.longValue());
									}

								} else if (CommonUtils.EKieuDuLieu.DATE.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriNgay());
								} else if (CommonUtils.EKieuDuLieu.CHECKBOX.getMa() == kieuDuLieu) {
									if (child.getGiaTriChu() != null) {
										valueMap.put(maTemp, child.getGiaTriChu().equals("true") ? true : false);
									} else {
										valueMap.put(maTemp, false);
									}
								} else if (CommonUtils.EKieuDuLieu.DANH_MUC.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.RADIO_BUTTON.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.LUA_CHON_NHIEU.getMa() == kieuDuLieu) {
									String valueTmp = child.getGiaTriChu();
									if (valueTmp != null && !valueTmp.isEmpty() && valueTmp.trim().length() > 2) {
										valueTmp = valueTmp.trim().substring(1, valueTmp.length() - 1);
										if (!valueTmp.isEmpty()) {
											String[] arr = valueTmp.split(",");
											if (arr != null && arr.length > 0) {
												LinkedHashSet<String> hs = new LinkedHashSet<String>();
												for (String str : arr) {
													hs.add(str.trim());
												}
												valueMap.put(maTemp, hs);
											}
										}
									}
								} else if (CommonUtils.EKieuDuLieu.USER.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.GROUP.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.XAC_THUC_FILE.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.FILE.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.EDITOR.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.DON_VI.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.DON_VI_USER.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.CHUC_VU.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								} else if (CommonUtils.EKieuDuLieu.COMPLETED_USER.getMa() == kieuDuLieu) {
									valueMap.put(maTemp, child.getGiaTriChu());
								}
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String getHtmlData() {
		String html = "";
		try {
			if (bieuMauTree != null) {
				EFormTreeItem current = bieuMauTree.clone();
				if (current != null) {
					html += createBieuMauUI(current, null);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return html;
	}

	public String createBieuMauUI(EFormTreeItem treeItem, String parentId) {
		String layoutMaster = "<table style='width: 100%; table-layout: fixed; border:1px solid #777; border-collapse: collapse;'>";
		Object obj = treeItem.getE();

		if (obj instanceof BieuMau) {
			BieuMau bieuMau = (BieuMau) obj;
			List<EFormTreeItem> childList = treeItem.getChildList();
			int dem = 0;
			for (EFormTreeItem child : childList) {
				if (child != null) {
					if (child.getE() == null) {
						// Hang lap
						int i = 1;
						// Danh sach hang con
						List<EFormTreeItem> childSubList = child.getChildList();
						if (childSubList != null) {
							for (EFormTreeItem childSub : childSubList) {
								if (childSub.getE() instanceof BmHang) {
									String tr = createBmHangUI(childSub, (parentId == null ? "BM" + bieuMau.getId()
											: parentId + "BM" + bieuMau.getId()) + "STT" + i, dem);
									layoutMaster += "<tr>" + tr + "</tr>";
								}
								i++;
								dem++;
							}
						}
					} else if (child.getE() instanceof BmHang) {
						// Hang khong lap
						String tr = createBmHangUI(child,
								parentId == null ? "BM" + bieuMau.getId() : parentId + "BM" + bieuMau.getId(), dem);
						layoutMaster += "<tr>" + tr + "</tr>";
					}
				}
			}
		}
		layoutMaster += "</table>";
		return layoutMaster;
	}

	public String createBmHangUI(EFormTreeItem treeItem, String parentId, int rowNumInc) {
		String layoutMaster = "";
		Object obj = treeItem.getE();
		if (obj == null || obj instanceof BmHang) {
			// Danh sach o
			List<EFormTreeItem> childList = treeItem.getChildList();
			if (childList != null) {
				for (EFormTreeItem child : childList) {
					if (child != null) {
						if (child.getE() instanceof BmO) {
							BmO o = (BmO) child.getE();
							int colSpan = o.getGhepCot() != null ? o.getGhepCot().intValue() : 1;
							int rowSpan = o.getGhepHang() != null ? o.getGhepHang().intValue() : 1;
							String cell = createCellUI(child, parentId);
							layoutMaster += "<td colspan='" + colSpan + "' rowspan='" + rowSpan
									+ "' style='border: 1px solid #777;'>" + cell + "</td>";
						}
					}
				}
			}
		}
		return layoutMaster;
	}

	public String createCellUI(EFormTreeItem treeItem, String parentId) {
		String groupMaster = "";
		Object obj = treeItem.getE();
		if (obj instanceof BmO) {
			BmO bmO = (BmO) obj;
			List<EFormTreeItem> childList = treeItem.getChildList();

			for (EFormTreeItem child : childList) {
				if (child != null) {
					if (child.getE() instanceof BmOTruongThongTin) {
						BmOTruongThongTin oTruongThongTin = (BmOTruongThongTin) child.getE();
						groupMaster += createInputUI(oTruongThongTin, parentId);
					} else if (child.getE() instanceof BieuMau) {
						BieuMau bieuMau = (BieuMau) child.getE();
						GridLayout layoutSub = new GridLayout();
						initBieuMauGrid(layoutSub, bieuMau);
						String subTable = createBieuMauUI(child, parentId);
						groupMaster += subTable;
					}
				}
			}
			if (bmO.getLoaiO() != null && bmO.getLoaiO().intValue() == ELoaiO.TEXT.getCode()) {

				groupMaster += bmO.getTen();
			}
		}
		return groupMaster;
	}

	public String createInputUI(BmOTruongThongTin oTruongThongTin, String parentId) {
		String groupMaster = "";

		String valueKey = parentId + "O" + oTruongThongTin.getId();

		Object objecValue = valueMap.get(valueKey);
		if (oTruongThongTin.getTruongThongTin() != null
				&& oTruongThongTin.getTruongThongTin().getKieuDuLieu() != null) {

			int kieuDuLieu = oTruongThongTin.getTruongThongTin().getKieuDuLieu().intValue();
			if (oTruongThongTin != null && oTruongThongTin.getTruongThongTin() != null
					&& oTruongThongTin.getTruongThongTin().getLoaiHienThi() != null
					&& oTruongThongTin.getTruongThongTin().getLoaiHienThi()
							.intValue() == CommonUtils.ELoaiHienThi.INPUT_WITH_TEXT.getCode()) {
				groupMaster += oTruongThongTin.getTruongThongTin().getTen() + " ";
			}

			if (CommonUtils.EKieuDuLieu.TEXT.getMa() == kieuDuLieu) {
				if (objecValue != null) {
					groupMaster += objecValue;
				}
			} else if (CommonUtils.EKieuDuLieu.EMAIL.getMa() == kieuDuLieu) {
				if (objecValue != null) {
					groupMaster += objecValue;
				}
			} else if (CommonUtils.EKieuDuLieu.TEXT_AREA.getMa() == kieuDuLieu) {
				if (objecValue != null) {
					groupMaster += objecValue;
				}
			} else if (CommonUtils.EKieuDuLieu.NUMBER.getMa() == kieuDuLieu) {
				if (objecValue != null) {
					groupMaster += objecValue;
				}
			} else if (CommonUtils.EKieuDuLieu.SO_NGUYEN.getMa() == kieuDuLieu) {
				if (objecValue != null) {
					groupMaster += objecValue;
				}
			} else if (CommonUtils.EKieuDuLieu.DATE.getMa() == kieuDuLieu) {
				if (objecValue instanceof Date) {
					Date d = (Date) objecValue;
					SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
					groupMaster += df.format(d);
				}
			} else if (CommonUtils.EKieuDuLieu.CHECKBOX.getMa() == kieuDuLieu) {
				if (objecValue instanceof Boolean) {
					Boolean value = (Boolean) objecValue;
					if (value != null && value) {
						groupMaster += "&#9745;";
					} else {
						groupMaster += "&#9744;";
					}

				}
			} else if (CommonUtils.EKieuDuLieu.DANH_MUC.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					String value = (String) objecValue;
					List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(value, null, null, null, null, 0, 1);
					if (danhMucList != null && !danhMucList.isEmpty()) {
						DanhMuc dm = danhMucList.get(0);
						groupMaster += dm.getTen();
					}
				}
			} else if (CommonUtils.EKieuDuLieu.RADIO_BUTTON.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					String value = (String) objecValue;
					List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(value, null, null, null, null, 0, 1);
					if (danhMucList != null && !danhMucList.isEmpty()) {
						DanhMuc dm = danhMucList.get(0);
						groupMaster += dm.getTen();
					}
				}
			} else if (CommonUtils.EKieuDuLieu.LUA_CHON_NHIEU.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					String valueTmp = (String) objecValue;
					if (valueTmp != null && !valueTmp.isEmpty() && valueTmp.trim().length() > 2) {
						valueTmp = valueTmp.trim().substring(1, valueTmp.length() - 1);
						if (!valueTmp.isEmpty()) {
							String[] arr = valueTmp.split(",");
							if (arr != null && arr.length > 0) {
								LinkedHashSet<String> hs = new LinkedHashSet<String>();
								for (String value : arr) {
									if (value != null) {
										value = value.trim();
										List<DanhMuc> danhMucList = bieuMauService.getDanhMucFind(value, null, null,
												null, null, 0, 1);
										if (danhMucList != null && !danhMucList.isEmpty()) {
											DanhMuc dm = danhMucList.get(0);
											groupMaster += dm.getTen() + "; ";
										}
									}

								}
							}
						}
					}
				}
			} else if (CommonUtils.EKieuDuLieu.USER.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					String userId = (String) objecValue;
					User u = identityService.createUserQuery().userId(userId).singleResult();
					if (u != null) {
						groupMaster += u.getFirstName() + " " + u.getLastName();
					}

				}
			} else if (CommonUtils.EKieuDuLieu.GROUP.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					String groupId = (String) objecValue;
					Group g = identityService.createGroupQuery().groupId(groupId).singleResult();
					if (g != null) {
						groupMaster += g.getName();
					}

				}
			} else if (CommonUtils.EKieuDuLieu.XAC_THUC_FILE.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					groupMaster += (String) objecValue;
				}
			} else if (CommonUtils.EKieuDuLieu.FILE.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					groupMaster += (String) objecValue;
				}
			} else if (CommonUtils.EKieuDuLieu.EDITOR.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					groupMaster += (String) objecValue;
				}
			} else if (CommonUtils.EKieuDuLieu.DON_VI.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					String maDonVi = (String) objecValue;
					if (maDonVi != null) {
						List<HtDonVi> donViList = htDonViServices.getHtDonViFind(maDonVi, null, null, null, 0, 1);
						if (donViList != null && !donViList.isEmpty()) {
							groupMaster += donViList.get(0).getTen();
						}
					}

				}
			} else if (CommonUtils.EKieuDuLieu.DON_VI_USER.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					String maDonVi = (String) objecValue;
					if (maDonVi != null) {
						List<HtDonVi> donViList = htDonViServices.getHtDonViFind(maDonVi, null, null, null, 0, 1);
						if (donViList != null && !donViList.isEmpty()) {
							groupMaster += donViList.get(0).getTen();
						}
					}

				}
			} else if (CommonUtils.EKieuDuLieu.CHUC_VU.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					String maChucvu = (String) objecValue;
					if (maChucvu != null) {
						List<HtChucVu> chucVuList = htChucVuDao.getHtChucVuFind(maChucvu, null, null, 0, 1);
						if (chucVuList != null && !chucVuList.isEmpty()) {
							groupMaster += chucVuList.get(0).getTen();
						}
					}

				}
			} else if (CommonUtils.EKieuDuLieu.COMPLETED_USER.getMa() == kieuDuLieu) {
				if (objecValue instanceof String) {
					String userId = (String) objecValue;
					User u = identityService.createUserQuery().userId(userId).singleResult();
					if (u != null) {
						groupMaster += u.getFirstName() + " " + u.getLastName();
					}
				}
			}

		}
		return groupMaster;
	}

	public Map<String, String> getValueMap() {
		Map<String, String> map = new HashMap();
		if (valueMap != null && !valueMap.isEmpty()) {
			String maRoot = "BM" + bieuMauCurrent.getId();

			Pattern p = Pattern.compile("(BM([0-9]+))");
			Pattern p1 = Pattern.compile("(O([0-9]+))");

			for (Map.Entry<String, Object> entry : valueMap.entrySet()) {
				if (entry.getKey() != null && entry.getValue() != null) {

					String key = entry.getKey();

					// TODO chua xu ly cac truong lap
					if (!key.contains("STT")) {
						key = key.replaceAll(maRoot, "");
						Matcher m = p.matcher(key);
						while (m.find()) {
							String maBm = m.group(1);
							Long id = Long.parseLong(m.group(2));
							BieuMau bm = bieuMauService.findOne(id);
							if (bm != null) {
								key = key.replaceAll(maBm, bm.getMa() + "_");
							}
						}

						Matcher m1 = p1.matcher(key);
						while (m1.find()) {
							String ma = m1.group(1);
							Long id = Long.parseLong(m1.group(2));
							BmOTruongThongTin o = bmOTruongThongTinService.findOne(id);
							if (o != null && o.getTruongThongTin() != null) {
								key = key.replaceAll(ma, o.getTruongThongTin().getMa());
							}
						}
						System.out.println(key);

						if (entry.getValue() instanceof String) {
							String value = (String) entry.getValue();
							map.put(key, value);
						}
					}
				}
			}
		}

		return map;
	}

	public Map<String, Object> getFormVariableMap() {
		Map<String, Object> map = new HashMap();
		if (valueMap != null && !valueMap.isEmpty()) {
			String maRoot = "BM" + bieuMauCurrent.getId();

			Pattern p = Pattern.compile("(BM([0-9]+))");
			Pattern p1 = Pattern.compile("(O([0-9]+))");

			for (Map.Entry<String, Object> entry : valueMap.entrySet()) {
				if (entry.getKey() != null && entry.getValue() != null) {

					String key = entry.getKey();

					// TODO chua xu ly cac truong lap
					if (!key.contains("STT")) {
						key = key.replaceAll(maRoot, "");
						Matcher m = p.matcher(key);
						while (m.find()) {
							String maBm = m.group(1);
							Long id = Long.parseLong(m.group(2));
							BieuMau bm = bieuMauService.findOne(id);
							if (bm != null) {
								key = key.replaceAll(maBm, bm.getMa() + "_");
							}
						}

						Matcher m1 = p1.matcher(key);
						while (m1.find()) {
							String ma = m1.group(1);
							Long id = Long.parseLong(m1.group(2));
							BmOTruongThongTin o = bmOTruongThongTinService.findOne(id);
							if (o != null && o.getTruongThongTin() != null) {
								key = key.replaceAll(ma, o.getTruongThongTin().getMa());

								if (o.getTruongThongTin().getKieuDuLieu() != null) {
									int kieuDL = o.getTruongThongTin().getKieuDuLieu().intValue();
									if (kieuDL == CommonUtils.EKieuDuLieu.DON_VI.getMa()
											|| kieuDL == CommonUtils.EKieuDuLieu.DON_VI_USER.getMa()) {
										String dsDonVi = "";
										String nguoiDaiDien = "";
										String lanhDaoDonVi = "";
										Map<String, List<String>> chucVuUserMap = new HashMap();
										if (entry.getValue() != null && entry.getValue() instanceof String) {

											String maDv = (String) entry.getValue();
											HtDonVi donViCurrent = null;
											List<HtDonVi> donViList = htDonViServices.findByCode(maDv);
											if (donViList != null && !donViList.isEmpty()) {
												donViCurrent = donViList.get(0);
												if (donViCurrent != null && donViCurrent.getNguoiDaiDien() != null) {
													nguoiDaiDien = donViCurrent.getNguoiDaiDien();
												}
											}

											List<User> users = identityService.createUserQuery().list();

											if (users != null && !users.isEmpty()) {
												for (User u : users) {
													String maDvTmp = identityService.getUserInfo(u.getId(),
															"HT_DON_VI");
													if (maDvTmp != null && maDvTmp.equals(maDv)) {
														dsDonVi += u.getId() + ",";

														String maCvTmp = identityService.getUserInfo(u.getId(),
																"HT_CHUC_VU");
														if (donViCurrent != null
																&& donViCurrent.getHtChuVuCaoNhat() != null
																&& donViCurrent.getHtChuVuCaoNhat().getMa()
																		.equals(maCvTmp)) {
															lanhDaoDonVi = u.getId();
														}

														if (maCvTmp != null) {
															List<String> userChucVu = chucVuUserMap.get(maCvTmp);
															if (userChucVu == null) {
																userChucVu = new ArrayList();
																chucVuUserMap.put(maCvTmp, userChucVu);
															}
															if (!userChucVu.contains(u.getId())) {
																userChucVu.add(u.getId());
															}
														}
													}

												}
											}
											if (dsDonVi.lastIndexOf(",") > 0) {
												dsDonVi = dsDonVi.substring(0, dsDonVi.lastIndexOf(","));
											}

										}
										map.put(key + "_DS", dsDonVi);
										map.put(key + "_DAI_DIEN", nguoiDaiDien);
										map.put(key + "_LANH_DAO", lanhDaoDonVi);

										if (chucVuUserMap != null && !chucVuUserMap.isEmpty()) {
											for (Map.Entry<String, List<String>> e : chucVuUserMap.entrySet()) {
												if (e.getKey() != null) {
													String userChucVuStr = "";
													List<String> userIds = e.getValue();
													if (userIds != null && userIds.isEmpty()) {
														for (String str : userIds) {
															userChucVuStr += str + ",";
														}
													}
													if (userChucVuStr.lastIndexOf(",") > 0) {
														userChucVuStr = userChucVuStr.substring(0,
																userChucVuStr.lastIndexOf(","));
													}
													map.put(key + "_" + e.getKey().trim(), userChucVuStr);
												}

											}
										}
									} else if (kieuDL == CommonUtils.EKieuDuLieu.DATE.getMa()) {
										if (entry.getValue() != null && entry.getValue() instanceof Date) {
											Date date = (Date) entry.getValue();
											SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
											map.put(key + "_ISO8601", df.format(date));
										}
									}
								}
							}
						}

						if (entry.getValue() != null) {
							map.put(key, entry.getValue());
						}
					}
				}
			}
		}

		return map;
	}

	public CssLayout createFileItem(String fileName, String fileNameStr, List<String> fileList, TextField hiddenTxt,
			boolean readOnly) {
		Map<String, Object> dataMap = new HashMap();
		dataMap.put("FILE_NAME", fileNameStr);
		dataMap.put("FILE_LIST", fileList);
		dataMap.put("HIDDEN_TXT", hiddenTxt);

		CssLayout fileItem = new CssLayout();
		fileItem.setStyleName(ExplorerLayout.FILE_ITEM);

		Label lbl = new Label(fileName);
		lbl.setStyleName(ExplorerLayout.FILE_ITEM_LBL);

		fileItem.addComponent(lbl);

		CssLayout fileButtonWrap = new CssLayout();
		fileButtonWrap.setStyleName(ExplorerLayout.FILE_BTN_WRAP);

		Button btnDownload = new Button();
		btnDownload.setIcon(FontAwesome.ARROW_DOWN);
		btnDownload.setData(dataMap);
		btnDownload.addClickListener(this);
		btnDownload.setId(BTN_DOWNLOAD_ID);
		FileUtils.downloadWithButton(btnDownload, uploadPath + fileNameStr);
		btnDownload.setStyleName(ExplorerLayout.FILE_ITEM_BTN_DOWNLOAD);
		fileButtonWrap.addComponent(btnDownload);

		if (!readOnly) {
			Button btnDel = new Button();
			btnDel.setIcon(FontAwesome.REMOVE);

			btnDel.setData(dataMap);
			btnDel.addClickListener(this);
			btnDel.setId(BTN_REMOVE_ID);
			btnDel.setStyleName(ExplorerLayout.FILE_ITEM_BTN_REMOVE);
			fileButtonWrap.addComponent(btnDel);
		}

		fileItem.addComponent(fileButtonWrap);

		return fileItem;
	}

	public void commitField(FieldGroup fieldGroup) {
		List<Field> fields = new ArrayList(fieldGroup.getFields());
		for (Field f : fields) {
			if (f.isValid()) {
				f.commit();
			}
		}
		submitDataToMap();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		try {
			if (button != null && BTN_REMOVE_ID.equals(button.getId())) {
				Map<String, Object> fileMap = (Map) button.getData();
				button.getParent().getParent().setVisible(false);
				String fileName = (String) fileMap.get("FILE_NAME");
				List<String> fileList = (List<String>) fileMap.get("FILE_LIST");
				String fileNameStr = "";
				if (fileList != null) {
					fileList.remove(fileName);

					for (String str : fileList) {
						fileNameStr += str;
						if (fileList.indexOf(str) != fileList.size() - 1) {
							fileNameStr += ":";
						}
					}
				}
				TextField hiddenTxt = (TextField) fileMap.get("HIDDEN_TXT");
				hiddenTxt.setValue(fileNameStr);

				if (fileName != null && FileUtils.isExist(uploadPath + fileName)) {
					FileUtils.removeFile(uploadPath + fileName);
				}
			} else if (button != null && BTN_DOWNLOAD_ID.equals(button.getId())) {

				Map<String, Object> fileMap = (Map) button.getData();
				String fileName = (String) fileMap.get("FILE_NAME");
				List<String> fileList = (List<String>) fileMap.get("FILE_LIST");
				String fileNameStr = "";
				if (fileList != null) {
					fileList.add(fileName);

					for (String str : fileList) {
						fileNameStr += str;
						if (fileList.indexOf(str) != fileList.size() - 1) {
							fileNameStr += ":";
						}
					}
				}
				TextField hiddenTxt = (TextField) fileMap.get("HIDDEN_TXT");
				hiddenTxt.setValue(fileNameStr);

			} else if (button != null && BTN_ADD_ROW_ID.equals(button.getId())) {
				commitField(fieldGroup);
				if (button.getData() != null && button.getData() instanceof EFormTreeItem) {
					EFormTreeItem treeItem = (EFormTreeItem) button.getData();
					themHang(treeItem);
					refresh();
				}
			} else if (button != null && BTN_REMOVE_ROW_ID.equals(button.getId())) {
				commitField(fieldGroup);
				if (button.getData() != null) {
					Map<String, Object> data = (Map<String, Object>) button.getData();
					EFormTreeItem treeItem = (EFormTreeItem) data.get("removeItem");
					String removeId = (String) data.get("removeId");
					String removeKey = (String) data.get("removeKey");
					xoaHang(removeId, treeItem, removeKey);
					refresh();
				}
			} else if (button != null && BTN_CALC_ID.equals(button.getId())) {
				try {
					if (validateForm()) {
						String hsXml = this.getHoSoGiaTri();
						EFormHoSo eFormHoSo = CommonUtils.getEFormHoSo(hsXml);
						JSONObject rootObject = FormUtils.getHoSoJson(eFormHoSo);
						if (formulaList != null) {
							for (Formula f : formulaList) {
								JSONFormularParser fomularParser = new JSONFormularParser(rootObject);
								if (f.getFormulaPath() != null && !f.getFormulaPath().isEmpty()) {
									fomularParser.update(f.getResultField(), f.getFormula(), f.getFormulaPath());
								} else {
									fomularParser.update(f.getResultField(), f.getFormula());
								}
							}
						}
						FormUtils.updateHoSoFromJson(valueMap, rootObject, bieuMauCurrent,
								"BM" + bieuMauCurrent.getId(), bieuMauService);
						refresh();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package com.eform.ui.form.formular;

public enum ECalcType{
	SUM(1);
	private final int code;
	private ECalcType(int code){
		this.code=code;
	}
	public int getCode() {
		return code;
	}
}

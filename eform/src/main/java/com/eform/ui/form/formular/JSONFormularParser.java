package com.eform.ui.form.formular;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.math3.optim.nonlinear.scalar.gradient.NonLinearConjugateGradientOptimizer.Formula;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.eform.common.utils.NumberUtils;

public class JSONFormularParser {
	JSONObject rootObject = null;
	JSONArray rootArray = null;

	public JSONFormularParser(JSONObject rootObject) {
		this.rootObject = rootObject;
		this.rootArray = null;
	}

	public JSONFormularParser(JSONArray rootArray) {
		this.rootObject = null;
		this.rootArray = rootArray;
	}

	private Object calc(JSONObject obj, String fomularParser) {
		Pattern r = Pattern.compile("\\[([a-zA-Z0-9\\_\\.]+)\\]");
		Matcher m = r.matcher(fomularParser);
		while (m.find()) {
			DescriptiveStatistics stats = new DescriptiveStatistics();
			if (obj != null) {
				String key = fomularParser.substring(m.start() + 1, m.end() - 1);
				List<String> list = new ArrayList();
				while (key.contains(".")) {
					list.add(key.substring(0, key.indexOf(".")));
					key = key.substring(key.indexOf(".") + 1);
				}
				list.add(key);
				calc(stats, obj, list);
			}
//			fomularParser = fomularParser.substring(0, m.start()) + stats.getSum() + fomularParser.substring(m.end());
			
			String doubleStr = new BigDecimal(stats.getSum()).toPlainString();
			fomularParser = fomularParser.substring(0, m.start()) + doubleStr + fomularParser.substring(m.end());
			m = r.matcher(fomularParser);
		}
		
		//special functions
		//readnumber
		if(fomularParser.startsWith("readnumber")){
			String numberValue = fomularParser.replace("readnumber(", "");
			numberValue = numberValue.replace(")", "");
			return NumberUtils.readNum(numberValue);
		}
		return new Calc(fomularParser).eval();
	}

	private void calc(DescriptiveStatistics stats, JSONObject obj, List<String> paths) {
		if (paths != null && paths.size() > 0) {
			if (obj != null) {
				String key = paths.get(0);
				Object subObj = obj.get(key);
				if (subObj instanceof JSONObject) {
					calc(stats, (JSONObject) subObj, paths.subList(1, paths.size()));
				} else if (subObj instanceof JSONArray) {
					calc(stats, (JSONArray) subObj, paths.subList(1, paths.size()));
				} else if (subObj instanceof Integer) {
					stats.addValue(((Integer) subObj).doubleValue());
				} else if (subObj instanceof Double) {
					stats.addValue(((Double) subObj).doubleValue());
				}
			}
		}
	}

	private void calc(DescriptiveStatistics stats, JSONArray array, List<String> paths) {
		if (paths != null && paths.size() > 0) {
			if (array != null) {
				for (int i = 0, n = array.length(); i < n; i++) {
					Object obj = array.get(i);
					if (obj instanceof JSONObject) {
						calc(stats, (JSONObject) obj, paths);
					}
				}
			}
		}
	}

	private void update(JSONObject obj, String key, String fomularParser, List<String> paths) {
		if (paths != null && paths.size() > 0) {
			if (obj != null) {
				Object subObj = obj.get(paths.get(0));
				if (subObj instanceof JSONObject) {
					update((JSONObject) subObj, key, fomularParser, paths.subList(1, paths.size()));
				} else if (subObj instanceof JSONArray) {
					update((JSONArray) subObj, key, fomularParser, paths.subList(1, paths.size()));
				}
			}
		} else {
			obj.put(key, calc(obj, fomularParser));
		}
	}

	private void update(JSONArray array, String key, String fomularParser, List<String> paths) {
		if (array != null) {
			for (int i = 0, n = array.length(); i < n; i++) {
				Object obj = array.get(i);
				if (obj instanceof JSONObject) {
					update((JSONObject) obj, key, fomularParser, paths);
				}
			}
		}
	}

	public Double calc(ECalcType calcType, List<String> paths) {
		if (paths != null) {
			DescriptiveStatistics stats = new DescriptiveStatistics();
			if (rootObject != null) {
				calc(stats, rootObject, paths);
			}
			if (rootArray != null) {
				calc(stats, rootArray, paths);
			}
			if (calcType.getCode() == ECalcType.SUM.getCode()) {
				return stats.getSum();
			}
		}
		return null;
	}

	public Double calc(ECalcType calcType, String... path) {
		if (path != null && path.length > 0) {
			List<String> paths = new ArrayList<String>();
			for (int i = 0, n = path.length; i < n; i++) {
				if (path[i] != null && path[i].trim().isEmpty() == false) {
					paths.add(path[i].trim());
				}
			}
			return calc(calcType, paths);
		}
		return null;
	}

	public void update(String key, String fomularParser, List<String> paths) {
		if (paths != null) {
			if (rootObject != null) {
				update(rootObject, key, fomularParser, paths);
			}
			if (rootArray != null) {
				update(rootArray, key, fomularParser, paths);
			}
		}
	}

	public void update(String key, String fomularParser, String... path) {
		List<String> paths = new ArrayList<String>();
		if (path != null && path.length > 0) {
			for (int i = 0, n = path.length; i < n; i++) {
				if (path[i] != null && path[i].trim().isEmpty() == false) {
					paths.add(path[i].trim());
				}
			}
		}
		update(key, fomularParser, paths);
	}

//	public static void main(String[] args) {
//		try {
//			JSONObject rootObject = new JSONObject(
//					new JSONTokener(new FileInputStream(new File("src/main/resources/data.json"))));
//			JSONFormularParser fomularParserParser = new JSONFormularParser(rootObject);
//			fomularParserParser.update("tongcong", "[soluong]*[dongia]", "loop");
//			fomularParserParser.update("tongcong", "[loop.tongcong]");
//			Double result = fomularParserParser.calc(ECalcType.SUM, "loop", "soluong");
//			if (result != null) {
//				System.out.println(result);
//			} else {
//				System.out.println("Error");
//			}
//			System.out.println(rootObject.toString());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

}

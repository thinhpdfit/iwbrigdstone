package com.eform.activiti;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FormProperty;
import org.activiti.bpmn.model.SubProcess;
import org.activiti.bpmn.model.UserTask;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.eform.activiti.custom.BpmnJsonConverterCustom;
import com.eform.common.utils.FormUtils;
import com.eform.model.entities.BieuMau;
import com.eform.service.BieuMauService;
import com.eform.service.DanhMucService;
import com.eform.service.LoaiDanhMucService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author Tijs Rademakers
 */
@RestController
public class ModelSaveRestResource implements ModelDataJsonConstants {

	protected static final Logger LOGGER = LoggerFactory.getLogger(ModelSaveRestResource.class);

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private BieuMauService bieuMauService;
	@Autowired
	private LoaiDanhMucService loaiDanhMucService;
	@Autowired
	private DanhMucService danhMucService;
	

	@RequestMapping(value = "/model/{modelId}/save", method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.OK)
	public void saveModel(@PathVariable String modelId, @RequestParam MultiValueMap<String, String> values) {
		try {
			boolean hasFormProperties = false;
			String jsonXml = values.getFirst("json_xml");
			JsonNode modelNode = objectMapper.readTree(jsonXml);
			// custom documentation when null
			JsonNode propertiesNode = modelNode.path("properties");
			if (propertiesNode.isMissingNode()) {
				// if "name" node is missing
			} else {				
				if ("".equals(propertiesNode.path("documentation").asText())) {
					((ObjectNode) propertiesNode).put("documentation", propertiesNode.path("name").asText());
				}				
			}
			jsonXml = modelNode.toString();
			values.set("json_xml", jsonXml);
			BpmnModel bpmnModel = new BpmnJsonConverterCustom().convertToBpmnModel(modelNode);
			List<org.activiti.bpmn.model.Process> listProcess = bpmnModel.getProcesses();
			for (org.activiti.bpmn.model.Process process : listProcess) {
				System.out.println(process);
				Collection<FlowElement> elements = process.getFlowElements();
				List<FlowElement> listElements = new ArrayList(elements);
				while(!listElements.isEmpty()){
					FlowElement elementObj = listElements.remove(0);
					if (elementObj instanceof UserTask) {
						UserTask element = (UserTask) elementObj;
						String formKey = element.getFormKey();
						if (formKey != null) {
							String buttonKey = null;
							Pattern p = Pattern.compile("(.+)#(.+)");
							Matcher m = p.matcher(formKey);
							if(m.matches()){
								formKey = m.group(1);
								buttonKey = m.group(2);
							}
							
							hasFormProperties = true;
							List<BieuMau> bieuMauList = bieuMauService.findByCode(formKey);
							if (bieuMauList != null && !bieuMauList.isEmpty()) {
								BieuMau bieuMau = bieuMauList.get(0);
								FormUtils formUtils = new FormUtils(bieuMauService, loaiDanhMucService, danhMucService);
								List<FormProperty> formProperties = formUtils.getAllFormProperty(bieuMau, null);
								if(buttonKey != null){
									List<FormProperty> formProperties1 = formUtils.getButtonProperty(buttonKey);
									formProperties.addAll(formProperties1);
								}
								element.setFormProperties(formProperties);
							}
						}
					}else if (elementObj instanceof org.activiti.bpmn.model.StartEvent) {
						org.activiti.bpmn.model.StartEvent element = (org.activiti.bpmn.model.StartEvent) elementObj;
						String formKey = element.getFormKey();
						if (formKey != null) {
							String buttonKey = null;
							Pattern p = Pattern.compile("(.+)#(.+)");
							Matcher m = p.matcher(formKey);
							if(m.matches()){
								formKey = m.group(1);
								buttonKey = m.group(2);
							}
							
							hasFormProperties = true;
							List<BieuMau> bieuMauList = bieuMauService.findByCode(formKey);
							if (bieuMauList != null && !bieuMauList.isEmpty()) {
								BieuMau bieuMau = bieuMauList.get(0);
								FormUtils formUtils = new FormUtils(bieuMauService, loaiDanhMucService, danhMucService);
								List<FormProperty> formProperties = formUtils.getAllFormProperty(bieuMau, null);
								if(buttonKey != null){
									List<FormProperty> formProperties1 = formUtils.getButtonProperty(buttonKey);
									formProperties.addAll(formProperties1);
								}
								element.setFormProperties(formProperties);
							}
						}
					}else if(elementObj instanceof SubProcess){
						SubProcess element = (SubProcess) elementObj;
						Collection<FlowElement> subProcessElements = element.getFlowElements();
						List<FlowElement> subProcessListElement = new ArrayList(subProcessElements);
						listElements.addAll(subProcessListElement);
					}
				}
			}
			
			if(hasFormProperties){
				ObjectNode modelNodeTmp = new BpmnJsonConverterCustom().convertToJson(bpmnModel);
				String jsonXmlTmp = objectMapper.writeValueAsString(modelNodeTmp);
				values.set("json_xml", jsonXmlTmp);
			}
			Model model = repositoryService.getModel(modelId);

			ObjectNode modelJson = (ObjectNode) objectMapper.readTree(model.getMetaInfo());

			modelJson.put(MODEL_NAME, values.getFirst("name"));
			modelJson.put(MODEL_DESCRIPTION, values.getFirst("description"));
			model.setMetaInfo(modelJson.toString());
			model.setName(values.getFirst("name"));

			repositoryService.saveModel(model);

			repositoryService.addModelEditorSource(model.getId(), values.getFirst("json_xml").getBytes("utf-8"));

			InputStream svgStream = new ByteArrayInputStream(values.getFirst("svg_xml").getBytes("utf-8"));
			TranscoderInput input = new TranscoderInput(svgStream);

			PNGTranscoder transcoder = new PNGTranscoder();
			// Setup output
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			TranscoderOutput output = new TranscoderOutput(outStream);

			// Do the transformation
			transcoder.transcode(input, output);
			final byte[] result = outStream.toByteArray();
			repositoryService.addModelEditorSourceExtra(model.getId(), result);
			outStream.close();

		} catch (Exception e) {
			LOGGER.error("Error saving model", e);
			throw new ActivitiException("Error saving model", e);
		}
	}
}

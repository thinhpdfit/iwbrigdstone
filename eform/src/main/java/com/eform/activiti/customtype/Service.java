package com.eform.activiti.customtype;

import org.activiti.engine.form.AbstractFormType;

public class Service extends AbstractFormType {
	private static final long serialVersionUID = 1L;
	public static final String TYPE_NAME = "service";

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return TYPE_NAME;
	}

	@Override
	public Object convertFormValueToModelValue(String propertyValue) {
		// TODO Auto-generated method stub
		Integer month = Integer.valueOf(propertyValue);
		return month;
	}

	@Override
	public String convertModelValueToFormValue(Object modelValue) {
		// TODO Auto-generated method stub
		if (modelValue == null) {
			return null;
		}
		return modelValue.toString();
	}

}

package com.eform.activiti;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eform.common.EChucNang;
import com.eform.common.utils.CommonUtils;
import com.eform.common.workflow.WorkflowManagement;
import com.eform.dao.BieuMauDao;
import com.eform.dao.NhomQuyTrinhDao;
import com.eform.model.entities.BieuMau;
import com.eform.model.entities.NhomQuyTrinh;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
public class ModelListRestResource {

	@Autowired
	private BieuMauDao bieuMauDao;
	@Autowired
	private NhomQuyTrinhDao nhomQuyTrinhDao;

	@RequestMapping(value = "/{listType}/json", method = RequestMethod.GET, produces = "application/json")
	public ArrayNode getList(@PathVariable String listType, @RequestParam("ma") String ma,
			@RequestParam("ten") String ten) {

		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode list = objectMapper.createArrayNode();
		if (listType != null && listType.equalsIgnoreCase("category")) {

			List<Long> trangThai = null;
			WorkflowManagement<NhomQuyTrinh> bmWM = new WorkflowManagement<>(EChucNang.E_FORM_BIEU_MAU.getMa());
			if (bmWM.getTrangThaiSuDung() != null) {
				trangThai = new ArrayList<Long>();
				trangThai.add(new Long(bmWM.getTrangThaiSuDung().getId()));
			}

			List<NhomQuyTrinh> nhomQuyTrinhList = nhomQuyTrinhDao.getNhomQuyTrinhFind(ma, ten, trangThai, null, -1, -1);
			if (nhomQuyTrinhList != null) {
				for (NhomQuyTrinh bm : nhomQuyTrinhList) {
					ObjectNode node = objectMapper.createObjectNode();
					node.put("ma", bm.getMa());
					node.put("ten", bm.getTen());
					list.add(node);
				}
			}
		}
		if (listType != null && listType.equalsIgnoreCase("form")) {

			List<Long> trangThai = null;
			WorkflowManagement<BieuMau> bmWM = new WorkflowManagement<>(EChucNang.E_FORM_BIEU_MAU.getMa());
			if (bmWM.getTrangThaiSuDung() != null) {
				trangThai = new ArrayList<Long>();
				trangThai.add(new Long(bmWM.getTrangThaiSuDung().getId()));
			}

//			List<BieuMau> bieuMauList = bieuMauDao.getBieuMauFind(ma, ten,
//					new Long(CommonUtils.ELoaiBieuMau.BM_THUONG.getCode()), trangThai, -1, -1);
			List<BieuMau> bieuMauList = bieuMauDao.getBieuMauFind(ma, ten, null, trangThai, -1, -1);
			if (bieuMauList != null) {
				for (BieuMau bm : bieuMauList) {
					ObjectNode node = objectMapper.createObjectNode();
					node.put("ma", bm.getMa());
					node.put("ten", bm.getTen());
					list.add(node);
				}
			}
		}
		return list;
	}

	@RequestMapping(value = "/list/{listType}/json", method = RequestMethod.GET, produces = "application/json")
	public ArrayNode getPropertyTypes(@PathVariable String listType) {
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode list = objectMapper.createArrayNode();
		for (EPropertyTypes type : EPropertyTypes.values()) {
			list.add(type.getName());
		}
		return list;
	}

	public enum EPropertyTypes {
		STRING("string"), LONG("long"), BOOLEAN("boolean"), DATE("date"), TEST("test"), ENUM("enum");
		private final String name;

		private EPropertyTypes(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

	}
}
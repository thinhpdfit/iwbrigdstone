package com.eform.activiti;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import com.eform.common.utils.StringUtils;
import com.eform.security.CustomWebAuthenticationDetails;
import com.eform.security.GoogleAuthProvider;
import com.eform.security.custom.ActivitiUserDetails;

@EnableWebSecurity
public class MultiHttpSecurityConfig {

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) { 
		try {
			auth.authenticationProvider(customAuthenticationProvider());
			auth.authenticationProvider(getGoogleAuthenticationProvider());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Bean
	public AuthenticationProvider customAuthenticationProvider() {
		CustomAuthenticationProvider impl = new CustomAuthenticationProvider();
	    return impl ;
	}
	
	@Bean
	protected AuthenticationProvider getGoogleAuthenticationProvider() {
		return new GoogleAuthProvider();
	}
	


	private static AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails> authenticationDetailsSource() {

		return new AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails>() {

			@Override
			public WebAuthenticationDetails buildDetails(HttpServletRequest request) {
				return new CustomWebAuthenticationDetails(request);
			}

		};
	}
	@Configuration
	public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
	
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable();
			http.headers().frameOptions().sameOrigin();
			http.authorizeRequests()
					.antMatchers("/images/**", 
							"./VAADIN/**", 
							"/VAADIN/**", 
							"/css/**", 
							"/js/**", 
							"/sv/getdata/**",
							"/sv/ttnv/getbyid/*",
							"/mail-approve/**", 
							"/sync/**").permitAll()
					.antMatchers(HttpMethod.POST, "/api/login").permitAll()
					.anyRequest().authenticated().and().formLogin()
					.authenticationDetailsSource(authenticationDetailsSource()).defaultSuccessUrl("/", true)
					.loginPage("/login").permitAll().and().logout().permitAll();
		}
	}

	public class CustomAuthenticationProvider implements AuthenticationProvider {

		@Autowired
		protected IdentityService identityService;

		@Override
		public Authentication authenticate(Authentication authentication) throws AuthenticationException {
			
			String username = authentication.getName();
			String password = (String) authentication.getCredentials();
			if (identityService.checkPassword(username, StringUtils.getSHA512(password)) == false) {
				throw new BadCredentialsException("Tên tài khoản hoặc mật khẩu không chính xác. Vui lòng thử lại!");
			}
			String expiryDateStr = identityService.getUserInfo(username, "EXPIRY_DATE");
			if (expiryDateStr != null && !expiryDateStr.isEmpty() && expiryDateStr.trim().matches("[0-9]+")) {
				Date now = new Date();
				Date expiryDate = new Date(new Long(expiryDateStr));
				if (expiryDate != null && now.after(expiryDate)) {
					throw new BadCredentialsException(
							"Tài khoản đã hết hạn sử dụng hoặc bị khóa. Vui lòng liên hệ với quản trị viên để được trợ giúp!");
				}
			}

			List<User> users = identityService.createUserQuery().userId(username).list();
			List<Group> groups = identityService.createGroupQuery().groupMember(username).list();
			ActivitiUserDetails userDetails = null;
			for (User user : users) {

				userDetails = new ActivitiUserDetails(user, groups);
			}
			Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();

			return new UsernamePasswordAuthenticationToken(userDetails, password, authorities);
		}

		@Override
		public boolean supports(Class<?> arg0) {
			return true;
		}
	}

}
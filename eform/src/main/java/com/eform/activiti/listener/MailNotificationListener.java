package com.eform.activiti.listener;

import java.text.SimpleDateFormat;
import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.identity.User;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.IdentityLink;
import org.apache.commons.mail.HtmlEmail;

import com.eform.common.Constants;
import com.eform.common.SpringApplicationContext;
import com.eform.common.ThamSo;
import com.eform.common.utils.CommonUtils;
import com.eform.model.entities.HtThamSo;
import com.eform.service.HtThamSoService;

public class MailNotificationListener implements TaskListener{

	private HtThamSoService htThamSoService;
	private RepositoryService repositoryService;
	protected IdentityService identityService;

	@Override
	public void notify(DelegateTask delegateTask) {
		try {
			htThamSoService = SpringApplicationContext.getApplicationContext().getBean(HtThamSoService.class);
			repositoryService = SpringApplicationContext.getApplicationContext().getBean(RepositoryService.class);
			identityService =  SpringApplicationContext.getApplicationContext().getBean(IdentityService.class);
			String hostName = "";
			String fromAddress = "";
			String account = "";
			String mailPass = "";
			String mailCharset = "";
			boolean sslOnConnect = true;
			int smtpPort = 0;
			
			HtThamSo hostNameParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_HOST_NAME);
			if(hostNameParam != null){
				hostName = hostNameParam.getGiaTri();
			}
			
			HtThamSo fromAddressParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_FROM_ADDRESS);
			if(fromAddressParam != null){
				fromAddress = fromAddressParam.getGiaTri();
			}
			
			HtThamSo accountParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_ACCOUNT);
			if(accountParam != null){
				account = accountParam.getGiaTri();
			}
			HtThamSo mailPassParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_PASSWORD);
			if(mailPassParam != null){
				mailPass = mailPassParam.getGiaTri();
			}
			
			HtThamSo mailCharsetParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_CHARSET);
			if(mailCharsetParam != null){
				mailCharset = mailCharsetParam.getGiaTri();
			}
			
			HtThamSo sslOnConnectParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_SSL_ON_CONNECT);
			if(sslOnConnectParam != null && "1".equals(sslOnConnectParam.getGiaTri())){
				sslOnConnect = true;
			}
			
			HtThamSo smtpPortParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_SSL_ON_CONNECT);
			if(smtpPortParam != null && smtpPortParam.getGiaTri() != null && smtpPortParam.getGiaTri().matches("[0-9]+")){
				smtpPort = Integer.parseInt(smtpPortParam.getGiaTri());
			}
			
			String subject = "";
			
			HtThamSo subjectParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_NOTIC_SUBJECT);
			if(subjectParam != null && subjectParam.getGiaTri() != null){
				subject = subjectParam.getGiaTri();
			}
			
			String mailTemplate = "";
			
			HtThamSo mailTemplateParam = htThamSoService.getHtThamSoFind(ThamSo.MAIL_NOTIC_TEMPLATE);
			if(mailTemplateParam != null && mailTemplateParam.getGiaTri() != null){
				mailTemplate = mailTemplateParam.getGiaTri();
			}
			
			TaskService taskService = ProcessEngines.getDefaultProcessEngine().getTaskService();
			List<IdentityLink> identityLinks = taskService.getIdentityLinksForTask(delegateTask.getId());

			String taskName = delegateTask.getName();
			
			ProcessDefinition process = repositoryService.createProcessDefinitionQuery().processDefinitionId(delegateTask.getProcessDefinitionId()).singleResult() ;
			String processName = "#Không tên";
			if(process != null && process.getName() != null){
				processName = process.getName();
			}
			
			String createTime = "";
			
			if(delegateTask.getCreateTime() != null){
				SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_LONG);
				createTime = df.format(delegateTask.getCreateTime());
			}
			
			String rootPath = CommonUtils.getRootPath();
			HtThamSo taskDetailUrl = htThamSoService.getHtThamSoFind(ThamSo.TASK_DETAIL_URL);
			if(taskDetailUrl != null && taskDetailUrl.getGiaTri() != null ){
				rootPath+=taskDetailUrl.getGiaTri()+delegateTask.getId();
			}
			
			
			
			
			if(mailTemplate != null){
				mailTemplate = mailTemplate.replaceAll("<<TASK_TIME>>", createTime != null ? createTime : "");
				mailTemplate = mailTemplate.replaceAll("<<TASK_NAME>>",  taskName != null ? taskName : "");
				mailTemplate = mailTemplate.replaceAll("<<PRO_NAME>>", processName != null ? processName : "" );
				mailTemplate = mailTemplate.replaceAll("<<TASK_DETAIL_LINK>>", rootPath != null ? rootPath : "" );
			}
			
			if(subject != null){
				subject = subject.replaceAll("<<TASK_TIME>>", createTime != null ? createTime : "");
				subject = subject.replaceAll("<<TASK_NAME>>",  taskName != null ? taskName : "");
				subject = subject.replaceAll("<<PRO_NAME>>", processName != null ? processName : "" );
				subject = subject.replaceAll("<<TASK_DETAIL_LINK>>", rootPath != null ? rootPath : "" );
			}
			
			for(IdentityLink link : identityLinks) {
				try {
					
					
					if(link.getUserId() != null){
						User u = identityService.createUserQuery().userId(link.getUserId()).singleResult();
						if(u != null && u.getEmail() != null){
							String content = mailTemplate.replaceAll("<<USER_NAME>>", u.getFirstName()+" " +u.getLastName());
							
							HtmlEmail email = new HtmlEmail();
							email.setHostName(hostName);
							email.setSmtpPort(smtpPort);
							email.setSSLOnConnect(sslOnConnect);
							email.setFrom(fromAddress);
							email.setAuthentication(account, mailPass);
							email.setCharset(mailCharset);
							email.setSubject(subject);
							email.setHtmlMsg(content);
							email.addTo(u.getEmail());
							email.send();
						}
						
					}
					if(link.getGroupId() != null){
						List<User> uList = identityService.createUserQuery().memberOfGroup(link.getGroupId()).list();
						if(uList != null){
							for(User u : uList){
								try {
									if(u != null && u.getEmail() != null){
										String content = mailTemplate.replaceAll("<<USER_NAME>>", u.getFirstName()+" " +u.getLastName());
										HtmlEmail email = new HtmlEmail();
										email.setHostName(hostName);
										email.setSmtpPort(smtpPort);
										email.setSSLOnConnect(sslOnConnect);
										email.setFrom(fromAddress);
										email.setAuthentication(account, mailPass);
										email.setCharset(mailCharset);
										email.setSubject(subject);
										email.setHtmlMsg(content);
										email.addTo(u.getEmail());
										email.send();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

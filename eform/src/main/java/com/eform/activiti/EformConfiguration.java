package com.eform.activiti;

import java.io.IOException;
import java.util.Locale;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.servlet.GenericServlet;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.eform", entityManagerFactoryRef = "eformEntityManagerMf", transactionManagerRef = "eformTransactionManager")
public class EformConfiguration {

	private final Logger log = LoggerFactory.getLogger(ActivitiEngineConfiguration.class);

	@Autowired
	protected Environment environment;
	@Autowired
	@Qualifier("eformds")
	DataSource eformds;
//	@Bean(name = "eformds")
//	@ConfigurationProperties(prefix = "spring.secondDatasource")
//	public DataSource secondaryDataSource() {
//		SimpleDriverDataSource ds = new SimpleDriverDataSource();
//
//		try {
//			@SuppressWarnings("unchecked")
//			Class<? extends Driver> driverClass = (Class<? extends Driver>) Class
//					.forName(environment.getProperty("jdbc.driver", "org.postgresql.Driver"));
//			ds.setDriverClass(driverClass);
//
//		} catch (Exception e) {
//			log.error("Error loading driver class", e);
//		}
//
//		ds.setUrl(environment.getProperty("spring.datasource.url"));
//		ds.setUsername(environment.getProperty("spring.datasource.username"));
//		ds.setPassword(environment.getProperty("spring.datasource.password"));
//
//		return ds;
//	}
	
	@PersistenceContext(unitName = "eform")
    @Bean(name = "eformEntityManagerMf")
    public LocalContainerEntityManagerFactoryBean mySqlEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		LocalContainerEntityManagerFactoryBean em =  builder.dataSource(eformds).persistenceUnit("eform").packages("com.eform").build();
		System.out.println("LocalContainerEntityManagerFactoryBean: "+em);
		return em;
     }

	@Bean(name = "eformTransactionManager")
	public JpaTransactionManager db2TransactionManager(@Qualifier("eformEntityManagerMf") final EntityManagerFactory emf) {
	    JpaTransactionManager transactionManager = new JpaTransactionManager();
	    transactionManager.setEntityManagerFactory(emf);
	    return transactionManager;
	}
	
	@Bean
	public LocaleResolver localeResolver() {
	 SessionLocaleResolver slr = new SessionLocaleResolver();
	 slr.setDefaultLocale(new Locale("vi", "vn"));
	 return slr;
	}
	 
	@Bean
	public ResourceBundleMessageSource messageSource() {
	 ResourceBundleMessageSource source = new ResourceBundleMessageSource();
	 source.setBasenames("messages"); 
	 source.setUseCodeAsDefaultMessage(true);
	 return source;
	}

}

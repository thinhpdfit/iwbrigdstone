package com.eform.activiti.servicetask;

import org.json.JSONObject;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class custom extends CustomField<JSONObject> {
	TextField customtxt = new TextField("Service config");

	@Override
	public Class<? extends JSONObject> getType() {
		// TODO Auto-generated method stub
		return JSONObject.class;
	}

	@Override
	protected Component initContent() {
		// TODO Auto-generated method stub

		// customtxt.setSizeFull();
		// customtxt.setNullRepresentation("");
//		customtxt.setVisible(false);
		return customtxt;
	}

	@Override
	public JSONObject getValue() {
		return (customtxt.getValue() != null && !customtxt.getValue().equals("")) ? new JSONObject(customtxt.getValue())
				: null;
	}

	@Override
	public void setValue(JSONObject obj) {
		if (obj != null) {
			super.setInternalValue(obj);
			customtxt.setValue(obj.toString());
		}
	}

	@Override
	protected JSONObject getInternalValue() {
		return (customtxt.getValue() != null && !customtxt.getValue().equals("")) ? new JSONObject(customtxt.getValue())
				: null;
	}

	@Override
	protected void setInternalValue(JSONObject obj) {
		if (obj != null) {
			super.setInternalValue(obj);
			customtxt.setValue(obj.toString());
		}
	}

}

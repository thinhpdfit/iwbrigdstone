package com.eform.activiti.servicetask;

import java.util.Date;
import java.sql.Timestamp;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eform.demo.Dao.ThongTinNhanVienRes;
import com.eform.demo.entites.ThongTinNhanVien;

@RestController
public class ttnv {
	@Autowired
	private ThongTinNhanVienRes thongTinNhanVien;

	@RequestMapping(value = "/sv/ttnv", method = RequestMethod.GET, produces = "application/json")
	public Iterable<ThongTinNhanVien> getData() {

		return thongTinNhanVien.findAll();
	}

	@RequestMapping(value = "/sv/ttnv/setdata", method = RequestMethod.GET)
	public String setData(@RequestParam("hoten") String hoten, @RequestParam("thoigianvao") String thoigianvao,
			@RequestParam("thoigianra") String thoigianra) {
		ThongTinNhanVien ttnv = new ThongTinNhanVien();
		ttnv.setThoigianvao(new Timestamp(new Date().getTime()));
		ttnv.setThoigianra(new Timestamp(new Date().getTime()));
		ttnv.setHoten(hoten);
		thongTinNhanVien.save(ttnv);
		return "";
	}

	@RequestMapping(value = "/sv/ttnv/updatelydo", method = RequestMethod.GET)
	public String setData(@RequestParam("id") Long id, @RequestParam("lydo") String lydo) {
		ThongTinNhanVien ttnv = thongTinNhanVien.findOne(id);
		if (ttnv != null) {
			ttnv.setLydo(lydo);
			thongTinNhanVien.save(ttnv);
			return "true";
		}
		return "false";
	}
	@RequestMapping(value = "/sv/ttnv/getbyid", method = RequestMethod.GET)
	public ThongTinNhanVien getTTNVById(@RequestParam("id") Long id) {
		return id!=null ? thongTinNhanVien.findOne(id):null;
	}
}

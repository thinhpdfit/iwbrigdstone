package com.eform.websocket.message;

import java.util.ArrayList;

public class UserResponse {
	ArrayList<com.eform.model.entities.ChatMessage> chatMessageList=new ArrayList();
	ArrayList<com.eform.model.entities.ChatGroup> groupList=new ArrayList();

	public ArrayList<com.eform.model.entities.ChatMessage> getChatMessageList() {
		return chatMessageList;
	}

	public void setChatMessageList(ArrayList<com.eform.model.entities.ChatMessage> chatMessageList) {
		this.chatMessageList = chatMessageList;
	}

	public ArrayList<com.eform.model.entities.ChatGroup> getGroupList() {
		return groupList;
	}

	public void setGroupList(ArrayList<com.eform.model.entities.ChatGroup> groupList) {
		this.groupList = groupList;
	}
}

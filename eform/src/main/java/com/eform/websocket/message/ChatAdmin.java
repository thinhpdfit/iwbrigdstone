package com.eform.websocket.message;

public class ChatAdmin {
	private String id;
	private String name;
	private String secret;
	private Long lastAction;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public Long getLastAction() {
		return lastAction;
	}

	public void setLastAction(Long lastAction) {
		this.lastAction = lastAction;
	}
}

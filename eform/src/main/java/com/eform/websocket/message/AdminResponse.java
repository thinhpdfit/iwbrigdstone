package com.eform.websocket.message;

import java.util.ArrayList;

import com.eform.model.entities.ChatGroup;
import com.eform.model.entities.ChatMessage;

public class AdminResponse {
	ArrayList<ChatMessage> chatMessageList = new ArrayList();
	ArrayList<ChatGroup> groupList = new ArrayList();
	ArrayList<ChatAdmin> adminList = new ArrayList();
	
	Long peddingGroupCount;

	public ArrayList<ChatMessage> getChatMessageList() {
		return chatMessageList;
	}

	public void setChatMessageList(ArrayList<ChatMessage> chatMessageList) {
		this.chatMessageList = chatMessageList;
	}

	public ArrayList<ChatGroup> getGroupList() {
		return groupList;
	}

	public void setGroupList(ArrayList<ChatGroup> groupList) {
		this.groupList = groupList;
	}

	public ArrayList<ChatAdmin> getAdminList() {
		return adminList;
	}

	public void setAdminList(ArrayList<ChatAdmin> adminList) {
		this.adminList = adminList;
	}

	public Long getPeddingGroupCount() {
		return peddingGroupCount;
	}

	public void setPeddingGroupCount(Long peddingGroupCount) {
		this.peddingGroupCount = peddingGroupCount;
	}

}

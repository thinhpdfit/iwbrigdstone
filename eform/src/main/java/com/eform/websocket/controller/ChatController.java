package com.eform.websocket.controller;

import java.security.MessageDigest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.eform.model.entities.ChatGroup;
import com.eform.model.entities.ChatMessage;
import com.eform.service.ChatGroupService;
import com.eform.service.ChatMessageService;
import com.eform.websocket.message.AdminResponse;
import com.eform.websocket.message.ChatAdmin;
import com.eform.websocket.message.UserResponse;

@Controller
public class ChatController {

	@Autowired
	private ChatMessageService chatMessageService; 
	@Autowired
	private ChatGroupService chatGroupService; 
	
	
	long messageId = 1;
	long groupId = 1;

//	HashMap<String, ArrayList<ChatMessage>> groupHistory = new HashMap();
	HashMap<String, ChatGroup> groupMap = new HashMap();
//	HashMap<String, ChatGroup> userGroupMap = new HashMap();
	List<ChatGroup> penddingGroup = new ArrayList();
	HashMap<String, List<ChatGroup>> adminGroupMap = new HashMap();
	HashMap<String,ChatAdmin> adminMap=new HashMap();
	List<ChatAdmin> adminList=new ArrayList();

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	@MessageMapping("/user/group/{userId}/{clientId}")
	@SendTo("/topic/client/{clientId}")
	public UserResponse userGroupList(@DestinationVariable("userId") String userId,@DestinationVariable("clientId") String clientId) throws Exception {
		UserResponse userResponse = new UserResponse();
//		ChatGroup chatGroup = userGroupMap.get(userId);
//		if (chatGroup == null) {
//			chatGroup = new ChatGroup();
//			chatGroup.setId(groupId);
//			chatGroup.setName("Group " + groupId);
//			chatGroup.setTopic("/topic/group/" + groupId);
//			groupMap.put(String.valueOf(chatGroup.getId()), chatGroup);
//			userGroupMap.put(userId, chatGroup);
//			groupId++;
//		}
//		userResponse.getGroupList().add(chatGroup);
		
		List<com.eform.model.entities.ChatGroup> chatGroupList = chatGroupService.find(userId, null, 0, 1);
		com.eform.model.entities.ChatGroup chatGroup = null;
		if(chatGroupList != null && !chatGroupList.isEmpty()){
			chatGroup = chatGroupList.get(0);
		}
		
		if(chatGroup == null){
			chatGroup = new com.eform.model.entities.ChatGroup();
			chatGroup.setName("Group "+userId);
			chatGroup.setUserId(userId);
			chatGroup.setTopic("/topic/group/" );
			chatGroupService.save(chatGroup);
		}
		
		userResponse.getGroupList().add(chatGroup);
		
		return userResponse;
	}

	@MessageMapping("/user/chatHistory/{userId}/{clientId}/{groupId}")
	@SendTo("/topic/client/{clientId}")
	public UserResponse userChatHistory(@DestinationVariable("userId") String userId,@DestinationVariable("clientId") String clientId,
			@DestinationVariable("groupId") String groupId) throws Exception {
		
//		UserResponse userResponse = new UserResponse();
//		if (groupId != null && groupId.isEmpty() == false) {
//			ArrayList<ChatMessage> chatMessageList = groupHistory.get(groupId);
//			if (chatMessageList != null) {
//				userResponse.getChatMessageList().addAll(chatMessageList);
//			}
//		}
		
		UserResponse userResponse = new UserResponse();
		if (groupId != null && groupId.isEmpty() == false && groupId.matches("[0-9]+")) {
			ChatGroup group = chatGroupService.findOne(new Long(groupId));
			if(group != null){
				List<com.eform.model.entities.ChatMessage> chatMessageList = chatMessageService.find(userId, group, -1, -1);
				if (chatMessageList != null) {
					userResponse.getChatMessageList().addAll(chatMessageList);
				}
			}
			
		}
		
		return userResponse;
	}

	@MessageMapping("/user/chat/{userId}/{clientId}/{groupId}")
	@SendTo("/topic/group/{groupId}")
	public UserResponse userChat(@DestinationVariable("userId") String userId,@DestinationVariable("clientId") String clientId,
			@DestinationVariable("groupId") String groupId, ChatMessage chatMessage) throws Exception {
		UserResponse userResponse = null;
		if (chatMessage != null && chatMessage.getMessage() != null && chatMessage.getHash() != null) {
			String hash = getSHA512(chatMessage.getMessage());
//			if (hash != null && hash.equals(chatMessage.getHash())) {
//			chatMessage.setId(messageId++);
//			chatMessage.setFrom("Guest");
//			chatMessage.setGroupId(groupId);
//			chatMessage.setHash(null);
//			userResponse = new UserResponse();
//			if (groupId != null && groupId.isEmpty() == false) {
//				ArrayList<ChatMessage> chatMessageList = groupHistory.get(groupId);
//				if (chatMessageList == null) {
//					chatMessageList = new ArrayList();
//					groupHistory.put(groupId, chatMessageList);
//					penddingGroup.add(groupMap.get(groupId));
//				}
//				chatMessageList.add(chatMessage);
//				userResponse.getChatMessageList().add(chatMessage);
//				String autoResponse = null;
//				if (chatMessage.getMessage().contains("gia")) {
//					if (chatMessage.getMessage().contains("tra da")) {
//						autoResponse = "Gia tra da la 5k/coc";
//					}
//				}
//				if (autoResponse != null) {
//					ChatMessage autoResponseChatMessage = new ChatMessage();
//					autoResponseChatMessage.setId(messageId++);
//					autoResponseChatMessage.setFrom("Server");
//					autoResponseChatMessage.setGroupId(groupId);
//					autoResponseChatMessage.setMessage(autoResponse);
//					chatMessageList.add(autoResponseChatMessage);
//					userResponse.getChatMessageList().add(autoResponseChatMessage);
//				}
//			}
//		}
		
			List<com.eform.model.entities.ChatGroup> chatGroupList = chatGroupService.find(userId, null, 0, 1);
			com.eform.model.entities.ChatGroup chatGroup = null;
			if(chatGroupList != null && !chatGroupList.isEmpty()){
				chatGroup = chatGroupList.get(0);
			}
			
			if (hash != null && hash.equals(chatMessage.getHash())) {
			chatMessage.setUserId(userId);
			chatMessage.setFrom(userId);
			chatMessage.setGroup(chatGroup);
			chatMessage.setSendDate(new Timestamp(new Date().getTime()));
			chatMessage.setHash(null);
			userResponse = new UserResponse();
			if (chatGroup != null) {
				List<ChatMessage> chatMessageList = chatMessageService.find(null, chatGroup, -1, -1);
				if (chatMessageList == null) {
					chatMessageList = new ArrayList();
					penddingGroup.add(groupMap.get(groupId));
				}
				chatMessageService.save(chatMessage);
				chatMessageList.add(chatMessage);
				userResponse.getChatMessageList().add(chatMessage);
				String autoResponse = null;
				if (chatMessage.getMessage().contains("gia")) {
					if (chatMessage.getMessage().contains("tra da")) {
						autoResponse = "Gia tra da la 5k/coc";
					}
				}
				if (autoResponse != null) {
					ChatMessage autoResponseChatMessage = new ChatMessage();
					autoResponseChatMessage.setId(messageId++);
					autoResponseChatMessage.setFrom("Server");
					autoResponseChatMessage.setGroup(chatGroup);
					autoResponseChatMessage.setMessage(autoResponse);
					chatMessageList.add(autoResponseChatMessage);
					userResponse.getChatMessageList().add(autoResponseChatMessage);
				}
			}
		}
			
		}
		return userResponse;
	}
	
	@MessageMapping("/admin/group/{adminId}/{clientId}")
	@SendTo("/topic/client/{clientId}")
	public AdminResponse adminGroupList(@DestinationVariable("adminId") String adminId,@DestinationVariable("clientId") String clientId) throws Exception {
//		ChatAdmin chatAdmin=adminMap.get(adminId);
//		if(chatAdmin==null){
//			chatAdmin=new ChatAdmin();
//			chatAdmin.setId(Long.parseLong(adminId));
//			chatAdmin.setName(adminId);
//			adminList.add(chatAdmin);
//			adminMap.put(adminId, chatAdmin);
//		}
//		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
//		AdminResponse adminResponse = new AdminResponse();
//		adminResponse.setPeddingGroupCount(new Long(penddingGroup.size()));
//		List<ChatGroup> groupList = adminGroupMap.get(adminId);
//		if (groupList != null) {
//			adminResponse.getGroupList().addAll(groupList);
//		}
		
		ChatAdmin chatAdmin=adminMap.get(adminId);
		if(chatAdmin==null){
			chatAdmin=new ChatAdmin();
			chatAdmin.setId(adminId);
			chatAdmin.setName(adminId);
			adminList.add(chatAdmin);
			adminMap.put(adminId, chatAdmin);
		}
		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
		AdminResponse adminResponse = new AdminResponse();
		adminResponse.setPeddingGroupCount(new Long(penddingGroup.size()));
		List<ChatGroup> groupList = chatGroupService.find(null, null, -1, -1);
		if (groupList != null) {
			adminResponse.getGroupList().addAll(groupList);
		}
		return adminResponse;
	}

	@MessageMapping("/admin/ping/{adminId}/{clientId}")
	@SendTo("/topic/client/{clientId}")
	public AdminResponse adminPing(@DestinationVariable("adminId") String adminId,@DestinationVariable("clientId") String clientId) throws Exception {
		ChatAdmin chatAdmin=adminMap.get(adminId);
		if(chatAdmin==null){
			chatAdmin=new ChatAdmin();
			chatAdmin.setId(adminId);
			chatAdmin.setName(adminId);
			adminList.add(chatAdmin);
			adminMap.put(adminId, chatAdmin);
		}
		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
		AdminResponse adminResponse = new AdminResponse();
		adminResponse.setPeddingGroupCount(new Long(penddingGroup.size()));
		return adminResponse;
	}

	@MessageMapping("/admin/chatHistory/{adminId}/{clientId}/{groupId}")
	@SendTo("/topic/client/{clientId}")
	public AdminResponse adminChatHistory(@DestinationVariable("adminId") String adminId,@DestinationVariable("clientId") String clientId,
			@DestinationVariable("groupId") String groupId) throws Exception {
//		ChatAdmin chatAdmin=adminMap.get(adminId);
//		if(chatAdmin==null){
//			chatAdmin=new ChatAdmin();
//			chatAdmin.setId(Long.parseLong(adminId));
//			chatAdmin.setName(adminId);
//			adminList.add(chatAdmin);
//			adminMap.put(adminId, chatAdmin);
//		}
//		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
//		AdminResponse adminResponse = new AdminResponse();
//		adminResponse.setPeddingGroupCount(new Long(penddingGroup.size()));
//		if (groupId != null && groupId.isEmpty() == false) {
//			ArrayList<ChatMessage> chatMessageList = groupHistory.get(groupId);
//			if (chatMessageList == null) {
//				chatMessageList = new ArrayList();
//				groupHistory.put(groupId, chatMessageList);
//			}
//			adminResponse.getChatMessageList().addAll(chatMessageList);
//		}
		
		ChatAdmin chatAdmin=adminMap.get(adminId);
		if(chatAdmin==null){
			chatAdmin=new ChatAdmin();
			chatAdmin.setId(adminId);
			chatAdmin.setName(adminId);
			adminList.add(chatAdmin);
			adminMap.put(adminId, chatAdmin);
		}
		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
		AdminResponse adminResponse = new AdminResponse();
		adminResponse.setPeddingGroupCount(new Long(penddingGroup.size()));
		if (groupId != null && groupId.isEmpty() == false && groupId.matches("[0-9]+")) {
			ChatGroup group = chatGroupService.findOne(new Long(groupId));
			
			if(group != null){
				List<ChatMessage> chatMessageList = chatMessageService.find(null, group, -1, -1);
				if (chatMessageList == null) {
					chatMessageList = new ArrayList();
					
				}
				adminResponse.getChatMessageList().addAll(chatMessageList);
			}
			
			
		}
		return adminResponse;
	}

	@MessageMapping("/admin/chat/{adminId}/{clientId}/{groupId}")
	@SendTo("/topic/group/{groupId}")
	public AdminResponse adminChat(@DestinationVariable("adminId") String adminId,@DestinationVariable("clientId") String clientId,
			@DestinationVariable("groupId") String groupId, ChatMessage chatMessage) throws Exception {
//		ChatAdmin chatAdmin=adminMap.get(adminId);
//		if(chatAdmin==null){
//			chatAdmin=new ChatAdmin();
//			chatAdmin.setId(Long.parseLong(adminId));
//			chatAdmin.setName(adminId);
//			adminList.add(chatAdmin);
//			adminMap.put(adminId, chatAdmin);
//		}
//		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
//		AdminResponse adminResponse = null;
//		if (chatMessage != null && chatMessage.getMessage() != null && chatMessage.getHash() != null) {
//			String hash = getSHA512(chatMessage.getMessage());
//			if (hash != null && hash.equals(chatMessage.getHash())) {
//				chatMessage.setId(messageId++);
//				chatMessage.setFrom("Admin");
//				chatMessage.setGroupId(groupId);
//				chatMessage.setHash(null);
//				adminResponse = new AdminResponse();
//				adminResponse.setPeddingGroupCount(new Long(penddingGroup.size()));
//				if (groupId != null && groupId.isEmpty() == false) {
//					ArrayList<ChatMessage> chatMessageList = groupHistory.get(groupId);
//					if (chatMessageList == null) {
//						chatMessageList = new ArrayList();
//						groupHistory.put(groupId, chatMessageList);
//					}
//					if (chatMessage != null && chatMessage.getMessage() != null) {
//						chatMessageList.add(chatMessage);
//						adminResponse.getChatMessageList().add(chatMessage);
//					}
//				}
//			}
//		}
		ChatGroup group = null;
		if (groupId != null && groupId.isEmpty() == false && groupId.matches("[0-9]+")) {
			group = chatGroupService.findOne(new Long(groupId));
			
		}
		
		ChatAdmin chatAdmin=adminMap.get(adminId);
		if(chatAdmin==null){
			chatAdmin=new ChatAdmin();
			chatAdmin.setId(adminId);
			chatAdmin.setName(adminId);
			adminList.add(chatAdmin);
			adminMap.put(adminId, chatAdmin);
		}
		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
		AdminResponse adminResponse = null;
		if (chatMessage != null && chatMessage.getMessage() != null && chatMessage.getHash() != null) {
			String hash = getSHA512(chatMessage.getMessage());
			if (hash != null && hash.equals(chatMessage.getHash())) {
				chatMessage.setFrom(adminId);
				chatMessage.setUserId(adminId);
				chatMessage.setSendDate(new Timestamp(new Date().getTime()));
				chatMessage.setGroup(group);
				chatMessage.setHash(null);
				adminResponse = new AdminResponse();
				adminResponse.setPeddingGroupCount(new Long(penddingGroup.size()));
				if (group != null) {
					List<ChatMessage> chatMessageList = chatMessageService.find(null, group, -1, -1);
					if (chatMessageList == null) {
						chatMessageList = new ArrayList();
					}
					if (chatMessage != null && chatMessage.getMessage() != null) {
						chatMessageService.save(chatMessage);
						chatMessageList.add(chatMessage);
						adminResponse.getChatMessageList().add(chatMessage);
					}
				}
			}
		}
		
		
		
		
		
		
		
		return adminResponse;
	}

	@MessageMapping("/admin/assign/{adminId}/{clientId}/{assignedAdminId}/{groupId}")
	@SendTo("/topic/group/{groupId}")
	public AdminResponse adminAssign(@DestinationVariable("adminId") String adminId,@DestinationVariable("clientId") String clientId,@DestinationVariable("assignedAdminId") String assignedAdminId,
			@DestinationVariable("groupId") String groupId) throws Exception {
//		ChatAdmin chatAdmin=adminMap.get(adminId);
//		if(chatAdmin==null){
//			chatAdmin=new ChatAdmin();
//			chatAdmin.setId(Long.parseLong(adminId));
//			chatAdmin.setName(adminId);
//			adminList.add(chatAdmin);
//			adminMap.put(adminId, chatAdmin);
//		}
//		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
//		AdminResponse adminResponse = new AdminResponse();
//		if(adminId!=null&&assignedAdminId!=null&& adminId.equals(assignedAdminId)==false){
//		AdminResponse assignedAdminResponse = new AdminResponse();
//		if (groupId != null && groupId.isEmpty() == false) {
//			ArrayList<ChatMessage> chatMessageList = groupHistory.get(groupId);
//			if (chatMessageList == null) {
//				chatMessageList = new ArrayList();
//				groupHistory.put(groupId, chatMessageList);
//			}
//			ChatMessage chatMessage=new ChatMessage();
//			chatMessage.setId(messageId++);
//			chatMessage.setFrom("Server");
//			chatMessage.setMessage(adminId+" was assigned "+assignedAdminId+" to group.");
//			chatMessage.setGroupId(groupId);
//			chatMessage.setHash(null);
//			if (chatMessage != null && chatMessage.getMessage() != null) {
//				chatMessageList.add(chatMessage);
//				adminResponse.getChatMessageList().add(chatMessage);
//			}
//			ChatGroup chatGroup=groupMap.get(groupId);
//			if(chatGroup!=null){
//				List<ChatGroup> groupList = adminGroupMap.get(assignedAdminId);
//				if (groupList == null) {
//					groupList = new ArrayList();
//					adminGroupMap.put(assignedAdminId, groupList);
//				}
//				groupList.add(chatGroup);
//				assignedAdminResponse.getGroupList().add(chatGroup);
//			}
//		}
//		simpMessagingTemplate.convertAndSend("/topic/admin/"+assignedAdminId,assignedAdminResponse);
//		}
		
		
		ChatGroup group = null;
		if (groupId != null && groupId.isEmpty() == false && groupId.matches("[0-9]+")) {
			group = chatGroupService.findOne(new Long(groupId));
		}
		
		ChatAdmin chatAdmin=adminMap.get(adminId);
		if(chatAdmin==null){
			chatAdmin=new ChatAdmin();
			chatAdmin.setId(adminId);
			chatAdmin.setName(adminId);
			adminList.add(chatAdmin);
			adminMap.put(adminId, chatAdmin);
		}
		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
		AdminResponse adminResponse = new AdminResponse();
		if(adminId!=null&&assignedAdminId!=null&& adminId.equals(assignedAdminId)==false){
		AdminResponse assignedAdminResponse = new AdminResponse();
		if (group != null) {
			List<ChatMessage> chatMessageList = chatMessageService.find(null, group, -1, -1);
			if (chatMessageList == null) {
				chatMessageList = new ArrayList();
				
			}
			ChatMessage chatMessage=new ChatMessage();
			chatMessage.setId(messageId++);
			chatMessage.setFrom("Server");
			chatMessage.setMessage(adminId+" was assigned "+assignedAdminId+" to group.");
			chatMessage.setGroup(group);
			chatMessage.setHash(null);
			if (chatMessage != null && chatMessage.getMessage() != null) {
				chatMessageList.add(chatMessage);
				adminResponse.getChatMessageList().add(chatMessage);
			}
			ChatGroup chatGroup=groupMap.get(groupId);
			if(chatGroup!=null){
				List<ChatGroup> groupList = adminGroupMap.get(assignedAdminId);
				if (groupList == null) {
					groupList = new ArrayList();
					adminGroupMap.put(assignedAdminId, groupList);
				}
				groupList.add(chatGroup);
				assignedAdminResponse.getGroupList().add(chatGroup);
			}
		}
		simpMessagingTemplate.convertAndSend("/topic/admin/"+assignedAdminId,assignedAdminResponse);
		}
		return adminResponse;
	}

	@MessageMapping("/admin/nextGroup/{adminId}/{clientId}")
	@SendTo("/topic/admin/{adminId}")
	public AdminResponse adminNextGroup(@DestinationVariable("adminId") String adminId,@DestinationVariable("clientId") String clientId) throws Exception {
		ChatAdmin chatAdmin=adminMap.get(adminId);
		if(chatAdmin==null){
			chatAdmin=new ChatAdmin();
			chatAdmin.setId(adminId);
			chatAdmin.setName(adminId);
			adminList.add(chatAdmin);
			adminMap.put(adminId, chatAdmin);
		}
		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
		AdminResponse adminResponse = new AdminResponse();
		if (penddingGroup.size() > 0) {
			List<ChatGroup> groupList = adminGroupMap.get(adminId);
			if (groupList == null) {
				groupList = new ArrayList();
				adminGroupMap.put(adminId, groupList);
			}
			ChatGroup nextGroup = penddingGroup.remove(0);
			groupList.add(nextGroup);
			adminResponse.getGroupList().add(nextGroup);
		}
		return adminResponse;
	}

	@MessageMapping("/admin/adminList/{adminId}/{clientId}")
	@SendTo("/topic/client/{clientId}")
	public AdminResponse adminList(@DestinationVariable("adminId") String adminId,@DestinationVariable("clientId") String clientId) throws Exception {
		ChatAdmin chatAdmin=adminMap.get(adminId);
		if(chatAdmin==null){
			chatAdmin=new ChatAdmin();
			chatAdmin.setId(adminId);
			chatAdmin.setName(adminId);
			adminList.add(chatAdmin);
			adminMap.put(adminId, chatAdmin);
		}
		chatAdmin.setLastAction(Calendar.getInstance().getTimeInMillis());
		long pastTime=Calendar.getInstance().getTimeInMillis()-60*10*1000;
		
		
		AdminResponse adminResponse = new AdminResponse();
		adminResponse.setPeddingGroupCount(new Long(penddingGroup.size()));
		for(ChatAdmin tmp:adminList){
			if(tmp.getLastAction()!=null&&tmp.getLastAction().longValue()>pastTime){
				adminResponse.getAdminList().add(tmp);
			}
		}
		return adminResponse;
	}

	public String getSHA512(String valueTohash) {
		String generatedPassword = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			byte[] bytes = md.digest(valueTohash.getBytes("UTF-8"));
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}
}

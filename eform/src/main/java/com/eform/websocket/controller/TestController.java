package com.eform.websocket.controller;


import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.IdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.StringUtils;

import com.eform.common.utils.CommonUtils;
@Controller
public class TestController {
	

	@Autowired
	private IdentityService identityService;
	
	@RequestMapping(path="/",method={RequestMethod.GET})
	public String index(Model model, HttpServletRequest request) {
		return "index";
	}
	
	@RequestMapping(path="/admin",method={RequestMethod.GET})
	public String admin(@RequestParam(name="chatBoxHeight",defaultValue="500") String chatBoxHeight,Model model, HttpServletRequest request) {
		model.addAttribute("chatBoxHeight", chatBoxHeight);
		
		
		String userId = CommonUtils.getUserLogedIn();
		
//		String secret = identityService.getUserInfo(userId, "CHAT_SECRET");
//		if(secret == null || secret.isEmpty()){
//			secret = StringUtils.randomAlphanumeric(15);
//			identityService.setUserInfo(userId, "CHAT_SECRET", secret);
//		}
		
		model.addAttribute("adminId", "'"+userId+"'");
		return "admin";
	} 
	
	@RequestMapping(path="/user",method={RequestMethod.GET})
	public String user(@RequestParam(name="chatBoxHeight",defaultValue="500") String chatBoxHeight,Model model, HttpServletRequest request) {
		model.addAttribute("chatBoxHeight", chatBoxHeight);
		String userId = CommonUtils.getUserLogedIn();

		model.addAttribute("userId", "'"+userId+"'");
		return "user";
	}
}

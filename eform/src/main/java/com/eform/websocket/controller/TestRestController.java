package com.eform.websocket.controller;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.util.StringUtils;

import com.eform.common.utils.CommonUtils;
import com.eform.websocket.message.ChatClient;

@RestController
public class TestRestController {

	@Autowired
	private IdentityService identityService;
	
	
	@RequestMapping("/newClient")
	public ChatClient newClientId(HttpServletRequest request) {
		String userId = CommonUtils.getUserLogedIn();
		
		String secret = identityService.getUserInfo(userId, "CHAT_SECRET");
		if(secret == null || secret.isEmpty()){
			secret = StringUtils.randomAlphanumeric(15);
			identityService.setUserInfo(userId, "CHAT_SECRET", secret);
		}
		Random random=new Random();
		ChatClient chatClient=new ChatClient();
		chatClient.setId(random.nextLong()+"");
		chatClient.setSecret(secret);
		
		return chatClient;
	}
	@RequestMapping(value="/newUser", produces="text/plain")
	public String newUserId(HttpServletRequest request) {
		String userId = CommonUtils.getUserLogedIn();
		return userId;
	}

}

package com.eform.websocket.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.eform.websocket.message.Greeting;
import com.eform.websocket.message.HelloMessage;

@Controller
public class GreetingController {
	public GreetingController() {
		super();
	}

	@MessageMapping("/hello/{id}")
	@SendTo("/topic/greetings/{id}")
	public Greeting greeting(HelloMessage message) throws Exception {
		Thread.sleep(1000); // simulated delay
		if (message.getName().contains("gia")) {
			if (message.getName().contains("tra da")) {
				return new Greeting("5k/coc");
			}
		}
		return new Greeting("Hello, " + message.getName() + "!");
	}

}
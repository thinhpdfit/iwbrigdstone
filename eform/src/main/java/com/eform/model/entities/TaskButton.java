package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.json.JSONObject;

import com.eform.common.type.jpa.JSONObjectUserType;

@Entity
@Table(name = "TASK_BUTTON")
@Cacheable(false)
@TypeDefs({ @TypeDef(name = "CustomJsonObject", typeClass = JSONObjectUserType.class) })
public class TaskButton implements Serializable {

	@Id
	@Column(name = "ID", nullable = false)
	@SequenceGenerator(name = "TASK_BUTTON_SEQ_GEN", sequenceName = "TASK_BUTTON_SEQ", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TASK_BUTTON_SEQ_GEN")
	private Long id;

	@Column(name = "ACTIVITI_ID", length = 100)
	private String activitiId;
	@Column(name = "MODEL_ID", length = 100)
	private String modelId;

	@Column(name = "BUTTONS")
	@Type(type = "CustomJsonObject")
	private JSONObject buttons;
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskButton other = (TaskButton) obj;
		if (activitiId == null) {
			if (other.activitiId != null)
				return false;
		} else if (!activitiId.equals(other.activitiId))
			return false;
		if (modelId == null) {
			if (other.modelId != null)
				return false;
		} else if (!modelId.equals(other.modelId))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActivitiId() {
		return activitiId;
	}

	public void setActivitiId(String activitiId) {
		this.activitiId = activitiId;
	}

	public JSONObject getButtons() {
		return buttons;
	}

	public void setButtons(JSONObject buttons) {
		this.buttons = buttons;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

}

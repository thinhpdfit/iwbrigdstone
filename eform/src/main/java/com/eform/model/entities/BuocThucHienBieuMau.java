
package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BUOC_THUC_HIEN_BIEU_MAU")
@Cacheable(false)
@SequenceGenerator(name = "BUOC_THUC_HIEN_BIEU_MAU_SEQ_GEN", sequenceName = "BUOC_THUC_HIEN_BIEU_MAU_SEQ",allocationSize = 1, initialValue = 1)
public class BuocThucHienBieuMau
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BUOC_THUC_HIEN_BIEU_MAU_SEQ_GEN")
    private Long id;
    @Column(name = "KET_QUA")
    private Long ketQua;
    @Column(name = "CHINH_SUA")
    private Long chinhSua;
    @Column(name = "THU_TU")
    private Long thuTu;
    @ManyToOne
    @JoinColumn(name = "QUY_TRINH_ID")
    private QuyTrinh quyTrinh;
    @ManyToOne
    @JoinColumn(name = "BIEU_MAU_ID")
    private BieuMau bieuMau;
    @ManyToOne
    @JoinColumn(name = "BUOC_THUC_HIEN_ID")
    private BuocThucHien buocThucHien;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof BuocThucHienBieuMau)) {
            areEqual = true;
            final BuocThucHienBieuMau otherBuocThucHienBieuMau = ((BuocThucHienBieuMau) other);
            if ((this.id == null)||(otherBuocThucHienBieuMau.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBuocThucHienBieuMau.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        BuocThucHienBieuMau cloneBuocThucHienBieuMau = new BuocThucHienBieuMau();
        cloneBuocThucHienBieuMau.setId(getId());
        cloneBuocThucHienBieuMau.setKetQua(getKetQua());
        cloneBuocThucHienBieuMau.setChinhSua(getChinhSua());
        cloneBuocThucHienBieuMau.setThuTu(getThuTu());
        return cloneBuocThucHienBieuMau;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getKetQua() {
        return ketQua;
    }

    public void setKetQua(Long ketQua) {
        this.ketQua = ketQua;
    }

    public Long getChinhSua() {
        return chinhSua;
    }

    public void setChinhSua(Long chinhSua) {
        this.chinhSua = chinhSua;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public QuyTrinh getQuyTrinh() {
        return quyTrinh;
    }

    public void setQuyTrinh(QuyTrinh quyTrinh) {
        this.quyTrinh = quyTrinh;
    }

    public BieuMau getBieuMau() {
        return bieuMau;
    }

    public void setBieuMau(BieuMau bieuMau) {
        this.bieuMau = bieuMau;
    }

    public BuocThucHien getBuocThucHien() {
        return buocThucHien;
    }

    public void setBuocThucHien(BuocThucHien buocThucHien) {
        this.buocThucHien = buocThucHien;
    }

}

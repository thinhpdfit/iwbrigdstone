
package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BUOC_THUC_HIEN")
@Cacheable(false)
@SequenceGenerator(name = "BUOC_THUC_HIEN_SEQ_GEN", sequenceName = "BUOC_THUC_HIEN_SEQ",allocationSize = 1, initialValue = 1)
public class BuocThucHien
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BUOC_THUC_HIEN_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "TEN_TRANG_THAI", length = 2000)
    private String tenTrangThai;
    @Column(name = "TEN_PHE_DUYET", length = 2000)
    private String tenPheDuyet;
    @Column(name = "TEN_TU_CHOI", length = 2000)
    private String tenTuChoi;
    @Column(name = "TRICH_YEU")
    private String trichYeu;
    @Column(name = "NOI_DUNG", length = 2000)
    private String noiDung;
    @Column(name = "FRONT_END")
    private Long frontEnd;
    @Column(name = "THU_TU")
    private Long thuTu;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @ManyToOne
    @JoinColumn(name = "QUY_TRINH_ID")
    private QuyTrinh quyTrinh;
    @ManyToOne
    @JoinColumn(name = "PHE_DUYET_ID")
    private BuocThucHien pheDuyet;
    @ManyToOne
    @JoinColumn(name = "TU_CHOI_ID")
    private BuocThucHien tuChoi;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof BuocThucHien)) {
            areEqual = true;
            final BuocThucHien otherBuocThucHien = ((BuocThucHien) other);
            if ((this.id == null)||(otherBuocThucHien.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBuocThucHien.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        BuocThucHien cloneBuocThucHien = new BuocThucHien();
        cloneBuocThucHien.setId(getId());
        cloneBuocThucHien.setMa(getMa());
        cloneBuocThucHien.setTen(getTen());
        cloneBuocThucHien.setTenTrangThai(getTenTrangThai());
        cloneBuocThucHien.setTenPheDuyet(getTenPheDuyet());
        cloneBuocThucHien.setTenTuChoi(getTenTuChoi());
        cloneBuocThucHien.setTrichYeu(getTrichYeu());
        cloneBuocThucHien.setNoiDung(getNoiDung());
        cloneBuocThucHien.setFrontEnd(getFrontEnd());
        cloneBuocThucHien.setThuTu(getThuTu());
        cloneBuocThucHien.setTrangThai(getTrangThai());
        return cloneBuocThucHien;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenTrangThai() {
        return tenTrangThai;
    }

    public void setTenTrangThai(String tenTrangThai) {
        this.tenTrangThai = tenTrangThai;
    }

    public String getTenPheDuyet() {
        return tenPheDuyet;
    }

    public void setTenPheDuyet(String tenPheDuyet) {
        this.tenPheDuyet = tenPheDuyet;
    }

    public String getTenTuChoi() {
        return tenTuChoi;
    }

    public void setTenTuChoi(String tenTuChoi) {
        this.tenTuChoi = tenTuChoi;
    }

    public String getTrichYeu() {
        return trichYeu;
    }

    public void setTrichYeu(String trichYeu) {
        this.trichYeu = trichYeu;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public Long getFrontEnd() {
        return frontEnd;
    }

    public void setFrontEnd(Long frontEnd) {
        this.frontEnd = frontEnd;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

    public QuyTrinh getQuyTrinh() {
        return quyTrinh;
    }

    public void setQuyTrinh(QuyTrinh quyTrinh) {
        this.quyTrinh = quyTrinh;
    }

    public BuocThucHien getPheDuyet() {
        return pheDuyet;
    }

    public void setPheDuyet(BuocThucHien pheDuyet) {
        this.pheDuyet = pheDuyet;
    }

    public BuocThucHien getTuChoi() {
        return tuChoi;
    }

    public void setTuChoi(BuocThucHien tuChoi) {
        this.tuChoi = tuChoi;
    }

}

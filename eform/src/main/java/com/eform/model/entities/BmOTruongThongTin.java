
package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BM_O_TRUONG_THONG_TIN")
@Cacheable(false)
public class BmOTruongThongTin
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @SequenceGenerator(name = "BM_O_TRUONG_THONG_TIN_SEQ_GEN", sequenceName = "BM_O_TRUONG_THONG_TIN_SEQ", initialValue = 1, allocationSize =1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BM_O_TRUONG_THONG_TIN_SEQ_GEN")
    private Long id;
    @Column(name = "THU_TU")
    private Long thuTu;
    @ManyToOne
    @JoinColumn(name = "BIEU_MAU_ID")
    private BieuMau bieuMau;
    @ManyToOne
    @JoinColumn(name = "BM_HANG_ID")
    private BmHang bmHang;
    @ManyToOne
    @JoinColumn(name = "BM_O_ID")
    private BmO bmO;
    @ManyToOne
    @JoinColumn(name = "TRUONG_THONG_TIN_ID")
    private TruongThongTin truongThongTin;

    @Column(name = "READONLY")
    private Long readOnly;
    @Column(name = "constrant")
    private String constrant;
    @Column(name = "filter_type")
    private String filterType;
    
    
    /* 
     * default
ex:{
	list:{
		item1:value1,
		item2:value1,
		item3:value1,
	}
}

service_config
{
	url:http://api.com/getdata,
	type:get|post,
	parammap:{
		service_param:form_param,
	},
	keyselect:ex,list,item2
}
     * 
     * */
    

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof BmOTruongThongTin)) {
            areEqual = true;
            final BmOTruongThongTin otherBmOTruongThongTin = ((BmOTruongThongTin) other);
            if ((this.id == null)||(otherBmOTruongThongTin.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBmOTruongThongTin.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        BmOTruongThongTin cloneBmOTruongThongTin = new BmOTruongThongTin();
        cloneBmOTruongThongTin.setId(getId());
        cloneBmOTruongThongTin.setThuTu(getThuTu());
        return cloneBmOTruongThongTin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public BieuMau getBieuMau() {
        return bieuMau;
    }

    public void setBieuMau(BieuMau bieuMau) {
        this.bieuMau = bieuMau;
    }

    public BmHang getBmHang() {
        return bmHang;
    }

    public void setBmHang(BmHang bmHang) {
        this.bmHang = bmHang;
    }

    public BmO getBmO() {
        return bmO;
    }

    public void setBmO(BmO bmO) {
        this.bmO = bmO;
    }

    public TruongThongTin getTruongThongTin() {
        return truongThongTin;
    }

    public void setTruongThongTin(TruongThongTin truongThongTin) {
        this.truongThongTin = truongThongTin;
    }

	public Long getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(Long readOnly) {
		this.readOnly = readOnly;
	}

	public String getConstrant() {
		return constrant;
	}

	public void setConstrant(String constrant) {
		this.constrant = constrant;
	}

	public String getFilterType() {
		return filterType;
	}

	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}
    
    

}

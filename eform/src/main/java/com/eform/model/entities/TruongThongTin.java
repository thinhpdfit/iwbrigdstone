
package com.eform.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TRUONG_THONG_TIN")
@Cacheable(false)
@SequenceGenerator(name = "TRUONG_THONG_TIN_SEQ_GEN", sequenceName = "TRUONG_THONG_TIN_SEQ", allocationSize = 1, initialValue = 1)
public class TruongThongTin
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRUONG_THONG_TIN_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @Column(name = "KIEU_DU_LIEU")
    private Long kieuDuLieu;
    @Column(name = "LOAI_HIEN_THI")
    private Long loaiHienThi;
    @Column(name = "INLINE_STYLE", length = 2000)
    private String inlineStyle;
    @Column(name = "REQUIRED")
    private Long required;
    @Column(name = "EXPRESSIONS", length = 2000)
    private String expressions;
    @Column(name = "EXPRESSION_MESSAGE", length = 2000)
    private String expressionMessage;
    @Column(name = "MINLENGTH")
    private Long minlength;
    @Column(name = "MAXLENGTH")
    private Long maxlength;
    @Column(name = "MINVALUE")
    private BigDecimal minvalue;
    @Column(name = "MAXVALUE")
    private BigDecimal maxvalue;
    @ManyToOne
    @JoinColumn(name = "LOAI_DANH_MUC_ID")
    private LoaiDanhMuc loaiDanhMuc;
    @Column(name = "FILE_SIZE")
    private Long fileSize;
    @Column(name = "FILE_EXTENSION_ALOW", length = 2000)
    private String fileExtensionAlow;
    

    @Column(name = "min_duration", length = 2000)
    private String minDuration;
    @Column(name = "max_duration", length = 2000)
    private String maxDuration;
    @Column(name = "min_duration_type")
    private Long minDurationType;
    @Column(name = "max_duration_type")
    private Long maxDurationType;
    @Column(name = "default_value")
    private String defaultValue;
    @Column(name = "date_default_type")
    private Long dateDefaultType;
    @Column(name = "time_detail")
    private Long timeDetail;
    @Column(name = "table_display")
    private Long tableDisplay;
    @Column(name = "search_form_display")
    private Long searchFormDisplay;
    @Column(name = "table_display_order")
    private Long tableDisplayOrder;
    @Column(name = "search_form_display_order")
    private Long searchFormDisplayOrder;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof TruongThongTin)) {
            areEqual = true;
            final TruongThongTin otherTruongThongTin = ((TruongThongTin) other);
            if ((this.id == null)||(otherTruongThongTin.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherTruongThongTin.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        TruongThongTin cloneTruongThongTin = new TruongThongTin();
        cloneTruongThongTin.setId(getId());
        cloneTruongThongTin.setMa(getMa());
        cloneTruongThongTin.setTen(getTen());
        cloneTruongThongTin.setTrangThai(getTrangThai());
        cloneTruongThongTin.setKieuDuLieu(getKieuDuLieu());
        cloneTruongThongTin.setLoaiHienThi(getLoaiHienThi());
        cloneTruongThongTin.setInlineStyle(getInlineStyle());
        cloneTruongThongTin.setRequired(getRequired());
        cloneTruongThongTin.setExpressions(getExpressions());
        cloneTruongThongTin.setMinlength(getMinlength());
        cloneTruongThongTin.setMaxlength(getMaxlength());
        cloneTruongThongTin.setMinvalue(getMinvalue());
        cloneTruongThongTin.setMaxvalue(getMaxvalue());
        cloneTruongThongTin.setFileExtensionAlow(getFileExtensionAlow());
        cloneTruongThongTin.setFileSize(getFileSize());
        cloneTruongThongTin.setDefaultValue(getDefaultValue());
        cloneTruongThongTin.setMinDuration(getMinDuration());
        cloneTruongThongTin.setMaxDuration(getMaxDuration());
        cloneTruongThongTin.setMinDurationType(getMinDurationType());
        cloneTruongThongTin.setMaxDuration(getMaxDuration());
        cloneTruongThongTin.setDateDefaultType(getDateDefaultType());
        cloneTruongThongTin.setTableDisplay(getTableDisplay());
        cloneTruongThongTin.setTableDisplayOrder(getTableDisplayOrder());
        cloneTruongThongTin.setSearchFormDisplay(getSearchFormDisplay());
        cloneTruongThongTin.setSearchFormDisplayOrder(getSearchFormDisplayOrder());
        return cloneTruongThongTin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

    public Long getKieuDuLieu() {
        return kieuDuLieu;
    }

    public void setKieuDuLieu(Long kieuDuLieu) {
        this.kieuDuLieu = kieuDuLieu;
    }

    public Long getLoaiHienThi() {
        return loaiHienThi;
    }

    public void setLoaiHienThi(Long loaiHienThi) {
        this.loaiHienThi = loaiHienThi;
    }

    public String getInlineStyle() {
        return inlineStyle;
    }

    public void setInlineStyle(String inlineStyle) {
        this.inlineStyle = inlineStyle;
    }

    public Long getRequired() {
        return required;
    }

    public void setRequired(Long required) {
        this.required = required;
    }

    public String getExpressions() {
        return expressions;
    }

    public void setExpressions(String expressions) {
        this.expressions = expressions;
    }

    public Long getMinlength() {
        return minlength;
    }

    public void setMinlength(Long minlength) {
        this.minlength = minlength;
    }

    public Long getMaxlength() {
        return maxlength;
    }

    public void setMaxlength(Long maxlength) {
        this.maxlength = maxlength;
    }

    public LoaiDanhMuc getLoaiDanhMuc() {
        return loaiDanhMuc;
    }

    public void setLoaiDanhMuc(LoaiDanhMuc loaiDanhMuc) {
        this.loaiDanhMuc = loaiDanhMuc;
    }

	public BigDecimal getMinvalue() {
		return minvalue;
	}

	public void setMinvalue(BigDecimal minvalue) {
		this.minvalue = minvalue;
	}

	public BigDecimal getMaxvalue() {
		return maxvalue;
	}

	public void setMaxvalue(BigDecimal maxvalue) {
		this.maxvalue = maxvalue;
	}

	public String getExpressionMessage() {
		return expressionMessage;
	}

	public void setExpressionMessage(String expressionMessage) {
		this.expressionMessage = expressionMessage;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileExtensionAlow() {
		return fileExtensionAlow;
	}

	public void setFileExtensionAlow(String fileExtensionAlow) {
		this.fileExtensionAlow = fileExtensionAlow;
	}

	public String getMinDuration() {
		return minDuration;
	}

	public void setMinDuration(String minDuration) {
		this.minDuration = minDuration;
	}

	public String getMaxDuration() {
		return maxDuration;
	}

	public void setMaxDuration(String maxDuration) {
		this.maxDuration = maxDuration;
	}


	public Long getMinDurationType() {
		return minDurationType;
	}

	public void setMinDurationType(Long minDurationType) {
		this.minDurationType = minDurationType;
	}

	public Long getMaxDurationType() {
		return maxDurationType;
	}

	public void setMaxDurationType(Long maxDurationType) {
		this.maxDurationType = maxDurationType;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Long getDateDefaultType() {
		return dateDefaultType;
	}

	public void setDateDefaultType(Long dateDefaultType) {
		this.dateDefaultType = dateDefaultType;
	}

	public Long getTimeDetail() {
		return timeDetail;
	}

	public void setTimeDetail(Long timeDetail) {
		this.timeDetail = timeDetail;
	}

	public Long getTableDisplay() {
		return tableDisplay;
	}

	public void setTableDisplay(Long tableDisplay) {
		this.tableDisplay = tableDisplay;
	}

	public Long getSearchFormDisplay() {
		return searchFormDisplay;
	}

	public void setSearchFormDisplay(Long searchFormDisplay) {
		this.searchFormDisplay = searchFormDisplay;
	}

	public Long getTableDisplayOrder() {
		return tableDisplayOrder;
	}

	public void setTableDisplayOrder(Long tableDisplayOrder) {
		this.tableDisplayOrder = tableDisplayOrder;
	}

	public Long getSearchFormDisplayOrder() {
		return searchFormDisplayOrder;
	}

	public void setSearchFormDisplayOrder(Long searchFormDisplayOrder) {
		this.searchFormDisplayOrder = searchFormDisplayOrder;
	}


	
}

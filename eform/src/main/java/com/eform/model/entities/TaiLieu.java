
package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TAI_LIEU")
@Cacheable(false)
@SequenceGenerator(name = "TAI_LIEU_SEQ_GEN", sequenceName = "TAI_LIEU_SEQ", allocationSize = 1, initialValue = 1)
public class TaiLieu
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAI_LIEU_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "MO_TA")
    private String moTa;
    @Column(name = "UPLOAD_PATH", length = 2000)
    private String uploadPath;
    @Column(name = "TRANG_THAI")
    private Long trangThai;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof TaiLieu)) {
            areEqual = true;
            final TaiLieu otherTaiLieu = ((TaiLieu) other);
            if ((this.id == null)||(otherTaiLieu.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherTaiLieu.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        TaiLieu cloneTaiLieu = new TaiLieu();
        cloneTaiLieu.setId(getId());
        cloneTaiLieu.setMa(getMa());
        cloneTaiLieu.setTen(getTen());
        cloneTaiLieu.setMoTa(getMoTa());
        cloneTaiLieu.setUploadPath(getUploadPath());
        cloneTaiLieu.setTrangThai(getTrangThai());
        return cloneTaiLieu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }


    public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

}

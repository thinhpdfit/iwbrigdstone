package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BM_HANG")
@Cacheable(false)
public class BmHang
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @SequenceGenerator(name = "BM_HANG_SEQ_GEN", sequenceName = "BM_HANG_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BM_HANG_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "LOAI_HANG")
    private Long loaiHang;
    @Column(name = "THU_TU")
    private Long thuTu;
    @ManyToOne
    @JoinColumn(name = "BIEU_MAU_ID")
    private BieuMau bieuMau;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof BmHang)) {
            areEqual = true;
            final BmHang otherBmHang = ((BmHang) other);
            if ((this.id == null)||(otherBmHang.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBmHang.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        BmHang cloneBmHang = new BmHang();
        cloneBmHang.setId(getId());
        cloneBmHang.setMa(getMa());
        cloneBmHang.setTen(getTen());
        cloneBmHang.setLoaiHang(getLoaiHang());
        cloneBmHang.setThuTu(getThuTu());
        return cloneBmHang;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getLoaiHang() {
        return loaiHang;
    }

    public void setLoaiHang(Long loaiHang) {
        this.loaiHang = loaiHang;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public BieuMau getBieuMau() {
        return bieuMau;
    }

    public void setBieuMau(BieuMau bieuMau) {
        this.bieuMau = bieuMau;
    }

}

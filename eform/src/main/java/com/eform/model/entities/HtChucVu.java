package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "HT_CHUCVU")
@Cacheable(false)
@SequenceGenerator(name = "HT_CHUCVU_SEQ_GEN", sequenceName = "HT_CHUCVU_SEQ", allocationSize = 1, initialValue = 1)
public class HtChucVu
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HT_CHUCVU_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "TRANG_THAI")
    private Long trangThai;

	public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof HtChucVu)) {
            areEqual = true;
            final HtChucVu otherHtChucVu = ((HtChucVu) other);
            if ((this.id == null)||(otherHtChucVu.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherHtChucVu.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        HtChucVu cloneHtChucVu = new HtChucVu();
        cloneHtChucVu.setId(getId());
        cloneHtChucVu.setMa(getMa());
        cloneHtChucVu.setTen(getTen());
        cloneHtChucVu.setTrangThai(getTrangThai());
        return cloneHtChucVu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }
}

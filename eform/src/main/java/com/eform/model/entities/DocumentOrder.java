
package com.eform.model.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.json.JSONObject;

import com.eform.common.type.jpa.JSONObjectUserType;

@Entity
@Table(name = "document_order")
@Cacheable(false)
@SequenceGenerator(name = "document_order_seq_gen", sequenceName = "document_order_seq", allocationSize = 1, initialValue = 1)
public class DocumentOrder
    implements Serializable
{

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_order_seq_gen")
    private Long id;

    @Column(name = "document_book", length = 200)
    private String documentBook;
    @Column(name = "order_val", length = 200)
    private Long order;
	public DocumentOrder() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getDocumentBook() {
		return documentBook;
	}
	public void setDocumentBook(String documentBook) {
		this.documentBook = documentBook;
	}
	public Long getOrder() {
		return order;
	}
	public void setOrder(Long order) {
		this.order = order;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentOrder other = (DocumentOrder) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
}

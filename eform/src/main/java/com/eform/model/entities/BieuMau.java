
package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.json.JSONObject;

import com.eform.common.type.jpa.JSONObjectUserType;

@Entity
@Table(name = "BIEU_MAU")
@Cacheable(false)
@TypeDefs({ @TypeDef(name = "CustomJsonObject", typeClass = JSONObjectUserType.class) })
public class BieuMau
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @SequenceGenerator(name = "BIEU_MAU_SEQ_GEN", sequenceName = "BIEU_MAU_SEQ", initialValue = 1, allocationSize =0 )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BIEU_MAU_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "LOAI_BIEU_MAU")
    private Long loaiBieuMau;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @Column(name = "JASPER_JRXML")
    private String jasperJrxml;
    @Column(name = "JASPER")
    private byte[] jasper;
    @Column(name = "SO_HANG")
    private Long soHang;
    @Column(name = "SO_COT")
    private Long soCot;
    @Column(name = "width")
    private String width;
    @Column(name = "SERVICE_CONFIG")
    @Type(type = "CustomJsonObject")
    private JSONObject serviceConfig;
    @Column(name = "formservicce_config")
    @Type(type = "CustomJsonObject")
    private JSONObject formServicceConfig;
    
	public JSONObject getFormServicceConfig() {
		return formServicceConfig;
	}

	public void setFormServicceConfig(JSONObject formServicceConfig) {
		this.formServicceConfig = formServicceConfig;
	}

	public JSONObject getServiceConfig() {
		return serviceConfig;
	}

	public void setServiceConfig(JSONObject serviceConfig) {
		this.serviceConfig = serviceConfig;
	}

	public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof BieuMau)) {
            areEqual = true;
            final BieuMau otherBieuMau = ((BieuMau) other);
            if ((this.id == null)||(otherBieuMau.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBieuMau.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        BieuMau cloneBieuMau = new BieuMau();
        cloneBieuMau.setId(getId());
        cloneBieuMau.setMa(getMa());
        cloneBieuMau.setTen(getTen());
        cloneBieuMau.setLoaiBieuMau(getLoaiBieuMau());
        cloneBieuMau.setTrangThai(getTrangThai());
        cloneBieuMau.setJasperJrxml(getJasperJrxml());
        cloneBieuMau.setJasper(getJasper());
        cloneBieuMau.setSoCot(soCot);
        cloneBieuMau.setSoHang(soHang);
        cloneBieuMau.setWidth(width);
        return cloneBieuMau;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getLoaiBieuMau() {
        return loaiBieuMau;
    }

    public void setLoaiBieuMau(Long loaiBieuMau) {
        this.loaiBieuMau = loaiBieuMau;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

    public String getJasperJrxml() {
        return jasperJrxml;
    }

    public void setJasperJrxml(String jasperJrxml) {
        this.jasperJrxml = jasperJrxml;
    }

    public byte[] getJasper() {
        return jasper;
    }

    public void setJasper(byte[] jasper) {
        this.jasper = jasper;
    }

	public Long getSoHang() {
		return soHang;
	}

	public void setSoHang(Long soHang) {
		this.soHang = soHang;
	}

	public Long getSoCot() {
		return soCot;
	}

	public void setSoCot(Long soCot) {
		this.soCot = soCot;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}
	

}

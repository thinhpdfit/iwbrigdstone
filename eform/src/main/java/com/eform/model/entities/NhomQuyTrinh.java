
package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "NHOM_QUY_TRINH")
@Cacheable(false)
@SequenceGenerator(name = "NHOM_QUY_TRINH_SEQ_GEN", sequenceName = "NHOM_QUY_TRINH_SEQ", allocationSize = 1, initialValue = 1)
public class NhomQuyTrinh
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NHOM_QUY_TRINH_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @ManyToOne
    @JoinColumn(name = "NHOM_QUY_TRINH_ID")
    private NhomQuyTrinh nhomQuyTrinh;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof NhomQuyTrinh)) {
            areEqual = true;
            final NhomQuyTrinh otherNhomQuyTrinh = ((NhomQuyTrinh) other);
            if ((this.id == null)||(otherNhomQuyTrinh.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherNhomQuyTrinh.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        NhomQuyTrinh cloneNhomQuyTrinh = new NhomQuyTrinh();
        cloneNhomQuyTrinh.setId(getId());
        cloneNhomQuyTrinh.setMa(getMa());
        cloneNhomQuyTrinh.setTen(getTen());
        cloneNhomQuyTrinh.setTrangThai(getTrangThai());
        return cloneNhomQuyTrinh;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

	public NhomQuyTrinh getNhomQuyTrinh() {
		return nhomQuyTrinh;
	}

	public void setNhomQuyTrinh(NhomQuyTrinh nhomQuyTrinh) {
		this.nhomQuyTrinh = nhomQuyTrinh;
	}

}

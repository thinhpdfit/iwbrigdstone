
package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "QUY_TRINH_TAI_LIEU")
@Cacheable(false)
@SequenceGenerator(name = "QUY_TRINH_TAI_LIEU_SEQ_GEN", sequenceName = "QUY_TRINH_TAI_LIEU_SEQ", allocationSize = 1, initialValue = 1)
public class QuyTrinhTaiLieu
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUY_TRINH_TAI_LIEU_SEQ_GEN")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "QUY_TRINH_ID")
    private QuyTrinh quyTrinh;
    @ManyToOne
    @JoinColumn(name = "TAI_LIEU_ID")
    private TaiLieu taiLieu;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof QuyTrinhTaiLieu)) {
            areEqual = true;
            final QuyTrinhTaiLieu otherQuyTrinhTaiLieu = ((QuyTrinhTaiLieu) other);
            if ((this.id == null)||(otherQuyTrinhTaiLieu.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherQuyTrinhTaiLieu.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        QuyTrinhTaiLieu cloneQuyTrinhTaiLieu = new QuyTrinhTaiLieu();
        cloneQuyTrinhTaiLieu.setId(getId());
        return cloneQuyTrinhTaiLieu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public QuyTrinh getQuyTrinh() {
        return quyTrinh;
    }

    public void setQuyTrinh(QuyTrinh quyTrinh) {
        this.quyTrinh = quyTrinh;
    }

    public TaiLieu getTaiLieu() {
        return taiLieu;
    }

    public void setTaiLieu(TaiLieu taiLieu) {
        this.taiLieu = taiLieu;
    }

}

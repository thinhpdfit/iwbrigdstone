package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EMAIL_PHE_DUYET")
@Cacheable(false)
@SequenceGenerator(name = "EMAIL_PHE_DUYET_SEQ_GEN", sequenceName = "EMAIL_PHE_DUYET_SEQ", allocationSize = 1, initialValue = 1)
public class EmailPheDuyet
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMAIL_PHE_DUYET_SEQ_GEN")
    private Long id;
    @Column(name = "PROCESS_INSTANCE_ID", length = 2000)
    private String processInstanceId;
    @Column(name = "ACTIVITY_ID", length = 2000)
    private String activityId;
    @Column(name = "REF_CODE", length = 2000)
    private String refCode;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @Column(name = "TRUONG_KY_SO", length = 2000)
    private String truongKySo;

	public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof EmailPheDuyet)) {
            areEqual = true;
            final EmailPheDuyet otherHtQuyen = ((EmailPheDuyet) other);
            if ((this.id == null)||(otherHtQuyen.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherHtQuyen.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        EmailPheDuyet clonEmailPheDuyet = new EmailPheDuyet();
        clonEmailPheDuyet.setId(getId());
        clonEmailPheDuyet.setProcessInstanceId(getProcessInstanceId());
        clonEmailPheDuyet.setActivityId(getActivityId());
        clonEmailPheDuyet.setRefCode(getRefCode());
        clonEmailPheDuyet.setTrangThai(getTrangThai());
        return clonEmailPheDuyet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public Long getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Long trangThai) {
		this.trangThai = trangThai;
	}

	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public String getTruongKySo() {
		return truongKySo;
	}

	public void setTruongKySo(String truongKySo) {
		this.truongKySo = truongKySo;
	}
	
	
}

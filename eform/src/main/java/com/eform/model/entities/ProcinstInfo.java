
package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.json.JSONObject;

import com.eform.common.type.jpa.JSONObjectUserType;

@Entity
@Table(name = "procinst_info")
@Cacheable(false)
@SequenceGenerator(name = "procinst_info_seq_gen", sequenceName = "procinst_info_seq", allocationSize = 1, initialValue = 1)
public class ProcinstInfo implements Serializable
{

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "procinst_info_seq_gen")
    private Long id;
    @Column(name = "info_key", length = 2000)
    private String infoKey;
    @Column(name = "info")
    private String info;
    @Column(name = "code",length = 2000)
    private String code;
    



	@Column(name = "proc_inst_id", length = 2000)
    private String procinstId;
    
    
    
	public ProcinstInfo() {
		super();
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProcinstInfo other = (ProcinstInfo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getInfoKey() {
		return infoKey;
	}



	public void setInfoKey(String infoKey) {
		this.infoKey = infoKey;
	}



	public String getInfo() {
		return info;
	}



	public void setInfo(String info) {
		this.info = info;
	}



	public String getProcinstId() {
		return procinstId;
	}



	public void setProcinstId(String procinstId) {
		this.procinstId = procinstId;
	}

	
	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}

	



}

package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "bao_cao_thong_ke_don_vi")
@Cacheable(false)
public class BaoCaoThongKeDonVi
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @SequenceGenerator(name = "bao_cao_thong_ke_don_vi_seq_gen", sequenceName = "bao_cao_thong_ke_don_vi_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bao_cao_thong_ke_don_vi_seq_gen")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "bao_cao_thong_ke_id")
    private BaoCaoThongKe baoCaoThongKe;
    
    @ManyToOne
    @JoinColumn(name = "ht_donvi_id")
    private HtDonVi htDonVi;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof BaoCaoThongKeDonVi)) {
            areEqual = true;
            final BaoCaoThongKeDonVi otherBmHang = ((BaoCaoThongKeDonVi) other);
            if ((this.id == null)||(otherBmHang.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBmHang.id.equals(this.id));
        }
        return areEqual;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BaoCaoThongKe getBaoCaoThongKe() {
		return baoCaoThongKe;
	}

	public void setBaoCaoThongKe(BaoCaoThongKe baoCaoThongKe) {
		this.baoCaoThongKe = baoCaoThongKe;
	}

	public HtDonVi getHtDonVi() {
		return htDonVi;
	}

	public void setHtDonVi(HtDonVi htDonVi) {
		this.htDonVi = htDonVi;
	}

}

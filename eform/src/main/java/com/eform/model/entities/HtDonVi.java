package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "HT_DONVI")
@Cacheable(false)
@SequenceGenerator(name = "HT_DONVI_SEQ_GEN", sequenceName = "HT_DONVI_SEQ", allocationSize = 1, initialValue = 1)
public class HtDonVi
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HT_DONVI_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @ManyToOne
    @JoinColumn(name = "HT_DONVI_ID")
    private HtDonVi htDonVi;
    @Column(name = "NGUOI_DAI_DIEN", length = 2000)
    private String nguoiDaiDien;
    
    @ManyToOne
    @JoinColumn(name = "ht_chuvu_cao_nhat_id")
    private HtChucVu htChuVuCaoNhat;

	

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HtDonVi other = (HtDonVi) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

	public HtDonVi getHtDonVi() {
		return htDonVi;
	}

	public void setHtDonVi(HtDonVi htDonVi) {
		this.htDonVi = htDonVi;
	}

	public String getNguoiDaiDien() {
		return nguoiDaiDien;
	}

	public void setNguoiDaiDien(String nguoiDaiDien) {
		this.nguoiDaiDien = nguoiDaiDien;
	}

	public HtChucVu getHtChuVuCaoNhat() {
		return htChuVuCaoNhat;
	}

	public void setHtChuVuCaoNhat(HtChucVu htChuVuCaoNhat) {
		this.htChuVuCaoNhat = htChuVuCaoNhat;
	}
	
	
}

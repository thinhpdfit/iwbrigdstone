
package com.eform.model.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "FILE_MANAGEMENT")
@Cacheable(false)
@SequenceGenerator(name = "FILE_MANAGEMENT_SEQ_GEN", sequenceName = "FILE_MANAGEMENT_SEQ", allocationSize = 1, initialValue = 1)
public class FileManagement implements Serializable {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILE_MANAGEMENT_SEQ_GEN")
	private Long id;

	@Column(name = "TIEU_DE", length = 2000)
	private String tieuDe;

	@Column(name = "MO_TA")
	private String moTa;
	

	@Column(name = "NGUOI_TAO", length = 2000)
	private String nguoiTao;

	@Column(name = "NGAY_TAO")
	private Timestamp ngayTao;

	@Column(name = "NGUOI_CAP_NHAT", length = 2000)
	private String nguoiCapNhat;

	@Column(name = "NGAY_CAP_NHAT")
	private Timestamp ngayCapNhat;

	public boolean equals(Object other) {
		boolean areEqual = false;
		if ((other != null) && (other instanceof FileManagement)) {
			areEqual = true;
			final FileManagement otherQuyTrinh = ((FileManagement) other);
			if ((this.id == null) || (otherQuyTrinh.id == null)) {
				return super.equals(other);
			}
			areEqual = (areEqual && otherQuyTrinh.id.equals(this.id));
		}
		return areEqual;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTieuDe() {
		return tieuDe;
	}

	public void setTieuDe(String tieuDe) {
		this.tieuDe = tieuDe;
	}

	public String getMoTa() {
		return moTa;
	}

	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}

	public String getNguoiTao() {
		return nguoiTao;
	}

	public void setNguoiTao(String nguoiTao) {
		this.nguoiTao = nguoiTao;
	}

	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	public String getNguoiCapNhat() {
		return nguoiCapNhat;
	}

	public void setNguoiCapNhat(String nguoiCapNhat) {
		this.nguoiCapNhat = nguoiCapNhat;
	}

	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}
	
	

}


package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.json.JSONObject;

import com.eform.common.type.jpa.JSONObjectUserType;

@Entity
@Table(name = "HT_THAM_SO")
@Cacheable(false)
@SequenceGenerator(name = "HT_THAM_SO_SEQ_GEN", sequenceName = "HT_THAM_SO_SEQ", allocationSize = 1, initialValue = 1)
@TypeDefs({ @TypeDef(name = "CustomJsonObject", typeClass = JSONObjectUserType.class) })
public class HtThamSo
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HT_THAM_SO_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "GIA_TRI", length = 2000)
    private String giaTri;
    
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "TRANG_THAI", length = 10)
    private Long trangThai;
    @Column(name = "LOAI", length = 10)
    private Long loai;
    @Column(name = "THU_TU", length = 10)
    private Long thuTu;
    @Column(name = "MO_TA", length = 2000)
    private String moTa;
    @Column(name = "LOAI_HIEN_THI	", length = 10)
    private Long loaiHienThi;
    @Column(name = "BIEU_THUC_KIEM_TRA", length = 2000)
    private String bieuThucKiemTra;
    @Column(name = "DU_LIEU")
    @Type(type = "CustomJsonObject")
    private JSONObject duLieu;


    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof HtThamSo)) {
            areEqual = true;
            final HtThamSo otherHtThamSo = ((HtThamSo) other);
            if ((this.id == null)||(otherHtThamSo.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherHtThamSo.id.equals(this.id));
        }
        return areEqual;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getGiaTri() {
        return giaTri;
    }

    public void setGiaTri(String giaTri) {
        this.giaTri = giaTri;
    }

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public Long getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Long trangThai) {
		this.trangThai = trangThai;
	}

	public Long getLoai() {
		return loai;
	}

	public void setLoai(Long loai) {
		this.loai = loai;
	}

	public Long getThuTu() {
		return thuTu;
	}

	public void setThuTu(Long thuTu) {
		this.thuTu = thuTu;
	}

	public String getMoTa() {
		return moTa;
	}

	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}

	public Long getLoaiHienThi() {
		return loaiHienThi;
	}

	public void setLoaiHienThi(Long loaiHienThi) {
		this.loaiHienThi = loaiHienThi;
	}

	public String getBieuThucKiemTra() {
		return bieuThucKiemTra;
	}

	public void setBieuThucKiemTra(String bieuThucKiemTra) {
		this.bieuThucKiemTra = bieuThucKiemTra;
	}

	public JSONObject getDuLieu() {
		return duLieu;
	}

	public void setDuLieu(JSONObject duLieu) {
		this.duLieu = duLieu;
	}
	
	

}


package com.eform.model.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SIGNATURE")
@Cacheable(false)
@SequenceGenerator(name = "SIGNATURE_SEQ_GEN", sequenceName = "SIGNATURE_SEQ", allocationSize = 1, initialValue = 1)
public class Signature implements Serializable{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SIGNATURE_SEQ_GEN")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "FILE_VERSION_ID")
    private FileVersion fileVersion;

    @Column(name = "GHI_CHU")
    private String ghiChu;

    @Column(name = "NGUOI_KY", length=2000)
    private String nguoiKy;
    
    @Column(name = "NGAY_KY")
    private Timestamp ngayKy;
    
    @Column(name = "THU_TU", length=10)
    private Long thuTu;
    



	public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof Signature)) {
            areEqual = true;
            final Signature otherQuyTrinh = ((Signature) other);
            if ((this.id == null)||(otherQuyTrinh.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherQuyTrinh.id.equals(this.id));
        }
        return areEqual;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FileVersion getFileVersion() {
		return fileVersion;
	}

	public void setFileVersion(FileVersion fileVersion) {
		this.fileVersion = fileVersion;
	}


	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	public String getNguoiKy() {
		return nguoiKy;
	}

	public void setNguoiKy(String nguoiKy) {
		this.nguoiKy = nguoiKy;
	}

	public Timestamp getNgayKy() {
		return ngayKy;
	}

	public void setNgayKy(Timestamp ngayKy) {
		this.ngayKy = ngayKy;
	}

	public Long getThuTu() {
		return thuTu;
	}

	public void setThuTu(Long thuTu) {
		this.thuTu = thuTu;
	}

	

}

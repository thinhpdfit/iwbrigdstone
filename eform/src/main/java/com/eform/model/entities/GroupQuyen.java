package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "GROUP_QUYEN")
@Cacheable(false)
@SequenceGenerator(name = "GROUP_QUYEN_SEQ_GEN", sequenceName = "GROUP_QUYEN_SEQ", allocationSize = 1, initialValue = 1)
public class GroupQuyen
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GROUP_QUYEN_SEQ_GEN")
    private Long id;
    @Column(name = "GROUP_ID", length = 20)
    private String groupId;
    @ManyToOne
    @JoinColumn(name = "HT_QUYEN_ID")
    private HtQuyen htQuyen;
    @Column(name = "ACTION")
    private Long action;


    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof GroupQuyen)) {
            areEqual = true;
            final GroupQuyen otherTruongThongTin = ((GroupQuyen) other);
            if ((this.id == null)||(otherTruongThongTin.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherTruongThongTin.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        GroupQuyen cloneGroupQuyen = new GroupQuyen();
        cloneGroupQuyen.setId(getId());
        cloneGroupQuyen.setGroupId(getGroupId());
        return cloneGroupQuyen;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public HtQuyen getHtQuyen() {
		return htQuyen;
	}

	public void setHtQuyen(HtQuyen htQuyen) {
		this.htQuyen = htQuyen;
	}

	public Long getAction() {
		return action;
	}

	public void setAction(Long action) {
		this.action = action;
	}
	
	

}

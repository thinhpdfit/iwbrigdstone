package com.eform.model.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.json.JSONObject;

import com.eform.common.type.jpa.JSONObjectUserType;

@Entity
@Table(name = "BAO_CAO_THONG_KE")
@Cacheable(false)
@TypeDefs({ @TypeDef(name = "CustomJsonObject", typeClass = JSONObjectUserType.class) })
public class BaoCaoThongKe
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @SequenceGenerator(name = "BAO_CAO_THONG_KE_SEQ_GEN", sequenceName = "BAO_CAO_THONG_KE_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BAO_CAO_THONG_KE_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    
    @Column(name = "KIEU_BAO_CAO")
    private Long kieuBaoCao;
    @Column(name = "LAYOUT")
    private Long layout;
    @Column(name = "KIEU_DO_THI", length = 100)
    private String kieuDoThi;
    @Column(name = "PROCESS_DEF_ID", length = 100)
    private String processDefId;
    
    @ManyToOne
    @JoinColumn(name = "BIEU_MAU_ID")
    private BieuMau bieuMau;
    @Column(name = "gia_tri")
    @Type(type = "CustomJsonObject")
    private JSONObject  giaTri;
    @Column(name = "TU_NGAY")
    private Timestamp tuNgay;
    @Column(name = "DEN_NGAY")
    private Timestamp denNgay;
    @Column(name = "NGAY_TAO")
    private Timestamp ngayTao;
    @Column(name = "NGAY_CAP_NHAT")
    private Timestamp ngayCapNhat;
    @Column(name = "NGUOI_TAO", length = 20)
    private String nguoiTao;
    @Column(name = "NGUOI_CAP_NHAT", length = 20)
    private String nguoiCapNhat;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @Column(name = "TIEU_DE", length = 2000)
    private String tieuDe;
    @ManyToOne
    @JoinColumn(name = "ht_donvi_id")
    private HtDonVi htDonVi;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof BaoCaoThongKe)) {
            areEqual = true;
            final BaoCaoThongKe otherBctk = ((BaoCaoThongKe) other);
            if ((this.id == null)||(otherBctk.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBctk.id.equals(this.id));
        }
        return areEqual;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public BieuMau getBieuMau() {
        return bieuMau;
    }

    public void setBieuMau(BieuMau bieuMau) {
        this.bieuMau = bieuMau;
    }

	public Long getKieuBaoCao() {
		return kieuBaoCao;
	}

	public void setKieuBaoCao(Long kieuBaoCao) {
		this.kieuBaoCao = kieuBaoCao;
	}

	public Long getLayout() {
		return layout;
	}

	public void setLayout(Long layout) {
		this.layout = layout;
	}

	public String getKieuDoThi() {
		return kieuDoThi;
	}

	public void setKieuDoThi(String kieuDoThi) {
		this.kieuDoThi = kieuDoThi;
	}
	

	public String getProcessDefId() {
		return processDefId;
	}

	public void setProcessDefId(String processDefId) {
		this.processDefId = processDefId;
	}

	
	public JSONObject getGiaTri() {
		return giaTri;
	}

	public void setGiaTri(JSONObject giaTri) {
		this.giaTri = giaTri;
	}

	public Timestamp getTuNgay() {
		return tuNgay;
	}

	public void setTuNgay(Timestamp tuNgay) {
		this.tuNgay = tuNgay;
	}

	public Timestamp getDenNgay() {
		return denNgay;
	}

	public void setDenNgay(Timestamp denNgay) {
		this.denNgay = denNgay;
	}

	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	public String getNguoiTao() {
		return nguoiTao;
	}

	public void setNguoiTao(String nguoiTao) {
		this.nguoiTao = nguoiTao;
	}

	public String getNguoiCapNhat() {
		return nguoiCapNhat;
	}

	public void setNguoiCapNhat(String nguoiCapNhat) {
		this.nguoiCapNhat = nguoiCapNhat;
	}

	public Long getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Long trangThai) {
		this.trangThai = trangThai;
	}

	public String getTieuDe() {
		return tieuDe;
	}

	public void setTieuDe(String tieuDe) {
		this.tieuDe = tieuDe;
	}

	public HtDonVi getHtDonVi() {
		return htDonVi;
	}

	public void setHtDonVi(HtDonVi htDonVi) {
		this.htDonVi = htDonVi;
	}

}

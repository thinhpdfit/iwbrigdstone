package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "HT_NGUOIDUNG_CHUCVU")
@Cacheable(false)
@SequenceGenerator(name = "HT_NGUOIDUNG_CHUCVU_SEQ_GEN", sequenceName = "HT_NGUOIDUNG_CHUCVU_SEQ", allocationSize = 1, initialValue = 1)
public class HtNguoidungChucvu
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HT_NGUOIDUNG_CHUCVU_SEQ_GEN")
    private Long id;
    @Column(name = "TEN_DANG_NHAP", length = 2000)
    private String tenDangNhap;
    @ManyToOne
    @JoinColumn(name = "HT_DONVI_ID")
    private HtDonVi htDonVi;
    @ManyToOne
    @JoinColumn(name = "HT_CHUCVU_ID")
    private HtChucVu htChucVu;

	public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof HtNguoidungChucvu)) {
            areEqual = true;
            final HtNguoidungChucvu otherHtNguoidungChucvu = ((HtNguoidungChucvu) other);
            if ((this.id == null)||(otherHtNguoidungChucvu.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherHtNguoidungChucvu.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        HtNguoidungChucvu cloneHtNguoidungChucvu = new HtNguoidungChucvu();
        cloneHtNguoidungChucvu.setId(getId());
        cloneHtNguoidungChucvu.setTenDangNhap(getTenDangNhap());
        return cloneHtNguoidungChucvu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getTenDangNhap() {
		return tenDangNhap;
	}

	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}

	public HtDonVi getHtDonVi() {
		return htDonVi;
	}

	public void setHtDonVi(HtDonVi htDonVi) {
		this.htDonVi = htDonVi;
	}

	public HtChucVu getHtChucVu() {
		return htChucVu;
	}

	public void setHtChucVu(HtChucVu htChucVu) {
		this.htChucVu = htChucVu;
	}
}

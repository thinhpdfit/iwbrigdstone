
package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "FORMULA")
@Cacheable(false)
@SequenceGenerator(name = "FORMULA_SEQ_GEN", sequenceName = "FORMULA_SEQ", initialValue = 1)
public class Formula
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @SequenceGenerator(name = "FORMULA_SEQ_GEN", sequenceName = "FORMULA_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FORMULA_SEQ_GEN")
    private Long id;
    
    
    @Column(name = "result_field", length = 200)
    private String resultField;
    
    @Column(name = "formula_path", length = 2000)
    private String formulaPath;
    @Column(name = "formula", length = 2000)
    private String formula;
    
    @Column(name = "thu_tu")
    private Long thuTu;
    @ManyToOne
    @JoinColumn(name = "BIEU_MAU_ID")
    private BieuMau bieuMau;
    

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof Formula)) {
            areEqual = true;
            final Formula otherBmO = ((Formula) other);
            if ((this.id == null)||(otherBmO.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherBmO.id.equals(this.id));
        }
        return areEqual;
    }


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getResultField() {
		return resultField;
	}


	public void setResultField(String resultField) {
		this.resultField = resultField;
	}


	public String getFormulaPath() {
		return formulaPath;
	}


	public void setFormulaPath(String formulaPath) {
		this.formulaPath = formulaPath;
	}


	public String getFormula() {
		return formula;
	}


	public void setFormula(String formula) {
		this.formula = formula;
	}


	public Long getThuTu() {
		return thuTu;
	}


	public void setThuTu(Long thuTu) {
		this.thuTu = thuTu;
	}


	public BieuMau getBieuMau() {
		return bieuMau;
	}


	public void setBieuMau(BieuMau bieuMau) {
		this.bieuMau = bieuMau;
	}

   
    
}

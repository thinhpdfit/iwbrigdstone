
package com.eform.model.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.json.JSONObject;

import com.eform.common.type.jpa.JSONObjectUserType;

@Entity
@Table(name = "HO_SO")
@Cacheable(false)
@SequenceGenerator(name = "HO_SO_SEQ_GEN", sequenceName = "HO_SO_SEQ", allocationSize = 1, initialValue = 1)
@TypeDefs({ @TypeDef(name = "CustomJsonObject", typeClass = JSONObjectUserType.class) })
public class HoSo
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HO_SO_SEQ_GEN")
    private Long id;
    
    @Column(name = "MA", length = 2000)
    private String ma;
    
    @Column(name = "NGAY_TAO")
    private Timestamp ngayTao;
    
    @Column(name = "NGUOI_TAO", length = 2000)
    private String nguoiTao;
    
    @Column(name = "NGAY_CAP_NHAT")
    private Timestamp ngayCapNhat;
    
    @Column(name = "NGUOI_CAP_NHAT", length = 2000)
    private String nguoiCapNhat;
    
    @Column(name = "process_instance_id")
    private String processInstanceId;
    
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    
    @Column(name = "HO_SO_GIA_TRI")
    private String hoSoGiaTri;
    
    @ManyToOne
    @JoinColumn(name = "BIEU_MAU_ID")
    private BieuMau bieuMau;
    
    @Column(name = "task_id")
    private String taskId;

    @Column(name = "ho_so_json")
    @Type(type = "CustomJsonObject")
    private JSONObject hoSoJson;
    

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof HoSo)) {
            areEqual = true;
            final HoSo otherHoSo = ((HoSo) other);
            if ((this.id == null)||(otherHoSo.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherHoSo.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        HoSo cloneHoSo = new HoSo();
        cloneHoSo.setId(getId());
        cloneHoSo.setMa(getMa());
        cloneHoSo.setNgayTao(getNgayTao());
        cloneHoSo.setNguoiTao(getNguoiTao());
        cloneHoSo.setNgayCapNhat(getNgayCapNhat());
        cloneHoSo.setNguoiCapNhat(getNguoiCapNhat());
        cloneHoSo.setTrangThai(getTrangThai());
        cloneHoSo.setHoSoGiaTri(getHoSoGiaTri());
        cloneHoSo.setProcessInstanceId(getProcessInstanceId());
        cloneHoSo.setTaskId(getTaskId());
        return cloneHoSo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public Timestamp getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Timestamp ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public Timestamp getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Timestamp ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getNguoiCapNhat() {
        return nguoiCapNhat;
    }

    public void setNguoiCapNhat(String nguoiCapNhat) {
        this.nguoiCapNhat = nguoiCapNhat;
    }



    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

    public String getHoSoGiaTri() {
        return hoSoGiaTri;
    }

    public void setHoSoGiaTri(String hoSoGiaTri) {
        this.hoSoGiaTri = hoSoGiaTri;
    }

    public BieuMau getBieuMau() {
        return bieuMau;
    }

    public void setBieuMau(BieuMau bieuMau) {
        this.bieuMau = bieuMau;
    }

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public JSONObject getHoSoJson() {
		return hoSoJson;
	}

	public void setHoSoJson(JSONObject  hoSoJson) {
		this.hoSoJson = hoSoJson;
	}

}

package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "HT_QUYEN")
@Cacheable(false)
@SequenceGenerator(name = "HT_QUYEN_SEQ_GEN", sequenceName = "HT_QUYEN_SEQ", allocationSize = 1, initialValue = 1)
public class HtQuyen
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HT_QUYEN_SEQ_GEN")
    private Long id;
    @Column(name = "MA", length = 20)
    private String ma;
    @Column(name = "TEN", length = 2000)
    private String ten;
    @Column(name = "VIEW_NAME", length = 2000)
    private String viewName;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @Column(name = "ICON")
    private Integer icon;
    @Column(name = "MENU", length = 1)
    private Long menu;
    @ManyToOne
    @JoinColumn(name = "HT_QUYEN_ID")
    private HtQuyen htQuyen;
    @Column(name = "ACTION")
    private Long action;
    @Column(name = "prodef_id")
    private String prodefId;
    @Column(name = "form_code")
    private String maBieuMau;

    public HtQuyen getHtQuyen() {
		return htQuyen;
	}

	public void setHtQuyen(HtQuyen htQuyen) {
		this.htQuyen = htQuyen;
	}

	public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof HtQuyen)) {
            areEqual = true;
            final HtQuyen otherHtQuyen = ((HtQuyen) other);
            if ((this.id == null)||(otherHtQuyen.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherHtQuyen.id.equals(this.id));
        }
        return areEqual;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        HtQuyen cloneHtQuyen = new HtQuyen();
        cloneHtQuyen.setId(getId());
        cloneHtQuyen.setMa(getMa());
        cloneHtQuyen.setTen(getTen());
        cloneHtQuyen.setTrangThai(getTrangThai());
        cloneHtQuyen.setIcon(getIcon());
        cloneHtQuyen.setViewName(getViewName());
        return cloneHtQuyen;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

	public Integer getIcon() {
		return icon;
	}

	public void setIcon(Integer icon) {
		this.icon = icon;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public Long getMenu() {
		return menu;
	}

	public void setMenu(Long menu) {
		this.menu = menu;
	}

	public Long getAction() {
		return action;
	}

	public void setAction(Long action) {
		this.action = action;
	}

	public String getProdefId() {
		return prodefId;
	}

	public void setProdefId(String prodefId) {
		this.prodefId = prodefId;
	}

	public String getMaBieuMau() {
		return maBieuMau;
	}

	public void setMaBieuMau(String maBieuMau) {
		this.maBieuMau = maBieuMau;
	}
	
}

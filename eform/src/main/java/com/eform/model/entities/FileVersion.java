
package com.eform.model.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "FILE_VERSION")
@Cacheable(false)
@SequenceGenerator(name = "FILE_VERSION_SEQ_GEN", sequenceName = "FILE_VERSION_SEQ", allocationSize = 1, initialValue = 1)
public class FileVersion implements Serializable {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILE_VERSION_SEQ_GEN")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "FILE_MANAGEMENT_ID")
	private FileManagement fileManagement;

	@Column(name = "VERSION")
	private Long version;

	@Column(name = "NGUOI_CAP_NHAT", length = 2000)
	private String nguoiCapNhat;

	@Column(name = "NGAY_CAP_NHAT")
	private Timestamp ngayCapNhat;

	@Column(name = "GHI_CHU")
	private String ghiChu;

	@Column(name = "DINH_DANG", length = 2000)
	private String dinhDang;

	@Column(name = "UPLOAD_PATH", length = 2000)
	private String uploadPath;

	@Column(name = "PDF_PATH", length = 2000)
	private String pdfPath;
	

	@Column(name = "TRANG_THAI")
	private Long trangThai;
	

	@Column(name = "SIGNED_PATH", length = 2000)
	private String signedPath;
	

	public boolean equals(Object other) {
		boolean areEqual = false;
		if ((other != null) && (other instanceof FileVersion)) {
			areEqual = true;
			final FileVersion otherQuyTrinh = ((FileVersion) other);
			if ((this.id == null) || (otherQuyTrinh.id == null)) {
				return super.equals(other);
			}
			areEqual = (areEqual && otherQuyTrinh.id.equals(this.id));
		}
		return areEqual;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public FileManagement getFileManagement() {
		return fileManagement;
	}

	public void setFileManagement(FileManagement fileManagement) {
		this.fileManagement = fileManagement;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getNguoiCapNhat() {
		return nguoiCapNhat;
	}

	public void setNguoiCapNhat(String nguoiCapNhat) {
		this.nguoiCapNhat = nguoiCapNhat;
	}

	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	public String getDinhDang() {
		return dinhDang;
	}

	public void setDinhDang(String dinhDang) {
		this.dinhDang = dinhDang;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public Long getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Long trangThai) {
		this.trangThai = trangThai;
	}

	public String getSignedPath() {
		return signedPath;
	}

	public void setSignedPath(String signedPath) {
		this.signedPath = signedPath;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	

}

package com.eform.model.entities;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DEPLOYMENT_HT_DONVI")
@Cacheable(false)
@SequenceGenerator(name = "DEPLOYMENT_HT_DONVI_SEQ_GEN", sequenceName = "DEPLOYMENT_HT_DONVI_SEQ", allocationSize = 1, initialValue = 1)
public class DeploymentHtDonVi
    implements Serializable
{

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEPLOYMENT_HT_DONVI_SEQ_GEN")
    private Long id;
    @Column(name = "DEPLOYMENT_ID", length = 10)
    private String deploymentId;
    @ManyToOne
    @JoinColumn(name = "HT_DONVI_ID")
    private HtDonVi htDonVi;

    public boolean equals(Object other) {
        boolean areEqual = false;
        if ((other!= null)&&(other instanceof DeploymentHtDonVi)) {
            areEqual = true;
            final DeploymentHtDonVi otherTruongThongTin = ((DeploymentHtDonVi) other);
            if ((this.id == null)||(otherTruongThongTin.id == null)) {
                return super.equals(other);
            }
            areEqual = (areEqual&&otherTruongThongTin.id.equals(this.id));
        }
        return areEqual;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }




	public String getDeploymentId() {
		return deploymentId;
	}


	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}


	public HtDonVi getHtDonVi() {
		return htDonVi;
	}


	public void setHtDonVi(HtDonVi htDonVi) {
		this.htDonVi = htDonVi;
	}

}

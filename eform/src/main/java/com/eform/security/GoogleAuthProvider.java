package com.eform.security;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

import com.eform.common.utils.StringUtils;
import com.eform.security.custom.ActivitiUserDetails;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

public class GoogleAuthProvider implements AuthenticationProvider {
	private static final HttpTransport transport = new NetHttpTransport();
	private static final JsonFactory jsonFactory = new JacksonFactory();
	@Autowired
	protected IdentityService identityService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		Object details = authentication.getDetails();
		if (details instanceof CustomWebAuthenticationDetails) {
			CustomWebAuthenticationDetails webAuthenticationDetails = (CustomWebAuthenticationDetails) details;
			if (webAuthenticationDetails != null && webAuthenticationDetails.getAuthProvider() != null
					&& webAuthenticationDetails
							.getAuthProvider() == CustomWebAuthenticationDetails.EAuthProvider.GOOGLE) {
				String loginEmail = (String) authentication.getPrincipal();
				String idTokenString = (String) authentication.getCredentials();

				GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
						.setAudience(Collections.singletonList(
								"551102288950-82shbdiem8atb3siumnfahu1cs7iv5d8.apps.googleusercontent.com"))
						// Or, if multiple clients access the backend:
						// .setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2,
						// CLIENT_ID_3))
						.build();

				// (Receive idTokenString by HTTPS POST)

				GoogleIdToken idToken = null;
				try {
					idToken = verifier.verify(idTokenString);
				} catch (Exception e) {
					// khong ket noi duoc toi server
					e.printStackTrace();
				}
				if (idToken != null) {
					Payload payload = idToken.getPayload();
					// Print user identifier
					String userId = payload.getSubject();
					System.out.println("User ID: " + userId);

					// Get profile information from payload
					String email = payload.getEmail();
					boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
					String name = (String) payload.get("name");
					String familyName = (String) payload.get("family_name");
					String givenName = (String) payload.get("given_name");
					if (email != null && email.equalsIgnoreCase(loginEmail) && emailVerified) {
						List<User> users = identityService.createUserQuery().userId(email).list();
						if(users != null && !users.isEmpty()){
							List<Group> groups = identityService.createGroupQuery().groupMember(email).list();
							ActivitiUserDetails userDetails = new ActivitiUserDetails(users.get(0), groups);
							Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
							return new UsernamePasswordAuthenticationToken(userDetails, idTokenString, authorities);
						}else{
							//Create new user
							User userNew = identityService.newUser(email);
							userNew.setEmail(email);
							
							if(name != null){
								String firstName = name;
								String lastName = name;
								if(name.lastIndexOf(" ") > 0){
									firstName = name.substring(0, name.lastIndexOf(" "));
									lastName = name.substring(name.lastIndexOf(" "));
								}
								userNew.setFirstName(firstName.trim());
								userNew.setLastName(lastName.trim());
							}
							
							userNew.setPassword(StringUtils.getSHA512(email));
							identityService.saveUser(userNew);
							
							ActivitiUserDetails userDetails = new ActivitiUserDetails(userNew, null);
							Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
							return new UsernamePasswordAuthenticationToken(userDetails, idTokenString, authorities);
						}
					}

					// Use or store profile information
					// ...

				} else {
					System.out.println("Invalid ID token.");
				}
			}
		}
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
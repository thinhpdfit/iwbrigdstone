package com.eform.security;

import org.springframework.security.web.authentication.WebAuthenticationDetails;
import javax.servlet.http.HttpServletRequest;

public class CustomWebAuthenticationDetails extends WebAuthenticationDetails {

    private EAuthProvider authProvider;

    public CustomWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        String authProviderString = request.getParameter("authProvider");
        if(authProviderString!=null&&authProviderString.isEmpty()==false){
        	if(authProviderString.equalsIgnoreCase("google")){
            	authProvider=EAuthProvider.GOOGLE;
        	}
        }
        if(authProvider==null){
        	authProvider=EAuthProvider.LOCAL;
        }
    }
    
    public boolean checkAuthProviderType(EAuthProvider checkAuthProvider){
    	return authProvider!=null&&authProvider==checkAuthProvider;
    }

    public EAuthProvider getAuthProvider() {
        return authProvider;
    }
    
    public enum EAuthProvider{
    	LOCAL,GOOGLE
    }
    
}
package com.Beans;

import java.io.Serializable;

public class CustomerInfo  implements Serializable {
	private static final long serialVersionUID = 1L;
	private long income;
	private long customerAmount;
	private boolean checkAmount;
	
	public CustomerInfo() {
		super();
	}
	public long getIncome() {
	return income;
	}
	public void setIncome(long income) {
	this.income = income;
	}
	public long getCustomerAmount() {
		return customerAmount;
	}
	public void setCustomerAmount(long customerAmount) {
		this.customerAmount = customerAmount;
	}
	public boolean isCheckAmount() {
		return checkAmount;
	}
	public void setCheckAmount(boolean checkAmount) {
		this.checkAmount = checkAmount;
	}
	

}
